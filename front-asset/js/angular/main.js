/* Jquery Code */
/* counrty select dropdown 2 start here */
function DropDown(el) {
  this.dd = el;
  this.placeholder = this.dd.children('span');
  this.opts = this.dd.find('ul.dropdown > li');
  this.val = '';
  this.index = -1;
  this.initEvents();
}
DropDown.prototype = {
  initEvents: function() {
    var obj = this;
    obj.dd.on('click', function(event) {
      $(this).toggleClass('active');
      return false;
    });
    obj.opts.on('click', function() {
      var opt = $(this);
      var obj_a = opt.find("a");
      var language_text = obj_a.text();
      $("#new_word").html(obj_a.html());
    });
  },
  getValue: function() {
    return this.val;
  },
  getIndex: function() {
    return this.index;
  }
}


jQuery(function($)
{
  var dd = new DropDown($('#dd'));
  $(document).click(function() {
    // all dropdowns
    $('.wrapper-dropdown-1').removeClass('active');
  });
});


jQuery(function($)
{
  function DropDown(el) {
    this.ff = el;
    this.placeholder = this.ff.children('span');
    this.opts = this.ff.find('ul.dropdown > li');
    this.val = '';
    this.index = -1;
    this.initEvents();
  }
  DropDown.prototype = {
    initEvents: function() {
     var obj = this;
     obj.ff.on('click', function(event) {
      $(this).toggleClass('active');
      return false;
    });
     obj.opts.on('click', function() {
      var opt = $(this);
      var obj_a = opt.find("a");
      var language_text = obj_a.text();
      $("#new_word_two").html(obj_a.html());
    });
   },
   getValue: function() {
     return this.val;
   },
   getIndex: function() {
     return this.index;
   }
 }  
 var ff = new DropDown($('#ff'));

 $(document).click(function() {

   $('.wrapper-dropdown-1').removeClass('active');
 });
 
});



jQuery(function($)
{
  $('.menu_class').on('click',function()
  {
   var div_id = $(this).attr('lang');             
   $("html, body").animate({
     scrollTop: $('#'+div_id).offset().top-180 
   }, 1000);
   return false;
 });         
  $('.menu_class').on('click',function()
  {
    var arr_link = $('.menu_class');
    $.each(arr_link,function(index,a)
    {
      $(a).removeClass('active-tab');
    });

    $(this).addClass('active-tab');
  });
});




/* footer script start here */
$(document).ready(function() {
    var min_applicable_width = 991;
    applyResponsiveSlideUp($(this).width(), min_applicable_width);
});

function applyResponsiveSlideUp(current_width, min_applicable_width) {
  /* Set For Initial Screen */
  initResponsiveSlideUp(current_width, min_applicable_width);

  /* Listen Window Resize for further changes */
  $(window).bind('resize', function() {
    if ($(this).width() <= min_applicable_width) {
      unbindResponsiveSlideup();
      bindResponsiveSlideup();
    } else {
      unbindResponsiveSlideup();
    }
  });
}

function initResponsiveSlideUp(current_width, min_applicable_width) {
  if (current_width <= min_applicable_width) {
    unbindResponsiveSlideup();
    bindResponsiveSlideup();
  } else {
    unbindResponsiveSlideup();
  }
}

function bindResponsiveSlideup() {
  $(".menu_name").hide();
  $(".footer_heading").bind('click', function() {
    var $ans = $(this).parent().find(".menu_name");
    $ans.slideToggle();
    $(".menu_name").not($ans).slideUp();
    $('.menu_name').removeClass('active');

    $('.footer_heading').not($(this)).removeClass('active');
    $(this).toggleClass('active');
    $(this).parent().find(".menu_name").toggleClass('active');
  });
}

function unbindResponsiveSlideup() {
  $(".footer_heading").unbind('click');
  $(".menu_name").show();
}

/* footer script end here */

/*scrollbar start here*/
 (function($){
           
       $(window).on("load",function(){
 
 $.mCustomScrollbar.defaults.scrollButtons.enable=true; //enable scrolling buttons by default
 $.mCustomScrollbar.defaults.axis="yx"; //enable 2 axis scrollbars by default
 
       $(".scroll-block").mCustomScrollbar({theme:"dark"});
      
 });
 })(jQuery);
/*scrollbar end here*/
/*  ------ Project ---------*/


/* Angular Code */
var app = angular.module('miniMe', ['ngRoute', 'ngMessages','ui.bootstrap','ngAutocomplete','ngFileUpload'])
app.controller('ctrl', ['$scope', function($scope){
        
}]);

app.controller('ContryDropdown', function($scope) 
{
  
});
/* select dropdown 2 end here */

/* account phone dropdown start here */
app.controller('ContryDropdownThree', function($scope) 
{     


});
/* account phone dropdown start here */

/* header menu responsive script start here */
app.controller('HeaderCntrl', function($scope) 
{ 
    $scope.openNav = function() {       
            document.getElementById("mySidenav").style.width = "250px";
            $("body").css({
                "margin-left": "250px",
                "overflow-x": "hidden",
                "transition": "margin-left .5s",
                "position": "fixed"
            });
            $("#main").addClass("overlay");
        }
 $scope.closeNav = function() {        
            document.getElementById("mySidenav").style.width = "0";
            $("body").css({
                "margin-left": "0px",
                "transition": "margin-left .5s",
                "position": "relative"
            });
            $("#main").removeClass("overlay");
        }

        //  Min Top Sub Menu Start Here  



        $(".min-menu > li > .drop-block").click(function() {
        if (false == $(this).next().hasClass('menu-active')) {
        $('.sub-menu > ul').removeClass('menu-active');
        }
        $(this).next().toggleClass('menu-active');        

        return false;
        });

        // script for hide show of sub menu

        //     $('.main-content').click(function(){
        //         $(this).next('.sub-menu').slideToggle('1000');
        //         $(this).find('.arrow i').toggleClass('fa-angle-down fa-angle-up')
        //         });

        $("body").click(function() {
        $('.sub-menu > ul').removeClass('menu-active');
        });
});
/* header menu responsive script end here */

/* script for sticky header*/
app.directive('setClassWhenAtTop', function ($window) {
    var $win = angular.element($window); // wrap window object as jQuery object

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var topClass = attrs.setClassWhenAtTop, // get CSS class from directive's attribute value
                offsetTop = element.offset().top; // get element's top relative to the document
            var extraOffsetAdjustment = attrs.extraOffsetAdjustment || 0;
            $win.on('scroll', function (e) {
                if ($win.scrollTop() > offsetTop - extraOffsetAdjustment) {
                    element.addClass(topClass);
                } else {
                    element.removeClass(topClass);
                }
            });
        }
    };
});

app.controller('ctrl', function ($scope) {
    $scope.scrollTo = function (target)
    {
    };
});
/* script for sticky header end */
/* script for  start here*/
app.controller('quickCallctrl', function ($scope) {


});
            
/* script for  end here*/

/**
* Configure the Routes
*/
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
    // Home
    .when("/", { templateUrl: "home.html", controller: "PageCtrl" })
    .when("/dashboard", { templateUrl: "dashboard.html", controller: "PageCtrl" })
    .when("/listing-page", { templateUrl: "listing-page.html", controller: "PageCtrl" })
    .when("/login", { templateUrl: "login.html", controller: "PageCtrl" })
    .when("/sign-up", { templateUrl: "sign-up.html", controller: "PageCtrl" })
    .when("/message", { templateUrl: "message.html", controller: "PageCtrl" })
    .when("/my-inquiries", { templateUrl: "my-inquiries.html", controller: "PageCtrl" })
    .when("/my-inquiries-details", { templateUrl: "my-inquiries-details.html", controller: "PageCtrl" })
    .when("/notifications", { templateUrl: "notifications.html", controller: "PageCtrl" })
    .when("/review&ratings", { templateUrl: "review&ratings.html", controller: "PageCtrl" })
    .when("/seller-profile", { templateUrl: "seller-profile.html", controller: "PageCtrl" })
    .when("/about-us", { templateUrl: "about-us.html", controller: "PageCtrl" })
    .when("/account-setting", { templateUrl: "account-setting.html", controller: "PageCtrl" })
    .when("/edit-profile", { templateUrl: "edit-profile.html", controller: "PageCtrl" })
    .when("/forgot-password", { templateUrl: "forgot-password.html", controller: "PageCtrl" })
    .when("/how-it-work", { templateUrl: "how-it-work.html", controller: "PageCtrl" })
    .when("/contact-us", { templateUrl: "contact-us.html", controller: "PageCtrl" })
    .when("/404-page", { templateUrl: "404-page.html", controller: "PageCtrl" })    
    .when("/pricing", { templateUrl: "pricing.html", controller: "PageCtrl" })
    .when("/reset-password", { templateUrl: "reset-password.html", controller: "PageCtrl" })
    .when("/help", { templateUrl: "help.html", controller: "PageCtrl" })
    
    // else 404
    .otherwise("/404", { templateUrl: "404.html", controller: "PageCtrl" });
} ]);
app.controller('PageCtrl', function ( $scope,$location/*, $location, $http */) {
    console.log("Page Controller reporting for duty.");
   // $scope.pageClass = 'page-effect';
    console.log($location.path());
});

/* tabbing start here */
app.controller('TabsDemoCtrl', function ($scope, $window) {
  $scope.tabs = [
    { title:'Dynamic Title 1', content:'Dynamic content 1' },
    { title:'Dynamic Title 2', content:'Dynamic content 2', disabled: true }
  ];


  $scope.model = {
    name: 'Tabs'
  };
});
/* tabbing end here */




/*--------------------------------------------------------------------
| Sign Up.                
---------------------------------------------------------------------*/
app.controller('SignupCntrl', function ($scope, $http,$httpParamSerializerJQLike) {
  
  $scope.user             = {};
  $scope.storeSignup      = storeSignup;
  $scope.obj_autocomplete = {};
  

  /* on change function */
  $scope.$watch("obj_autocomplete",function(new_val,old_val)
  {
    if(typeof new_val != 'undefined' &&  typeof new_val.address_components != 'undefined')
    {
      $scope.user.state = "";
      $scope.user.city = "";
      $scope.user.zipcode = "";
      $scope.user.latitude ="";
      $scope.user.longitude ="";

      
      angular.forEach(new_val.address_components,function(val,index)
      {
        /*if(val.types.indexOf('postal_code') != -1)
        {
          $scope.user.zipcode = val.long_name;
        }*/
      
        if(val.types.indexOf('locality') != -1)
        {
          $scope.user.city = val.long_name;
        }

        if(val.types.indexOf('administrative_area_level_1') != -1)
        {
          $scope.user.state = val.long_name;
        }

        if(val.types.indexOf('country') != -1)
        {
          $scope.user.country = val.long_name;
        }

             
        $scope.user.latitude  = new_val.address_components.lat;
        $scope.user.longitude = new_val.address_components.lng;
        
      });
    }
  });


  function storeSignup () 
  {
      if($scope.user.pwd != $scope.user.cmf_pwd){
        $('#err_cmpass').html('Password does not match.');
        setTimeout(function(){
        $('#err_cmpass').html('');
        },3000);
        return false;
      }

      ajaxindicatorstart();
      $http({
           method   : 'POST',
           url      : site_url+"signup/register",
           data     : $httpParamSerializerJQLike($scope.user),
           headers  : {
                       'Content-Type': 'application/x-www-form-urlencoded'
                    }
      }).then(function successCallback(response) 
      { 
          ajaxindicatorstop();
          if(response.data.status == "success") {
               
            $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
            $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');

            /* resetForm */
            $scope.user = {};
            $scope.signupUserForm.$setPristine();
            $scope.signupUserForm.$setUntouched();
            /* end resetForm */


          } else if(response.data.status == "error") {

            $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
            $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
          }
      }, function errorCallback(response) {

          ajaxindicatorstop();
          $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
          $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
      }); // end errorCallback
  };  // end SignupCntrl
});// end SignupCntrl

/*--------------------------------------------------------------------
| Login Module.
---------------------------------------------------------------------*/
app.controller('LoginCntrl', function ($scope, $http,$httpParamSerializerJQLike) {
  
  $scope.user             = {};
  $scope.processLogin      = processLogin;

  angular.element('#radio5').on('click',function()
  {
     if($('#hidden_email').val() !="" && $('#hidden_pass').val()!=""){
       $scope.user.email = $('#hidden_email').val();
       $('#email').val($('#hidden_email').val());

       $scope.user.pwd = $('#hidden_pass').val();
       $('#pwd').val($('#hidden_pass').val());
     }
  });

  function processLogin () 
  {
      ajaxindicatorstart();
      $http({
           method   : 'POST',
           url      : site_url+"login/process",
           data     : $httpParamSerializerJQLike($scope.user),
           headers  : {
                       'Content-Type': 'application/x-www-form-urlencoded'
                    }
      }).then(function successCallback(response) 
      { 
          ajaxindicatorstop();
          if(response.data.status == "LoginSuccess") {
           
                $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
                if(response.data.user_type == "Buyer"){
                    location.href = site_url+'buyer/dashboard';
                }
                else
                if(response.data.user_type == "Seller"){
                    location.href = site_url+'seller/dashboard';
                }  
                else {
                  $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> Something Wrong... </div>');
                  $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
                }

          } else if(response.data.status == "UnverifyUser" || response.data.status == "BlockUser" || response.data.status == "notFound") {

            $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
            $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
          }
      }, function errorCallback(response) {

          ajaxindicatorstop();
          $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
          $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
      }); // end errorCallback
  };  // end processLogin
});// end LoginCntrl


/*--------------------------------------------------------------------
| Forgot Password Module.
---------------------------------------------------------------------*/
app.controller('ForgotPassCntrl', function ($scope, $http,$httpParamSerializerJQLike) {
  
  $scope.user               = {};
  $scope.processForgotPass  = processForgotPass;

  function processForgotPass () 
  {
      ajaxindicatorstart();
      $http({
           method   : 'POST',
           url      : site_url+"login/forgot_password",
           data     : $httpParamSerializerJQLike($scope.user),
           headers  : {
                       'Content-Type': 'application/x-www-form-urlencoded'
                    }
      }).then(function successCallback(response) 
      { 
          ajaxindicatorstop();
          if(response.data.status == "success") {
                 $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                 $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');

                  /* resetForm */
                  $scope.user = {};
                  $scope.ForgotPassForm.$setPristine();
                  $scope.ForgotPassForm.$setUntouched();
                  /* end resetForm */

          } else {
                $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
          }
      }, function errorCallback(response) {

          ajaxindicatorstop();
          $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
          $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
      }); // end errorCallback
  };  // end processForgotPass
});// end ForgotPassCntrl

/*--------------------------------------------------------------------
| Reset Password Module.
---------------------------------------------------------------------*/
app.controller('ResetPassCntrl', function ($scope, $http,$httpParamSerializerJQLike) {
  
  $scope.user               = {};
  $scope.processResetPass   = processResetPass;

  function processResetPass () 
  {
      if($scope.user.newpwd != $scope.user.cmf_pwd){
      $('#err_cmpass').html('Password does not match.');
      setTimeout(function(){
      $('#err_cmpass').html('');
      },3000);
      return false;
      }

      ajaxindicatorstart();
      $http({
           method   : 'POST',
           url      : site_url+"login/reset_password",
           data     : $httpParamSerializerJQLike($scope.user),
           headers  : {
                       'Content-Type': 'application/x-www-form-urlencoded'
                    }
      }).then(function successCallback(response) 
      { 
          ajaxindicatorstop();
          if(response.data.status == "success") {
                $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');

                /* resetForm */
                $scope.user = {};
                $scope.ResetPassForm.$setPristine();
                $scope.ResetPassForm.$setUntouched();
                /* end resetForm */
                
          } else {
                $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
          }
      }, function errorCallback(response) {

          ajaxindicatorstop();
          $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
          $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
      }); // end errorCallback
  };  // end processResetPass
});// end ResetPassCntrl




/*--------------------------------------------------------------------
| SELLER SECTION
---------------------------------------------------------------------*/



    /*--------------------------------------------------------------------
    | Edit Profile
    ---------------------------------------------------------------------*/
    app.controller('SellerEditProfileCntrl', function ($scope, $http,$httpParamSerializerJQLike,Upload) {
      
      $scope.user                    = {};
      $scope.loadSellerProfile       = loadSellerProfile;
      $scope.editProfileSave         = editProfileSave;
      $scope.obj_autocomplete        = {};
        
      $scope.$watch("obj_autocomplete",function(new_val,old_val)
      {
        if(typeof new_val != 'undefined' &&  typeof new_val.address_components != 'undefined')
        {
          $scope.user.state   = "";
          $scope.user.city    = "";
          $scope.user.pincode = "";

          
          angular.forEach(new_val.address_components,function(val,index)
          {
            if(val.types.indexOf('postal_code') != -1)
            {
              $scope.user.pincode = val.long_name;
            }

            if(val.types.indexOf('locality') != -1)
            {
              $scope.user.city = val.long_name;
            }

            if(val.types.indexOf('administrative_area_level_1') != -1)
            {
              $scope.user.state = val.long_name;
            }

            if(val.types.indexOf('country') != -1)
            {
              $scope.user.country = val.long_name;
            }

            $scope.user.latitude  = new_val.address_components.lat;
            $scope.user.longitude = new_val.address_components.lng;

          });
        }
      });  

      angular.element('#user_image').on('change',function()
      {
          readURL(this);
      });
      function readURL(input) 
      {   
          if (input.files && input.files[0]) 
          {   
              ajaxindicatorstart();

              var  ext_a  = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1);
              if(!(ext_a == "jpg" || ext_a == "jpeg" || ext_a == "gif" || ext_a == "png" || ext_a == "GIF" || ext_a == "JPG" || ext_a == "JPEG" || ext_a == "PNG")) {
                    //alert('Only jpg, png, gif, jpeg type images is allowed');
                    $(this).val('');
                    ajaxindicatorstop();
                    return;
              }
              else {
                      /* Height And Width Restrict */
                        var fileUpload = input;
                        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+()$");
                          if (regex.test(fileUpload.value.toLowerCase())) {
                       
                              //Check whether HTML5 is supported.
                              if (typeof (fileUpload.files) != "undefined") {
                                  //Initiate the FileReader object.
                                  var reader = new FileReader();
                                  //Read the contents of Image File.
                                  reader.readAsDataURL(fileUpload.files[0]);
                                  reader.onload = function (e) {
                                      //Initiate the JavaScript Image object.
                                      var image = new Image();
                       
                                      //Set the Base64 string return from FileReader as source.
                                      image.src = e.target.result;
                                             
                                      //Validate the File Height and Width.
                                      image.onload = function () {
                                          var height = this.height;
                                          var width = this.width;
                                          if (height < 160 || width < 160) {
                                              $(this).val('');
                                              sweetAlert("NOTE! : Profile Image allow greater than 160 x 160 (h X w) dimension only.");
                                              ajaxindicatorstop();
                                              return false;
                                          }
                                          else {

                                              /* angular js code */
                                              var reader = new FileReader();

                                              reader.onload = function (e) 
                                              {
                                                angular.element("#img_profile").attr('src', e.target.result);
                                              }
                                              var user_image = input.files[0];
                                              reader.readAsDataURL(input.files[0]);
                                              $scope.user.user_image = input.files[0];
                                              ajaxindicatorstop();
                                              return true;
                                              /* end angular js code */

                                          }
                                      };
                                  }
                              } else {
                                  angular.element("#img_profile").val('');
                                  sweetAlert("This browser does not support HTML5.");
                                  ajaxindicatorstop();
                                  return false;
                              }
                          } else {
                              angular.element("#img_profile").val('');
                              sweetAlert("Please select a valid Image file.");
                              ajaxindicatorstop();
                              return false;
                          }
                      /* end Height And Width Restrict */
              }
          }
      }


      function loadSellerProfile() 
      {
        $http({
               method:"GET",
               url:site_url+'seller/get_profile',
        }).then(function successCallback(response)
        { 
          if(response.data.status == 'success')
          { 
            console.log(response.data.profile_data);
            $scope.user   = response.data.profile_data;

            var user_image = response.data.profile_data.user_image;

            if(user_image != "")   
            {
            $scope.user_image = site_url+'/images/seller_image/'+ response.data.profile_data.user_image;  
            }
            else
            {
            $scope.user_image = site_url+'/images/default/default-user-img.jpg';
            }
          }
        },function errorCallback(error) {}); 
      }

      function editProfileSave ()  {
          ajaxindicatorstart();
          var file = {};
          file.upload = Upload.upload(
          {
            url:  site_url+'seller/update' ,
            data: $scope.user,
          });

          file.upload.then(function (response) 
          {
              ajaxindicatorstop();

              if(response.data.status == "success_with_reload") {

                 $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                 $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
                 setTimeout(function(){
                  location.reload();
                 }, 500);

              } else  if(response.data.status == "success") {

                 $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                 $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
                 setTimeout(function(){
                  location.reload();
                 }, 500);

              } else if(response.data.status == "error") {

                 $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                 $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
              }
          },function (response) {
            ajaxindicatorstop();
            if (response.status > 0)
                $scope.errorMsg = response.status + ': ' + response.data;
          }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          });   
      };


    });// end SellerEditProfileCntrl


   
    /*--------------------------------------------------------------------
    | Change Profile
    ---------------------------------------------------------------------*/

    app.controller('SellerChangePassCntrl', function ($scope, $http,$httpParamSerializerJQLike) {
      
      $scope.user                    = {};
      $scope.ChangePassword          = ChangePassword;
      function ChangePassword() 
      {
          if($scope.user.newpwd != $scope.user.cmf_pwd){
          $('#err_cmpass').html('Password does not match.');
          setTimeout(function(){
          $('#err_cmpass').html('');
          },3000);
          return false;
          }

          ajaxindicatorstart();
          $http({
               method   : 'POST',
               url      : site_url+"seller/change_password",
               data     : $httpParamSerializerJQLike($scope.user),
               headers  : {
                           'Content-Type': 'application/x-www-form-urlencoded'
                        }
          }).then(function successCallback(response) 
          { 
              ajaxindicatorstop();
              if(response.data.status == "success") {
                    $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');


                    /* resetForm */
                    $scope.user = {};
                    $scope.ChangePass.$setPristine();
                    $scope.ChangePass.$setUntouched();
                    /* end resetForm */

              } else {
                    $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
              }
          }, function errorCallback(response) {

              ajaxindicatorstop();
              $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
              $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
          }); // end errorCallback
      };  // end processResetPass

    });// end SellerChangePassCntrl  


    /*--------------------------------------------------------------------
    | POST OFFER
    ---------------------------------------------------------------------*/
    app.controller('PostOffer', function ($scope, $http,$httpParamSerializerJQLike,Upload) {
      
      $scope.user                   = {};
      $scope.storePostOffer         = storePostOffer;
      $scope.obj_autocomplete       = {};

      $scope.$watch("obj_autocomplete",function(new_val,old_val)
      {
        if(typeof new_val != 'undefined' &&  typeof new_val.address_components != 'undefined')
        {
          angular.forEach(new_val.address_components,function(val,index)
          {
            if(val.types.indexOf('locality') != -1)
            {
              $scope.user.city = val.long_name;
            }
           
          });
        }
      });

      angular.element('#req_image').on('change',function()
      {
                 readURL(this);

      });
      function readURL(input) 
      {   
          if (input.files && input.files[0]) 
          {   
              ajaxindicatorstart();

              var  ext_a  = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1);
              if(!(ext_a == "jpg" || ext_a == "jpeg" || ext_a == "gif" || ext_a == "png" || ext_a == "GIF" || ext_a == "JPG" || ext_a == "JPEG" || ext_a == "PNG")) {
                    //alert('Only jpg, png, gif, jpeg type images is allowed');
                    $(this).val('');
                    /* for image reload */
                     angular.element("#img_profile").attr('src', site_url+'front-asset/images/gallrey.png');
                    /* for image reload */  
                    ajaxindicatorstop();
                    return false;
              }
              else {
                      /* Height And Width Restrict */
                        var fileUpload = input;
                        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+()$");
                          if (regex.test(fileUpload.value.toLowerCase())) {
                       
                              //Check whether HTML5 is supported.
                              if (typeof (fileUpload.files) != "undefined") {
                                  //Initiate the FileReader object.
                                  var reader = new FileReader();
                                  //Read the contents of Image File.
                                  reader.readAsDataURL(fileUpload.files[0]);
                                  reader.onload = function (e) {
                                      //Initiate the JavaScript Image object.
                                      var image = new Image();
                       
                                      //Set the Base64 string return from FileReader as source.
                                      image.src = e.target.result;
                                             
                                      //Validate the File Height and Width.
                                      image.onload = function () {
                                          var height = this.height;
                                          var width = this.width;
                                          if (height != 162 && width != 260) {

                                              $(this).val('');
                                              /* for image reload */
                                               angular.element("#img_profile").attr('src', site_url+'front-asset/images/gallrey.png');
                                              /* for image reload */  
                                              sweetAlert("NOTE!: Image allow 162 x 260 (h X w) dimension only.");
                                              ajaxindicatorstop();
                                              return false;
                                          }
                                          else {

                                              /* angular js code */
                                              var reader = new FileReader();

                                              reader.onload = function (e) 
                                              {
                                                angular.element("#img_profile").attr('src', e.target.result);
                                              }
                                              var req_image = input.files[0];
                                              reader.readAsDataURL(input.files[0]);
                                              $scope.user.req_image = input.files[0];
                                              ajaxindicatorstop();
                                              return true;
                                              /* end angular js code */

                                          }
                                      };
                                  }
                              } else {
                                  angular.element("#img_profile").val('');
                                  sweetAlert("This browser does not support HTML5.");
                                  ajaxindicatorstop();
                                  return false;
                              }
                          } else {
                              angular.element("#img_profile").val('');
                              sweetAlert("Please select a valid Image file.");
                              ajaxindicatorstop();
                              return false;
                          }
                      /* end Height And Width Restrict */
              }
          }
      }
      
      function storePostOffer()  {
       
          ajaxindicatorstart();
          var file = {};
          file.upload = Upload.upload(
          {
            url:  site_url+'seller/post_offer' ,
            data: $scope.user,
          });

          file.upload.then(function (response) 
          {
              ajaxindicatorstop();
              if(response.data.status == "success") {
                 angular.element('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                 angular.element('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
                 
                 /* resetForm */
                  $scope.user = {};
                  $scope.offerForm.$setPristine();
                  $scope.offerForm.$setUntouched();

                    /* for image reload */
                     angular.element("#img_profile").attr('src', site_url+'front-asset/images/gallrey.png');
                    /* for image reload */  

                 /* end resetForm */  


              } else if(response.data.status == "error") {

                 $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                 $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
              }
          },function (response) {
            ajaxindicatorstop();
            if (response.status > 0)
                $scope.errorMsg = response.status + ': ' + response.data;
          }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          });   
      };

    });// end PostRequirment

    /*--------------------------------------------------------------------
    | UPLOAD PRODUCT
    ---------------------------------------------------------------------*/
    app.controller('UploadProduct', function ($scope, $http,$httpParamSerializerJQLike,Upload) {
      
      $scope.user                   = {};
      $scope.storeProduct         = storeProduct;
      $scope.obj_autocomplete       = {};

      $scope.$watch("obj_autocomplete",function(new_val,old_val)
      {
        if(typeof new_val != 'undefined' &&  typeof new_val.address_components != 'undefined')
        {
          angular.forEach(new_val.address_components,function(val,index)
          {
            if(val.types.indexOf('locality') != -1)
            {
              $scope.user.city = val.long_name;
            }
            $scope.user.latitude  = new_val.address_components.lat;
            $scope.user.longitude = new_val.address_components.lng;

           
          });
        }
      });

      angular.element('#req_image').on('change',function()
      {
                 readURL(this);

      });
      function readURL(input) 
      {   
          if (input.files && input.files[0]) 
          {   
              ajaxindicatorstart();

              var  ext_a  = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1);
              if(!(ext_a == "jpg" || ext_a == "jpeg" || ext_a == "gif" || ext_a == "png" || ext_a == "GIF" || ext_a == "JPG" || ext_a == "JPEG" || ext_a == "PNG")) {
                    //alert('Only jpg, png, gif, jpeg type images is allowed');
                    $(this).val('');
                    /* for image reload */
                     angular.element("#img_profile").attr('src', site_url+'front-asset/images/gallrey.png');
                    /* for image reload */  
                    ajaxindicatorstop();
                    return false;
              }
              else {
                      /* Height And Width Restrict */
                        var fileUpload = input;
                        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+()$");
                          if (regex.test(fileUpload.value.toLowerCase())) {
                       
                              //Check whether HTML5 is supported.
                              if (typeof (fileUpload.files) != "undefined") {
                                  //Initiate the FileReader object.
                                  var reader = new FileReader();
                                  //Read the contents of Image File.
                                  reader.readAsDataURL(fileUpload.files[0]);
                                  reader.onload = function (e) {
                                      //Initiate the JavaScript Image object.
                                      var image = new Image();
                       
                                      //Set the Base64 string return from FileReader as source.
                                      image.src = e.target.result;
                                             
                                      //Validate the File Height and Width.
                                      image.onload = function () {
                                          var height = this.height;
                                          var width = this.width;
                                          if (height < 162 && width < 260) {

                                              $(this).val('');
                                              /* for image reload */
                                               angular.element("#img_profile").attr('src', site_url+'front-asset/images/gallrey.png');
                                              /* for image reload */  
                                              sweetAlert("NOTE!: Image allow greater than 162 x 260 (h X w) dimension only.");
                                              ajaxindicatorstop();
                                              return false;
                                          }
                                          else {

                                              /* angular js code */
                                              var reader = new FileReader();

                                              reader.onload = function (e) 
                                              {
                                                angular.element("#img_profile").attr('src', e.target.result);
                                              }
                                              var req_image = input.files[0];
                                              reader.readAsDataURL(input.files[0]);
                                              $scope.user.req_image = input.files[0];
                                              ajaxindicatorstop();
                                              return true;
                                              /* end angular js code */

                                          }
                                      };
                                  }
                              } else {
                                  angular.element("#img_profile").val('');
                                  sweetAlert("This browser does not support HTML5.");
                                  ajaxindicatorstop();
                                  return false;
                              }
                          } else {
                              angular.element("#img_profile").val('');
                              sweetAlert("Please select a valid Image file.");
                              ajaxindicatorstop();
                              return false;
                          }
                      /* end Height And Width Restrict */
              }
          }
      }
      
      function storeProduct()  {
       
          ajaxindicatorstart();
          var file = {};
          file.upload = Upload.upload(
          {
            url:  site_url+'seller/upload_product' ,
            data: $scope.user,
          });

          file.upload.then(function (response) 
          {
              ajaxindicatorstop();
              if(response.data.status == "success") {
                 angular.element('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp; '+response.data.msg+' </div>');
                 angular.element('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
                 
                 /* resetForm */
                  $scope.user = {};
                  $scope.productForm.$setPristine();
                  $scope.productForm.$setUntouched();

                    /* for image reload */
                     angular.element("#img_profile").attr('src', site_url+'front-asset/images/gallrey.png');
                    /* for image reload */  

                 /* end resetForm */  


              } else if(response.data.status == "error") {

                 $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp; '+response.data.msg+' </div>');
                 $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
              }
          },function (response) {
            ajaxindicatorstop();
            if (response.status > 0)
                $scope.errorMsg = response.status + ': ' + response.data;
          }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          });   
      };

    });// end PostRequirment

    

    /*--------------------------------------------------------------------
    | EDIT  UPLOAD PRODUCT
    ---------------------------------------------------------------------*/
    app.controller('editUploadProduct', function ($scope, $http,$httpParamSerializerJQLike,Upload) {
      
      $scope.user                   = {};
      $scope.storeProduct           = storeProduct;
      $scope.loadeditproduct        = loadeditproduct;
      $scope.obj_autocomplete       = {};

      $scope.$watch("obj_autocomplete",function(new_val,old_val)
      {
        if(typeof new_val != 'undefined' &&  typeof new_val.address_components != 'undefined')
        {
          angular.forEach(new_val.address_components,function(val,index)
          {
            if(val.types.indexOf('locality') != -1)
            {
              $scope.user.city = val.long_name;
            }
            $scope.user.latitude  = new_val.address_components.lat;
            $scope.user.longitude = new_val.address_components.lng;

           
          });
        }
      });

      angular.element('#req_image').on('change',function()
      {
                 readURL(this);

      });
      function readURL(input) 
      {   
          if (input.files && input.files[0]) 
          {   
              ajaxindicatorstart();

              var  ext_a  = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1);
              if(!(ext_a == "jpg" || ext_a == "jpeg" || ext_a == "gif" || ext_a == "png" || ext_a == "GIF" || ext_a == "JPG" || ext_a == "JPEG" || ext_a == "PNG")) {
                    //alert('Only jpg, png, gif, jpeg type images is allowed');
                    $(this).val('');
                    /* for image reload */
                     angular.element("#img_profile").attr('src', site_url+'front-asset/images/gallrey.png');
                    /* for image reload */  
                    ajaxindicatorstop();
                    return false;
              }
              else {
                      /* Height And Width Restrict */
                        var fileUpload = input;
                        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+()$");
                          if (regex.test(fileUpload.value.toLowerCase())) {
                       
                              //Check whether HTML5 is supported.
                              if (typeof (fileUpload.files) != "undefined") {
                                  //Initiate the FileReader object.
                                  var reader = new FileReader();
                                  //Read the contents of Image File.
                                  reader.readAsDataURL(fileUpload.files[0]);
                                  reader.onload = function (e) {
                                      //Initiate the JavaScript Image object.
                                      var image = new Image();
                       
                                      //Set the Base64 string return from FileReader as source.
                                      image.src = e.target.result;
                                             
                                      //Validate the File Height and Width.
                                      image.onload = function () {
                                          var height = this.height;
                                          var width = this.width;
                                          if (height < 162 && width < 260) {

                                              $(this).val('');
                                              /* for image reload */
                                               angular.element("#img_profile").attr('src', site_url+'front-asset/images/gallrey.png');
                                              /* for image reload */  
                                              sweetAlert("NOTE!: Image allow greater than 162 x 260 (h X w) dimension only.");
                                              ajaxindicatorstop();
                                              return false;
                                          }
                                          else {

                                              /* angular js code */
                                              var reader = new FileReader();

                                              reader.onload = function (e) 
                                              {
                                                angular.element("#img_profile").attr('src', e.target.result);
                                              }
                                              var req_image = input.files[0];
                                              reader.readAsDataURL(input.files[0]);
                                              $scope.user.req_image = input.files[0];
                                              ajaxindicatorstop();
                                              return true;
                                              /* end angular js code */

                                          }
                                      };
                                  }
                              } else {
                                  angular.element("#img_profile").val('');
                                  sweetAlert("This browser does not support HTML5.");
                                  ajaxindicatorstop();
                                  return false;
                              }
                          } else {
                              angular.element("#img_profile").val('');
                              sweetAlert("Please select a valid Image file.");
                              ajaxindicatorstop();
                              return false;
                          }
                      /* end Height And Width Restrict */
              }
          }
      }

      function loadeditproduct(product_id) 
      {
          $http({
                 method:"GET",
                 url:site_url+'seller/get_product/'+product_id,
          }).then(function successCallback(response)
          { 
            if(response.data.status == 'success')
            { 
              //console.log(response.data.requirement);
              $scope.user   = response.data.product_data;

              $scope.user.req_image  = response.data.product_data.req_image;

              var req_image = response.data.product_data.req_image;
              if(req_image != "")   
              {
              $scope.req_image = site_url+'images/seller_upload_product_image/'+ response.data.product_data.req_image;  
              }
              else
              {
              $scope.req_image = site_url+'front-asset/images/gallrey.png';
              }
              angular.element('#img_profile').attr("src", $scope.req_image);;
            }
          },function errorCallback(error) {}); 
      }

      
      function storeProduct(product_id)  {
       
          ajaxindicatorstart();
          var file    = {};
          file.upload = Upload.upload(
          {
            url:  site_url+'seller/upload_product/'+product_id,
            data: $scope.user,
          });

          file.upload.then(function (response) 
          {
              ajaxindicatorstop();
              if(response.data.status == "success") {
                 angular.element('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp; '+response.data.msg+' </div>');
                 angular.element('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
                 
                 /* resetForm */
                  $scope.user = {};
                  $scope.productForm.$setPristine();
                  $scope.productForm.$setUntouched();

                    /* for image reload */
                     angular.element("#img_profile").attr('src', site_url+'front-asset/images/gallrey.png');
                    /* for image reload */  

                     if(response.data.redirect == "yes"){
                      setTimeout(function(){
                        location.href=site_url+'seller/product_detail/'+product_id;
                      },1500);
                     }

                 /* end resetForm */  
              } else if(response.data.status == "error") {

                 $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp; '+response.data.msg+' </div>');
                 $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
              }
          },function (response) {
            ajaxindicatorstop();
            if (response.status > 0)
                $scope.errorMsg = response.status + ': ' + response.data;
          }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          });   
      };

    });// end PostRequirment


/*--------------------------------------------------------------------
  | BUYER MAKE OFFER TO REQUIRMENT
  ---------------------------------------------------------------------*/

      app.controller('InqueryToInquery', function ($scope, $http,$httpParamSerializerJQLike) {
      $scope.user              = {};
      $scope.inqueryReply        = inqueryReply;

      angular.element('.post_data').on('click',function()
      {
         $scope.user.buyer_id = $(this).data('buyerid');
         angular.element('#buyer_id').val($(this).data('buyerid'));
         $scope.user.inquiry_id = $(this).data('inquryid');
         angular.element('#inquiry_id').val($(this).data('inquryid'));
         
      });

      function inqueryReply() 
      {
          ajaxindicatorstart();
          $http({
               method   : 'POST',
               url      : site_url+"seller/inquiry_reply",
               data     : $httpParamSerializerJQLike($scope.user),
               headers  : {
                           'Content-Type': 'application/x-www-form-urlencoded'
                        }
          }).then(function successCallback(response) 
          { 
              ajaxindicatorstop();
              if(response.data.status == "success") {
                    $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                    $('.status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');

                  /* resetForm */
                  $scope.user = {};
                  $scope.replyToinquery.$setPristine();
                  $scope.replyToinquery.$setUntouched();
                  /* end resetForm */

                  setTimeout(function(){
                  angular.element('.close').click();
                  },2000); 
 

              } else {
                    $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                    $('.status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
              }
          }, function errorCallback(response) {
              ajaxindicatorstop();
              $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
              $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
          }); // end errorCallback
      };  // end processResetPass

    });// end OfferToRequirmentCntrl 


    app.controller('ReplyToInquery', function ($scope, $http,$httpParamSerializerJQLike) {
      $scope.user              = {};
      $scope.ReplyInquery        = ReplyInquery;

      angular.element('.post_data').on('click',function()
      {
         $scope.user.buyer_id = $(this).data('buyerid');
         angular.element('#buyer_id').val($(this).data('buyerid'));
         $scope.user.inquiry_id = $(this).data('inquryid');
         angular.element('#inquiry_id').val($(this).data('inquryid'));
         
      });

      function ReplyInquery() 
      {
         $scope.user.apply_offer_id=$('#apply_offer_id').val();
         $scope.user.buyer_id=$('#id_buyer').val();
         $scope.user.id_seller=$('#id_seller').val();
         $scope.user.inquiry_id=$('#id_offer').val();  // Offer id
        
          ajaxindicatorstart();
          $http({
               method   : 'POST',
               url      : site_url+"seller/reply_inquiry",
               data     : $httpParamSerializerJQLike($scope.user),
               headers  : {
                           'Content-Type': 'application/x-www-form-urlencoded'
                        }
          }).then(function successCallback(response) 
          { 
              ajaxindicatorstop();
              if(response.data.status == "success") {
                    $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                    $('.status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');

                  /* resetForm */
                  $scope.user = {};
                  $scope.replyToinquery.$setPristine();
                  $scope.replyToinquery.$setUntouched();
                  /* end resetForm */

                  setTimeout(function(){
                  angular.element('.close').click();
                  },2000); 
 

              } else {
                    $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                    $('.status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
              }
          }, function errorCallback(response) {
              ajaxindicatorstop();
              $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
              $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
          }); // end errorCallback
      };  // end processResetPass

    });// end OfferToRequirmentCntrl   

  /*--------------------------------------------------------------------
  | SELLER MAKE OFFER TO REQUIRMENT
  ---------------------------------------------------------------------*/
/*--------------------------------------------------------------------
| END SELLER SECTION
---------------------------------------------------------------------*/




/*--------------------------------------------------------------------
| BUYER SECTION
---------------------------------------------------------------------*/



    /*--------------------------------------------------------------------
    | Edit Profile
    ---------------------------------------------------------------------*/
    app.controller('BuyerEditProfileCntrl', function ($scope, $http,$httpParamSerializerJQLike,Upload) {
      
      $scope.user                    = {};
      $scope.loadBuyerProfile        = loadBuyerProfile;
      $scope.editProfileSave         = editProfileSave;
      $scope.obj_autocomplete        = {};
        
      $scope.$watch("obj_autocomplete",function(new_val,old_val)
      {
        if(typeof new_val != 'undefined' &&  typeof new_val.address_components != 'undefined')
        {
          $scope.user.state   = "";
          $scope.user.city    = "";
          $scope.user.pincode = "";

          
          angular.forEach(new_val.address_components,function(val,index)
          {
            if(val.types.indexOf('postal_code') != -1)
            {
              $scope.user.pincode = val.long_name;
            }

            if(val.types.indexOf('locality') != -1)
            {
              $scope.user.city = val.long_name;
            }

            if(val.types.indexOf('administrative_area_level_1') != -1)
            {
              $scope.user.state = val.long_name;
            }

            if(val.types.indexOf('country') != -1)
            {
              $scope.user.country = val.long_name;
            }

            $scope.user.latitude  = new_val.address_components.lat;
            $scope.user.longitude = new_val.address_components.lng;

          });
        }
      });  

      angular.element('#user_image').on('change',function()
      {
           readURL(this);

      });
      

      function readURL(input) 
      {   
          if (input.files && input.files[0]) 
          {   
              ajaxindicatorstart();

              var  ext_a  = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1);
              if(!(ext_a == "jpg" || ext_a == "jpeg" || ext_a == "gif" || ext_a == "png" || ext_a == "GIF" || ext_a == "JPG" || ext_a == "JPEG" || ext_a == "PNG")) {
                    //alert('Only jpg, png, gif, jpeg type images is allowed');
                    $(this).val('');
                    ajaxindicatorstop();
                    return false;
              }
              else {
                      /* Height And Width Restrict */
                        var fileUpload = input;
                        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+()$");
                          if (regex.test(fileUpload.value.toLowerCase())) {
                       
                              //Check whether HTML5 is supported.
                              if (typeof (fileUpload.files) != "undefined") {
                                  //Initiate the FileReader object.
                                  var reader = new FileReader();
                                  //Read the contents of Image File.
                                  reader.readAsDataURL(fileUpload.files[0]);
                                  reader.onload = function (e) {
                                      //Initiate the JavaScript Image object.
                                      var image = new Image();
                       
                                      //Set the Base64 string return from FileReader as source.
                                      image.src = e.target.result;
                                             
                                      //Validate the File Height and Width.
                                      image.onload = function () {
                                          var height = this.height;
                                          var width = this.width;
                                          if (height < 160 && width < 160) {
                                              $(this).val('');
                                              sweetAlert("NOTE!: Profile Image allow greater than 160 x 160 (h X w) dimension only.");
                                              ajaxindicatorstop();
                                              return false;
                                          }
                                          else {

                                              /* angular js code */
                                              var reader = new FileReader();

                                              reader.onload = function (e) 
                                              {
                                                angular.element("#img_profile").attr('src', e.target.result);
                                              }
                                              var user_image = input.files[0];
                                              reader.readAsDataURL(input.files[0]);
                                              $scope.user.user_image = input.files[0];
                                              ajaxindicatorstop();
                                              return true;
                                              /* end angular js code */

                                          }
                                      };
                                  }
                              } else {
                                  angular.element("#img_profile").val('');
                                  sweetAlert("This browser does not support HTML5.");
                                  ajaxindicatorstop();
                                  return false;
                              }
                          } else {
                              angular.element("#img_profile").val('');
                              sweetAlert("Please select a valid Image file.");
                              ajaxindicatorstop();
                              return false;
                          }
                      /* end Height And Width Restrict */
              }
          }
      }


      function loadBuyerProfile() 
      {
        $http({
               method:"GET",
               url:site_url+'buyer/get_profile',
        }).then(function successCallback(response)
        { 
          if(response.data.status == 'success')
          { 
            console.log(response.data.profile_data);
            $scope.user   = response.data.profile_data;

            var user_image = response.data.profile_data.user_image;

            if(user_image != "")   
            {
             $scope.user_image = site_url+'/images/buyer_image/'+ response.data.profile_data.user_image;  
            }
            else
            {
             $scope.user_image = site_url+'/images/default/default-user-img.jpg';
            }


            /* check file is not empty or exist */
            /*$.ajax({
                url: site_url+'/images/buyer_image/'+ response.data.profile_data.user_image,
                type:'HEAD',
                error: function()
                {
                    $scope.user_image = site_url+'/images/default/default-user-img.jpg';
                },
                success: function()
                {
                    if(user_image != "")   
                    {
                       $scope.user_image = site_url+'/images/buyer_image/'+ response.data.profile_data.user_image;  
                    }
                    else
                    {
                       $scope.user_image = site_url+'/images/default/default-user-img.jpg';
                    }
                }
            });*/
            /* end check file is not empty or exist */

          }
        },function errorCallback(error) {}); 
      }

      function editProfileSave ()  {
          ajaxindicatorstart();
          var file = {};
          file.upload = Upload.upload(
          {
            url:  site_url+'buyer/update' ,
            data: $scope.user,
          });

          file.upload.then(function (response) 
          {
              ajaxindicatorstop();

              if(response.data.status == "success_with_reload") {

                 $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                 $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
                 setTimeout(function(){
                  location.reload();
                 }, 500);

              } else  if(response.data.status == "success") {

                 $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                 $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
                 setTimeout(function(){
                  location.reload();
                 }, 500);

              } else if(response.data.status == "error") {

                 $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                 $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
              }
          },function (response) {
            ajaxindicatorstop();
            if (response.status > 0)
                $scope.errorMsg = response.status + ': ' + response.data;
          }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          });   
      };

    });// end BuyerEditProfileCntrl


   
    /*--------------------------------------------------------------------
    | Change Profile
    ---------------------------------------------------------------------*/

    app.controller('BuyerChangePassCntrl', function ($scope, $http,$httpParamSerializerJQLike) {
      
      $scope.user                    = {};
      $scope.ChangePassword          = ChangePassword;
      function ChangePassword() 
      {
          if($scope.user.newpwd != $scope.user.cmf_pwd){
          $('#err_cmpass').html('Password does not match.');
          setTimeout(function(){
          $('#err_cmpass').html('');
          },3000);
          return false;
          }

          ajaxindicatorstart();
          $http({
               method   : 'POST',
               url      : site_url+"buyer/change_password",
               data     : $httpParamSerializerJQLike($scope.user),
               headers  : {
                           'Content-Type': 'application/x-www-form-urlencoded'
                        }
          }).then(function successCallback(response) 
          { 
              ajaxindicatorstop();
              if(response.data.status == "success") {
                    $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');

                  /* resetForm */
                  $scope.user = {};
                  $scope.ChangePass.$setPristine();
                  $scope.ChangePass.$setUntouched();
                  /* end resetForm */



              } else {
                    $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
              }
          }, function errorCallback(response) {

              ajaxindicatorstop();
              $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
              $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
          }); // end errorCallback
      };  // end processResetPass

    });// end BuyerChangePassCntrl  



    /*--------------------------------------------------------------------
    | POST REQUIRMENT
    ---------------------------------------------------------------------*/
    app.controller('PostRequirment', function ($scope, $http,$httpParamSerializerJQLike,Upload) {
      
      $scope.user                   = {};
      $scope.storePostRequirment         = storePostRequirment;
      $scope.obj_autocomplete       = {};

      $scope.resetForm  = resetForm;

      function resetForm(requirmentForm) {
        $scope.user = {};
        requirmentForm.$setPristine();
        requirmentForm.$setUntouched();
      }

      $scope.$watch("obj_autocomplete",function(new_val,old_val)
      {
        if(typeof new_val != 'undefined' &&  typeof new_val.address_components != 'undefined')
        {
          angular.forEach(new_val.address_components,function(val,index)
          {
            if(val.types.indexOf('locality') != -1)
            {
              $scope.user.city = val.long_name;
            }

            $scope.user.latitude  = new_val.address_components.lat;
            $scope.user.longitude = new_val.address_components.lng;
           
          });
        }
      });

      angular.element('#req_image').on('change',function()
      {
                 readURL(this);

      });
      function readURL(input) 
      {   
          if (input.files && input.files[0]) 
          {   
              ajaxindicatorstart();

              var  ext_a  = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1);
              if(!(ext_a == "jpg" || ext_a == "jpeg" || ext_a == "gif" || ext_a == "png" || ext_a == "GIF" || ext_a == "JPG" || ext_a == "JPEG" || ext_a == "PNG")) {
                    //alert('Only jpg, png, gif, jpeg type images is allowed');
                    $(this).val('');
                    /* for image reload */
                     angular.element("#img_profile").attr('src', site_url+'front-asset/images/gallrey.png');
                    /* for image reload */  
                    ajaxindicatorstop();
                    return false;
              }
              else {
                      /* Height And Width Restrict */
                        var fileUpload = input;
                        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+()$");
                          if (regex.test(fileUpload.value.toLowerCase())) {
                       
                              //Check whether HTML5 is supported.
                              if (typeof (fileUpload.files) != "undefined") {
                                  //Initiate the FileReader object.
                                  var reader = new FileReader();
                                  //Read the contents of Image File.
                                  reader.readAsDataURL(fileUpload.files[0]);
                                  reader.onload = function (e) {
                                      //Initiate the JavaScript Image object.
                                      var image = new Image();
                       
                                      //Set the Base64 string return from FileReader as source.
                                      image.src = e.target.result;
                                             
                                      //Validate the File Height and Width.
                                      image.onload = function () {
                                          var height = this.height;
                                          var width = this.width;
                                          if (height < 162 && width < 260) {

                                              $(this).val('');
                                              /* for image reload */
                                               angular.element("#img_profile").attr('src', site_url+'front-asset/images/gallrey.png');
                                              /* for image reload */  
                                              sweetAlert("NOTE!: Image allow greater than 162 x 260 (h X w) dimension only.");
                                              ajaxindicatorstop();
                                              return false;
                                          }
                                          else {

                                              /* angular js code */
                                              var reader = new FileReader();

                                              reader.onload = function (e) 
                                              {
                                                angular.element("#img_profile").attr('src', e.target.result);
                                              }
                                              var req_image = input.files[0];
                                              reader.readAsDataURL(input.files[0]);
                                              $scope.user.req_image = input.files[0];
                                              ajaxindicatorstop();
                                              return true;
                                              /* end angular js code */

                                          }
                                      };
                                  }
                              } else {
                                 angular.element("#img_profile").val('');
                                 sweetAlert("This browser does not support HTML5.");
                                  ajaxindicatorstop();
                                  return false;
                              }
                          } else {
                              angular.element("#img_profile").val('');
                              sweetAlert("Please select a valid Image file.");
                              ajaxindicatorstop();
                              return false;
                          }
                      /* end Height And Width Restrict */
              }
          }
      }
      
      function storePostRequirment()  {
          ajaxindicatorstart();
          var file = {};
          file.upload = Upload.upload(
          {
            url:  site_url+'buyer/post_requirment' ,
            data: $scope.user,
          });

          file.upload.then(function (response) 
          {
              ajaxindicatorstop();
              if(response.data.status == "success") {
                 angular.element('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                 angular.element('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
                 
                 /* resetForm */
                  $scope.user = {};
                  $scope.requirmentForm.$setPristine();
                  $scope.requirmentForm.$setUntouched();

                    /* for image reload */
                     angular.element("#img_profile").attr('src', site_url+'front-asset/images/gallrey.png');
                    /* for image reload */  

                 /* end resetForm */  


              } else if(response.data.status == "error") {

                 $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                 $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
              }
          },function (response) {
            ajaxindicatorstop();
            if (response.status > 0)
                $scope.errorMsg = response.status + ': ' + response.data;
          }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          });   
      };

    });// end PostRequirment



    /*--------------------------------------------------------------------
    | POST REQUIRMENT
    ---------------------------------------------------------------------*/
    app.controller('EditPostRequirment', function ($scope, $http,$httpParamSerializerJQLike,Upload) {
      
      $scope.user                   = {};
      $scope.loadeditRequirement    = loadeditRequirement;
      $scope.storeEditRequirment    = storeEditRequirment;
      $scope.obj_autocomplete       = {};

      

      $scope.$watch("obj_autocomplete",function(new_val,old_val)
      {
        if(typeof new_val != 'undefined' &&  typeof new_val.address_components != 'undefined')
        {
          angular.forEach(new_val.address_components,function(val,index)
          {
            if(val.types.indexOf('locality') != -1)
            {
              $scope.user.city = val.long_name;
            }

            $scope.user.latitude  = new_val.address_components.lat;
            $scope.user.longitude = new_val.address_components.lng;
           
          });
        }
      });

      angular.element('#req_image').on('change',function()
      {
                 readURL(this);

      });
      function readURL(input) 
      {   
          if (input.files && input.files[0]) 
          {   
              ajaxindicatorstart();

              var  ext_a  = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1);
              if(!(ext_a == "jpg" || ext_a == "jpeg" || ext_a == "gif" || ext_a == "png" || ext_a == "GIF" || ext_a == "JPG" || ext_a == "JPEG" || ext_a == "PNG")) {
                    //alert('Only jpg, png, gif, jpeg type images is allowed');
                    $(this).val('');
                    /* for image reload */
                     angular.element("#img_profile").attr('src', site_url+'front-asset/images/gallrey.png');
                    /* for image reload */  
                    ajaxindicatorstop();
                    return false;
              }
              else {
                      /* Height And Width Restrict */
                        var fileUpload = input;
                        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+()$");
                          if (regex.test(fileUpload.value.toLowerCase())) {
                       
                              //Check whether HTML5 is supported.
                              if (typeof (fileUpload.files) != "undefined") {
                                  //Initiate the FileReader object.
                                  var reader = new FileReader();
                                  //Read the contents of Image File.
                                  reader.readAsDataURL(fileUpload.files[0]);
                                  reader.onload = function (e) {
                                      //Initiate the JavaScript Image object.
                                      var image = new Image();
                       
                                      //Set the Base64 string return from FileReader as source.
                                      image.src = e.target.result;
                                             
                                      //Validate the File Height and Width.
                                      image.onload = function () {
                                          var height = this.height;
                                          var width = this.width;
                                          if (height < 162 && width < 260) {

                                              $(this).val('');
                                              /* for image reload */
                                               angular.element("#img_profile").attr('src', site_url+'front-asset/images/gallrey.png');
                                              /* for image reload */  
                                              sweetAlert("NOTE!: Image allow greater than 162 x 260 (h X w) dimension only.");
                                              ajaxindicatorstop();
                                              return false;
                                          }
                                          else {

                                              /* angular js code */
                                              var reader = new FileReader();

                                              reader.onload = function (e) 
                                              {
                                                angular.element("#img_profile").attr('src', e.target.result);
                                              }
                                              var req_image = input.files[0];
                                              reader.readAsDataURL(input.files[0]);
                                              $scope.user.req_image = input.files[0];
                                              ajaxindicatorstop();
                                              return true;
                                              /* end angular js code */

                                          }
                                      };
                                  }
                              } else {
                                 angular.element("#img_profile").val('');
                                 sweetAlert("This browser does not support HTML5.");
                                  ajaxindicatorstop();
                                  return false;
                              }
                          } else {
                              angular.element("#img_profile").val('');
                              sweetAlert("Please select a valid Image file.");
                              ajaxindicatorstop();
                              return false;
                          }
                      /* end Height And Width Restrict */
              }
          }
      }

      function loadeditRequirement(requirement_id) 
      {
          $http({
                 method:"GET",
                 url:site_url+'buyer/get_requirement/'+requirement_id,
          }).then(function successCallback(response)
          { 
            if(response.data.status == 'success')
            { 
              //console.log(response.data.requirement);
              $scope.user   = response.data.requirement;

              $scope.user.req_image  = response.data.requirement.req_image;

              var req_image = response.data.requirement.req_image;
              if(req_image != "")   
              {
              $scope.req_image = site_url+'images/buyer_post_requirment_image/'+ response.data.requirement.req_image;  
              }
              else
              {
              $scope.req_image = site_url+'front-asset/images/gallrey.png';
              }
              angular.element('#img_profile').attr("src", $scope.req_image);;
            }
          },function errorCallback(error) {}); 
        }
     
      function storeEditRequirment(requirement_id)  {
          ajaxindicatorstart();
          var file = {};
          file.upload = Upload.upload(
          {
            url:  site_url+'buyer/post_requirment/'+requirement_id ,
            data: $scope.user,
          });

          file.upload.then(function (response) 
          {
              ajaxindicatorstop();
              if(response.data.status == "success") {
                 angular.element('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                 angular.element('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
                 
                 /* resetForm */
                  $scope.user = {};
                  $scope.requirmentForm.$setPristine();
                  $scope.requirmentForm.$setUntouched();

                    /* for image reload */
                     angular.element("#img_profile").attr('src', site_url+'front-asset/images/gallrey.png');
                    /* for image reload */  
                 /* end resetForm */  

                 if(response.data.redirect == "yes"){
                  setTimeout(function(){
                    location.href=site_url+'buyer/requirment_detail/'+requirement_id;
                  },1500);
                 }

              } else if(response.data.status == "error") {

                 $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                 $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
              }
          },function (response) {
            ajaxindicatorstop();
            if (response.status > 0)
                $scope.errorMsg = response.status + ': ' + response.data;
          }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          });   
      };

    });// end PostRequirment



  /*--------------------------------------------------------------------
  | CONTACT SELLER
  ---------------------------------------------------------------------*/

      app.controller('ContactSeller', function ($scope, $http,$httpParamSerializerJQLike) {
      $scope.user                      = {};
      $scope.contact_to_seller         = contact_to_seller;

      angular.element('.sellid').on('click',function()
      {
         $scope.user.seller_id = $(this).data('sellid');
         angular.element('#seller_id').val($(this).data('sellid'));
      });

      function contact_to_seller() 
      {
          ajaxindicatorstart();
          $http({
               method   : 'POST',
               url      : site_url+"buyer/contact_to_seller",
               data     : $httpParamSerializerJQLike($scope.user),
               headers  : {
                           'Content-Type': 'application/x-www-form-urlencoded'
                        }
          }).then(function successCallback(response) 
          { 
              ajaxindicatorstop();
              if(response.data.status == "success") {
                    $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');

                  /* resetForm */
                  $scope.user = {};
                  $scope.contacttoseller.$setPristine();
                  $scope.contacttoseller.$setUntouched();
                  /* end resetForm */

                  setTimeout(function(){
                  angular.element('.close').click();
                  },2000); 
 

              } else {
                    $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
              }
          }, function errorCallback(response) {
              ajaxindicatorstop();
              $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
              $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
          }); // end errorCallback
      };  // end processResetPass

    });// end OfferToRequirmentCntrl  

  /*--------------------------------------------------------------------
  | CONTACT SELLER
  ---------------------------------------------------------------------*/


  /*--------------------------------------------------------------------
  | START ADD TO CONTACT FOR BUYER
  ---------------------------------------------------------------------*/

      app.controller('AddtoContact', function ($scope, $http,$httpParamSerializerJQLike) {
      $scope.user                      = {};
      $scope.add_to_contact            = add_to_contact;

      angular.element('.seller_id').on('click',function()
      {
         $scope.user.seller_id = $(this).data('seller_id');
         angular.element('#seller_id').val($(this).data('seller_id'));
      });

      function add_to_contact() 
      {
          ajaxindicatorstart();
          $http({
               method   : 'POST',
               url      : site_url+"buyer/add_to_contact",
               data     : $httpParamSerializerJQLike($scope.user),
               headers  : {
                           'Content-Type': 'application/x-www-form-urlencoded'
                        }
          }).then(function successCallback(response) 
          { 
              ajaxindicatorstop();
              if(response.data.contact_status == "success") {
                    $('#status_msg1').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.contact_msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');

                  /* resetForm */
                  $scope.user = {};
                  $scope.addtocontact.$setPristine();
                  $scope.addtocontact.$setUntouched();
                  /* end resetForm */

                  setTimeout(function(){
                  angular.element('.close').click();
                  },2000); 
 

              } else {
                    $('#status_msg1').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.contact_msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');
              }
          }, function errorCallback(response) {
              ajaxindicatorstop();
              $('#status_msg1').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.contact_msg+' </div>');
              $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');
          }); // end errorCallback
      };  // end processResetPass

    });// end OfferToRequirmentCntrl  

  /*--------------------------------------------------------------------
  | END ADD TO CONTACT FOR BUYER
  ---------------------------------------------------------------------*/

    /*--------------------------------------------------------------------
  | START ADD TO CONTACT FOR SELLER
  ---------------------------------------------------------------------*/

      app.controller('AddtoContactSeller', function ($scope, $http,$httpParamSerializerJQLike) {
      $scope.user                      = {};
      $scope.add_to_contact_seller     = add_to_contact_seller;

      angular.element('.buyer_id').on('click',function()
      {
         $scope.user.buyer_id = $(this).data('buyer_id');
         angular.element('#buyer_id').val($(this).data('buyer_id'));
      });

      function add_to_contact_seller() 
      {
          ajaxindicatorstart();
          $http({
               method   : 'POST',
               url      : site_url+"seller/add_to_contact",
               data     : $httpParamSerializerJQLike($scope.user),
               headers  : {
                           'Content-Type': 'application/x-www-form-urlencoded'
                        }
          }).then(function successCallback(response) 
          { 
              ajaxindicatorstop();
              if(response.data.contact_status == "success") {
                    $('#status_msg1').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.contact_msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');

                  /* resetForm */
                  $scope.user = {};
                  $scope.addtocontactseller.$setPristine();
                  $scope.addtocontactseller.$setUntouched();
                  /* end resetForm */

                  setTimeout(function(){
                  angular.element('.close').click();
                  },2000); 
 

              } else {
                    $('#status_msg1').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.contact_msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');
              }
          }, function errorCallback(response) {
              ajaxindicatorstop();
              $('#status_msg1').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.contact_msg+' </div>');
              $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');
          }); // end errorCallback
      };  // end processResetPass

    });// end OfferToRequirmentCntrl  

  /*--------------------------------------------------------------------
  | END ADD TO CONTACT FOR SELLER
  ---------------------------------------------------------------------*/


/*--------------------------------------------------------------------
| END BUYER SECTION
---------------------------------------------------------------------*/

/*--------------------------------------------------------------------
| MARKETPLACE
---------------------------------------------------------------------*/


  /*--------------------------------------------------------------------
  | SELLER MAKE OFFER TO REQUIRMENT
  ---------------------------------------------------------------------*/

      app.controller('OfferToRequirmentCntrl', function ($scope, $http,$httpParamSerializerJQLike) {
      $scope.user                      = {};
      $scope.requirment_offer          = requirment_offer;

      angular.element('.let_require').click(function()
      {
       
         $scope.user.requirment_id = $(this).data('reqid');
         angular.element('#requirment_id').val($(this).data('reqid'));
         angular.element('#buyername').html($(this).data('buyername'));
         angular.element('#requirment').html($(this).data('requirment'));
         angular.element('#sel_category').html($(this).data('category'));
         angular.element('#price').html($(this).data('price'));
      });

      function requirment_offer() 
      {
          ajaxindicatorstart();
          $http({
               method   : 'POST',
               url      : site_url+"Marketplace/make_offer",
               data     : $httpParamSerializerJQLike($scope.user),
               headers  : {
                           'Content-Type': 'application/x-www-form-urlencoded'
                        }
          }).then(function successCallback(response) 
          { 
              ajaxindicatorstop();
              if(response.data.status == "success") {
                    $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');

                  /* resetForm */
                  $scope.user = {};
                  $scope.makeofferForm.$setPristine();
                  $scope.makeofferForm.$setUntouched();
                  /* end resetForm */

                  setTimeout(function(){
                  angular.element('.close').click();
                  },2000); 
 

              } else {
                    $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
              }
          }, function errorCallback(response) {
              ajaxindicatorstop();
              $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
              $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
          }); // end errorCallback
      };  // end processResetPass

    });// end OfferToRequirmentCntrl  

  /*--------------------------------------------------------------------
  | SELLER MAKE OFFER TO REQUIRMENT
  ---------------------------------------------------------------------*/

  /*--------------------------------------------------------------------
  | BUYER MAKE OFFER TO REQUIRMENT
  ---------------------------------------------------------------------*/

      app.controller('OfferToOfferCntrl', function ($scope, $http,$httpParamSerializerJQLike) {
      $scope.user              = {};
      $scope.give_offer        = give_offer;

      angular.element('.off_req').on('click',function()
      {
         $scope.user.offer_id = $(this).data('offreq');
         angular.element('#offer_id').val($(this).data('offreq'));

         angular.element('#sellername').html($(this).data('sellername'));
         angular.element('#offer').html($(this).data('offer'));
         angular.element('#sel_category').html($(this).data('category'));
         angular.element('#qty').html($(this).data('qty'));
      });

      function give_offer() 
      {
          ajaxindicatorstart();
          $http({
               method   : 'POST',
               url      : site_url+"Marketplace/make_offer_to_sellersoffer",
               data     : $httpParamSerializerJQLike($scope.user),
               headers  : {
                           'Content-Type': 'application/x-www-form-urlencoded'
                        }
          }).then(function successCallback(response) 
          { 
              ajaxindicatorstop();
              if(response.data.status == "success") {
                    $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');

                  /* resetForm */
                  $scope.user = {};
                  $scope.giveofferForm.$setPristine();
                  $scope.giveofferForm.$setUntouched();
                  /* end resetForm */

                  setTimeout(function(){
                  angular.element('.close').click();
                  },2000); 
 

              } else {
                    $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
              }
          }, function errorCallback(response) {
              ajaxindicatorstop();
              $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
              $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
          }); // end errorCallback
      };  // end processResetPass

    });// end OfferToRequirmentCntrl  

  /*--------------------------------------------------------------------
  | BUYER MAKE OFFER TO REQUIRMENT
  ---------------------------------------------------------------------*/


/*--------------------------------------------------------------------
| END MARKETPLACE
---------------------------------------------------------------------*/

/*--------------------------------------------------------------------
| MEMBERSHIP
---------------------------------------------------------------------*/


  /*--------------------------------------------------------------------
  | SELLER MAKE REQUIEST TO ADMIN FOR UPLOAD PRODUCT
  ---------------------------------------------------------------------*/

      app.controller('ProductUploadRequest', function ($scope, $http,$httpParamSerializerJQLike) {
      $scope.user                      = {};
      $scope.product_requiest          = product_requiest;
 
      function product_requiest() 
      {
          ajaxindicatorstart();
          $http({
               method   : 'POST',
               url      : site_url+"purchase/make_request_to_upload_product",
               data     : $httpParamSerializerJQLike($scope.user),
               headers  : {
                           'Content-Type': 'application/x-www-form-urlencoded'
                        }
          }).then(function successCallback(response) 
          { 
              ajaxindicatorstop();
              if(response.data.status == "success") {
                    $('#status_msg1').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');

                  /* resetForm */
                  $scope.user = {};
                  $scope.makeProductRequestForm.$setPristine();
                  $scope.makeProductRequestForm.$setUntouched();
                  /* end resetForm */
                  setTimeout(function(){
                  angular.element('.close').click();
                  },4000); 
              } else {
                    $('#status_msg1').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');
              }
          }, function errorCallback(response) {
              ajaxindicatorstop();
              $('#status_msg1').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
              $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');
          }); // end errorCallback
      };  // end processResetPass

    });// end OfferToRequirmentCntrl  

  /*--------------------------------------------------------------------
  | SELLER SELLER MAKE REQUIEST TO ADMIN FOR UPLOAD PRODUCT
  ---------------------------------------------------------------------*/

  /*--------------------------------------------------------------------
  | SELLER MAKE REQUIEST TO ADMIN FOR UPLOAD PRODUCT
  ---------------------------------------------------------------------*/

      app.controller('MakeOfferRequest', function ($scope, $http,$httpParamSerializerJQLike) {
      $scope.user                      = {};
      $scope.make_offer                = make_offer;
 
      function make_offer() 
      {
          ajaxindicatorstart();
          $http({
               method   : 'POST',
               url      : site_url+"purchase/make_request_to_make_offers",
               data     : $httpParamSerializerJQLike($scope.user),
               headers  : {
                           'Content-Type': 'application/x-www-form-urlencoded'
                        }
          }).then(function successCallback(response) 
          { 
              ajaxindicatorstop();
              if(response.data.status == "success") {
                    $('#status_msg1').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');

                  /* resetForm */
                  $scope.user = {};
                  $scope.makeOfferRequestForm.$setPristine();
                  $scope.makeOfferRequestForm.$setUntouched();
                  /* end resetForm */
                  setTimeout(function(){
                  angular.element('.close').click();
                  },4000); 
              } else {
                    $('#status_msg1').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');
              }
          }, function errorCallback(response) {
              ajaxindicatorstop();
              $('#status_msg1').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
              $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');
          }); // end errorCallback
      };  // end processResetPass

    });// end OfferToRequirmentCntrl  

  /*--------------------------------------------------------------------
  | SELLER SELLER MAKE REQUIEST TO ADMIN FOR UPLOAD PRODUCT
  ---------------------------------------------------------------------*/


  /*--------------------------------------------------------------------
  | BUYER MAKE REQUIEST TO ADMIN FOR POST REQUIREMENT
  ---------------------------------------------------------------------*/

      app.controller('RequirementPostRequest', function ($scope, $http,$httpParamSerializerJQLike) {
      $scope.user                      = {};
      $scope.requirement_requiest      = requirement_requiest;
 
      function requirement_requiest() 
      {
          ajaxindicatorstart();
          $http({
               method   : 'POST',
               url      : site_url+"purchase/make_request_to_post_requirements",
               data     : $httpParamSerializerJQLike($scope.user),
               headers  : {
                           'Content-Type': 'application/x-www-form-urlencoded'
                        }
          }).then(function successCallback(response) 
          { 
              ajaxindicatorstop();
              if(response.data.status == "success") {
                    $('#status_msg1').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');

                  /* resetForm */
                  $scope.user = {};
                  $scope.makeRequirementPostForm.$setPristine();
                  $scope.makeRequirementPostForm.$setUntouched();
                  /* end resetForm */
                  setTimeout(function(){
                  angular.element('.close').click();
                  },4000); 
              } else {
                    $('#status_msg1').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');
              }
          }, function errorCallback(response) {
              ajaxindicatorstop();
              $('#status_msg1').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
              $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');
          }); // end errorCallback
      };  // end processResetPass

    });// end OfferToRequirmentCntrl  

  /*--------------------------------------------------------------------
  | END BUYER MAKE REQUIEST TO ADMIN FOR POST REQUIREMENT
  ---------------------------------------------------------------------*/


  /*--------------------------------------------------------------------
  | BUYER MAKE REQUIEST TO ADMIN FOR SEND OFFERS TO SELLRS
  ---------------------------------------------------------------------*/

      app.controller('MakeOfferRequestForSeller', function ($scope, $http,$httpParamSerializerJQLike) {
      $scope.user                      = {};
      $scope.make_offers_to_seller     = make_offers_to_seller;
 
      function make_offers_to_seller() 
      {
          ajaxindicatorstart();
          $http({
               method   : 'POST',
               url      : site_url+"purchase/admin_request_to_make_offers_for_sellers",
               data     : $httpParamSerializerJQLike($scope.user),
               headers  : {
                           'Content-Type': 'application/x-www-form-urlencoded'
                        }
          }).then(function successCallback(response) 
          { 
              ajaxindicatorstop();
              if(response.data.status == "success") {
                    $('#status_msg1').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');

                  /* resetForm */
                  $scope.user = {};
                  $scope.makeOfferRequestToSellerForm.$setPristine();
                  $scope.makeOfferRequestToSellerForm.$setUntouched();
                  /* end resetForm */
                  setTimeout(function(){
                  angular.element('.close').click();
                  },4000); 
              } else {
                    $('#status_msg1').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                    $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');
              }
          }, function errorCallback(response) {
              ajaxindicatorstop();
              $('#status_msg1').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
              $('html, body').animate({scrollTop:$('#status_msg1').position().top}, 'slow');
          }); // end errorCallback
      };  // end processResetPass

    });// end OfferToRequirmentCntrl  

  /*--------------------------------------------------------------------
  | END BUYER MAKE REQUIEST TO ADMIN FOR SEND OFFERS TO SELLRS
  ---------------------------------------------------------------------*/



/*--------------------------------------------------------------------
| END MEMBERSHIP
---------------------------------------------------------------------*/


/*--------------------------------------------------------------------
| CONTACT US
---------------------------------------------------------------------*/

    app.controller('ContactUs', function ($scope, $http,$httpParamSerializerJQLike) {
    $scope.user          = {};
    $scope.contactus     = contactus;

    function contactus() 
    {
        ajaxindicatorstart();
        $http({
             method   : 'POST',
             url      : site_url+"contact_us/submit_enquiry",
             data     : $httpParamSerializerJQLike($scope.user),
             headers  : {
                         'Content-Type': 'application/x-www-form-urlencoded'
                      }
        }).then(function successCallback(response) 
        { 
            ajaxindicatorstop();
            if(response.data.status == "success") {
                  $('#status_msg').html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.data.msg+' </div>');
                  $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');

                /* resetForm */
                $scope.user = {};
                $scope.ContactUsForm.$setPristine();
                $scope.ContactUsForm.$setUntouched();
                /* end resetForm */
                setTimeout(function(){
                angular.element('.close').click();
                },2000); 
            } else {
                  $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
                  $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
            }
        }, function errorCallback(response) {
            ajaxindicatorstop();
            $('#status_msg').html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp;'+response.data.msg+' </div>');
            $('html, body').animate({scrollTop:$('#status_msg').position().top}, 'slow');
        }); // end errorCallback
    };  // end contactus

  });// end ContactUs  

/*--------------------------------------------------------------------
| END BUYER MAKE REQUIEST TO ADMIN FOR SEND OFFERS TO SELLRS
---------------------------------------------------------------------*/
