var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'long_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name',
        postal_code: 'short_name',
      };

      function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete((document.getElementById('autocomplete')),{types: []});/*'geocode'*/
        autocomplete.addListener('place_changed', fillInAddress);
      }
function key_exists(key, search) {
    if (!search || (search.constructor !== Array && search.constructor !== Object)) {
        return false;
    }
    for (var i = 0; i < search.length; i++) {
        if (search[i] === key) {
            return true;
        }
    }
    return key in search;
  }
    function fillInAddress()
    {
      var place = autocomplete.getPlace();
      var isValid = key_exists('place_id', place); /*true*/
      if(isValid == true)
      {
          $('#cityLat').val(place.geometry.location.lat());
          $('#cityLng').val(place.geometry.location.lng());


          for (var component in componentForm) 
          {
            $("#"+component).val("");
            $("#"+component).attr('readonly',true);
          }
          
          if(place.address_components.length > 0 )
          {
            $.each(place.address_components,function(index,elem)
            {
                var addressType = elem.types[0];
                if(componentForm[addressType])
                {
                  var val = elem[componentForm[addressType]];
                  $("#"+addressType).val(val) ;  
                }
            });
          }
          initiateSearch(place.geometry.location.lat(),place.geometry.location.lng());
        }
        else
        {
          $('#autocomplete').val('');
        }
        return false;
      }

      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
/*
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });

            new google.maps.Geocoder().geocode(
            {'latLng': geolocation},
            function (res, status) {
                var zip = res[0].formatted_address.match(/,\s\w{2}\s(\d{5})/);
                $("#location").val(zip);          
            }
            );
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }*/
      