
//<!--country-language-js-start-here-->
function DropDown(el) {
    this.dd = el;
    this.placeholder = this.dd.children('span');
    this.opts = this.dd.find('ul.dropdown > li');
    this.val = '';
    this.index = -1;
    this.initEvents();
}
DropDown.prototype = {
    initEvents: function () {
        var obj = this;
        obj.dd.on('click', function (event) {
            $(this).toggleClass('active');
            return false;
        });
        obj.opts.on('click', function () {
            var opt = $(this);
            var obj_a = opt.find("a");
            var language_text = obj_a.text();
            $("#new_word").html(obj_a.html());
        });
    },
    getValue: function () {
        return this.val;
    },
    getIndex: function () {
        return this.index;
    }
}
$(function () {

    var dd = new DropDown($('#dd'));

    $(document).click(function () {
        // all dropdowns
        $('.wrapper-dropdown-1').removeClass('active');
    });

});

//<!--country-language-js-close-here-->

$(document).ready(function () {
    
    //<!--Sticky Menu-->
var stickyNavTop = $('.header, .page-head-name').offset().top;

    var stickyNav = function () {
        var scrollTop = $(window).scrollTop();

        if (scrollTop > stickyNavTop) {
            $('.header, .seller-tab-block').addClass('sticky');
        } else {
            $('.header, .seller-tab-block').removeClass('sticky');
        }
    };

    stickyNav();

    $(window).scroll(function () {
        stickyNav();
    });

//<!--Sticky Menu-->
    
    /*  Animation */
    wow = new WOW({
        animateClass: 'animated',
        offset: 100,
        callback: function (box) {
            console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
    });
    wow.init();
    if ($("#moar").length > 0) {
        document.getElementById('moar').onclick = function () {
            var section = document.createElement('section');
            section.className = 'section--purple wow fadeInDown';
            this.parentNode.insertBefore(section, this);
        };
    }


    // footer dropdown links start 
    var min_applicable_width = 991;

    $(document).ready(function () {
        applyResponsiveSlideUp($(this).width(), min_applicable_width);
    });

    function applyResponsiveSlideUp(current_width, min_applicable_width) {
        /* Set For Initial Screen */
        initResponsiveSlideUp(current_width, min_applicable_width);

        /* Listen Window Resize for further changes */
        $(window).bind('resize', function () {
            if ($(this).width() <= min_applicable_width) {
                unbindResponsiveSlideup();
                bindResponsiveSlideup();
            } else {
                unbindResponsiveSlideup();
            }
        });
    }

    function initResponsiveSlideUp(current_width, min_applicable_width) {
        if (current_width <= min_applicable_width) {
            unbindResponsiveSlideup();
            bindResponsiveSlideup();
        } else {
            unbindResponsiveSlideup();
        }
    }

    function bindResponsiveSlideup() {
        $(".menu_name").hide();
        $(".footer_heading").bind('click', function () {
            var $ans = $(this).parent().find(".menu_name");
            $ans.slideToggle();
            $(".menu_name").not($ans).slideUp();
            $('.menu_name').removeClass('active');

            $('.footer_heading').not($(this)).removeClass('active');
            $(this).toggleClass('active');
            $(this).parent().find(".menu_name").toggleClass('active');
        });
    }

    function unbindResponsiveSlideup() {
        $(".footer_heading").unbind('click');
        $(".menu_name").show();
    }
    // footer dropdown links end

    //<!-- Min Top Menu Start Here  -->

    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        $("body").css({
            "margin-left": "250px",
            "overflow-x": "hidden",
            "transition": "margin-left .5s",
            "position": "fixed"
        });
        $("#main").addClass("overlay");
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        $("body").css({
            "margin-left": "0px",
            "transition": "margin-left .5s",
            "position": "relative"
        });
        $("#main").removeClass("overlay");
    }
    //  Min Top Sub Menu Start Here  

    $(".min-menu > li > .drop-block").click(function () {
        if (false == $(this).next().hasClass('menu-active')) {
            $('.sub-menu > ul').removeClass('menu-active');
        }
        $(this).next().toggleClass('menu-active');

        return false;
    });

    // script for hide show of sub menu

    //     $('.main-content').click(function(){
    //         $(this).next('.sub-menu').slideToggle('1000');
    //         $(this).find('.arrow i').toggleClass('fa-angle-down fa-angle-up')
    //         });

    $("body").click(function () {
        $('.sub-menu > ul').removeClass('menu-active');
    });


    //  Min Top Sub Menu End Here

    //<!-- Min Top Menu Start End  -->


});