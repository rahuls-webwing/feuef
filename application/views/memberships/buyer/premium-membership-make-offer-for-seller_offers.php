<style>.footer-col-block {display: none;}.copyright-block {margin-top: 0;}</style>
<div class="middle-login-content">
    <div class="container">
        <div class="login-main-block" >
            <div class="col-sm-6 col-md-6 col-lg-6 padd-l-no">
                <div class="login-left-block">
                    <div class="login-overlay"></div>
                    <div class="main-block-login">
                        <img src="<?php echo base_url(); ?>front-asset/images/logo-login.png" alt="" />
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6 padd-l-no purchase_div"  >
                <?php  $this->load->view('status-msg'); ?>
                <div style="margin: 0;" class="prising-block">
                    <a href="#">
                        <span class="main-title "><?php if(isset($get_pay_amt[0]['pricing_name'])) { echo 'GET '.$get_pay_amt[0]['pricing_name'].' MEMBERSHIP FOR MAKE OFFER'; } else { echo "Not Available"; } ?></span>
                        <span class="price-colm free"><sup><?php echo CURRENCY; ?></sup><?php if(isset($get_pay_amt[0]['membership_price'])) { echo $get_pay_amt[0]['membership_price']; } else { echo "Not Available"; } ?></span>
                        <span class="colum"><?php if(isset($get_pay_amt[0]['upload_qty'])) { echo 'Get More '.$get_pay_amt[0]['upload_qty'].' Make Offer Request From Live Market'; } else { echo "Not Available"; } ?></span>
                        <!-- <span class="colum bck">3 Proposals</span>
                        <span class="colum">Profile Edit</span>
                        <span class="colum bck">-</span>
                        <span class="colum ">-</span>
                        <span class="colum bck">-</span>
                        <span class="colum ">-</span> -->
                        <span class="register-btn">
     
                            <?php if(count($get_request_is_accept) > 0 && $get_request_is_accept[0]['is_accepet']=="yes" ){ ?>
                            <button class="register-now" type="button" id="purchase">Purchase</button>
                            <?php } else if(count($get_last_payment_history) == '0'){ ?>
                            <button class="register-now" type="button" id="purchase">Purchase</button>
                            <?php } else { ?>
                            <button class="register-now" data-toggle="modal" data-backdrop="static" data-target="#myReqModal"  type="button">Send Request</button>
                            <?php } ?>
                        </span>
                    </a>
                </div>
            </div>

            <!-- start payment  -->
           
            <div id="start_payment"  style="display:none;">
              <form id="frm_payment" name="frm_payment" enctype="multipart/form-data" method="post" action="<?php echo base_url();?>purchase/market_offers">
                <div class="col-sm-6 col-md-6 col-lg-6 p-r padd-l-no">
                    <div class="side-menu payment-tabing-menu">
                        <ul>                                
                            <li class="active-pay" id="li_monthly-free1">
                                <a class="act common" id="monthly-free1" data-target="credit">
                                    <span><i class="fa fa-credit-card" aria-hidden="true"></i></span> <span class="tab-label"> Credit/Debit Card</span>
                                </a>
                            </li>
                            <li class="last-border" id="li_monthly-free11">
                                <a class="common" id="monthly-free11" data-target="paypal">
                                    <span><i class="fa fa-paypal" aria-hidden="true"></i></span> <span class="tab-label">Pay Pal</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-6 col-md-6 col-lg-6 pp-l padd-l-no">
                    <div class="payment_card">
                        <div class="payment-credit-card paymt-sec" id="credit">
                            <div class="top_card_icon">
                                <div class="card_title">Pay using Credit Card</div>
                                <div class="card_icon_img">
                                    <a href="javascript:void(0);"><img alt="" src="<?php echo base_url();?>front-asset/images/pay_pal.png" /></a>
                                    <a href="javascript:void(0);"><img alt="" src="<?php echo base_url();?>front-asset/images/master_card.png" /></a>
                                    <a href="javascript:void(0);"><img alt="" src="<?php echo base_url();?>front-asset/images/visa.png" /></a>
                                </div>
                            </div>
                            <!-- <form id="frm_payment" name="frm_payment" method="post" action="<?php echo base_url();?>payment/paynow/"> -->
                            <div class="payment-form">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-10">
                                        <div class="row">

                                            <div class="col-sm-12 col-md-12 col-lg-12">
                                                <div class="user_box">
                                                    <div class="input-bx-b">
                                                        <input type="text"
                                                               class="tra-input" 
                                                               id="card_type" 
                                                               name="card_type" 
                                                               placeholder="Credit/Debit Card" 
                                                               readonly/>

                                                        <div class="card_icon_img" id="card_type-img">
                                                            <a href="javascript:void(0);" id="visa"><img alt="" src="<?php echo base_url();?>front-asset/images/pay-visa.png" /></a>
                                                            <a href="javascript:void(0);" id="mastercard"><img alt="" src="<?php echo base_url();?>front-asset/images/pay-mstercard.png" /></a>
                                                        </div>
                                                    </div>
                                                    <div  class="error_msg"  id="error_msg_card_type"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12">
                                                <div class="user_box-imf">
                                                    <input type="text" 
                                                           class="input-bx-f allowOnlySixteen" 
                                                           id="cc_number" 
                                                           name="cc_number" 
                                                           placeholder="Card Number" />
                                                    <div class="img-odiv" ><img src="<?php echo base_url();?>front-asset/images/cart-nubs.png" alt="" /></div>
                                                    <div id="error_msg_cc_number" class="error_msg"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12">   <div class="user_box"><div class="label_text tnf">Expiry Date</div></div></div>
                                            <div class="col-sm-12 col-md-6 col-lg-5">
                                                <div class="user_box">
                                                    <input type="text" 
                                                           class="input-bx-b exp" 
                                                           id="cc_exp" 
                                                           name="cc_exp" 
                                                           placeholder="MM/YYYY" />
                                                    <div class="error_msg" id="error_msg_cc_exp"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-6">
                                                <div class="user_box">
                                                    <input class="input-bx-b input-card " 
                                                           id="cc_cvc" 
                                                           name="cc_cvc" 
                                                           type="text" 
                                                           placeholder=" CVV " /><span></span>
                                                    <div class=" error_msg" id="error_msg_cc_cvc"></div>
                                                </div>
                                            </div>
                                            <div class="clr"></div>
                                            <div class="col-lg-12">
                                                <div class="btn-apymt">
                                                    <button class="change-btn-pass" id="btn_cc_pay" name="btn_cc_pay" type="submit">Pay <?php echo $get_pay_amt[0]['membership_price'].' '.currencyCode; ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="payment-paypal paymt-sec payment-form" id="paypal">
                          <div class="row">
                                 <div class="col-lg-12">
                                <div class="top_card_icon">
                                    <div class="card_title">Pay using Paypal</div>
                                    <div class="card_icon_img">
                                        <a href="#"><img alt="" src="<?php echo base_url();?>front-asset/images/pay_pal.png" /></a>
                                    </div>
                                </div>
                                <div class="pay-tril"><span>Click to connect to paypal</span>(You won't be charged until the order is placed.) </div>
                                <div class="clr"></div>
                                <div class="pay-tril">By clicking Continue to PayPal, you authorise TaskersHub to charge your PayPal account for the full amount of all fees related to your buying activity on TaskersHub.</span></div>

                                <div class="btn-apymt">
                                    <button class="change-btn-pass" type="submit" id="btn_pay" name="btn_pay">Pay <?php echo $get_pay_amt[0]['membership_price'].' '.currencyCode; ?></button>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="clr"></div>
                    </div>
                  </div>
                </form>  
               </div>  
             <!-- end  payment-->
            <div class="clr"></div>
        </div>
    </div>
</div>


<!-- requiest Modal -->
<div id="myReqModal" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">Send Request</h4>
        </div>
        <div class="modal-body">
        <?php $this->load->view('status-msg1'); ?>
        <div ng-controller="MakeOfferRequestForSeller">  
            <form  
                id="makeOfferRequestToSellerForm"
                class=""
                name="makeOfferRequestToSellerForm" 
                enctype="multipart/form-data"
                novalidate 
                ng-submit="makeOfferRequestToSellerForm.$valid && make_offers_to_seller();"  >

                    <div class="mobile-nu-block input-first box-msg" ng-class="{'has-error':makeOfferRequestToSellerForm.description.$touched && makeOfferRequestToSellerForm.description.$invalid}">
                        <textarea rows="" 
                                      cols="" 
                                      name="description" 
                                      class="beginningSpace_restrict" 
                                      ng-model="user.description"
                                      ng-required="true"
                                      ng-maxlength="500"
                                      placeholder="Enter request description"></textarea>

                        <span class="highlight"></span> 
                        <label>Message</label>

                        <div class="error-new-block" ng-messages="makeOfferRequestToSellerForm.description.$error" ng-if="makeOfferRequestToSellerForm.$submitted || makeOfferRequestToSellerForm.description.$touched">
                        <div>
                        <div class="err_msg_div" style="display:none;">
                            <p ng-message="required"    class="error">  This field is required</p>
                            <p ng-message="maxlength"   class="error">  This field to long. Only 500 characters allow.</p>
                            </div>
                        </div>
                        <script type="text/javascript">
                                $(document).ready(function(){
                                  setTimeout(function(){
                                    $('.err_msg_div').removeAttr('style');
                                  },200);
                                });
                            </script>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-password changes enq">
                        <button class="change-btn-pass " type="submit">Send</button>
                    </div>
                </div>
            </form>    
        </div>

    </div>
</div>
</div>
<!-- End requiest Modal -->


<script type="text/javascript">
    $(document).ready(function() { 
      $('.close').on('click',function() {
        setTimeout(function(){
            location.reload();
        })
      });
    });
</script>



<script type="text/javascript">
  $(document).ready(function() {
    $('#purchase').click(function(){
        $('#start_payment').show();
        $('.purchase_div').hide();
        $('#cc_number').focus();
    });
  });
</script>
<!-- Payment Method Validations jQuery -->
<script type="application/javascript">
    //onclick toggle
    $(document).ready(function() {
        $('.container1-b').hide();
        $(".container1-b:first").addClass("act1").show();
        $('.regi_toggle .billing_cycle').click(function() {
            $('.billing_cycle').removeClass('act'); //remove the class from the button
            $(this).addClass('act'); //add the class to currently clicked button
            var target = "#" + $(this).data("target");
            $(".container1-b").not(target).hide();
            $(target).show();
            target.slideToggle();
        });

        $(".billing_cycle").click(function(event) {
            event.stopPropagation();
            /*alert("The span element was clicked.");*/
        });
        $('.paymt-sec').hide();
        $(".paymt-sec:first").addClass("ac1").show();
        $('.side-menu .common').click(function() {
          //alert();

           var curr_id = $(this).attr('id');
            $('.common').removeClass('act2'); //remove the class from the button
            $(this).addClass('act2'); //add the class to currently clicked button
            var target = "#" + $(this).data("target");
            console.log(target);
            $(".paymt-sec").not(target).hide();
            $(target).show();
            //target.slideToggle();
          if(curr_id=='monthly-free1')
          {
            $('#li_monthly-free1').addClass('active-pay');
            $('#li_monthly-free1').addClass('last-border');
            $('#li_monthly-free11').removeClass('active-pay');
            $('#li_monthly-free11').removeClass('last-border');
          }
         if(curr_id=='monthly-free11')
          {
            $('#li_monthly-free11').addClass('active-pay');
            $('#li_monthly-free11').addClass('last-border');
            $('#li_monthly-free1').removeClass('active-pay');
            $('#li_monthly-free1').removeClass('last-border');
          }
        });
       /* --------------T.A--------------------------------*/ 
        $('#visa').click(function(){
            $('#card_type').val('Visa');
        });
        $('#mastercard').click(function(){
            $('#card_type').val('Mastercard');
        });
        $('#amazon').click(function(){
            $('#card_type').val('Amazon');
        });
        $('#discover').click(function(){
            $('#card_type').val('Discover');
        });
        $('#btn_cc_pay').click(function(){


            var card_type      = $('#card_type').val();
            var Acc_no         = $('#cc_number').val();
            var Expirydate     = $('#cc_exp').val();
            var cvv            = $('#cc_cvc').val();
            var flag=1;
            if(card_type=="")
            {
              $('#error_msg_card_type').show();
              $('#error_msg_card_type').html('Please select card type');
              $('#card_type').focus();
               $('#card_type-img').on('click',function()
               {

                  $('#error_msg_card_type').hide();
               });

               flag=0;
            }
            if(Acc_no=="")
            {
              $('#error_msg_cc_number').show();
              $('#error_msg_cc_number').html('Please enter card no');
              $('#cc_number').focus();
               $('#cc_number').on('keyup',function()
               {
                 //$('#error_msg_cc_number').hide();
               });

               flag=0;
            }
            if(Expirydate=="")
            {
              $('#error_msg_cc_exp').show();
              $('#error_msg_cc_exp').html('Please enter expiry date');
              $('#cc_exp').focus();
               $('#cc_exp').on('click',function()
               {
                  $('#error_msg_cc_exp').hide();
               });

               flag=0;
            }
            if(Expirydate!="")
            {
                 var txtVal      = $('#cc_exp').val();
                 var filter      = new RegExp("(0[123456789]|10|11|12)([/])([1-2][0-9][0-9][0-9])");

                 var lastFive    = txtVal.substr(txtVal.length - 4);
                 var currentYear = new Date().getFullYear();

                 var firstTwo    = txtVal.substring(0, 2);
                 var currentDay = new Date();
     
                 var d = new Date();
                 var currentDay = d.getMonth(); 
     
             
                 if(!filter.test(txtVal) || lastFive <  currentYear || firstTwo < currentDay || lastFive ==  currentYear && firstTwo < currentDay)
                 {   
                    $('#error_msg_cc_exp').show();
                    $('#error_msg_cc_exp').html('Invalid Date!!!');
                    $('#cc_exp').focus();
                     $('#cc_exp').on('click',function()
                     {

                        $('#error_msg_cc_exp').hide();
                     });

                     flag=0;
                 }
            }
            if(cvv=="")
            {
              $('#error_msg_cc_cvc').show();
              $('#error_msg_cc_cvc').html('Please enter cvv');
              $('#cc_cvc').focus();
               $('#cc_cvc').on('keyup',function()
               {
                  //$('#error_msg_cc_cvc').hide();
               });

               flag=0;
            }
            /* credit card expiry date validation (mm/yyyy) --------------T.A*/
             $('#cc_exp').blur(function() {
                 var txtVal      = $('#cc_exp').val();
                 var filter      = new RegExp("(0[123456789]|10|11|12)([/])([1-2][0-9][0-9][0-9])");

                 var lastFive    = txtVal.substr(txtVal.length - 4);
                 var currentYear = new Date().getFullYear();

                 var firstTwo    = txtVal.substring(0, 2);
                 var currentDay = new Date();
     
                 var d = new Date();
                 var currentDay = d.getMonth(); 
     
                 flag=1;
                 if(!filter.test(txtVal) || lastFive <  currentYear || firstTwo < currentDay || lastFive ==  currentYear && firstTwo < currentDay)
                 {   
                    $('#error_msg_cc_exp').show();
                    $('#error_msg_cc_exp').html('Invalid Date!!!');
                    $('#cc_exp').focus();
                    $('#cc_exp').on('click',function()
                    {

                      $('#error_msg_cc_exp').hide();
                    });
                    flag=0;
                 }
             });
            /* end credit card expiry date ------------------------------T.A*/
            if(flag==1){
              return true;
            }
            else 
            {
              return false;
            }
        }); 
        /* character restriction */
        $("#cc_number").bind("keydown", function (event) {
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 17 || event.keyCode == 86 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 190 ||
                 // Allow: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
            } else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    $('#error_msg_cc_number').show();
                    $('#error_msg_cc_number').html('Please enter only numbers');
                    event.preventDefault();
                }
                else
                {
                    $('#error_msg_cc_number').hide();
                }
            }
        });
      $("#cc_cvc").bind("keydown", function (event) {
          if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 190 ||
                   // Allow: Ctrl+A
                  (event.keyCode == 65 && event.ctrlKey === true) ||

                   // Allow: home, end, left, right
                  (event.keyCode >= 35 && event.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
              } else {
                   // Ensure that it is a number and stop the keypress
                  if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                      $('#error_msg_cc_cvc').show();
                      $('#error_msg_cc_cvc').html('Please enter only numbers');
                      event.preventDefault();
                  }
                  else
                  {
                      $('#error_msg_cc_cvc').hide();
                  }
              }
      });
    /* end character restriction --------------T.A*/
}); // end document ready
</script>