<div class="col-sm-3 col-md-3 col-lg-3">
    <div class="footer_heading2 my-account-block">
        My Account
    </div>
    <div class="menu_name2 left-bar-block">
        <ul>
            <li><a data-hover="-&nbsp;Dashboard" href="<?php echo base_url().'buyer/dashboard';?>">-&nbsp;Dashboard</a></li>
            <li><a data-hover="-&nbsp;Edit&nbsp;Profile" href="<?php echo base_url().'buyer/edit';?>">-&nbsp;Edit&nbsp;Profile</a></li>
            <li><a data-hover="-&nbsp;Change&nbsp;Password" href="<?php echo base_url().'buyer/change_password';?>">-&nbsp;Change&nbsp;Password</a></li>
            <li><a data-hover="-&nbsp;Post&nbsp;Requirement&nbsp;(Live&nbsp;Market)" href="<?php echo base_url().'buyer/post_requirment';?>">-&nbsp;Post&nbsp;Requirement&nbsp;(Live&nbsp;Market)</a></li>

            <li >
			<?php 
			/*$this->db->where('tbl_buyer_post_requirement.buyer_id' , $this->session->userdata('user_id'));
			$this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
			$this->db->where('tbl_buyer_post_requirement.status <>' , 'Delete');
			$this->db->where('tbl_buyer_post_requirement.requirment_status' , 'open');
			$this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_buyer_post_requirement.subcategory_id');
			$this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
			$getRequirments  = $this->master_model->getRecordCount('tbl_buyer_post_requirement');
			if($getRequirments <= 9){ $requirement_cnt = '0'.$getRequirments; } else if($getRequirments >= 10) { $requirement_cnt = '10+'; }*/
			$getRequirments = 0;
			?>
            <a data-hover="-&nbsp;Posted&nbsp;requirement" href="<?php echo base_url().'buyer/requirments';?>"
              <?php if($this->router->fetch_class()=='buyer' && $this->router->fetch_method()=='requirments' )
              { echo 'class="acti"'; }?> >-&nbsp;Posted&nbsp;requirement <?php if($getRequirments > 0) { echo '<span class="count-block-new">'.$requirement_cnt.'</span>';  } ?></a></li>

            <li <?php if($this->router->fetch_class()=='buyer' &&  $this->router->fetch_method()=='requirment_detail')
              { echo 'style="display:block;"'; } else {  echo 'style="display:none;"'; }?> >
            <a  data-hover="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Requirement&nbsp;Detail" href="#" class="acti" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Requirement&nbsp;Detail</a></li>  


            <li <?php if($this->router->fetch_class()=='buyer' &&  $this->router->fetch_method()=='edit_requirement')
              { echo 'style="display:block;"'; } else {  echo 'style="display:none;"'; }?> >
            <a  data-hover="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Requirement&nbsp;edit" href="#" class="acti" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Requirement&nbsp;edit</a></li> 

            <li >
            <?php 
			/*$this->db->where('tbl_buyer_post_requirement.buyer_id' , $this->session->userdata('user_id'));
			$this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
			$this->db->where('tbl_buyer_post_requirement.status <>' , 'Delete');
			$this->db->where('tbl_buyer_post_requirement.requirment_status' , 'closed');
			$this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_buyer_post_requirement.subcategory_id');
			$this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
			$getaccRequirments  = $this->master_model->getRecordCount('tbl_buyer_post_requirement');
			if($getaccRequirments <= 9){ $accrequirement_cnt = '0'.$getaccRequirments; } else if($getaccRequirments >= 10) { $accrequirement_cnt = '10+'; }*/
			$getaccRequirments = 0;
			?>
            <a data-hover="-&nbsp;Closed&nbsp;requirement" href="<?php echo base_url().'buyer/closed_requirments';?>"
              <?php if($this->router->fetch_class()=='buyer' && $this->router->fetch_method()=='closed_requirments' )
              { echo 'class="acti"'; }?> >-&nbsp;Closed&nbsp;requirement <?php if($getaccRequirments > 0) { echo '<span class="count-block-new">'.$accrequirement_cnt.'</span>';  } ?></a></li>

            <li <?php if($this->router->fetch_class()=='buyer' &&  $this->router->fetch_method()=='closed_requirment_detail')
              { echo 'style="display:block;"'; } else {  echo 'style="display:none;"'; }?>>
            <a  data-hover="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Closed&nbsp;Requirement&nbsp;Detail" href="#" class="acti" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Closed&nbsp;Requirement&nbsp;Detail</a></li>

            <?php 
            $this->db->where('status' , 'Unblock');
            $this->db->where('is_read' , 'no');
            $this->db->where('tbl_buyer_notifications.buyer_id' , $this->session->userdata('user_id'));
            $get_noti_count  = $this->master_model->getRecordCount('tbl_buyer_notifications');
            if($get_noti_count <= 9){ $noti_cnt = '0'.$get_noti_count; } else if($get_noti_count >= 10) { $noti_cnt = '10+'; }
            ?>
            <li >
            <a data-hover="-&nbsp;Notifications" href="<?php echo base_url().'buyer/notifications';?>"
              <?php if($this->router->fetch_class()=='buyer' && $this->router->fetch_method()=='notifications')
              { echo 'class="acti"'; }?> >-&nbsp;Notifications <?php if($get_noti_count > 0) { echo '<span class="count-block-new">'.$noti_cnt.'</span>';  } ?></a></li>  

            <li >
            <a <?php if($this->router->fetch_class()=='buyer' && $this->router->fetch_method()=='notification_details')
              { echo 'style="display:block;"'; } else {  echo 'style="display:none;"'; }?> data-hover="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Notifications&nbsp;Details" href="#" class="acti" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Notifications&nbsp;Details</a></li>

            <li >

            <li >
            <a <?php if($this->router->fetch_class()=='buyer' && $this->router->fetch_method()=='sent_inquiry_detail')
              { echo 'style="display:block;"'; } else {  echo 'style="display:none;"'; }?> data-hover="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Sent&nbsp;Inquiry&nbsp;Details" href="#" class="acti" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Sent&nbsp;Inquiry&nbsp;Details</a></li>


            <li >
            <a data-hover="-&nbsp;Order&nbsp;forms" href="<?php echo base_url().'buyer/order_forms';?>"
              <?php if($this->router->fetch_class()=='buyer' && $this->router->fetch_method()=='order_forms')
              { echo 'class="acti"'; }?> >-&nbsp;Order&nbsp;forms</a></li> 



            <li > 	
			<?php 
			/*$this->db->where('tbl_buyer_primum_membership_for_requirements_purchase_history.buyer_id' , $this->session->userdata('user_id'));
			$reqPaymentHistory  = $this->master_model->getRecordCount('tbl_buyer_primum_membership_for_requirements_purchase_history');
			if($reqPaymentHistory <= 9){ $req_pay_cnt = '0'.$reqPaymentHistory; } else if($reqPaymentHistory >= 10) { $req_pay_cnt = '10+'; }*/
			$reqPaymentHistory = 0;
			?>
            <a data-hover="-&nbsp;Requirement&nbsp;Transactions" href="<?php echo base_url().'buyer/requirement_payment_history';?>"
              <?php if($this->router->fetch_class()=='buyer' && $this->router->fetch_method()=='requirement_payment_history')
              { echo 'class="acti"'; }?> >-&nbsp;Requirement&nbsp;Transactions <?php if($reqPaymentHistory > 0) { echo '<span class="count-block-new">'.$req_pay_cnt.'</span>';  } ?></a></li>    

            <li >
			<?php 
			/*$this->db->where('tbl_buyer_primum_membership_for_make_offer_purchase_history.buyer_id' , $this->session->userdata('user_id'));
			$offerPaymentHistory  = $this->master_model->getRecordCount('tbl_buyer_primum_membership_for_make_offer_purchase_history');
			if($offerPaymentHistory <= 9){ $off_pay_cnt = '0'.$offerPaymentHistory; } else if($offerPaymentHistory >= 10) { $off_pay_cnt = '10+'; }*/
			$offerPaymentHistory =0;
			?>
            <a data-hover="-&nbsp;Make&nbsp;Offer&nbsp;Transactions" href="<?php echo base_url().'buyer/make_offer_payment_history';?>"
              <?php if($this->router->fetch_class()=='buyer' && $this->router->fetch_method()=='make_offer_payment_history')
              { echo 'class="acti"'; }?> >-&nbsp;Make&nbsp;Offer&nbsp;Transactions <?php if($offerPaymentHistory > 0) { echo '<span class="count-block-new">'.$off_pay_cnt.'</span>';  } ?></a></li>      
        


            <li >
            <a data-hover="-&nbsp;Blog&nbsp;add" href="<?php echo base_url().'blogs/add';?>"
              <?php if($this->router->fetch_class()=='blogs' && $this->router->fetch_method()=='add')
              { echo 'class="acti"'; }?> >-&nbsp;Blog&nbsp;add</a></li>  

            <li >
            <a data-hover="-&nbsp;Blog&nbsp;Manage" href="<?php echo base_url().'blogs/manage';?>"
              <?php if($this->router->fetch_class()=='blogs' && $this->router->fetch_method()=='manage')
              { echo 'class="acti"'; }?> >-&nbsp;Blog&nbsp;Manage</a></li>  

            <li >
            <a <?php if($this->router->fetch_class()=='blogs' && $this->router->fetch_method()=='edit')
              { echo 'style="display:block;"'; } else {  echo 'style="display:none;"'; }?> data-hover="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Blog&nbsp;Edit" href="#" class="acti" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Blog&nbsp;Edit</a></li>

           <li >
            <a data-hover="-&nbsp;Contacts" href="<?php echo base_url().'buyer/contact_list';?>"
              <?php if($this->router->fetch_class()=='buyer' && $this->router->fetch_method()=='contact_list')
              { echo 'class="acti"'; }?> >-&nbsp;Contacts</a></li>



          <!--  <li >
            <a data-hover="-&nbsp;Product&nbsp;Order&nbsp;Send" href="<?php echo base_url().'buyer/product_order_send';?>"
              <?php if($this->router->fetch_class()=='buyer' && $this->router->fetch_method()=='product_order_send')
              { echo 'class="acti"'; }?> >-&nbsp;Product&nbsp;Order&nbsp;Send</a></li> -->

          

        </ul>
    </div>
</div>


<script type="text/javascript">
	//left-side bar active class script start here
	var pathname = window.location.pathname;
	var globle_last_param = pathname.substring(pathname.lastIndexOf('/') + 1);

	$('.left-bar-block').find('li > a').each(function(index) {

		var current_url = this.href;
		var current_last_param = current_url.substring(current_url.lastIndexOf('/') + 1);

		if (globle_last_param == current_last_param) {
			$(this).addClass('acti');
		} else {
			//$(this).removeClass('acti');
		}
	});
	//left-side bar active class script start here
</script>


<script type="text/javascript">
	$(document).ready(function() {				
		// footer dropdown links start 
		var min_applicable_width = 767;

		$(document).ready(function() {
			applyResponsiveSlideUp($(this).width(), min_applicable_width);
		});

		function applyResponsiveSlideUp(current_width, min_applicable_width) {
			/* Set For Initial Screen */
			initResponsiveSlideUp(current_width, min_applicable_width);

			/* Listen Window Resize for further changes */
			$(window).bind('resize', function() {
				if ($(this).width() <= min_applicable_width) {
					unbindResponsiveSlideup();
					bindResponsiveSlideup();
				} else {
					unbindResponsiveSlideup();
				}
			});
		}

		function initResponsiveSlideUp(current_width, min_applicable_width) {
			if (current_width <= min_applicable_width) {
				unbindResponsiveSlideup();
				bindResponsiveSlideup();
			} else {
				unbindResponsiveSlideup();
			}
		}

		function bindResponsiveSlideup() {
			$(".menu_name2").hide();
			$(".footer_heading2").bind('click', function() {
				var $ans = $(this).parent().find(".menu_name2");
				$ans.slideToggle();
				$(".menu_name2").not($ans).slideUp();
				$('.menu_name2').removeClass('active');

				$('.footer_heading2').not($(this)).removeClass('active');
				$(this).toggleClass('active');
				$(this).parent().find(".menu_name2").toggleClass('active');
			});
		}

		function unbindResponsiveSlideup() {
			$(".footer_heading2").unbind('click');
			$(".menu_name2").show();
		}
		// footer dropdown links end

	});

</script>