<?php $this->load->view('buyer/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3>Posted Requirement</h3>
        </div>
    </div>
</div>
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <?php $this->load->view('buyer/left-bar'); ?>
                <div class="col-sm-9 col-md-9 col-lg-9">
                    <?php $this->load->view('status-msg'); ?>
                    <div class="account-info-block">                        

                    <?php if(isset($getRequirments) && sizeof($getRequirments) > 0) {?>

                        <?php foreach ($getRequirments as $value) {
                            ?>
                                <div class="main-img-block">
                                    <div class="row">
                                        <div class="col-sm-4 col-md-4 col-lg-4">
                                            <div class="">

                                                <?php if(!empty($value['req_image']) && file_exists('images/buyer_post_requirment_image/'.$value['req_image'])){?>
                                                    <img src="<?php echo base_url().'images/buyer_post_requirment_image/'.$value['req_image']; ?>" alt="list_img_1" class="img-responsive list_view_img_1" />
                                                <?php } else { ?>
                                                <img src="<?php echo base_url().'images/buyer_post_requirment_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                                                <?php } ?> 

                                            </div>
                                        </div>
                                        <div class="col-sm-8 col-md-8 col-lg-8">
                                            <div class="event_news_txt">
                                                <div class="head_txt">
                                                  <a href="<?php echo base_url().'buyer/closed_requirment_detail/'.$value['id'];?>"><?php if(!empty($value['title'])) { echo ucfirst($value['title']); } else { echo "Not Available" ;} ?></a>
                                                </div>
                                                <div class="list_up"><span><i class="fa fa-folder-open"></i>
                                                    </span>
                                                         <?php if(!empty($value['category_name'])) { echo substr($value['category_name'], 0 , 50); } else { echo "Not Available"; } ?> ->
                                                         <?php if(!empty($value['subcategory_name'])) { echo substr($value['subcategory_name'], 0 , 50); } else { echo "Not Available"; } ?>
                                                </div>                                                
                                                <div class="list_add"><span><i class="fa fa-map-marker"></i>
                                                    </span>
                                                    <?php if(!empty($value['location'])) { echo substr($value['location'], 0 , 50); } else { echo "Not Available"; } ?> 

                                                </div>

                                                <div class="lsit_text_content">
                                                <div class="list_up "><span><i class="fa fa-calendar" aria-hidden="true"></i> Posted Date :
                                                    </span>
                                                         <?php echo date('F j, Y', strtotime($value['created_date']))." at ".date("g:i a", strtotime($value['created_date'])); ?> 
                                                </div>
                                                </div>
                                                
                                                <div class="lsit_text_content">
                                                <div class="list_up up-list-price"><span> <?php echo CURRENCY; ?>
                                                    </span>
                                                         <?php if(!empty($value['price'])) { echo $value['price']; } else { echo "Not Available"; } ?> 
                                                </div>
                                                </div>

                                                <div class="lsit_text_content">
                                                   <?php if(!empty($value['description'])) { echo substr($value['description'], 0 , 300); } else { echo "Not Available"; } ?>.. ...
                                                    <div class="clr"></div>
                                                </div>
                                                <div class="btns-block-accepted">
                                                    <a class="view-block-btn-list" href="<?php echo base_url().'buyer/closed_requirment_detail/'.$value['id'];?>"><i class="fa fa-eye"></i></a> <?php /*<a href="#">Accepted</a>*/?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                </div>

                            <?php
                        }
                    } else {
                        $this->load->view('no-data-found');
                    }?>
                    </div>
                    <!--pagigation start here-->
                    <div style="margin-top:-12px;">
                      <?php echo $this->pagination->create_links(); ?>
                    </div>
                    <!--pagigation end here-->
                </div>
            </div>
        </div>
    </div>
</div>
