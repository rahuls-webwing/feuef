<?php $this->load->view('buyer/top-bar'); 

if(isset($inquires_detail[0]['seller_id'])) { $buyer_id = $inquires_detail[0]['seller_id']; } else { $buyer_id = 0; }
$this->db->where('tbl_user_master.id' , $buyer_id);
$get_sender_details = $this->master_model->getRecords('tbl_user_master');

?>
<div class="page-head-name">
<div class="container">
    <div class="name-container-dash">
        <h3>My inquiry Details</h3>
    </div>
</div>
</div>
<div class="middel-container">
<div class="inner-content-block">
    <div class="container">
        <div class="row">
           <?php $this->load->view('buyer/left-bar'); ?>
           <div class="col-sm-9 col-md-9 col-lg-9">
            <!-- success msg -->
                      <div style="display:none;" class="alert-box success alert alert-success alert-dismissible msg_div"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button><span>Done:</span> You successfully Send order.</div>
                      <!-- end success msg -->
            <?php if(isset($inquires_detail) && sizeof($inquires_detail) > 0) { ?>
            
                <div class="table-main-block">
                    <div class="back-btn-main-block" style="padding:10px;">
                        <div class="replay-btn">
                            <a class="btn-delete-block" href="javascript:history.back()"><i class="fa fa-angle-double-left"></i></i> Back</a>
                        </div>
                        <div class="clr"></div>
                    </div>
                    
                    <div class="main-details-block">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Send To:
                                    </div>
                                    <div class="details-content-first">
                                        <?php if(isset($get_sender_details[0]['name'])) { echo $get_sender_details[0]['name']; } else { echo "Not Available"; } ?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Receiver Email:
                                    </div>
                                    <div class="details-content-first">
                                       <?php if(isset($get_sender_details[0]['email'])) { echo $get_sender_details[0]['email']; } else { echo "Not Available"; } ?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                             <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Inquiry Date:
                                    </div>
                                    <div class="details-content-first">
                                       <?php if(isset($inquires_detail[0]['posted_date'])) { echo date('F j, Y', strtotime($inquires_detail[0]['posted_date']))." at ".date("g:i a", strtotime($inquires_detail[0]['posted_date'])); } else { echo "Not Available"; } ?>  
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Mobile No:
                                    </div>
                                    <div class="details-content-first">
                                         <?php if(isset($get_sender_details[0]['mobile_number'])) { echo $get_sender_details[0]['mobile_number']; } else { echo "Not Available"; } ?>

                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                             <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Category:
                                    </div>
                                    <div class="details-content-first">
                                        <?php if(isset($inquires_detail[0]['category_name'])) { echo $inquires_detail[0]['category_name']; } else { echo "Not Available"; }?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Product:
                                    </div>
                                    <div class="details-content-first">
                                       <?php if(isset($inquires_detail[0]['product_name'])) { echo $inquires_detail[0]['product_name']; } else { echo "Not Available"; }?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="main-details-block n-bg-block">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="" style="text-align:justify;">
                                    <div class="first-name-block-details">
                                        Description:
                                    </div>
                                    <?php if(isset($inquires_detail[0]['description'])) { echo $inquires_detail[0]['description']; } else { echo "Not Available"; }?>
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>

                    <div class="back-btn-main-block" style="padding:10px;">
                        <div class="replay-btn">
                            <button class="msg_btn accepet_offer btn-replay-block" product_name="<?php echo $inquires_detail[0]['product_name']; ?>" seller_id="<?php echo $get_sender_details[0]['id']; ?>" type="button"> Order</button>
                            <!-- <button class="msg_btn accepet_offer btn-replay-block" product_name="< ?php echo $inquires_detail[0]['product_name']; ?>" product_category="< ?php echo $inquires_detail[0]['category_name']; ?>" reqid="<?php  ?>" offerid="<?php  ?>" type="button"> Order</button> -->
                        </div>
                        <div class="clr"></div>
                    </div>

                </div>
            <?php
            } else {
            $this->load->view('no-data-found');
            } ?>
           </div> 
        </div>
    </div>
</div>        
</div>

<!-- Order Popup form -->
<!-- Offer Modal -->
<div id="myModal" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">CONDITIONS</h4>
        </div>
        <div class="modal-body">

                    <input type="hidden" id="product_name" name="product_name">
                    <input type="hidden" id="seller_id"  name="seller_id">

                    <div class="mobile-nu-block enquiry">

                            <input type="text" 
                                   name="Merchandise_Description" 
                                   id="Merchandise_Description" 
                                   class="beginningSpace_restrict " 
                                   placeholder="Merchandise Description" />
                                   <div class="error" id="err_Merchandise_Description"></div>
                        <span class="highlight"></span>
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Transport" 
                                   id="Transport" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Transport"/>
                                   <div class="error" id="err_Transport"></div>
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Delivery_Date" 
                                   id="Delivery_Date" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Delivery Date"/>
                                   <div class="error" id="err_Delivery_Date"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Order_type" 
                                   id="Order_type" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Order type"/>
                                   <div class="error" id="err_Order_type"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Payment_Currency"  
                                   id="Payment_Currency" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Payment Currency"/>
                                   <div class="error" id="err_Payment_Currency"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Payment_Conditions" 
                                   id="Payment_Conditions" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Payment Conditions"/>
                                   <div class="error" id="err_Payment_Conditions"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Packing_Weight"  
                                   id="Packing_Weight" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Packing Weight"/>
                                   <div class="error" id="err_Packing_Weight"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Trade_confirmed_by" 
                                   id="Trade_confirmed_by" 
                                   
                                   class="beginningSpace_restrict "  
                                   placeholder="Trade confirmed by"/>
                                   <div class="error" id="err_Trade_confirmed_by"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Final_price" 
                                   id="Final_price" 
                                   
                                   class="beginningSpace_restrict" 
                                   placeholder="Final Price"/>
                                   <div class="error" id="err_Final_price"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Quantity" 
                                   id="Quantity" 
                                   
                                   class="beginningSpace_restrict char_restrict" 
                                   placeholder="Quantity"/>
                                   <div class="error" id="err_Quantity"></div> 
                        <span class="highlight"></span>
                        
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-password changes enq">
                        <button class="change-btn-pass submit_order" type="button">Ok</button>
                    </div>
                </div>
            </form>    
        </div>

    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() { 
      $('.close').on('click',function() {
        setTimeout(function(){
            location.reload();
        })
      });
    });
</script>
<!-- End Offer Modal -->
<script type="text/javascript">
$(document).ready(function(){


  $('.accepet_offer').click(function(){

    var product_name    = $(this).attr('product_name');
    var seller_id       = $(this).attr('seller_id');
    $('#product_name').val(product_name);
    $('#seller_id').val(seller_id);
    $('#myModal').modal('show');
    return false;
  });

  $('.submit_order').click(function(){

    
    
    var product_name      = $('#product_name').val();
    var seller_id         = $('#seller_id').val();

    var flag =1;
    var Merchandise_Description  = $('#Merchandise_Description').val();
    var Transport                = $('#Transport').val();
    var Delivery_Date            = $('#Delivery_Date').val(); 
    var Order_type               = $('#Order_type').val();
    var Final_price              = $('#Final_price').val();
    var Quantity                 = $('#Quantity').val();
    var Payment_Currency         = $('#Payment_Currency').val();
    var Payment_Conditions       = $('#Payment_Conditions').val();
    var Packing_Weight           = $('#Packing_Weight').val();
    var Trade_confirmed_by       = $('#Trade_confirmed_by').val();
    

   $('#err_Merchandise_Description').html('');
   $('#err_Transport').html('');
   $('#err_Delivery_Date').html('');
   $('#err_Order_type').html('');
/*   $('#err_Final_price').html('');
   $('#err_Quantity').html('');*/
   $('#err_Payment_Currency').html('');
   $('#err_Payment_Conditions').html('');
   $('#err_Packing_Weight').html('');
   $('#err_Trade_confirmed_by').html('');
  
   if(Trade_confirmed_by=="")
   {
         $('#err_Trade_confirmed_by').html('This field is required');
         $('#err_Trade_confirmed_by').show();
         $('#Trade_confirmed_by').on('keyup', function(){
          $('#err_Trade_confirmed_by').hide();
         });
         flag=0;
         $('#Trade_confirmed_by').focus();
   } 
   if(Packing_Weight=="")
   {
         $('#err_Packing_Weight').html('This field is required');
         $('#err_Packing_Weight').show();
         $('#Packing_Weight').on('keyup', function(){
          $('#err_Packing_Weight').hide();
         });
         flag=0;
         $('#Packing_Weight').focus();
   } 
   if(Payment_Conditions=="")
   {
         $('#err_Payment_Conditions').html('This field is required');
         $('#err_Payment_Conditions').show();
         $('#Payment_Conditions').on('keyup', function(){
          $('#err_Payment_Conditions').hide();
         });
         flag=0;
         $('#Payment_Conditions').focus();
   } 
   if(Payment_Currency=="")
   {
         $('#err_Payment_Currency').html('This field is required');
         $('#err_Payment_Currency').show();
         $('#Payment_Currency').on('keyup', function(){
          $('#err_Payment_Currency').hide();
         });
         flag=0;
         $('#Payment_Currency').focus();
   } 
   if(Order_type=="")
   {
         $('#err_Order_type').html('This field is required');
         $('#err_Order_type').show();
         $('#Order_type').on('keyup', function(){
          $('#err_Order_type').hide();
         });
         flag=0;
         $('#Order_type').focus();
   }
   /*if(Final_price=="")
   {
         $('#err_Final_price').html('This field is required');
         $('#err_Final_price').show();
         $('#Final_price').on('keyup', function(){
          $('#err_Final_price').hide();
         });
         flag=0;
         $('#Final_price').focus();
   }
   if(Quantity=="")
   {
         $('#err_Quantity').html('This field is required');
         $('#err_Quantity').show();
         $('#Quantity').on('keyup', function(){
          $('#err_Quantity').hide();
         });
         flag=0;
         $('#Quantity').focus();
   }*/
   if(Delivery_Date=="")
   {
         $('#err_Delivery_Date').html('This field is required');
         $('#err_Delivery_Date').show();
         $('#Delivery_Date').on('keyup', function(){
          $('#err_Delivery_Date').hide();
         });
         flag=0;
         $('#Delivery_Date').focus();
   } 
   if(Transport=="")
   {
         $('#err_Transport').html('This field is required');
         $('#err_Transport').show();
         $('#Transport').on('keyup', function(){
          $('#err_Transport').hide();
         });
         flag=0;
         $('#Transport').focus();
   } 
   if(Merchandise_Description=="")
   {

         $('#err_Merchandise_Description').html('This field is required');
         $('#err_Merchandise_Description').show();
         $('#Merchandise_Description').on('keyup', function(){
          $('#err_Merchandise_Description').hide();
         });
         flag=0;
         $('#Merchandise_Description').focus();
   } 


   if(flag == 0) {
     return false;
     event.preventDefault();
   } else {
          swal({   
               title: "Are you sure?",   
               text : "You want to send this Order ?",  
               type : "warning",   
               showCancelButton: true,   
               confirmButtonColor: "#8cc63e",  
               confirmButtonText: "Yes",  
               cancelButtonText: "No",   
               closeOnConfirm: false,   
               closeOnCancel: false }, function(isConfirm){   
               if (isConfirm) 
               { 
                  swal("Done!", "Your action successfully performed.", "success");

                  jQuery.ajax({
                    url:site_url+'buyer/order_report/',
                    method:"POST",
                    data  : {
                             Merchandise_Description:Merchandise_Description,
                             Transport:Transport,
                             Delivery_Date:Delivery_Date,
                             Order_type:Order_type,
                             Final_price:Final_price,
                             Quantity:Quantity,
                             Payment_Currency:Payment_Currency,
                             Payment_Conditions:Payment_Conditions,
                             Packing_Weight:Packing_Weight,
                             Trade_confirmed_by:Trade_confirmed_by,
                             seller_id:seller_id,
                             product_name:product_name,
                           },
                    dataType:"json",
                    beforeSend:function()
                    {
                      ajaxindicatorstart();
                    },
                    success:function(response)
                    {
                      $('.msg_div').show();
                      setTimeout(function(){
                        $('.confirm').click();
                        $('#myModal').modal('hide');
                        $('html, body').animate({scrollTop:$('.msg_div').position().top}, 'slow');
                      },500); 
                    },
                    complete:function(response)
                    {
                      ajaxindicatorstop();
                    }
                  });
               } 
               else
               { 
                  swal("Cancelled"); 
                  setTimeout(function(){
                    $('.confirm').click();
                  },500);  
               } 
          });
      }
  });
});
</script>

<!-- DatePiker -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepiker/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/datepiker/1.12.1.js"></script>
<script>
$( function() {
$( "#Delivery_Date" ).datepicker().datepicker("setDate", new Date());
} );
</script>
<!-- End DatePiker -->
