<style type="text/css">
    .label-important, .badge-important {
        background-color: #fe1010;
    }
    /*.row {
        margin-right: -15px;
        margin-left: -15px;
        width: 108%;
    }*/
</style>
<?php $this->load->view('buyer/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3>Order Forms</h3>
        </div>
    </div>
</div>
 
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <?php $this->load->view('buyer/left-bar'); ?>

                <div class="col-sm-12 col-md-8 col-lg-9">
                    <div class="row">
                   
                
                 <div class="row" style="margin-top:3px;">
                   <div class="col-sm-12 col-md-8 col-lg-12" style="padding-right: 0px;">
                    <div class="table-responsive">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-hover fill-head" style="1px solid #ececec;">
                           <tr class="pay_head" >

                              <td style="text-indent:5px;">Sender</td>
                              <td style="text-indent:5px;">Receiver</td>
                              <td style="text-indent:5px;">Offer Name</td>
                              <td style="text-indent:5px;">Type</td>
                              <td style="text-indent:5px;">Order Form</td>

                              <td style="text-indent:5px;">Send/Received</td>
                              <td style="text-indent:5px;">Status</td>
                              <td style="text-indent:5px;">Date</td>
                              <td style="text-indent:5px;">Resend</td>

                           </tr>
                           <?php 
                           $total=0;
                           if(!empty($getOffers)){ 
                                
                                 foreach($getOffers as $value) {
                                  if(!empty($value['order_form']) && file_exists('uploads/invoice/'.$value['order_form'])){
                                  ?>
                                  <tr class="pay_row_one">
                                   
                                    <?php if($value['from_id']) 
                                      { 
                                        $this->db->where('id' , $value['from_id']);
                                        $get_receiver_name = $this->master_model->getRecords('tbl_user_master');
                                      ?>
                                            <td style="padding:12px!important;"> 
                                               <?php if(!empty($get_receiver_name[0]['name'])) { echo $get_receiver_name[0]['name']; } else { echo "Not Available"; } ?>  
                                            </td>

                                    <?php } ?>

                                    <?php if($value['to_id']) 
                                      { 
                                        $this->db->where('id' , $value['to_id']);
                                        $get_receiver_name = $this->master_model->getRecords('tbl_user_master');
                                      ?>
                                            <td style="padding:12px!important;"> 
                                               <?php if(!empty($get_receiver_name[0]['name'])) { echo $get_receiver_name[0]['name']; } else { echo "Not Available"; } ?>  
                                            </td>

                                    <?php } ?>

                                    <td style="padding:12px!important;"> 
                                         <?php if(!empty($value['order_name'])) { echo $value['order_name']; } else { echo "Not Available"; } ?>
                                    </td>
                                    <td style="padding:12px!important;"> 
                                         <?php if(!empty($value['type'])) { echo $value['type']; } else { echo "Not Available"; } ?>
                                    </td>

                                    <td style="padding:12px!important;"> 
                                       <a class="btn-delete-block" href="<?php echo base_url().'uploads/invoice/'.$value['order_form'] ?>" download><i class="fa fa-cloud-download" aria-hidden="true"></i> Download pdf</a>
                                      
                                    </td>
                                    
                                    <?php if($value['from_id'] == $this->session->userdata('user_id')) 
                                      { ?>
                                            <td style="padding:12px!important;"> 
                                             send 
                                            </td>
                                      <?php
                                      } 
                                      else 
                                      { ?>
                                            <td style="padding:12px!important;"> 
                                               received 
                                            </td>

                                    <?php } ?>

                                      <td style="padding:12px!important;">  
                                          <?php 
                                            if(!empty($value['type']))
                                            {
                                                  if($value['type']=='offer')
                                                  {
                                                    $this->db->select('status');
                                                    $this->db->where('id' , $value['apply_offer_id']);
                                                    $offer_status = $this->master_model->getRecords('tbl_apply_for_offers');
                                                    
                                                    if(isset($offer_status[0]['status']) && !empty($offer_status[0]['status']) && $offer_status[0]['status']=='Accepted')
                                                    {
                                                      echo $offer_status[0]['status'];  
                                                    } 
                                                    else
                                                    {
                                                      echo "Pending";
                                                    } 
                                                  }
                                                  else if($value['type']=='product')
                                                  {
                                                    $this->db->select('status');
                                                    $this->db->where('id' , $value['apply_offer_id']);
                                                    $offer_status = $this->master_model->getRecords('tbl_apply_for_order');
                                                    
                                                    if(isset($offer_status[0]['status']) && !empty($offer_status[0]['status']) && $offer_status[0]['status']=='Accepted')
                                                    {
                                                      echo $offer_status[0]['status'];  
                                                    } 
                                                    else
                                                    {
                                                      echo "Pending";
                                                    } 
                                                  }
                                                  else if($value['type']=='requirement')
                                                  {
                                                    $this->db->select('status');
                                                    $this->db->where('id' , $value['apply_offer_id']);
                                                    $offer_status = $this->master_model->getRecords('tbl_apply_for_requirment');
                                                    
                                                    if(isset($offer_status[0]['status']) && !empty($offer_status[0]['status']) && $offer_status[0]['status']=='Accepted')
                                                    {
                                                      echo $offer_status[0]['status'];  
                                                    } 
                                                    else
                                                    {
                                                      echo "Pending";
                                                    } 
                                                  }


                                            } 
                                            ?>
                                    </td>

                                    <td style="padding:12px!important;">  <?php echo date('F j, Y', strtotime($value['date']));?>  
                                    </td>
                                    <td style="padding:12px!important;"> 
     <?php if($value['type']=='product')
     { ?>
         <br>
         <button class="msg_btn accepet_offer btn-replay-block" style="max-width: 138px;"  apply_offer_id="<?php echo $value['apply_offer_id']; ?>" offer_id="<?php echo $value['order_id']; ?>" order_for="<?php echo $value['type']; ?>" product_name="<?php if(!empty($value['order_name'])) { echo $value['order_name']; } else { echo "Not Available"; } ?>" seller_id=<?php echo $value['to_id']; ?> type="button" title="re-send order"> <i class="fa fa-reply" aria-hidden="true"></i></button>
     <?php } else if($value['type']=='offer') { ?>

         <br>
         <button class="msg_btn accepet_offer btn-replay-block" style="max-width: 138px;" apply_offer_id="<?php echo $value['apply_offer_id']; ?>" offer_id="<?php echo $value['order_id']; ?>" order_for="<?php echo $value['type']; ?>" product_name="<?php if(!empty($value['order_name'])) { echo $value['order_name']; } else { echo "Not Available"; } ?>" seller_id=<?php echo $value['to_id']; ?> type="button" title="re-send order"> <i class="fa fa-reply" aria-hidden="true"></i></button>
      <?php } else if($value['type']=='requirement') { ?>
         <br>
         <button class="msg_btn accepet_offer btn-replay-block" style="max-width: 138px;" apply_offer_id="<?php echo $value['apply_offer_id']; ?>" offer_id="<?php echo $value['order_id']; ?>" order_for="<?php echo $value['type']; ?>" product_name="<?php if(!empty($value['order_name'])) { echo $value['order_name']; } else { echo "Not Available"; } ?>" seller_id=<?php echo $value['to_id']; ?> type="button" title="re-send order"> <i class="fa fa-reply" aria-hidden="true"></i></button>
         <?php
     }?>
                                    </td>

                                  </tr>
                                  <?php  } } }
                                  else
                                  { ?>
                                    <tr class="pay_row_one">
                                      <td align="center" style="padding:3px!important;" colspan="5"> 
                                        <div >
                                              <?php  $this->load->view('no-data-found'); ?>
                                        </div>
                                      </td>
                                    </tr>
                                  <?php } ?>
                                 
                        </table>
                       </div>
                      </div> 
                    </div>
                  <!--pagigation start here-->
                 <div class="product-pagi-block">
                <?php echo $this->pagination->create_links(); ?>
               </div>
            </div>
        </div>
    </div>
</div>



<!-- for offer -->
<input type="hidden" name="order_for" id="order_for">
<input type="hidden" name="offer_id"  id="offer_id">
<input type="hidden" name="apply_offer_id"  id="apply_offer_id">

<!-- for offer -->




<!-- Order Popup form -->
<!-- Offer Modal -->
<div id="myModal" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">CONDITIONS</h4>
        </div>
        <div class="modal-body">

                    <input type="hidden" id="product_name" name="product_name">
                    <input type="hidden" id="seller_id"  name="seller_id">

                    <div class="mobile-nu-block enquiry">

                            <input type="text" 
                                   name="Merchandise_Description" 
                                   id="Merchandise_Description" 
                                   class="beginningSpace_restrict " 
                                   placeholder="Merchandise Description" />
                                   <div class="error" id="err_Merchandise_Description"></div>
                        <span class="highlight"></span>
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Transport" 
                                   id="Transport" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Transport"/>
                                   <div class="error" id="err_Transport"></div>
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Delivery_Date" 
                                   id="Delivery_Date" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Delivery Date"/>
                                   <div class="error" id="err_Delivery_Date"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Order_type" 
                                   id="Order_type" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Order type"/>
                                   <div class="error" id="err_Order_type"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Payment_Currency"  
                                   id="Payment_Currency" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Payment Currency"/>
                                   <div class="error" id="err_Payment_Currency"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Payment_Conditions" 
                                   id="Payment_Conditions" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Payment Conditions"/>
                                   <div class="error" id="err_Payment_Conditions"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Packing_Weight"  
                                   id="Packing_Weight" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Packing Weight"/>
                                   <div class="error" id="err_Packing_Weight"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Trade_confirmed_by" 
                                   id="Trade_confirmed_by" 
                                   
                                   class="beginningSpace_restrict "  
                                   placeholder="Trade confirmed by"/>
                                   <div class="error" id="err_Trade_confirmed_by"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Final_price" 
                                   id="Final_price" 
                                   
                                   class="beginningSpace_restrict" 
                                   placeholder="Final Price"/>
                                   <div class="error" id="err_Final_price"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Quantity" 
                                   id="Quantity" 
                                   
                                   class="beginningSpace_restrict  " 
                                   placeholder="Quantity"/>
                                   <div class="error" id="err_Quantity"></div> 
                        <span class="highlight"></span>
                        
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-password changes enq">
                        <button class="change-btn-pass submit_order" type="button">Ok</button>
                    </div>
                </div>
            </form>    
        </div>

    </div>
</div>
</div>
<script type="text/javascript">
$("#sort_by").on('change', function(){

$("#sort_by_search").click();

});
</script>
<script type="text/javascript">
    $(document).ready(function() { 
      $('.close').on('click',function() {
        setTimeout(function(){
            location.reload();
        })
      });
    });
</script>
<!-- End Offer Modal -->
<script type="text/javascript">
$(document).ready(function(){


  $('.accepet_offer').click(function(){

    var order_for       = $(this).attr('order_for');
    var apply_offer_id       = $(this).attr('apply_offer_id');
    var offer_id        = $(this).attr('offer_id');
    var product_name    = $(this).attr('product_name');
    var seller_id       = $(this).attr('seller_id');

    $('#product_name').val(product_name);
    $('#seller_id').val(seller_id);
    $('#order_for').val(order_for);
    $('#offer_id').val(offer_id);
    $('#apply_offer_id').val(apply_offer_id);
    
    $('#myModal').modal('show');
    return false;
  });

  $('.submit_order').click(function(){

    
    
    var product_name      = $('#product_name').val();
    var order_for         = $('#order_for').val();
    var offer_id          = $('#offer_id').val();
    var seller_id         = $('#seller_id').val();

    var flag =1;
    var Merchandise_Description  = $('#Merchandise_Description').val();
    var Transport                = $('#Transport').val();
    var Delivery_Date            = $('#Delivery_Date').val(); 
    var Order_type               = $('#Order_type').val();
    var Final_price              = $('#Final_price').val();
    var Quantity                 = $('#Quantity').val();
    var Payment_Currency         = $('#Payment_Currency').val();
    var Payment_Conditions       = $('#Payment_Conditions').val();
    var Packing_Weight           = $('#Packing_Weight').val();
    var Trade_confirmed_by       = $('#Trade_confirmed_by').val();
    

   $('#err_Merchandise_Description').html('');
   $('#err_Transport').html('');
   $('#err_Delivery_Date').html('');
   $('#err_Order_type').html('');
   /*$('#err_Final_price').html('');
   $('#err_Quantity').html('');*/
   $('#err_Payment_Currency').html('');
   $('#err_Payment_Conditions').html('');
   $('#err_Packing_Weight').html('');
   $('#err_Trade_confirmed_by').html('');
  
   if(Trade_confirmed_by=="")
   {
         $('#err_Trade_confirmed_by').html('This field is required');
         $('#err_Trade_confirmed_by').show();
         $('#Trade_confirmed_by').on('keyup', function(){
          $('#err_Trade_confirmed_by').hide();
         });
         flag=0;
         $('#Trade_confirmed_by').focus();
   } 
   if(Packing_Weight=="")
   {
         $('#err_Packing_Weight').html('This field is required');
         $('#err_Packing_Weight').show();
         $('#Packing_Weight').on('keyup', function(){
          $('#err_Packing_Weight').hide();
         });
         flag=0;
         $('#Packing_Weight').focus();
   } 
   if(Payment_Conditions=="")
   {
         $('#err_Payment_Conditions').html('This field is required');
         $('#err_Payment_Conditions').show();
         $('#Payment_Conditions').on('keyup', function(){
          $('#err_Payment_Conditions').hide();
         });
         flag=0;
         $('#Payment_Conditions').focus();
   } 
   if(Payment_Currency=="")
   {
         $('#err_Payment_Currency').html('This field is required');
         $('#err_Payment_Currency').show();
         $('#Payment_Currency').on('keyup', function(){
          $('#err_Payment_Currency').hide();
         });
         flag=0;
         $('#Payment_Currency').focus();
   } 
   if(Order_type=="")
   {
         $('#err_Order_type').html('This field is required');
         $('#err_Order_type').show();
         $('#Order_type').on('keyup', function(){
          $('#err_Order_type').hide();
         });
         flag=0;
         $('#Order_type').focus();
   }
   if(Final_price=="")
   {
         $('#err_Final_price').html('This field is required');
         $('#err_Final_price').show();
         $('#Final_price').on('keyup', function(){
          $('#err_Final_price').hide();
         });
         flag=0;
         $('#Final_price').focus();
   }
   if(Quantity=="")
   {
         $('#err_Quantity').html('This field is required');
         $('#err_Quantity').show();
         $('#Quantity').on('keyup', function(){
          $('#err_Quantity').hide();
         });
         flag=0;
         $('#Quantity').focus();
   }
   if(Delivery_Date=="")
   {
         $('#err_Delivery_Date').html('This field is required');
         $('#err_Delivery_Date').show();
         $('#Delivery_Date').on('keyup', function(){
          $('#err_Delivery_Date').hide();
         });
         flag=0;
         $('#Delivery_Date').focus();
   } 
   if(Transport=="")
   {
         $('#err_Transport').html('This field is required');
         $('#err_Transport').show();
         $('#Transport').on('keyup', function(){
          $('#err_Transport').hide();
         });
         flag=0;
         $('#Transport').focus();
   } 
   if(Merchandise_Description=="")
   {

         $('#err_Merchandise_Description').html('This field is required');
         $('#err_Merchandise_Description').show();
         $('#Merchandise_Description').on('keyup', function(){
          $('#err_Merchandise_Description').hide();
         });
         flag=0;
         $('#Merchandise_Description').focus();
   } 


   if(flag == 0) {
     return false;
     event.preventDefault();
   } else {
          swal({   
               title: "Are you sure?",   
               text : "You want to send this Order ?",  
               type : "warning",   
               showCancelButton: true,   
               confirmButtonColor: "#8cc63e",  
               confirmButtonText: "Yes",  
               cancelButtonText: "No",   
               closeOnConfirm: false,   
               closeOnCancel: false }, function(isConfirm){   
               if (isConfirm) 
               { 
                  swal("Done!", "Your action successfully performed.", "success");
                  
                  if(order_for == 'requirement'){

                            var offerd_id = $('#apply_offer_id').val();
                            var requirment_id  = $('#offer_id').val();

                            jQuery.ajax({
                            url:site_url+'buyer/accept_offer/'+requirment_id+'/'+offerd_id,
                            method:"POST",
                            data  : {
                                     Merchandise_Description:Merchandise_Description,
                                     Transport:Transport,
                                     Delivery_Date:Delivery_Date,
                                     Order_type:Order_type,
                                     Final_price:Final_price,
                                     Quantity:Quantity,
                                     Payment_Currency:Payment_Currency,
                                     Payment_Conditions:Payment_Conditions,
                                     Packing_Weight:Packing_Weight,
                                     Trade_confirmed_by:Trade_confirmed_by},
                            dataType:"json",
                            beforeSend:function()
                            {
                              ajaxindicatorstart();
                            },
                            success:function(response)
                            {
                                swal("Done!", response.msg, "success");
                                $('.msg_div').show();
                                setTimeout(function(){
                                $('.confirm').click();
                                $('#myModal').modal('hide');

                                setTimeout(function(){
                                location.reload();
                                },500); 
                                },500); 
                            },
                            complete:function(response)
                            {
                              ajaxindicatorstop();
                            }
                          });
                  
                  }else if(order_for == 'offer'){

                          var from_id        = $('#from_id').val();
                          var to_id          = $('#seller_id').val();
                          var offer_id       = $('#offer_id').val();

                          var apply_offer_id = $('#apply_offer_id').val();
                          var offer_title    = $('#product_name').val();
                          var offer_type     = 'offer';

                          jQuery.ajax({
                          url:site_url+'buyer/buyer_order/'+offer_id,
                          method:"POST",
                          data  : {
                                   from_id:from_id,
                                   to_id:to_id,
                                   offer_id:offer_id,
                                   offer_title:offer_title,
                                   apply_offer_id:apply_offer_id,
                                   offer_type:offer_type,
                                   Merchandise_Description:Merchandise_Description,
                                   Transport:Transport,
                                   Delivery_Date:Delivery_Date,
                                   Order_type:Order_type,
                                   Final_price:Final_price,
                                   Quantity:Quantity,
                                   Payment_Currency:Payment_Currency,
                                   Payment_Conditions:Payment_Conditions,
                                   Packing_Weight:Packing_Weight,
                                   Trade_confirmed_by:Trade_confirmed_by},
                                   dataType:"json",
                          
                          beforeSend:function()
                          {
                            ajaxindicatorstart();
                          },
                          success:function(response)
                          {
                            swal("Done!", response.msg, "success");
                            $('.msg_div').show();
                            setTimeout(function(){
                              $('.confirm').click();
                              $('#myModal').modal('hide');

                                  setTimeout(function(){
                                   location.reload();
                                  },500); 
                            },500); 
                          },
                          complete:function(response)
                          {
                            ajaxindicatorstop();
                          }
                        });
                  
                  }
                  else if(order_for == 'product'){

                        jQuery.ajax({
                        url:site_url+'buyer/order_report/',
                        method:"POST",
                        data  : {
                                 Merchandise_Description:Merchandise_Description,
                                 Transport:Transport,
                                 Delivery_Date:Delivery_Date,
                                 Order_type:Order_type,
                                 Final_price:Final_price,
                                 Quantity:Quantity,
                                 Payment_Currency:Payment_Currency,
                                 Payment_Conditions:Payment_Conditions,
                                 Packing_Weight:Packing_Weight,
                                 Trade_confirmed_by:Trade_confirmed_by,
                                 seller_id:seller_id,
                                 product_name:product_name,
                               },
                        dataType:"json",
                        beforeSend:function()
                        {
                          ajaxindicatorstart();
                        },
                        success:function(response)
                        {
                          $('.msg_div').show();
                          setTimeout(function(){
                            $('.confirm').click();
                            $('#myModal').modal('hide');

                                setTimeout(function(){
                                 location.reload();
                                },500); 
                          },500); 
                        },
                        complete:function(response)
                        {
                          ajaxindicatorstop();
                        }
                      });

                  }

                  
               } 
               else
               { 
                  swal("Cancelled"); 
                  setTimeout(function(){
                    $('.confirm').click();
                  },500);  
               } 
          });
      }
  });
});
</script>

<!-- DatePiker -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepiker/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/datepiker/1.12.1.js"></script>
<script>
$( function() {
$( "#Delivery_Date" ).datepicker().datepicker("setDate", new Date());
} );
</script>
<!-- End DatePiker -->