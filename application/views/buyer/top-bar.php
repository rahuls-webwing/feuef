<div class="page-head-block">
    <div class="page-head-overlay"></div>
    <div class="container">
        <div class="profile-pic-main">
            <div class="pic-profile-block">

                <?php if(!empty($this->session->userdata('user_image')) && file_exists('images/buyer_image/'.$this->session->userdata('user_image'))){?>
                <img style="height:100%;" src="<?php echo base_url().'images/buyer_image/'.$this->session->userdata('user_image'); ?>" alt="" />
                <?php } else { ?>
                  <img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
                <?php } ?> 
            

            </div>
            <div class="profile-discription">
                <div class="name-head-user">
                    <?php if(!empty($this->session->userdata('user_name'))) {
                           echo $this->session->userdata('user_name'); 
                          } else { 
                            echo "Not Available"; 
                          } 
                    ?>
                </div>
                <div class="address-user">
                    <?php if(!empty($this->session->userdata('user_city'))) {
                           echo  $this->session->userdata('user_city'); 
                          } else { 
                            echo "Not Available"; 
                          } ?> 

                          , 

                          <?php if(!empty($this->session->userdata('user_country'))) {
                           echo  $this->session->userdata('user_country'); 
                          } else { 
                            echo "Not Available"; 
                          } ?>
                </div>
                <div class="other-info-user">
                    <!-- Wines, Cereals, Snacks, Chocolate, Canned Food, Fish -->
                </div>
            </div>
        </div>
    </div>
</div>