<style type="text/css">
.table-main-block {
    margin-bottom: 30px;
    padding-top: 0px;
}
</style>
<?php
/*echo "<pre>";
  print_r($get_notifications);
echo "</pre>";*/
 ?>
<?php $this->load->view('buyer/top-bar'); ?>
    <div class="page-head-name">
        <div class="container">
            <div class="name-container-dash">
                <h3>Notification</h3>
            </div>
        </div>
    </div>
    <div class="middel-container">
        <div class="inner-content-block">
            <div class="container">
                <div class="row">
                    <?php $this->load->view('buyer/left-bar'); ?>
                    <div class="col-sm-9 col-md-9 col-lg-9">
                        <?php $this->load->view('status-msg'); ?>
                        <div class="table-main-block notifi-block-main">
                            <div class="table-responsive">
                                <table class="table inquiries-table">
                                    <tbody>
                                        <tr class="table-head-block">
                                            <th class="rtl-padd-bock" style="padding-left:30px">Notification</th>
                                            <th>Date</th>                                            
                                            <th class="rtl-padd-bock-last" style="text-align: center; padding-right:25px">Action</th>
                                        </tr>
                                        <?php if(isset($get_notifications) && sizeof($get_notifications) > 0) { ?>

                                            <?php foreach ($get_notifications as $value) {
                                              
                                            $this->db->where('tbl_user_master.id' , $value['buyer_id']);
                                            $get_sender_details = $this->master_model->getRecords('tbl_user_master');
                                          

                                            if($value['is_read']=='yes'){ ?>
                                                <tr>
                                                    <td class="rtl-padd-bock" style="padding-left:30px"><?php if(isset($value['notification'])) { echo substr($value['notification'], 0 , 60).'...'; } else { echo "Not Availabel"; } ?></td>
                                                    <td style="font-family:arial"><?php echo date('F j, Y', strtotime($value['created_date']))." at ".date("g:i a", strtotime($value['created_date'])); ?></td>                                            
                                                    <td class="rtl-padd-bock-last" style="text-align: center; padding-right:25px">
                                                    <?php
                                                      $arg="";
                                                      if($value['inq_reply_id']!='0')
                                                      {
                                                        
                                                        $arg="id_".$value['id'];
                                                      }
                                                      else
                                                      {
                                                        $arg=$value['id']; 
                                                      }
                                                     ?>
                                                    <a href="<?php echo base_url().'buyer/notification_details/'.$arg; ?>"><i class="fa fa-eye" style="font-size:1.2em;"></i></a>
                                                    <a class="delete_selected_noti" title="Delete "  style="text-decoration:none;" data-noti="<?php echo $value['id'];  ?>" href="javascript:void(0);"><i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                </tr>

                                            <?php } else { ?>

                                                <tr>
                                                    <td class="rtl-padd-bock" style="padding-left:30px; color: #1c9aea;"><?php if(isset($value['notification'])) { echo substr($value['notification'], 0 , 60).'...'; } else { echo "Not Availabel"; } ?></td>
                                                    <td style="font-family:arial;color: #1c9aea;"><?php echo date('F j, Y', strtotime($value['created_date']))." at ".date("g:i a", strtotime($value['created_date'])); ?></td>                                            
                                                    <td class="rtl-padd-bock-last" style="text-align: center; padding-right:25px;color: #1c9aea;">

                                                      <?php
                                                      $arg="";
                                                      if($value['inq_reply_id']!='0')
                                                      {
                                                        
                                                        $arg="id_".$value['id'];
                                                      }
                                                      else
                                                      {
                                                        $arg=$value['id']; 
                                                      }
                                                     ?>
                                                    
                                                    <a href="<?php echo base_url().'buyer/notification_details/'.$arg; ?>"><i class="fa fa-eye" style="font-size:1.2em;"></i></a>
                                                    <a class="delete_selected_noti" title="Delete "  style="text-decoration:none;" data-noti="<?php echo $value['id'];  ?>" href="javascript:void(0);"><i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                </tr>

                                            <?php } } ?>    

                                        <?php } else {
                                            ?><tr>
                                                 <td  colspan="9" style="font-family:arial"><?php
                                                    $this->load->view('no-data-found');
                                               ?></td>
                                              </tr>
                                            <?php
                                        }?>
                                    </tbody>
                                </table>
                            </div>
                            <!--pagigation start here-->
                              <?php echo $this->pagination->create_links(); ?>
                            <!--pagigation end here-->
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>    
<script type="text/javascript">
  
  $('.delete_selected_noti').click(function(){
        var notification_id = jQuery(this).data("noti");
        swal({   
             title: "Are you sure?",   
             text: "You want to delete this notification ?",  
             type: "warning",   
             showCancelButton: true,   
             confirmButtonColor: "#8cc63e",  
             confirmButtonText: "Yes",  
             cancelButtonText: "No",   
             closeOnConfirm: false,   
             closeOnCancel: false }, function(isConfirm){   
              if (isConfirm) 
              { 
                     swal("Deleted!", "Your notification has been deleted.", "success"); 
                           location.href=site_url+"buyer/notification_delete/"+notification_id;
              } 
              else
              { 
                     swal("Cancelled", "Your notification is safe :)", "error");          
                } 
            });
    }); 

</script>      