<style type="text/css">
    .label-important, .badge-important {
        background-color: #fe1010;
    }
</style>
<?php $this->load->view('buyer/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3>Post Requirement</h3>
        </div>
    </div>
</div>
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <?php $this->load->view('buyer/left-bar'); ?>
                <div class="col-sm-9 col-md-9 col-lg-9">
                    <div class="account-info-block">
                        <div class="row">                            
                          <?php $this->load->view('status-msg'); ?>   
                         <div ng-controller="PostRequirment">    
                            <form  
                               class=""
                               name="requirmentForm" 
                               enctype="multipart/form-data"
                               novalidate 
                               ng-submit="requirmentForm.$valid && storePostRequirment();"  >
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Requirement Title <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block"
                                        ng-class="{'has-error':requirmentForm.title.$touched && requirmentForm.title.$invalid}">
                                            <input type="text" 
                                                   name="title" 
                                                   class="beginningSpace_restrict " 
                                                   ng-model="user.title"
                                                   ng-required="true"
                                                   ng-maxlength="100"
                                                   placeholder="Enter Title"/>
                                        </div>

                                        <div class="error-new-block" ng-messages="requirmentForm.title.$error" ng-if="requirmentForm.$submitted || requirmentForm.title.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            <p ng-message="maxlength"   class="error">  This field to long. Only 100 characters allow.</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                    </div>
                                </div>
                                
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Requirement category <span class="required-field"><sup>*</sup></span></div>

                                        <div class="select-bock-container" ng-class="{'has-error':requirmentForm.subcategory_id.$touched && requirmentForm.subcategory_id.$invalid}" >
                                            <select name="subcategory_id" 

                                                    ng-model="user.subcategory_id"
                                                    ng-required="true">


                                                    <option value="">Select requirement category</option>


                                                      <?php foreach ($getCat as $cate_value) {
                                                      ?>
                                                          <optgroup label="<?php echo '- '.$cate_value['category_name']; ?>" >
                                                          <?php foreach ($getSubcat as $value) {
                                                          ?>

                                                                  <?php if($value['category_id'] ==  $cate_value['category_id']) { ?>

                                                                    <option value="<?php echo $value['subcategory_id']; ?>"><?php echo $value['subcategory_name']; ?></option>

                                                                  <?php  } ?>

                                                               
                                                           <?php
                                                          } ?>     
                                                         </optgroup>

                                                      <?
                                                      } ?>
                                            </select>


                                            <div class="error-new-block" ng-messages="requirmentForm.subcategory_id.$error" ng-if="requirmentForm.$submitted || requirmentForm.subcategory_id.$touched">
                                            <div>
                                            <div class="err_msg_div" style="display:none;">
                                                <p ng-message="required"    class="error">  This field is required</p>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                    $(document).ready(function(){
                                                      setTimeout(function(){
                                                        $('.err_msg_div').removeAttr('style');
                                                      },200);
                                                    });
                                                </script>
                                            </div>


                                        </div>

                                    </div>
                                </div>


                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Price (<?php echo CURRENCY; ?>) <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block"
                                        ng-class="{'has-error':requirmentForm.price.$touched && requirmentForm.price.$invalid}">
                                            <input type="text" 
                                                   name="price" 
                                                   class="beginningSpace_restrict " 
                                                   ng-model="user.price"
                                                   ng-required="true"
                                                   placeholder="Enter price"/>
                                        </div>

                                        <div class="error-new-block" ng-messages="requirmentForm.price.$error" ng-if="requirmentForm.$submitted || requirmentForm.price.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                    </div>
                                </div>
                                
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Location <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block" ng-class="{'has-error':requirmentForm.location.$touched && requirmentForm.location.$invalid}">
                                            <input type="text" 
                                                   name="location" 
                                                   class="beginningSpace_restrict" 
                                                   ng-model="user.location" 
                                                   placeholder="Enter Location"
                                                   ng-autocomplete
                                                   ng-required="true"
                                                   details="obj_autocomplete"/>
                                        </div>

                                        <div class="error-new-block" ng-messages="requirmentForm.location.$error" ng-if="requirmentForm.$submitted || requirmentForm.location.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>


                                    </div>
                                </div>        

                                <input type="hidden" name="latitude"  id="latitude" ng-model="user.latitude" placeholder="latitude">
                                <input type="hidden" name="longitude" id="longitude" ng-model="user.longitude" placeholder="longitude">   


                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Requirement type <span class="required-field"><sup>*</sup></span></div>

                                        <div class="select-bock-container" ng-class="{'has-error':requirmentForm.req_type.$touched && requirmentForm.req_type.$invalid}" >
                                            <select name="req_type" 

                                                    ng-model="user.req_type"
                                                    ng-required="true">
                                                    <option value="">Select requirement type</option>
                                                    <option value="Fix">Fix</option>
                                                    <option value="livemarket">Livemarket</option>

                                            </select>

                                            <div class="error-new-block" ng-messages="requirmentForm.req_type.$error" ng-if="requirmentForm.$submitted || requirmentForm.req_type.$touched">
                                            <div>
                                            <div class="err_msg_div" style="display:none;">
                                                <p ng-message="required"    class="error">  This field is required</p>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                    $(document).ready(function(){
                                                      setTimeout(function(){
                                                        $('.err_msg_div').removeAttr('style');
                                                      },200);
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </div>                    
                                
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="email-box-2">
                                        <div class="email-title">Requirement description <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block" ng-class="{'has-error':requirmentForm.description.$touched && requirmentForm.description.$invalid}">
                                            <textarea rows="" 
                                                      cols="" 
                                                      name="description" 
                                                      class="beginningSpace_restrict" 
                                                      ng-model="user.description"
                                                      ng-required="true"
                                                      ng-maxlength="1000"
                                                      placeholder="Enter Requirement description"></textarea>
                                        </div>

                                        <div class="error-new-block" ng-messages="requirmentForm.description.$error" ng-if="requirmentForm.$submitted || requirmentForm.description.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            <p ng-message="maxlength"   class="error">  This field to long. Only 1000 characters allow.</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12">                                
                                <div class="requirement-upload"> 
                                 <div class="email-title">Requirement Image <span class="required-field"><sup>*</sup></span></div>                                    
                                    <div class="fileUpload">    
                                        <!-- <img src="<?php echo base_url(); ?>/front-asset/images/gallrey.png" alt="" /> -->
                                        <img src="<?php echo base_url(); ?>/front-asset/images/gallrey.png" id="img_profile" 
                                        styel="req_img" class="" alt="profile" />
                                        <input type="file" 
                                               class="upload allowOnlyImg"
                                               name="req_image" 
                                               id="req_image" 
                                               ng-model="user.req_image" />
                                    </div>

                                    <div class="img-err">
                                        <div id="err-img" style="color:red;" class="" ></div>
                                    </div>
                                    <div class="img-err">
                                        <div style="color:#007aca; margin-top: 10px;" class="" ><b><span class="label label-important">NOTE!</span> Image allow greater than 162x260 (h X w) dimension only and also image is mandatory.</b></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="btn-password save col-sm-12 col-md-12 col-lg-12">
                                        <button style="max-width: 130px; float:right;" class="change-btn-pass pull-right" type="submit">Post</button>
                                    </div>
                                </div>
                           </div>  
                       </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLKCmJEIrOrzdRklZHYer8q9qx2XLJ4Vs&sensor=false&libraries=places"></script>

<script type="text/javascript">
$(document).ready(function(){
 $('#req_image').click(function(){
    ajaxindicatorstart();
    setTimeout(function(){
      ajaxindicatorstop();
    },2000);
 });
}); 
</script>

