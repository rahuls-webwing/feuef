<?php
/*echo "<pre>";
    print_r($get_notifications);
echo "</pre>";*/


$btn_val='Accept'; 
$disable='';
if(!empty($get_notifications[0]['apply_offer_id']))
{
    $this->db->select("tbl_apply_for_offers.status");
    $this->db->where('tbl_apply_for_offers.id' , $get_notifications[0]['apply_offer_id']);
    $status = $this->master_model->getRecords('tbl_apply_for_offers');    
    if(!empty($status[0]['status']))
    {
        $status=$status[0]['status'];
        if($status=='Accepted')
        {
            $btn_val='Order Accepted';
            $disable='disabled';
        }
        else
        {
            $btn_val='Order'; 
            $disable='';
        }
    }
}


?>
<?php $this->load->view('buyer/top-bar'); 
if(isset($get_notifications[0]['inq_reply_id']) && $get_notifications[0]['inq_reply_id']=='0')
{
   


if(isset($get_notifications[0]['seller_id'])) { $seller_id = $get_notifications[0]['seller_id']; } else { $seller_id = 0; }
$this->db->where('tbl_user_master.id' , $seller_id);
$get_sender_details = $this->master_model->getRecords('tbl_user_master');
?>
<div class="page-head-name">
<div class="container">
    <div class="name-container-dash">
        <h3>Notifications Details</h3>
    </div>
</div>
</div>
<div class="middel-container">
<div class="inner-content-block">
    <div class="container">
        <div class="row">
           <?php $this->load->view('buyer/left-bar'); ?>
           <div class="col-sm-9 col-md-9 col-lg-9">
            <?php if(isset($get_notifications) && sizeof($get_notifications) > 0) { ?>
            
                <div class="table-main-block">
                    <div class="back-btn-main-block" style="padding:10px;">
                        <div class="replay-btn">
                            <!-- <a class="btn-replay-block" href="#"><i class="fa fa-reply"></i> reply</a> -->
                            <a class="btn-delete-block" href="javascript:history.back()"><i class="fa fa-angle-double-left"></i></i> Back</a>
                        </div>
                        <div class="clr"></div>
                    </div>
                    <div class="main-details-block">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Notification:
                                    </div>
                                    <div class="details-content-first">
                                        <?php if(isset($get_notifications[0]['notification'])) { echo $get_notifications[0]['notification']; } else { echo "Not Available"; }?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <?php if(isset($get_notifications[0]['url']) && !empty($get_notifications[0]['url'])) { ?>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Url:
                                    </div>
                                    <div class="details-content-first">
                                        <?php if(isset($get_notifications[0]['url'])) { echo '<a href="'.base_url().$get_notifications[0]['url'].'">View details</a>'; } else { echo "Not Available"; }?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Sender:
                                    </div>
                                    <div class="details-content-first">
                                        <?php if(isset($get_sender_details[0]['name'])) { echo $get_sender_details[0]['name']; } else { echo "Not Available"; } ?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Sender Email:
                                    </div>
                                    <div class="details-content-first">
                                       <?php if(isset($get_sender_details[0]['email'])) { echo $get_sender_details[0]['email']; } else { echo "Not Available"; } ?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                             <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Notification Date:
                                    </div>
                                    <div class="details-content-first">
                                        <?php echo date('F j, Y', strtotime($get_notifications[0]['created_date']))." at ".date("g:i a", strtotime($get_notifications[0]['created_date'])); ?> 
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <?php if(isset($get_notifications[0]['details']) && !empty($get_notifications[0]['details'])) { ?>
                    <div class="main-details-block n-bg-block">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="" style="text-align:justify;">
                                    <div class="first-name-block-details">
                                        Details:
                                    </div>
                                    <?php if(isset($get_notifications[0]['details'])) { echo $get_notifications[0]['details']; } else { echo "Not Available"; }?>
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            <?php
            } else {
            $this->load->view('no-data-found');
            } ?>
           </div> 
        </div>
    </div>
</div>        
</div>
<?php }
else
{
    ?>
            <div class="middel-container">
<div class="inner-content-block">
    <div class="container">
        <div class="row">
           <?php $this->load->view('buyer/left-bar'); ?>
           <div class="col-sm-9 col-md-9 col-lg-9">
            
            
                <div class="table-main-block">
                    <div class="back-btn-main-block" style="padding:10px;">
                        <div class="replay-btn">
                            <!-- <a class="btn-replay-block" href="#"><i class="fa fa-reply"></i> reply</a> -->
                            <a class="btn-delete-block" href="javascript:history.back()"><i class="fa fa-angle-double-left"></i></i> Back</a>
                        </div>
                        <div class="clr"></div>
                    </div>
                    <div class="main-details-block">
                        <div class="row">


                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Seller Name:
                                    </div>
                                    <div class="details-content-first">
                                        <?php echo $get_notifications[0]['name']; ?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Seller Offer:
                                    </div>
                                    <div class="details-content-first">
                                        <?php echo $get_notifications[0]['title']; ?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Your Offer:
                                    </div>
                                    <div class="details-content-first">
                                        <?php echo $get_notifications[0]['offer_description']; ?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                           
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Seller Reply:
                                    </div>
                                    <div class="details-content-first">
                                        <?php echo $get_notifications[0]['reply_desc']; ?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Date:
                                    </div>
                                    <div class="details-content-first">
                                        <?php 
                                            $date=$get_notifications[0]['created_date'];
                                             list($created_date) = explode(' ', $date);
                                        ?>
                                        <?php echo $created_date; ?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            
                             
                           
                        </div>
                    </div>
                    
                    <div class="main-details-block n-bg-block">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="" style="text-align:justify;">
                                    <div class="first-name-block-details">
                                        
                                    </div>
                                        <button class="btn btn-primary  accepet_offer" reqid="" offerid="<?php echo $get_notifications[0]['id'] ?>" from_id="<?php echo $this->session->userdata('user_id') ?>" to_id="<?php echo $get_notifications[0]['user_id'] ?>" type="button" apply_offer_id="<?php echo $get_notifications[0]['apply_offer_id']  ?>" offer_title="<?php echo $get_notifications[0]['title']; ?>" <?php echo $disable; ?> > <?php echo $btn_val; ?>  </button>
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            
           </div> 
        </div>
    </div>
</div>        
</div>      
    <?php
}

 ?>

 <!-- Order model starts here -->

<div id="myModal" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">CONDITIONS</h4>
        </div>
        <div class="modal-body">
        <form id="buyer_order_form">
                    <input type="hidden" id="from_id" name="from_id" >
                    <input type="hidden" id="to_id" name="from_id">
                    <input type="hidden" id="offer_id"  name="offer_id">
                    <input type="hidden" id="apply_offer_id">
                    <input type="hidden" id="offer_title">

                    <div class="mobile-nu-block enquiry">

                            <input type="text" 
                                   name="Merchandise_Description" 
                                   id="Merchandise_Description" 
                                   class="beginningSpace_restrict " 
                                   placeholder="Merchandise Description" />
                                   <div class="error" id="err_Merchandise_Description"></div>
                        <span class="highlight"></span>
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Transport" 
                                   id="Transport" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Transport"/>
                                   <div class="error" id="err_Transport"></div>
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Delivery_Date" 
                                   id="Delivery_Date" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Delivery Date"/>
                                   <div class="error" id="err_Delivery_Date"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Order_type" 
                                   id="Order_type" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Order type"/>
                                   <div class="error" id="err_Order_type"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Payment_Currency"  
                                   id="Payment_Currency" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Payment Currency"/>
                                   <div class="error" id="err_Payment_Currency"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Payment_Conditions" 
                                   id="Payment_Conditions" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Payment Conditions"/>
                                   <div class="error" id="err_Payment_Conditions"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Packing_Weight"  
                                   id="Packing_Weight" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Packing Weight"/>
                                   <div class="error" id="err_Packing_Weight"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Trade_confirmed_by" 
                                   id="Trade_confirmed_by" 
                                   
                                   class="beginningSpace_restrict "  
                                   placeholder="Trade confirmed by"/>
                                   <div class="error" id="err_Trade_confirmed_by"></div> 
                        <span class="highlight"></span>
                        
                    </div>

                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Final_price" 
                                   id="Final_price" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Final Price"/>
                                   <div class="error" id="err_Final_price"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Quantity" 
                                   id="Quantity" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Quantity"/>
                                   <div class="error" id="err_Quantity"></div> 
                        <span class="highlight"></span>
                        
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-password changes enq">
                        <button class="change-btn-pass submit_accepet_offer" type="button">Ok</button>
                    </div>
                </div>
            </form>    
        </div>

    </div>
</div>
</div>

 <!-- ///////  \\\\\\\\\\\\\\\\\ -->

 <script>
 $('.accepet_offer').click(function(){
    $("#myModal").modal("show");
    $('#from_id').val($(this).attr('from_id'));
    $('#to_id').val($(this).attr('to_id'));
    $('#offer_id').val($(this).attr('offerid'));
    $('#apply_offer_id').val($(this).attr('apply_offer_id'));
    $('#offer_title').val($(this).attr('offer_title'));

 });
</script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepiker/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/datepiker/1.12.1.js"></script>
<script>

$( function() {
$( "#Delivery_Date" ).datepicker().datepicker("setDate", new Date());
} );

 $('.submit_accepet_offer').click(function(){

    
    
    var from_id=$('#from_id').val();
    var to_id=$('#to_id').val();
    var offer_id  = $('#offer_id').val();

    var apply_offer_id=$('#apply_offer_id').val();
    var offer_title=$('#offer_title').val();
    var offer_type='offer';


    var flag =1;
    var Merchandise_Description  = $('#Merchandise_Description').val();
    var Transport                = $('#Transport').val();
    var Delivery_Date            = $('#Delivery_Date').val(); 
    var Order_type               = $('#Order_type').val();
    var Final_price              = $('#Final_price').val();
    var Quantity                 = $('#Quantity').val();
    var Payment_Currency         = $('#Payment_Currency').val();
    var Payment_Conditions       = $('#Payment_Conditions').val();
    var Packing_Weight           = $('#Packing_Weight').val();
    var Trade_confirmed_by       = $('#Trade_confirmed_by').val();


    

   $('#err_Merchandise_Description').html('');
   $('#err_Transport').html('');
   $('#err_Delivery_Date').html('');
   $('#err_Order_type').html('');
   /*$('#err_Final_price').html('');
   $('#err_Quantity').html('');*/
   $('#err_Payment_Currency').html('');
   $('#err_Payment_Conditions').html('');
   $('#err_Packing_Weight').html('');
   $('#err_Trade_confirmed_by').html('');
  
   if(Trade_confirmed_by=="")
   {
         $('#err_Trade_confirmed_by').html('This field is required');
         $('#err_Trade_confirmed_by').show();
         $('#Trade_confirmed_by').on('keyup', function(){
          $('#err_Trade_confirmed_by').hide();
         });
         flag=0;
         $('#Trade_confirmed_by').focus();
   } 
   if(Packing_Weight=="")
   {
         $('#err_Packing_Weight').html('This field is required');
         $('#err_Packing_Weight').show();
         $('#Packing_Weight').on('keyup', function(){
          $('#err_Packing_Weight').hide();
         });
         flag=0;
         $('#Packing_Weight').focus();
   } 
   if(Payment_Conditions=="")
   {
         $('#err_Payment_Conditions').html('This field is required');
         $('#err_Payment_Conditions').show();
         $('#Payment_Conditions').on('keyup', function(){
          $('#err_Payment_Conditions').hide();
         });
         flag=0;
         $('#Payment_Conditions').focus();
   } 
   if(Payment_Currency=="")
   {
         $('#err_Payment_Currency').html('This field is required');
         $('#err_Payment_Currency').show();
         $('#Payment_Currency').on('keyup', function(){
          $('#err_Payment_Currency').hide();
         });
         flag=0;
         $('#Payment_Currency').focus();
   } 
   if(Order_type=="")
   {
         $('#err_Order_type').html('This field is required');
         $('#err_Order_type').show();
         $('#Order_type').on('keyup', function(){
          $('#err_Order_type').hide();
         });
         flag=0;
         $('#Order_type').focus();
   }
   /*if(Final_price=="")
   {
         $('#err_Final_price').html('This field is required');
         $('#err_Final_price').show();
         $('#Final_price').on('keyup', function(){
          $('#err_Final_price').hide();
         });
         flag=0;
         $('#Final_price').focus();
   }
   if(Quantity=="")
   {
         $('#err_Quantity').html('This field is required');
         $('#err_Quantity').show();
         $('#Quantity').on('keyup', function(){
          $('#err_Quantity').hide();
         });
         flag=0;
         $('#Quantity').focus();
   }*/
   if(Delivery_Date=="")
   {
         $('#err_Delivery_Date').html('This field is required');
         $('#err_Delivery_Date').show();
         $('#Delivery_Date').on('keyup', function(){
          $('#err_Delivery_Date').hide();
         });
         flag=0;
         $('#Delivery_Date').focus();
   } 
   if(Transport=="")
   {
         $('#err_Transport').html('This field is required');
         $('#err_Transport').show();
         $('#Transport').on('keyup', function(){
          $('#err_Transport').hide();
         });
         flag=0;
         $('#Transport').focus();
   } 
   if(Merchandise_Description=="")
   {

         $('#err_Merchandise_Description').html('This field is required');
         $('#err_Merchandise_Description').show();
         $('#Merchandise_Description').on('keyup', function(){
          $('#err_Merchandise_Description').hide();
         });
         flag=0;
         $('#Merchandise_Description').focus();
   } 

var offerd_id="4";
   if(flag == 0) {
     return false;
     event.preventDefault();
   } else {
          swal({   
               title: "Are you sure?",   
               text : "You want to accept this offer ?",  
               type : "warning",   
               showCancelButton: true,   
               confirmButtonColor: "#8cc63e",  
               confirmButtonText: "Yes",  
               cancelButtonText: "No",   
               closeOnConfirm: false,   
               closeOnCancel: false }, function(isConfirm){   
               if (isConfirm) 
               { 
                  //swal("Done!", "Your action successfully performed.", "success");
                  jQuery.ajax({
                    url:site_url+'buyer/buyer_order/'+offer_id+'/'+offerd_id,
                    method:"POST",
                    data  : {
                             from_id:from_id,
                             to_id:to_id,
                             offer_id:offer_id,
                             offer_title:offer_title,
                             apply_offer_id:apply_offer_id,
                             offer_type:offer_type,
                             Merchandise_Description:Merchandise_Description,
                             Transport:Transport,
                             Delivery_Date:Delivery_Date,
                             Order_type:Order_type,
                             Final_price:Final_price,
                             Quantity:Quantity,
                             Payment_Currency:Payment_Currency,
                             Payment_Conditions:Payment_Conditions,
                             Packing_Weight:Packing_Weight,
                             Trade_confirmed_by:Trade_confirmed_by},
                             dataType:"json",
                    
                    beforeSend:function()
                    {
                      ajaxindicatorstart();
                    },
                    success:function(response)
                    {
                      swal("Done!", response.msg, "success");
                      $('#buyer_order_form')[0].reset();
                     //alert(response.offer_id);
                      setTimeout(function(){
                        $('.confirm').click();
                        $('#myModal').modal('hide');
                        $('.offer_div').hide();
                        $('.requirment_div'+offer_id).hide();
                        $('.msg_div'+offer_id).show();
                       
                          $("html, body").animate({ scrollTop: 0 }, "slow");
                      },2000); 
                    },
                    complete:function(response)
                    {
                      ajaxindicatorstop();
                    }
                  });
               } 
               else
               { 
                  swal("Cancelled"); 
                  setTimeout(function(){
                    $('.confirm').click();
                  },500);  
               } 
          });
      }
  });
 </script>