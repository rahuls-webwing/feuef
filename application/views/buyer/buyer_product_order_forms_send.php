<style type="text/css">
    .label-important, .badge-important {
        background-color: #fe1010;
    }
</style>
<?php $this->load->view('buyer/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3>Product Order Forms Send</h3>
        </div>
    </div>
</div>
 
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <?php $this->load->view('buyer/left-bar'); ?>

                <div class="col-sm-12 col-md-8 col-lg-9">
                    <div class="row">
                  
                  <!-- success msg -->
                    <div style="display:none;" class="alert-box success alert alert-success alert-dismissible msg_div"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button><span>Done:</span> You successfully Send order.</div>
                    <!-- end success msg --> 
                
                 <div class="row" style="margin-top:3px;">
                   <div class="col-sm-12 col-md-8 col-lg-12" style="padding-right: 0px;">
                    <div class="table-responsive">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-hover fill-head" style="1px solid #ececec;">
                           <tr class="pay_head" >
                              <td style="text-indent:5px;">Seller Name</td>
                              <td style="text-indent:5px;">Requirement Name</td>
                              <td style="text-indent:5px;">Accepted Date</td>
                              <td style="text-indent:5px;">Order Form</td>
                              <td style="text-indent:5px;">Resend Form</td>
                           </tr>
                           <?php 
                           $total=0;
                           if(!empty($get_order_form))
                           { 
                                 
                                 foreach($get_order_form as $value) 
                                 {
                                      if(!empty($value['order_from']) && file_exists('uploads/invoice/'.$value['order_from']))
                                      {
                                      ?>
                                      <tr class="pay_row_one">
                                        <td style="padding:12px!important;"> <?php echo $value['name']; ?></td>
                                        <td style="padding:12px!important;"> <?php echo $value['product_name']; ?></td>
                                        <td style="padding:12px!important;">  <?php echo date('F j, Y', strtotime($value['order_created_date']))." at ".date("g:i a", strtotime($value['order_created_date'])); ?>  </td>
                                        <td style="padding:12px!important;"> 
                                           <a class="btn-delete-block" href="<?php echo base_url().'uploads/invoice/'.$value['order_from'] ?>" download><i class="fa fa-cloud-download" aria-hidden="true"></i> Order Form</a>
                                        </td>
                                        <td style="padding:12px!important;"> 
                                           <!-- <a class="btn-delete-block" href=""><i class="fa fa-reply" aria-hidden="true"></i> Order Form</a> -->
                                           <button class="msg_btn accepet_offer btn-replay-block" product_name="<?php echo $value['product_name']; ?>" seller_id=<?php echo $value['order_seller_id']; ?> type="button"> <i class="fa fa-reply" aria-hidden="true"></i> Order</button>

                                           <!-- <button class="msg_btn accepet_offer btn-replay-block" product_name="< ?php echo $inquires_detail[0]['product_name']; ?>" seller_id="< ?php echo $get_sender_details[0]['id']; ?>" type="button"> Order</button> -->
                                        </td>
                                      </tr>
                                <?php  }// end if
                                  }// end foreach

                                
                            }
                            else
                            { ?>
                              <tr class="pay_row_one">
                                <td align="center" style="padding:3px!important;" colspan="5"> 
                                  <div >
                                        <?php  $this->load->view('no-data-found'); ?>
                                  </div>
                                </td>
                              </tr>
                            <?php } ?>
                        </table>
                       </div>
                      </div> 
                    </div>
                  <!--pagigation start here-->
                 <div class="product-pagi-block">
                <?php echo $this->pagination->create_links(); ?>
               </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$("#sort_by").on('change', function(){

$("#sort_by_search").click();

});
</script>

<!-- Order Popup form -->
<!-- Offer Modal -->
<div id="myModal" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">CONDITIONS</h4>
        </div>
        <div class="modal-body">

                    <input type="hidden" id="product_name" name="product_name">
                    <input type="hidden" id="seller_id"  name="seller_id">

                    <div class="mobile-nu-block enquiry">

                            <input type="text" 
                                   name="Merchandise_Description" 
                                   id="Merchandise_Description" 
                                   class="beginningSpace_restrict " 
                                   placeholder="Merchandise Description" />
                                   <div class="error" id="err_Merchandise_Description"></div>
                        <span class="highlight"></span>
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Transport" 
                                   id="Transport" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Transport"/>
                                   <div class="error" id="err_Transport"></div>
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Delivery_Date" 
                                   id="Delivery_Date" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Delivery Date"/>
                                   <div class="error" id="err_Delivery_Date"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Order_type" 
                                   id="Order_type" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Order type"/>
                                   <div class="error" id="err_Order_type"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Payment_Currency"  
                                   id="Payment_Currency" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Payment Currency"/>
                                   <div class="error" id="err_Payment_Currency"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Payment_Conditions" 
                                   id="Payment_Conditions" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Payment Conditions"/>
                                   <div class="error" id="err_Payment_Conditions"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Packing_Weight"  
                                   id="Packing_Weight" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Packing Weight"/>
                                   <div class="error" id="err_Packing_Weight"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Trade_confirmed_by" 
                                   id="Trade_confirmed_by" 
                                   
                                   class="beginningSpace_restrict "  
                                   placeholder="Trade confirmed by"/>
                                   <div class="error" id="err_Trade_confirmed_by"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Final_price" 
                                   id="Final_price" 
                                   
                                   class="beginningSpace_restrict" 
                                   placeholder="Final Price"/>
                                   <div class="error" id="err_Final_price"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Quantity" 
                                   id="Quantity" 
                                   
                                   class="beginningSpace_restrict char_restrict " 
                                   placeholder="Quantity"/>
                                   <div class="error" id="err_Quantity"></div> 
                        <span class="highlight"></span>
                        
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-password changes enq">
                        <button class="change-btn-pass submit_order" type="button">Ok</button>
                    </div>
                </div>
            </form>    
        </div>

    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() { 
      $('.close').on('click',function() {
        setTimeout(function(){
            location.reload();
        })
      });
    });
</script>
<!-- End Offer Modal -->
<script type="text/javascript">
$(document).ready(function(){


  $('.accepet_offer').click(function(){

    var product_name    = $(this).attr('product_name');
    var seller_id       = $(this).attr('seller_id');
    $('#product_name').val(product_name);
    $('#seller_id').val(seller_id);
    $('#myModal').modal('show');
    return false;
  });

  $('.submit_order').click(function(){

    
    
    var product_name      = $('#product_name').val();
    var seller_id         = $('#seller_id').val();

    var flag =1;
    var Merchandise_Description  = $('#Merchandise_Description').val();
    var Transport                = $('#Transport').val();
    var Delivery_Date            = $('#Delivery_Date').val(); 
    var Order_type               = $('#Order_type').val();
    var Final_price              = $('#Final_price').val();
    var Quantity                 = $('#Quantity').val();
    var Payment_Currency         = $('#Payment_Currency').val();
    var Payment_Conditions       = $('#Payment_Conditions').val();
    var Packing_Weight           = $('#Packing_Weight').val();
    var Trade_confirmed_by       = $('#Trade_confirmed_by').val();
    

   $('#err_Merchandise_Description').html('');
   $('#err_Transport').html('');
   $('#err_Delivery_Date').html('');
   $('#err_Order_type').html('');
/*   $('#err_Final_price').html('');
   $('#err_Quantity').html('');*/
   $('#err_Payment_Currency').html('');
   $('#err_Payment_Conditions').html('');
   $('#err_Packing_Weight').html('');
   $('#err_Trade_confirmed_by').html('');
  
   if(Trade_confirmed_by=="")
   {
         $('#err_Trade_confirmed_by').html('This field is required');
         $('#err_Trade_confirmed_by').show();
         $('#Trade_confirmed_by').on('keyup', function(){
          $('#err_Trade_confirmed_by').hide();
         });
         flag=0;
         $('#Trade_confirmed_by').focus();
   } 
   if(Packing_Weight=="")
   {
         $('#err_Packing_Weight').html('This field is required');
         $('#err_Packing_Weight').show();
         $('#Packing_Weight').on('keyup', function(){
          $('#err_Packing_Weight').hide();
         });
         flag=0;
         $('#Packing_Weight').focus();
   } 
   if(Payment_Conditions=="")
   {
         $('#err_Payment_Conditions').html('This field is required');
         $('#err_Payment_Conditions').show();
         $('#Payment_Conditions').on('keyup', function(){
          $('#err_Payment_Conditions').hide();
         });
         flag=0;
         $('#Payment_Conditions').focus();
   } 
   if(Payment_Currency=="")
   {
         $('#err_Payment_Currency').html('This field is required');
         $('#err_Payment_Currency').show();
         $('#Payment_Currency').on('keyup', function(){
          $('#err_Payment_Currency').hide();
         });
         flag=0;
         $('#Payment_Currency').focus();
   } 
   if(Order_type=="")
   {
         $('#err_Order_type').html('This field is required');
         $('#err_Order_type').show();
         $('#Order_type').on('keyup', function(){
          $('#err_Order_type').hide();
         });
         flag=0;
         $('#Order_type').focus();
   }
   /*if(Final_price=="")
   {
         $('#err_Final_price').html('This field is required');
         $('#err_Final_price').show();
         $('#Final_price').on('keyup', function(){
          $('#err_Final_price').hide();
         });
         flag=0;
         $('#Final_price').focus();
   }
   if(Quantity=="")
   {
         $('#err_Quantity').html('This field is required');
         $('#err_Quantity').show();
         $('#Quantity').on('keyup', function(){
          $('#err_Quantity').hide();
         });
         flag=0;
         $('#Quantity').focus();
   }*/
   if(Delivery_Date=="")
   {
         $('#err_Delivery_Date').html('This field is required');
         $('#err_Delivery_Date').show();
         $('#Delivery_Date').on('keyup', function(){
          $('#err_Delivery_Date').hide();
         });
         flag=0;
         $('#Delivery_Date').focus();
   } 
   if(Transport=="")
   {
         $('#err_Transport').html('This field is required');
         $('#err_Transport').show();
         $('#Transport').on('keyup', function(){
          $('#err_Transport').hide();
         });
         flag=0;
         $('#Transport').focus();
   } 
   if(Merchandise_Description=="")
   {

         $('#err_Merchandise_Description').html('This field is required');
         $('#err_Merchandise_Description').show();
         $('#Merchandise_Description').on('keyup', function(){
          $('#err_Merchandise_Description').hide();
         });
         flag=0;
         $('#Merchandise_Description').focus();
   } 


   if(flag == 0) {
     return false;
     event.preventDefault();
   } else {
          swal({   
               title: "Are you sure?",   
               text : "You want to send this Order ?",  
               type : "warning",   
               showCancelButton: true,   
               confirmButtonColor: "#8cc63e",  
               confirmButtonText: "Yes",  
               cancelButtonText: "No",   
               closeOnConfirm: false,   
               closeOnCancel: false }, function(isConfirm){   
               if (isConfirm) 
               { 
                  swal("Done!", "Your action successfully performed.", "success");

                  jQuery.ajax({
                    url:site_url+'buyer/order_report/',
                    method:"POST",
                    data  : {
                             Merchandise_Description:Merchandise_Description,
                             Transport:Transport,
                             Delivery_Date:Delivery_Date,
                             Order_type:Order_type,
                             Final_price:Final_price,
                             Quantity:Quantity,
                             Payment_Currency:Payment_Currency,
                             Payment_Conditions:Payment_Conditions,
                             Packing_Weight:Packing_Weight,
                             Trade_confirmed_by:Trade_confirmed_by,
                             seller_id:seller_id,
                             product_name:product_name,
                           },
                    dataType:"json",
                    beforeSend:function()
                    {
                      ajaxindicatorstart();
                    },
                    success:function(response)
                    {
                      $('.msg_div').show();
                      setTimeout(function(){
                        $('.confirm').click();
                        /*location.reload();*/
                        $('#myModal').modal('hide');
                        $('html, body').animate({scrollTop:$('.msg_div').position().top}, 'slow');
                      },500); 
                    },
                    complete:function(response)
                    {
                      ajaxindicatorstop();
                    }
                  });
               } 
               else
               { 
                  swal("Cancelled"); 
                  setTimeout(function(){
                    $('.confirm').click();
                  },500);  
               } 
          });
      }
  });
});
</script>

<!-- DatePiker -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepiker/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/datepiker/1.12.1.js"></script>
<script>
$( function() {
$( "#Delivery_Date" ).datepicker().datepicker("setDate", new Date());
} );
</script>
<!-- End DatePiker -->
