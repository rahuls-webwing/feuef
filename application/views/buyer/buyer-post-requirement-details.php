<?php $this->load->view('buyer/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3><?php if(!empty($getRequirmentsDetail[0]['title'])) { echo ucfirst($getRequirmentsDetail[0]['title']); } else { echo "Not Available" ;} ?></h3>
        </div>
    </div>
</div>
<div class="middel-container" >
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <?php $this->load->view('buyer/left-bar'); ?>
                <div class="col-sm-9 col-md-9 col-lg-9">

                <div class="back-btn-main-block">
                    <!-- <div class="bck-btn">
                        <a href="#"><i class="fa fa-angle-double-left"></i> Back</a>
                    </div> -->
                    <?php if(isset($getRequirmentsDetail[0]['id'])) { $requirment_id = $getRequirmentsDetail[0]['id']; } else { $requirment_id = 0; } ?>
                    <div class="replay-btn">
                        <a class="btn-replay-block" href="javascript:history.back()"><i class="fa fa-reply"></i> back</a>
                        <a class="btn-delete-block" href="<?php echo base_url().'buyer/edit_requirement/'.$requirment_id;?>"><i class="fa fa-pencil-square"></i> Edit</a>
                        <a class="btn-delete-block delete_selected_req" data-req="<?php echo $requirment_id;  ?>" href="javascript:void(0);"><i class="fa fa-trash-o"></i> Delete</a>
                    </div>
                    <div class="clr"></div>
                </div>


                <?php $this->load->view('status-msg'); ?>
                 
                    
                <?php if(isset($getRequirmentsDetail) && sizeof($getRequirmentsDetail) > 0) {?>

                   <!-- success msg -->
                   <div style="display:none;" class="alert-box success alert alert-success alert-dismissible msg_div<?php echo $getRequirmentsDetail[0]['id']; ?>"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button><span>Done:</span> You successfully accepted this offer. you see this requirment in your closed requirment panel.</div>
                   <!-- end success msg -->

                    <div class="requirment_div<?php echo $getRequirmentsDetail[0]['id']; ?>">

                      <div class="col-sm-4 col-md-4 col-lg-4 " >
                        <div class="details-img-block">
                            <?php if(!empty($getRequirmentsDetail[0]['req_image']) && file_exists('images/buyer_post_requirment_image/'.$getRequirmentsDetail[0]['req_image'])){?>
                            <img style="height:162px;width:260px;"  src="<?php echo base_url().'images/buyer_post_requirment_image/'.$getRequirmentsDetail[0]['req_image']; ?>" alt="list_img_1" class="img-responsive list_view_img_1" />
                            <?php } else { ?>
                            <img src="<?php echo base_url().'images/buyer_post_requirment_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                            <?php } ?> 
                        </div>
                      </div>  
                        <div class="account-info-block">
                        <div id="description" class="details-descrip">
                            <div class="descrip-head-block">
                                <?php if(!empty($getRequirmentsDetail[0]['title'])) { echo ucfirst($getRequirmentsDetail[0]['title']); } else { echo "Not Available" ;} ?>
                            </div>
                            <div class="list_up"><span><i class="fa fa-folder-open"></i>
                                </span>
                                    <?php if(!empty($getRequirmentsDetail[0]['category_name'])) { echo substr($getRequirmentsDetail[0]['category_name'], 0 , 50); } else { echo "Not Available"; } ?> ->
                                    <?php if(!empty($getRequirmentsDetail[0]['subcategory_name'])) { echo substr($getRequirmentsDetail[0]['subcategory_name'], 0 , 50); } else { echo "Not Available"; } ?>
                            </div>
                            <div class="list_add"><span><i class="fa fa-map-marker"></i>
                                </span>

                                <?php if(!empty($getRequirmentsDetail[0]['location'])) { echo $getRequirmentsDetail[0]['location']; } else { echo "Not Available"; } ?> 
                            </div>
                            <div class="lsit_text_content">
                            <div class="list_up "><span><i class="fa fa-calendar" aria-hidden="true"></i> Posted Date :
                            </span>
                                 <?php echo date('F j, Y', strtotime($getRequirmentsDetail[0]['created_date']))." at ".date("g:i a", strtotime($getRequirmentsDetail[0]['created_date'])); ?> 
                            </div>
                            </div>
                            <div class="lsit_text_content">
                                <div class="list_up up-list-price"><span> <?php echo CURRENCY; ?>
                                    </span>
                                         <?php if(!empty($getRequirmentsDetail[0]['price'])) { echo $getRequirmentsDetail[0]['price']; } else { echo "Not Available"; } ?> 
                                </div>
                                </div>
                            <div class="descrip-content-block">
                                <?php if(!empty($getRequirmentsDetail[0]['description'])) { echo $getRequirmentsDetail[0]['description']; } else { echo "Not Available"; } ?>
                            </div>
                        </div>
                      </div>

                    </div>   


                <?php
                } else {
                    $this->load->view('no-data-found');
                }?>

                <!-- Offered Sellers -->
                
                <?php if(isset($offered_sellers) && sizeof($offered_sellers) > 0 && isset($getRequirmentsDetail) && sizeof($getRequirmentsDetail) > 0) {
                  $datetime2      = new DateTime(date("Y-m-d H:m:s"));
                  $endTimeStamp   = strtotime(date("Y-m-d H:m:s"));
                  ?>
                    
                    <?php foreach ($offered_sellers as $offerd_seller) { 
                    
                    ?>
                      <?php
                      /* for hours and time count */    
                      $datetime1 = new DateTime($offerd_seller['offer_created_date']);
                      
                      $interval = $datetime1->diff($datetime2);
                      if($interval->format('%h')==0)
                      {
                        $hours="";
                      }
                      else
                      {
                        $hours=$interval->format('%h')." Hours " ;
                      }
                      if($interval->format('%i')==0)
                      {
                        $Minutes="";
                      }
                      else
                      {
                        $Minutes=$interval->format('%i')." Minutes";
                      }
                      if($Minutes == '' && $hours == '')
                      {
                        $aa = $interval->format('%s');
                        $Minutes ="$aa Second";
                      }
                      /* end for hours and time count */   
                      /* for day count */    
                      $startTimeStamp = strtotime($offerd_seller['offer_created_date']);
                      $timeDiff       = abs($endTimeStamp - $startTimeStamp);
                      $numberDays     = $timeDiff/86400;  // 86400 seconds in one day
                      // and you might want to convert to integer
                      $numberDays = intval($numberDays);
                      if($numberDays==0)
                      {
                          $numberDays="";
                      }
                      else
                      {
                          ($numberDays>1)  ? $day='Day' : $day='Days';
                          $numberDays=intval($numberDays).' '.$day;
                      }
                  ?>
                       <div class="search-grey-bx offer_div" >
                        <div class="row">
                            <div class="col-sm-12 col-md-8 col-lg-10   br-right">
                                <div class="going-profile-detail">
                                    <div class="going-pro">
                                        <?php if(!empty($offerd_seller['user_image']) && file_exists('images/seller_image/'.$offerd_seller['user_image'])){?>
                                        <img src="<?php echo base_url().'images/seller_image/'.$offerd_seller['user_image']; ?>" alt="" />
                                        <?php } else { ?>
                                        <img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
                                        <?php } ?> 
                                    </div>
                                    <div class="going-pro-content">
                                        <div style="display:inline-block;" class="profile-name1">
                                            
                                            <?php if(!empty($offerd_seller['name'])) { ?>
                                             <a href="<?php echo base_url().'seller/profile/'.$offerd_seller['offered_seller_id']; ?>"><?php echo  $offerd_seller['name']; ?></a>
                                            <?php  
                                            } else { 
                                            echo "Not Available"; 
                                            } 
                                            ?> 
 
                                        </div>
                                        <div class="premium-text">
                                           <span>Offered in:</span> <?php  echo $numberDays.' '. $hours.' '. $Minutes; ?> ago
                                        </div>
                                        <?php /* 
                                        <div class="sub-project-dec"> Country:
                                             
                                             <?php if(!empty($offerd_seller['country'])) { echo $offerd_seller['country']; } else { echo "Not Available"; } ?>

                                        </div>
                                        */ ?>
                                        <div class="sub-project-dec"> <i class="fa fa-calendar" aria-hidden="true"></i> Offered Date :
                                             
                                             <?php echo date('F j, Y', strtotime($offerd_seller['offer_created_date']))." at ".date("g:i a", strtotime($offerd_seller['offer_created_date'])); ?> 
                                        </div>
                                        <div style="height:auto;" class="more-project-dec">
                                            <div class="content-block-container">
                                                <?php if(!empty($offerd_seller['offer_description'])) { echo $offerd_seller['offer_description']; } else { echo "Not Available"; } ?>
                                            </div>
                                            <div class="clr"></div>
                                            <div class="palm-text"><span><img alt="location icon" src="<?php echo base_url(); ?>front-asset/images/post-location.png"> </span> 
                                               
                                                <?php if(!empty($offerd_seller['city'])) {
                                                echo  $offerd_seller['city']; 
                                                } else { 
                                                echo "Not Available"; 
                                                } ?> 

                                                , 

                                                <?php if(!empty($offerd_seller['country'])) {
                                                echo  $offerd_seller['country']; 
                                                } else { 
                                                echo "Not Available"; 
                                                } ?>

                                            </div>
                                            <div class="clr"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="col-sm-12 col-md-4 col-lg-2">
                                    <div class="rating-profile">
                                        <div class="projrct-prce1" style="margin: 29px 0 12px 8px;"><span><?php echo CURRENCY; ?><?php if(!empty($offerd_seller['offered_price'])) { echo $offerd_seller['offered_price']; } else { echo "Not Available"; } ?> </div>
                                        <div class="" style="">
                                            <button class="msg_btn" type="button" onclick="javascript:jqcc.cometchat.chatWith('<?php echo $offerd_seller['offered_seller_id']; ?>');"> Chat <i class="fa fa-envelope-o"></i></button>                                                  
                                            &nbsp;
                                            <button class="msg_btn accepet_offer" reqid="<?php echo $offerd_seller['requirment_id']; ?>" offerid="<?php echo $offerd_seller['id']; ?>" type="button"> Accept</button>  
                                        </div>
                                        <!-- <div class="rating_profile">
                                            <div class="rating-title1">Rating   <span>:</span> </div>
                                            <div class="rate-t">
                                                <div class="rating-list1"><img alt="img" src="images/rate-star.png"> </div>
                                                (4.8)
                                            </div>
                                        </div>
                                        <div class="rating_profile">
                                            <div class="rating-title1">Review <span>:</span> </div>
                                            <div class="star-rat">9</div>
                                        </div>
                                        <div class="rating_profile">
                                            <div class="rating-title1">Completion Rate<span>:</span> </div>
                                            <div class="star-rat">65%</div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>    
                    <?php } ?>   

                <?php
                } else {
                    $this->load->view('no-data-found');
                }?>    
                <!-- end Offered Sellers -->
                </div>
                <!--pagigation start here-->
                <div style="margin-top:-12px;">
                  <?php echo $this->pagination->create_links(); ?>
                </div>
                <!--pagigation end here-->
            </div>
        </div>
    </div>
</div>
<!-- Offer Modal -->
<div id="myModal" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">CONDITIONS</h4>
        </div>
        <div class="modal-body">

                    <input type="hidden" id="offerd_id" name="offerd_id" >
                    <input type="hidden" id="requirment_id"  name="requirment_id">

                    <div class="mobile-nu-block enquiry">

                            <input type="text" 
                                   name="Merchandise_Description" 
                                   id="Merchandise_Description" 
                                   class="beginningSpace_restrict " 
                                   placeholder="Merchandise Description" />
                                   <div class="error" id="err_Merchandise_Description"></div>
                        <span class="highlight"></span>
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Transport" 
                                   id="Transport" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Transport"/>
                                   <div class="error" id="err_Transport"></div>
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Delivery_Date" 
                                   id="Delivery_Date" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Delivery Date"/>
                                   <div class="error" id="err_Delivery_Date"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Order_type" 
                                   id="Order_type" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Order type"/>
                                   <div class="error" id="err_Order_type"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Payment_Currency"  
                                   id="Payment_Currency" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Payment Currency"/>
                                   <div class="error" id="err_Payment_Currency"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Payment_Conditions" 
                                   id="Payment_Conditions" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Payment Conditions"/>
                                   <div class="error" id="err_Payment_Conditions"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Packing_Weight"  
                                   id="Packing_Weight" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Packing Weight"/>
                                   <div class="error" id="err_Packing_Weight"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Trade_confirmed_by" 
                                   id="Trade_confirmed_by" 
                                   
                                   class="beginningSpace_restrict "  
                                   placeholder="Trade confirmed by"/>
                                   <div class="error" id="err_Trade_confirmed_by"></div> 
                        <span class="highlight"></span>
                        
                    </div>
                     <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Final_price" 
                                   id="Final_price" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Final Price"/>
                                   <div class="error" id="err_Final_price"></div> 
                        <span class="highlight"></span>
                    </div>
                    <div class="mobile-nu-block enquiry">
                            <input type="text" 
                                   name="Quantity" 
                                   id="Quantity" 
                                   
                                   class="beginningSpace_restrict " 
                                   placeholder="Quantity"/>
                                   <div class="error" id="err_Quantity"></div> 
                        <span class="highlight"></span>
                        
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-password changes enq">
                        <button class="change-btn-pass submit_accepet_offer" type="button">Ok</button>
                    </div>
                </div>
            </form>    
        </div>

    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() { 
      $('.close').on('click',function() {
        setTimeout(function(){
            location.reload();
        })
      });
    });
</script>
<!-- End Offer Modal -->
<script type="text/javascript">
$(document).ready(function(){

  $('.accepet_offer').click(function(){

    var offerd_id           = $(this).attr('offerid');
    var requirment_id       = $(this).attr('reqid');
    $('#offerd_id').val(offerd_id);
    $('#requirment_id').val(requirment_id);
    $('#myModal').modal('show');
    return false;
  });

  $('.submit_accepet_offer').click(function(){


    var offerd_id = $('#offerd_id').val();
    var requirment_id  = $('#requirment_id').val();

   var flag =1;
    var Merchandise_Description  = $('#Merchandise_Description').val();
    var Transport                = $('#Transport').val();
    var Delivery_Date            = $('#Delivery_Date').val(); 
    var Order_type               = $('#Order_type').val();
    var Final_price              = $('#Final_price').val();
    var Quantity                 = $('#Quantity').val();
    var Payment_Currency         = $('#Payment_Currency').val();
    var Payment_Conditions       = $('#Payment_Conditions').val();
    var Packing_Weight           = $('#Packing_Weight').val();
    var Trade_confirmed_by       = $('#Trade_confirmed_by').val();



   $('#err_Merchandise_Description').html('');
   $('#err_Transport').html('');
   $('#err_Delivery_Date').html('');
   $('#err_Order_type').html('');
   $('#err_Payment_Currency').html('');
   $('#err_Payment_Conditions').html('');
   $('#err_Packing_Weight').html('');
   $('#err_Trade_confirmed_by').html('');
  
   if(Trade_confirmed_by=="")
   {
         $('#err_Trade_confirmed_by').html('This field is required');
         $('#err_Trade_confirmed_by').show();
         $('#Trade_confirmed_by').on('keyup', function(){
          $('#err_Trade_confirmed_by').hide();
         });
         flag=0;
         $('#Trade_confirmed_by').focus();
   } 
   if(Packing_Weight=="")
   {
         $('#err_Packing_Weight').html('This field is required');
         $('#err_Packing_Weight').show();
         $('#Packing_Weight').on('keyup', function(){
          $('#err_Packing_Weight').hide();
         });
         flag=0;
         $('#Packing_Weight').focus();
   } 
   if(Payment_Conditions=="")
   {
         $('#err_Payment_Conditions').html('This field is required');
         $('#err_Payment_Conditions').show();
         $('#Payment_Conditions').on('keyup', function(){
          $('#err_Payment_Conditions').hide();
         });
         flag=0;
         $('#Payment_Conditions').focus();
   } 
   if(Payment_Currency=="")
   {
         $('#err_Payment_Currency').html('This field is required');
         $('#err_Payment_Currency').show();
         $('#Payment_Currency').on('keyup', function(){
          $('#err_Payment_Currency').hide();
         });
         flag=0;
         $('#Payment_Currency').focus();
   } 
   if(Order_type=="")
   {
         $('#err_Order_type').html('This field is required');
         $('#err_Order_type').show();
         $('#Order_type').on('keyup', function(){
          $('#err_Order_type').hide();
         });
         flag=0;
         $('#Order_type').focus();
   } 
   if(Delivery_Date=="")
   {
         $('#err_Delivery_Date').html('This field is required');
         $('#err_Delivery_Date').show();
         $('#Delivery_Date').on('keyup', function(){
          $('#err_Delivery_Date').hide();
         });
         flag=0;
         $('#Delivery_Date').focus();
   } 
   if(Transport=="")
   {
         $('#err_Transport').html('This field is required');
         $('#err_Transport').show();
         $('#Transport').on('keyup', function(){
          $('#err_Transport').hide();
         });
         flag=0;
         $('#Transport').focus();
   } 
   if(Merchandise_Description=="")
   {

         $('#err_Merchandise_Description').html('This field is required');
         $('#err_Merchandise_Description').show();
         $('#Merchandise_Description').on('keyup', function(){
          $('#err_Merchandise_Description').hide();
         });
         flag=0;
         $('#Merchandise_Description').focus();
   } 


   if(flag == 0) {
     return false;
     event.preventDefault();
   } else {
                swal({   
                 title: "Are you sure?",   
                 text : "You want to accept this offer ?",  
                 type : "warning",   
                 showCancelButton: true,   
                 confirmButtonColor: "#8cc63e",  
                 confirmButtonText: "Yes",  
                 cancelButtonText: "No",   
                 closeOnConfirm: false,   
                 closeOnCancel: false }, function(isConfirm){   
                 if (isConfirm) 
                 { 
                    swal("Done!", "Your action successfully performed.", "success");


                    jQuery.ajax({
                      url:site_url+'buyer/accept_offer/'+requirment_id+'/'+offerd_id,
                      method:"POST",
                      data  : {
                               Merchandise_Description:Merchandise_Description,
                               Transport:Transport,
                               Delivery_Date:Delivery_Date,
                               Order_type:Order_type,
                               Final_price:Final_price,
                               Quantity:Quantity,
                               Payment_Currency:Payment_Currency,
                               Payment_Conditions:Payment_Conditions,
                               Packing_Weight:Packing_Weight,
                               Trade_confirmed_by:Trade_confirmed_by},
                      dataType:"json",
                      beforeSend:function()
                      {
                        ajaxindicatorstart();
                      },
                      success:function(response)
                      {

                        setTimeout(function(){
                          $('.confirm').click();
                          $('#myModal').modal('hide');
                          $('.offer_div').hide();
                          $('.requirment_div'+requirment_id).hide();
                          $('.msg_div'+requirment_id).show();
                          $('html, body').animate({scrollTop:$('.msg_div'+requirment_id).position().top}, 'slow');
                        },500); 
                      },
                      complete:function(response)
                      {
                        ajaxindicatorstop();
                      }
                    });
                 } 
                 else
                 { 
                    swal("Cancelled"); 
                    setTimeout(function(){
                      $('.confirm').click();
                    },500);  
                 } 
             });
   }


     
  });
});
</script>
<script type="text/javascript">
  
  $('.delete_selected_req').click(function(){
        var requirment_id = jQuery(this).data("req");
        swal({   
             title: "Are you sure?",   
             text: "You want to delete this requirement ?",  
             type: "warning",   
             showCancelButton: true,   
             confirmButtonColor: "#8cc63e",  
             confirmButtonText: "Yes",  
             cancelButtonText: "No",   
             closeOnConfirm: false,   
             closeOnCancel: false }, function(isConfirm){   
              if (isConfirm) 
              { 
                     swal("Deleted!", "Your requirement has been deleted.", "success"); 
                           location.href=site_url+"buyer/delete_requirement/"+requirment_id;
              } 
              else
              { 
                     swal("Cancelled", "Your requirement is safe :)", "error");          
                } 
            });
    }); 

</script>     
<!-- DatePiker -->

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepiker/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/datepiker/1.12.1.js"></script>
<script>
$( function() {
$( "#Delivery_Date" ).datepicker().datepicker("setDate", new Date());
} );
</script>

<!-- End DatePiker -->