<?php $this->load->view('buyer/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3>Dashboard</h3>
        </div>
    </div>
</div>
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <!-- <div ng-include='"left-bar.html"'></div> -->
                <?php $this->load->view('buyer/left-bar'); ?>
                <div class="col-sm-9 col-md-9 col-lg-9">
                    <?php $this->load->view('status-msg'); ?>   
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="profile-block">
                                <div class="my-profile-txt">
                                    My Profile <a href="<?php echo base_url().'buyer/edit'; ?>"><i class="fa fa-pencil-square-o"></i></a>
                                </div>
                                <div class="main-block-info">
                                    <div class="profile-pic-block">
                                        <?php if(!empty($this->session->userdata('user_image')) && file_exists('images/'.lcfirst($this->session->userdata('user_type')).'_image/'.$this->session->userdata('user_image'))){?>
                                        <img src="<?php echo base_url().'images/'.lcfirst($this->session->userdata('user_type')).'_image/'.$this->session->userdata('user_image'); ?>" alt="" />
                                        <?php } else { ?>
                                        <img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
                                        <?php } ?> 
                                    </div>
                                    <div class="profile-name">
                                        <?php if(!empty($this->session->userdata('user_name'))) {
                                        echo $this->session->userdata('user_name'); 
                                        } else { 
                                        echo "Not Available"; 
                                        } 
                                        ?> 
                                    </div>
                                    <div class="co-name-block">
                                        <?php if(!empty($this->session->userdata('user_city'))) {
                                        echo  $this->session->userdata('user_city'); 
                                        } else { 
                                        echo "Not Available"; 
                                        } ?> 

                                        , 

                                        <?php if(!empty($this->session->userdata('user_country'))) {
                                        echo  $this->session->userdata('user_country'); 
                                        } else { 
                                        echo "Not Available"; 
                                        } ?>
                                    </div>
                                    <div class="socila-icon-profile">
                                        <!-- <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        </ul> -->
                                    </div>
                                    <div class="content-block-profile">
                                        <?php if(!empty($this->session->userdata('user_address'))) {
                                        echo substr($this->session->userdata('user_address'), 0 , 114).'..';
                                        } else { 
                                        echo "Not Available"; 
                                        } 
                                        ?> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="message-bx-block open_chat">
                                <?php 
                                $this->db->where('read' , '0');
                                $this->db->where('cometchat.to' , $this->session->userdata('user_id'));
                                $get_chat_count  = $this->master_model->getRecordCount('cometchat');
                                if($get_chat_count <= 9){ $chat_cnt = '0'.$get_chat_count; } else if($get_chat_count >= 10) { $chat_cnt = '10+'; }
                                ?>
                                <a href="#">
                                    <span class="head-block-bx">Messages</span>
                                    <div class="mes-icon-block"><i class="fa fa-envelope-o"></i></div>
                                    <span class="message-count-block">
                                        <?php echo $chat_cnt; ?>
                                    </span>
                                    <span class="text-count-block">
                                        New<br/> Messages
                                    </span>
                                    <span class="total-txt-block">
                                        (Total)
                                    </span>
                                </a>
                            </div>
                            <div class="message-bx-block inquir-block-bx">
                                <?php 
                                $this->db->where('tbl_buyer_post_requirement.buyer_id' , $this->session->userdata('user_id'));
                                $this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
                                $this->db->where('tbl_buyer_post_requirement.status <>' , 'Delete');
                                $this->db->where('tbl_buyer_post_requirement.requirment_status' , 'open');
                                $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_buyer_post_requirement.subcategory_id');
                                $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
                                $getRequirments  = $this->master_model->getRecordCount('tbl_buyer_post_requirement');
                                if($getRequirments <= 9){ $requirement_cnt = '0'.$getRequirments; } else if($getRequirments >= 10) { $requirement_cnt = '10+'; }
                                ?>
                                <a href="<?php echo base_url().'buyer/requirments'; ?>">
                                    <span class="head-block-bx">My Requirements</span>
                                    <div class="mes-icon-block"><i class="fa fa-suitcase"></i></div>
                                    <span class="message-count-block">
                                        <?php echo $requirement_cnt; ?>
                                    </span>
                                    <span class="text-count-block">
                                        Posted<br/> Requirements
                                    </span>
                                    <span class="total-txt-block">
                                        (Total)
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <?php 
                            $this->db->where('status' , 'Unblock');
                            $this->db->where('is_read' , 'no');
                            $this->db->where('tbl_buyer_notifications.buyer_id' , $this->session->userdata('user_id'));
                            $get_noti_count  = $this->master_model->getRecordCount('tbl_buyer_notifications');
                            if($get_noti_count <= 9){ $noti_cnt = '0'.$get_noti_count; } else if($get_noti_count >= 10) { $noti_cnt = '10+'; }
                            ?>
                            <div class="message-bx-block notifi-bx-block">
                                <a href="<?php echo base_url().'buyer/notifications'; ?>">
                                    <span class="head-block-bx">Notifications</span>
                                    <div class="mes-icon-block"><i class="fa fa-bell-o"></i></div>
                                    <span class="message-count-block">
                                        <?php echo $noti_cnt; ?>
                                    </span>
                                    <span class="text-count-block">
                                        New<br/> Notifications
                                    </span>
                                    <span class="total-txt-block">
                                        (Total)
                                    </span>
                                </a>
                            </div>
                            <div class="message-bx-block review-rati-block">
                                <?php 
                                $this->db->where('tbl_buyer_primum_membership_for_make_offer_purchase_history.buyer_id' , $this->session->userdata('user_id'));
                                $offerPaymentHistory  = $this->master_model->getRecordCount('tbl_buyer_primum_membership_for_make_offer_purchase_history');
                                if($offerPaymentHistory <= 9){ $off_pay_cnt = '0'.$offerPaymentHistory; } else if($offerPaymentHistory >= 10) { $off_pay_cnt = '10+'; }
                                ?>
                                <a href="<?php echo base_url().'buyer/make_offer_payment_history'; ?>">
                                    <span class="head-block-bx">Offer Transactions</span>
                                    <div class="mes-icon-block"><b><?php echo CURRENCY; ?></b></div>
                                    <span class="message-count-block">
                                        <?php echo $off_pay_cnt; ?>
                                    </span>
                                    <span class="text-count-block">
                                        Offer<br/> Transactions
                                    </span>
                                    <span class="total-txt-block">
                                        (Total)
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <?php

                                $this->db->order_by('tbl_apply_for_order.id' , 'desc');
                                $this->db->select('tbl_apply_for_order.*,tbl_user_master.name');
                                $this->db->where('tbl_apply_for_order.order_buyer_id' , $this->session->userdata('user_id'));
                                $this->db->where('tbl_apply_for_order.order_from !=' , '');
                                $this->db->join('tbl_user_master', 'tbl_user_master.id=tbl_apply_for_order.order_seller_id');
                                $get_order_form = $this->master_model->getRecordCount('tbl_apply_for_order');
                               
                                if($get_order_form <= 9){ $order = '0'.$get_order_form; } else if($get_order_form >= 10) { $order = '10+'; }
                            ?>
                            <div class="message-bx-block current-plan-block">
                                <a href="<?php echo base_url().'buyer/product_order_send'; ?>">  
                                    <span class="head-block-bx">ORDERS</span>
                                    <div class="mes-icon-block"><b><i class="fa fa-outdent" aria-hidden="true"></i></b></div>
                                    <span class="message-count-block">
                                        <?php echo $order; ?>
                                    </span>
                                    <span class="text-count-block">
                                       ORDERS
                                    </span>
                                    <span class="total-txt-block">
                                        (Total)
                                    </span>
                                </a>
                            </div>                                    
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <?php

                                $this->db->order_by('id' , 'desc');
                                $this->db->where('receiver_id' , $this->session->userdata('user_id'));
                                $this->db->where('sender_type' , "Seller");
                                $get_contact_list  = $this->master_model->getRecordCount('tbl_contact_info_in_dashboard');
                               
                                if($get_contact_list <= 9){ $contact = '0'.$get_contact_list; } else if($get_contact_list >= 10) { $contact = '10+'; }
                            ?>
                            <div class="message-bx-block current-plan-block">
                                <a href="<?php echo base_url().'buyer/contact_list'; ?>">  
                                    <span class="head-block-bx">CONTACT'S</span>
                                    <div class="mes-icon-block"><b><i class="fa fa-phone-square" aria-hidden="true"></i></b></div>
                                    <span class="message-count-block">
                                        <?php echo $contact; ?>
                                    </span>
                                    <span class="text-count-block">
                                       CONTACT'S
                                    </span>
                                    <span class="total-txt-block">
                                        (Total)
                                    </span>
                                </a>
                            </div>                                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
        