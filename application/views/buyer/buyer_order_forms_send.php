<style type="text/css">
    .label-important, .badge-important {
        background-color: #fe1010;
    }
</style>
<?php $this->load->view('buyer/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3>Order Forms Received</h3>
        </div>
    </div>
</div>
 
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <?php $this->load->view('buyer/left-bar'); ?>

                <div class="col-sm-12 col-md-8 col-lg-9">
                    <div class="row">
                   
                
                 <div class="row" style="margin-top:3px;">
                   <div class="col-sm-12 col-md-8 col-lg-12" style="padding-right: 0px;">
                    <div class="table-responsive">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-hover fill-head" style="1px solid #ececec;">
                           <tr class="pay_head" >
                              <td style="text-indent:5px;">Seller Name</td>
                              <td style="text-indent:5px;">Requirement Name</td>
                              <td style="text-indent:5px;">Accepted Date</td>
                              <td style="text-indent:5px;">Order Form</td>
                           </tr>
                           <?php 
                           $total=0;
                           if(!empty($get_order_form) || !empty($get_requirement_form))
                           { 
                                 
                                 foreach($get_order_form as $value) 
                                 {
                                      if(!empty($value['order_from']) && file_exists('uploads/invoice/'.$value['order_from']))
                                      {
                                      ?>
                                      <tr class="pay_row_one">
                                        <td style="padding:12px!important;"> <?php echo $value['name']; ?></td>
                                        <td style="padding:12px!important;"> <?php echo $value['product_name']; ?></td>
                                        <td style="padding:12px!important;">  <?php echo date('F j, Y', strtotime($value['order_created_date']))." at ".date("g:i a", strtotime($value['order_created_date'])); ?>  </td>
                                        <td style="padding:12px!important;"> 
                                           <a class="btn-delete-block" href="<?php echo base_url().'uploads/invoice/'.$value['order_from'] ?>" download><i class="fa fa-cloud-download" aria-hidden="true"></i> Order Form</a>
                                        </td>
                                      </tr>
                                <?php  }// end if
                                  }// end foreach

                                foreach($getRequirments as $value) 
                                {

                                  if(!empty($value['order_from']) && file_exists('uploads/invoice/'.$value['order_from']))
                                  {
                                  ?>
                                  <tr class="pay_row_one">
                                    <td style="padding:12px!important;"> <?php echo $value['name']; ?></td>
                                    <td style="padding:12px!important;"> <?php echo $value['title']; ?></td>
                                    <td style="padding:12px!important;">  <?php echo date('F j, Y', strtotime($value['created_date']))." at ".date("g:i a", strtotime($value['created_date'])); ?>  </td>
                                    <td style="padding:12px!important;"> 
                                       <a class="btn-delete-block" href="<?php echo base_url().'uploads/invoice/'.$value['order_from'] ?>" download><i class="fa fa-cloud-download" aria-hidden="true"></i> Order Form</a>
                                    </td>

                                  </tr>
                                  <?php  } // end if
                                } // end foreach
                            }
                            else
                            { ?>
                              <tr class="pay_row_one">
                                <td align="center" style="padding:3px!important;" colspan="5"> 
                                  <div >
                                        <?php  $this->load->view('no-data-found'); ?>
                                  </div>
                                </td>
                              </tr>
                            <?php } ?>
                        </table>
                       </div>
                      </div> 
                    </div>
                  <!--pagigation start here-->
                 <div class="product-pagi-block">
                <?php echo $this->pagination->create_links(); ?>
               </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$("#sort_by").on('change', function(){

$("#sort_by_search").click();

});
</script>