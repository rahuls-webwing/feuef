<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <title><?php echo PROJECT_NAME; ?></title>
      <style type="text/css">
         body{background:#f5f7fa;font-family: Helvetica;}
          @media only screen and (max-width:600px){
              table{width: 100% !important;}
          }
      </style>
   </head>
   <body>
      <table style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color:#f5f7fa;" cellspacing="0" cellpadding="0" align="center" height="100%" border="0" width="100%">
         <tbody>
            <tr>
               <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 10px;width: 100%;border-top: 0;" valign="top" align="center">
                  <table style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;max-width: 600px !important;" cellspacing="0" cellpadding="0" border="0" width="100%">
                     <tbody>
                        <tr>
                           <td style="background:#ffffff;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;" valign="top">
                              <table style="min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
                                 <tbody>
                                    <tr>
                                       <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top">
                                          <table style="min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" align="left" border="0" width="100%">
                                             <tbody>
                                                <tr>
                                                   <td>
                                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                         <tr style="background:#1C9AEA;">
                                                            <td style="padding:15px; color: #858585; font-size:13px;font-family: Helvetica;"><span style="font-size:11px;font-family: Helvetica;color#858585;margin-top:2px;vertical-align:top;"></span>
                                                               <img src="<?php echo base_url(); ?>front-asset/images/logo.png" alt=""/>
                                                            </td>
                                                            <td style="padding:15px;">
                                                               <div style="float:right;"> <a href="#" style="font-family: Helvetica; color: #fff; font-size:13px;text-decoration:none;" >Date: <?php echo date('F j, Y', strtotime(date('Y-m-d'))); ?></a> </div>
                                                            </td>
                                                         </tr>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td style="background:#ffffff none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 0;" valign="top"></td>
                        </tr>
                        <tr>
                           <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top">
                              <table style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;" cellspacing="0" cellpadding="0" align="center" border="0" width="100%">
                                 <tbody>
                                    <tr>
                                       <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top">
                                          <table style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;" cellspacing="0" cellpadding="0" align="right" border="0" width="100%">
                                             <tbody>
                                                <tr>
                                                   <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                      <table style="min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                         <tbody >
                                                            <tr>
                                                               <td  style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top">
                                                                  <table style="max-width: 100%;min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"  cellspacing="0" cellpadding="0" align="left" border="0" width="100%">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td  style=" padding:9px 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: center;display:block;" valign="top">
                                                                              <p style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;"><span style="font-size:22px;color:#333;text-transform:uppercase;">
                                                                                 <strong>  LIVE MARKET OPPORTUNITY  </strong> </span>
                                                                              </p>
                                                                           </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td  style=" padding:9px 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: center;display:block;" valign="top">
                                                                              <p style="padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: Helvetica;font-size: 12px;line-height: 150%;"><span style="font-size:16px;color:#333;">
                                                                                 Hello, you have received a new offer, please check here below the details: </span>
                                                                              </p>
                                                                           </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="padding:20px 20px 35px">
                                                                              <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="border:1px solid #efefef; padding: 10px; width:150px; color:#333; font-weight:600; font-size:13px;">Name</td>
                                                                                       <td style="border:1px solid #efefef; padding: 10px; border-left:none; color: #7b7b7b; font-size:14px;"><?php echo $name;?></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                       <td style="border:1px solid #efefef; padding: 10px; width:150px; color:#333; font-weight:600; font-size:13px; border-top:none; border-bottom:none;">Email</td>
                                                                                       <td style="border:1px solid #efefef; padding: 10px; border-left:none; color: #7b7b7b; font-size:14px;"><?php echo $email;?></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                       <td valighn="top" style="border:1px solid #efefef; padding: 10px; width:150px; color:#333; font-weight:600; font-size:13px;">Requirement Name</td>
                                                                                       <td style="border:1px solid #efefef; padding: 10px; border-left:none; color: #7b7b7b; font-size:14px;"><?php echo $Requirement;?></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                       <td valighn="top" style="border:1px solid #efefef; padding: 10px; width:150px; color:#333; font-weight:600; font-size:13px;">Country</td>
                                                                                       <td style="border:1px solid #efefef; padding: 10px; border-left:none; color: #7b7b7b; font-size:14px;"><?php echo $country;?></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                       <td valighn="top" style="border:1px solid #efefef; padding: 10px; width:150px; color:#333; font-weight:600; font-size:13px;">Price</td>
                                                                                       <td style="border:1px solid #efefef; padding: 10px; border-left:none; color: #7b7b7b; font-size:14px;"><?php echo  CURRENCY.$price;?></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                       <td style="border:1px solid #efefef; padding: 10px; width:150px; color:#333; font-weight:600; font-size:13px; border-top:none;">Message</td>
                                                                                       <td style="border:1px solid #efefef; padding: 10px; border-left:none; border-top:none;"><?php echo $message;?></td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td  style=" padding: 0 18px 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;" valign="top">
                                                                              <p style="margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;"><a href="<?php echo base_url().$url; ?>" style=" background: #1C9AEA ;border-radius: 5px;color: #fff;display: block;margin: 0 auto; max-width: 120px;padding: 10px;text-align: center;text-decoration: none;    width: 100%"> View </a> <span style="color:#0000FF"></span></p>
                                                                           </td>
                                                                        </tr>
                                                                      </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <table id="templateSidebar" style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;" cellspacing="0" cellpadding="0" align="left" border="0" width="200">
                                             <tbody>
                                                <tr>
                                                   <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top"></td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td  style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #fff;padding: 9px 0;" valign="top">
                              <table style="min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
                                 <tbody>
                                    <tr>
                                       <td  style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top">
                                          <table style="max-width: 100%;min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" align="left" border="0" width="100%">
                                             <tbody>
                                                <tr>
                                                   <td  style=" padding: 0 18px 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size:16px;line-height: 150%;text-align: center;" valign="top">
                                                      
                                                      <?php
                                                        $admin_data=$this->master_model->getRecords('admin_login', array('id'=>'1'));
                                                      ?>
                                                      <strong>Our mailing address is:</strong><br/>
                                                      <?php if(isset($admin_data[0]['admin_email'])) { echo $admin_data[0]['admin_email']; } else { echo "--";} ?><br/>
                                                      <!-- <br/>
                                                      Want to change how you receive these emails?<br/>
                                                      You can <a href="#" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color:#00ADD8;font-weight: normal;text-decoration: underline;">update your preferences</a> or <a href="#" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color:#00ADD8;font-weight: normal;text-decoration: underline;">unsubscribe from this list</a><br/> -->
                                                      <br/>
                                                      &nbsp;
                                                   </td>
                                                </tr>
                                             </tbody>
                                             <!-- START OF SUB-FOOTER BLOCK-->
                                             <table width="100%" align="center" bgcolor="#f5f7fa" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                   <td valign="top" width="100%">
                                                      <table align="center" cellpadding="0" cellspacing="0" border="0">
                                                         <tr>
                                                            <td width="100%">
                                                               <table class="table_scale" width="600" bgcolor="#071e30" cellpadding="0" cellspacing="0" border="0" style="border-top: 1px solid #1a3a51;">
                                                                  <tr>
                                                                     <td width="100%">
                                                                        <table width="600" cellpadding="0" cellspacing="0" border="0">
                                                                           <tr>
                                                                              <td class="spacer" width="30"> </td>
                                                                              <td>
                                                                                 <!-- START OF LEFT COLUMN-->
                                                                                 <table class="full" align="center" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                                                    <!-- START OF TEXT-->
                                                                                    <tr>
                                                                                       <td class="center" align="center" style="margin: 0; padding: 10px 0; font-weight:300; font-size:15px ; color:#ffffff; font-family: 'Open Sans',  sans-serif; line-height: 23px;mso-line-height-rule: exactly;"> <span>Copyright © <?php echo date('Y'); ?> <?php echo PROJECT_NAME; ?>, All rights reserved. </span> </td>
                                                                                    </tr>
                                                                                    <!-- END OF TEXT-->
                                                                                 </table>
                                                                                 <!-- END OF LEFT COLUMN-->                                                                                                   
                                                                              </td>
                                                                              <td class="spacer" width="30"> </td>
                                                                           </tr>
                                                                        </table>
                                                                     </td>
                                                                  </tr>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </table>
                                             <!-- END OF SUB-FOOTER BLOCK-->
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
   </body>
</html>