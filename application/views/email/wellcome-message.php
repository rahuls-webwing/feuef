<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />
<title><?php echo PROJECT_NAME; ?></title>
<style type="text/css">
body{background:#fafafa;}
</style>
</head>
<body>
<table style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #FAFAFA;" cellspacing="0" cellpadding="0" align="center" height="100%" border="0" width="100%">
<tbody>
<tr>
<td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 10px;width: 100%;border-top: 0;" valign="top" align="center">
<table style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;max-width: 600px !important;" cellspacing="0" cellpadding="0" border="0" width="100%">
<tbody>
<tr>
<td style="background:#FAFAFA;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding: 9px 0;" valign="top">
<table style="min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
<tbody>
<tr>
<td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top">
<table style="min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" align="left" border="0" width="100%">
<tbody>
<tr>
<td>
<!-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td style="padding:42px 0 8px 0; color: #858585; font-size:13px;font-family: Helvetica;"><span style="font-size:11px;font-family: Helvetica;color#858585;margin-top:2px;vertical-align:top;"></span>
Use this area to offer a short preview of your email's content. 
</td>
<td style="padding:42px 0 8px 0;">
<div style="float:right;"> <a href="#" style="font-family: Helvetica; color: #858585; font-size:13px;" >View this email in your browser</a> </div>
</td>
</tr>
</table> -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr style="background:#1C9AEA;">
<td style="padding:15px; color: #858585; font-size:13px;font-family: Helvetica;"><span style="font-size:11px;font-family: Helvetica;color#858585;margin-top:2px;vertical-align:top;"></span>
<img src="<?php echo base_url(); ?>front-asset/images/logo.png" alt=""/>
</td>
<td style="padding:15px;">
<div style="float:right;"> <a href="#" style="font-family: Helvetica; color: #fff; font-size:13px;text-decoration:none;" >Date: <?php echo date('F j, Y', strtotime(date('Y-m-d'))); ?></a> </div>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td style="text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top">
<img alt="" src="<?php echo base_url(); ?>front-asset/images/1.jpg" style="padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"  align="middle" width="100%"/>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="background:#ffffff none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 0;" valign="top"></td>
</tr>
<tr>
<td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top">
<table id="templateColumns" style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;" cellspacing="0" cellpadding="0" align="center" border="0" width="100%">
<tbody>
<tr>
<td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top">
<table style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;" cellspacing="0" cellpadding="0" align="right" border="0" width="100%">
<tbody>
<tr>
<td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
<table style="min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
<tbody >
<tr>
<td  style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top">
  <table style="max-width: 100%;min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"  cellspacing="0" cellpadding="0" align="left" border="0" width="100%">
     <tbody>
        <tr>
           <td height="20"></td>
        </tr>
        <tr>
           <td>
              <a href="<?php echo base_url(); ?>marketplace" style="text-decoration:none;color:#fff;display: block;padding:12px 18px;margin: 0px 10px; text-align: center; background:#40A240; border-radius: 3px;font-family: Helvetica;text-transform:uppercase;"> Live Market</a>
           </td>
        </tr>
        <tr>
           <td height="10"></td>
        </tr>
        <tr>
           <td>
              <a href="<?php echo base_url(); ?>buyer/dashboard" style="text-decoration:none;color:#fff;display: block;padding:12px 18px;margin: 0px 10px; text-align: center; background:#084C9E; color:#fff;  border-radius: 3px;font-family: Helvetica;text-transform:uppercase;"> Buyer</a>
           </td>
        </tr>
        <tr>
           <td height="10"></td>
        </tr>
        <tr>
           <td>
              <a href="<?php echo base_url(); ?>seller/dashboard" style="text-decoration:none;color:#fff;display: block;padding:12px 18px;margin: 0px 10px; text-align: center; background:#000005; color:#fff;  border-radius: 3px;font-family: Helvetica;text-transform:uppercase;">
              Seller
              </a>
           </td>
        </tr>
        <tr>
           <td height="40"></td>
        </tr>
        <tr>
           <td  style="padding: 0 18px 9px 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;" valign="top">
              <h1 style="display: block;margin: 0;padding: 0;color: #202020;font-family: Helvetica;font-size: 22px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;">Business Network and Live Market</h1>
              <p style="margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;"><span style="font-size:14px">Are you a buyer?&nbsp;Are you a seller?</span></p>
              <p style="margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;">Merqantis is a E-Marketplace providing solutions to connect buyers and sellers of Foods and Beverages. We can guarantee you a great experience on our online platform where you will be able to find the best contacts to start trading goods.</p>
              <p style="margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;"><strong>Dashboard</strong></p>
              <p style="margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;">Our dashboard permits you to manage your stocks, your profits, your contacts and even your live products<br/>
                 &nbsp;
              </p>
              <p style="margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;"><strong>Live Market</strong></p>
              <p style="margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;">Bring and show your products to the world market players and close new businesses</p>
              <p style="margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;"><br/>
                 &nbsp;
              </p>
              <p style="margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;">If you need inspiration please follow our blog <span style="color:#0000FF"><u><a href="<?php echo base_url(); ?>blogs"> Market News </a></u></span></p>
           </td>
        </tr>
        <tr>
           <td height="20"></td>
        </tr>
     </tbody>
  </table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table id="templateSidebar" style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;" cellspacing="0" cellpadding="0" align="left" border="0" width="200">
<tbody>
<tr>
<td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td  style="background:#FAFAFA none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 2px solid #EAEAEA;border-bottom: 0;padding: 9px 0;" valign="top">
<table style="min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
<tbody>
<tr>
<td style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top" align="center">
<table style="min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
<tbody>
<tr>
<td style="padding:0 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" align="center">
<table style="min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"  cellspacing="0" cellpadding="0" border="0" width="100%">
<tbody>
<tr>
<td style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top" align="center">
  <table style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" align="center" border="0">
     <tbody>
        <tr>
           <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top" align="center">
              <table style="display: inline;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" align="left" border="0">
                 <tbody>
                    <tr>
                       <td style="padding:0 10px 9px 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"  valign="top">
                          <table style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
                             <tbody>
                                <tr>
                                   <td style="padding: 5px 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="middle" align="left">
                                      <table style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" align="left" border="0" width="">
                                         <tbody>
                                            <tr>
                                               <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="middle" align="center" width="30">
                                                  <a href="www.twitter.com" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img src="<?php echo base_url(); ?>front-asset/images/emailer-twitter.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="" height="30" alt="" width="30"/></a>
                                               </td>
                                            </tr>
                                         </tbody>
                                      </table>
                                   </td>
                                </tr>
                             </tbody>
                          </table>
                       </td>
                    </tr>
                 </tbody>
              </table>
              <table style="display: inline;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" align="left" border="0">
                 <tbody>
                    <tr>
                       <td style="padding:0 10px 9px 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"  valign="top">
                          <table style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
                             <tbody>
                                <tr>
                                   <td style="padding:5px 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="middle" align="left">
                                      <table style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" align="left" border="0" width="">
                                         <tbody>
                                            <tr>
                                               <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="middle" align="center" width="30">
                                                  <a href="www.facebook.com" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img alt="" src="<?php echo base_url(); ?>front-asset/images/emailer-fb.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="" height="30" width="30"/></a>
                                               </td>
                                            </tr>
                                         </tbody>
                                      </table>
                                   </td>
                                </tr>
                             </tbody>
                          </table>
                       </td>
                    </tr>
                 </tbody>
              </table>
              <table style="display: inline;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" align="left" border="0">
                 <tbody>
                    <tr>
                       <td style="padding: 0 0 9px 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"  valign="top">
                          <table style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
                             <tbody>
                                <tr>
                                   <td style="padding: 5px 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="middle" align="left">
                                      <table style="border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" align="left" border="0" width="">
                                         <tbody>
                                            <tr>
                                               <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="middle" align="center" width="30">
                                                  <a href="<?php echo base_url(); ?>" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img alt="" src="<?php echo base_url(); ?>front-asset/images/emailer-link.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" class="" height="30" width="30"/></a>
                                               </td>
                                            </tr>
                                         </tbody>
                                      </table>
                                   </td>
                                </tr>
                             </tbody>
                          </table>
                       </td>
                    </tr>
                 </tbody>
              </table>
           </td>
        </tr>
     </tbody>
  </table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table style="min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;table-layout: fixed !important;" cellspacing="0" cellpadding="0" border="0" width="100%">
<tbody>
<tr>
<td style="min-width: 100%;padding: 10px 18px 25px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
<table style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EEEEEE;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
<tbody>
<tr>
<td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
<span></span>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table style="min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
<tbody>
<tr>
<td  style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top">
<table style="max-width: 100%;min-width: 100%;border-collapse: collapse;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" cellspacing="0" cellpadding="0" align="left" border="0" width="100%">
<tbody>
<tr>
<td  style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: center;" valign="top">
<em>Copyright &copy; *|<?php echo date('Y'); ?>|* *|<?php echo PROJECT_NAME; ?>|*, All rights reserved.</em><br/>
<br/>
<br/>
<strong>Our mailing address is:</strong><br/>
*|<a href="<?php echo base_url(); ?>"> www.merqantis.com </a>|*<br/>
<br/>
<!-- Want to change how you receive these emails?<br/>
You can <a href="#" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #656565;font-weight: normal;text-decoration: underline;">update your preferences</a> or <a href="#" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #656565;font-weight: normal;text-decoration: underline;">unsubscribe from this list</a><br/>
<br/> -->
&nbsp;
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>