<style>.footer-col-block {display: none;}.copyright-block {margin-top: 0;}
.log-social li a {
    background: #485a96;
    border: 1px solid #485a96;
    color: #fff;
    display: block;
    font-size: 18px;
    padding-top: 5px;
    height: 40px;
    transition: all 0.5s ease;
    border-radius: 3px;
    /* margin-right: 5px; */
}

a#facebook_login {
    background-color: blue;
    padding: 17px;
    border-radius: 67px;
    margin-top: 174px !important;
    top: 102px !important;
    color: white;
}
button.log-btn.g-signin.google-btn {
    padding: 6px;
    margin-right: 273px !important;
    float: right;
}
</style>
<div class="middle-login-content">
    <div class="container">
        <div class="">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <?php $this->load->view('status-msg'); ?>
               <div ng-controller="SignupCntrl">
               <!--    <form  class="" name="signupUserForm" novalidate ng-submit="signupUserForm.$valid && storeSignup();"> -->
                  <form  class="" name="signupUserForm" method="post">
                  <div id="success"></div>
               <div id="error"></div>
                      <div class="login-form-block sign-up-form-block">
                          <div class="radio-btn-block">
                              <div class="radio-btns">
                                  <div class="radio-btn">
                                      <input
                                             type="radio" 
                                             value="Buyer" 
                                             id="f-option" 
                                             ng-required="true"
                                             name="type">

 <!-- <input type="radio" name="type"  id="f-option"  value="Buyer" checked="checked"/>Male &nbsp; <input type="radio" name="gender" value="Female"/>Female -->
                                      <label for="f-option">SignUp as a Buyer</label>

                                      <div for="f-option" class="check ng-untouched ng-valid ng-not-empty ng-dirty ng-valid-parse"></div>
                                  </div>
                                  <div class="radio-btn">
                                      <input    
                                             type="radio" 
                                             value="Seller"  
                                             id="s-option"
                                            
                                             name="type">

                                      <label for="s-option">SignUp as a Seller</label>
                                      <div  class="check">
                                          <div class="inside"></div>
                                      </div>
                                  </div>

                                  <!-- err msg -->
                                  <div style="color:red;" id="err_type"></div>
                                  <div class="clr"></div>

                              </div>
                          </div>
                          <div class="row">
                              <div class="col-sm-12 col-md-12 col-lg-6">
                                  <div class="mobile-nu-block input-first" ng-class="{ 'has-error': signupUserForm.name.$touched 
                                  && signupUserForm.name.$invalid }">
                                      <input autocomplete="off"  class="beginningSpace_restrict  num_restrict" 
                                             type="text" 
                                             name="name" 
                                              
                                             />

                                      <span class="highlight"></span>
                                     <div style="color:red;" id="err_name"></div>
                                      <label>First-Name</label>
                                  </div>
                              </div>


                              <div class="col-sm-12 col-md-12 col-lg-6">
                                  <div class="mobile-nu-block input-first" ng-class="{ 'has-error': signupUserForm.lname.$touched 
                                  && signupUserForm.lname.$invalid }">
                                      <input autocomplete="off"  class="beginningSpace_restrict  num_restrict" 
                                             type="text" 
                                             name="lname" 
                                             ng-model="user.lname"  
                                              />

                                      <span class="highlight"></span>
                                    <div style="color:red;" id="err_lname"></div>
                                      <label>Last-Name</label>
                                  </div>
                              </div>
                              

                              <div class="col-sm-12 col-md-12 col-lg-6">
                                  <div class="mobile-nu-block input-first" ng-class="{ 'has-error': signupUserForm.email.$touched 
                                  && signupUserForm.email.$invalid }">
                                      <input  
                                             type="email" 
                                             name="email" 
                                             ng-model="user.email" 
                                            />
                                            <div style="color:red;" id="err_email"></div>
                                      <span class="highlight"></span>
                                      
                                      <label>Email</label>
                                  </div>
                              </div>
                              <div class="col-sm-12 col-md-12 col-lg-6">
                                  <div class="mobile-nu-block input-first" ng-class="{ 'has-error': signupUserForm.mobile.$touched 
                                  && signupUserForm.mobile.$invalid }">
                                      <input autocomplete="off"  class="beginningSpace_restrict  allowOnlyFourteen" 
                                             type="text" 
                                             name="mobile" 
                                             
                                             ng-model="user.mobile" 
                                              />
                                                  <div style="color:red;" id="err_mobile"></div>
                                      <span class="highlight"></span>
                                      
                                      <label>Mobile number</label>
                                  </div>
                              </div>
                              <div class="col-sm-12 col-md-12 col-lg-6">
                                  <div class="mobile-nu-block input-first" 
                                   ng-class="{ 'has-error': signupUserForm.address.$touched
                                   && signupUserForm.address.$invalid }">
                                      <input autocomplete="off"  class="beginningSpace_restrict " 
                                             type="text" name="address" 
                                             placeholder="address" 
                                             id="address" 
                                             ng-model="user.address"  
                                             
                                             ng-autocomplete
                                             details="obj_autocomplete"
                                              />
                                                  <div style="color:red;" id="err_address"></div>
                                      <span class="highlight"></span>
                                     
                                      <label>Address</label>
                                  </div>
                              </div>
                              
                              <div class="geo-details" >
                                  <div class="col-sm-12 col-md-12 col-lg-6">
                                      <div class="mobile-nu-block input-first" ng-class="{ 'has-error': signupUserForm.city.$touched 
                                      && signupUserForm.city.$invalid }">
                                          <input autocomplete="off"  
                                              class="beginningSpace_restrict "   
                                              type="text" 
                                              name="city" 
                                              ng-model="user.city" 
                                              data-geo="administrative_area_level_2" 
                                               />
                                                   <div style="color:red;" id="err_city"></div>
                                          <span class="highlight"></span>
                                         
                                          <label>City</label>
                                      </div>
                                  </div>

                                  <input type="hidden" name="latitude"  id="latitude" ng-model="user.latitude" placeholder="latitude">
                                  <input type="hidden" name="longitude" id="longitude" ng-model="user.longitude" placeholder="longitude">
                                  
                                  <div class="col-sm-12 col-md-12 col-lg-6">
                                      <div class="mobile-nu-block input-first" ng-class="{ 'has-error': signupUserForm.zipcode.$touched && signupUserForm.zipcode.$invalid }">
                                          <input autocomplete="off"  class="beginningSpace_restrict " 
                                                 type="text" 
                                                 name="zipcode" 
                                                 ng-model="user.zipcode"  
                                                 data-geo="postal_code"  
                                                 />
                                          <span class="highlight"></span>
                                              <div style="color:red;" id="err_zipcode"></div>
                                          <label>zipcode</label>
                                      </div>
                                  </div>    
                                  <div class="col-sm-12 col-md-12 col-lg-6">
                                      <div class="mobile-nu-block input-first" ng-class="{ 'has-error': signupUserForm.state.$touched && signupUserForm.state.$invalid }">
                                          <input autocomplete="off"  class="beginningSpace_restrict " 
                                                 type="text" 
                                                 name="state" 
                                                 data-geo="administrative_area_level_1"
                                                 ng-model="user.state"  
                                                 />
                                          <span class="highlight"></span>
                                              <div style="color:red;" id="err_state"></div>
                                         <!--  <div class="error-new-block" ng-messages="signupUserForm.state.$error" ng-if="signupUserForm.$submitted || signupUserForm.state.$touched">
                                              <div>
                                                  <div class="err_msg_div" style="display:none;">
                                                  
                                                  <p ng-message="required"    class="error">  Please enter state Name</p>
                                                  
                                                  </div>
                                              </div>
                                              <script type="text/javascript">
                                                      $(document).ready(function(){
                                                        setTimeout(function(){
                                                          $('.err_msg_div').removeAttr('style');
                                                        },200);
                                                      });
                                                  </script>
                                          </div> -->
                                          <label>state</label>
                                      </div>
                                  </div>    
                                  <div class="col-sm-12 col-md-12 col-lg-6">
                                      <div class="mobile-nu-block input-first" ng-class="{ 'has-error': signupUserForm.country.$touched && signupUserForm.country.$invalid }">
                                          <input autocomplete="off"  class="beginningSpace_restrict " 
                                                 
                                                 type="text" 
                                                 name="country" 
                                                 data-geo="country"
                                                 ng-model="user.country"  
                                                 />
                                          <span class="highlight"></span>
                                              <div style="color:red;" id="err_country"></div>
                                          <!-- <div class="error-new-block" ng-messages="signupUserForm.country.$error" ng-if="signupUserForm.$submitted || signupUserForm.country.$touched">
                                              <div>
                                                  <div class="err_msg_div" style="display:none;">
                                                  
                                                  <p ng-message="required"    class="error">  Please enter Country Name</p>
                                                  
                                                  </div>
                                              </div>
                                              <script type="text/javascript">
                                                      $(document).ready(function(){
                                                        setTimeout(function(){
                                                          $('.err_msg_div').removeAttr('style');
                                                        },200);
                                                      });
                                                  </script>
                                          </div> -->
                                          <label>Country</label>
                                      </div>
                                  </div>                              
                              </div>
                              
                              <div class="col-sm-12 col-md-12 col-lg-6">
                                  <div class="mobile-nu-block input-first" ng-class="{ 'has-error': signupUserForm.pwd.$touched && signupUserForm.pwd.$invalid }">
                                      <input autocomplete="off"  class="beginningSpace_restrict CopyPast_restrict" 
                                             type="password" 
                                             name="pwd" 
                                             ng-model="user.pwd" 
                                             
                                             />
                                      <span class="highlight"></span>
                                          <div style="color:red;" id="err_pwd"></div>
                                     <!--  <div class="error-new-block" ng-messages="signupUserForm.pwd.$error" ng-if="signupUserForm.$submitted || signupUserForm.pwd.$touched">
                                          <div>
                                          <div class="err_msg_div" style="display:none;">
                                              
                                              <p ng-message="required"    class="error">  Please enter Password</p>
                                              <p ng-message="minlength"   class="error">  Please enter at least six character</p>
                                              <p ng-message="maxlength"   class="error">  Password to long</p>
                                              <p ng-message="pwd"         class="error">  Password is not valid</p>
                                              
                                              </div>
                                          </div>
                                          <script type="text/javascript">
                                                  $(document).ready(function(){
                                                    setTimeout(function(){
                                                      $('.err_msg_div').removeAttr('style');
                                                    },200);
                                                  });
                                              </script>
                                          </div> -->
                                      <label>Password</label>
                                  </div>
                              </div>

                              <div class="col-sm-12 col-md-12 col-lg-6">
                                  <div class="mobile-nu-block input-first" ng-class="{ 'has-error': signupUserForm.cmf_pwd.$touched 
                                  && signupUserForm.cmf_pwd.$invalid }">
                                      <input autocomplete="off"
                                             class="beginningSpace_restrict CopyPast_restrict"
                                             type="password" 
                                             name="cmf_pwd" 
                                             ng-model="user.cmf_pwd"
                                             match-password="pwd" 
                                             ng-minlength="5" 
                                             ng-maxlength="40" 
                                              />
                                      <span class="highlight"></span>
                                          <div style="color:red;" id="err_cmf_pwd"></div>
                                      <!-- 
                                      <div class="error-new-block"  ng-messages="signupUserForm.cmf_pwd.$error" ng-if="signupUserForm.$submitted || signupUserForm.cmf_pwd.$touched">
                                          <div>
                                          <div class="err_msg_div" style="display:none;">
                                              <p ng-message="required"    class="error">  Please enter confirm password</p>
                                              <p ng-message="minlength"   class="error">  Please enter at least six character</p>
                                              <p ng-message="maxlength"   class="error">  Password to long</p>
                                              <p ng-message="pwd"         class="error">  Password is not valid</p>
                                              <p id="err_cmpass"          class="error"></p>
                                          </div>

                                          </div>
                                          <script type="text/javascript">
                                              $(document).ready(function(){
                                                setTimeout(function(){
                                                  $('.err_msg_div').removeAttr('style');
                                                },200);
                                              });
                                          </script>
                                          </div> -->
                                      <label>Confirm Password</label>
                                  </div>
                              </div>

                              <div class="col-sm-12 col-md-12 col-lg-6">

                                  <li><a href="javascript:void(0);" id="facebook_login" tabindex="11"><i class="fa fa-facebook"></i> Facebook</a></li>
                                  <li><button class="log-btn g-signin google-btn"
                                            data-scope="https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email "
                                            data-requestvisibleactions="http://schemas.google.com/AddActivity"
                                            data-clientId="1051154865069-da2p0u9o8rcmehebtblf8t3os9gomisf.apps.googleusercontent.com"
                                            data-accesstype="offline"
                                            data-callback="mycoddeSignIn"
                                            data-theme="dark"
                                            data-cookiepolicy="single_host_origin">
                                  <i class="fa fa-google-plus"></i> Google+
                                 </button></li>    
                              </div>
                          </div>

                          <div class="check-bx">
                                  <input autocomplete="off"
                                  ng-class="{ 'has-error': signupUserForm.TandC.$touched 
                                  && signupUserForm.name.$invalid }" 
                                         class="css-checkbox" 
                                         ng-messages="signupUserForm.TandC.$error" 
                                         id="radio5" 
                                         name="TandC" 
                                         ng-model="user.TandC" 
                                        
                                         type="checkbox">
                                  <label class="css-label radGroup2" for="radio5">I agree to <?php echo PROJECT_NAME; ?>  Terms & Conditions,  Privacy Policy and  Marketplace Rules.</label>
                                  <!-- <div class="error-new-block respo-manage-error" ng-messages="signupUserForm.TandC.$error" ng-if="signupUserForm.$submitted || signupUserForm.TandC.$touched">
                                  <div>
                                  <div class="err_msg_div" style="display:none;">
                                      <p ng-message="required"    class="error">  
                                         Please agree to terms and conditions.
                                      </p>
                                      </div>
                                  </div>
                                  <script type="text/javascript">
                                      $(document).ready(function(){
                                        setTimeout(function(){
                                          $('.err_msg_div').removeAttr('style');
                                        },200);
                                      });
                                  </script>
                                  </div> -->
                          </div>
                          <div class="btn-block-main-login">
                              <div class="btn-login-block">
                                  <button class="login-btn"  name="submit"  id="submit" value="Submit" type="submit">Signup</button>
                              </div>
                              <div class="clr"></div>
                          </div>
                          <div class="login-content-block">
                              You have already an Account? <a href="<?php echo base_url(); ?>login">login Now!</a>
                          </div>
                      </div>
                  </form>
               </div> 
             </div>
            <div class="clr"></div>
        </div>
    </div>
</div>


<script>
 $('#submit').click(function(){
    var type                = $("input[name='type']:checked").val();
    var name              = $('#name').val();
    var lname               = $('#lname').val();
     var email               = $('#email').val();
    var mobile                  = $('#mobile').val();
    var address              = $('#address').val();
    var city               = $('#city').val();
    var state                  = $('#state').val();
     var zipcode              = $('#zipcode').val();
    var pwd               = $('#pwd').val();

    var cmf_pwd                 = $('#cmf_pwd').val();
      var country                 = $('#country').val();
      var baseUrl                = "<?php echo base_url(); ?>"
        var error = false;
if($.trim(name)== "")
      {
         $('#name').val('');
                 
         $('#err_name').html('Please enter first name');
         
         $('#name').focus();
         
      }

if (!$('input[name=type]:checked').val() ) {
 $('#type').val('');
                 
         $('#err_type').html('Please select type of user');
         
         $('#type').focus();     

}
 if($.trim(lname)== "")
      {
         $('#lname').val('');
                
         $('#err_lname').html('Please enter last name');
        
         $('#lname').focus();
       
      }

      if($.trim(zipcode)== "")
      {
         $('#zipcode').val('');
                
         $('#err_zipcode').html('Please enter Zip code');
        
         $('#zipcode').focus();
       
      }
      if($.trim(mobile)=="")
      {
         $('#mobile').val('');
                 
         $('#err_mobile').html('Please enter Mobile Number');
        
         $('#mobile').focus();
        
      }
      if($.trim(address)=="")
      {
         $('#address').val('');
                 
         $('#err_address').html('Please enter Address');
        
         $('#address').focus();
        
      }
       if($.trim(city)== "")
      {
         $('#city').val('');
                
         $('#err_city').html('Please enter City');
        
         $('#city').focus();
       
      }
      if($.trim(state)=="")
      {
         $('#state').val('');
                 
         $('#err_state').html('Please enter State');
        
         $('#state').focus();
        
      }
      if($.trim(country)=="")
      {
         $('#country').val('');
                 
         $('#err_country').html('Please enter Country');
        
         $('#country').focus();
        
      }
    if($.trim(email) == "")
      {
         $('#email').val('');
                 
         $('#err_email').html('Please enter Email ID');
        
         $('#email').focus();
        
      }
     /* else if(!email_filter.test(email))
     {
       
        $('#err_email').html('Please enter valid email address');
                 
        $('#email').focus();
       
   
     }*/

if($.trim(pwd) == "")
      {
         $('#pwd').val('');
               
         $('#err_pwd').html('Please enter password');
        
         $('#pwd').focus();
      
      }

       /*else if(pwd.length<6)
      {
        $('#pwd').val('');         
         $('#err_pwd').html('Please enter minimum 6 digit password');
          $('#pwd').focus();
         
      }*/
      if($.trim(cmf_pwd) == "")
      {
         $('#cmf_pwd').val('');
               
         $('#err_cmf_pwd').html('Please enter Confirm password');
        
         $('#cmf_pwd').focus();
      
      }
       $.ajax({
      url:baseUrl+"/signup/register",
      
      type:'POST',
      data: {name:name,lname:lname,email:email,pwd:pwd,type:type,mobile:mobile,city:city,state:state,country:country},
      dataType:'json',
        success:function(response)
        {
            /*alert(response.status);
            return;*/
           if(response.status == "success") {
            $("#error").html();
            $("#success").html(response.msg).attr('style' , 'color:green');
           // $(".unfav").attr('style' , 'color:#ef5025');
          } 
          if(response.status == "error") {
            $("#success").html();
            $("#error").html(response.msg).attr('style' , 'color:red');
           // $(".unfav").attr('style' , 'color:#ef5025');
          } 
          else 
          {
            ajaxindicatorstop();
            $("#success").html();
            $("#error").html(response.msg).attr('style' , 'color:red');;
          }
   
          return true;
   
        }
   });


     }); 
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLKCmJEIrOrzdRklZHYer8q9qx2XLJ4Vs&sensor=false&libraries=places"></script>

<!-- Facebook Login Starts Here  -->
  <script type="text/javascript">

     /*Load Facebook Base URL*/
    window.fbAsyncInit = function() {
        //Initiallize the facebook using the facebook javascript sdk
        FB.init({
            appId: '<?php $this->config->load("facebook"); echo $this->config->item("appID");?>', // App ID 
            // appId:'963588753682463',
            cookie: true, 
            status: true, 
            xfbml: true, 
            oauth: true 
        });
    };
    //Read the baseurl from the config.php file
    (function(d) {

        var js, id = 'facebook-jssdk',
            ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));

    jQuery('#facebook_login').click(function(d) {
      d.preventDefault();
        
        FB.login(function(response) {

          console.log(response.authResponse);
            if (response.authResponse) {
                FB.api('/me', 'get', {
                    fields: 'email,first_name,last_name'
                }, function(response) {

                //  alert('response'); die;
              //  console.log(response);

                    var email = response.email;
                    var name  = response.first_name;
                    var lname = response.last_name;

                   // console.log(email);
                  //alert(email); die;

                    if(email==undefined || email=="")
                    {
                        alert(lang.err_login_1);
                        return false;
                    }

                    FB.api('/me/picture?type=normal', function(response) {
                        var site_url = "<?php echo base_url(); ?>";
                        //var datastr = "email="+email+"&name="+name+"&lname="+lname;

                        //alert(email); return false;
                        jQuery.ajax({
                            url: site_url + 'signup/fb_login',
                            type: 'POST',
                            data: { email : email , name : name , lname : lname },
                            //dataType:'json',
                            success: function(response) {

                              alert(response);
                              return false;

                              if ($.trim(response.result) == "registration_success") 
                                {
                                    window.location.href = site_url+'signup/update';
                                }
                                else if($.trim(response.result) == 'registration_error')
                                {
                                    $('#error_div').text(response.message);
                                    $('#error_div').show();
                                    $('#error_div').focus();
                                }
                                else if($.trim(response.result) == "error")
                                {
                                     $('#error_div').text(response.message);
                                     $('#error_div').show();
                                }
                                else if($.trim(response.result) == 'login_success')
                                {
                                    window.location.href = site_url+'Signup/dashboard';
                                }
                                return false;
                            }
                        });
                    });
                    return false;
                });
            }
        }, {
            scope: 'public_profile,email'
        }); 
    });
</script>

<script type="text/javascript">
var gpclass = (function(){
    
    //Defining Class Variables here
    var response = undefined;
    return {
        //Class functions / Objects
        
        mycoddeSignIn:function(response){
          if (response['status']['signed_in'] && response['status']['method'] == 'PROMPT') {
                  // User clicked on the sign in button. Do your staff here.
                  gapi.client.load('plus','v1',this.getUserInformation);
              }
            // The user is signed in
            else if (response['access_token']) {
            
                //Get User Info from Google Plus API
                //gapi.client.load('plus','v1',this.getUserInformation);
                
            } else if (response['error']) {
                // There was an error, which means the user is not signed in.
                //alert('There was an error: ' + authResult['error']);
            }
        },
        
        getUserInformation: function(){
            var request = gapi.client.plus.people.get( {'userId' : 'me'} );
            var rdo_val = $('input[name="customer_type1"]:checked').val();
            request.execute( function(profile) {
                var email = profile['emails'].filter(function(v) {
                    return v.type === 'account'; // Filter out the primary email
                })[0].value;
                var fName = profile.displayName;
                var datastr="&email="+email+"&name="+fName;
                if(email!="" && fName!="")
                {
                    $.ajax({
                          url:site_url+'user/gplogin',
                          type:'POST',
                          data:datastr,
                          dataType:'json',
                          success:function(response)
                          {
                            if (response.result == "registration_success") 
                            {
                                $('#success_div_comm').text(response.message);
                                $('#success_div_comm').show();
                                $('#error_div').hide();
                                window.location.href = site_url+'user/dashboard';
                                //window.location.reload();
                            }
                            else if(response.result == 'registration_error')
                            {
                                $('#error_div').text(response.message);
                                $('#error_div').show();
                                $('#error_div').focus();
                                $('#success_div').hide();
                                //window.location.href = site_url;
                                //window.location.reload();
                            }
                            else if(response.result == "error")
                            {
                                 //fbLogoutUser_alt();
                                 $('#error_div1').text(response.message);
                                 $('#error_div1').show();
                                 $('#error_div1').focus();
                                 $('#success_div').hide();
                                 //window.location.href = site_url;
                                 //window.location.reload();
                            }
                            else if(response.result == 'login')
                            {
                                $('#success_div').text(response.message);
                                $('#success_div').show();
                                $('#error_div').hide();
                                window.location.href = site_url+'user/dashboard';
                                //$('#logout_url').attr('href',response.logout_url);
                                //window.location.href = site_url;
                                 
                            }
                          }
                    });
                }
                
            });
        }
    
    }; //End of Return
    })();
    
    function mycoddeSignIn(gpSignInResponse){
        gpclass.mycoddeSignIn(gpSignInResponse);
    }
function onStarted(args){
  console.log("started");
  console.log(args);
}
function onEnded(args){
  console.log("ended");
  console.log(args);
}
function onCallback(args){
  console.log("callback");
  console.log(args);
}
</script>
<!-- Google Login Script Ends Here -->