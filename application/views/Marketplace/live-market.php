<script type="text/javascript" src="<?php echo base_url() ?>front-asset/js/jquery.simplyscroll.js"></script>
<script type="text/javascript">
(function($) {
	$(function() {
		$("#scroller").simplyScroll({orientation:'vertical',customClass:'vert'});
        $("#scroller1").simplyScroll({orientation:'vertical',customClass:'vert'});
	});
})(jQuery);
</script>


<!-- Vertical Slider Trending -->
<script type="text/javascript" src="<?php echo base_url() ?>front-asset/js/jquery.bxslider.js"></script>    
<!-- Vertical Slider Trending End -->


<div class="page-head-block">
    <div class="page-head-overlay"></div>
    <div class="container">
        <div class="head-txt-block">
            Live Market
        </div>
    </div>
</div>
<div class="middel-container">
    <div class="inner-content-block live-market-inner">
        <div class="white-blokc-live-market">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-8 col-lg-7 filter-main-block">
                        
                    <form id="serach_critearea" action="<?php echo base_url().'marketplace'; ?>" method="GET">
                        <div class="main-block-input">
                            <div class="input-block live-filter-block">
                                <input name="location" id="location" value="<?php  if(isset($_REQUEST['location']) && $_REQUEST['location'] != ""){ echo $_REQUEST['location']; } ?>" placeholder="Enter the location" type="text">
                                <button class="search-btn-live" type="submit"><img src="<?php echo base_url() ?>front-asset/images/blog-search-icon.png" alt="" /> </button>
                            </div>                    
                            <div class="select-bock-container live-filter-block">
                                <select name="category" id="category">
                                <option value="">--Select a category--</option>
                                <option <?php  if(isset($_REQUEST['category']) &&
                                        $_REQUEST['category'] == ""){ echo 'selected'; } ?> value="">--All Category--</option>
                                <?php foreach ($getCat as $cate_value) {?>
                                    <option <?php  if(isset($_REQUEST['category']) && $_REQUEST['category'] != "" &&
                                        $_REQUEST['category'] == $cate_value['category_id']){ echo 'selected'; } ?> value="<?php echo $cate_value['category_id']; ?>"><?php echo $cate_value['category_name']; ?></option>
                                <?php } ?>      
                                </select>
                            </div>
                            <div class="btn-go-live">
                                <button class="live-go-btn" type="submit">Go!</button>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </form>    
                    </div>
                </div>                
            </div>
        </div>
        <div class="container">
            <div class="row">                                             
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="last-reques-block">
                        LATEST REQUIREMENTS 
                    </div>
                    <div class="main-table-market">
                        <div class="scroller-box">
                                        <div class="title-head-block">
                                        <div class="title-box t0">Name</div>
                                        <div class="title-box t1">Requirement</div>
                                        <div class="title-box t4">Category</div>
                                        <div class="title-box t3">Unit price (€)</div>
                                        <div class="title-box t4">Country</div>
                                        <div class="title-box t5">Action</div>
                                    <div class="clr"></div>    
                                    </div>
                                    <?php if(isset($latest_requirment) && sizeof($latest_requirment) > 0) { ?>
                                      <ul id="scroller">
                                   
                                    
                                        <?php foreach ($latest_requirment as $requirment) { 

                                        $this->db->where('status' , 'Unblock');    
                                        $get_user     = $this->master_model->getRecords('tbl_user_master',array('id'=>$requirment['buyer_id']));

                                        ?>
                                        <li>
                                           <div class="row-box">
                                            <div class="sub-box">
                                            <?php if(isset($get_user[0]['name'])) 
                                                         { echo $get_user[0]['name'];} else { echo "Not Available"; } ?>
                                            </div>
                                            <div class="sub-box">
                                            <?php if(isset($requirment['title'])) 
                                                         { echo '<a href="'.base_url().'search/requirment_detail/'.$requirment['id'].'">'.$requirment['title'].'</a>';  } else { echo "Not Available"; } ?>

                                            </div>
                                            <div class="sub-box t3">
                                            <?php if(isset($requirment['category_name'])) 
                                                         { echo $requirment['category_name'];} else { echo "Not Available"; } ?>

                                            </div>
                                            <div class="sub-box t4">
                                             <?php if(isset($requirment['price'])) 
                                                         { echo CURRENCY.$requirment['price'];} else { echo "Not Available"; } ?>

                                            </div>
                                            <div class="sub-box">
                                            <?php if(isset($requirment['location'])) 
                                                         { echo $requirment['location'];} else { echo "Not Available"; } ?>
                                            </div>
                                            <div class="sub-box">

                                                <?php  if(!empty($this->session->userdata('user_id')) && $this->session->userdata('user_type') == "Seller"){ ?>
                                                       <a data-toggle="modal" 
                                                          data-backdrop="static" 
                                                          data-reqid="<?php echo $requirment['id'] ?>" 
                                                          data-buyername="<?php  if(isset($get_user[0]['name'])) echo $get_user[0]['name']; ?>"
                                                          data-requirment="<?php echo $requirment['title'] ?>"
                                                          data-category="<?php echo $requirment['category_name'] ?>"
                                                          data-price="<?php echo $requirment['price'] ?>"
                                                          data-target="#myReqModal" 
                                                       class="send-btn-request let_require new-btnline"><span>MakeOffer</span></a>
                                                  <?php } else {?>

                                                       <a data-toggle="modal" style="background-color:grey;" title="Sorry!!, you not a seller" class="send-btn-request new-btnline"><span>MakeOffer</span></a>

                                                  <?php } ?>
                                            </div>  
                                            <div class="clr"></div>                                          
                                            </div>
                                        </li>
                                        <?php } ?>

                                    <?php
                                    } else {
                                        ?><div style="padding:5px;"><?php 
                                        $this->load->view('no-data-found');
                                        ?></div><?php 
                                    } ?>
                                   
                                   </ul>   
                            </div>
                    </div>
                </div>

                <?php  if(!empty($this->session->userdata('user_id')) && $this->session->userdata('user_type') == "Seller"){ ?>
                <!-- Requirment Modal -->
                <div id="myReqModal" class="modal fade main" role="dialog">
                <div class="modal-dialog Change-Password">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header Password">
                            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
                            <h4 class="modal-title">Make Offer</h4>
                        </div>
                        <div class="modal-body">
                        <?php $this->load->view('status-msg'); ?>




                        <table class="table inquiries-table " style="border-top: 0px solid #ddd;" border="0" >
                            <tbody>
                                <tr >
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 18%;border-top: 0px solid #ddd;"><b>Buyer Name:</b></td>
                                    <td class="rtl-padd-bock" id="buyername" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                                <tr>
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 18%;border-top: 0px solid #ddd;"><b>Requirement:</b></td>
                                    <td class="rtl-padd-bock" id="requirment" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                                <tr >
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 18%;border-top: 0px solid #ddd;"><b>Category :</b></td>
                                    <td class="rtl-padd-bock" id="sel_category" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                                <tr >
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 18%;border-top: 0px solid #ddd;"><b>Price (<?php echo CURRENCY; ?>):</b></td>
                                    <td class="rtl-padd-bock" id="price" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                            <tbody>
                        </table>  
                        <div ng-controller="OfferToRequirmentCntrl">  


                            <form  
                                id="makeofferForm"
                                class=""
                                name="makeofferForm" 
                                enctype="multipart/form-data"
                                novalidate 
                                ng-submit="makeofferForm.$valid && requirment_offer();"  >

                                    <input type="hidden" name="requirment_id" id="requirment_id" ng-model="user.requirment_id" placeholder="requirment_id">
                                   
                                    <div class="mobile-nu-block enquiry" ng-class="{'has-error':makeofferForm.price.$touched && makeofferForm.price.$invalid}">
                                            <input type="text" 
                                                   name="price" 
                                                   class="beginningSpace_restrict char_restrict" 
                                                   ng-model="user.price"
                                                   ng-required="true"
                                                   placeholder="Enter price"/>
                                        <div class="error-new-block" ng-messages="makeofferForm.price.$error" ng-if="makeofferForm.$submitted || makeofferForm.price.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                        <span class="highlight"></span>
                                        <label>Unit price (<?php echo CURRENCY; ?>)</label>

                                    </div>


                                    <div class="mobile-nu-block enquiry" ng-class="{'has-error':makeofferForm.country.$touched && makeofferForm.country.$invalid}">
                                            <input type="text" 
                                                   name="country" 
                                                   class="beginningSpace_restrict" 
                                                   ng-model="user.country" 
                                                   placeholder="Enter country"
                                                   ng-required="true"
                                                   />
                                        <div class="error-new-block" ng-messages="makeofferForm.country.$error" ng-if="makeofferForm.$submitted || makeofferForm.country.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                        <span class="highlight"></span>
                                        <label>Country</label>

                                    </div>
                                    
                                    <div class="mobile-nu-block input-first box-msg" ng-class="{'has-error':makeofferForm.description.$touched && makeofferForm.description.$invalid}">
                                        <textarea rows="" 
                                                      cols="" 
                                                      name="description" 
                                                      class="beginningSpace_restrict" 
                                                      ng-model="user.description"
                                                      ng-required="true"
                                                      ng-maxlength="500"
                                                      placeholder="Enter offer description"></textarea>

                                        <span class="highlight"></span> 
                                        <label>Message</label>

                                        <div class="error-new-block" ng-messages="makeofferForm.description.$error" ng-if="makeofferForm.$submitted || makeofferForm.description.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            <p ng-message="maxlength"   class="error">  This field to long. Only 1000 characters allow.</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <div class="btn-password changes enq">
                                        <button class="change-btn-pass " type="submit">Send</button>
                                    </div>
                                </div>
                            </form>    
                        </div>

                    </div>
                </div>
                </div>
                <!-- End Requirment Modal -->
                <script type="text/javascript">
                    $(document).ready(function() { 
                      $('.close').on('click',function() {
                        setTimeout(function(){
                            location.reload();
                        })
                      });
                    });
                </script>
                <?php } ?>



                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="last-reques-block">
                        LATEST OFFERS
                    </div>
                    <div class="main-table-market">
                        <div class="scroller-box">
                                        <div class="title-head-block">
                                        <div class="title-box t0">Name</div>
                                        <div class="title-box t1">Offer</div>
                                        <div class="title-box t4">Category</div>
                                        <div class="title-box t3">Unit price (€)</div>
                                        <div class="title-box t4">Country</div>
                                        <div class="title-box t5">Action</div>
                                    <div class="clr"></div>    
                                    </div>
                                    <?php if(isset($latest_offers) && sizeof($latest_offers) > 0) { ?>
                                      <ul id="scroller1">
                                      
                                        <?php foreach ($latest_offers as $offer) { 

                                        $this->db->where('status' , 'Unblock');    
                                        $get_user     = $this->master_model->getRecords('tbl_user_master',array('id'=>$offer['seller_id']));

                                        ?>
                                        <li>
                                           <div class="row-box">
                                            <div class="sub-box">
                                            <?php if(isset($get_user[0]['name'])) 
                                                         { echo $get_user[0]['name'];} else { echo "Not Available"; } ?>
                                            </div>
                                            <div class="sub-box">
                                            <?php if(isset($offer['title'])) 
                                                         { echo $offer['title'];  } else { echo "Not Available"; } ?>

                                            </div>
                                            <div class="sub-box t3">
                                            <?php if(isset($offer['category_name'])) 
                                                         { echo $offer['category_name'];} else { echo "Not Available"; } ?>

                                            </div>
                                            <div class="sub-box t4">
                                             <?php if(isset($offer['price'])) 
                                                         { echo CURRENCY.$offer['price'];} else { echo "Not Available"; } ?>

                                            </div>
                                            <div class="sub-box">
                                            <?php if(isset($offer['location'])) 
                                                         { echo $offer['location'];} else { echo "Not Available"; } ?>
                                            </div>
                                            <div class="sub-box">

                                                <?php  if(!empty($this->session->userdata('user_id')) && $this->session->userdata('user_type') == "Buyer"){ ?>
                                                       <a data-toggle="modal" data-backdrop="static"

                                                       data-sellername="<?php if(isset($get_user[0]['name'])) echo $get_user[0]['name']; ?>"
                                                       data-offer="<?php echo $offer['title'] ?>"
                                                       data-category="<?php echo $offer['category_name'] ?>"
                                                       data-qty="<?php echo $offer['price'] ?>"
                                                       data-offreq="<?php echo $offer['id'] ?>" data-target="#myOfferModal" class="send-btn-request off_req new-btnline"><span>MakeOffer</span></a>
                                                  <?php } else {?>

                                                      <a data-toggle="modal" style="background-color:grey;" title="Sorry!!, you not a buyer" class="send-btn-request"><span>MakeOffer</span></a>

                                                  <?php } ?>
                                            </div>  
                                            <div class="clr"></div>                                          
                                            </div>
                                        </li>
                                        <?php } ?>

                                    <?php
                                    } else {
                                        ?><div style="padding:5px;"><?php 
                                        $this->load->view('no-data-found');
                                        ?></div><?php 
                                    } ?>
                                   
                                   </ul>   
                            </div>
                    </div>
                </div>
                
                <?php  if(!empty($this->session->userdata('user_id')) && $this->session->userdata('user_type') == "Buyer"){ ?>
                <!-- Offer Modal -->
                <div id="myOfferModal" class="modal fade main" role="dialog">
                <div class="modal-dialog Change-Password">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header Password">
                            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
                            <h4 class="modal-title">Make Offer</h4>
                        </div>
                        <div class="modal-body">
                        <?php $this->load->view('status-msg'); ?>

                        <table class="table inquiries-table " border="0" >
                            <tbody>
                                <tr >
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 30%;border-top: 0px solid #ddd;"><b>Seller Name:</b></td>
                                    <td class="rtl-padd-bock" id="sellername" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                                <tr>
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 30%;border-top: 0px solid #ddd;"><b>Offer:</b></td>
                                    <td class="rtl-padd-bock" id="offer" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                                <tr >
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 30%;border-top: 0px solid #ddd;"><b>Category :</b></td>
                                    <td class="rtl-padd-bock" id="sel_category" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                                <tr >
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 30%;border-top: 0px solid #ddd;"><b>Price(<?php echo CURRENCY; ?>):</b></td>
                                    <td class="rtl-padd-bock" id="qty" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                            <tbody>
                        </table>  
                        <div ng-controller="OfferToOfferCntrl">  
                            <form  
                                id="giveofferForm"
                                class=""
                                name="giveofferForm" 
                                enctype="multipart/form-data"
                                novalidate 
                                ng-submit="giveofferForm.$valid && give_offer();"  >

                                    <input type="hidden" name="offer_id" id="offer_id" ng-model="user.offer_id" placeholder="offer_id">
                                   
                                    <div class="mobile-nu-block enquiry" ng-class="{'has-error':giveofferForm.price.$touched && giveofferForm.price.$invalid}">
                                            <input type="text" 
                                                   name="price" 
                                                   class="beginningSpace_restrict char_restrict" 
                                                   ng-model="user.price"
                                                   ng-required="true"
                                                   placeholder="Enter price"/>
                                        <div class="error-new-block" ng-messages="giveofferForm.price.$error" ng-if="giveofferForm.$submitted || giveofferForm.price.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                        <span class="highlight"></span>
                                        <label>Unit price (<?php echo CURRENCY; ?>)</label>

                                    </div>


                                    <div class="mobile-nu-block enquiry" ng-class="{'has-error':giveofferForm.country.$touched && giveofferForm.country.$invalid}">
                                            <input type="text" 
                                                   name="country" 
                                                   class="beginningSpace_restrict" 
                                                   ng-model="user.country" 
                                                   placeholder="Enter country"
                                                   ng-required="true"
                                                   />
                                        <div class="error-new-block" ng-messages="giveofferForm.country.$error" ng-if="giveofferForm.$submitted || giveofferForm.country.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                        <span class="highlight"></span>
                                        <label>Country</label>

                                    </div>
                                    
                                    <div class="mobile-nu-block input-first box-msg" ng-class="{'has-error':giveofferForm.description.$touched && giveofferForm.description.$invalid}">
                                        <textarea rows="" 
                                                      cols="" 
                                                      name="description" 
                                                      class="beginningSpace_restrict" 
                                                      ng-model="user.description"
                                                      ng-required="true"
                                                      ng-maxlength="500"
                                                      placeholder="Enter offer description"></textarea>

                                        <span class="highlight"></span> 
                                        <label>Message</label>

                                        <div class="error-new-block" ng-messages="giveofferForm.description.$error" ng-if="giveofferForm.$submitted || giveofferForm.description.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            <p ng-message="maxlength"   class="error">  This field to long. Only 1000 characters allow.</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <div class="btn-password changes enq">
                                        <button class="change-btn-pass " type="submit">Send</button>
                                    </div>
                                </div>
                            </form>    
                        </div>

                    </div>
                </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function() { 
                      $('.close').on('click',function() {
                        setTimeout(function(){
                            location.reload();
                        })
                      });
                    });
                </script>

                <?php } ?>
                <!-- End Offer Modal -->


            </div>
        </div>
    </div>    
</div>


<script type="application/javascript">

$(function() {
    $('marquee').mouseover(function() {
        $(this).attr('scrollamount',0);
    }).mouseout(function() {
         $(this).attr('scrollamount',2);
    });

});


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLKCmJEIrOrzdRklZHYer8q9qx2XLJ4Vs&sensor=false&libraries=places"></script>
<script src="<?php echo base_url(); ?>assets/js/geolocator/jquery.geocomplete.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var location = "";
          $("#location").geocomplete({
            details: ".geo-details",
            detailsAttribute: "data-geo",
            location:location,
            types:[]
        });
    });
</script>    