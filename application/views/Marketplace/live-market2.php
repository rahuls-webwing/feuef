<script type="text/javascript" src="<?php echo base_url() ?>front-asset/js/jquery.simplyscroll.js"></script>
<script type="text/javascript">
(function($) {
    $(function() {
        $("#scroller").simplyScroll({orientation:'vertical',customClass:'vert'});
        $("#scroller1").simplyScroll({orientation:'vertical',customClass:'vert'});
    });
})(jQuery);
</script>


<link rel="stylesheet" href="<?php echo base_url() ?>assets/giocomplete/jquery-ui.css">
<script src="<?php echo base_url() ?>assets/giocomplete/jquery-ui.js"></script>

<!-- Multi Select CSS -->
<link href="<?php echo base_url();?>front-asset/css/select2.min.css" rel="stylesheet" type="text/css" />
<div id="listing-header"></div>

<div class="listing-section">
    <div class="container-fluid">
        <div class="row">
            <!--list view start here-->
            <form id="serach_critearea" action="<?php echo base_url().'marketplace'; ?>" method="GET">

            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="box-listing space-top-gowed">
                 <div class="row">


                    <div class="col-sm-3 col-md-2 col-lg-2">
                        <div class="user-one locations-input">
                            <input  class="con-input" name="location" id="location" value="<?php  if(isset($_REQUEST['location']) && $_REQUEST['location'] != ""){ echo $_REQUEST['location']; } ?>" placeholder="Enter the location"  type="text" />
                            <div class="locations-input"><i class="fa fa-user-o" aria-hidden="true"></i></div> 
                        </div>
                    </div>


                     <div class="col-sm-3 col-md-2 col-lg-2">
                        <div class="gry-bx select-style">
                            <select class="frm-select fnt"   name="category" id="category">
                                <option value="">--Select a category--</option>
                                <option <?php  if(isset($_REQUEST['category']) &&
                                        $_REQUEST['category'] == ""){ echo 'selected'; } ?> value="">--All Category--</option>
                                <?php foreach ($getCat as $cate_value) {?>
                                    <option <?php  if(isset($_REQUEST['category']) && $_REQUEST['category'] != "" &&
                                        $_REQUEST['category'] == $cate_value['category_id']){ echo 'selected'; } ?> value="<?php echo $cate_value['category_id']; ?>"><?php echo $cate_value['category_name']; ?></option>
                                <?php } ?>      
                            </select>
                        </div>
                    </div>
                
                    <input type="hidden" name="min-price" id="min-price" value="<?php if(isset($_REQUEST['min-price'])) { echo $_REQUEST['min-price']; } else { echo '0'; }  ?>" >
                    <input type="hidden" name="max-price" id="max-price" value="<?php if(isset($_REQUEST['max-price'])) { echo $_REQUEST['max-price']; } else { echo '0'; }  ?>" >
                    <div class="col-sm-2 col-md-2 col-lg-2">
                       <div class="price-slider seller-list-prices">
                           <div class="range-t input-bx" for="amount">
                               <div id="slider-price-range" class="slider-rang"></div>
                                  <div class="amount-no" id="slider_price_range_txt">
                               </div>
                           </div>
                       </div>
                    </div>

            <div class="col-sm-2 col-md-1 col-lg-1">
             <button type="submit"  class="more-f-bor"><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>

            <?php  if(isset($_REQUEST['location'])){ ?>
            <div class="col-sm-2 col-md-1 col-lg-1">
             <a href="<?php echo base_url().'marketplace'; ?>"  class="more-f-bor"><i class="fa fa-refresh" aria-hidden="true"></i></a>
            </div>
            <?php } ?>
     </div>
 </div>
</form>
</div>

<!-- requirement prices -->
<?php
$max_price =[];
foreach ($offer_prices as $price) {
    $max_price[] = strtok($price['price'],'.');
}
foreach ($requirment_prices as $price) {
    $max_price[] = strtok($price['price'],'.');
}
?>
<input type="hidden"  id="max_product_price" value="<?php echo MAX($max_price); ?>" ?>
<!-- end requirement prices -->

<div class="col-sm-7 col-md-7 col-lg-7">


            <div class="row">                                             
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="last-reques-block">
                        LATEST REQUIREMENTS 
                    </div>
                    <div class="main-table-market">
                        <div class="scroller-box">
                                        <div class="title-head-block">
                                        <div class="title-box t0">Name</div>
                                        <div class="title-box t1">Requirement</div>
                                        <div class="title-box t4">Category</div>
                                        <div class="title-box t3">Unit price (€)</div>
                                        <div class="title-box t4">Country</div>
                                        <div class="title-box t5">Action</div>
                                    <div class="clr"></div>    
                                    </div>
                                    <?php if(isset($latest_requirment) && sizeof($latest_requirment) > 0) { ?>
                                      <ul id="scroller">
                                   
                                    
                                        <?php foreach ($latest_requirment as $requirment) { 

                                        $this->db->where('status' , 'Unblock');    
                                        $get_user     = $this->master_model->getRecords('tbl_user_master',array('id'=>$requirment['buyer_id']));

                                        ?>
                                        <li>
                                           <div class="row-box">
                                            <div class="sub-box">
                                            <?php if(isset($get_user[0]['name'])) 
                                                         { echo $get_user[0]['name'];} else { echo "Not Available"; } ?>
                                            </div>
                                            <div class="sub-box">
                                            <?php if(isset($requirment['title'])) 
                                                         { echo '<a href="'.base_url().'search/requirment_detail/'.$requirment['id'].'">'.$requirment['title'].'</a>';  } else { echo "Not Available"; } ?>

                                            </div>
                                            <div class="sub-box t3">
                                            <?php if(isset($requirment['category_name'])) 
                                                         { echo $requirment['category_name'];} else { echo "Not Available"; } ?>

                                            </div>
                                            <div class="sub-box t4">
                                             <?php if(isset($requirment['price'])) 
                                                         { echo CURRENCY.$requirment['price'];} else { echo "Not Available"; } ?>

                                            </div>
                                            <div class="sub-box">
                                            <?php if(isset($requirment['location'])) 
                                                         { echo $requirment['location'];} else { echo "Not Available"; } ?>
                                            </div>
                                            <div class="sub-box">

                                                <?php  if(!empty($this->session->userdata('user_id')) && $this->session->userdata('user_type') == "Seller"){ ?>
                                                       <a data-toggle="modal" 
                                                          data-backdrop="static" 
                                                          data-reqid="<?php echo $requirment['id'] ?>" 
                                                          data-buyername="<?php  if(isset($get_user[0]['name'])) echo $get_user[0]['name']; ?>"
                                                          data-requirment="<?php echo $requirment['title'] ?>"
                                                          data-category="<?php echo $requirment['category_name'] ?>"
                                                          data-price="<?php echo $requirment['price'] ?>"
                                                          data-target="#myReqModal" 
                                                       class="send-btn-request let_require new-btnline"><span>MakeOffer</span></a>
                                                  <?php } else {?>

                                                       <a data-toggle="modal" style="background-color:grey;" title="Sorry!!, you not a seller" class="send-btn-request new-btnline"><span>MakeOffer</span></a>

                                                  <?php } ?>
                                            </div>  
                                            <div class="clr"></div>                                          
                                            </div>
                                        </li>
                                        <?php } ?>

                                    <?php
                                    } else {
                                        ?><div style="padding:5px;"><?php 
                                        $this->load->view('no-data-found');
                                        ?></div><?php 
                                    } ?>
                                   
                                   </ul>   
                            </div>
                    </div>
                </div>

                <?php  if(!empty($this->session->userdata('user_id')) && $this->session->userdata('user_type') == "Seller"){ ?>
                <!-- Requirment Modal -->
                <div id="myReqModal" class="modal fade main" role="dialog">
                <div class="modal-dialog Change-Password">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header Password">
                            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
                            <h4 class="modal-title">Make Offer</h4>
                        </div>
                        <div class="modal-body">
                        <?php $this->load->view('status-msg'); ?>




                        <table class="table inquiries-table " style="border-top: 0px solid #ddd;" border="0" >
                            <tbody>
                                <tr >
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 18%;border-top: 0px solid #ddd;"><b>Buyer Name:</b></td>
                                    <td class="rtl-padd-bock" id="buyername" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                                <tr>
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 18%;border-top: 0px solid #ddd;"><b>Requirement:</b></td>
                                    <td class="rtl-padd-bock" id="requirment" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                                <tr >
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 18%;border-top: 0px solid #ddd;"><b>Category :</b></td>
                                    <td class="rtl-padd-bock" id="sel_category" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                                <tr >
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 18%;border-top: 0px solid #ddd;"><b>Price (<?php echo CURRENCY; ?>):</b></td>
                                    <td class="rtl-padd-bock" id="price" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                            <tbody>
                        </table>  
                        <div ng-controller="OfferToRequirmentCntrl">  


                            <form  
                                id="makeofferForm"
                                class=""
                                name="makeofferForm" 
                                enctype="multipart/form-data"
                                novalidate 
                                ng-submit="makeofferForm.$valid && requirment_offer();"  >

                                    <input type="hidden" name="requirment_id" id="requirment_id" ng-model="user.requirment_id" placeholder="requirment_id">
                                   
                                    <div class="mobile-nu-block enquiry" ng-class="{'has-error':makeofferForm.price.$touched && makeofferForm.price.$invalid}">
                                            <input type="text" 
                                                   name="price" 
                                                   class="beginningSpace_restrict char_restrict" 
                                                   ng-model="user.price"
                                                   ng-required="true"
                                                   placeholder="Enter price"/>
                                        <div class="error-new-block" ng-messages="makeofferForm.price.$error" ng-if="makeofferForm.$submitted || makeofferForm.price.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                        <span class="highlight"></span>
                                        <label>Unit price (<?php echo CURRENCY; ?>)</label>

                                    </div>


                                    <div class="mobile-nu-block enquiry" ng-class="{'has-error':makeofferForm.country.$touched && makeofferForm.country.$invalid}">
                                            <input type="text" 
                                                   name="country" 
                                                   class="beginningSpace_restrict" 
                                                   ng-model="user.country" 
                                                   placeholder="Enter country"
                                                   ng-required="true"
                                                   />
                                        <div class="error-new-block" ng-messages="makeofferForm.country.$error" ng-if="makeofferForm.$submitted || makeofferForm.country.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                        <span class="highlight"></span>
                                        <label>Country</label>

                                    </div>
                                    
                                    <div class="mobile-nu-block input-first box-msg" ng-class="{'has-error':makeofferForm.description.$touched && makeofferForm.description.$invalid}">
                                        <textarea rows="" 
                                                      cols="" 
                                                      name="description" 
                                                      class="beginningSpace_restrict" 
                                                      ng-model="user.description"
                                                      ng-required="true"
                                                      ng-maxlength="500"
                                                      placeholder="Enter offer description"></textarea>

                                        <span class="highlight"></span> 
                                        <label>Message</label>

                                        <div class="error-new-block" ng-messages="makeofferForm.description.$error" ng-if="makeofferForm.$submitted || makeofferForm.description.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            <p ng-message="maxlength"   class="error">  This field to long. Only 1000 characters allow.</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <div class="btn-password changes enq">
                                        <button class="change-btn-pass " type="submit">Send</button>
                                    </div>
                                </div>
                            </form>    
                        </div>

                    </div>
                </div>
                </div>
                <!-- End Requirment Modal -->
                <script type="text/javascript">
                    $(document).ready(function() { 
                      $('.close').on('click',function() {
                        setTimeout(function(){
                            location.reload();
                        })
                      });
                    });
                </script>
                <?php } ?>



                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="last-reques-block">
                        LATEST OFFERS
                    </div>
                    <div class="main-table-market">
                        <div class="scroller-box">
                                        <div class="title-head-block">
                                        <div class="title-box t0">Name</div>
                                        <div class="title-box t1">Offer</div>
                                        <div class="title-box t4">Category</div>
                                        <div class="title-box t3">Unit price (€)</div>
                                        <div class="title-box t4">Country</div>
                                        <div class="title-box t5">Action</div>
                                    <div class="clr"></div>    
                                    </div>
                                    <?php if(isset($latest_offers) && sizeof($latest_offers) > 0) { ?>
                                      <ul id="scroller1">
                                      
                                        <?php foreach ($latest_offers as $offer) { 

                                        $this->db->where('status' , 'Unblock');    
                                        $get_user     = $this->master_model->getRecords('tbl_user_master',array('id'=>$offer['seller_id']));

                                        ?>
                                        <li>
                                           <div class="row-box">
                                            <div class="sub-box">
                                            <?php if(isset($get_user[0]['name'])) 
                                                         { echo $get_user[0]['name'];} else { echo "Not Available"; } ?>
                                            </div>
                                            <div class="sub-box">
                                            <?php if(isset($offer['title'])) 
                                                         { echo $offer['title'];  } else { echo "Not Available"; } ?>

                                            </div>
                                            <div class="sub-box t3">
                                            <?php if(isset($offer['category_name'])) 
                                                         { echo $offer['category_name'];} else { echo "Not Available"; } ?>

                                            </div>
                                            <div class="sub-box t4">
                                             <?php if(isset($offer['price'])) 
                                                         { echo CURRENCY.$offer['price'];} else { echo "Not Available"; } ?>

                                            </div>
                                            <div class="sub-box">
                                            <?php if(isset($offer['location'])) 
                                                         { echo $offer['location'];} else { echo "Not Available"; } ?>
                                            </div>
                                            <div class="sub-box">

                                                <?php  if(!empty($this->session->userdata('user_id')) && $this->session->userdata('user_type') == "Buyer"){ ?>
                                                       <a data-toggle="modal" data-backdrop="static"

                                                       data-sellername="<?php if(isset($get_user[0]['name'])) echo $get_user[0]['name']; ?>"
                                                       data-offer="<?php echo $offer['title'] ?>"
                                                       data-category="<?php echo $offer['category_name'] ?>"
                                                       data-qty="<?php echo $offer['price'] ?>"
                                                       data-offreq="<?php echo $offer['id'] ?>" data-target="#myOfferModal" class="send-btn-request off_req new-btnline"><span>MakeOffer</span></a>
                                                  <?php } else {?>

                                                      <a data-toggle="modal" style="background-color:grey;" title="Sorry!!, you not a buyer" class="send-btn-request"><span>MakeOffer</span></a>

                                                  <?php } ?>
                                            </div>  
                                            <div class="clr"></div>                                          
                                            </div>
                                        </li>
                                        <?php } ?>

                                    <?php
                                    } else {
                                        ?><div style="padding:5px;"><?php 
                                        $this->load->view('no-data-found');
                                        ?></div><?php 
                                    } ?>
                                   
                                   </ul>   
                            </div>
                    </div>
                </div>
                
                <?php  if(!empty($this->session->userdata('user_id')) && $this->session->userdata('user_type') == "Buyer"){ ?>
                <!-- Offer Modal -->
                <div id="myOfferModal" class="modal fade main" role="dialog">
                <div class="modal-dialog Change-Password">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header Password">
                            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
                            <h4 class="modal-title">Make Offer</h4>
                        </div>
                        <div class="modal-body">
                        <?php $this->load->view('status-msg'); ?>

                        <table class="table inquiries-table " border="0" >
                            <tbody>
                                <tr >
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 30%;border-top: 0px solid #ddd;"><b>Seller Name:</b></td>
                                    <td class="rtl-padd-bock" id="sellername" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                                <tr>
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 30%;border-top: 0px solid #ddd;"><b>Offer:</b></td>
                                    <td class="rtl-padd-bock" id="offer" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                                <tr >
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 30%;border-top: 0px solid #ddd;"><b>Category :</b></td>
                                    <td class="rtl-padd-bock" id="sel_category" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                                <tr >
                                    <td class="rtl-padd-bock" style="padding-left:14px; width: 30%;border-top: 0px solid #ddd;"><b>Price(<?php echo CURRENCY; ?>):</b></td>
                                    <td class="rtl-padd-bock" id="qty" style="border-top: 0px solid #ddd;"></td>
                                </tr>
                            <tbody>
                        </table>  
                        <div ng-controller="OfferToOfferCntrl">  
                            <form  
                                id="giveofferForm"
                                class=""
                                name="giveofferForm" 
                                enctype="multipart/form-data"
                                novalidate 
                                ng-submit="giveofferForm.$valid && give_offer();"  >

                                    <input type="hidden" name="offer_id" id="offer_id" ng-model="user.offer_id" placeholder="offer_id">
                                   
                                    <div class="mobile-nu-block enquiry" ng-class="{'has-error':giveofferForm.price.$touched && giveofferForm.price.$invalid}">
                                            <input type="text" 
                                                   name="price" 
                                                   class="beginningSpace_restrict char_restrict" 
                                                   ng-model="user.price"
                                                   ng-required="true"
                                                   placeholder="Enter price"/>
                                        <div class="error-new-block" ng-messages="giveofferForm.price.$error" ng-if="giveofferForm.$submitted || giveofferForm.price.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                        <span class="highlight"></span>
                                        <label>Unit price (<?php echo CURRENCY; ?>)</label>

                                    </div>


                                    <div class="mobile-nu-block enquiry" ng-class="{'has-error':giveofferForm.country.$touched && giveofferForm.country.$invalid}">
                                            <input type="text" 
                                                   name="country" 
                                                   class="beginningSpace_restrict" 
                                                   ng-model="user.country" 
                                                   placeholder="Enter country"
                                                   ng-required="true"
                                                   />
                                        <div class="error-new-block" ng-messages="giveofferForm.country.$error" ng-if="giveofferForm.$submitted || giveofferForm.country.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                        <span class="highlight"></span>
                                        <label>Country</label>

                                    </div>
                                    
                                    <div class="mobile-nu-block input-first box-msg" ng-class="{'has-error':giveofferForm.description.$touched && giveofferForm.description.$invalid}">
                                        <textarea rows="" 
                                                      cols="" 
                                                      name="description" 
                                                      class="beginningSpace_restrict" 
                                                      ng-model="user.description"
                                                      ng-required="true"
                                                      ng-maxlength="500"
                                                      placeholder="Enter offer description"></textarea>

                                        <span class="highlight"></span> 
                                        <label>Message</label>

                                        <div class="error-new-block" ng-messages="giveofferForm.description.$error" ng-if="giveofferForm.$submitted || giveofferForm.description.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            <p ng-message="maxlength"   class="error">  This field to long. Only 1000 characters allow.</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <div class="btn-password changes enq">
                                        <button class="change-btn-pass " type="submit">Send</button>
                                    </div>
                                </div>
                            </form>    
                        </div>

                    </div>
                </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function() { 
                      $('.close').on('click',function() {
                        setTimeout(function(){
                            location.reload();
                        })
                      });
                    });
                </script>

                <?php } ?>
                <!-- End Offer Modal -->
            </div>

    </div>
<!--map-box start here-->
<div class="col-sm-5 col-md-5 col-lg-5"><!--pad-r-less-->
    <div class="row">
        <div class="map-sec">
          <div id="map" width="100%" height="1270"></div>
        </div>
    </div>
</div>
<!--map-box end here-->
</div>
</div>
</div>




<script type="application/javascript">
$(function() {
    $('marquee').mouseover(function() {
        $(this).attr('scrollamount',0);
    }).mouseout(function() {
         $(this).attr('scrollamount',2);
    });

});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLKCmJEIrOrzdRklZHYer8q9qx2XLJ4Vs&sensor=false&libraries=places"></script>
<script src="<?php echo base_url(); ?>assets/js/geolocator/jquery.geocomplete.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var location = "";
          $("#location").geocomplete({
            details: ".geo-details",
            detailsAttribute: "data-geo",
            location:location,
            types:[]
        });
    });
</script>    


<!-- Clustering  MAP -->
<style>
#map {
height: 100%;
}
html, body {
height: 100%;
margin: 0;
padding: 0;
}
    
    .content-img.address-urt{font-size: 14px;    color: #cacaca;}
.img-block-section {
  overflow: hidden;
  margin-bottom: 15px;
  margin-top: 0px;
  position: relative;
  box-shadow: 2px 0 5px rgba(0, 0, 0, 0.37);
  -webkit-box-shadow: 2px 0 5px rgba(0, 0, 0, 0.37);
  -moz-box-shadow: 2px 0 5px rgba(0, 0, 0, 0.37);
  cursor: pointer;
  transition: all 0.5s ease 0s;
  -webkit-transition: all 0.5s ease 0s;
  -moz-transition: all 0.5s ease 0s;
   height: 290px;
}
.details-bx {
  bottom: 0;
  position: absolute;
  width: 100%;padding-bottom: 10px;
}
.details-section {
  background: rgba(0, 0, 0, 0) url("../images/tras-bg.png") repeat scroll 0 0;
/*  height: 100px;*/
}
.img-circle {
  float: left;
  height: 10px;
  width: 10px;
}
.content-img.seller-cont-img {
font-weight: 600;
padding-top: 6px;
}
.content-img {
  color: #333;
  display: block;
  float: left;
  font-family: "ralewaysemibold",sans-serif;
  font-size: 16px;
  max-width: 122px;
  overflow: hidden;
  padding-left: 12px;
  padding-top: 6px;
  text-overflow: ellipsis;
  white-space: nowrap;
  width: 100%;
}
.price-cont {
  color: #fff;
  float: right;
  font-size: 22px;
  padding-top: 12px;
}
</style>

<!-- <div id="map" style="width: 1000px; height: 600px;"></div> -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyCccvQtzVx4aAt05YnfzJDSWEzPiVnNVsY&libraries=places" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/markerclustererplus/src/markerclusterer.js"></script>
<script src="<?php echo base_url(); ?>front-asset/js/no-country-place.js" type="text/javascript"></script>
<script>
var locations = [];
locations = [
    <?php   
        if(count($userLatLng))
        {
            foreach($userLatLng as $row) 
            {
                if(!empty($row['latitude']) && !empty($row['longitude']))     
                {  
                    if($row['user_type'] == "Seller"){
                       $user_type = 'seller';
                       $url       =  base_url()."seller/profile/".$row["id"];
                       $style     =  "";
                    }
                    else {
                       $user_type = 'buyer';
                       $url       = '#';
                       $style     = 'style="cursor:text"';
                    }
                    if(!empty($row['user_image']) && file_exists('images/'.$user_type.'_image/'.$row['user_image']))
                    {
                        if($row['user_type'] == "Seller"){
                           $img = base_url().'images/seller_image/'.$row['user_image'];
                        }
                        else {
                           $img = base_url().'images/buyer_image/'.$row['user_image'];
                        }
                        
                    }
                    else 
                    {
                        $img = base_url().'images/default/default-user-img.jpg';
                    }
                    ?>
                      ['<div class="col-sm-12 col-sm-12 col-lg-12"><div class="img-block-section"><div class="details-bx"><div class="details-section"><a <?php echo $style; ?> href="<?php echo $url; ?>"><div class="content-img"><?php echo $row["name"]; ?></div><div class="content-img address-urt"><?php echo $row["address"]; ?></div><div class="content-img seller-cont-img"><?php echo $row["user_type"]; ?></div></a></div></div><div class="img-grid"><img src="<?php echo $img; ?>" alt="" class="img-resize" style="max-width: none;height: 200px;width: 200px;"></div></div></div>',<?php echo $row['latitude']; ?>,<?php echo $row['longitude']; ?>],
                    <?php
                }
            }
        }
    ?>
];

<?php
    // find center   
    if(count($userLatLng))
    {
        foreach($userLatLng as $row) 
        {
            if(!empty($row['latitude']) && !empty($row['longitude']))     
            {  
                 $minlat = false;
                 $minlng = false;
                 $maxlat = false;
                 $maxlng = false;
                     if ($minlat === false) { $minlat = $row['latitude']; } else { $minlat = ($row['latitude'] < $minlat) ? $row['latitude'] : $minlat; }
                     if ($maxlat === false) { $maxlat = $row['latitude']; } else { $maxlat = ($row['latitude'] > $maxlat) ? $row['latitude'] : $maxlat; }
                     if ($minlng === false) { $minlng = $row['longitude']; } else { $minlng = ($row['longitude'] < $minlng) ? $row['longitude'] : $minlng; }
                     if ($maxlng === false) { $maxlng = $row['longitude']; } else { $maxlng = ($row['longitude'] > $maxlng) ? $row['longitude'] : $maxlng; }
                  // Calculate the center
                  $lat = $maxlat - (($maxlat - $minlat) / 2);
                  $lon = $maxlng - (($maxlng - $minlng) / 2);
            }
        }
    }
?>



var defaultLat =  <?php if(isset($lat)){ echo $lat; } else { echo '19.997453'; } ?>;
var defaultLng =  <?php if(isset($lon)){ echo $lon; } else { echo '73.789802'; } ?>;
var _address   =  "<?php  if(isset($_REQUEST['location']) && $_REQUEST['location'] != ""){ echo $_REQUEST['location']; } else { echo ""; }?>";
var map;       
var mc;/*marker clusterer*/
var mcOptions = {
      gridSize: 20,
      //maxZoom: 7,
      zoom:10,
      imagePath: "https://cdn.rawgit.com/googlemaps/v3-utility-library/master/markerclustererplus/images/m"
  };
/*global infowindow*/
var infowindow = new google.maps.InfoWindow();
/*geocoder*/

var _address = _address;
mapInitialize(defaultLat, defaultLng,_address);
function createMarker(latlng,text)
{
var marker = new google.maps.Marker({
    position: latlng,
    map: map,
    icon: "<?php echo base_url().'images/fevicon/map-icn.png'; ?>"

});
/*get array of markers currently in cluster*/
var allMarkers = mc.getMarkers();
 
/*check to see if any of the existing markers match the latlng of the new marker*/
if (allMarkers.length != 0) {
  for (i=0; i < allMarkers.length; i++) {
    var existingMarker = allMarkers[i];
    var pos = existingMarker.getPosition();
    if (latlng.equals(pos)) {
      text = text + " " + locations[i][0];
    }
  }
}

google.maps.event.addListener(marker, 'click', function(){
    infowindow.close();
    infowindow.setContent(text);
    infowindow.open(map,marker);
});
mc.addMarker(marker);
return marker;
}

function mapInitialize(lat,lng,_address)
{
var geocoder = new google.maps.Geocoder(); 
 geocoder.geocode( { 'address': _address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      /*map.setCenter(results[0].geometry.location);*/
      map.setOptions({ maxZoom: 15 });
      map.fitBounds(results[0].geometry.viewport);
    }
  });
  var options = {
      zoom: 3,
      center: new google.maps.LatLng(lat,lng), 
      mapTypeId: google.maps.MapTypeId.ROADMAP,
  };
  map = new google.maps.Map(document.getElementById('map'), options); 

  /*marker cluster*/
  var gmarkers = [];
  mc = new MarkerClusterer(map, [], mcOptions);
  for (i=0; i<locations.length; i++)
  {
      var latlng = new google.maps.LatLng(parseFloat(locations[i][1]),parseFloat(locations[i][2]));                
      gmarkers.push(createMarker(latlng,locations[i][0]));
  }
}
</script>
<!-- Clustering  MAP -->


<script type="text/javascript">
        $(function() {

           var max_product_price          = $('#max_product_price').val();
           var max_product_price_constant = $('#max_product_price').val();
           var min_price                  = $('#min-price').val();
           var max_price                  = $('#max-price').val();

           if(max_price != 0)
           {
            max_product_price = max_price;
           }

           $("#slider-price-range").slider({
               range: true,
               min: 0,
               max: max_product_price_constant,
               values: [min_price, max_product_price],
               slide: function(event, ui) {
                   $('#min-price').val(ui.values[0]);
                   $('#max-price').val(ui.values[1]);
                   $("#slider_price_range_txt").html("<span class='slider_price_min'>$ " + ui.values[0] + "</span>  <span style='margin-left:50%;' class='slider_price_max'>$ " + ui.values[1] + " </span>");
               }
           });
           $("#slider_price_range_txt").html("<span class='slider_price_min'> $ " + $("#slider-price-range").slider("values", 0) + "</span>  <span style='margin-left:50%;' class='slider_price_max'>$ " + $("#slider-price-range").slider("values", 1) + "</span>");
         });      
</script> 