<div class="page-head-block">
    <div class="page-head-overlay"></div>
    <div class="container">
        <div class="contact-us-title">Contact Us</div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-9 col-lg-9">
          <?php $this->load->view('status-msg'); ?>
            <div class="send-us">Send us a Message</div>
            <div ng-controller="ContactUs">
                <form  
                   class=""
                   name="ContactUsForm" 
                   enctype="multipart/form-data"
                   novalidate ng-submit="ContactUsForm.$valid && contactus();"  >


                    <div class="contact-fild">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="mobile-nu-block conatct" ng-class="{ 'has-error': ContactUsForm.name.$touched && ContactUsForm.name.$invalid }">
                                <input type="text" 
                                       name="name" 
                                       class="beginningSpace_restrict " 
                                       ng-model="user.name"
                                       ng-required="true"
                                       ng-maxlength="100"
                                       placeholder="Enter Title"/>
                                <span class="highlight"></span>
                                <label> Name</label>
                                <div class="error-new-block" ng-messages="ContactUsForm.name.$error" ng-if="ContactUsForm.$submitted || ContactUsForm.name.$touched">
                                <div>
                                <div class="err_msg_div" style="display:none;">
                                    <p ng-message="required"    class="error">  This field is required</p>
                                    <p ng-message="maxlength"   class="error">  This field to long. Only 100 characters allow.</p>

                                    </div>
                                </div>
                                <script type="text/javascript">
                                        $(document).ready(function(){
                                          setTimeout(function(){
                                            $('.err_msg_div').removeAttr('style');
                                          },200);
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                           <div class="mobile-nu-block input-first" ng-class="{ 'has-error': ContactUsForm.email.$touched 
                                && ContactUsForm.email.$invalid }">
                                    <input autocomplete="off"  class="beginningSpace_restrict " 
                                           type="email" 
                                           name="email" 
                                           ng-model="user.email" 
                                           ng-required="true" />

                                    <span class="highlight"></span>
                                    <div class="error-new-block" ng-messages="ContactUsForm.email.$error" ng-if="ContactUsForm.$submitted || ContactUsForm.email.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            <p ng-message="email"       class="error">  This needs to be a valid email</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>
                                    <label>Email</label>
                                </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="mobile-nu-block conatct" ng-class="{ 'has-error': ContactUsForm.sub.$touched 
                            && ContactUsForm.sub.$invalid }">
                                <input type="text" 
                                       name="sub" 
                                       class="beginningSpace_restrict " 
                                       ng-model="user.sub"
                                       ng-required="true"
                                       ng-maxlength="100"
                                       placeholder="Enter Title"/>
                                <span class="highlight"></span>
                                <label> Subject</label>
                                <div class="error-new-block" ng-messages="ContactUsForm.sub.$error" ng-if="ContactUsForm.$submitted || ContactUsForm.sub.$touched">
                                <div>
                                <div class="err_msg_div" style="display:none;">
                                    <p ng-message="required"    class="error">  This field is required</p>
                                    <p ng-message="maxlength"   class="error">  This field to long. Only 100 characters allow.</p>

                                    </div>
                                </div>
                                <script type="text/javascript">
                                        $(document).ready(function(){
                                          setTimeout(function(){
                                            $('.err_msg_div').removeAttr('style');
                                          },200);
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="mobile-nu-block input-first" ng-class="{ 'has-error': ContactUsForm.mobile.$touched 
                            && ContactUsForm.mobile.$invalid }">
                                <input autocomplete="off"  class="beginningSpace_restrict   allowOnlyFourteen" 
                                       type="text" 
                                       name="mobile" 
                                       ng-minlength="10" 
                                       ng-maxlength="16"
                                       ng-model="user.mobile" 
                                       ng-required="true" />
                                <span class="highlight"></span>
                                <div class="error-new-block" ng-messages="ContactUsForm.mobile.$error" ng-if="ContactUsForm.$submitted || ContactUsForm.mobile.$touched">
                                    <div>
                                    <div class="err_msg_div" style="display:none;">

                                        <p ng-message="required"    class="error">  This field is required</p>
                                        <p ng-message="minlength"   class="error">  Please enter at least 10 digit</p>
                                        <p ng-message="maxlength"   class="error">  Mobile no to long</p>
                                        
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                            $(document).ready(function(){
                                              setTimeout(function(){
                                                $('.err_msg_div').removeAttr('style');
                                              },200);
                                            });
                                        </script>
                                    </div>
                                <label>Mobile number</label>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="mobile-nu-block input-first" ng-class="{'has-error':ContactUsForm.description.$touched && ContactUsForm.description.$invalid}">
                                <textarea rows="" 
                                                      cols="" 
                                                      name="description" 
                                                      class="beginningSpace_restrict" 
                                                      ng-model="user.description"
                                                      ng-required="true"
                                                      ng-maxlength="1000"
                                                      placeholder=" Enter Product description"></textarea>
                                <span class="highlight"></span>
                                 <div class="error-new-block" ng-messages="ContactUsForm.description.$error" ng-if="ContactUsForm.$submitted || ContactUsForm.description.$touched">
                                <div class="err_msg_div" style="display:none;">
                                    <p ng-message="required"    class="error">  This field is required</p>
                                    <p ng-message="maxlength"   class="error">  This field to long. Only 1000 characters allow.</p>
                                </div>
                                <script type="text/javascript">
                                        $(document).ready(function(){
                                          setTimeout(function(){
                                            $('.err_msg_div').removeAttr('style');
                                          },200);
                                        });
                                    </script>
                                </div>
                                <label>Message</label>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="btn-password send">
                                <button class="change-btn-pass" type="submit">Send</button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>

        </div>
        <?php
        $admin_data=$this->master_model->getRecords('admin_login', array('id'=>'1'));
        ?>
        <div class="col-sm-3 col-md-3 col-lg-3">
            <div class="send-us contact">Contact Info</div>
            <div class="cont-info">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="loction">
                            <div class="loc-image">
                                <img src="<?php echo base_url(); ?>front-asset/images/loction-icon.png" />
                            </div>
                            <div class="main-loc-text">
                                <div class="loc-text">Location</div>
                                <div class="loc-sml-text"><?php if(isset($admin_data[0]['admin_address'])) { echo $admin_data[0]['admin_address']; } else { echo "--";} ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="loction">
                            <div class="loc-image">
                                <img src="<?php echo base_url(); ?>front-asset/images/phone-img.png" />
                            </div>
                            <div class="main-loc-text">
                                <div class="loc-text">Phone</div>
                                <div class="loc-sml-text"><?php if(isset($admin_data[0]['admin_contactus'])) { echo $admin_data[0]['admin_contactus']; } else { echo "--";} ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="loction">
                            <div class="loc-image">
                                <img src="<?php echo base_url(); ?>front-asset/images/email-img.png" />
                            </div>
                            <div class="main-loc-text">
                                <div class="loc-text">Email</div>
                                <div class="loc-sml-text"><?php if(isset($admin_data[0]['admin_email'])) { echo $admin_data[0]['admin_email']; } else { echo "--";} ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="map-loc">
        <div class="send-us location-map">Location Map </div>
        <div id="map_canvas" style="height:551px;width:100%;">
     </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function (){
var address="<?php if(isset($admin_data[0]['admin_address'])) { echo $admin_data[0]['admin_address']; } else { echo 'United States';} ?>";
  initialize();
});
//var address=$("#_address").val();
var address = "<?php if(isset($admin_data[0]['admin_address'])) { echo $admin_data[0]['admin_address']; } else { echo 'United States';} ?>";
var geocoder;
var map;
var address = address;
function initialize() {

  geocoder      = new google.maps.Geocoder();
  var latlng    = new google.maps.LatLng(-34.397, 150.644);
  var myOptions = {
    zoom: 18,
    center: latlng,
    mapTypeControl: true,
    mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
    navigationControl: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

  if (geocoder) {
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
          map.setCenter(results[0].geometry.location);

          var infowindow = new google.maps.InfoWindow(
            { content: '<b>'+address+'</b>',
            size: new google.maps.Size(150,50)
          });

          var marker = new google.maps.Marker({
            position: results[0].geometry.location,
            map: map, 
            title:address
          }); 
          google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map,marker);
          });

        } else {
          sweetAlert("No results found");
        }
      } else {
        sweetAlert("Geocode was not successful for the following reason: " + status);
      }
    });
  }
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLKCmJEIrOrzdRklZHYer8q9qx2XLJ4Vs&sensor=false&libraries=places"></script>
<script src="<?php echo base_url(); ?>assets/js/geolocator/jquery.geocomplete.min.js"></script>