

    <div class="page-head-block">
        <div class="page-head-overlay"></div>
        <div class="container">
            <div class="contact-us-title">Pricing</div>
        </div>
    </div>
    <?php 
            $this->db->where('pricing_name'  , 'Premium');
            $this->db->where('membership_id' , 2);
            $get_pay_amt = $this->master_model->getRecords('tbl_pricing_master');
    ?>
    <?php 
            $this->db->where('pricing_name'  , 'Free');
            $this->db->where('membership_id' , 5);
            $get_free_amt_desc = $this->master_model->getRecords('tbl_pricing_master');
    ?>
    <div class="container">
        <div class="row">
           <div class="col-sm-12 col-md-4 col-lg-6">
                <div class="prising-block">
                    <a href="<?php echo base_url().'signup'; ?>">
                        <span class="main-title ">Free</span>
                        <span class="price-colm free"><sup><?php echo CURRENCY ?>0.00</sup></span>
                        <span class="colum"><sup><?php if(isset($get_free_amt_desc[0]['short_desc']) && !empty($get_free_amt_desc[0]['short_desc'])) { echo $get_free_amt_desc[0]['short_desc']; } else { echo 'Not Available';} ?></sup></span>
                        <span class="register-btn">
                            <?php if(empty($this->session->userdata('user_id'))){
                                ?><button class="register-now"  type="button">Register</button><?php 
                            } ?>
                         
                        </span>
                    </a>
                </div>
            </div>

            <div class="col-sm-12 col-md-4 col-lg-6">
                <div class="prising-block">
                    <a href="#">
                        <span class="main-title">Premium</span>
                        <span class="price-colm free"><sup><?php echo CURRENCY ?> <?php if(isset($get_pay_amt[0]['membership_price']) && !empty($get_pay_amt[0]['membership_price'])) { echo $get_pay_amt[0]['membership_price']; } else { echo 'Not Available';} ?></sup></span>
                        <span class="colum"><sup><?php if(isset($get_pay_amt[0]['short_desc']) && !empty($get_pay_amt[0]['short_desc'])) { echo $get_pay_amt[0]['short_desc']; } else { echo 'Not Available';} ?></sup></span>
                    </a>
                </div>
            </div>

        </div>
    </div>

</body>

</html>
