<div class="page-head-block help-page-block">
    <div class="page-head-overlay"></div>
    <div class="container">
        <div class="head-txt-block">
            Help
        </div>
    </div>
</div>
<div class="container">  
        <div class="privacy-point-head">
            Copyright Notice
        </div>
        <div class="privacy-point-content">
            You acknowledge that all content included on this Site, including, without limitation, the information, data, software, photographs, graphs, typefaces, graphics, images, illustrations, maps, designs, icons, written and other material and compilations (collectively, "Content") are intellectual property and copyrighted works of Digital Room, Inc., its licensees, and/or various third-party providers ("Providers"). Reproductions or storage of Content retrieved from this Site, in all forms, media and technologies now existing or hereafter developed, is subject to the U.S. Copyright Act of 1976, Title 17 of the United States Code. Except where expressly provided otherwise by us, nothing made available to users via the Site may be construed to confer any license or ownership right in or materials published or otherwise made available through the Site or its services, whether by estoppel, implication, or otherwise. All rights not granted to you in the Terms &amp; Conditions are expressly reserved by us.
        </div>
        <div class="privacy-point-head">
            Corporate Identification and Trademarks
        </div>
        <div class="privacy-point-content">
            Printmanager.com" and any and all other marks appearing on this Site are trademarks of Printmanager.com in the United States and other jurisdictions ("Trademarks"). You may not use, copy, reproduce, republish, upload, post, transmit distribute or modify the Trademarks in any way, including in advertising or publicity pertaining to distribution of materials on this Site, without Printmanager.com's prior written consent. The use of Trademarks on any other website or network computer environment is prohibited. Printmanager.com prohibits the use of Trademarks as a "hot" link on, or to, any other website unless establishment of such a link is pre-approved by Printmanager.com in writing.
        </div>
        <div class="privacy-point-head">
            User Conduct &amp; Eligibility
        </div>
        <div class="privacy-point-content">
            <p>You are solely responsible for the content and context of any materials you post or submit through the Site. You warrant and agree that while using the Site, you shall not upload, post, transmit, distribute or otherwise publish through the Site any materials which: (a) are unlawful, threatening, harassing or profane; (b) restrict or inhibit any other user from using or enjoying the Site; (c) constitute or encourage conduct that would constitute a criminal offense or give rise to civil liability; or (d) contain a virus or other harmful component or false or misleading indications or origin or statements of fact. </p>
            <p>As a condition of your use of certain services offered through the Site, you may be required to register an account with us and must provide true and accurate account information at all times (including, without limitation, ensuring that your account information remains current at all times.) You agree to promptly update your membership information (if applicable) in order to keep it current, complete and accurate.</p>
            <p><span>Account Security:</span> You are solely responsible for protecting the confidentiality of your password and may not disclose your password to any other person. In the event that an unauthorized user gains access to the password-protected area of the Site as a result of your acts or omissions, you agree that you shall be liable for any such unauthorized use.</p>
            <p><span>Minimum Age:</span> The Site and its services are intended solely for persons who are 18 years of age or older. Any access to or use of the Site or its service by anyone under 18 years of age is expressly prohibited. By accessing or using the Site, you represent and warrant that you are 18 years old or older.</p>
        </div>
        <div class="privacy-point-head">
            Copyright
        </div>
        <div class="privacy-point-content">
            The Digital Millennium Copyright Act of 1998 (the "DMCA") provides recourse for copyright owners who believe that material appearing on the Internet infringes their rights under U.S. Copyright law. If you believe in good faith that materials appearing on this Site infringe your copyright, you (or your agent) may send us a notice requesting that the material be removed, or access to it blocked. A conforming notice must contain the following: (a) your name, address, telephone number, and email address (if any); (b) identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to locate the material; (c) statement that you, the complaining party, has a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; (d) a statement under penalty of perjury that the information in the notification is accurate and that you are (or are authorized to act on behalf of) the owner of an exclusive right that is allegedly infringed; and (d) your physical or electronic signature as the owner or a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.
            <p>You can find more information is the U.S. Copyright Office website, currently located at https://www.printmanager.com/copyright. In accordance with the DMCA, Digital Room, Inc. has designated an agent to receive notification of alleged copyright infringement in accordance with the DMCA:</p>
            <p>
                <span class="span-txt-block">Physical Address:</span>
                <span>121 King Street,</span>
                <span>Melbourne VIC 3000, </span>
                <span>Australia </span>
                <span>+61 3 8376 6284 (10 AM - 07 PM) </span>
                <span>+61 3 8376 6284 (10 AM - 07 PM</span>
                <span class="span-txt-block">E-mail Address: <a href="mailto:copyright@printingmanage.com">copyright@printingmanage.com</a></span>
            </p>
        </div>        
        
    </div>  