<style>
    .footer-col-block {display: none;}.copyright-block {margin-top: 0;}
    body{background: #f2f6f9;}
    .footer-block-main{position: fixed; bottom: 0; width: 100%;}
</style>
<div class="middle-login-content forgot-pass-block">
    <div class="container">


        <div class="forgot-pass-main">
        
        <?php $this->load->view('status-msg'); ?>
            
          <div ng-controller="ForgotPassCntrl">
            <form class="" name="ForgotPassForm" novalidate  ng-submit="ForgotPassForm.$valid && processForgotPass();">
            <div class="login-form-block">
                <div class="login-head-block">
                    Forget Password
                </div>
                <div class="login-content-block">
                    We will send a link on your registered email to reset your password.
                </div>
                <div class="mobile-nu-block input-first" ng-class="{ 'has-error': ForgotPassForm.email.$touched 
                && ForgotPassForm.email.$invalid }">
                    <input type="email"
                           name="email" 
                           ng-model="user.email" 
                           ng-required="true" />
                    <span class="highlight"></span>
                    <div class="error-new-block" ng-messages="ForgotPassForm.email.$error" ng-if="ForgotPassForm.$submitted || ForgotPassForm.mobile.$touched">
                      
                        <div class="err_msg_div" style="display:none;">
                        <p ng-message="required"    class="error">  This field is required</p>
                        <p ng-message="email"       class="error">  This needs to be a valid email</p>
                        </div>

                        <script type="text/javascript">
                        $(document).ready(function(){
                        setTimeout(function(){
                        $('.err_msg_div').removeAttr('style');
                        },200);
                        });
                        </script>
                       
                    </div>
                    <label>Email Address</label>
                </div>
                <div class="btn-block-main-login">
                    <div class="btn-login-block">
                        <button class="login-btn" type="submit">Submit</button>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
            </form>
         </div>

            <div class="clr"></div>
        </div>
    </div>
</div>