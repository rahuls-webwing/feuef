<style type="text/css">
.pagination>li>a, .pagination>li>span {
    position: relative;
    float: left;
    top: -40px;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #ddd;
}
.label-important, .badge-important {
        background-color: #fe1010;
}
body {
    letter-spacing: 0px;
}
.mobile-block {
     text-align: left; 
}


</style>
<div class="page-head-block listing-search">
<div class="page-head-overlay"></div>
<div class="textbx-block-search">

    <button type="submit" style="width: 97%;" id="search_listing" class="search-btn-home">
       All Categories
   </button>
   

</div>
</div>

<div class="middel-container">
<div class="inner-content-block">
    <div class="container">
        <div class="row">
             <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="row mobile-block">
                    <div class="categories-block-mian">
                        <div class="container">
                            <div class="categories-head-block">
                                What is your market category?
                            </div>
                            <div class="categories-semihead">
                                Explore some of the best tips from around the city from our partners and friends.
                            </div>
                            <div class="row">
                            <?php  if(isset($getCat) && sizeof($getCat) > 0) { ?>
                                <?php $cat_show = 0;  foreach ($getCat as $category) { $cat_show++; 
                                    $this->db->where('tbl_seller_upload_product.category_id' ,$category['category_id']);
                                    $cate_product = $this->master_model->getRecords('tbl_seller_upload_product');
                                    ?>

                                        <?php if($cat_show == 5){ ?>

                                                <div class="col-sm-6 col-md-6 col-lg-6">
                                                <div class="cate-img-block mobile-height">
                                                    <a href="<?php echo base_url().'search?cat_id%5B%5D='.$category['category_id']; ?>">
                                                        <?php 
                                                        if(isset($category['profile_image']) && file_exists('uploads/cat_logo/'.$category['profile_image']))
                                                        {
                                                        ?>
                                                        <img   src="<?php echo base_url().'uploads/cat_logo/'.$category['profile_image'];?>" alt=""/>
                                                        <?php
                                                        }
                                                        else
                                                        {
                                                        ?>
                                                        <img src="<?php echo base_url().'images/noimage200X150.png';?>" alt="" />
                                                        <?php
                                                        }
                                                        ?>
                                                        <span class="img-overlay-cate"></span>
                                                        <span class="cate-img-head">
                                                        <?php if(isset($category['category_name'])) { echo $category['category_name']; } else { echo "Not Available"; } ?>
                                                    </span>
                                                        <span class="cate-icon-img">

                                                        <?php if($category['category_name'] == 'Oils'){

                                                            ?> <img src="<?php echo base_url(); ?>front-asset/images/cate-oil-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Canned Food') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-canned-food-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Vegetables') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-vagitable-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Chocolate') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-chocolate-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Snacks') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-snacks-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Cereals') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-cereals-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Wines') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-wines-icon-img.png" alt="" /><?php

                                                        }?>
                                                    </span>
                                                        <span class="cate-count-block">
                                                        <?php echo count($cate_product); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                </div>
                                        <?php }
                                        else{ ?>

                                                <div class="col-sm-3 col-md-3 col-lg-3">
                                                <div class="cate-img-block">
                                                    <a href="<?php echo base_url().'search?cat_id%5B%5D='.$category['category_id']; ?>">
                                                        <?php 
                                                        if(isset($category['profile_image']) && file_exists('uploads/cat_logo/'.$category['profile_image']))
                                                        {
                                                        ?>
                                                        <img style="height:269px;width:269px;"  src="<?php echo base_url().'uploads/cat_logo/'.$category['profile_image'];?>" alt=""/>
                                                        <?php
                                                        }
                                                        else
                                                        {
                                                        ?>
                                                        <img src="<?php echo base_url().'images/noimage200X150.png';?>" alt="" />
                                                        <?php
                                                        }
                                                        ?>
                                                        <span class="img-overlay-cate"></span>
                                                        <span class="cate-img-head">
                                                        <?php if(isset($category['category_name'])) { echo $category['category_name']; } else { echo "Not Available"; } ?>
                                                        </span>
                                                        <span class="cate-icon-img">
                                                        <?php if($category['category_name'] == 'Oils'){

                                                            ?> <img src="<?php echo base_url(); ?>front-asset/images/cate-oil-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Canned Food') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-canned-food-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Vegetables') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-vagitable-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Chocolate') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-chocolate-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Snacks') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-snacks-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Cereals') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-cereals-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Wines') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-wines-icon-img.png" alt="" /><?php

                                                        }?>
                                                        </span>
                                                        <span class="cate-count-block">
                                                        <?php echo count($cate_product); ?>
                                                    </span>
                                                   </a>
                                                </div>
                                              </div>
                                        <?php } ?>
                                    <?php } ?>    
                                <?php } else { ?>
                                <?php $this->load->view('no-data-found'); ?>
                            <?php } ?>
                        </div>
                        <!--pagigation start here-->
                        <div style="margin-top:-12px;">
                          <?php echo $this->pagination->create_links(); ?>
                        </div>
                        <!--pagigation end here-->
                     </div>
                  </div>
                </div>
            </div>
         </div>
    </div>        
</div>    

