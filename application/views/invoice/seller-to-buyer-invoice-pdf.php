<table width="100%" border="0" cellspacing="5" cellpadding="0"><tr><td></td></tr>
</table>
<table width="100%" border="0" cellspacing="5" cellpadding="0" style="border:1px solid #ddd;font-family:Arial, Helvetica, sans-serif;">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
           <td width="5%">&nbsp;</td>
           <td width="20%" ><a href="#"><img  src="<?php echo base_url(); ?>front-asset/images/logo-login.png" alt="logo" /></a></td>
          <td width="20%" style=" text-align:center; font-size:42px;">
            &nbsp;
          </td>
                    <td width="65%">
                        
                        <h3 style="border-radius:40px;width:250px;text-align: left;color: #333;font-size: 46px;letter-spacing: 0.4px;  text-transform: uppercase;font-weight: 600; margin-left:0px; display:block">ORDER NOTE</h3>
                    </td>
          </tr>
          <tr>
              <td colspan="3"height="30px"></td>
          </tr>
      </table>
      <table width="100%" border="0" cellspacing="5" cellpadding="0" style="font-size:28px; font-family:Arial, Helvetica, sans-serif; ">
        <tr>
          <td >
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:28px; font-family:Arial, Helvetica, sans-serif;">
              <tr >
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:28px; font-family:Arial, Helvetica, sans-serif;">
                    <tr> <td><b>From:</b></td> </tr>                   
                    <tr> <td><b>Seller Name:</b>  <?php if(!empty($this->session->userdata('user_name'))) { echo $this->session->userdata('user_name'); } else { 'Not Available'; } ?></td> </tr>                   
                    <tr> <td><b>Delivery Address :</b>  <?php if(!empty($getreqdetails[0]['location'])) { echo $getreqdetails[0]['location']; } else { 'Not Available'; } ?></td> </tr>
                    <tr> <td><b>Phone no:</b> <?php if(!empty($this->session->userdata('user_mobile'))) { echo $this->session->userdata('user_mobile'); } else { 'Not Available'; } ?></td> </tr>
                    <tr> <td><b>Email:</b>  <?php if(!empty($this->session->userdata('user_email'))) { echo $this->session->userdata('user_email'); } else { 'Not Available'; } ?></td> </tr>
                  </table>
                </td>
                
              </tr>
            </table>
          </td>
          <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:28px; font-family:Arial, Helvetica, sans-serif;">
              <tr >
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:28px; font-family:Arial, Helvetica, sans-serif;">
                    <tr> <td><b>To:</b></td> </tr>   
                    <tr> <td><b>Buyer Name:</b>  <?php if(!empty($get_user_data[0]['name'])) { echo  $get_user_data[0]['name']; } else { 'Not Available'; } ?></td> </tr>                   
                    <tr> <td><b>Delivery Country :</b>  <?php if(!empty($get_user_data[0]['address'])) { echo $get_user_data[0]['address']; } else { 'Not Available'; } ?></td></tr>
                    <tr> <td><b>Phone number:</b> <?php if(!empty($get_user_data[0]['mobile_number'])) { echo $get_user_data[0]['mobile_number']; } else { 'Not Available'; } ?></td> </tr>
                    <tr> <td><b>Email:</b><?php if(!empty($get_user_data[0]['email'])) { echo $get_user_data[0]['email']; } else { 'Not Available'; } ?></td> </tr>
                  </table>
                </td>
              
              </tr>
            </table> 
          </td>
        </tr>
        <!--       Table2-->
        <tr>
          <td colspan="2" style="text-align:center; height:10px; ">
           &nbsp;
            </td>
          </tr>
       <tr>
           <td colspan="2" style="font-size:28px;text-align:center;height:30px;">
               <!-- (to be completed by the buyer) -->
           </td>
       </tr>
       
        <tr>
          <td colspan="2">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:28px; font-family:Arial, Helvetica, sans-serif; ">
             <tr>
                <td>
                    <table width="100%" border="1" cellspacing="0" cellpadding="5" style="font-size:28px; font-family:Arial, Helvetica, sans-serif; ">
               <tr>
                <td style="padding:5px; background-color: #00B0F0; color:#fff;"><b>  Offer Name</b></td>
                <td style="padding:5px; background-color: #00B0F0; color:#fff;"><b>  Type</b></td>
                <td style="padding:5px; background-color: #00B0F0; color:#fff;"><b>  Final Price</b></td>
                <td style="padding:5px; background-color: #00B0F0; color:#fff;"><b>  Quantity</b></td>
                <td style="padding:5px; background-color: #00B0F0; color:#fff;"><b>  Total Price</b></td>
              </tr>
              
              <tr>
                <td style="padding:5px; min-height:200px;height:100%;"><?php if(!empty($getreqdetails[0]['title'])) { echo $getreqdetails[0]['title']; } else { 'Not Available'; } ?></td>
                <td style="padding:5px; min-height:200px;height:100%;"><?php if(!empty($getreqdetails[0]['req_type'])) { echo $getreqdetails[0]['req_type']; } else { 'Not Available'; } ?></td>
                <td style="padding:5px; min-height:200px;height:100%;"><?php echo CURRENCY; ?><?php if(!empty($getofferdetails[0]['Final_price'])) { echo $getofferdetails[0]['Final_price']; } else { 'Not Available'; } ?> </td>
                <td style="padding:5px; min-height:200px;height:100%;"><?php if(!empty($getofferdetails[0]['Quantity'])) { echo $getofferdetails[0]['Quantity']; } else { 'Not Available'; } ?> </td>
                <td style="padding:5px; height:200px;"><?php echo CURRENCY; ?><?php if(!empty($getofferdetails[0]['Final_price']) && !empty($getofferdetails[0]['Quantity'])) { echo $getofferdetails[0]['Final_price']*$getofferdetails[0]['Quantity']; } else { 'Not Available'; } ?></td>
              </tr>
             
              
            </table>
                </td>
               
              </tr>
            
              
            </table>
          </td>
         
        </tr>
        
         <tr>
          <td colspan="2" style="text-align:center; height:20px; ">
           &nbsp;
            </td>
          </tr>
        
        <tr>
          <td colspan="2" style="text-align:center; ">
          <span style="font-size:40px;font-weight:bold;">CONDITIONS &nbsp;</span><span style="font-size:20px;"> (to be completed by the <?php echo$this->session->userdata('user_type'); ?>)</span>
            </td>
          </tr>
        
        <tr>
          <td colspan="2">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:28px; font-family:Arial, Helvetica, sans-serif; ">
             <tr>
                <td>
                    <table width="100%" border="1" cellspacing="0" cellpadding="5" style="font-size:28px; font-family:Arial, Helvetica, sans-serif; ">
              <tr>
                <td style="padding:5px;">Merchandise Description </td>
                <td style="padding:5px;"><?php if(!empty($getofferdetails[0]['Merchandise_Description'])) { echo $getofferdetails[0]['Merchandise_Description']; } else { 'Not Available'; } ?></td>
              </tr>
              <tr>
                <td style="padding:5px;">Transport </td>
                <td style="padding:5px;"><?php if(!empty($getofferdetails[0]['Transport'])) { echo $getofferdetails[0]['Transport']; } else { 'Not Available'; } ?></td>
              </tr>
              <tr>
                <td style="padding:5px;">Delivery Date : </td>
                <td style="padding:5px;"><?php echo date('F j, Y', strtotime($getofferdetails[0]['Delivery_Date'])); ?> </td>
              </tr>
              <tr>
                <td style="padding:5px;">Order type : </td>
                <td style="padding:5px;"><?php if(!empty($getofferdetails[0]['Order_type'])) { echo $getofferdetails[0]['Order_type']; } else { 'Not Available'; } ?></td>
              </tr>
              <tr>
                <td style="padding:5px;">Payment Currency : </td>
                <td style="padding:5px;"><?php if(!empty($getofferdetails[0]['Payment_Currency'])) { echo $getofferdetails[0]['Payment_Currency']; } else { 'Not Available'; } ?></td>
              </tr>
              <tr>
                <td style="padding:5px;">Payment Conditions : </td>
                <td style="padding:5px;"><?php if(!empty($getofferdetails[0]['Payment_Conditions'])) { echo $getofferdetails[0]['Payment_Conditions']; } else { 'Not Available'; } ?></td>
              </tr>
              <tr>
                <td style="padding:5px;">Packing Weight : </td>
                <td style="padding:5px;"><?php if(!empty($getofferdetails[0]['Packing_Weight'])) { echo $getofferdetails[0]['Packing_Weight']; } else { 'Not Available'; } ?></td>
              </tr>
              <tr>
                <td style="padding:5px;">Trade confirmed by : </td>
                <td style="padding:5px;"><?php if(!empty($getofferdetails[0]['Trade_confirmed_by'])) { echo $getofferdetails[0]['Trade_confirmed_by']; } else { 'Not Available'; } ?></td>
              </tr>
            </table>
                </td>
              </tr>
            </table>
          </td>
         
        </tr>
       
        <tr>
          <td colspan="2" style="text-align:center; height:20px; ">
           &nbsp;
            </td>
          </tr>
<tr><td colspan="2" style="padding:5px; text-align:center; color:#a2a2a2;">
    <i>Copyright <?php echo PROJECT_NAME; ?>  <?php echo date('Y'); ?>. All Rights Reserved Terms & Privacy</i>
</td></tr>
        
      </table>
    </td>
  </tr>
<br><br>
</table>