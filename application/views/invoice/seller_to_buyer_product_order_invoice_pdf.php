<table width="100%" border="0" cellspacing="5" cellpadding="0"><tr><td></td></tr>
</table>
<table width="100%" border="0" cellspacing="5" cellpadding="0" style="border:1px solid #ddd;font-family:Arial, Helvetica, sans-serif;">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
           <td width="5%">&nbsp;</td>
           <td width="20%" ><a href="#"><img  src="<?php echo base_url(); ?>front-asset/images/logo-login.png" alt="logo" /></a></td>
          <td width="20%" style=" text-align:center; font-size:42px;">
            &nbsp;
          </td>
                    <td width="65%">
                        
                        <h3 style="border-radius:40px;width:250px;text-align: left;color: #333;font-size: 46px;letter-spacing: 0.4px;  text-transform: uppercase;font-weight: 600; margin-left:0px; display:block">ORDER NOTE</h3>
                    </td>
          </tr>
          <tr>
              <td colspan="3"height="30px"></td>
          </tr>
      </table>
      <table width="100%" border="0" cellspacing="5" cellpadding="0" style="font-size:28px; font-family:Arial, Helvetica, sans-serif; ">
        <tr>
          <td >
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:28px; font-family:Arial, Helvetica, sans-serif;">
              <tr >
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:28px; font-family:Arial, Helvetica, sans-serif;">
                    <tr> <td><b>From:</b></td> </tr>                   
                    <tr> <td><b>Buyer Name:</b>  <?php if(!empty($this->session->userdata('user_name'))) { echo $this->session->userdata('user_name'); } else { 'Not Available'; } ?></td> </tr>                   
                    <tr> <td><b>Delivery Address :</b>  <?php if(!empty($this->session->userdata('user_address'))) { echo $this->session->userdata('user_address'); } else { 'Not Available'; } ?></td> </tr>
                    <tr> <td><b>Phone no:</b> <?php if(!empty($this->session->userdata('user_mobile'))) { echo $this->session->userdata('user_mobile'); } else { 'Not Available'; } ?></td> </tr>
                    <tr> <td><b>Email:</b>  <?php if(!empty($this->session->userdata('user_email'))) { echo $this->session->userdata('user_email'); } else { 'Not Available'; } ?></td> </tr>
                  </table>
                </td>
                
              </tr>
            </table>
          </td>
          <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:28px; font-family:Arial, Helvetica, sans-serif;">
              <tr >
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:28px; font-family:Arial, Helvetica, sans-serif;">
                    <tr> <td><b>To:</b></td> </tr>   
                    <tr> <td><b>Seller Name:</b>  <?php if(!empty($get_user_data[0]['name'])) { echo  $get_user_data[0]['name']; } else { 'Not Available'; } ?></td> </tr>                   
                    <tr> <td><b>Delivery Country :</b>  <?php if(!empty($get_user_data[0]['address'])) { echo $get_user_data[0]['address']; } else { 'Not Available'; } ?></td></tr>
                    <tr> <td><b>Phone number:</b> <?php if(!empty($get_user_data[0]['mobile_number'])) { echo $get_user_data[0]['mobile_number']; } else { 'Not Available'; } ?></td> </tr>
                    <tr> <td><b>Email:</b><?php if(!empty($get_user_data[0]['email'])) { echo $get_user_data[0]['email']; } else { 'Not Available'; } ?></td> </tr>
                  </table>
                </td>
              
              </tr>
            </table> 
          </td>
        </tr>
        <!--       Table2-->
        <tr>
          <td colspan="2" style="text-align:center; height:10px; ">
           &nbsp;
            </td>
          </tr>
       <tr>
           <td colspan="2" style="font-size:28px;text-align:center;height:30px;">
               <!-- (to be completed by the buyer) -->
           </td>
       </tr>
       
        <tr>
          <td colspan="2">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:28px; font-family:Arial, Helvetica, sans-serif; ">
             <tr>
                <td>
                    <table width="100%" border="1" cellspacing="0" cellpadding="5" style="font-size:28px; font-family:Arial, Helvetica, sans-serif; ">
               <tr>
                <td style="padding:5px; background-color: #00B0F0; color:#fff;"><b>  Product Name</b></td>
                <td style="padding:5px; background-color: #00B0F0; color:#fff;"><b>  Type</b></td>
                <td style="padding:5px; background-color: #00B0F0; color:#fff;"><b>  Final Price</b></td>
                <td style="padding:5px; background-color: #00B0F0; color:#fff;"><b>  Quantity</b></td>
                <td style="padding:5px; background-color: #00B0F0; color:#fff;"><b>  Total Price</b></td>
              </tr>
              
              <tr>
                <td style="padding:5px; min-height:200px;height:100%;"><?php if(!empty($product_name)) { echo $product_name; } else { 'Not Available'; } ?></td>
                <td style="padding:5px; min-height:200px;height:100%;">Product</td>
                <td style="padding:5px; min-height:200px;height:100%;"><?php echo CURRENCY; ?><?php if(!empty($pdf_data['Final_price'])) { echo $pdf_data['Final_price']; } else { 'Not Available'; } ?> </td>
                <td style="padding:5px; min-height:200px;height:100%;"><?php if(!empty($pdf_data['Quantity'])) { echo $pdf_data['Quantity']; } else { 'Not Available'; } ?> </td>
                <td style="padding:5px; height:200px;"><?php echo CURRENCY; ?><?php if(!empty($pdf_data['Final_price']) && !empty($pdf_data['Quantity'])) { echo $pdf_data['Final_price']*$pdf_data['Quantity']; } else { 'Not Available'; } ?></td>
              </tr>
             
              
            </table>
                </td>
               
              </tr>
            
              
            </table>
          </td>
         
        </tr>
        
         <tr>
          <td colspan="2" style="text-align:center; height:20px; ">
           &nbsp;
            </td>
          </tr>
        
        <tr>
          <td colspan="2" style="text-align:center; ">
          <span style="font-size:40px;font-weight:bold;">CONDITIONS &nbsp;</span><span style="font-size:20px;"> (to be completed by the <?php echo$this->session->userdata('user_type'); ?>)</span>
            </td>
          </tr>
        
        <tr>
          <td colspan="2">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:28px; font-family:Arial, Helvetica, sans-serif; ">
             <tr>
                <td>
                    <table width="100%" border="1" cellspacing="0" cellpadding="5" style="font-size:28px; font-family:Arial, Helvetica, sans-serif; ">
              <tr>
                <td style="padding:5px;">Merchandise Description </td>
                <td style="padding:5px;"><?php if(!empty($pdf_data['Merchandise_Description'])) { echo $pdf_data['Merchandise_Description']; } else { 'Not Available'; } ?></td>
              </tr>
              <tr>
                <td style="padding:5px;">Transport </td>
                <td style="padding:5px;"><?php if(!empty($pdf_data['Transport'])) { echo $pdf_data['Transport']; } else { 'Not Available'; } ?></td>
              </tr>
              <tr>
                <td style="padding:5px;">Delivery Date : </td>
                <td style="padding:5px;"><?php echo date('F j, Y', strtotime($pdf_data['Delivery_Date'])); ?> </td>
              </tr>
              <tr>
                <td style="padding:5px;">Order type : </td>
                <td style="padding:5px;"><?php if(!empty($pdf_data['Order_type'])) { echo $pdf_data['Order_type']; } else { 'Not Available'; } ?></td>
              </tr>
              <tr>
                <td style="padding:5px;">Payment Currency : </td>
                <td style="padding:5px;"><?php if(!empty($pdf_data['Payment_Currency'])) { echo $pdf_data['Payment_Currency']; } else { 'Not Available'; } ?></td>
              </tr>
              <tr>
                <td style="padding:5px;">Payment Conditions : </td>
                <td style="padding:5px;"><?php if(!empty($pdf_data['Payment_Conditions'])) { echo $pdf_data['Payment_Conditions']; } else { 'Not Available'; } ?></td>
              </tr>
              <tr>
                <td style="padding:5px;">Packing Weight : </td>
                <td style="padding:5px;"><?php if(!empty($pdf_data['Packing_Weight'])) { echo $pdf_data['Packing_Weight']; } else { 'Not Available'; } ?></td>
              </tr>
              <tr>
                <td style="padding:5px;">Trade confirmed by : </td>
                <td style="padding:5px;"><?php if(!empty($pdf_data['Trade_confirmed_by'])) { echo $pdf_data['Trade_confirmed_by']; } else { 'Not Available'; } ?></td>
              </tr>
            </table>
                </td>
              </tr>
            </table>
          </td>
         
        </tr>
       
        <tr>
          <td colspan="2" style="text-align:center; height:20px; ">
           &nbsp;
            </td>
          </tr>
<tr><td colspan="2" style="padding:5px; text-align:center; color:#a2a2a2;">
    <i>Copyright <?php echo PROJECT_NAME; ?>  <?php echo date('Y'); ?>. All Rights Reserved Terms & Privacy</i>
</td></tr>
        
      </table>
    </td>
  </tr>
<br><br>
</table>