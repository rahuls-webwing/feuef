<div class="page-head-block">
    <div class="page-head-overlay"></div>
    <div class="container">
        <div class="head-txt-block">
            Pro Network
        </div>
    </div>
</div>
<div class="container">
    <div class="col-sm-8 col-md-8 col-lg-8">
        
      <?php 
        if(count($blogs)>0) { 
          foreach ($blogs as $blogs_row) {
            
            $this->db->where('id' , $blogs_row['Bolg_user_id']);
            $get_user = $this->master_model->getRecords('tbl_user_master');
            ?>
                <div class="block-one-main">
                    <div class="blog-one-head">
                        <a href="<?php echo base_url().'blogs/details/'.$blogs_row['blogs_id'];?>"> <?php echo $blogs_row['blogs_name_en']; ?></a>
                    </div>
                    <div class="blog-img-one">
                        <?php  if(!empty($blogs_row['blogs_img']) && file_exists('uploads/blogs_images/'.$blogs_row['blogs_img'])){?>
                         <img class="event_pic " src="<?php echo base_url().'timthumb.php?src='.base_url().'uploads/blogs_images/'.$blogs_row['blogs_img'].'&h=300&w=555&zc=0'; ?>" alt="" data-description="">
                        <?php } else { ?>
                        <!-- <img src="<?php echo base_url().'images/large_no_image.jpg'; ?>" alt="" /> -->
                        <?php } ?> 

                        <?php  if(!empty($blogs_row['blogs_img']) && file_exists('uploads/blogs_images/'.$blogs_row['blogs_img'])){?>
                             <div class="event_icon">
                                <strong><?php echo date( "d", strtotime( $blogs_row['blogs_added_date'] ) ); ?></strong>
                                <span><?php echo date( "F", strtotime( $blogs_row['blogs_added_date'] ) ); ?></span>
                             </div>
                        <?php } else { ?>

                        <?php } ?> 
                        
                        <?php 
                                $where_arr = array('message_read'=>'1','comm_blog_id'=>$blogs_row['blogs_id']);
                                $data['blogs_comments_data'] = $this->master_model->getRecords('tbl_blogs_comments',$where_arr);
                                ?>
                        <?php  if(!empty($blogs_row['blogs_img']) && file_exists('uploads/blogs_images/'.$blogs_row['blogs_img'])){?>
                            <div class="comm-add-lab">
                                <div class="admin-block">
                                   <span><img src="<?php echo base_url(); ?>front-asset/images/blog-user.png" alt="" /></span> <span>By <?php if(!empty($get_user[0]['name'])){ echo $get_user[0]['name']; } else { echo $blogs_row['blogs_added_by']; } ?> </span>
                                </div>
                                <div class="admin-block">
                                    <span><img src="<?php echo base_url(); ?>front-asset/images/blog-chat.png" alt="" /></span> <span><?php echo count($data['blogs_comments_data']); ?></span>
                                </div>
                                <div class="clr"></div>
                            </div>    
                        <?php } else { ?>
                        <?php } ?>     
                    </div>            
                    <div class="blog-content">
                        <?php  echo $blogs_row['blogs_description_en']; ?>
                        <div class="read-more-block-blog">
                            <a href="<?php echo base_url().'blogs/details/'.$blogs_row['blogs_id'];?>">
                             <span>Read More</span><span><i class="fa fa-long-arrow-right"></i></span>
                            </a>
                        </div>

                        <?php  if(!empty($blogs_row['blogs_img']) && file_exists('uploads/blogs_images/'.$blogs_row['blogs_img'])){?>
                            
                        <?php } else { ?>

                            <div class="content-blog-no-img">
                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                  <span class="calndr-blogs">
                                    <?php echo date( "d", strtotime( $blogs_row['blogs_added_date'] ) ); ?>
                                    <?php echo date( "F", strtotime( $blogs_row['blogs_added_date'] ) ); ?>
                                  </span>       
                                <span><i class="fa fa-user" aria-hidden="true"></i></span>  
                                  <span>By <?php if(!empty($get_user[0]['name'])){ echo $get_user[0]['name']; } else { echo $blogs_row['blogs_added_by']; } ?>
                                </span>  
                                <span>
                                    <i class="fa fa-comment" aria-hidden="true"></i>
                                </span>
                                <span><?php echo count($data['blogs_comments_data']); ?></span>         
                            </div>

                        <?php } ?> 
                            


                    </div>
                </div>
             <?php } 
         } else {
            ?> 
            <div class="block-one-main" style="margin-top: 65px;">
             <?php 
                 $this->load->view('no-data-found');
             ?>
            </div>      
        <?php  }?>
       <!--pagigation start here-->
        <div style="margin-top:-12px;">
          <?php echo $this->pagination->create_links(); ?>
        </div>
        <!--pagigation end here-->
    </div>
    <div class="col-sm-4 col-md-4 col-lg-4">
        <div class="">
            <div class="">
                <?php 
                if(!empty($this->session->userdata('user_type')) && $this->session->userdata('user_type') == "Seller") {?>
                <a href="<?php echo base_url().'blogs/add';?>"><button class="change-btn-pass ">Add Post</button></a>
                <?php } else  if(!empty($this->session->userdata('user_type')) && $this->session->userdata('user_type') == "Buyer") { ?>
                <a href="<?php echo base_url().'blogs/add';?>"><button class="change-btn-pass ">Add Post</button></a>
                <?php } else {?>
                <a href="<?php echo base_url().'login';?>"><button class="change-btn-pass ">Add Post</button></a>
                <?php } ?>
            </div>
        </div>



        <div class="res-posts-block">
            <div class="cate-right-head">
                Recent Posts
            </div>
            
            <?php 
            if(count($recblogs)>0) { 
              $cnt=0; foreach ($recblogs as $blogs_row) { $cnt++;
                if($cnt <=5){
                ?>
                    <div class="posts-block">
                        <div class="post-img-block">
                            <?php if(!empty($blogs_row['blogs_img']) && file_exists('uploads/blogs_images/'.$blogs_row['blogs_img'])){ ?>
                             <img style="height:50px;width:70px;" src="<?php echo base_url().'timthumb.php?src='.base_url().'uploads/blogs_images/'.$blogs_row['blogs_img'].'&h=300&w=555&zc=0'; ?>" alt="" data-description="">
                            <?php } else { ?>
                            <img style="height:50px;width:70px;" src="<?php echo base_url().'images/large_no_image.jpg'; ?>" alt="" />
                            <?php } ?> 
                        </div>
                        <div class="post-content">
                            <div class="post-content-head">
                               <a href="<?php echo base_url().'blogs/details/'.$blogs_row['blogs_id'];?>"> <?php echo $blogs_row['blogs_name_en']; ?></a>
                            </div>
                            <div class="post-date">
                                 <?php echo date( "F j, Y", strtotime( $blogs_row['blogs_added_date'] ) ); ?>
                            </div>
                        </div>                                                                                          
                        <div class="clr"></div>
                    </div>
                <?php } } }  else { ?> 
                <div class="block-one-main" style="margin-top: 65px;">
                 <?php 
                     $this->load->view('no-data-found');
                 ?>
                </div>      
            <?php  } ?>


        </div>
    </div>
</div>
