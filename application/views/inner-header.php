<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php $this->load->view('cometchatjs'); ?>
<style type="text/css">
#inner-header .header-icon-text, #user-inner-header .header-icon-text{position: relative;}
.req-count {background: #16a085;border-radius: 50%;color: #fff;height: 22	px;line-height: 23px;position: absolute; right: -11px; text-align: center; top: -15px; width: 22px; font-size: 12px;}
.req-count > a {color: #fff;font-size:12px;}

.noti-count {background: #26c6da;border-radius: 50%;color: #fff;height: 22	px;line-height: 23px;position: absolute; right: -11px; text-align: center; top: -15px; width: 22px; font-size: 12px;}
.noti-count > a {color: #fff;font-size:12px;}

.revi-count {background: #e05d6f;border-radius: 50%;color: #fff;height: 22	px;line-height: 23px;position: absolute; right: -11px; text-align: center; top: -15px; width: 22px; font-size: 12px;}
.revi-count > a {color: #fff;font-size:12px;}

.msg-count {background: #f9a825;border-radius: 50%;color: #fff;height: 22	px;line-height: 23px;position: absolute; right: -11px; text-align: center; top: -15px; width: 22px; font-size: 12px;}
.msg-count > a {color: #fff;font-size:12px;}
</style>
<!--<div class="main-banner-block">-->
<div class="header header-home" set-class-when-at-top="fix-to-top" ng-controller="HeaderCntrl">
	<div class="logo-block wow fadeInDown" data-wow-delay="0.2s">
		<a href="<?php echo base_url(); ?>home">
			<img src="<?php echo base_url(); ?>images/fevicon/favicon.png" style="width: 50%;" alt="" class="main-logo" />
		</a>
	</div>
	<span class="menu-icon" ng-click="openNav()">&#9776;</span>
	<!--Menu Start-->
	<div id="mySidenav" class="sidenav">
		<a href="javascript:void(0)" class="closebtn" ng-click="closeNav()">&times;</a>
		<div class="banner-img-block">
			<img src="<?php echo base_url(); ?>images/fevicon/favicon.png" alt="" />
		</div>
		<ul class="min-menu">			
           <!-- <li class="lang" ng-controller="ContryDropdown">
				<div class="wrapper-dropdown-1" id="dd">
					<span class="arrow"><i class="fa fa-angle-down"></i></span>
					<span class="place-name" id="new_word">							
					    Location...				
					</span>
					<ul class="dropdown city-drop">
					<img class="img-arrow-menu" src="<?php echo base_url(); ?>front-asset/images/menu-arrow-ul.png" alt="" />
						<li >
							<a href="#">								
								<span>Mumbai</span>
							</a>
						</li>
						<li >
							<a data-flag="dutch" href="#">								
								<span>Rajasthan</span>
							</a>
						</li>
					</ul>
				</div>
			</li>   -->        

			

            <?php if(!empty($this->session->userdata('user_type')) && $this->session->userdata('user_type') == "Seller") {
			  ?>
			     <li class="becom-seller"><a data-hover="Live&nbsp;Market&nbsp;" href="<?php echo base_url().'marketplace' ?>">Live&nbsp;Market&nbsp;</a></li>
			     <li class="becom-seller"><a data-hover="Search&nbsp;Requirements&nbsp;" href="<?php echo base_url().'search/requirments' ?>">Search&nbsp;Requirements&nbsp;</a></li>
			  <?php
			} else if(!empty($this->session->userdata('user_type')) && $this->session->userdata('user_type') == "Buyer") {
			  ?>
			    <li class="becom-seller"><a data-hover="Live&nbsp;Market&nbsp;" href="<?php echo base_url().'marketplace' ?>">Live&nbsp;Market&nbsp;</a></li>
			    <li class="becom-seller"><a data-hover="Search&nbsp;Sellers&nbsp;<" href="<?php echo base_url().'search?seller=' ?>">Search&nbsp;Products&nbsp;</a></li>
			  <?php
			} else {
              ?>
                <!-- <li class="becom-seller"><a data-hover="Become&nbsp;a&nbsp;Seller" href="">Become&nbsp;a&nbsp;Seller</a></li>
                <li class="becom-buyer"><a data-hover="Become&nbsp;a&nbsp;buyer" href="">Become&nbsp;a&nbsp;buyer</a></li> -->
              <?php
			}
			?> 

			<li><a data-hover="Pro Network" href="<?php echo base_url().'blogs'; ?>">Pro Network</a></li>


            <!-- review -->

            <?php 
            if(!empty($this->session->userdata('user_type')) && $this->session->userdata('user_type') == "Seller") {
            $this->db->where('status' , 'Unblock');
            $this->db->where('is_read' , 'no');
            $this->db->where('tbl_seller_rating.seller_id' , $this->session->userdata('user_id'));
            $get_revi_count  = $this->master_model->getRecordCount('tbl_seller_rating');
           
            if($get_revi_count <= 9){ $revi_cnt = '0'.$get_revi_count; } else if($get_revi_count >= 10) { $revi_cnt = '10+'; }
            ?>
			<li class="hide-mobile-block">
			<a href="<?php echo base_url().lcfirst($this->session->userdata('user_type')).'/reviews'; ?>" title="<?php echo $revi_cnt; ?> new reviews">
			<span class="header-icon-text hidden-xs" style="top: -10px;"><i class="fa fa-comments-o"></i>
			<span class="revi-count" style="top: -17px;"><?php echo $revi_cnt; ?></span>
			</span>
			</a>
			</li>
			<?php } ?>
			<!--end  noti -->


			<!-- msg -->
			<?php /*
            <?php 
            $this->db->where('read' , '0');
            $this->db->where('cometchat.to' , $this->session->userdata('user_id'));
            $get_chat_count  = $this->master_model->getRecordCount('cometchat');
            if($get_chat_count <= 9){ $chat_cnt = '0'.$get_chat_count; } else if($get_chat_count >= 10) { $chat_cnt = '10+'; }
            ?>
            <li class="open_chat">
			<a href="#" title="<?php echo $chat_cnt; ?> new messages">
			<span class="header-icon-text" style="top: -10px;"><i class="fa fa-envelope-o"></i>
			<span class="msg-count" style="top: -17px;"><?php echo $chat_cnt; ?></span>
			</span>
			</a>
			</li>
			*/?>
			<!--end  msg -->


            <!-- msg -->
            <?php
            if(!empty($this->session->userdata('user_type')) && $this->session->userdata('user_type') == "Seller") {
	            $this->db->where('status' , 'Unblock');
	            $this->db->where('is_read' , 'no');
	            $this->db->where('tbl_seller_contact_inquires.seller_id' , $this->session->userdata('user_id'));
	            $get_inq_count  = $this->master_model->getRecordCount('tbl_seller_contact_inquires');
	            if($get_inq_count <= 9){ $inq_cnt = '0'.$get_inq_count; } else if($get_inq_count >= 10) { $inq_cnt = '10+'; }
	            ?>
	            <li class="hide-mobile-block">
				<a href="<?php echo base_url().lcfirst($this->session->userdata('user_type')).'/my_inquires'; ?>" title="<?php echo $inq_cnt; ?> new inquiry's">
				<span class="header-icon-text hidden-xs" style="top: -10px;"><i class="fa fa-suitcase"></i>
				<span class="req-count" style="top: -17px;"><?php echo $inq_cnt; ?></span>
				</span>
				</a>
				</li>
		    <?php } ?>		
			<!--end  msg -->

            <!-- noti -->

            <?php 
            $this->db->where('status' , 'Unblock');
            $this->db->where('is_read' , 'no');
            if(!empty($this->session->userdata('user_type')) && $this->session->userdata('user_type') == "Seller") {
            $this->db->where('tbl_seller_notifications.seller_id' , $this->session->userdata('user_id')); 	
            $get_noti_count  = $this->master_model->getRecordCount('tbl_seller_notifications');
            } else {

            $this->db->where('tbl_buyer_notifications.buyer_id' , $this->session->userdata('user_id')); 	
            $get_noti_count  = $this->master_model->getRecordCount('tbl_buyer_notifications');	
            } 

            if($get_noti_count <= 9){ $noti_cnt = '0'.$get_noti_count; } else if($get_noti_count >= 10) { $noti_cnt = '10+'; }
            ?>
			<li class="hide-mobile-block">
			<a href="<?php echo base_url().lcfirst($this->session->userdata('user_type')).'/notifications'; ?>" title="<?php echo $noti_cnt; ?> new notifications">
			<span class="header-icon-text hidden-xs" style="top: -10px;"><i class="fa fa-bell-o"></i>
			<span class="noti-count" style="top: -17px;"><?php echo $noti_cnt; ?></span>
			</span>
			</a>
			</li>
			<!--end  noti -->

            <li class="sub-menu name-block-user front-hide">
				<a href="" class="drop-block block-drop-new">
					<span class="img-block-user">

						<?php if(!empty($this->session->userdata('user_image')) && file_exists('images/'.lcfirst($this->session->userdata('user_type')).'_image/'.$this->session->userdata('user_image'))){?>
						<img   src="<?php echo base_url().'images/'.lcfirst($this->session->userdata('user_type')).'_image/'.$this->session->userdata('user_image'); ?>" alt="" />
						<?php } else { ?>
						<img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
						<?php } ?> 

					</span><span class="name-client-container"> 
					    <?php if(!empty($this->session->userdata('user_name'))) {
                            echo $this->session->userdata('user_name'); 
                          } else { 
                            echo "Not Available"; 
                          } 
                        ?> 
                    <i class="fa fa-angle-down"></i></span>
				</a>
				<ul class="su-menu" style="width:250px;"> 
					<img class="img-arrow-menu" src="<?php echo base_url(); ?>front-asset/images/menu-arrow-ul.png" alt="" />
					<li><a href="<?php echo base_url().lcfirst($this->session->userdata('user_type')).'/dashboard'; ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
					<li><a href="<?php echo base_url().lcfirst($this->session->userdata('user_type')).'/edit'; ?>"><i class="fa fa-pencil-square-o"></i>Edit Profille</a></li>
					<li><a href="<?php echo base_url().lcfirst($this->session->userdata('user_type')).'/change_password'; ?>"><i class="fa fa-unlock-alt"></i>Change Password</a></li>
					
                    <?php 
                    $this->db->where('pricing_name'  , 'Premium');
					$this->db->where('membership_id' , 2);
					$get_pay_amt = $this->master_model->getRecords('tbl_pricing_master');
					?>
					<?php if(!empty($this->session->userdata('user_type')) && $this->session->userdata('user_type') == "Seller") {
				    ?>
					<li><a href="<?php echo base_url().'purchase/for_product'; ?>" title="<?php if(isset($get_pay_amt[0]['upload_qty'])) { echo 'Get more '.$get_pay_amt[0]['upload_qty'].' products for upload'; } else { echo "Not Available"; } ?>"><i class="fa fa-dropbox" aria-hidden="true"></i><?php if(isset($get_pay_amt[0]['upload_qty'])) { echo 'Get '.$get_pay_amt[0]['upload_qty'].' More Products'; } else { echo "Not Available"; } ?></a></li>
				    <li><a href="<?php echo base_url().'purchase/for_offers'; ?>" title="<?php if(isset($get_pay_amt[0]['upload_qty'])) { echo 'Get more '.$get_pay_amt[0]['upload_qty'].' make offer requests (from live market to buyer requirements)'; } else { echo "Not Available"; } ?>"><i class="fa fa-dropbox" aria-hidden="true"></i><?php if(isset($get_pay_amt[0]['upload_qty'])) { echo 'Get '.$get_pay_amt[0]['upload_qty'].' More Offer Request'; } else { echo "Not Available"; } ?></a></li>
				    <?php } ?>


				    <?php if(!empty($this->session->userdata('user_type')) && $this->session->userdata('user_type') == "Buyer") {
				    ?>
					<li><a href="<?php echo base_url().'purchase/post_requirements'; ?>" title="<?php if(isset($get_pay_amt[0]['upload_qty'])) { echo 'Get more '.$get_pay_amt[0]['upload_qty'].' requirements for post'; } else { echo "Not Available"; } ?>"><i class="fa fa-dropbox" aria-hidden="true"></i><?php if(isset($get_pay_amt[0]['upload_qty'])) { echo 'Get '.$get_pay_amt[0]['upload_qty'].' More Requirements'; } else { echo "Not Available"; } ?></a></li>
				    <li><a href="<?php echo base_url().'purchase/market_offers'; ?>" title="<?php if(isset($get_pay_amt[0]['upload_qty'])) { echo 'Get more '.$get_pay_amt[0]['upload_qty'].' make offer requests (from live market to sellers offers)'; } else { echo "Not Available"; } ?>"><i class="fa fa-dropbox" aria-hidden="true"></i><?php if(isset($get_pay_amt[0]['upload_qty'])) { echo 'Get '.$get_pay_amt[0]['upload_qty'].' More Offer Request'; } else { echo "Not Available"; } ?></a></li>

				    <?php } ?>
					<li><a href="<?php echo base_url().lcfirst($this->session->userdata('user_type')).'/logout'; ?>"><i class="fa fa-sign-out"></i>Logout</a></li>
				</ul>				
			</li>
		</ul>
		<div class="clr"></div>
	</div>
	<ul class="mobile-noti-block">
	      <!-- review -->

            <?php 
            if(!empty($this->session->userdata('user_type')) && $this->session->userdata('user_type') == "Seller") {
            $this->db->where('status' , 'Unblock');
            $this->db->where('is_read' , 'no');
            $this->db->where('tbl_seller_rating.seller_id' , $this->session->userdata('user_id'));
            $get_revi_count  = $this->master_model->getRecordCount('tbl_seller_rating');
           
            if($get_revi_count <= 9){ $revi_cnt = '0'.$get_revi_count; } else if($get_revi_count >= 10) { $revi_cnt = '10+'; }
            ?>
			<li>
			<a href="<?php echo base_url().lcfirst($this->session->userdata('user_type')).'/reviews'; ?>" title="<?php echo $revi_cnt; ?> new reviews">
			<span class="header-icon-text" style="top: -10px;"><i class="fa fa-comments-o"></i>
			<span class="revi-count" style="top: -17px;"><?php echo $revi_cnt; ?></span>
			</span>
			</a>
			</li>
			<?php } ?>
			<!--end  noti -->

			<!-- msg -->
			<?php /*
            <?php 
            $this->db->where('read' , '0');
            $this->db->where('cometchat.to' , $this->session->userdata('user_id'));
            $get_chat_count  = $this->master_model->getRecordCount('cometchat');
            if($get_chat_count <= 9){ $chat_cnt = '0'.$get_chat_count; } else if($get_chat_count >= 10) { $chat_cnt = '10+'; }
            ?>
            <li class="open_chat">
			<a href="#" title="<?php echo $chat_cnt; ?> new messages">
			<span class="header-icon-text" style="top: -10px;"><i class="fa fa-envelope-o"></i>
			<span class="msg-count" style="top: -17px;"><?php echo $chat_cnt; ?></span>
			</span>
			</a>
			</li>
			*/?>
			<!--end  msg -->

            <!-- msg -->
            <?php
            if(!empty($this->session->userdata('user_type')) && $this->session->userdata('user_type') == "Seller") {
	            $this->db->where('status' , 'Unblock');
	            $this->db->where('is_read' , 'no');
	            $this->db->where('tbl_seller_contact_inquires.seller_id' , $this->session->userdata('user_id'));
	            $get_inq_count  = $this->master_model->getRecordCount('tbl_seller_contact_inquires');
	            if($get_inq_count <= 9){ $inq_cnt = '0'.$get_inq_count; } else if($get_inq_count >= 10) { $inq_cnt = '10+'; }
	            ?>
	            <li>
				<a href="<?php echo base_url().lcfirst($this->session->userdata('user_type')).'/my_inquires'; ?>" title="<?php echo $inq_cnt; ?> new inquiry's">
				<span class="header-icon-text" style="top: -10px;"><i class="fa fa-suitcase"></i>
				<span class="req-count" style="top: -17px;"><?php echo $inq_cnt; ?></span>
				</span>
				</a>
				</li>
		    <?php } ?>		
			<!--end  msg -->

            <!-- noti -->

            <?php 
            $this->db->where('status' , 'Unblock');
            $this->db->where('is_read' , 'no');
            if(!empty($this->session->userdata('user_type')) && $this->session->userdata('user_type') == "Seller") {

            $this->db->where('tbl_seller_notifications.seller_id' , $this->session->userdata('user_id')); 	
            $get_noti_count  = $this->master_model->getRecordCount('tbl_seller_notifications');
            } else {

            $this->db->where('tbl_buyer_notifications.buyer_id' , $this->session->userdata('user_id')); 	
            $get_noti_count  = $this->master_model->getRecordCount('tbl_buyer_notifications');	
            } 
            if($get_noti_count <= 9){ $noti_cnt = '0'.$get_noti_count; } else if($get_noti_count >= 10) { $noti_cnt = '10+'; }
            ?>
			<li>
			<a href="<?php echo base_url().lcfirst($this->session->userdata('user_type')).'/notifications'; ?>" title="<?php echo $noti_cnt; ?> new notifications">
			<span class="header-icon-text" style="top: -10px;"><i class="fa fa-bell-o"></i>
			<span class="noti-count" style="top: -17px;"><?php echo $noti_cnt; ?></span>
			</span>
			</a>
			</li>
			<!--end  noti -->
	</ul>
	<div class="clr"></div>
</div>
<!--</div>-->
<script type="text/javascript">
$(document).ready(function(){
	$('.open_chat').click(function(){
		$('#cometchat_hidden_content').click();
		setTimeout(function(){
			$('#cometchat_userstab').click();
		},100);
	});
});
</script>