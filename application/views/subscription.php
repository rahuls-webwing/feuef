<div class="footer-col-block newsletter-main">
        <div class="container">


            <!-- <div class="newsletter-head">
                Our Market report
            </div>
            <div class="newsletter-content-txt">
                Follow international market news, prices and features about our Live market
            </div> -->

            <?php
                $dynamic  = $this->master_model->getRecords('tbl_dynamic_pages',array('slug'=>'home-our-market-report'));
                if(isset($dynamic[0]['page_description'])){ echo $dynamic[0]['page_description']; } 
            ?>
            
            <div class="newletter-input-block">
            	<input type="hidden" name="base_url" id="base_url" class="form-control" value="<?php echo base_url();?>"/>
                <div class="input-bx-block">
                    <input type="email" id="sub_email" name="sub_email" placeholder="youremail@domain.com" />                 
                <div class="error-msg-block ">
                <div class="form-error" style="color:rgb(169, 55, 55);" id="err_email_id" ></div>
                <div class="" style="color:green;" id="news_success"></div>
                <div class="form-error" style="color:rgb(169, 55, 55);" id="news_error"></div>
                </div>
                </div>
                <div class="subscrib-btn-block">
                    <button type="submit" id="subscribe" name="subscribe">Subscribe</button>
                </div>
            </div>
        </div>
</div>
<script type="text/javascript">
// Subscribe newsletters
$("#subscribe").on('click', function(){
	var sub_email=$("#sub_email").val();
	var site_url=$("#base_url").val();
	var filter = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;
	var flag=1;
	if(sub_email=="")
	{
	$('#err_email_id').html('Enter Email Address');
	  
	$('#err_email_id').show();
	$('#sub_email').focus();
	$('#sub_email').on('keyup', function(){
	$('#err_email_id').hide();
	  });
	flag=0;
	}
	else if(!filter.test(sub_email))
	{
	$('#err_email_id').html('Please enter valid email');

	$('#err_email_id').show();
	$('#sub_email').focus();
	$('#sub_email').on('keyup', function(){
	$('#err_email_id').hide();
	  });
	flag=0;
	}
	else
	{
	$('#err_email_id').html('');
	}
	if(flag==1)
	{
    ajaxindicatorstart();
	$.ajax({
	      url:site_url+'home/subscribe/',
	      type:'POST',
	      data:{sub_email:sub_email},
	      success:function(res)
	      {
	      	  ajaxindicatorstop();
	          if(res=="exists")
	          {
	            $("#news_success").hide();
	            $("#news_error").show();
	            $("#news_error").html("You are already subscribed.");
	            setTimeout(function(){
                 $("#news_error").html("");
                },2000);
	          }
	          else if(res=="error")
	          {
	            $("#news_success").hide();
	            $("#news_error").show();
	            $("#news_error").html("Please try again.");
	            setTimeout(function(){
                $("#news_error").html("");
                },2000);
	          }
	          else if(res=="notlogin")
	          {
	            $("#news_success").hide();
	            $("#news_error").show();
	            $("#news_error").html("Please login first.");
	            setTimeout(function(){
                    $("#news_error").html("");
	            },2000);
	          }
	          else if(res=="success")
	          {
	              $("#news_error").hide();
	              $("#news_success").show();
	              $("#news_success").html("Subscribed successfully.");
	              setTimeout(function(){
	              	$('#sub_email').val('');
                    $("#news_success").html("");
	              },2000);
	              
	          }
	      }
	  });
	}else
	{
	
	$("#news_success").hide();
	$("#news_error").hide();
	return false;
	}
	});
	//signip Experts form
</script>