<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php $this->load->view('subscription'); ?>
<div class="footer-block-main">
	<div class="container">
		<div class="row">
			<div class="footer-col-block">				
				<div class="col-sm-12 col-md-3 col-lg-3">
					<div class="footer_heading footer-col-head">
						<?php echo PROJECT_NAME; ?>
					</div>
					<div class="menu_name points-footer">
						<ul>
							<li><a href="<?php echo base_url().'cms/view/about-us'; ?>"><span><i class="fa fa-circle-o"></i></span> <span>About Us</span></a></li>
							<li><a href="<?php echo base_url().'cms/view/how-it-works'; ?>"><span><i class="fa fa-circle-o"></i></span> <span>How it Works</span></a></li>
							<li><a href="<?php echo base_url().'marketplace' ?>"><span><i class="fa fa-circle-o"></i></span> <span>Live market</span></a></li>
							<li><a href="<?php echo base_url().'pricing' ?>"><span><i class="fa fa-circle-o"></i></span> <span>Pricing</span></a></li>
							<li><a href="<?php echo base_url().'blogs' ?>"><span><i class="fa fa-circle-o"></i></span> <span>Pro Network</span></a></li>
							<li><a href="<?php echo base_url().'contact_us'; ?>"><span><i class="fa fa-circle-o"></i></span> <span>Contact Us</span></a></li>
						</ul>
					</div>
				</div>


				<?php   
                $qry_item   =  "SELECT tbl_category_master.category_id, 
                                      tbl_category_master.category_name, 
                                      COUNT(*) AS task_cat_id_count
                                      FROM tbl_buyer_post_requirement 
                                      JOIN tbl_category_master ON tbl_category_master.category_id = tbl_buyer_post_requirement.category_id
                                      WHERE tbl_category_master.category_status = '1'  
                                      AND tbl_buyer_post_requirement.requirment_status = 'open'         
                                      GROUP BY tbl_buyer_post_requirement.category_id
                                      ORDER BY task_cat_id_count DESC";

                $result_cat = $this->db->query($qry_item)->result_array();                       
                                  
               ?>
				<div class="col-sm-12 col-md-3 col-lg-3">
					<div class="footer_heading  footer-col-head">Popular Requirement's Category </div>
						<div class="menu_name points-footer">
							 <ul>
							 
							 <?php
							 $i=0; 
							 if(!empty($result_cat))
							 { 
							    foreach($result_cat as $row_cat)
							    {
							       if($i<=5)
							       {
							       ?>
			              
							        <li><a href="<?php echo base_url().'search/requirments?cat_id%5B%5D='.$row_cat['category_id']; ?>"><span><i class="fa fa-circle-o"></i></span> <span><?php echo $row_cat['category_name']; ?></span></a></li>
							       <?php } $i++; 
						        }
							 } ?>
							          
							 </ul>
						</div>
					</div>



				<?php   
                $qry_item   =  "SELECT tbl_category_master.category_id, 
                                      tbl_category_master.category_name, 
                                      COUNT(*) AS task_cat_id_count
                                      FROM tbl_seller_upload_product 
                                      JOIN tbl_category_master ON tbl_category_master.category_id = tbl_seller_upload_product.category_id
                                      WHERE tbl_category_master.category_status = '1'      
                                      GROUP BY tbl_seller_upload_product.category_id
                                      ORDER BY task_cat_id_count DESC";

                $result_cat = $this->db->query($qry_item)->result_array();                       
                ?>
				<div class="col-sm-12 col-md-3 col-lg-3">
					<div class="footer_heading  footer-col-head">Popular Products Category</div>
						<div class="menu_name points-footer">
							 <ul>
							 
							 <?php
							 $i=0; 
							 if(!empty($result_cat))
							 { 
							    foreach($result_cat as $row_cat)
							    {
							       if($i<=5)
							       {
							       ?>
							        <li><a href="<?php echo base_url().'search?cat_id%5B%5D='.$row_cat['category_id']; ?>"><span><i class="fa fa-circle-o"></i></span> <span><?php echo $row_cat['category_name']; ?></span></a></li>
							       <?php } $i++; 
						        }
							 } ?>
							          
							 </ul>
						</div>
					</div>
				<div class="col-sm-12 col-md-3 col-lg-3">
					<div class="footer_heading footer-col-head">
						Contact info
					</div>
					<div class="menu_name points-footer">
						<ul>
							<?php
                            $admin_data=$this->master_model->getRecords('admin_login', array('id'=>'1'));
							?>
                            <li><span><i class="fa fa-phone"></i></span> <span><?php if(isset($admin_data[0]['admin_contactus'])) { echo $admin_data[0]['admin_contactus']; } else { echo "--";} ?></span></li><br/>
                            <li class="email-id-block"><span><i class="fa fa-envelope-o"></i></span> <span><?php if(isset($admin_data[0]['admin_email'])) { echo $admin_data[0]['admin_email']; } else { echo "--";} ?></span></li>
						</ul>
						<div class="social-icon-block">
						    <ul>
						       <!--  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
						        <li class="twitt-block"><a href="#"><i class="fa fa-twitter"></i></a></li>
						        <li class="google-block"><a href="#"><i class="fa fa-google-plus"></i></a></li>
						        <li class="youtobe-block"><a href="#"><i class="fa fa-youtube"></i></a></li>  -->
						    </ul>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>
	<div class="copyright-block">
        <div class="container">
            Copyright <a href="<?php echo base_url().'home'; ?>"><?php echo PROJECT_NAME; ?></a> <i class="fa fa-copyright"></i> <?php echo date('Y'); ?>. All Rights Reserved <a href="index.html">Terms</a> &amp; <a href="index.html">Privacy</a>
        </div>
	</div>
</div>
<a class="cd-top hidden-xs hidden-sm" href="#0">Top</a>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>front-asset/js/backtotop.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>front-asset/js/wow.min.js"></script>
<!--angular js-->
<?php /*
<script src="<?php echo base_url(); ?>front-asset/js/angular/angular.min.js" type="text/javascript"></script>
*/?>
<script src="<?php echo base_url(); ?>front-asset/js/angular/ui-bootstrap-tpls-2.4.0.min.js" type="text/javascript"></script>
<!--angular message js-->
<script src="<?php echo base_url(); ?>front-asset/js/angular/angular-messages.js" type="text/javascript"></script>


<!--angular js giocomplete-->
<script src="<?php echo base_url(); ?>assets/ng-google-autocomplete/ngAutocomplete.js" type="text/javascript"></script>

<!--coustom js of angular-->
<script src="<?php echo base_url(); ?>front-asset/js/angular/main.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>front-asset/js/angular/ng-file-upload-shim.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>front-asset/js/angular/ng-file-upload.js" type="text/javascript"></script>

<!--scrollbar plugins-->
<link href="<?php echo base_url(); ?>front-asset/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>front-asset/js/jquery.mCustomScrollbar.concat.min.js"></script>   
    
<!--   select multi script-->

<!-- <script type="text/javascript">
    $(".js-example-basic-multiple").select2();
</script> -->
<!--   select multi script-->
    