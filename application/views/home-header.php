<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!--<div class="main-banner-block">-->
<div class="header header-home" set-class-when-at-top="fix-to-top" ng-controller="HeaderCntrl">
	<div class="logo-block wow fadeInDown" data-wow-delay="0.2s">
		<a href="<?php echo base_url(); ?>home">
			<img src="<?php echo base_url(); ?>images/fevicon/favicon.png" style="width: 50%;" alt="" class="main-logo" />
		</a>
	</div>
	<span class="menu-icon" ng-click="openNav()">&#9776;</span>
	<!--Menu Start-->
	<div id="mySidenav" class="sidenav">
		<a href="javascript:void(0)" class="closebtn" ng-click="closeNav()">&times;</a>
		<div class="banner-img-block">
			<img src="<?php echo base_url(); ?>images/fevicon/favicon.png" alt="" />
		</div>
		<ul class="min-menu">			
           <!-- <li class="lang" ng-controller="ContryDropdown">
				<div class="wrapper-dropdown-1" id="dd">
					<span class="arrow"><i class="fa fa-angle-down"></i></span>
					<span class="place-name" id="new_word">							
					    Location...				
					</span>
					<ul class="dropdown city-drop">
					<img class="img-arrow-menu" src="<?php echo base_url(); ?>front-asset/images/menu-arrow-ul.png" alt="" />
						<li ng-click="setLanguage('en');">
							<a href="#">								
								<span>Mumbai</span>
							</a>
						</li>
						<li ng-click="setLanguage('ar');">
							<a data-flag="dutch" href="#">								
								<span>Rajasthan</span>
							</a>
						</li>
					</ul>
				</div>
			</li>  -->        
			<li class="becom-seller"><a data-hover="Live&nbsp;Market&nbsp;" href="<?php echo base_url().'marketplace' ?>">Live&nbsp;Market&nbsp;</a></li>   
			<!-- <li class="becom-seller"><a data-hover="Become&nbsp;a&nbsp;Seller" href="">Become&nbsp;a&nbsp;Seller</a></li>
			<li class="becom-buyer"><a data-hover="Become&nbsp;a&nbsp;buyer" href="">Become&nbsp;a&nbsp;buyer</a></li> -->
			<li><a data-hover="Help" href="<?php echo base_url().'cms/view/help'; ?>">Help</a></li>
			<li><a data-hover="Login" href="<?php echo base_url(); ?>login">Login</a></li>
			<li><a data-hover="Sign&nbsp;Up" href="<?php echo base_url(); ?>signup">Sign&nbsp;Up</a></li>
		</ul>
		<div class="clr"></div>
	</div>
	<div class="clr"></div>
</div>
<!--</div>-->
