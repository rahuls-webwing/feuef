<?php $this->load->view('seller/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3><?php if(!empty($Sent_offer_detail[0]['title'])) { echo ucfirst($Sent_offer_detail[0]['title']); } else { echo "Not Available" ;} ?></h3>
        </div>
    </div>
</div>
<div class="middel-container" >
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <?php $this->load->view('seller/left-bar'); ?>
                <div class="col-sm-9 col-md-9 col-lg-9">

                <div class="back-btn-main-block">
                    <!-- <div class="bck-btn">
                        <a href="#"><i class="fa fa-angle-double-left"></i> Back</a>
                    </div> -->
                    <div class="replay-btn">
                        <a class="btn-replay-block" href="javascript:history.back()"><i class="fa fa-reply"></i> back</a>
                        <!-- <a class="btn-delete-block" href="#"><i class="fa fa-trash-o"></i> Delete</a> -->
                    </div>
                    <div class="clr"></div>
                </div>


                <?php $this->load->view('status-msg'); ?>
                 
                    
                <?php if(isset($Sent_offer_detail) && sizeof($Sent_offer_detail) > 0) {

                $get_buyer_info = $this->master_model->getRecords('tbl_user_master',array('id'=>$Sent_offer_detail[0]['buyer_id']));
                /*echo "<pre>";
                print_r($get_buyer_info);*/
                ?>

                    <!-- success msg -->
                   <div style="display:none;" class="alert-box success alert alert-success alert-dismissible msg_div<?php echo $Sent_offer_detail[0]['id']; ?>"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button><span>Done:</span> You successfully accepted this offer. you see this requirment in your closed requirment panel.</div>
                   <!-- end success msg -->

                    <div class="requirment_div<?php echo $Sent_offer_detail[0]['id']; ?>"> 
                      <div class="col-sm-4 col-md-4 col-lg-4" >
                        <div class="details-img-block">
                            <?php if(!empty($Sent_offer_detail[0]['req_image']) && file_exists('images/buyer_post_requirment_image/'.$Sent_offer_detail[0]['req_image'])){?>
                            <img style="height:162px;width:260px;"  src="<?php echo base_url().'images/buyer_post_requirment_image/'.$Sent_offer_detail[0]['req_image']; ?>" alt="list_img_1" class="img-responsive list_view_img_1" />
                            <?php } else { ?>
                            <img src="<?php echo base_url().'images/buyer_post_requirment_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                            <?php } ?> 
                        </div>
                      </div>  
                        <div class="account-info-block">
                        <div id="description" class="details-descrip">
                            <div class="">
                                <?php if(!empty($Sent_offer_detail[0]['title'])) { echo '<b>'.ucfirst($Sent_offer_detail[0]['title']).'</b>'; } else { echo "Not Available" ;} ?>
                            </div>
                            <div class="list_up"><span><i class="fa fa-folder-open"></i>
                                </span>
                                    <?php if(!empty($Sent_offer_detail[0]['category_name'])) { echo substr($Sent_offer_detail[0]['category_name'], 0 , 50); } else { echo "Not Available"; } ?> ->
                                    <?php if(!empty($Sent_offer_detail[0]['subcategory_name'])) { echo substr($Sent_offer_detail[0]['subcategory_name'], 0 , 50); } else { echo "Not Available"; } ?>
                            </div>
                            <div class="list_add"><span><i class="fa fa-map-marker"></i>
                                </span>

                                <?php if(!empty($Sent_offer_detail[0]['location'])) { echo $Sent_offer_detail[0]['location']; } else { echo "Not Available"; } ?> 
                            </div>
                            <div class="lsit_text_content">
                            <span><i class="fa fa-calendar" aria-hidden="true"></i> Posted Date :
                            </span>
                                 <?php echo date('F j, Y', strtotime($Sent_offer_detail[0]['created_date']))." at ".date("g:i a", strtotime($Sent_offer_detail[0]['created_date'])); ?> 
                            
                            </div>

                            <div class="lsit_text_content">
                            <span><i class="fa fa-user" aria-hidden="true"></i> Buyer Name :
                            </span>
                                 <?php if(!empty($get_buyer_info[0]['name'])) { echo $get_buyer_info[0]['name']; } else { echo "Not Available"; } ?>
                            </div>

                            <div class="lsit_text_content">
                            <span><i class="fa fa-envelope-o" aria-hidden="true"></i> Buyer Email :
                            </span>
                                 <?php if(!empty($get_buyer_info[0]['email'])) { 
                                  ?><a href="mailto:<?php echo $get_buyer_info[0]['email']; ?>?Subject=Inquiry" class="stop_loader" target="_top"><?php echo $get_buyer_info[0]['email']; ?></a><?php
                                 } else { 

                                  echo "Not Available"; } ?>
                                
                            </div>
                            <div class="lsit_text_content">
                                <span> <?php echo CURRENCY; ?>
                                    </span>
                                         <?php if(!empty($Sent_offer_detail[0]['price'])) { echo $Sent_offer_detail[0]['price']; } else { echo "Not Available"; } ?> 
                                </div>
                            <div class="" style="color:#444;">
                                <?php if(!empty($Sent_offer_detail[0]['description'])) { echo $Sent_offer_detail[0]['description']; } else { echo "Not Available"; } ?>
                            </div>
                        </div>
                      </div>
                    </div>

                <?php
                } else {
                    $this->load->view('no-data-found');
                }?>

                <!-- Offered Sellers -->
                
                <?php if(isset($offered_sellers) && sizeof($offered_sellers) > 0) {
                  $datetime2      = new DateTime(date("Y-m-d H:m:s"));
                  $endTimeStamp   = strtotime(date("Y-m-d H:m:s"));
                  ?>
                    
                    <?php foreach ($offered_sellers as $offerd_seller) { 
                    
                    ?>
                      <?php
                      /* for hours and time count */    
                      $datetime1 = new DateTime($offerd_seller['offer_created_date']);
                      
                      $interval = $datetime1->diff($datetime2);
                      if($interval->format('%h')==0)
                      {
                        $hours="";
                      }
                      else
                      {
                        $hours=$interval->format('%h')." Hours " ;
                      }
                      if($interval->format('%i')==0)
                      {
                        $Minutes="";
                      }
                      else
                      {
                        $Minutes=$interval->format('%i')." Minutes";
                      }
                      if($Minutes == '' && $hours == '')
                      {
                        $aa = $interval->format('%s');
                        $Minutes ="$aa Second";
                      }
                      /* end for hours and time count */   
                      /* for day count */    
                      $startTimeStamp = strtotime($offerd_seller['offer_created_date']);
                      $timeDiff       = abs($endTimeStamp - $startTimeStamp);
                      $numberDays     = $timeDiff/86400;  // 86400 seconds in one day
                      // and you might want to convert to integer
                      $numberDays = intval($numberDays);
                      if($numberDays==0)
                      {
                          $numberDays="";
                      }
                      else
                      {
                          ($numberDays>1)  ? $day='Day' : $day='Days';
                          $numberDays=intval($numberDays).' '.$day;
                      }
                  ?>
                       <div class="search-grey-bx offer_div" >
                        <div class="row">
                            <div class="col-sm-12 col-md-8 col-lg-10   br-right">
                                <div class="going-profile-detail">
                                    <div class="going-pro">
                                        <?php if(!empty($offerd_seller['user_image']) && file_exists('images/seller_image/'.$offerd_seller['user_image'])){?>
                                        <img src="<?php echo base_url().'images/seller_image/'.$offerd_seller['user_image']; ?>" alt="" />
                                        <?php } else { ?>
                                        <img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
                                        <?php } ?> 
                                    </div>
                                    <div class="going-pro-content">
                                        <div style="display:inline-block;" class="profile-name1">
                                            
                                            <?php if(!empty($offerd_seller['name'])) { ?>
                                             <a href="<?php echo base_url().'seller/profile/'.$offerd_seller['offered_seller_id']; ?>"><?php echo  $offerd_seller['name']; ?></a>
                                            <?php  
                                            } else { 
                                            echo "Not Available"; 
                                            } 
                                            ?> 
 
                                        </div>
                                        <div class="premium-text">
                                           <span>Offered in:</span> <?php  echo $numberDays.' '. $hours.' '. $Minutes; ?> ago
                                        </div>
                                       
                                        <div class="sub-project-dec"> <i class="fa fa-calendar" aria-hidden="true"></i> Offered Date :
                                             
                                             <?php echo date('F j, Y', strtotime($offerd_seller['offer_created_date']))." at ".date("g:i a", strtotime($offerd_seller['offer_created_date'])); ?> 
                                        </div>
                                        <div style="height:auto;" class="more-project-dec">
                                            <div class="content-block-container">
                                                <?php if(!empty($offerd_seller['offer_description'])) { echo $offerd_seller['offer_description']; } else { echo "Not Available"; } ?>
                                            </div>
                                            <div class="clr"></div>
                                            <div class="palm-text"><span><img alt="location icon" src="<?php echo base_url(); ?>front-asset/images/post-location.png"> </span> 
                                               
                                                <?php if(!empty($offerd_seller['city'])) {
                                                echo  $offerd_seller['city']; 
                                                } else { 
                                                echo "Not Available"; 
                                                } ?> 

                                                , 

                                                <?php if(!empty($offerd_seller['country'])) {
                                                echo  $offerd_seller['country']; 
                                                } else { 
                                                echo "Not Available"; 
                                                } ?>

                                            </div>
                                            <div class="clr"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="col-sm-12 col-md-4 col-lg-2">
                                    <div class="rating-profile">
                                        <div class="projrct-prce1"><span><?php echo CURRENCY; ?><?php if(!empty($offerd_seller['offered_price'])) { echo $offerd_seller['offered_price']; } else { echo "Not Available"; } ?> </div>
                                        <div class="">
                                            <?php if(isset($offerd_seller['status']) && $offerd_seller['status'] =="Accepted")
                                            { ?>
                                              <button  class="msg_btn txt_btn"  type="button"> Accepted</button>
                                            <?php } else { ?>
                                              <button  class="sent_btn txt_btn"  type="button"> Sent</button>
                                            <?php } ?>
                                        </div>
                                        <!-- <div class="rating_profile">
                                            <div class="rating-title1">Rating   <span>:</span> </div>
                                            <div class="rate-t">
                                                <div class="rating-list1"><img alt="img" src="images/rate-star.png"> </div>
                                                (4.8)
                                            </div>
                                        </div>
                                        <div class="rating_profile">
                                            <div class="rating-title1">Review <span>:</span> </div>
                                            <div class="star-rat">9</div>
                                        </div>
                                        <div class="rating_profile">
                                            <div class="rating-title1">Completion Rate<span>:</span> </div>
                                            <div class="star-rat">65%</div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>    
                    <?php } ?>   

                <?php
                } else {
                    $this->load->view('no-data-found');
                }?>    
                <!-- end Offered Sellers -->
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
$('.stop_loader').click(function(){
setTimeout(function(){
ajaxindicatorstop();
},2000)
});
}); 
</script>