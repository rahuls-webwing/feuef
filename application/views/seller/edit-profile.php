<style type="text/css">
	.img-err {
	    width: 250px;
	    height: 8px;
	    margin: 30px auto;
	    border-radius: 15px;
	    position: relative;
	}
    .label-important, .badge-important {
        background-color: #fe1010;
    }

</style>
<?php $this->load->view('seller/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3>Edit Profile</h3>
        </div>
    </div>
</div>
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <?php $this->load->view('seller/left-bar'); ?>
                <div class="col-sm-9 col-md-9 col-lg-9">
                    <div class="account-info-block">

                    <?php $this->load->view('status-msg'); ?>
                    <div ng-controller="SellerEditProfileCntrl">    
                        <form  
                           class=""
                           ng-init="loadSellerProfile();"
                           name="sellerEditForm" 
                           enctype="multipart/form-data"
                           novalidate ng-submit="sellerEditForm.$valid && editProfileSave();"  >
                            <div class="row">

                                <div class="pro-pic-bar">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div class="profile-pic-block-pro">
                                            <img ng-src="{{user_image}}" src="<?php echo base_url(); ?>images/default/default-user-img.jpg" id="img_profile" class="" alt="profile" />
                                            <div class="over-lay">
                                                <div class="profile-pic-edit">
                                                    <div class="fileUpload">
                                                        <span><i class="fa fa-pencil-square-o"></i></span>
                                                        <input type="file" 
                                                               class="upload allowOnlyImg"
                                                               name="user_image" 
                                                               id="user_image" 
                                                               ng-model ="user.user_image"  />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      
                                        <div  style="color:#007aca; text-align:center;" class="" ><b><span class="label label-important">NOTE!</span> Profile Image allow  greater than 160 x 160 (h X w) dimension only.</b></div>
                                        <div class="img-err">
                                            <div id="err-img" style="color:red;" class="pro-txt" ></div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Full Name <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block" ng-class="{ 'has-error': sellerEditForm.name.$touched 
                                && sellerEditForm.name.$invalid }">
                                            <input type="text" 
                                                   class="beginningSpace_restrict num_restrict" 
                                                   name="name" 
                                                   ng-model="user.name" 
                                                   ng-required="true"
                                                   placeholder="Full Name" 
                                                   />
                                        </div>
                                        <div class="error-new-block" ng-messages="sellerEditForm.name.$error" ng-if="sellerEditForm.$submitted || sellerEditForm.name.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>
                                    </div>
                                </div>  
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Last Name <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block" ng-class="{ 'has-error': sellerEditForm.lname.$touched 
                                && sellerEditForm.lname.$invalid }">
                                            <input type="text" 
                                                   class="beginningSpace_restrict num_restrict" 
                                                   name="lname" 
                                                   ng-model="user.lname" 
                                                   ng-required="true"
                                                   placeholder="Last Name" 
                                                   />
                                        </div>
                                        <div class="error-new-block" ng-messages="sellerEditForm.lname.$error" ng-if="sellerEditForm.$submitted || sellerEditForm.lname.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Email Address</div>
                                        <div class="input-block">
                                            <input type="text" 
                                                   class="beginningSpace_restrict"
                                                   name="email" 
                                                   ng-model="user.email"
                                                   placeholder="email" 
                                                   />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Phone / Mobile Number <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block" ng-class="{ 'has-error': sellerEditForm.mobile_number.$touched 
                                && sellerEditForm.mobile_number.$invalid }">
                                            <input type="text"
                                                   class="beginningSpace_restrict   allowOnlyFourteen" 
                                                   name="mobile_number"
                                                   ng-model="user.mobile_number"
                                                   ng-minlength="10" 
                                                   ng-maxlength="16"
                                                   ng-required="true"
                                                   placeholder="phone no" 
                                                    />
                                        </div>
                                        <div class="error-new-block" ng-messages="sellerEditForm.mobile_number.$error" ng-if="sellerEditForm.$submitted || sellerEditForm.mobile_number.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">

                                            <p ng-message="required"    class="error">  This field is required</p>
                                            <p ng-message="minlength"   class="error">  Please enter at least 10 digit</p>
                                            <p ng-message="maxlength"   class="error">  Mobile no to long</p>
                                            
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Address <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block" ng-class="{ 'has-error': sellerEditForm.address.$touched 
                                && sellerEditForm.address.$invalid }">
                                            <input type="text"
                                                   name="address"
                                                   class="beginningSpace_restrict"
                                                   ng-model="user.address"
                                                   ng-autocomplete
                                                   ng-required="true"
                                                   details="obj_autocomplete"
                                                   placeholder="address" 
                                                   />
                                        </div>
                                        <div class="error-new-block" ng-messages="sellerEditForm.address.$error" ng-if="sellerEditForm.$submitted || sellerEditForm.address.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>
                                    </div>
                                </div> 

                                <input type="hidden" name="latitude"  id="latitude" ng-model="user.latitude" placeholder="latitude">
                                <input type="hidden" name="longitude" id="longitude" ng-model="user.longitude" placeholder="longitude"> 


                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">City <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block" ng-class="{ 'has-error': sellerEditForm.city.$touched 
                                    && sellerEditForm.city.$invalid }">
                                            <input type="text" 
                                                   name="city" 
                                                   class="beginningSpace_restrict"
                                                   ng-model="user.city"
                                                   ng-required="true"
                                                   placeholder="city" 
                                                    />
                                        </div>
                                        <div class="error-new-block" ng-messages="sellerEditForm.city.$error" ng-if="sellerEditForm.$submitted || sellerEditForm.city.$touched">
                                            <div>
                                                <div class="err_msg_div" style="display:none;">
                                                <p ng-message="required"    class="error">  This field is required</p>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>
                                    </div>
                                </div> 

                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Pin Code </div>
                                        <div class="input-block">
                                            <input type="text" 
                                                   name="pincode"
                                                   class="beginningSpace_restrict allowOnlySix"
                                                   ng-model="user.pincode" 
                                                   placeholder="Pincode" 
                                                    />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">State <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block">
                                            <input type="text" 
                                                   name="state"
                                                   class="beginningSpace_restrict allowOnlySix"
                                                   ng-model="user.state" 
                                                   placeholder="State" 
                                                    />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Country <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block" ng-class="{ 'has-error': sellerEditForm.country.$touched && sellerEditForm.country.$invalid }">
                                            <input type="text" 
                                                   name="country"
                                                   class="beginningSpace_restrict"
                                                   ng-model="user.country"
                                                   ng-required="true"
                                                   placeholder="country" 
                                                    />
                                        </div>
                                        <div class="error-new-block" ng-messages="sellerEditForm.country.$error" ng-if="sellerEditForm.$submitted || sellerEditForm.country.$touched">
                                            <div>
                                                <div class="err_msg_div" style="display:none;">
                                                
                                                <p ng-message="required"    class="error">  This field is required</p>
                                                
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                    $(document).ready(function(){
                                                      setTimeout(function(){
                                                        $('.err_msg_div').removeAttr('style');
                                                      },200);
                                                    });
                                                </script>
                                        </div>
                                    </div>
                                </div> 
                              
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="btn-password save">
                                        <button class="change-btn-pass" type="submit">Save</button>
                                    </div>
                                </div>


                            </div>

                        </form>
                     </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLKCmJEIrOrzdRklZHYer8q9qx2XLJ4Vs&sensor=false&libraries=places"></script>
<script type="text/javascript">
$(document).ready(function(){
 $('#user_image').click(function(){
    ajaxindicatorstart();
    setTimeout(function(){
      ajaxindicatorstop();
    },2000);
 });
}); 
</script>
<script language="JavaScript" type="text/javascript">
function changeDate(i){
var e = document.getElementById('day');
	while(e.length>0)
	e.remove(e.length-1);
	var j=-1;
	if(i=="")
	k=0;
	else if(i==2)
	k=28;
	else if(i==4||i==6||i==9||i==11)
	k=30;
	else
	k=31;
	while(j++<k){
	var s=document.createElement('option');
	var e=document.getElementById('day');
	if(j==0){
	s.text="Day";
	s.value="";
	try{
	e.add(s,null);}
	catch(ex){
	e.add(s);}}
	else{
	s.text=j;
	s.value=j;
	try{
	e.add(s,null);}
	catch(ex){
	e.add(s);}}}}
	var d = new Date();
	var y = d.getFullYear();
	/*y = 1993;*/
	while (y-->1940){
	var s = document.createElement('option');
	var e = document.getElementById('year');
	s.text=y;
	s.value=y;
	try{
	e.add(s,null);}
	catch(ex){
	e.add(s);}}
</script>
