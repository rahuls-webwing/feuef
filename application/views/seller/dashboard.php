<?php $this->load->view('seller/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3>Dashboard</h3>
        </div>
    </div>
</div>
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <!-- <div ng-include='"left-bar.html"'></div> -->
                <?php $this->load->view('seller/left-bar'); ?>
                <div class="col-sm-9 col-md-9 col-lg-9">
                    <?php $this->load->view('status-msg'); ?>   
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="profile-block">
                                <div class="my-profile-txt">
                                    My Profile <a href="<?php echo base_url().'seller/edit'; ?>"><i class="fa fa-pencil-square-o"></i></a>
                                </div>
                                <div class="main-block-info">
                                    <div class="profile-pic-block">
                                        <?php if(!empty($this->session->userdata('user_image')) && file_exists('images/'.lcfirst($this->session->userdata('user_type')).'_image/'.$this->session->userdata('user_image'))){?>
                                        <img src="<?php echo base_url().'images/'.lcfirst($this->session->userdata('user_type')).'_image/'.$this->session->userdata('user_image'); ?>" alt="" />
                                        <?php } else { ?>
                                        <img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
                                        <?php } ?> 
                                    </div>
                                    <div class="profile-name">
                                        <?php if(!empty($this->session->userdata('user_name'))) {

                                        ?><a style="color:#444;" href="<?php echo base_url().'seller/profile/'.$this->session->userdata('user_id'); ?>"><?php 
                                        echo $this->session->userdata('user_name'); 
                                        ?></a><?php 
                                        } else { 
                                        echo "Not Available"; 
                                        } 
                                        ?> 
                                    </div>
                                    <div class="co-name-block">
                                        <?php if(!empty($this->session->userdata('user_city'))) {
                                        echo  $this->session->userdata('user_city'); 
                                        } else { 
                                        echo "Not Available"; 
                                        } ?> 

                                        , 

                                        <?php if(!empty($this->session->userdata('user_country'))) {
                                        echo  $this->session->userdata('user_country'); 
                                        } else { 
                                        echo "Not Available"; 
                                        } ?>
                                    </div>
                                    <div class="socila-icon-profile">
                                        <ul>
                                            <!-- <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li> -->
                                        </ul>
                                    </div>
                                    <div class="content-block-profile">
                                        <?php if(!empty($this->session->userdata('user_address'))) {
                                        echo substr($this->session->userdata('user_address'), 0 , 114).'..';

                                        } else { 
                                        echo "Not Available"; 
                                        } 
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="message-bx-block open_chat">
                                <?php 
                                $this->db->where('read' , '0');
                                $this->db->where('cometchat.to' , $this->session->userdata('user_id'));
                                $get_chat_count  = $this->master_model->getRecordCount('cometchat');
                                if($get_chat_count <= 9){ $chat_cnt = '0'.$get_chat_count; } else if($get_chat_count >= 10) { $chat_cnt = '10+'; }
                                ?>
                                <a href="#">
                                    <span class="head-block-bx">Messages</span>
                                    <div class="mes-icon-block"><i class="fa fa-envelope-o"></i></div>
                                    <span class="message-count-block">
                                        <?php echo $chat_cnt; ?>
                                    </span>
                                    <span class="text-count-block">
                                        New<br/> Messages
                                    </span>
                                    <span class="total-txt-block">
                                        (Total)
                                    </span>
                                </a>
                            </div>
                            <div class="message-bx-block inquir-block-bx">
                                <?php 
                                $this->db->where('status' , 'Unblock');
                                $this->db->where('is_read' , 'no');
                                $this->db->where('tbl_seller_contact_inquires.seller_id' , $this->session->userdata('user_id'));
                                $get_inq_count  = $this->master_model->getRecordCount('tbl_seller_contact_inquires');
                                if($get_inq_count <= 9){ $inq_cnt = '0'.$get_inq_count; } else if($get_inq_count >= 10) { $inq_cnt = '10+'; }
                                ?>
                                <a href="<?php echo base_url().'seller/my_inquires'; ?>">
                                    <span class="head-block-bx">My Inquiries</span>
                                    <div class="mes-icon-block"><i class="fa fa-suitcase"></i></div>
                                    <span class="message-count-block">
                                        <?php echo $inq_cnt; ?>
                                    </span>
                                    <span class="text-count-block">
                                        New<br/> Inquiries
                                    </span>
                                    <span class="total-txt-block">
                                        (Total)
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <?php 
                            $this->db->where('status' , 'Unblock');
                            $this->db->where('is_read' , 'no');
                            $this->db->where('tbl_seller_notifications.seller_id' , $this->session->userdata('user_id'));
                            $get_noti_count  = $this->master_model->getRecordCount('tbl_seller_notifications');
                            if($get_noti_count <= 9){ $noti_cnt = '0'.$get_noti_count; } else if($get_noti_count >= 10) { $noti_cnt = '10+'; }
                            ?>
                            <div class="message-bx-block notifi-bx-block">
                                <a href="<?php echo base_url().'seller/notifications'; ?>">
                                    <span class="head-block-bx">Notifications</span>
                                    <div class="mes-icon-block"><i class="fa fa-bell-o"></i></div>
                                    <span class="message-count-block">
                                        <?php echo $noti_cnt; ?>
                                    </span>
                                    <span class="text-count-block">
                                        New<br/> Notifications
                                    </span>
                                    <span class="total-txt-block">
                                        (Total)
                                    </span>
                                </a>
                            </div>
                            <div class="message-bx-block review-rati-block">
                                <?php 
                                $this->db->where('status' , 'Unblock');
                                $this->db->where('is_read' , 'no');
                                $this->db->where('tbl_seller_rating.seller_id' , $this->session->userdata('user_id'));
                                $get_rew_count  = $this->master_model->getRecordCount('tbl_seller_rating');
                                if($get_rew_count <= 9){ $rew_cnt = '0'.$get_rew_count; } else if($get_rew_count >= 10) { $rew_cnt = '10+'; }
                                ?>
                                <a href="<?php echo base_url().'seller/reviews'; ?>">
                                    <span class="head-block-bx">Reviews &amp; Rating</span>
                                    <div class="mes-icon-block"><i class="fa fa-comments-o"></i></div>
                                    <span class="message-count-block">
                                        <?php echo $rew_cnt; ?>
                                    </span>
                                    <span class="text-count-block">
                                        New Collected<br/> Reviews
                                    </span>
                                    <span class="total-txt-block">
                                        (Total)
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <?php
                            $this->db->where('tbl_seller_upload_product.seller_id' , $this->session->userdata('user_id'));
                            $this->db->where('tbl_seller_upload_product.status' , 'Unblock');
                            $this->db->where('tbl_seller_upload_product.status <>' , 'Delete');
                            $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_seller_upload_product.subcategory_id');
                            $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
                            $getProducts  = $this->master_model->getRecordCount('tbl_seller_upload_product');
                            if($getProducts <= 9){ $product_cnt = '0'.$getProducts; } else if($getProducts >= 10) { $product_cnt = '10+'; }
                            ?>
                            <div class="message-bx-block posted-pro-block">
                                <a href="<?php echo base_url().'seller/products'; ?>">    
                                    <span class="head-block-bx">Posted Products</span>
                                    <div class="mes-icon-block"><i class="fa fa-shopping-bag"></i></div>
                                    <span class="message-count-block">
                                        <?php echo $product_cnt; ?>
                                    </span>
                                    <span class="text-count-block">
                                        My<br/> Products
                                    </span>
                                    <span class="total-txt-block">
                                        (Total)
                                    </span>
                                </a>
                            </div>                                    
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <?php

                                $this->db->order_by('tbl_apply_for_order.id' , 'desc');
                                $this->db->select('tbl_apply_for_order.*,tbl_user_master.name');
                                $this->db->where('tbl_apply_for_order.order_seller_id' , $this->session->userdata('user_id'));
                                $this->db->where('tbl_apply_for_order.order_from !=' , '');
                                $this->db->join('tbl_user_master', 'tbl_user_master.id=tbl_apply_for_order.order_buyer_id');
                                $get_order_form = $this->master_model->getRecordCount('tbl_apply_for_order');
                               
                                if($get_order_form <= 9){ $order = '0'.$get_order_form; } else if($get_order_form >= 10) { $order = '10+'; }
                            ?>
                            <div class="message-bx-block current-plan-block">
                                <a href="<?php echo base_url().'seller/product_order_received'; ?>">  
                                    <span class="head-block-bx">ORDERS</span>
                                    <div class="mes-icon-block"><b><i class="fa fa-outdent" aria-hidden="true"></i></b></div>
                                    <span class="message-count-block">
                                        <?php echo $order; ?>
                                    </span>
                                    <span class="text-count-block">
                                       ORDERS
                                    </span>
                                    <span class="total-txt-block">
                                        (Total)
                                    </span>
                                </a>
                            </div>                                    
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <?php

                                $this->db->order_by('id' , 'desc');
                                $this->db->where('receiver_id' , $this->session->userdata('user_id'));
                                $this->db->where('sender_type' , "Buyer");
                                $get_contact_list  = $this->master_model->getRecordCount('tbl_contact_info_in_dashboard');
                               
                                if($get_contact_list <= 9){ $contact = '0'.$get_contact_list; } else if($get_contact_list >= 10) { $contact = '10+'; }
                            ?>
                            <div class="message-bx-block current-plan-block">
                                <a href="<?php echo base_url().'seller/contact_list'; ?>">  
                                    <span class="head-block-bx">CONTACT'S</span>
                                    <div class="mes-icon-block"><b><i class="fa fa-phone-square" aria-hidden="true"></i></b></div>
                                    <span class="message-count-block">
                                        <?php echo $contact; ?>
                                    </span>
                                    <span class="text-count-block">
                                       CONTACT'S
                                    </span>
                                    <span class="total-txt-block">
                                        (Total)
                                    </span>
                                </a>
                            </div>                                    
                        </div>
                        <!-- <div class="col-sm-12 col-md-6 col-lg-6">
                            <div class="message-bx-block current-plan-block">
                                <a href="#">
                                    <span class="head-block-bx">My Trades</span>
                                    <div class="mes-icon-block"><b><?php echo CURRENCY; ?></b></div>
                                    <span class="message-count-block">
                                        $23,100
                                    </span>
                                    <span class="text-count-block">
                                        Membership<br/> Planes  
                                    </span>
                                    <span class="total-txt-block">
                                        (Total)
                                    </span>
                                </a>
                            </div>                                    
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
        