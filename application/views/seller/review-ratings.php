<style type="text/css">
.table-main-block {
    margin-bottom: 30px;
    padding-top: 0px;
}
</style>
<?php $this->load->view('seller/top-bar'); ?>
    <div class="page-head-name">
        <div class="container">
            <div class="name-container-dash">
                <h3>Review &amp; Rating</h3>
            </div>
        </div>
    </div>
    <div class="middel-container">
        <div class="inner-content-block">
            <div class="container">
                <div class="row">
                    <?php $this->load->view('seller/left-bar'); ?>
                    <div class="col-sm-9 col-md-9 col-lg-9">
                        <?php $this->load->view('status-msg'); ?>
                        <div class="table-main-block review-rati-white">
                            
                            <div class="review-rating-main">
                                
                                <?php if(isset($getreview) && sizeof($getreview) > 0) { ?>

                                    <?php  foreach ($getreview as  $review) { 
                                        $get_buyer_details = $this->master_model->getRecords('tbl_user_master',array('id'=>$review['buyer_id']));
                                      ?>
                                        <div class="block-main-reviews">
                                            <div class="profile-pic-review">
                                                <?php if(!empty($get_buyer_details[0]['user_image']) && file_exists('images/buyer_image/'.$get_buyer_details[0]['user_image'])){?>
                                                <img src="<?php echo base_url().'images/buyer_image/'.$get_buyer_details[0]['user_image']; ?>" alt="" />
                                                <?php } else { ?>
                                                <img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
                                                <?php } ?> 
                                            </div>
                                            <div class="close-btn-block">
                                                
                                                <a href="<?php echo base_url().'seller/reviews_detail/'.$review['review_id']; ?>"><i class="fa fa-eye" style="font-size:1.5em;"></i></a>
                                                <!-- <a class="delete_selected_review" title="Delete "  style="text-decoration:none;" data-revi="<?php echo $review['review_id'];  ?>" href="javascript:void(0);"><i class="fa fa-trash-o" style="font-size:1.5em;"></i></a> -->
                                            </div>
                                            <div class="review-content-block">
                                                <div class="review-head-block">
                                                    <?php if(isset($get_buyer_details[0]['name'])) { echo $get_buyer_details[0]['name'];} else { echo "Not Available" ;} ?>
                                                </div>
                                                <div class="rating-star-block">


                                                    <?php 
                                                        $ratings         = 0;
                                                        $arv_rating      = 0;       
                                                        $arv_rating      = $review['ratings'];  // find rating avarage
                                                        ?> 
                                                        <span class="product-satrs-block">
                                                        <?php
                                                            $finalrate = $chekRating = mb_substr($arv_rating,  0, 4);
                                                            for($i=1;$i<=5;$i++) {
                                                            $selected = "";

                                                            $point = $i - $finalrate;

                                                            if(!empty($finalrate) && $i<=$finalrate) {
                                                              ?>
                                                               <img alt="img" src="<?php echo base_url();?>images/review/star-full.png" style="margin-top:11px;"/>
                                                               <?php
                                                             } else if($point == '0.5' || $point < '0.5' || $point < '1') {
                                                              ?>
                                                              <img alt="img" src="<?php echo base_url();?>images/review/star-half.png" style="margin-top:11px;"/>
                                                               <?php 
                                                            } else {
                                                              ?>
                                                              <img alt="img" src="<?php echo base_url();?>images/review/star-blank.png" style="margin-top:11px;"/>
                                                              <?php 
                                                            }
                                                        } 
                                                    ?>
                                                   
                                                   </span>
                                                    <div style="padding-left:15px;">
                                                       <span class="rivew-count"  ><?php echo $arv_rating ; ?></span>  
                                                    </div>
                                                    <span>- <?php echo date('F j, Y', strtotime($review['commented_at']))." at ".date("g:i a", strtotime($review['commented_at'])); ?></span>
                                                </div>
                                                <div class="message-content-review">
                                                    <?php if($review['is_read'] == 'no') { ?>
                                                    <span class="label label-important" style="background-color:red; margin-top:-15px;">Unread!</span> 
                                                    <?php } ?>
                                                    <?php if(isset($review['comment'])) { echo $review['comment'];} else { echo "Not Available" ;} ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>    
                                <?php } else {
                                   $this->load->view('no-data-found');
                                }?>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>
    <script type="text/javascript">
  
      $('.delete_selected_review').click(function(){
            var review_id = jQuery(this).data("revi");
            swal({   
                 title: "Are you sure?",   
                 text: "You want to delete this review ?",  
                 type: "warning",   
                 showCancelButton: true,   
                 confirmButtonColor: "#8cc63e",  
                 confirmButtonText: "Yes",  
                 cancelButtonText: "No",   
                 closeOnConfirm: false,   
                 closeOnCancel: false }, function(isConfirm){   
                  if (isConfirm) 
                  { 
                         swal("Deleted!", "Your review has been deleted.", "success"); 
                         location.href=site_url+"seller/review_delete/"+review_id;
                  } 
                  else
                  { 
                         swal("Cancelled", "Your review is safe :)", "error");          
                    } 
                });
        }); 

    </script>   