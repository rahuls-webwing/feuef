<style type="text/css">
    .label-important, .badge-important {
        background-color: #fe1010;
    }
</style>
<?php $this->load->view('seller/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3>Order Forms</h3>
        </div>
    </div>
</div>
 
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <?php $this->load->view('seller/left-bar'); ?>

                <div class="col-sm-12 col-md-8 col-lg-9">
                    <div class="row">
                   
                
                 <div class="row" style="margin-top:3px;">
                   <div class="col-sm-12 col-md-8 col-lg-12" style="padding-right: 0px;">
                    <div class="table-responsive">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-hover fill-head" style="1px solid #ececec;">
                           <tr class="pay_head" >

                              
                              <td style="text-indent:5px;">Sender</td>
                              <td style="text-indent:5px;">Receiver</td>
                              <td style="text-indent:5px;">Offer Name</td>
                              <td style="text-indent:5px;">Type</td>
                              <td style="text-indent:5px;">Order Form</td>

                              <td style="text-indent:5px;">Send/Received</td>
                              <td style="text-indent:5px;">Status</td>
                              <td style="text-indent:5px;">Date</td>

                           </tr>
                           <?php 
                           $total=0;
                           if(!empty($getOffers)){ 
                                
                                 foreach($getOffers as $value) {
                                  if(!empty($value['order_form']) && file_exists('uploads/invoice/'.$value['order_form'])){
                                  ?>
                                  <tr class="pay_row_one">
                                   
                                    <?php if($value['from_id']) 
                                      { 
                                        $this->db->where('id' , $value['from_id']);
                                        $get_receiver_name = $this->master_model->getRecords('tbl_user_master');
                                      ?>
                                            <td style="padding:12px!important;"> 
                                               <?php if(!empty($get_receiver_name[0]['name'])) { echo $get_receiver_name[0]['name']; } else { echo "Not Available"; } ?>  
                                            </td>

                                    <?php } ?>

                                    <?php if($value['to_id']) 
                                      { 
                                        $this->db->where('id' , $value['to_id']);
                                        $get_receiver_name = $this->master_model->getRecords('tbl_user_master');
                                      ?>
                                            <td style="padding:12px!important;"> 
                                               <?php if(!empty($get_receiver_name[0]['name'])) { echo $get_receiver_name[0]['name']; } else { echo "Not Available"; } ?>  
                                            </td>

                                    <?php } ?>

                                    <td style="padding:12px!important;"> 
                                         <?php if(!empty($value['order_name'])) { echo $value['order_name']; } else { echo "Not Available"; } ?>
                                    </td>
                                    <td style="padding:12px!important;"> 
                                         <?php if(!empty($value['type'])) { echo $value['type']; } else { echo "Not Available"; } ?>
                                    </td>

                                    <td style="padding:12px!important;"> 
                                       <a class="btn-delete-block" href="<?php echo base_url().'uploads/invoice/'.$value['order_form'] ?>" download><i class="fa fa-cloud-download" aria-hidden="true"></i> Order Form</a>
                                    </td>
                                    
                                    <?php if($value['from_id'] == $this->session->userdata('user_id')) 
                                      { ?>
                                            <td style="padding:12px!important;"> 
                                             send 
                                            </td>
                                      <?php
                                      } 
                                      else 
                                      { ?>
                                            <td style="padding:12px!important;"> 
                                               received 
                                            </td>

                                    <?php } ?>

                                      <td style="padding:12px!important;">  
                                          <?php 
                                            if(!empty($value['type']))
                                            {
                                                  if($value['type']=='offer')
                                                  {
                                                    $this->db->select('status');
                                                    $this->db->where('id' , $value['apply_offer_id']);
                                                    $offer_status = $this->master_model->getRecords('tbl_apply_for_offers');
                                                    
                                                    if(isset($offer_status[0]['status']) && !empty($offer_status[0]['status']) && $offer_status[0]['status']=='Accepted')
                                                    {
                                                      echo $offer_status[0]['status'];  
                                                    } 
                                                    else
                                                    {
                                                      echo "Pending";
                                                    } 
                                                  }
                                                  else if($value['type']=='product')
                                                  {
                                                    $this->db->select('status');
                                                    $this->db->where('id' , $value['apply_offer_id']);
                                                    $offer_status = $this->master_model->getRecords('tbl_apply_for_order');
                                                    
                                                    if(isset($offer_status[0]['status']) && !empty($offer_status[0]['status']) && $offer_status[0]['status']=='Accepted')
                                                    {
                                                      echo $offer_status[0]['status'];  
                                                    } 
                                                    else
                                                    {
                                                      echo "Pending";
                                                    } 
                                                  }
                                                  else if($value['type']=='requirement')
                                                  {
                                                    $this->db->select('status');
                                                    $this->db->where('id' , $value['apply_offer_id']);
                                                    $offer_status = $this->master_model->getRecords('tbl_apply_for_requirment');
                                                    
                                                    if(isset($offer_status[0]['status']) && !empty($offer_status[0]['status']) && $offer_status[0]['status']=='Accepted')
                                                    {
                                                      echo $offer_status[0]['status'];  
                                                    } 
                                                    else
                                                    {
                                                      echo "Pending";
                                                    } 
                                                  }


                                            } 
                                            ?>
                                    </td>

                                    <td style="padding:12px!important;">  <?php echo date('F j, Y', strtotime($value['date'])); ?>  
                                    </td>

                                  </tr>
                                  <?php  } } }
                                  else
                                  { ?>
                                    <tr class="pay_row_one">
                                      <td align="center" style="padding:3px!important;" colspan="5"> 
                                        <div >
                                              <?php  $this->load->view('no-data-found'); ?>
                                        </div>
                                      </td>
                                    </tr>
                                  <?php } ?>
                                 
                        </table>
                       </div>
                      </div> 
                    </div>
                  <!--pagigation start here-->
                 <div class="product-pagi-block">
                <?php echo $this->pagination->create_links(); ?>
               </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$("#sort_by").on('change', function(){

$("#sort_by_search").click();

});
</script>