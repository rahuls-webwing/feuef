<?php $this->load->view('seller/top-bar'); 

if(isset($inquires_detail[0]['buyer_id'])) { $buyer_id = $inquires_detail[0]['buyer_id']; } else { $buyer_id = 0; }
$this->db->where('tbl_user_master.id' , $buyer_id);
$get_sender_details = $this->master_model->getRecords('tbl_user_master');
      
?>
<div class="page-head-name">
<div class="container">
    <div class="name-container-dash">
        <h3>My inquiry Details</h3>
    </div>
</div>
</div>
<div class="middel-container">
<div class="inner-content-block">
    <div class="container">
        <div class="row">
           <?php $this->load->view('seller/left-bar'); ?>
           <div class="col-sm-9 col-md-9 col-lg-9">
            <?php if(isset($inquires_detail) && sizeof($inquires_detail) > 0) { ?>
            
                <div class="table-main-block">
                    <div class="back-btn-main-block" style="padding:10px;">
                        <div class="replay-btn">
                            <a class="btn-replay-block post_data" data-buyerid="<?php echo $get_sender_details[0]['id']; ?>" data-inquryid="<?php echo $inquires_detail[0]['id']; ?>" href="#" data-target="#myOfferModal" data-toggle="modal" data-backdrop="static"><i class="fa fa-reply"></i> reply</a>
                            <a class="btn-delete-block" href="javascript:history.back()"><i class="fa fa-angle-double-left"></i></i> Back</a>
                        </div>
                        <div class="clr"></div>
                    </div>
                    <div class="main-details-block">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Sender:
                                    </div>
                                    <div class="details-content-first">
                                        <?php if(isset($get_sender_details[0]['name'])) { echo $get_sender_details[0]['name']; } else { echo "Not Available"; } ?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Sender Email:
                                    </div>
                                    <div class="details-content-first">
                                       <?php if(isset($get_sender_details[0]['email'])) { echo $get_sender_details[0]['email']; } else { echo "Not Available"; } ?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                             <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Inquiry Date:
                                    </div>
                                    <div class="details-content-first">
                                       <?php if(isset($inquires_detail[0]['posted_date'])) { echo date('F j, Y', strtotime($inquires_detail[0]['posted_date']))." at ".date("g:i a", strtotime($inquires_detail[0]['posted_date'])); } else { echo "Not Available"; } ?>  
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Mobile No:
                                    </div>
                                    <div class="details-content-first">
                                         <?php if(isset($get_sender_details[0]['mobile_number'])) { echo $get_sender_details[0]['mobile_number']; } else { echo "Not Available"; } ?>

                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                             <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Category:
                                    </div>
                                    <div class="details-content-first">
                                        <?php if(isset($inquires_detail[0]['category_name'])) { echo $inquires_detail[0]['category_name']; } else { echo "Not Available"; }?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Product:
                                    </div>
                                    <div class="details-content-first">
                                       <?php if(isset($inquires_detail[0]['product_name'])) { echo $inquires_detail[0]['product_name']; } else { echo "Not Available"; }?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="main-details-block n-bg-block">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="" style="text-align:justify;">
                                    <div class="first-name-block-details">
                                        Description:
                                    </div>
                                    <?php if(isset($inquires_detail[0]['description'])) { echo $inquires_detail[0]['description']; } else { echo "Not Available"; }?>
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            } else {
            $this->load->view('no-data-found');
            } ?>
           </div> 
        </div>
    </div>
</div>        
</div>

<!-- Offer Modal -->
<div id="myOfferModal" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title"><i class="fa fa-reply"></i> reply</h4>
        </div>
        <div class="modal-body">
        <?php $this->load->view('status-msg'); ?>
       
        <div ng-controller="InqueryToInquery">  
            <form  
                id="replyToinquery"
                class=""
                name="replyToinquery" 
                enctype="multipart/form-data"
                novalidate 
                ng-submit="replyToinquery.$valid && inqueryReply();"  >

                    <input type="hidden" name="buyer_id" id="buyer_id" ng-model="user.buyer_id" placeholder="buyer_id">
                    <input type="hidden" name="inquiry_id" id="inquiry_id" ng-model="user.inquiry_id" placeholder="inquiry_id">
                    <div class="mobile-nu-block input-first box-msg" ng-class="{'has-error':replyToinquery.description.$touched && replyToinquery.description.$invalid}">
                        <textarea rows="" 
                                      cols="" 
                                      name="description" 
                                      class="beginningSpace_restrict" 
                                      ng-model="user.description"
                                      ng-required="true"
                                      ng-maxlength="500"
                                      placeholder="Enter offer description"></textarea>

                        <span class="highlight"></span> 
                        <label>Message</label>

                        <div class="error-new-block" ng-messages="replyToinquery.description.$error" ng-if="replyToinquery.$submitted || replyToinquery.description.$touched">
                        <div>
                        <div class="err_msg_div" style="display:none;">
                            <p ng-message="required"    class="error">  This field is required</p>
                            <p ng-message="maxlength"   class="error">  This field to long. Only 1000 characters allow.</p>
                            </div>
                        </div>
                        <script type="text/javascript">
                                $(document).ready(function(){
                                  setTimeout(function(){
                                    $('.err_msg_div').removeAttr('style');
                                  },200);
                                });
                            </script>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-password changes enq">
                        <button class="change-btn-pass " type="submit">Send</button>
                    </div>
                </div>
            </form>    
        </div>

    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() { 
      $('.close').on('click',function() {
        setTimeout(function(){
            location.reload();
        })
      });
    });
</script>
<!-- End Offer Modal -->