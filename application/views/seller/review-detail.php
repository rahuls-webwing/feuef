<?php $this->load->view('seller/top-bar'); 

if(isset($getreviewes[0]['buyer_id'])) { $buyer_id = $getreviewes[0]['buyer_id']; } else { $buyer_id = 0; }
$this->db->where('tbl_user_master.id' , $buyer_id);
$get_sender_details = $this->master_model->getRecords('tbl_user_master');
      
?>
<div class="page-head-name">
<div class="container">
    <div class="name-container-dash">
        <h3>Notifications Details</h3>
    </div>
</div>
</div>
<div class="middel-container">
<div class="inner-content-block">
    <div class="container">
        <div class="row">
           <?php $this->load->view('seller/left-bar'); ?>
           <div class="col-sm-9 col-md-9 col-lg-9">
            <?php if(isset($getreviewes) && sizeof($getreviewes) > 0) { ?>
            
                <div class="table-main-block">
                    <div class="back-btn-main-block" style="padding:10px;">
                        <div class="replay-btn">
                            <!-- <a class="btn-replay-block" href="#"><i class="fa fa-reply"></i> reply</a> -->
                            <a class="btn-delete-block" href="javascript:history.back()"><i class="fa fa-angle-double-left"></i></i> Back</a>
                        </div>
                        <div class="clr"></div>
                    </div>
                    <div class="main-details-block">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        
                                    <?php if(!empty($get_sender_details[0]['user_image']) && file_exists('images/buyer_image/'.$get_sender_details[0]['user_image'])){?>
                                    <img src="<?php echo base_url().'images/buyer_image/'.$get_sender_details[0]['user_image']; ?>" alt="" />
                                    <?php } else { ?>
                                    <img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
                                    <?php } ?> 
                                        
                                    </div>
                                    
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Buyer:
                                    </div>
                                    <div class="details-content-first">
                                        <?php if(isset($get_sender_details[0]['name'])) { echo $get_sender_details[0]['name']; } else { echo "Not Available"; } ?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Buyer Email:
                                    </div>
                                    <div class="details-content-first">
                                       <?php if(isset($get_sender_details[0]['email'])) { echo $get_sender_details[0]['email']; } else { echo "Not Available"; } ?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Review Date:
                                    </div>
                                    <div class="details-content-first">
                                        <?php if(isset($getreviewes[0]['commented_at'])) {  echo date('F j, Y', strtotime($getreviewes[0]['commented_at']))." at ".date("g:i a", strtotime($getreviewes[0]['commented_at'])); } else { echo "Not Available"; } ?> 
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Rating:
                                    </div>
                                    <?php if(isset($getreviewes[0]['ratings'])) { ?>
                                    <div class="details-content-first">
                                            <?php 
                                            $ratings         = 0;
                                            $arv_rating      = 0;       
                                            $arv_rating      = $getreviewes[0]['ratings'];  // find rating avarage
                                            ?> 
                                            <span class="product-satrs-block">
                                            <?php
                                            $finalrate = $chekRating = mb_substr($arv_rating,  0, 4);
                                            for($i=1;$i<=5;$i++) {
                                            $selected = "";

                                            $point = $i - $finalrate;

                                            if(!empty($finalrate) && $i<=$finalrate) {
                                            ?>
                                            <img alt="img" src="<?php echo base_url();?>images/review/star-full.png" style="margin-top:11px;"/>
                                            <?php
                                            } else if($point == '0.5' || $point < '0.5' || $point < '1') {
                                            ?>
                                            <img alt="img" src="<?php echo base_url();?>images/review/star-half.png" style="margin-top:11px;"/>
                                            <?php 
                                            } else {
                                            ?>
                                            <img alt="img" src="<?php echo base_url();?>images/review/star-blank.png" style="margin-top:11px;"/>
                                            <?php 
                                            }
                                            } 
                                            ?>
                                            <span class="rivew-count" ><?php echo $arv_rating ; ?></span>  
                                    </div>
                                    <?php } else { echo "Not Available"; }?>
                                    <div class="clr"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if(isset($getreviewes[0]['comment']) && !empty($getreviewes[0]['comment'])) { ?>
                    <div class="main-details-block n-bg-block">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="" style="text-align:justify;">
                                    <div class="first-name-block-details">
                                        Review:
                                    </div>
                                    <div class="clearfix"></div>
                                    <?php if(isset($getreviewes[0]['comment'])) { echo $getreviewes[0]['comment']; } else { echo "Not Available"; }?>
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            <?php
            } else {
            $this->load->view('no-data-found');
            } ?>
           </div> 
        </div>
    </div>
</div>        
</div>