<?php $this->load->view('seller/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3><?php if(!empty($getProductDetail[0]['title'])) { echo ucfirst($getProductDetail[0]['title']); } else { echo "Not Available" ;} ?></h3>
        </div>
    </div>
</div>
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <?php $this->load->view('seller/left-bar'); ?>
                <div class="col-sm-9 col-md-9 col-lg-9">
                <div class="back-btn-main-block">
                    <!-- <div class="bck-btn">
                        <a href="#"><i class="fa fa-angle-double-left"></i> Back</a>
                    </div> -->
                    <?php if(isset($getProductDetail[0]['id'])) { $product_id = $getProductDetail[0]['id']; } else { $product_id = 0; } ?>
                    <div class="replay-btn">
                        <a class="btn-replay-block" href="javascript:history.back()"><i class="fa fa-reply"></i> back</a>
                        <a class="btn-delete-block" href="<?php echo base_url().'seller/edit_product/'.$product_id;?>"><i class="fa fa-pencil-square"></i> Edit</a>
                        <a class="btn-delete-block delete_selected_product" data-pid="<?php echo $product_id;  ?>" href="javascript:void(0);"><i class="fa fa-trash-o"></i> Delete</a>
                    </div>
                    <div class="clr"></div>
                </div>
                <?php $this->load->view('status-msg'); ?>
                    
                <?php if(isset($getProductDetail) && sizeof($getProductDetail) > 0) {?>

                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <div class="details-img-block">
                            <?php if(!empty($getProductDetail[0]['req_image']) && file_exists('images/seller_upload_product_image/'.$getProductDetail[0]['req_image'])){?>
                            <img style="height:162px;width:260px;"  src="<?php echo base_url().'images/seller_upload_product_image/'.$getProductDetail[0]['req_image']; ?>" alt="list_img_1" class="" />
                            <?php } else { ?>
                            <img src="<?php echo base_url().'images/seller_upload_product_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                            <?php } ?> 
                        </div>
                     </div>   
                    <div class="account-info-block"> 
                        <div id="description" class="details-descrip">
                            <div class="descrip-head-block">
                                <?php if(!empty($getProductDetail[0]['title'])) { echo ucfirst($getProductDetail[0]['title']); } else { echo "Not Available" ;} ?>
                            </div>
                            <div class="list_up"><span><i class="fa fa-folder-open"></i>
                                </span>
                                    <?php if(!empty($getProductDetail[0]['category_name'])) { echo substr($getProductDetail[0]['category_name'], 0 , 50); } else { echo "Not Available"; } ?> ->
                                    <?php if(!empty($getProductDetail[0]['subcategory_name'])) { echo substr($getProductDetail[0]['subcategory_name'], 0 , 50); } else { echo "Not Available"; } ?>
                            </div>
                            <div class="list_add"><span><i class="fa fa-map-marker"></i>
                                </span>

                                <?php if(!empty($getProductDetail[0]['location'])) { echo $getProductDetail[0]['location']; } else { echo "Not Available"; } ?> 
                            </div>
                            <div class="lsit_text_content">
                            <div class="list_up "><span><i class="fa fa-calendar" aria-hidden="true"></i> Posted Date :
                            </span>
                                 <?php echo date('F j, Y', strtotime($getProductDetail[0]['created_date']))." at ".date("g:i a", strtotime($getProductDetail[0]['created_date'])); ?> 
                            </div>
                            </div>
                            <div class="lsit_text_content">
                            <div class="list_up up-list-price"><span> <?php echo CURRENCY; ?>
                                </span>
                                     <?php if(!empty($getProductDetail[0]['price'])) { echo $getProductDetail[0]['price']; } else { echo "Not Available"; } ?> 
                            </div>
                            </div>
                            <div class="descrip-content-block">
                                <?php if(!empty($getProductDetail[0]['description'])) { echo $getProductDetail[0]['description']; } else { echo "Not Available"; } ?>
                            </div>
                        </div>
                    </div>

                <?php
                } else {
                    $this->load->view('no-data-found');
                }?>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  
  $('.delete_selected_product').click(function(){
        var product_id = jQuery(this).data("pid");
        swal({   
             title: "Are you sure?",   
             text: "You want to delete this product ?",  
             type: "warning",   
             showCancelButton: true,   
             confirmButtonColor: "#8cc63e",  
             confirmButtonText: "Yes",  
             cancelButtonText: "No",   
             closeOnConfirm: false,   
             closeOnCancel: false }, function(isConfirm){   
              if (isConfirm) 
              { 
                     swal("Deleted!", "Your product has been deleted.", "success"); 
                           location.href=site_url+"seller/delete_product/"+product_id;
              } 
              else
              { 
                     swal("Cancelled", "Your product is safe :)", "error");          
                } 
            });
    }); 

</script>     