<?php $this->load->view('seller/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3>Change Password</h3>
        </div>
    </div>
</div>
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
             <div class="row">
                <?php $this->load->view('seller/left-bar'); ?>
                <div class="col-sm-9 col-md-9 col-lg-9">
                    <div class="forgot-pass-main change-pass-block">
                    <?php $this->load->view('status-msg'); ?>
                    <div ng-controller="SellerChangePassCntrl">
                        <form  class=""
                               name="ChangePass" 
                               novalidate
                               ng-submit="ChangePass.$valid && ChangePassword();">
                           

                            <div class="login-form-block">
                                <div class="login-head-block">
                                    Change Password
                                </div>
                                <div class="login-content-block">
                                    Change your password.
                                </div>
                               

                                <div class="mobile-nu-block input-first" ng-class="{ 'has-error': ChangePass.oldpwd.$touched && ChangePass.oldpwd.$invalid }">
                                    <input 
                                          type="password" 
                                          name="oldpwd" 
                                          ng-model="user.oldpwd"
                                          class="beginningSpace_restrict CopyPast_restrict" 
                                          ng-minlength="6" 
                                          ng-maxlength="50" 
                                          ng-required="true" />

                                    <span class="highlight"></span>
                                    <div class="error-new-block"  ng-messages="ChangePass.oldpwd.$error" ng-if="ChangePass.$submitted || ChangePass.oldpwd.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            <p ng-message="minlength"   class="error">  Please enter at least six character</p>
                                            <p ng-message="maxlength"   class="error">  Password to long</p>
                                            <p ng-message="pwd"         class="error">  Password is not valid</p>
                                            </div>

                                        </div>
                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                              setTimeout(function(){
                                                $('.err_msg_div').removeAttr('style');
                                              },200);
                                            });
                                        </script>
                                        </div>
                                    <label>Old Password</label>
                                </div>


                                <div class="mobile-nu-block input-first" ng-class="{ 'has-error': ChangePass.newpwd.$touched && ChangePass.newpwd.$invalid }">
                                    <input type="password"
                                           name="newpwd" 
                                           ng-model="user.newpwd" 
                                           class="beginningSpace_restrict CopyPast_restrict"
                                           ng-minlength="6" 
                                           ng-maxlength="50" 
                                           ng-required="true" />

                                    <span class="highlight"></span>
                                    

                                    <div class="error-new-block" ng-messages="ChangePass.newpwd.$error" ng-if="ChangePass.$submitted || ChangePass.newpwd.$touched">
                                    <div>
                                    <div class="err_msg_div" style="display:none;">
                                        
                                        <p ng-message="required"    class="error">  This field is required</p>
                                        <p ng-message="minlength"   class="error">  Please enter at least six character</p>
                                        <p ng-message="maxlength"   class="error">  Password to long</p>
                                        <p ng-message="newpwd"      class="error">  Password is not valid</p>
                                        
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                            $(document).ready(function(){
                                              setTimeout(function(){
                                                $('.err_msg_div').removeAttr('style');
                                              },200);
                                            });
                                        </script>
                                    </div>




                                    <label>New Password</label>
                                </div>


                                <div class="mobile-nu-block input-first" ng-class="{ 'has-error': ChangePass.cmf_pwd.$touched && ChangePass.cmf_pwd.$invalid }">
                                    <input type="password" 
                                           name="cmf_pwd" 
                                           class="beginningSpace_restrict CopyPast_restrict"
                                           ng-model="user.cmf_pwd" 
                                           ng-minlength="6" 
                                           ng-maxlength="50" 
                                           ng-required="true" />

                                    <span class="highlight"></span>
                                    <div class="error-new-block"  ng-messages="ChangePass.cmf_pwd.$error" ng-if="ChangePass.$submitted || ChangePass.cmf_pwd.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            <p ng-message="minlength"   class="error">  Please enter at least six character</p>
                                            <p ng-message="maxlength"   class="error">  Password to long</p>
                                            <p ng-message="cmf_pwd"     class="error">  Password is not valid</p>
                                            <p id="err_cmpass"          class="error"></p>
                                            </div>

                                        </div>
                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                              setTimeout(function(){
                                                $('.err_msg_div').removeAttr('style');
                                              },200);
                                            });
                                        </script>
                                        </div>
                                    <label>Confirm Password</label>
                                </div>


                                <div class="btn-block-main-login">
                                    <div class="btn-login-block">
                                        <button class="login-btn" type="submit">Submit</button>
                                    </div>
                                    <div class="clr"></div>
                                </div>


                            </div>
                        </form>
                    </div>    


            <div class="clr"></div>
        </div>
                 </div>
        </div>
    </div>    
</div>
        