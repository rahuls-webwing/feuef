<?php $this->load->view('seller/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3>My inquiries</h3>
        </div>
    </div>
</div>
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
               <?php $this->load->view('seller/left-bar'); ?>
                <div class="col-sm-9 col-md-9 col-lg-9">
                    <?php $this->load->view('status-msg'); ?>
                    <div class="table-main-block">
                        <div class="table-responsive">
                            <table class="table inquiries-table">
                                <tbody>
                                    
                                    <tr class="table-head-block">
                                        <td class="rtl-padd-bock" style="padding-left:30px">Name</td>
                                        <td>Email</td>
                                        <td>Phone</td>
                                        <td>Product Category</td>
                                        <td>product Name</td>
                                        <td>Country</td>
                                        <td>Inquiry date</td>
                                        <td>Message</td>
                                        <td class="rtl-padd-bock-last" style="text-align: center; padding-right:25px">Action</td>
                                    </tr>
                                    <?php if(isset($my_inquires) && sizeof($my_inquires) > 0) { ?>

                                        <?php foreach ($my_inquires as $value) {
                                        $this->db->where('tbl_user_master.id' , $value['buyer_id']);
                                        $get_sender_details = $this->master_model->getRecords('tbl_user_master');
                                      
                                         if($value['is_read']=='yes'){ ?>
                                                <tr>
                                                    <td style="font-family:arial;padding-left:30px"><?php if(isset($get_sender_details[0]['name'])) { echo $get_sender_details[0]['name']; } else { echo "Not Available"; }?></td>
                                                    <td style="font-family:arial"><?php if(isset($get_sender_details[0]['email'])) { echo substr($get_sender_details[0]['email'], 0 , 10).'...'; } else { echo "Not Available"; }?></td>
                                                    <td style="font-family:arial"><?php if(isset($get_sender_details[0]['mobile_number'])) { echo $get_sender_details[0]['mobile_number']; } else { echo "Not Available"; }?></td>
                                                    <td><?php if(isset($value['category_name'])) { echo $value['category_name']; } else { echo "Not Available"; }?></td>
                                                    <td><?php if(isset($value['product_name'])) { echo $value['product_name']; } else { echo "Not Available"; }?></td>
                                                    <td><?php if(isset($value['country'])) { echo $value['country']; } else { echo "Not Available"; }?></td>
                                                    <td><?php echo date('F j, Y', strtotime($value['posted_date']))." at ".date("g:i a", strtotime($value['posted_date'])); ?> </td>
                                                    <td><?php if(!empty($value['description'])) { echo substr($value['description'], 0 , 10).'...'; } else { echo "Not Available"; } ?></td>
                                                    <td class="rtl-padd-bock-last" style="text-align: center; padding-right:25px">
                                                        <a href="<?php echo base_url().'seller/inquiry_details/'.$value['id']; ?>"><i class="fa fa-eye" style="font-size:1.2em;"></i></a>
                                                        <a class="delete_selected_inquiry" title="Delete "  style="text-decoration:none;" data-inquiry="<?php echo $value['id'];  ?>" href="javascript:void(0);"><i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                </tr>

                                            <?php } else { ?>

                                                <tr>
                                                    <td style="font-family:arial;padding-left:30px; color: #1c9aea;"><?php if(isset($get_sender_details[0]['name'])) { echo $get_sender_details[0]['name']; } else { echo "Not Available"; }?></td>
                                                    <td style="font-family:arial; color: #1c9aea;"><?php if(isset($get_sender_details[0]['email'])) { echo substr($get_sender_details[0]['email'], 0 , 10).'...'; } else { echo "Not Available"; }?></td>
                                                    <td style="font-family:arial; color: #1c9aea;"><?php if(isset($get_sender_details[0]['mobile_number'])) { echo $get_sender_details[0]['mobile_number']; } else { echo "Not Available"; }?></td>
                                                    <td style="color: #1c9aea;"><?php if(isset($value['category_name'])) { echo $value['category_name']; } else { echo "Not Available"; }?></td>
                                                    <td style="color: #1c9aea;"><?php if(isset($value['product_name'])) { echo $value['product_name']; } else { echo "Not Available"; }?></td>
                                                    <td style="color: #1c9aea;"><?php if(isset($value['country'])) { echo $value['country']; } else { echo "Not Available"; }?></td>
                                                    <td style="color: #1c9aea;"><?php echo date('F j, Y', strtotime($value['posted_date']))." at ".date("g:i a", strtotime($value['posted_date'])); ?> </td>
                                                    <td style="color: #1c9aea;"><?php if(!empty($value['description'])) { echo substr($value['description'], 0 , 10).'...'; } else { echo "Not Available"; } ?></td>
                                                    <td class="rtl-padd-bock-last" style="text-align: center; padding-right:25px">
                                                        <a href="<?php echo base_url().'seller/inquiry_details/'.$value['id']; ?>"><i class="fa fa-eye" style="font-size:1.2em;"></i></a>
                                                        <a class="delete_selected_inquiry" title="Delete "  style="text-decoration:none;" data-inquiry="<?php echo $value['id'];  ?>" href="javascript:void(0);"><i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                </tr>

                                            <?php } } ?>    

                                    <?php
                                    } else {
                                        ?><tr>
                                             <td  colspan="9" style="font-family:arial"><?php
                                                $this->load->view('no-data-found');
                                           ?></td>
                                          </tr>
                                        <?php
                                    }?>
                                    
                                </tbody>
                            </table>
                        </div>
                        <!--pagigation start here-->
                        <div style="margin-top:-12px;">
                          <?php echo $this->pagination->create_links(); ?>
                        </div>
                        <!--pagigation end here-->
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>

<script type="text/javascript">
  
  $('.delete_selected_inquiry').click(function(){
        var inquiry_id = jQuery(this).data("inquiry");

       // alert('this inquiry ID'); die;

        swal({   
             title: "Are you sure?",   
             text: "You want to delete this inquiry ?",  
             type: "warning",   
             showCancelButton: true,   
             confirmButtonColor: "#8cc63e",  
             confirmButtonText: "Yes",  
             cancelButtonText: "No",   
             closeOnConfirm: false,   
             closeOnCancel: false }, function(isConfirm){   
              if (isConfirm) 
              { 
                     swal("Deleted!", "Your inquiry has been deleted.", "success"); 
                           location.href=site_url+"seller/inquiry_delete/"+inquiry_id;
              } 
              else
              { 
                     swal("Cancelled", "Your inquiry is safe :)", "error");          
                } 
            });
    }); 

</script>   

