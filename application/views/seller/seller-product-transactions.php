<style type="text/css">
    .label-important, .badge-important {
        background-color: #fe1010;
    }
</style>
<?php $this->load->view('seller/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3>Product Transactions</h3>
        </div>
    </div>
</div>
 
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <?php $this->load->view('seller/left-bar'); ?>

                <div class="col-sm-12 col-md-8 col-lg-9">
                 
                    <div class="row">
                     <div class="col-sm-12 col-md-8 col-lg-12" style="padding-right: 0px;">
                       <form action="<?php echo base_url().'seller/product_payment_history'; ?>" method='get' > 
                             <div style="background-color: white;border: 1px solid #d2dfe7 !important;" class="select-bock-container live-filter-block pull-right">
                               <select class="frm-select" id="sort_by" name="sort_by" >

                                  <option value="">         All  </option>
                                  <option value="Daily"    <?php if(isset($_REQUEST['sort_by'])) { if($_REQUEST['sort_by']=="Daily"){ echo 'selected="selected"' ;  } }  ?>  >   Daily   </option>
                                  <option value="Weekly"   <?php if(isset($_REQUEST['sort_by'])) { if($_REQUEST['sort_by']=="Weekly"){ echo 'selected="selected"' ; } }  ?> >  Weekly  </option>
                                  <option value="Monthly"  <?php if(isset($_REQUEST['sort_by'])) { if($_REQUEST['sort_by']=="Monthly"){ echo 'selected="selected"'; } }  ?> > Monthly </option>
                                  <option value="Yearly"   <?php if(isset($_REQUEST['sort_by'])) { if($_REQUEST['sort_by']=="Yearly"){ echo 'selected="selected"' ; } }  ?> >  Yearly  </option>
                                  
                               </select>
                              </div>
                            </div>     

                             <div class="col-sm-12 col-md-2 col-lg-0" > 
                              <button class="update" type="submit"  style="max-width:100%; visibility:hidden;" id="sort_by_search" name="sort_by_search">Search</button>
                             </div>  
                      </form>  
                    </div> 
                
                 <div class="row" style="margin-top:3px;">
                   <div class="col-sm-12 col-md-8 col-lg-12" style="padding-right: 0px;">
                    <div class="table-responsive">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-hover fill-head" style="1px solid #ececec;">
                           <tr class="pay_head" >
                              <td style="text-indent:5px;">Date</td>
                              <td style="text-indent:5px;">Trans Id</td>
                              <td style="text-indent:5px;">Premium Membership</td>
                              <td style="text-indent:5px;">Benifit</td>
                              <td style="text-indent:5px;">Type</td>
                              <td style="text-indent:5px;">Status</td>
                              <td style="text-indent:5px;">Amount</td>
                           </tr>

  
                           <?php 
                           $total=0;
                           if(!empty($HistoryData)){ 
                                 
                                 foreach($HistoryData as $field => $value) {
                                  ?>
                                  <tr class="pay_row_one">
                                    <td style="padding:12px!important;">  <?php echo date('F j, Y', strtotime($value['payment_date']))." at ".date("g:i a", strtotime($value['payment_date'])); ?>  </td>
                                    <td style="padding:12px!important;"> 
                                       <?php if(isset($value['transaction_id'])) { echo $value['transaction_id']; } else { echo 'Not Available'; } ?>
                                    </td>
                                    <td style="padding:12px!important;"> Premium membership purchase for upload product's</td>
                                    <td style="padding:12px!important;"> 
                                       <?php if(isset($value['upload_qty'])) { echo 'Get '.$value['upload_qty'].' Products'; } else { echo 'Not Available'; } ?>
                                    </td>
                                    <td style="padding:12px!important;"> 
                                       <?php if(isset($value['pament_type'])) { echo $value['pament_type']; } else { echo 'Not Available'; } ?>
                                    </td>

                                    <td style="padding:12px!important;"> 
                                       <?php if(isset($value['payment_status'])) { echo $value['payment_status']; } else { echo 'Not Available'; } ?>
                                    </td>

                                    <td style="padding:12px!important;"> <?php echo CURRENCY; ?><?php echo $value['transaction_price']; ?> </td>
                                  </tr>
                                  <?php $total+=$value['transaction_price']; } }
                                  else
                                  { ?>
                                    <tr class="pay_row_one">
                                      <td align="center" style="padding:3px!important;" colspan="6"> 
                                        <div >
                                              <?php  $this->load->view('no-data-found'); ?>
                                        </div>
                                      </td>
                                    </tr>
                                  <?php } ?>
                                  <tr class="pay_row_one" >
                                      <td  style="padding:12px!important; ">&nbsp;</td>
                                      <td  style="padding:12px!important; ">&nbsp;</td>
                                      <td  style="padding:12px!important; ">&nbsp;</td>
                                      <td  style="padding:12px!important; ">&nbsp;</td>
                                       <td  style="padding:12px!important; ">&nbsp;</td>
                                      <td  style="padding:12px!important; text-align:right; "><b>Total</b></td>
                                      <td  align="" style="padding:12px!important; "> <b><?php echo CURRENCY; ?>  <?php echo $total.'.00'; ?></b></td>
                                  </tr>

                        </table>
                       </div>
                      </div> 
                    </div>
                  <!--pagigation start here-->
                 <div class="product-pagi-block">
                <?php echo $this->pagination->create_links(); ?>
               </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$("#sort_by").on('change', function(){

$("#sort_by_search").click();

});
</script>