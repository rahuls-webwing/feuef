<style type="text/css">
.table-main-block {
    margin-bottom: 30px;
    padding-top: 0px;
}
</style>
<?php $this->load->view('seller/top-bar'); ?>
    <div class="page-head-name">
        <div class="container">
            <div class="name-container-dash">
                <h3>Contacts</h3>
            </div>
        </div>
    </div>
    <div class="middel-container">
        <div class="inner-content-block">
            <div class="container">
                <div class="row">
                    <?php $this->load->view('seller/left-bar'); ?>
                    <div class="col-sm-9 col-md-9 col-lg-9">
                        <?php $this->load->view('status-msg'); ?>
                        <div class="table-main-block notifi-block-main">
                            <div class="table-responsive">
                                <table class="table inquiries-table">
                                    <tbody>
                                        <tr class="table-head-block">
                                            <th style="padding-left:30px">Name</th>
                                            <th>Email</th>
                                            <th>Contact No</th>
                                            <th>Country</th>
                                            <th class="rtl-padd-bock-last" style="text-align: center; padding-right:25px">Action</th>
                                        </tr>
                                        <?php if(isset($get_contact_list) && sizeof($get_contact_list) > 0) { ?>

                                            <?php foreach ($get_contact_list as $value) { ?>
                                              
                                              <tr>
                                                <td style="padding-left:30px"><?php echo $value['name']; ?></td>
                                                <td><?php echo $value['email']; ?></td>
                                                <td><?php echo $value['mobile_number']; ?></td>
                                                <td><?php echo $value['country']; ?></td>
                                                <td class="rtl-padd-bock-last" style="text-align: center; padding-right:25px">
                                                    <a class="delete_selected_contact" title="Delete "  style="text-decoration:none;" data-contact="<?php echo $value['id'];  ?>" href="javascript:void(0);"><i class="fa fa-trash-o"></i></a>
                                                    </td>
                                              </tr>

                                            <?php } ?>    

                                        <?php } else {
                                            ?><tr>
                                                 <td  colspan="9" style="font-family:arial"><?php
                                                    $this->load->view('no-data-found');
                                               ?></td>
                                              </tr>
                                            <?php
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!--pagigation start here-->
                              <?php echo $this->pagination->create_links(); ?>
                            <!--pagigation end here-->
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>    
<script type="text/javascript">
  
  $('.delete_selected_contact').click(function(){
        var contact_id = jQuery(this).data("contact");
        swal({   
             title: "Are you sure?",   
             text: "You want to delete this contact ?",  
             type: "warning",   
             showCancelButton: true,   
             confirmButtonColor: "#8cc63e",  
             confirmButtonText: "Yes",  
             cancelButtonText: "No",   
             closeOnConfirm: false,   
             closeOnCancel: false }, function(isConfirm){   
              if (isConfirm) 
              { 
                     swal("Deleted!", "Your contact has been deleted.", "success"); 
                           location.href=site_url+"seller/contact_delete/"+contact_id;
              } 
              else
              { 
                     swal("Cancelled", "Your contact is safe :)", "error");          
                } 
            });
    }); 

</script>      