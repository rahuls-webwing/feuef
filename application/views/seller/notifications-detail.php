<?php $this->load->view('seller/top-bar'); 

$from_id=$get_notifications[0]['seller_id'];

/*echo "<pre>";

  print_r($get_notifications);
echo "</pre>";*/

$to_id=$get_notifications[0]['buyer_id'];

$offerid="";
if(!empty($get_notifications[0]['url']))
{
$url=$get_notifications[0]['url'];

$offerid = substr($url, strrpos($url, '/') + 1); 
}

$btn_val=' Accept';
$disable='';
if(isset($get_notifications[0]['apply_offer_id']))
{
    $this->db->select("tbl_apply_for_offers.status");
    $this->db->where('tbl_apply_for_offers.id' , $get_notifications[0]['apply_offer_id']);
    $status = $this->master_model->getRecords('tbl_apply_for_offers');    
    if(!empty($status[0]['status']))
    {
        $status=$status[0]['status'];
        if($status=='Accepted')
        {
            $btn_val='Accepted';
            $disable='disabled';
        }
        else
        {
            $btn_val='Accept'; 
            $disable='';
        }
    }
        
    
}


if(isset($get_notifications[0]['buyer_id'])){ $buyer_id = $get_notifications[0]['buyer_id']; } else { $buyer_id = 0; }
$this->db->where('tbl_user_master.id' , $buyer_id);
$get_sender_details = $this->master_model->getRecords('tbl_user_master');
    

 $this->db->select("order_form");     
 $this->db->from("tbl_order_forms");
$this->db->where('id' , $get_notifications[0]['order_form_id']);
$pdf=$this->db->get()->result_array();
    $order_form="";
    if(sizeof($pdf)!=0)
    {
        $order_form=$pdf[0]['order_form'];    
    }
    

?>
<div class="page-head-name">
<div class="container">
    <div class="name-container-dash">
        <h3>Notifications Details</h3>
    </div>
</div>
</div>
<div class="middel-container">
<div class="inner-content-block">
    <div class="container">
        <div class="row">
           <?php $this->load->view('seller/left-bar'); ?>
           <div class="col-sm-9 col-md-9 col-lg-9">
            <?php if(isset($get_notifications) && sizeof($get_notifications) > 0) { ?>
              <input type="hidden" id="apply_offer_id" value="<?php echo $get_notifications[0]['apply_offer_id'] ?>">
                <div class="table-main-block">
                    <div class="back-btn-main-block" style="padding:10px;">
                        <div class="replay-btn">
                            <!-- <a class="btn-replay-block" href="#"><i class="fa fa-reply"></i> reply</a> -->
                            <a class="btn-delete-block" href="javascript:history.back()"><i class="fa fa-angle-double-left"></i></i> Back</a>
                        </div>
                        <div class="clr"></div>
                    </div>
                    <div class="main-details-block">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Notification:
                                    </div>
                                    <div class="details-content-first">
                                        <?php if(isset($get_notifications[0]['notification'])) { echo $get_notifications[0]['notification']; } else { echo "Not Available"; }?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <?php if(isset($get_notifications[0]['url']) && !empty($get_notifications[0]['url'])) { ?>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Url:
                                    </div>
                                    <div class="details-content-first">
                                        <?php if(isset($get_notifications[0]['url'])) { echo '<a href="'.base_url().$get_notifications[0]['url'].'">View details</a>'; } else { echo "Not Available"; }?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Sender:
                                    </div>
                                    <div class="details-content-first">
                                        <?php if(isset($get_sender_details[0]['name'])) { echo $get_sender_details[0]['name']; } else { echo "Not Available"; } ?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Sender Email:
                                    </div>
                                    <div class="details-content-first">
                                       <?php if(isset($get_sender_details[0]['email'])) { echo $get_sender_details[0]['email']; } else { echo "Not Available"; } ?>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                             <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Notification Date:
                                    </div>
                                    <div class="details-content-first">
                                        <?php if(isset($get_notifications[0]['created_date'])) {  echo date('F j, Y', strtotime($get_notifications[0]['created_date']))." at ".date("g:i a", strtotime($get_notifications[0]['created_date'])); } else { echo "Not Available"; } ?> 
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <?php if($order_form!=''){ ?>
                            <div class="col-sm-12 col-md-12 col-lg-12 inq-padding-bottom">
                                <div class="main-content-block">
                                    <div class="first-name-block-details short-width-block">
                                        Order Form:
                                    </div>
                                    <div class="details-content-first">
                                        <a class="btn btn-default" target="_blank" href="<?php echo base_url('uploads/invoice/'.$order_form); ?>"><span class="fa fa-cloud-download" title="Download Order Form"> Download</span></a> 
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>

                            <?php } ?>
                           
                        </div>
                    </div>
                    <?php if(isset($get_notifications[0]['details']) && !empty($get_notifications[0]['details'])) { ?>
                    <div class="main-details-block n-bg-block">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="" style="text-align:justify;">
                                    <div class="first-name-block-details">
                                        Details:
                                    </div>
                                    <?php if(isset($get_notifications[0]['details'])) { echo $get_notifications[0]['details']; } else { echo "Not Available"; }?>
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if($order_form!='') { ?>
                    <div class="main-details-block n-bg-block">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="" style="text-align:justify;">
                                    <div class="first-name-block-details">
                                        <button class="msg_btn accept_order" to_id="<?php echo $to_id; ?>" from_id="<?php echo $from_id; ?>" offerid="<?php echo $offerid; ?>" reqid=""  type="button" <?php echo $disable; ?> > <?php echo $btn_val; ?></button>
                                    </div>
                                    
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            <?php
            } else {
            $this->load->view('no-data-found');
            } ?>
           </div> 
        </div>
    </div>
</div>        
</div>



<script>

$(document).ready(function(){
    $('.accept_order').click(function(){
    

    var from_id=$(this).attr('from_id');
    var to_id=$(this).attr('to_id');
    var offer_id=$(this).attr('offerid');
    var apply_offer_id=$('#apply_offer_id').val();

    
    var offerd_id=4;
    swal({   
               title: "Are you sure?",   
               text : "You want to accept this offer ?",  
               type : "warning",   
               showCancelButton: true,   
               confirmButtonColor: "#8cc63e",  
               confirmButtonText: "Yes",  
               cancelButtonText: "No",   
               closeOnConfirm: false,   
               closeOnCancel: false }, function(isConfirm){   
               if (isConfirm) 
               { 
                   
                   $.ajax({
                        url:site_url+'seller/accept_order/'+offer_id+'/'+offerd_id,
                        type:'post',
                        data:{from_id:from_id,offer_id:offer_id,apply_offer_id:apply_offer_id,to_id:to_id},
                        dataType:'json',
                        success:function(data){
                            //alert(data.msg);
                            swal("Done!", data.msg, "success");

                            setTimeout(function(){
                               $('.confirm').click();
                                location.reload();
                              },2000); 

                        },
                        beforeSend:function()
                        {
                          ajaxindicatorstart();
                        },
                    
                    complete:function(response)
                    {
                      ajaxindicatorstop();
                    }
                   });
               }
               else
               { 
                  swal("Cancelled"); 
                  setTimeout(function(){
                    $('.confirm').click();
                  },500);  
               }

             });

    
    return false;
  });

    


});

</script>