<div class="col-sm-3 col-md-3 col-lg-3">
    <div class="footer_heading2 my-account-block">
        My Account
    </div>
    <div class="menu_name2 left-bar-block">
        <ul>
            <li><a data-hover="-&nbsp;Dashboard" href="<?php echo base_url().'seller/dashboard';?>">-&nbsp;Dashboard</a></li>
            <li><a data-hover="-&nbsp;Edit&nbsp;Profile" href="<?php echo base_url().'seller/edit';?>">-&nbsp;Edit&nbsp;Profile</a></li>
            <li><a data-hover="-&nbsp;Change&nbsp;Password" href="<?php echo base_url().'seller/change_password';?>">-&nbsp;Change&nbsp;Password</a></li>

            <li >
            <a data-hover="-&nbsp;Post&nbsp;Offer&nbsp;" href="<?php echo base_url().'seller/post_offer';?>"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='post_offer')
              { echo 'class="acti"'; }?> >-&nbsp;Post&nbsp;Offer&nbsp;</a></li>

            <li >
            <a data-hover="-&nbsp;Posted&nbsp;Offers" href="<?php echo base_url().'seller/offers';?>"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='offers')
              { echo 'class="acti"'; }?> >-&nbsp;Posted&nbsp;Offers</a></li>  

            <li >
            <a <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='offer_detail')
              { echo 'style="display:block;"'; } else {  echo 'style="display:none;"'; }?> data-hover="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Offer&nbsp;Detail" href="#" class="acti" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Offer&nbsp;Detail</a></li> 


            <li >
            <a data-hover="-&nbsp;Closed&nbsp;offers" href="<?php echo base_url().'seller/closed_offers';?>"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='closed_offers' )
              { echo 'class="acti"'; }?> >-&nbsp;Closed&nbsp;offers </a></li>

            <li <?php if($this->router->fetch_class()=='seller' &&  $this->router->fetch_method()=='closed_offer_detail')
              { echo 'style="display:block;"'; } else {  echo 'style="display:none;"'; }?>>
            <a  data-hover="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Closed&nbsp;Offers&nbsp;Detail" href="#" class="acti" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Closed&nbsp;Offers&nbsp;Detail</a></li>



            <li >
            <a data-hover="-&nbsp;Sent&nbsp;Offers&nbsp;(Requirements)" href="<?php echo base_url().'seller/sent_offers';?>"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='sent_offers')
              { echo 'class="acti"'; }?> >-&nbsp;Sent&nbsp;Offers&nbsp;(Requirements)</a></li>  

            <li >
            <a <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='sent_offer_detail')
              { echo 'style="display:block;"'; } else {  echo 'style="display:none;"'; }?> data-hover="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Sent&nbsp;Offer&nbsp;Details" href="#" class="acti" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Sent&nbsp;Offer&nbsp;Details</a></li>
           

            <li >
            <a data-hover="-&nbsp;Upload&nbsp;Products&nbsp;" href="<?php echo base_url().'seller/upload_product';?>"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='upload_product')
              { echo 'class="acti"'; }?> >-&nbsp;Upload&nbsp;Products&nbsp;</a></li>   


            <li >
            <a data-hover="-&nbsp;Uploaded&nbsp;Products" href="<?php echo base_url().'seller/products';?>"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='products')
              { echo 'class="acti"'; }?> >-&nbsp;Uploaded&nbsp;Products</a></li>   

            <li >
            <a data-hover="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Product&nbsp;Edit" href="#"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='edit_product')
              { echo 'class="acti"'; }else {  echo 'style="display:none;"'; }?> >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Product&nbsp;Edit</a></li>   


            <li >
            <a <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='product_detail')
              { echo 'style="display:block;"'; } else {  echo 'style="display:none;"'; }?> data-hover="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Product&nbsp;Detail" href="#" class="acti" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Product&nbsp;Detail</a></li>  

            <?php 
            $this->db->where('status' , 'Unblock');
            $this->db->where('is_read' , 'no');
            $this->db->where('tbl_seller_contact_inquires.seller_id' , $this->session->userdata('user_id'));
            $get_inq_count  = $this->master_model->getRecordCount('tbl_seller_contact_inquires');
            if($get_inq_count <= 9){ $inq_cnt = '0'.$get_inq_count; } else if($get_inq_count >= 10) { $inq_cnt = '10+'; }
            ?>
            <li >
            <a data-hover="-&nbsp;My&nbsp;Inquiries" href="<?php echo base_url().'seller/my_inquires';?>"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='my_inquires')
              { echo 'class="acti"'; }?> >-&nbsp;My&nbsp;Inquiries <?php if($get_inq_count > 0) { echo '<span class="count-block-new">'.$inq_cnt.'</span>';  } ?></a></li>  

            <li >
            <a <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='inquiry_details')
              { echo 'style="display:block;"'; } else {  echo 'style="display:none;"'; }?> data-hover="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Inquiry&nbsp;Details" href="#" class="acti" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Inquiry&nbsp;Details</a></li>

            <?php 
            $this->db->where('status' , 'Unblock');
            $this->db->where('is_read' , 'no');
            $this->db->where('tbl_seller_notifications.seller_id' , $this->session->userdata('user_id'));
            $get_noti_count  = $this->master_model->getRecordCount('tbl_seller_notifications');
            if($get_noti_count <= 9){ $noti_cnt = '0'.$get_noti_count; } else if($get_noti_count >= 10) { $noti_cnt = '10+'; }
            ?>
            <li >
            <a data-hover="-&nbsp;Notifications" href="<?php echo base_url().'seller/notifications';?>"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='notifications')
              { echo 'class="acti"'; }?> >-&nbsp;Notifications <?php if($get_noti_count > 0) { echo '<span class="count-block-new">'.$noti_cnt.'</span>';  } ?></a></li>  

            <li >
            <a <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='notification_details')
              { echo 'style="display:block;"'; } else {  echo 'style="display:none;"'; }?> data-hover="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Notifications&nbsp;Details" href="#" class="acti" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Notifications&nbsp;Details</a></li>

           
            <?php 
            $this->db->where('status' , 'Unblock');
            $this->db->where('is_read' , 'no');
            $this->db->where('tbl_seller_rating.seller_id' , $this->session->userdata('user_id'));
            $get_rev_count  = $this->master_model->getRecordCount('tbl_seller_rating');
            if($get_rev_count <= 9){ $rew_cnt = '0'.$get_rev_count; } else if($get_rev_count >= 10) { $rew_cnt = '10+'; }
            ?>
            <li >
            <a data-hover="-&nbsp;My&nbsp;Reviews" href="<?php echo base_url().'seller/reviews';?>"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='reviews')
              { echo 'class="acti"'; }?> >-&nbsp;My&nbsp;Reviews <?php if($get_rev_count > 0) { echo '<span class="count-block-new">'.$rew_cnt.'</span>';  } ?></a></li>  

            <li >
            <a <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='reviews_detail')
              { echo 'style="display:block;"'; } else {  echo 'style="display:none;"'; }?> data-hover="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Review&nbsp;Details" href="#" class="acti" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Review&nbsp;Details</a></li>
         
            <li >
            <a data-hover="-&nbsp;Order&nbsp;forms" href="<?php echo base_url().'seller/order_forms';?>"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='order_forms')
              { echo 'class="acti"'; }?> >-&nbsp;Order&nbsp;forms</a></li> 

            <li >
            <a data-hover="-&nbsp;Post&nbsp;Offer&nbsp;Transactions" href="<?php echo base_url().'seller/offer_payment_history';?>"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='offer_payment_history')
              { echo 'class="acti"'; }?> >-&nbsp;Post&nbsp;Offer&nbsp;Transactions</a></li>  

            <li >
            <a data-hover="-&nbsp;Upload&nbsp;Product&nbsp;Transactions" href="<?php echo base_url().'seller/product_payment_history';?>"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='product_payment_history')
              { echo 'class="acti"'; }?> >-&nbsp;Upload&nbsp;Product&nbsp;Transactions</a></li>    


            <li >
            <a data-hover="-&nbsp;Make&nbsp;Offer&nbsp;Transactions" href="<?php echo base_url().'seller/make_offer_payment_history';?>"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='make_offer_payment_history')
              { echo 'class="acti"'; }?> >-&nbsp;Make&nbsp;Offer&nbsp;Transactions</a></li>   


            <li >
            <a data-hover="-&nbsp;Blog&nbsp;add" href="<?php echo base_url().'blogs/add';?>"
              <?php if($this->router->fetch_class()=='blogs' && $this->router->fetch_method()=='add')
              { echo 'class="acti"'; }?> >-&nbsp;Blog&nbsp;add</a></li>  

            <li >
            <a data-hover="-&nbsp;Blog&nbsp;Manage" href="<?php echo base_url().'blogs/manage';?>"
              <?php if($this->router->fetch_class()=='blogs' && $this->router->fetch_method()=='manage')
              { echo 'class="acti"'; }?> >-&nbsp;Blog&nbsp;Manage</a></li>    


            <li >
            <a <?php if($this->router->fetch_class()=='blogs' && $this->router->fetch_method()=='edit')
              { echo 'style="display:block;"'; } else {  echo 'style="display:none;"'; }?> data-hover="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Blog&nbsp;Edit" href="#" class="acti" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Blog&nbsp;Edit</a></li>


            <li >
            <a data-hover="-&nbsp;Contacts" href="<?php echo base_url().'seller/contact_list';?>"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='contact_list')
              { echo 'class="acti"'; }?> >-&nbsp;Contacts</a></li>


            <!-- <li >
            <a data-hover="-&nbsp;Product&nbsp;Order&nbsp;Received" href="<?php echo base_url().'seller/product_order_received';?>"
              <?php if($this->router->fetch_class()=='seller' && $this->router->fetch_method()=='product_order_received')
              { echo 'class="acti"'; }?> >-&nbsp;Product&nbsp;Order&nbsp;Received</a></li>
 -->
           
          <br>
        </ul>
    </div>
</div>


<script type="text/javascript">
	//left-side bar active class script start here
	var pathname = window.location.pathname;
	var globle_last_param = pathname.substring(pathname.lastIndexOf('/') + 1);

	$('.left-bar-block').find('li > a').each(function(index) {

		var current_url = this.href;
		var current_last_param = current_url.substring(current_url.lastIndexOf('/') + 1);

		if (globle_last_param == current_last_param) {
			$(this).addClass('acti');
		} else {
			//$(this).removeClass('acti');
		}
	});
	//left-side bar active class script start here
</script>


<script type="text/javascript">
	$(document).ready(function() {				
		// footer dropdown links start 
		var min_applicable_width = 767;

		$(document).ready(function() {
			applyResponsiveSlideUp($(this).width(), min_applicable_width);
		});

		function applyResponsiveSlideUp(current_width, min_applicable_width) {
			/* Set For Initial Screen */
			initResponsiveSlideUp(current_width, min_applicable_width);

			/* Listen Window Resize for further changes */
			$(window).bind('resize', function() {
				if ($(this).width() <= min_applicable_width) {
					unbindResponsiveSlideup();
					bindResponsiveSlideup();
				} else {
					unbindResponsiveSlideup();
				}
			});
		}

		function initResponsiveSlideUp(current_width, min_applicable_width) {
			if (current_width <= min_applicable_width) {
				unbindResponsiveSlideup();
				bindResponsiveSlideup();
			} else {
				unbindResponsiveSlideup();
			}
		}

		function bindResponsiveSlideup() {
			$(".menu_name2").hide();
			$(".footer_heading2").bind('click', function() {
				var $ans = $(this).parent().find(".menu_name2");
				$ans.slideToggle();
				$(".menu_name2").not($ans).slideUp();
				$('.menu_name2').removeClass('active');

				$('.footer_heading2').not($(this)).removeClass('active');
				$(this).toggleClass('active');
				$(this).parent().find(".menu_name2").toggleClass('active');
			});
		}

		function unbindResponsiveSlideup() {
			$(".footer_heading2").unbind('click');
			$(".menu_name2").show();
		}
		// footer dropdown links end

	});

</script>