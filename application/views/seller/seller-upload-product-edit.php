<style type="text/css">
    .label-important, .badge-important {
        background-color: #fe1010;
    }
</style>
<?php $this->load->view('seller/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3>Edit Product</h3>
        </div>
    </div>
</div>
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <?php $this->load->view('seller/left-bar'); ?>
                <div class="col-sm-9 col-md-9 col-lg-9">
                    <div class="account-info-block">
                        <div class="row">                            
                          <?php $this->load->view('status-msg'); ?>   
                         <div ng-controller="editUploadProduct">    
                            <form  
                               class=""
                               ng-init="loadeditproduct('<?php echo $product_id; ?>');"
                               name="productForm" 
                               enctype="multipart/form-data"
                               novalidate 
                               ng-submit="productForm.$valid && storeProduct('<?php echo $product_id; ?>');"  >
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Product Name <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block"
                                        ng-class="{'has-error':productForm.title.$touched && productForm.title.$invalid}">
                                            <input type="text" 
                                                   name="title" 
                                                   class="beginningSpace_restrict " 
                                                   ng-model="user.title"
                                                   ng-required="true"
                                                   ng-maxlength="100"
                                                   placeholder="Enter Title"/>
                                        </div>

                                        <div class="error-new-block" ng-messages="productForm.title.$error" ng-if="productForm.$submitted || productForm.title.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            <p ng-message="maxlength"   class="error">  This field to long. Only 100 characters allow.</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                    </div>
                                </div>
                                
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Product category <span class="required-field"><sup>*</sup></span></div>

                                        <div class="select-bock-container" ng-class="{'has-error':productForm.subcategory_id.$touched && productForm.subcategory_id.$invalid}" >
                                            <select name="subcategory_id" 

                                                    ng-model="user.subcategory_id"
                                                    ng-required="true">


                                                          <option value="">Select offer category</option>


                                                          <?php foreach ($getCat as $cate_value) {
                                                          ?>
                                                              <optgroup label="<?php echo '- '.$cate_value['category_name']; ?>" >
                                                              <?php foreach ($getSubcat as $value) {
                                                              ?>

                                                                   
                                                                      
                                                                      <?php if($value['category_id'] ==  $cate_value['category_id']) { ?>

                                                                        <option value="<?php echo $value['subcategory_id']; ?>"><?php echo $value['subcategory_name']; ?></option>

                                                                     <?php  } ?>

                                                                   
                                                               <?php
                                                              } ?>     
                                                             </optgroup>

                                                          <?
                                                          } ?>
                                            </select>


                                            <div class="error-new-block" ng-messages="productForm.subcategory_id.$error" ng-if="productForm.$submitted || productForm.subcategory_id.$touched">
                                            <div>
                                            <div class="err_msg_div" style="display:none;">
                                                <p ng-message="required"    class="error">  This field is required</p>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                    $(document).ready(function(){
                                                      setTimeout(function(){
                                                        $('.err_msg_div').removeAttr('style');
                                                      },200);
                                                    });
                                                </script>
                                            </div>


                                        </div>

                                    </div>
                                </div>


                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Price (<?php echo CURRENCY; ?>) <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block"
                                        ng-class="{'has-error':productForm.price.$touched && productForm.price.$invalid}">
                                            <input type="text" 
                                                   name="price" 
                                                   class="beginningSpace_restrict " 
                                                   ng-model="user.price"
                                                   ng-required="true"
                                                   placeholder="Enter price"/>
                                        </div>

                                        <div class="error-new-block" ng-messages="productForm.price.$error" ng-if="productForm.$submitted || productForm.price.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                    </div>
                                </div>
                                
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Location <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block" ng-class="{'has-error':productForm.location.$touched && productForm.location.$invalid}">
                                            <input type="text" 
                                                   name="location" 
                                                   class="beginningSpace_restrict" 
                                                   ng-model="user.location" 
                                                   placeholder="Enter Location"
                                                   ng-autocomplete
                                                   ng-required="true"
                                                   details="obj_autocomplete"/>
                                        </div>

                                        <div class="error-new-block" ng-messages="productForm.location.$error" ng-if="productForm.$submitted || productForm.location.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>


                                    </div>
                                </div>   

                                <input type="hidden" name="latitude"  id="latitude" ng-model="user.latitude" placeholder="latitude">
                                <input type="hidden" name="longitude" id="longitude" ng-model="user.longitude" placeholder="longitude">                                
                                
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="email-box-2">
                                        <div class="email-title">Product description <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block" ng-class="{'has-error':productForm.description.$touched && productForm.description.$invalid}">
                                            <textarea rows="" 
                                                      cols="" 
                                                      name="description" 
                                                      class="beginningSpace_restrict" 
                                                      ng-model="user.description"
                                                      ng-required="true"
                                                      ng-maxlength="1000"
                                                      placeholder="Enter Product description"></textarea>
                                        </div>

                                        <div class="error-new-block" ng-messages="productForm.description.$error" ng-if="productForm.$submitted || productForm.description.$touched">
                                        <div>
                                        <div class="err_msg_div" style="display:none;">
                                            <p ng-message="required"    class="error">  This field is required</p>
                                            <p ng-message="maxlength"   class="error">  This field to long. Only 1000 characters allow.</p>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                                $(document).ready(function(){
                                                  setTimeout(function(){
                                                    $('.err_msg_div').removeAttr('style');
                                                  },200);
                                                });
                                            </script>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12">                                
                                <div class="requirement-upload"> 
                                 <div class="email-title">Product Image <span class="required-field"><sup>*</sup></span></div>                                    
                                    <div class="fileUpload">    
                                        <!-- <img src="<?php echo base_url(); ?>/front-asset/images/gallrey.png" alt="" /> -->
                                        <img ng-src="{{req_image}}"  src="<?php echo base_url(); ?>/front-asset/images/gallrey.png" id="img_profile" 
                                        styel="req_img" class="" alt="profile" />
                                        <input type="file" 
                                               class="upload allowOnlyImg"
                                               name="req_image" 
                                               id="req_image" 
                                               ng-model="user.req_image" />
                                    </div>

                                    <div class="img-err">
                                        <div id="err-img" style="color:red;" class="" ></div>
                                    </div>
                                    <div class="img-err">
                                        <div style="color:#007aca; margin-top: 10px;" class="" ><b><span class="label label-important">NOTE!</span> Image allow greater than 162 x 260 (h X w) dimension only and also image is mandatory.</b></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="btn-password save">
                                        <button style="max-width: 130px;" class="change-btn-pass" type="submit">Update</button>
                                    </div>
                                </div>
                           </div>  
                       </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLKCmJEIrOrzdRklZHYer8q9qx2XLJ4Vs&sensor=false&libraries=places"></script>

<script type="text/javascript">
$(document).ready(function(){
 $('#req_image').click(function(){
    ajaxindicatorstart();
    setTimeout(function(){
      ajaxindicatorstop();
    },2000);
 });
}); 
</script>

