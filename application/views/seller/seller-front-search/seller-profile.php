<script src="<?php echo base_url().'front-asset/review/'; ?>jstarbox.js"></script>
<link rel="stylesheet" href="<?php echo base_url().'front-asset/review/'; ?>jstarbox.css" type="text/css" media="screen" charset="utf-8" />
<?php if(isset($get_profile_data) && sizeof($get_profile_data) > 0) { ?>
  <div ng-controller="quickCallctrl">
    <div class="page-head-block">
        <div class="page-head-overlay"></div>
        <div class="container">
            <div class="profile-pic-main">
                <div class="pic-profile-block">
                    
                    <?php if(!empty($get_profile_data[0]['user_image']) && file_exists('images/seller_image/'.$get_profile_data[0]['user_image'])){?>
                    <img style="height:100%;" src="<?php echo base_url().'images/seller_image/'.$get_profile_data[0]['user_image']; ?>" alt="" />
                    <?php } else { ?>
                    <img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
                    <?php } ?> 

                </div>
                <div class="profile-discription">
                    <div class="name-head-user">
                        <?php if(!empty($get_profile_data[0]['name'])) {
                            echo $get_profile_data[0]['name'];
                          } else { 
                            echo "Not Available"; 
                          } 
                        ?> 
                    </div>
                    <div class="address-user">
                        <?php if(!empty($get_profile_data[0]['city'])) {
                            echo $get_profile_data[0]['city'];
                          } else { 
                            echo "Not Available"; 
                          } 
                        ?> ,
                        <?php if(!empty($get_profile_data[0]['country'])) {
                            echo $get_profile_data[0]['country'];
                          } else { 
                            echo "Not Available"; 
                          } 
                        ?> 
                    </div>
                    <div class="other-info-user">
                      <?php $this->load->view('seller/seller-front-search/search-header'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-head-name seller-tab-block" set-class-when-at-top="fix-to-top" extra-offset-adjustment="150">
        <div class="container">
            <div class="name-container-dash">
               <ul>
                   <li><a class="menu_class" href="javascript:void(0);" lang="description"><h4>Description</h4></a></li>
                   <li><a class="menu_class" href="javascript:void(0);" lang="products"><h4>Products</h4></a></li>
                   <li><a class="menu_class" href="javascript:void(0);" lang="review_rating"><h4>Review &amp; Rating</h4></a></li>
               </ul>                
            </div>
        </div>
    </div>
    <div class="middel-container">
        <div class="inner-content-block">
            <div class="container">
                <?php $this->load->view('status-msg'); ?>
                <div class="row">                    
                    <div class="col-sm-8 col-md-9 col-lg-9">
                        <div id="description" class="main-descrip">
                            <div class="descrip-head-block">
                                Description
                            </div>
                            <div class="descrip-content-block" style="text-align:justify;">
                                <?php if(!empty($get_profile_data[0]['company_description'])) {
                                echo $get_profile_data[0]['company_description'];
                                } else { 
                                echo "Not Available"; 
                                } 
                                ?> 
                            </div>
                        </div>
                        <div id="products" class="products-main-block">
                            <div class="descrip-head-block">
                                <!-- Products -->
                            </div>
                            <div class="vagitable-main">
                                <div class="vagita-head-block">
                                    Products
                                </div>
                                <div class="green-border-block"></div>
                            </div>
                        </div>
                        

                        <?php if(isset($getProducts) && sizeof($getProducts)) { ?>
                            <div class="row col-block-mobile">
                            <?php $show_product = 0; foreach ($getProducts as $product) {  $show_product++; ?>

                               
                                    <?php if($show_product <= 6){ ?>

                                        <div class="col-sm-6 col-md-4 col-lg-4">
                                        <div class="product-block-main">
                                        <span class="img-block-product">
                                        <?php if(!empty($product['req_image']) && file_exists('images/seller_upload_product_image/'.$product['req_image'])){?>
                                        <img style="height:162px;width:260px;" src="<?php echo base_url().'images/seller_upload_product_image/'.$product['req_image']; ?>" alt="list_img_1" class="" />
                                        <?php } else { ?>
                                        <img style="height:162px;width:260px;" src="<?php echo base_url().'images/seller_upload_product_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                                        <?php } ?> 
                                        </span>
                                        <span class="product-content-block">
                                        <div class="icon-block-view">
                                        <ul>
                                        <!--<li><a href="#"><i class="fa fa-heart"></i></a></li>-->
                                        <!-- <li><a style="padding-top: 7px;" href="#"><i class="fa fa-envelope"></i></a></li> -->
                                        <li><a style="padding-top: 7px;" href="<?php echo base_url().'seller/product_details/'.$product['id'].'/'.$product['seller_id'] ?>"><i class="fa fa-eye"></i></a></li>
                                        </ul>
                                        </div>
                                        <span class="product-name-block">
                                        <?php if(!empty($product['title'])) { echo ucfirst($product['title']); } else { echo "Not Available" ;} ?>
                                        </span>
                                        <?php /* 
                                        <span class="product-satrs-block">
                                        <i class="fa fa-star star-acti"></i> <i class="fa fa-star star-acti"></i> <i class="fa fa-star star-acti"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> 
                                        </span>
                                        <span class="reviews-block-txt">
                                        4 review
                                        </span>
                                        </span>
                                        */ ?>
                                        <div class="item-content-block">
                                        <?php if(!empty($product['description'])) { echo substr($product['description'], 0 , 52).'...'; } else { echo "<span class='label label-important'><i class='fa fa-ban' aria-hidden='true'></i></span> Product description is not available."; } ?>
                                        </div>
                                        </div>
                                        </div>

                                    <?php } ?>
                                    
                                

                            <?php } ?>
                            </div>          

                        <?php if(count($getProducts) > 6) { ?>
                        <div class="view-all-reviews">
                        <a class="all-reviews-btn" href="<?php echo base_url().'seller/all_products/'.$product['seller_id']; ?>">View all Products</a>
                        </div>
                        <?php } ?>


                        <?php } else { ?>
                        <div class="row col-block-mobile">    
                        <div class="col-sm-12 col-md-12 col-lg-12">
                        <?php $this->load->view('no-data-found'); ?>
                        </div>
                        </div>
                        <?php } ?>

                         <div id="review_rating"></div>
                        
                        <?php if(isset($getreview) && sizeof($getreview) > 0) { ?>
                           <div class="reviews-maib-block"  id="reviews-maib-block">
                            <div class="descrip-head-block">
                                reviews (<?php echo count($getreview) ?>)
                            </div>
                            <?php $cnt=0; foreach ($getreview as  $review) { $cnt++;
                                $get_buyer_details = $this->master_model->getRecords('tbl_user_master',array('id'=>$review['buyer_id']));
                                 
                                if($cnt <=3){
                                ?>
                                    <div class="revews-block-seller">
                                        <div class="profile-pic-review-seller">
                                            <?php if(!empty($get_buyer_details[0]['user_image']) && file_exists('images/buyer_image/'.$get_buyer_details[0]['user_image'])){?>
                                            <img src="<?php echo base_url().'images/buyer_image/'.$get_buyer_details[0]['user_image']; ?>" alt="" />
                                            <?php } else { ?>
                                            <img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
                                            <?php } ?> 
                                        </div>
                                        <div class="arrow_box">
                                            <div class="name-review-block">
                                                <div class="review-block-name">
                                                    <?php if(isset($get_buyer_details[0]['name'])) { echo $get_buyer_details[0]['name'];} else { echo "Not Available" ;} ?>
                                                </div>
                                                <div class="review-time-date">
                                                    on <?php echo date('F j, Y', strtotime($review['commented_at']))." at ".date("g:i a", strtotime($review['commented_at'])); ?> 
                                                </div>
                                                <div class="clr"></div>
                                            </div>
                                            <?php 
                                                $ratings         = 0;
                                                $arv_rating      = 0;       
                                                $arv_rating      = $review['ratings'];  // find rating avarage
                                                ?> 
                                                <span class="product-satrs-block">
                                                <?php
                                                    $finalrate = $chekRating = mb_substr($arv_rating,  0, 4);
                                                    for($i=1;$i<=5;$i++) {
                                                    $selected = "";

                                                    $point = $i - $finalrate;

                                                    if(!empty($finalrate) && $i<=$finalrate) {
                                                      ?>
                                                       <img alt="img" src="<?php echo base_url();?>images/review/star-full.png" style="margin-top:11px;"/>
                                                       <?php
                                                     } else if($point == '0.5' || $point < '0.5' || $point < '1') {
                                                      ?>
                                                      <img alt="img" src="<?php echo base_url();?>images/review/star-half.png" style="margin-top:11px;"/>
                                                       <?php 
                                                    } else {
                                                      ?>
                                                      <img alt="img" src="<?php echo base_url();?>images/review/star-blank.png" style="margin-top:11px;"/>
                                                      <?php 
                                                    }
                                                } 
                                            ?> 
                                            </span>
                                            <div class="reviews-msg-block">
                                                <?php if(isset($review['comment'])) { echo $review['comment'];} else { echo "Not Available" ;} ?>
                                            </div>                                    
                                        </div>
                                    </div>
                               
                              <?php } } ?>
                             </div>
                            <?php if(count($getreview) > 3){
                            ?>
                             <div class="view-all-reviews">
                                <a class="all-reviews-btn" href="<?php echo base_url().'seller/all_reviews/'.$get_profile_data[0]['id'];?>">View all Reviews</a>
                            </div>
                            <?php
                            } ?>
                           
                        <?php } else { ?>
                        <div class="reviews-maib-block">
                        <?php $this->load->view('no-data-found'); ?>
                        </div>
                        <?php } ?>

                        <?php
                        if(!empty($this->session->userdata('user_id') && $this->session->userdata('user_type')=='Buyer')){
                           
                           $this->db->where('tbl_buyer_post_requirement.buyer_id' , $this->session->userdata('user_id'));
                           $this->db->where('tbl_apply_for_requirment.offered_seller_id' , $get_profile_data[0]['id']);
                           $this->db->where('tbl_apply_for_requirment.status' , 'Accepted');
                           $this->db->join('tbl_buyer_post_requirement' , 'tbl_buyer_post_requirement.id=tbl_apply_for_requirment.requirment_id');
                           $get_permission_to_review = $this->master_model->getRecordCount('tbl_apply_for_requirment');

                                if($get_permission_to_review > 0) {  
                                ?>
                                    <div  class="reviews-maib-block">
                                        <input type="hidden" id="seller_id" name="seller_id" value="<?php echo $get_profile_data[0]['id']; ?>">
                                        <div class="descrip-head-block">
                                            Rate and write a review
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-lg-12">
                                            <div class="form-group">
                                            <div class="form_right">
                                            <div class="starbox colours ghosting small autoupdate" data-button-count="10" data-star-count="5"> </div><span class="rivew-count" >0</span>
                                            </div>
                                            </div>
                                            </div>
                                            <input type="hidden"  id="review_count" name="review_count">
                                            <div class="col-sm-6 col-md-6 col-lg-6">
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12">
                                                <div class="mobile-nu-block form-review">
                                                    <textarea rows="" cols="" id="txt_comment" ng-required="true" name="txt_comment" ></textarea>
                                                    <span class="highlight"></span>
                                                    <label>Your reviews here ...</label>                            
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12">
                                                <!-- <span class="product-satrs-block review-from-block">
                                                    <i class="fa fa-star star-acti"></i> <i class="fa fa-star star-acti"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> 
                                                </span>  --> 
                                                <span id="show_error" class="show_error"></span>
                                                <span id="show_success" ></span>          
                                                <div class="submit-btn-blok">
                                                    <button class="btn-submit-review" id="btn_ratings" type="button">Submit your reviews</button>
                                                </div>                              
                                            </div>
                                        </div>
                                    </div>
                        <?php } } ?>



                    </div>
                    <div class="col-sm-4 col-md-3 col-lg-3">

                        <?php
                        if(!empty($this->session->userdata('user_id') && $this->session->userdata('user_type')=='Buyer')) {?>
                            <div class="send-inquiry-block">
                                

                                <!-- Previous way to add contact -->
                                <!-- <a data-backdrop="static" data-seller_id="<?php echo $get_profile_data[0]['id']; ?>" data-toggle="modal" data-target="#add_to_contact" type="button" class="send-inquiry-btn seller_id">Add to Contact</a> -->
                                <!-- end Previous way to add contact -->


                                <a data-sellerid="<?php echo $get_profile_data[0]['id']; ?>" type="button" class="send-inquiry-btn add_seller_in_contact">Add to Contact</a>


                                <br/>
                                <a data-backdrop="static"  data-sellid="<?php echo $get_profile_data[0]['id']; ?>"
                                 data-toggle="modal" data-target="#myModal" type="button" class="send-inquiry-btn sellid">Message Seller</a>
                            </div>
                           <?php
                        }
                        ?>
                        <div class="descrip-head-block">
                            Categories
                        </div>
                        <div class="min-menu cates-block-cantainer">
                            <ul>

                                <?php if(isset($getCat) && sizeof($getCat)) { ?>

                                  <?php foreach ($getCat as $category) { ?>
                                    <li><a  data-hover="<?php echo $category['category_name']; ?>" href="<?php echo base_url().'search?cat_id%5B%5D='.$category['category_id']; ?>"><?php echo $category['category_name']; ?></a></li>
                                  <?php } ?> 

                                <?php } else { ?>
                                <div class="row col-block-mobile">    
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                <?php $this->load->view('no-data-found'); ?>
                                </div>
                                </div>
                                <?php } ?>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>
 </div>   



<?php } else {

  ?><div style="margin:20px;"><?php 
  $this->load->view('no-data-found');
  ?></div><?php

} ?>

<?php
if($this->session->userdata('user_type')=='Buyer')
{
?>
<!-- Modal -->
<div id="myModal" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">Message Seller</h4>
        </div>
        <div class="modal-body">
        <?php $this->load->view('status-msg'); ?>
         <div ng-controller="ContactSeller">  
            <form  
            id="contacttoseller"
            class=""
            name="contacttoseller" 
            enctype="multipart/form-data"
            novalidate 
            ng-submit="contacttoseller.$valid && contact_to_seller();"  >

            <input type="hidden" name="seller_id" id="seller_id" ng-model="user.seller_id" placeholder="Seller id">
           
            <div class="select-bock-container drop" ng-class="{'has-error':contacttoseller.category_id.$touched && contacttoseller.category_id.$invalid}" >
            <select name="category_id" 
                    ng-model="user.category_id"
                    ng-required="true">
                    <option value="" >Category</option>
                    <?php foreach ($getCat as $cate_value) {?>
                    <option value="<?php echo $cate_value['category_id']; ?>"><?php echo $cate_value['category_name']; ?></option>
                    <?php } ?>
            </select>
            <div class="error-new-block" ng-messages="contacttoseller.category_id.$error" ng-if="contacttoseller.$submitted || contacttoseller.category_id.$touched">
            <div>
            <div class="err_msg_div" style="display:none;">
                <p ng-message="required"    class="error">  This field is required</p>
                </div>
            </div>
            <script type="text/javascript">
                    $(document).ready(function(){
                      setTimeout(function(){
                        $('.err_msg_div').removeAttr('style');
                      },200);
                    });
                </script>
            </div>
           </div>
            <div class="mobile-nu-block enquiry" ng-class="{'has-error':contacttoseller.product_name.$touched && contacttoseller.product_name.$invalid}">
                <input type="text" 
                       name="product_name" 
                       class="beginningSpace_restrict" 
                       ng-model="user.product_name" 
                       placeholder="Enter product name"
                       ng-required="true"
                />
                <div class="error-new-block" ng-messages="contacttoseller.product_name.$error" ng-if="contacttoseller.$submitted || contacttoseller.product_name.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>
                <span class="highlight"></span>
                <label>Product Name</label>
            </div>
            <div class="mobile-nu-block enquiry" ng-class="{'has-error':contacttoseller.country.$touched && contacttoseller.country.$invalid}">
                <input type="text" 
                           name="country" 
                           class="beginningSpace_restrict" 
                           ng-model="user.country" 
                           placeholder="Enter country"
                           ng-required="true"
                           />
                <div class="error-new-block" ng-messages="contacttoseller.country.$error" ng-if="contacttoseller.$submitted || contacttoseller.country.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>

                <span class="highlight"></span>
                <label>Country</label>

            </div>
            
            <div class="mobile-nu-block input-first box-msg" ng-class="{'has-error':contacttoseller.description.$touched && contacttoseller.description.$invalid}">
                <textarea rows="" 
                              cols="" 
                              name="description" 
                              class="beginningSpace_restrict" 
                              ng-model="user.description"
                              ng-required="true"
                              ng-maxlength="1000"
                              placeholder="Enter offer description"></textarea>

                <span class="highlight"></span> 
                <label>Message</label>

                <div class="error-new-block" ng-messages="contacttoseller.description.$error" ng-if="contacttoseller.$submitted || contacttoseller.description.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    <p ng-message="maxlength"   class="error">  This field to long. Only 1000 characters allow.</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>

            </div>
            </div>
            <div class="modal-footer">
                <div class="btn-password changes enq">
                    <button class="change-btn-pass " type="submit">Send</button>
                </div>
            </div>
          </form>
       </div>
    </div>

</div>
</div>
</div>

<!-- Modal -->
<div id="add_to_contact" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">Add to Contact</h4>
        </div>
        <div class="modal-body">
        <?php $this->load->view('status-msg1'); ?>
         <div ng-controller="AddtoContact">  
            <form  
            id="addtocontact"
            class=""
            name="addtocontact" 
            enctype="multipart/form-data"
            novalidate 
            ng-submit="addtocontact.$valid && add_to_contact();"  >

            <input type="hidden" name="seller_id" id="seller_id"  ng-model="user.seller_id">
           
            <div class="mobile-nu-block enquiry" ng-class="{'has-error':addtocontact.name.$touched && addtocontact.name.$invalid}">
                <input type="text" 
                       name="name" 
                       class="beginningSpace_restrict" 
                       ng-model="user.name" 
                       placeholder="Enter Your Name"
                       ng-required="true"
                />
                <div class="error-new-block" ng-messages="addtocontact.name.$error" ng-if="addtocontact.$submitted || addtocontact.name.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>
                <span class="highlight"></span>
                <label>Name</label>
            </div>

            <div class="mobile-nu-block enquiry" ng-class="{'has-error':addtocontact.email.$touched && addtocontact.email.$invalid}">
                <input type="text" 
                       name="email" 
                       class="beginningSpace_restrict" 
                       ng-model="user.email" 
                       placeholder="Enter Your Email"
                       ng-required="true"
                />
                <div class="error-new-block" ng-messages="addtocontact.email.$error" ng-if="addtocontact.$submitted || addtocontact.email.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>
                <span class="highlight"></span>
                <label>Email</label>
            </div>

            <div class="mobile-nu-block enquiry" ng-class="{'has-error':addtocontact.mobile_number.$touched && addtocontact.mobile_number.$invalid}">
                <input type="text" 
                       name="mobile_number" 
                       class="beginningSpace_restrict" 
                       ng-model="user.mobile_number" 
                       placeholder="Enter Your Contact No."
                       ng-required="true"
                />
                <div class="error-new-block" ng-messages="addtocontact.mobile_number.$error" ng-if="addtocontact.$submitted || addtocontact.mobile_number.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>
                <span class="highlight"></span>
                <label>Contact No.</label>
            </div>

            <div class="mobile-nu-block enquiry" ng-class="{'has-error':addtocontact.country.$touched && addtocontact.country.$invalid}">
                <input type="text" 
                           name="country" 
                           class="beginningSpace_restrict" 
                           ng-model="user.country" 
                           placeholder="Enter Your Country"
                           ng-required="true"
                           />
                <div class="error-new-block" ng-messages="addtocontact.country.$error" ng-if="addtocontact.$submitted || addtocontact.country.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>

                <span class="highlight"></span>
                <label>Country</label>
            </div>

            </div>
            <div class="modal-footer">
                <div class="btn-password changes enq">
                    <button class="change-btn-pass " type="submit">Send</button>
                </div>
            </div>
          </form>
       </div>
    </div>

</div>
</div>
</div>
<?php } ?>



<script type="text/javascript">

$(document).ready(function(){
 
 $('.add_seller_in_contact').click(function(){

    var seller_id      = $(this).data('sellerid');

    ajaxindicatorstart();
    jQuery.ajax({
    url: site_url+'buyer/add_seller_to_my_contact',
    type:"post",
    dataType : 'json',
    data:{

          seller_id      :seller_id,
    },
    success:function(response){
      ajaxindicatorstop();


  
      if(response.contact_status == "success") {
        jQuery("#status_msg").html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.contact_msg+' </div>');
      } else if(response.contact_status == "error") {
        jQuery("#status_msg").html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.contact_msg+' </div>');
      }

    }
    });

 });

});
</script>


<!--rating -->
<script type="text/javascript">
jQuery(function() {
    jQuery('.starbox').each(function() {
        var starbox = jQuery(this);
        starbox.starbox({
            average: starbox.attr('data-start-value'),
            changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
            ghosting: starbox.hasClass('ghosting'),
            autoUpdateAverage: starbox.hasClass('autoupdate'),
            buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
            stars: starbox.attr('data-star-count') || 5
        }).bind('starbox-value-changed', function(event, value) {
            if(starbox.hasClass('random')) {
                var val = Math.random();
                starbox.next().text(val*5);
                return val;
            } else {
                starbox.next().text(value*5);
            }
        }).bind('starbox-value-moved', function(event, value) {
            starbox.next().text(value*5);
            $('#review_count').val(value*5);
            $('#rivew-count').html(value*5);
            
        });
    });
});
//--><!]]>
</script>
<!--end rating -->


<script type="text/javascript">
    $(document).ready(function() { 
      $('.close').on('click',function() {
        setTimeout(function(){
            location.reload();
        })
      });
    });
</script>

<?php
if($this->session->userdata('user_type')=='Buyer')
{
?>
<script type="text/javascript">
jQuery("#btn_ratings").click(function(){

    var txt_comment    = $("#txt_comment").val();
    var seller_id      = $("#seller_id").val();
    var review_count      = $("#review_count").val();

    if(review_count == 0 )
    {
    jQuery("#show_error").html("<div class='alert alert-danger'>Please select your ratings.</div>").show().fadeOut(3000);
    return false;
    }
    if(txt_comment == "" )
    {
    jQuery("#show_error").html("<div class='alert alert-danger'>Please enter your reviews.</div>").show().fadeOut(3000);
    return false;
    }
    ajaxindicatorstart();
    jQuery.ajax({
    url: site_url+'buyer/rating_for_seller',
    type:"post",
    data:{

          txt_comment    :txt_comment,
          seller_id      :seller_id,
          review_cnt     :review_count

    },
    success:function(data){
      ajaxindicatorstop();
      jQuery("#show_success").html("<div class='alert alert-success'>Rating against this seller successfully post .</div>").show().fadeOut(14000);
      
        setTimeout(function(){
            window.location.href=site_url+'seller/profile/'+seller_id+'/'+'#reviews-maib-block';
        }, 1000);
    }
    });
return false;
});
</script>
<?php } ?>