<?php if(isset($get_profile_data) && sizeof($get_profile_data) > 0) { ?>
  <div ng-controller="quickCallctrl">
    <div class="page-head-block">
        <div class="page-head-overlay"></div>
        <div class="container">
            
            <div class="profile-pic-main">
                <div class="pic-profile-block">
                    
                    <?php if(!empty($get_profile_data[0]['user_image']) && file_exists('images/seller_image/'.$get_profile_data[0]['user_image'])){?>
                    <img style="height:100%;" src="<?php echo base_url().'images/seller_image/'.$get_profile_data[0]['user_image']; ?>" alt="" />
                    <?php } else { ?>
                    <img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
                    <?php } ?> 

                </div>
                <div class="profile-discription">
                    <div class="name-head-user">
                        <?php if(!empty($get_profile_data[0]['name'])) {
                            echo $get_profile_data[0]['name'];
                          } else { 
                            echo "Not Available"; 
                          } 
                        ?> 
                    </div>
                    <div class="address-user">
                        <?php if(!empty($get_profile_data[0]['city'])) {
                            echo $get_profile_data[0]['city'];
                          } else { 
                            echo "Not Available"; 
                          } 
                        ?> ,
                        <?php if(!empty($get_profile_data[0]['country'])) {
                            echo $get_profile_data[0]['country'];
                          } else { 
                            echo "Not Available"; 
                          } 
                        ?> 
                    </div>
                    <div class="other-info-user">
                        <?php $this->load->view('seller/seller-front-search/search-header'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-head-name seller-tab-block" set-class-when-at-top="fix-to-top" extra-offset-adjustment="150">
        <div class="container">
            <div class="name-container-dash">
               <ul>
                   <li><a class="menu_class" href="javascript:void(0);" lang="products"><h4>All Products</h4></a></li>
               </ul>                
            </div>
        </div>
    </div>
    <div class="middel-container">
        <div class="inner-content-block">
            <div class="container">
                <div class="row">                    
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div id="products" class="products-main-block">
                            <div class="descrip-head-block">
                                <!-- Products -->
                            </div>
                            <div class="vagitable-main">
                                <div class="vagita-head-block">
                                    All Products
                                </div>
                                <div class="green-border-block"></div>
                            </div>
                        </div>
                        

                        <?php if(isset($getProducts) && sizeof($getProducts) > 0) { ?>
                            <div class="row col-block-mobile">
                            <?php  foreach ($getProducts as $product) {  ?>

                               
                                   
                                        <div class="col-sm-6 col-md-3 col-lg-3">
                                        <div class="product-block-main">
                                        <span class="img-block-product">
                                        <?php if(!empty($product['req_image']) && file_exists('images/seller_upload_product_image/'.$product['req_image'])){?>
                                        <img style="height:162px;width:260px;" src="<?php echo base_url().'images/seller_upload_product_image/'.$product['req_image']; ?>" alt="list_img_1" class="" />
                                        <?php } else { ?>
                                        <img style="height:162px;width:260px;" src="<?php echo base_url().'images/seller_upload_product_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                                        <?php } ?> 
                                        </span>
                                        <span class="product-content-block">
                                        <div class="icon-block-view">
                                        <ul>
                                        <!--<li><a href="#"><i class="fa fa-heart"></i></a></li>-->
                                        <!-- <li><a style="padding-top: 7px;" href="#"><i class="fa fa-envelope"></i></a></li> -->
                                        <li><a style="padding-top: 7px;" href="<?php echo base_url().'seller/product_details/'.$product['id'].'/'.$product['seller_id'] ?>"><i class="fa fa-eye"></i></a></li>
                                        </ul>
                                        </div>
                                        <span class="product-name-block">
                                        <?php if(!empty($product['title'])) { echo ucfirst($product['title']); } else { echo "Not Available" ;} ?>
                                        </span>
                                        <?php /* 
                                        <span class="product-satrs-block">
                                        <i class="fa fa-star star-acti"></i> <i class="fa fa-star star-acti"></i> <i class="fa fa-star star-acti"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> 
                                        </span>
                                        <span class="reviews-block-txt">
                                        4 review
                                        </span>
                                        </span>
                                        */ ?>
                                        <div class="item-content-block">
                                        <?php if(!empty($product['description'])) { echo substr($product['description'], 0 , 52).'...'; } else { echo "<span class='label label-important'><i class='fa fa-ban' aria-hidden='true'></i></span> Product description is not available."; } ?>
                                        </div>
                                        </div>
                                        </div>

                                
                            <?php } ?>
                            </div>          

                       
                       
                        <?php echo $this->pagination->create_links(); ?>
                       
                        


                        <?php } else { ?>
                        <div class="row col-block-mobile">    
                        <div class="col-sm-12 col-md-12 col-lg-12">
                        <?php $this->load->view('no-data-found'); ?>
                        </div>
                        </div>
                        <?php } ?>
                   
                </div>
            </div>
        </div>        
    </div>
 </div>   
    
<?php } else {

  ?><div style="margin:20px;"><?php 
  $this->load->view('no-data-found');
  ?></div><?php

} ?>