<?php if(isset($get_profile_data) && sizeof($get_profile_data) > 0) { ?>
<div ng-controller="quickCallctrl">
    <div class="page-head-block">
        <div class="page-head-overlay"></div>
        <div class="container">
            <div class="profile-pic-main">
                <div class="pic-profile-block">
                    
                    <?php if(!empty($get_profile_data[0]['user_image']) && file_exists('images/seller_image/'.$get_profile_data[0]['user_image'])){?>
                    <img style="height:100%;" src="<?php echo base_url().'images/seller_image/'.$get_profile_data[0]['user_image']; ?>" alt="" />
                    <?php } else { ?>
                    <img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
                    <?php } ?> 

                </div>
                <div class="profile-discription">
                    <div class="name-head-user">
                        <?php if(!empty($get_profile_data[0]['name'])) {
                            echo $get_profile_data[0]['name'];
                          } else { 
                            echo "Not Available"; 
                          } 
                        ?> 
                    </div>
                    <div class="address-user">
                        <?php if(!empty($get_profile_data[0]['city'])) {
                            echo $get_profile_data[0]['city'];
                          } else { 
                            echo "Not Available"; 
                          } 
                        ?> ,
                        <?php if(!empty($get_profile_data[0]['country'])) {
                            echo $get_profile_data[0]['country'];
                          } else { 
                            echo "Not Available"; 
                          } 
                        ?> 
                    </div>
                    <div class="other-info-user">
                        <?php $this->load->view('seller/seller-front-search/search-header'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3><?php if(!empty($getProductDetail[0]['title'])) { echo ucfirst($getProductDetail[0]['title']); } else { echo "Not Available" ;} ?></h3>
        </div>
    </div>
</div>

<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-10 col-lg-10">
                <div class="back-btn-main-block">
                    <!-- <div class="bck-btn">
                        <a href="#"><i class="fa fa-angle-double-left"></i> Back</a>
                    </div> -->
                    <div class="replay-btn">
                        <a class="btn-replay-block" href="javascript:history.back()"><i class="fa fa-reply"></i> back</a>
                        <!-- <a class="btn-delete-block" href="#"><i class="fa fa-trash-o"></i> Delete</a> -->
                    </div>
                    <div class="clr"></div>
                </div>
                <?php $this->load->view('status-msg'); ?>
                    
                <?php if(isset($getProductDetail) && sizeof($getProductDetail) > 0) {?>

                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <div class="details-img-block">
                            <?php if(!empty($getProductDetail[0]['req_image']) && file_exists('images/seller_upload_product_image/'.$getProductDetail[0]['req_image'])){?>
                            <img style="height:162px;width:260px;"  src="<?php echo base_url().'images/seller_upload_product_image/'.$getProductDetail[0]['req_image']; ?>" alt="list_img_1" class="" />
                            <?php } else { ?>
                            <img src="<?php echo base_url().'images/seller_upload_product_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                            <?php } ?> 
                        </div>
                     </div>   
                    <div class="account-info-block"> 
                        <div id="description" class="details-descrip">
                            <div class="descrip-head-block">
                                <?php if(!empty($getProductDetail[0]['title'])) { echo ucfirst($getProductDetail[0]['title']); } else { echo "Not Available" ;} ?>
                            </div>
                            <div class="list_up"><span><i class="fa fa-folder-open"></i>
                                </span>
                                    <?php if(!empty($getProductDetail[0]['category_name'])) { echo substr($getProductDetail[0]['category_name'], 0 , 50); } else { echo "Not Available"; } ?> ->
                                    <?php if(!empty($getProductDetail[0]['subcategory_name'])) { echo substr($getProductDetail[0]['subcategory_name'], 0 , 50); } else { echo "Not Available"; } ?>
                            </div>
                            <div class="list_add"><span><i class="fa fa-map-marker"></i>
                                </span>

                                <?php if(!empty($getProductDetail[0]['location'])) { echo $getProductDetail[0]['location']; } else { echo "Not Available"; } ?> 
                            </div>
                            <div class="lsit_text_content">
                            <div class="list_up "><span><i class="fa fa-calendar" aria-hidden="true"></i> Posted Date :
                            </span>
                                 <?php echo date('F j, Y', strtotime($getProductDetail[0]['created_date']))." at ".date("g:i a", strtotime($getProductDetail[0]['created_date'])); ?> 
                            </div>
                            </div>
                            <div class="lsit_text_content">
                            <div class="list_up up-list-price"><span> <?php echo CURRENCY; ?>
                                </span>
                                     <?php if(!empty($getProductDetail[0]['price'])) { echo $getProductDetail[0]['price']; } else { echo "Not Available"; } ?> 
                            </div>
                            </div>
                            <div class="descrip-content-block">
                                <?php if(!empty($getProductDetail[0]['description'])) { echo $getProductDetail[0]['description']; } else { echo "Not Available"; } ?>
                            </div>
                        </div>
                    </div>

                <?php
                } else {
                    $this->load->view('no-data-found');
                }?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php } else {

  ?><div style="margin:20px;"><?php 
  $this->load->view('no-data-found');
  ?></div><?php

} ?>