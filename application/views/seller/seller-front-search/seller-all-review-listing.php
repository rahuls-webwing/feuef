<?php if(isset($get_profile_data) && sizeof($get_profile_data) > 0) { ?>
  <div ng-controller="quickCallctrl">
    <div class="page-head-block">
        <div class="page-head-overlay"></div>
        <div class="container">
            <div class="profile-pic-main">
                <div class="pic-profile-block">
                    
                    <?php if(!empty($get_profile_data[0]['user_image']) && file_exists('images/seller_image/'.$get_profile_data[0]['user_image'])){?>
                    <img style="height:100%;" src="<?php echo base_url().'images/seller_image/'.$get_profile_data[0]['user_image']; ?>" alt="" />
                    <?php } else { ?>
                    <img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
                    <?php } ?> 

                </div>
                <div class="profile-discription">
                    <div class="name-head-user">
                        <?php if(!empty($get_profile_data[0]['name'])) {
                            echo $get_profile_data[0]['name'];
                          } else { 
                            echo "Not Available"; 
                          } 
                        ?> 
                    </div>
                    <div class="address-user">
                        <?php if(!empty($get_profile_data[0]['city'])) {
                            echo $get_profile_data[0]['city'];
                          } else { 
                            echo "Not Available"; 
                          } 
                        ?> ,
                        <?php if(!empty($get_profile_data[0]['country'])) {
                            echo $get_profile_data[0]['country'];
                          } else { 
                            echo "Not Available"; 
                          } 
                        ?> 
                    </div>
                    <div class="other-info-user">
                        <?php $this->load->view('seller/seller-front-search/search-header'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-head-name seller-tab-block" set-class-when-at-top="fix-to-top" extra-offset-adjustment="150">
        <div class="container">
            <div class="name-container-dash">
               <ul>
                   <li><a class="menu_class" href="javascript:void(0);" lang="products"><h4>All Reviews </h4></a></li>
               </ul>                
            </div>
        </div>
    </div>
    <div class="middel-container">
        <div class="inner-content-block">
            <div class="container">
                <div class="row">                    
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div id="products" class="products-main-block">
                            <div class="descrip-head-block">
                                <!-- Products -->
                            </div>
                            <div class="vagitable-main">
                                <div class="vagita-head-block">
                                    All Reviews (<?php echo count($getreview) ?>)
                                </div>
                                <div class="green-border-block"></div>
                            </div>
                        </div>
                        <?php if(isset($getreview) && sizeof($getreview) > 0) { ?>
                           <div class="reviews-maib-block"  id="reviews-maib-block">
                            <?php  foreach ($getreview as  $review) { 
                                $get_buyer_details = $this->master_model->getRecords('tbl_user_master',array('id'=>$review['buyer_id']));
                                 
                                ?>
                                    <div class="revews-block-seller">
                                        <div class="profile-pic-review-seller">
                                           <?php if(!empty($get_buyer_details[0]['user_image']) && file_exists('images/buyer_image/'.$get_buyer_details[0]['user_image'])){?>
                                            <img src="<?php echo base_url().'images/buyer_image/'.$get_buyer_details[0]['user_image']; ?>" alt="" />
                                            <?php } else { ?>
                                            <img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
                                            <?php } ?> 
                                        </div>
                                        <div class="arrow_box">
                                            <div class="name-review-block">
                                                <div class="review-block-name">
                                                    <?php if(isset($get_buyer_details[0]['name'])) { echo $get_buyer_details[0]['name'];} else { echo "Not Available" ;} ?>
                                                </div>
                                                <div class="review-time-date">
                                                    on <?php echo date('F j, Y', strtotime($review['commented_at']))." at ".date("g:i a", strtotime($review['commented_at'])); ?> 
                                                </div>
                                                <div class="clr"></div>
                                            </div>
                                            <?php 
                                                $ratings         = 0;
                                                $arv_rating      = 0;       
                                                $arv_rating      = $review['ratings'];  // find rating avarage
                                                ?> 
                                                <span class="product-satrs-block">
                                                <?php
                                                    $finalrate = $chekRating = mb_substr($arv_rating,  0, 4);
                                                    for($i=1;$i<=5;$i++) {
                                                    $selected = "";

                                                    $point = $i - $finalrate;

                                                    if(!empty($finalrate) && $i<=$finalrate) {
                                                      ?>
                                                       <img alt="img" src="<?php echo base_url();?>images/review/star-full.png" style="margin-top:11px;"/>
                                                       <?php
                                                     } else if($point == '0.5' || $point < '0.5' || $point < '1') {
                                                      ?>
                                                      <img alt="img" src="<?php echo base_url();?>images/review/star-half.png" style="margin-top:11px;"/>
                                                       <?php 
                                                    } else {
                                                      ?>
                                                      <img alt="img" src="<?php echo base_url();?>images/review/star-blank.png" style="margin-top:11px;"/>
                                                      <?php 
                                                    }
                                                } 
                                            ?> 
                                            </span>
                                            <div class="reviews-msg-block">
                                                <?php if(isset($review['comment'])) { echo $review['comment'];} else { echo "Not Available" ;} ?>
                                            </div>                                    
                                        </div>
                                    </div>
                               
                              <?php } ?>
                             </div>
                         <?php echo $this->pagination->create_links(); ?>     
                        <?php } else { ?>
                        <div class="reviews-maib-block">
                        <?php $this->load->view('no-data-found'); ?>
                        </div>
                        <?php } ?>
                </div>
            </div>
        </div>        
    </div>
 </div>   
    
<?php } else {

  ?><div style="margin:20px;"><?php 
  $this->load->view('no-data-found');
  ?></div><?php

} ?>