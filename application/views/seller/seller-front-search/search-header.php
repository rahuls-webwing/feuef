<?php 
$get_seller_reviews = 0;
$ratings         = 0;
$arv_rating      = 0;
$compl_per       = 0;
$completion_per  = 0;


$arv_rating      = 0;       
$compl_per       = 0;           
$completion_per  = 0;

$this->db->where('tbl_seller_rating.seller_id', $get_profile_data[0]['id']);
$this->db->where('status !=' , 'Delete');
$this->db->where('tbl_seller_rating.review_for', 'seller');
$get_seller_reviews=$this->master_model->getRecords('tbl_seller_rating');
if(empty($get_seller_reviews))
{
}
else
{
foreach($get_seller_reviews as $field => $value)
{
$ratings       +=  $value['ratings'];
}
$arv_rating      = $ratings / count($get_seller_reviews);  // find rating avarage
$compl_per       = $arv_rating  * 100 / 5 ;             // find completion persentage by rating
$completion_per  = mb_substr( $compl_per, 0, 4 ) ;      // completion persentage by rating
}  
?> 
<div class="stars-blok-seller">
    <?php
        $finalrate = $chekRating = mb_substr($arv_rating,  0, 4);
        for($i=1;$i<=5;$i++) {
        $selected = "";

        $point = $i - $finalrate;

        if(!empty($finalrate) && $i<=$finalrate) {
          ?>
           <img alt="img" src="<?php echo base_url();?>images/review/star-full-big.png" style="margin-top:11px; height:20px;"/>
           <?php
         } else if($point == '0.5' || $point < '0.5' || $point < '1') {
          ?>
          <img alt="img" src="<?php echo base_url();?>images/review/star-half-big.png" style="margin-top:11px; height:20px;"/>
           <?php 
        } else {
          ?>
          <img alt="img" src="<?php echo base_url();?>images/review/star-blank-big.png" style="margin-top:11px; height:20px;"/>
          <?php 
        }
       } 
    ?> 
    <?php if(mb_substr( $arv_rating, 0, 4 ) >= 4){
    ?><span class="" style="color:#16a085;"><?php
    }else if(mb_substr( $arv_rating, 0, 4 ) >= 3){
    ?><span class="" style="color:#1c9aea;"><?php
    }else{
    ?><span class="" style="color:red;"><?php    
    }?>
    (Avg <?php echo mb_substr($arv_rating, 0, 4 ); ?>)</span>
</div>