<?php $this->load->view('seller/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3><?php if(!empty($getOfferDetail[0]['title'])) { echo ucfirst($getOfferDetail[0]['title']); } else { echo "Not Available" ;} ?></h3>
        </div>
    </div>
</div>
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <?php $this->load->view('seller/left-bar'); ?>
                <div class="col-sm-9 col-md-9 col-lg-9">
                <div class="back-btn-main-block">
                    <!-- <div class="bck-btn">
                        <a href="#"><i class="fa fa-angle-double-left"></i> Back</a>
                    </div> -->
                    <div class="replay-btn">
                        <a class="btn-replay-block" href="javascript:history.back()"><i class="fa fa-reply"></i> back</a>
                        
                        <?php if(!empty($getOfferDetail[0]['order_from']) && file_exists('uploads/invoice/'.$getOfferDetail[0]['order_from'])){?>
                        <a class="btn-delete-block" href="<?php echo base_url().'uploads/invoice/'.$getOfferDetail[0]['order_from'] ?>" download><i class="fa fa-cloud-download" aria-hidden="true"></i> Order Form</a>
                        <?php } ?>

                    </div>
                    <div class="clr"></div>
                </div>
                <?php $this->load->view('status-msg'); ?>
                    
                <?php if(isset($getOfferDetail) && sizeof($getOfferDetail) > 0) {?>

                      <div class="col-sm-4 col-md-4 col-lg-4">
                        <div class="details-img-block">
                            <?php if(!empty($getOfferDetail[0]['req_image']) && file_exists('images/seller_post_offer_image/'.$getOfferDetail[0]['req_image'])){?>
                            <img style="height:162px;width:260px;"  src="<?php echo base_url().'images/seller_post_offer_image/'.$getOfferDetail[0]['req_image']; ?>" alt="list_img_1" class="img-responsive list_view_img_1" />
                            <?php } else { ?>
                            <img src="<?php echo base_url().'images/seller_post_offer_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                            <?php } ?> 
                        </div>
                       </div>  
                       <div class="account-info-block">
                        <div id="description" class="details-descrip">
                            <div class="descrip-head-block">
                                <?php if(!empty($getOfferDetail[0]['title'])) { echo ucfirst($getOfferDetail[0]['title']); } else { echo "Not Available" ;} ?>
                            </div>
                            <div class="list_up"><span><i class="fa fa-folder-open"></i>
                                </span>
                                    <?php if(!empty($getOfferDetail[0]['category_name'])) { echo substr($getOfferDetail[0]['category_name'], 0 , 50); } else { echo "Not Available"; } ?> ->
                                    <?php if(!empty($getOfferDetail[0]['subcategory_name'])) { echo substr($getOfferDetail[0]['subcategory_name'], 0 , 50); } else { echo "Not Available"; } ?>
                            </div>
                            <div class="list_add"><span><i class="fa fa-map-marker"></i>
                                </span>

                                <?php if(!empty($getOfferDetail[0]['location'])) { echo $getOfferDetail[0]['location']; } else { echo "Not Available"; } ?> 
                            </div>
                            <div class="lsit_text_content">
                            <div class="list_up "><span><i class="fa fa-calendar" aria-hidden="true"></i> Posted Date :
                            </span>
                                 <?php echo date('F j, Y', strtotime($getOfferDetail[0]['created_date']))." at ".date("g:i a", strtotime($getOfferDetail[0]['created_date'])); ?> 
                            </div>
                            </div>
                            <!-- <div class="lsit_text_content">
                                <div class="list_up"><span> Qty :
                                    </span>
                                         <?php if(!empty($getOfferDetail[0]['qty'])) { echo $getOfferDetail[0]['qty']; } else { echo "Not Available"; } ?> 
                                </div>
                                </div> -->
                            <div class="lsit_text_content">
                                <div class="list_up"><span> Price :
                                    </span>
                                         <?php if(!empty($getOfferDetail[0]['price'])) { echo CURRENCY.$getOfferDetail[0]['price']; } else { echo "Not Available"; } ?> 
                                </div>
                                </div>    
                            <div class="descrip-content-block">
                                <?php if(!empty($getOfferDetail[0]['description'])) { echo $getOfferDetail[0]['description']; } else { echo "Not Available"; } ?>
                            </div>
                        </div>
                    </div>

                <?php
                } else {
                    $this->load->view('no-data-found');
                }?>

                <!-- Offered Buyers -->
                
                <?php if(isset($offered_buyers) && sizeof($offered_buyers) > 0 && isset($getOfferDetail) && sizeof($getOfferDetail) > 0) {
                  $datetime2      = new DateTime(date("Y-m-d H:m:s"));
                  $endTimeStamp   = strtotime(date("Y-m-d H:m:s"));
                  ?>
                    
                    <?php foreach ($offered_buyers as $offerd_buyer) {


                    
                    
                    ?>
                      <?php
                      /* for hours and time count */    
                      $datetime1 = new DateTime($offerd_buyer['offer_created_date']);
                      
                      $interval = $datetime1->diff($datetime2);
                      if($interval->format('%h')==0)
                      {
                        $hours="";
                      }
                      else
                      {
                        $hours=$interval->format('%h')." Hours " ;
                      }
                      if($interval->format('%i')==0)
                      {
                        $Minutes="";
                      }
                      else
                      {
                        $Minutes=$interval->format('%i')." Minutes";
                      }
                      if($Minutes == '' && $hours == '')
                      {
                        $aa = $interval->format('%s');
                        $Minutes ="$aa Second";
                      }
                      /* end for hours and time count */   
                      /* for day count */    
                      $startTimeStamp = strtotime($offerd_buyer['offer_created_date']);
                      $timeDiff       = abs($endTimeStamp - $startTimeStamp);
                      $numberDays     = $timeDiff/86400;  // 86400 seconds in one day
                      // and you might want to convert to integer
                      $numberDays = intval($numberDays);
                      if($numberDays==0)
                      {
                          $numberDays="";
                      }
                      else
                      {
                          ($numberDays>1)  ? $day='Day' : $day='Days';
                          $numberDays=intval($numberDays).' '.$day;
                      }
                  ?>
                       <div class="search-grey-bx offer_div" >
                        <div class="row">
                            <div class="col-sm-12 col-md-8 col-lg-10   br-right">
                                <div class="going-profile-detail">
                                    <div class="going-pro">
                                        <?php if(!empty($offerd_buyer['user_image']) && file_exists('images/buyer_image/'.$offerd_buyer['user_image'])){?>
                                        <img src="<?php echo base_url().'images/buyer_image/'.$offerd_buyer['user_image']; ?>" alt="" />
                                        <?php } else { ?>
                                        <img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
                                        <?php } ?> 
                                    </div>
                                    <div class="going-pro-content">
                                        <div style="display:inline-block;" class="profile-name1">
                                            
                                            <?php if(!empty($offerd_buyer['name'])) { ?>

                                             <?php /*    
                                             <a href="<?php echo base_url().'buyer/profile/'.$offerd_buyer['offered_buyer_id']; ?>">
                                             */ ?>
                                             <?php echo  $offerd_buyer['name']; ?>
                                             <?php /*
                                             </a>
                                             */ ?>
                                            <?php  
                                            } else { 
                                            echo "Not Available"; 
                                            } 
                                            ?> 
 
                                        </div>
                                        <div class="premium-text">
                                           <span>Offered in:</span> <?php  echo $numberDays.' '. $hours.' '. $Minutes; ?> ago
                                        </div>
                                        
                                       <div class="sub-project-dec"> 
                                          <i class="fa fa-envelope-o" aria-hidden="true"></i> Buyer Email :
                                             <?php if(!empty($offerd_buyer['email'])) { 
                                              ?><a href="mailto:<?php echo $offerd_buyer['email']; ?>?Subject=Inquiy" target="_top"><?php echo $offerd_buyer['email']; ?></a><?php
                                             } else { 
                                              echo "Not Available"; } ?>
                                        </div>
                                        <div class="sub-project-dec"> <i class="fa fa-calendar" aria-hidden="true"></i> Offered Date :
                                             
                                             <?php echo date('F j, Y', strtotime($offerd_buyer['offer_created_date']))." at ".date("g:i a", strtotime($offerd_buyer['offer_created_date'])); ?> 
                                        </div>
                                        <div style="height:auto;" class="more-project-dec">
                                            <div class="content-block-container">
                                                <?php if(!empty($offerd_buyer['offer_description'])) { echo $offerd_buyer['offer_description']; } else { echo "Not Available"; } ?>
                                            </div>
                                            <div class="clr"></div>
                                            <div class="palm-text"><span><img alt="location icon" src="<?php echo base_url(); ?>front-asset/images/post-location.png"> </span> 
                                               
                                                <?php if(!empty($offerd_buyer['city'])) {
                                                echo  $offerd_buyer['city']; 
                                                } else { 
                                                echo "Not Available"; 
                                                } ?> 

                                                , 

                                                <?php if(!empty($offerd_buyer['country'])) {
                                                echo  $offerd_buyer['country']; 
                                                } else { 
                                                echo "Not Available"; 
                                                } ?>

                                            </div>
                                            <div class="clr"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="col-sm-12 col-md-4 col-lg-2">
                                    <div class="rating-profile">
                                        <div class="projrct-prce1"><span><?php echo CURRENCY; ?><?php if(!empty($offerd_buyer['offered_price'])) { echo $offerd_buyer['offered_price']; } else { echo "Not Available"; } ?> </div>
                                        <div class="" style="">
                                            <!-- <button class="msg_btn" type="button" onclick="javascript:jqcc.cometchat.chatWith('<?php echo $offerd_buyer['offered_buyer_id']; ?>');"> Chat <i class="fa fa-envelope-o"></i></button>  -->                                                 
                                            &nbsp;
                                            <button class="msg_btn" type="button"> Accepted</button>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    <?php } ?>   

                <?php
                } else {
                    $this->load->view('no-data-found');
                }?>    
                <!-- end Offered buyers -->
  
                </div>
                <!--pagigation start here-->
                <div style="margin-top:-12px;">
                  <?php echo $this->pagination->create_links(); ?>
                </div>
                <!--pagigation end here-->
            </div>
        </div>
    </div>
</div>
