<style type="text/css">
    .label-important, .badge-important {
        background-color: #fe1010;
    }
    
</style>
<?php $this->load->view('seller/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3>Post Offer (Live Market)</h3>
        </div>
    </div>
</div>
 <?php
  $this->db->where('pricing_name'  , 'Offer post');
  $this->db->where('membership_id' , 4);
  $get_pay_amt = $this->master_model->getRecords('tbl_pricing_master');

  if(isset($get_pay_amt[0]['membership_price'])){ $get_pay_amt[0]['membership_price']= $get_pay_amt[0]['membership_price'];} else{ $get_pay_amt[0]['membership_price'] = 0; }
  ?>
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
            <div class="row">
                <?php $this->load->view('seller/left-bar'); ?>

                <div class="col-sm-9 col-md-9 col-lg-9">

                     <div class="img-err"  style="padding-bottom: 10px;">
                        <div style="color:#007aca; margin-top: 10px;" class="" ><b><span class="label label-important">NOTE!</span> For posting every offer need to pay <?php echo $get_pay_amt[0]['membership_price'].' '.currencyCode; ?> .</b></div>
                     </div>

                    <div class="account-info-block">
                        <div class="row">                            
                          <?php $this->load->view('status-msg'); ?>   
                         <div>    
                         <form id="frm_payment" name="frm_payment" enctype="multipart/form-data" method="post" action="<?php echo base_url();?>seller/post_offer/"> 
                                

                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Offer Title <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block">
                                            <input type="text" 
                                                   name="title"
                                                   id="title" 
                                                   class="beginningSpace_restrict " 
                                                   placeholder="Enter Title"/>
                                        </div>
                                        <p id="offer_title" class="error"></p>
                                    </div>
                                </div>
                                
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Offer category <span class="required-field"><sup>*</sup></span></div>

                                        <div class="select-bock-container">
                                            <select name="subcategory_id" id="subcategory_id">
                                                      <option value="">Select offer category</option>
                                                      <?php foreach ($getCat as $cate_value) {
                                                        ?>
                                                            <optgroup label="<?php echo '- '.$cate_value['category_name']; ?>" >
                                                            <?php foreach ($getSubcat as $value) {
                                                            ?>
                                                                    <?php if($value['category_id'] ==  $cate_value['category_id']) { ?>

                                                                      <option value="<?php echo $value['subcategory_id']; ?>"><?php echo $value['subcategory_name']; ?></option>

                                                                   <?php  } ?>
                                                             <?php
                                                            } ?>     
                                                           </optgroup>
                                                        <?php } ?>
                                                </optgroup>
                                            </select>
                                            <p id="err_subcategory_id" class="error"></p>
                                        </div>

                                    </div>
                                </div>


                                <!-- <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Qty <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block">
                                            <input type="text" 
                                                   name="qty" 
                                                   id="qty" 
                                                   class="beginningSpace_restrict" 
                                                   placeholder="Enter qty"/>
                                        </div>
                                        <p id="err_qty" class="error"></p>
                                    </div>
                                </div> -->

                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Price <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block">
                                            <input type="text" 
                                                   name="price" 
                                                   id="price" 
                                                   class="beginningSpace_restrict" 
                                                   placeholder="Enter price"/>
                                        </div>
                                        <p id="err_price" class="error"></p>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="email-box-2">
                                        <div class="email-title">Location <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block">
                                            <input type="text" 
                                                   name="location"
                                                   id="location" 
                                                   class="beginningSpace_restrict" 
                                                   placeholder="Enter Location"
                                                   />
                                        </div>
                                        <p id="err_location" class="error"></p>
                                    </div>
                                </div>  
                                <div class="geo-details">
                                <input type="hidden" name="latitude"  data-geo="lat"  id="latitude"  placeholder="latitude">
                                <input type="hidden" name="longitude" data-geo="lng" id="longitude"  placeholder="longitude">  
                                </div>


                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="email-box-2">
                                        <div class="email-title">Offer description <span class="required-field"><sup>*</sup></span></div>
                                        <div class="input-block">
                                            <textarea rows="" 
                                                      cols="" 
                                                      name="description" 
                                                      id="description" 
                                                      class="beginningSpace_restrict" 
                                                      placeholder="Enter Offer description"></textarea>
                                        </div>
                                        <p  id="desc_error" class="error"> </p>

                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-12 col-md-12 col-lg-12">                                
                                <div class="requirement-upload"> 
                                    <div class="email-title">Offer Image <span class="required-field"><sup>*</sup></span></div>                                   
                                    <div class="fileUpload">    
                                        <img src="<?php echo base_url(); ?>/front-asset/images/gallrey.png" id="img_profile" 
                                        styel="req_img" class="" alt="profile" />
                                        <input type="file" 
                                               class="upload allowOnlyImg"
                                               name="req_image" 
                                               id="req_image" 
                                               onChange="Upload()" 
                                               />
                                    </div>

                                    <div class="img-err">
                                        <div id="err_img" style="color:red;" class="" ></div>
                                    </div>
                                    <div class="img-err">
                                        <div style="color:#007aca; margin-top: 10px;" class="" ><b><span class="label label-important">NOTE!</span> Image allow greater than 162 x 260 (h X w) dimension only and also image is mandatory.</b></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="btn-password save">
                                        <button style="max-width: 130px;" class="change-btn-pass" id="post_offer" type="button">Post</button>
                                    </div>
                                </div>
                           </div>  


                          <!-- start payment  -->
                         
                          <div class="col-sm-12 col-md-12 col-lg-12">
                             <div class="img-err">
                                <div style="color:#007aca; margin-top: 10px;" class="" >&nbsp;</div>
                             </div>
                          </div>

                          <div id="start_payment" style="display:none;">
                            <div class="col-sm-3 col-md-3 col-lg-3 p-r">
                                <div class="side-menu">
                                    <ul>
                                            <a class="act common" id="monthly-free1" data-target="credit">
                                            <li class="active-pay" id="li_monthly-free1"><span><i class="fa fa-credit-card" aria-hidden="true"></i></span>Credit/Debit Card</li></a>
                                             <a class="common" id="monthly-free11" data-target="paypal">
                                             <li class="last-border" id="li_monthly-free11">
                                    <span><i class="fa fa-paypal" aria-hidden="true"></i></span>Pay Pal</li></a>
                                    </ul>
                                </div>
                            </div>


                            <div class="col-sm-9 col-md-9 col-lg-9 pp-l">
                                <div class="payment_card">
                                    <div class="payment-credit-card paymt-sec" id="credit">
                                        <div class="top_card_icon">
                                            <div class="card_title">Pay using Credit Card</div>
                                            <div class="card_icon_img">
                                                <a href="javascript:void(0);"><img alt="" src="<?php echo base_url();?>front-asset/images/pay_pal.png" /></a>
                                                <a href="javascript:void(0);"><img alt="" src="<?php echo base_url();?>front-asset/images/master_card.png" /></a>
                                                <a href="javascript:void(0);"><img alt="" src="<?php echo base_url();?>front-asset/images/visa.png" /></a>
                                            </div>
                                        </div>
                                        <!-- <form id="frm_payment" name="frm_payment" method="post" action="<?php echo base_url();?>payment/paynow/"> -->
                                        <div class="payment-form">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12 col-lg-10">
                                                    <div class="row">

                                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                                            <div class="user_box">
                                                                <div class="input-bx-b">
                                                                    <input type="text"
                                                                           class="tra-input" 
                                                                           id="card_type" 
                                                                           name="card_type" 
                                                                           placeholder="Credit/Debit Card" 
                                                                           readonly/>

                                                                    <div class="card_icon_img" id="card_type-img">
                                                                        <a href="javascript:void(0);" id="visa"><img alt="" src="<?php echo base_url();?>front-asset/images/pay-visa.png" /></a>
                                                                        <a href="javascript:void(0);" id="mastercard"><img alt="" src="<?php echo base_url();?>front-asset/images/pay-mstercard.png" /></a>
                                                                    </div>
                                                                </div>
                                                                <div  class="error_msg"  id="error_msg_card_type"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                                            <div class="user_box-imf">
                                                                <input type="text" 
                                                                       class="input-bx-f allowOnlySixteen" 
                                                                       id="cc_number" 
                                                                       name="cc_number" 
                                                                       placeholder="Card Number" />
                                                                <div class="img-odiv" ><img src="<?php echo base_url();?>front-asset/images/cart-nubs.png" alt="" /></div>
                                                                <div id="error_msg_cc_number" class="error_msg"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-lg-12">   <div class="user_box"><div class="label_text tnf">Expiry Date</div></div></div>
                                                        <div class="col-sm-12 col-md-6 col-lg-5">
                                                            <div class="user_box">
                                                                <input type="text" 
                                                                       class="input-bx-b exp" 
                                                                       id="cc_exp" 
                                                                       name="cc_exp" 
                                                                       placeholder="MM/YYYY" />
                                                                <div class="error_msg" id="error_msg_cc_exp"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                                            <div class="user_box">
                                                                <input class="input-bx-b input-card " 
                                                                       id="cc_cvc" 
                                                                       name="cc_cvc" 
                                                                       type="text" 
                                                                       placeholder=" CVV " /><span></span>
                                                                <div class=" error_msg" id="error_msg_cc_cvc"></div>
                                                            </div>
                                                        </div>
                                                        <div class="clr"></div>
                                                        <div class="col-lg-12">
                                                            <div class="btn-apymt">
                                                                <button class="change-btn-pass" id="btn_cc_pay" name="btn_cc_pay" type="submit">Pay <?php echo $get_pay_amt[0]['membership_price'].' '.currencyCode; ?></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="payment-paypal paymt-sec payment-form" id="paypal">
                                      <div class="row">
                                             <div class="col-lg-12">
                                            <div class="top_card_icon">
                                                <div class="card_title">Pay using Paypal</div>
                                                <div class="card_icon_img">
                                                    <a href="#"><img alt="" src="<?php echo base_url();?>front-asset/images/pay_pal.png" /></a>
                                                </div>
                                            </div>
                                            <div class="pay-tril"><span>Click to connect to paypal</span>(You won't be charged until the order is placed.) </div>
                                            <div class="clr"></div>
                                            <div class="pay-tril">By clicking Continue to PayPal, you authorise TaskersHub to charge your PayPal account for the full amount of all fees related to your buying activity on TaskersHub.</span></div>

                                            <div class="btn-apymt">
                                                <button class="change-btn-pass" type="submit" id="btn_pay" name="btn_pay">Pay <?php echo $get_pay_amt[0]['membership_price'].' '.currencyCode; ?></button>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <!-- </form> -->
                                    <div class="clr"></div>
                                </div>
                              </div>
                           </div>  
                           <!-- end  payment-->
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLKCmJEIrOrzdRklZHYer8q9qx2XLJ4Vs&sensor=false&libraries=places"></script>
<script src="<?php echo base_url(); ?>assets/js/geolocator/jquery.geocomplete.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var location = "";
      $("#location").geocomplete({
        details: ".geo-details",
        detailsAttribute: "data-geo",
        location:location,
        types:[]
    });
});
</script>

<script type="text/javascript">
function Upload() {
    //Get reference of FileUpload.
    var fileUpload = document.getElementById("req_image");
 
    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+()$");
    if (regex.test(fileUpload.value.toLowerCase())) {
 
        //Check whether HTML5 is supported.
        if (typeof (fileUpload.files) != "undefined") {
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
 
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;
                       
                //Validate the File Height and Width.
                image.onload = function () {
                    var height = this.height;
                    var width = this.width;
                    if (height < 162 && width < 260) {

                        $(this).val('');
                        sweetAlert("SORRY!!!, Image only allow greater than  162 x 260 (h X w) diamentions.");
                        $(".req_image").attr('src', site_url+'images/noimage200X150.png');
                        event.preventDefault(); 
                        return false;
                    }
                    else{
                       $('#img_profile').attr('src', e.target.result);
                       reader.readAsDataURL(input.files[0]);
                       return true;
                    }
                };
 
            }
        } else {
            sweetAlert("This browser does not support HTML5.");
            return false;
        }
    } else {
        sweetAlert("Please select a valid Image file.");
        return false;
    }
}


$(document).ready(function(){
 $('#req_image').click(function(){
    ajaxindicatorstart();
    setTimeout(function(){
      ajaxindicatorstop();
    },2000);
 });
}); 
</script>

<!-- Post Offer validations  -->
<script type="text/javascript">
  $(document).ready(function() {

    $('#title').click(function(){
       $('#offer_title').hide();
    });
    $('#subcategory_id').change(function(){
       $('#err_subcategory_id').hide();
    });
    /*$('#qty').click(function(){
       $('#err_qty').hide();
    });*/
    $('#price').click(function(){
       $('#err_price').hide();
    });
    $('#description').click(function(){
       $('#desc_error').hide();
    });
    $('#location').click(function(){
       $('#err_location').hide();
    });
    $('#req_image').change(function(){
       $('#err_img').hide();
    });


    $('#post_offer').click(function(){

      var offer_title         = $('#title').val();
      var offer_category      = $('#subcategory_id').val();
      /*var offer_qty         = $('#qty').val();*/
      var offer_price         = $('#offer_price').val();
      var offer_deascription  = $('#description').val();
      var offer_location      = $('#location').val();
      var offer_image         = $('#req_image').val();

      var flag= 1;

      $('#offer_title').hide();
      $('#err_subcategory_id').hide();
      $('#err_qty').hide();            
      $('#err_location').hide();
      $('#desc_error').hide();  
      $('#err_img').hide();

      if(offer_title==""){
        $('#offer_title').html('This field is required.').show();
        flag= 0;
      }
       if(offer_category==""){
        $('#err_subcategory_id').html('This field is required.').show();
        flag= 0;
      }
      /*if(offer_qty==""){
        $('#err_qty').html('This field is required.').show();
        flag= 0;
      }*/
      if(offer_price){
        $('#err_price').html('This field is required.').show();
        flag= 0;
      }
       if(offer_deascription==""){
        $('#desc_error').html('This field is required.').show();
        flag= 0;
      }
       if(offer_location==""){
        $('#err_location').html('This field is required.').show();
        flag= 0;
      }
       if(offer_image==""){
        $('#err_img').html('This field is required.').show();
        flag= 0;
      }
      if(flag == 1){
        $('#start_payment').show();
        $('#post_offer').hide();
        $('#cc_number').focus();
      }else {
        $('html, body').animate({scrollTop:$('#offer_title').position().top}, 'slow');
        return false;
      }
    });
  });
</script>

<!-- end Post Offer validations  -->


<!-- Payment Method Validations jQuery -->
<script type="application/javascript">
    //onclick toggle
    $(document).ready(function() {
        $('.container1-b').hide();
        $(".container1-b:first").addClass("act1").show();
        $('.regi_toggle .billing_cycle').click(function() {
            $('.billing_cycle').removeClass('act'); //remove the class from the button
            $(this).addClass('act'); //add the class to currently clicked button
            var target = "#" + $(this).data("target");
            $(".container1-b").not(target).hide();
            $(target).show();
            target.slideToggle();
        });

        $(".billing_cycle").click(function(event) {
            event.stopPropagation();
            /*alert("The span element was clicked.");*/
        });
        $('.paymt-sec').hide();
        $(".paymt-sec:first").addClass("ac1").show();
        $('.side-menu .common').click(function() {
          //alert();

           var curr_id = $(this).attr('id');
            $('.common').removeClass('act2'); //remove the class from the button
            $(this).addClass('act2'); //add the class to currently clicked button
            var target = "#" + $(this).data("target");
            console.log(target);
            $(".paymt-sec").not(target).hide();
            $(target).show();
            //target.slideToggle();
          if(curr_id=='monthly-free1')
          {
            $('#li_monthly-free1').addClass('active-pay');
            $('#li_monthly-free1').addClass('last-border');
            $('#li_monthly-free11').removeClass('active-pay');
            $('#li_monthly-free11').removeClass('last-border');
          }
         if(curr_id=='monthly-free11')
          {
            $('#li_monthly-free11').addClass('active-pay');
            $('#li_monthly-free11').addClass('last-border');
            $('#li_monthly-free1').removeClass('active-pay');
            $('#li_monthly-free1').removeClass('last-border');
          }
        });
       /* --------------T.A--------------------------------*/ 
        $('#visa').click(function(){
            $('#card_type').val('Visa');
        });
        $('#mastercard').click(function(){
            $('#card_type').val('Mastercard');
        });
        $('#amazon').click(function(){
            $('#card_type').val('Amazon');
        });
        $('#discover').click(function(){
            $('#card_type').val('Discover');
        });
        $('#btn_cc_pay').click(function(){


            var card_type      = $('#card_type').val();
            var Acc_no         = $('#cc_number').val();
            var Expirydate     = $('#cc_exp').val();
            var cvv            = $('#cc_cvc').val();
            var flag=1;
            if(card_type=="")
            {
              $('#error_msg_card_type').show();
              $('#error_msg_card_type').html('Please select card type');
              $('#card_type').focus();
               $('#card_type-img').on('click',function()
               {

                  $('#error_msg_card_type').hide();
               });

               flag=0;
            }
            if(Acc_no=="")
            {
              $('#error_msg_cc_number').show();
              $('#error_msg_cc_number').html('Please enter card no');
              $('#cc_number').focus();
               $('#cc_number').on('keyup',function()
               {
                 //$('#error_msg_cc_number').hide();
               });

               flag=0;
            }
            if(Expirydate=="")
            {
              $('#error_msg_cc_exp').show();
              $('#error_msg_cc_exp').html('Please enter expiry date');
              $('#cc_exp').focus();
               $('#cc_exp').on('click',function()
               {
                  $('#error_msg_cc_exp').hide();
               });

               flag=0;
            }
            if(Expirydate!="")
            {
                 var txtVal      = $('#cc_exp').val();
                 var filter      = new RegExp("(0[123456789]|10|11|12)([/])([1-2][0-9][0-9][0-9])");

                 var lastFive    = txtVal.substr(txtVal.length - 4);
                 var currentYear = new Date().getFullYear();

                 var firstTwo    = txtVal.substring(0, 2);
                 var currentDay = new Date();
     
                 var d = new Date();
                 var currentDay = d.getMonth(); 
     
             
                 if(!filter.test(txtVal) || lastFive <  currentYear || firstTwo < currentDay || lastFive ==  currentYear && firstTwo < currentDay)
                 {   
                    $('#error_msg_cc_exp').show();
                    $('#error_msg_cc_exp').html('Invalid Date!!!');
                    $('#cc_exp').focus();
                     $('#cc_exp').on('click',function()
                     {

                        $('#error_msg_cc_exp').hide();
                     });

                     flag=0;
                 }
            }
            if(cvv=="")
            {
              $('#error_msg_cc_cvc').show();
              $('#error_msg_cc_cvc').html('Please enter cvv');
              $('#cc_cvc').focus();
               $('#cc_cvc').on('keyup',function()
               {
                  //$('#error_msg_cc_cvc').hide();
               });

               flag=0;
            }
            /* credit card expiry date validation (mm/yyyy) --------------T.A*/
             $('#cc_exp').blur(function() {
                 var txtVal      = $('#cc_exp').val();
                 var filter      = new RegExp("(0[123456789]|10|11|12)([/])([1-2][0-9][0-9][0-9])");

                 var lastFive    = txtVal.substr(txtVal.length - 4);
                 var currentYear = new Date().getFullYear();

                 var firstTwo    = txtVal.substring(0, 2);
                 var currentDay = new Date();
     
                 var d = new Date();
                 var currentDay = d.getMonth(); 
     
                 flag=1;
                 if(!filter.test(txtVal) || lastFive <  currentYear || firstTwo < currentDay || lastFive ==  currentYear && firstTwo < currentDay)
                 {   
                    $('#error_msg_cc_exp').show();
                    $('#error_msg_cc_exp').html('Invalid Date!!!');
                    $('#cc_exp').focus();
                    $('#cc_exp').on('click',function()
                    {

                      $('#error_msg_cc_exp').hide();
                    });
                    flag=0;
                 }
             });
            /* end credit card expiry date ------------------------------T.A*/
            if(flag==1){
              return true;
            }
            else 
            {
              return false;
            }
        }); 
        /* character restriction */
        $("#cc_number").bind("keydown", function (event) {
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 17 || event.keyCode == 86 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 190 ||
                 // Allow: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
            } else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    $('#error_msg_cc_number').show();
                    $('#error_msg_cc_number').html('Please enter only numbers');
                    event.preventDefault();
                }
                else
                {
                    $('#error_msg_cc_number').hide();
                }
            }
        });
      $("#cc_cvc").bind("keydown", function (event) {
          if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 190 ||
                   // Allow: Ctrl+A
                  (event.keyCode == 65 && event.ctrlKey === true) ||

                   // Allow: home, end, left, right
                  (event.keyCode >= 35 && event.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
              } else {
                   // Ensure that it is a number and stop the keypress
                  if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                      $('#error_msg_cc_cvc').show();
                      $('#error_msg_cc_cvc').html('Please enter only numbers');
                      event.preventDefault();
                  }
                  else
                  {
                      $('#error_msg_cc_cvc').hide();
                  }
              }
      });
    /* end character restriction --------------T.A*/
}); // end document ready
</script>

