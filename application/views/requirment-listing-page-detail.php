<style type="text/css">
.pagination>li>a, .pagination>li>span {
    position: relative;
    float: left;
    top: -40px;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #ddd;
}
.label-important, .badge-important {
        background-color: #fe1010;
}
body {
    letter-spacing: 0px;
}
.mobile-block {
     text-align: left; 
}


</style>
<div class="page-head-block listing-search">
<div class="page-head-overlay"></div>
<div class="textbx-block-search">

    <button type="submit" style="width: 97%;" id="search_listing" class="search-btn-home">
       <?php if(isset($getRequirmentsDetail[0]['title'])) 
       { echo substr($getRequirmentsDetail[0]['title'], 0 , 20); if(strlen($getRequirmentsDetail[0]['title']) > 20){ echo "..."; } } else { echo 'Unknown'; } ?> details
   </button>
   

</div>
</div>
<!-- <div class="filter-block-listing">
<div class="container">
    <div class="row">
        <div class="col-sm-3 col-md-3 col-lg-3">
            <div class="location-input-bx">
                <input type="text" name="Location" placeholder="Location" />
            </div>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4">
            <div class="select-bx-listing">
                <select>
                    <option>Select Category</option>
                    <option>Category 1</option>
                    <option>Category 2</option>
                    <option>Category 3</option>
                </select>
            </div>
        </div>
        <div class="col-sm-3 col-md-3 col-lg-3">
            <div class="select-bx-listing">
                <select>
                    <option>Select Category</option>
                    <option>Category 1</option>
                    <option>Category 2</option>
                    <option>Category 3</option>
                </select>
            </div>
        </div>
        <div class="col-sm-2 col-md-2 col-lg-2">
            <div class="textbx-block-search filter-search">
                <button type="button">
                    <span class="icon-btn"><i class="fa fa-paper-plane"></i></span>
                    <span class="btn-txt">
                        <span><img src="<?php echo base_url() ?>front-asset/images/filter-search-icon.png" alt="" /></span> <span>Search</span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</div>
</div> -->
<div class="middel-container">
<div class="inner-content-block">
    <div class="container">
        <div class="row">
             <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="block-filter-container">
                    <div class="sort-by-block">
                        <span></span>
                        <!-- <span class="select-block-newest">
                            <select>
                                <option>Newest</option>
                                <option>Newest1</option>
                                <option>Newest2</option>
                                <option>Newest3</option>
                            </select>
                        </span> -->
                    </div>
                    <div class="clr"></div>
                </div>
                <div class="row mobile-block">
                
                <div class="col-sm-12 col-md-12 col-lg-12">
               





                            <div class="categories-block-mian">
                            <div class="container">
                            <div class="categories-head-block">
                                What is your market category?
                            </div>
                            <div class="categories-semihead">
                                Explore some of the best tips from around the city from our partners and friends.
                            </div>


                            <div class="row">

                            <?php  if(isset($getCat) && sizeof($getCat) > 0) { ?>

                                <?php $cat_show = 0;  foreach ($getCat as $category) { $cat_show++; 


                                    $this->db->where('tbl_seller_upload_product.category_id' ,$category['category_id']);
                                    $cate_product = $this->master_model->getRecords('tbl_seller_upload_product');
                                    ?>

                                    <?php if($cat_show <= 7) { ?>

                                        <?php if($cat_show == 5){ ?>

                                                <div class="col-sm-6 col-md-6 col-lg-6">
                                                <div class="cate-img-block mobile-height">
                                                    <a href="<?php echo base_url().'search?cat_id%5B%5D='.$category['category_id']; ?>">
                                                        

                                                        <?php 
                                                        if(isset($category['profile_image']) && file_exists('uploads/cat_logo/'.$category['profile_image']))
                                                        {
                                                        ?>
                                                        <img  src="<?php echo base_url().'uploads/cat_logo/'.$category['profile_image'];?>" alt=""/>
                                                        <?php
                                                        }
                                                        else
                                                        {
                                                        ?>
                                                        <img src="<?php echo base_url().'images/noimage200X150.png';?>" alt="" />
                                                        <?php
                                                        }
                                                        ?>




                                                        <span class="img-overlay-cate"></span>
                                                        <span class="cate-img-head">
                                                        <?php if(isset($category['category_name'])) { echo $category['category_name']; } else { echo "Not Available"; } ?>
                                                    </span>
                                                        <span class="cate-icon-img">

                                                        <?php if($category['category_name'] == 'Oils'){

                                                            ?> <img src="<?php echo base_url(); ?>front-asset/images/cate-oil-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Canned Food') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-canned-food-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Vegetables') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-vagitable-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Chocolate') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-chocolate-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Snacks') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-snacks-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Cereals') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-cereals-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Wines') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-wines-icon-img.png" alt="" /><?php

                                                        }?>
                                                       
                                                    </span>
                                                        <span class="cate-count-block">
                                                        <?php echo count($cate_product); ?>
                                                    </span>
                                                   
                                                    </a>
                                                </div>
                                                </div>

                                        <?php }
                                        else{ ?>

                                                <div class="col-sm-3 col-md-3 col-lg-3">
                                                <div class="cate-img-block">
                                                    <a href="<?php echo base_url().'search?cat_id%5B%5D='.$category['category_id']; ?>">
                                                        <?php 
                                                        if(isset($category['profile_image']) && file_exists('uploads/cat_logo/'.$category['profile_image']))
                                                        {
                                                        ?>
                                                        <img  src="<?php echo base_url().'uploads/cat_logo/'.$category['profile_image'];?>" alt=""/>
                                                        <?php
                                                        }
                                                        else
                                                        {
                                                        ?>
                                                        <img src="<?php echo base_url().'images/noimage200X150.png';?>" alt="" />
                                                        <?php
                                                        }
                                                        ?>
                                                        <span class="img-overlay-cate"></span>
                                                        <span class="cate-img-head">
                                                        <?php if(isset($category['category_name'])) { echo $category['category_name']; } else { echo "Not Available"; } ?>
                                                        </span>
                                                        <span class="cate-icon-img">
                                                        <?php if($category['category_name'] == 'Oils'){

                                                            ?> <img src="<?php echo base_url(); ?>front-asset/images/cate-oil-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Canned Food') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-canned-food-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Vegetables') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-vagitable-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Chocolate') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-chocolate-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Snacks') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-snacks-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Cereals') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-cereals-icon-img.png" alt="" /><?php

                                                        } else if($category['category_name'] == 'Wines') {
                                                            
                                                            ?><img src="<?php echo base_url(); ?>front-asset/images/cate-wines-icon-img.png" alt="" /><?php

                                                        }?>
                                                        </span>
                                                        <span class="cate-count-block">
                                                        <?php echo count($cate_product); ?>
                                                    </span>
                                                   </a>
                                                </div>
                                              </div>

                                        <?php } ?>

                                    <?php } ?>
                                    
                                <?php } ?>    
                            <?php } else { ?>

                                <?php $this->load->view('no-data-found'); ?>

                            <?php } ?>

                            </div>

                            <?php if(count($getCat) > 6) { ?>

                            <div class="all-listing-btn">
                                <a class="btn-listing-all" href="#">All Listing</a>
                            </div>

                            <?php } ?>


                            </div>
                            </div>





                </div>



            </div>
        </div>
    </div>
</div>        
</div>    


<!-- Requirment Modal -->
<div id="myReqModal" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">Make Offer</h4>
        </div>
        <div class="modal-body">
        <?php $this->load->view('status-msg'); ?>
        <div ng-controller="OfferToRequirmentCntrl">  
            <form  
                id="makeofferForm"
                class=""
                name="makeofferForm" 
                enctype="multipart/form-data"
                novalidate 
                ng-submit="makeofferForm.$valid && requirment_offer();"  >

                    <input type="hidden" name="requirment_id" id="requirment_id" ng-model="user.requirment_id" placeholder="requirment_id">
                   
                    <div class="mobile-nu-block enquiry" ng-class="{'has-error':makeofferForm.price.$touched && makeofferForm.price.$invalid}">
                            <input type="text" 
                                   name="price" 
                                   class="beginningSpace_restrict char_restrict" 
                                   ng-model="user.price"
                                   ng-required="true"
                                   placeholder="Enter price"/>
                        <div class="error-new-block" ng-messages="makeofferForm.price.$error" ng-if="makeofferForm.$submitted || makeofferForm.price.$touched">
                        <div>
                        <div class="err_msg_div" style="display:none;">
                            <p ng-message="required"    class="error">  This field is required</p>
                            </div>
                        </div>
                        <script type="text/javascript">
                                $(document).ready(function(){
                                  setTimeout(function(){
                                    $('.err_msg_div').removeAttr('style');
                                  },200);
                                });
                            </script>
                        </div>

                        <span class="highlight"></span>
                        <label>Unit price (<?php echo CURRENCY; ?>)</label>

                    </div>


                    <div class="mobile-nu-block enquiry" ng-class="{'has-error':makeofferForm.country.$touched && makeofferForm.country.$invalid}">
                            <input type="text" 
                                   name="country" 
                                   class="beginningSpace_restrict" 
                                   ng-model="user.country" 
                                   placeholder="Enter country"
                                   ng-required="true"
                                   />
                        <div class="error-new-block" ng-messages="makeofferForm.country.$error" ng-if="makeofferForm.$submitted || makeofferForm.country.$touched">
                        <div>
                        <div class="err_msg_div" style="display:none;">
                            <p ng-message="required"    class="error">  This field is required</p>
                            </div>
                        </div>
                        <script type="text/javascript">
                                $(document).ready(function(){
                                  setTimeout(function(){
                                    $('.err_msg_div').removeAttr('style');
                                  },200);
                                });
                            </script>
                        </div>

                        <span class="highlight"></span>
                        <label>Country</label>

                    </div>
                    
                    <div class="mobile-nu-block input-first box-msg" ng-class="{'has-error':makeofferForm.description.$touched && makeofferForm.description.$invalid}">
                        <textarea rows="" 
                                      cols="" 
                                      name="description" 
                                      class="beginningSpace_restrict" 
                                      ng-model="user.description"
                                      ng-required="true"
                                      ng-maxlength="500"
                                      placeholder="Enter offer description"></textarea>

                        <span class="highlight"></span> 
                        <label>Message</label>

                        <div class="error-new-block" ng-messages="makeofferForm.description.$error" ng-if="makeofferForm.$submitted || makeofferForm.description.$touched">
                        <div>
                        <div class="err_msg_div" style="display:none;">
                            <p ng-message="required"    class="error">  This field is required</p>
                            <p ng-message="maxlength"   class="error">  This field to long. Only 1000 characters allow.</p>
                            </div>
                        </div>
                        <script type="text/javascript">
                                $(document).ready(function(){
                                  setTimeout(function(){
                                    $('.err_msg_div').removeAttr('style');
                                  },200);
                                });
                            </script>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-password changes enq">
                        <button class="change-btn-pass" type="submit">Send</button>
                    </div>
                </div>
            </form>    
        </div>

    </div>
</div>
</div>
<!-- End Requirment Modal -->

<script type="text/javascript">
    $(document).ready(function() { 
      $('.close').on('click',function() {
        setTimeout(function(){
            location.reload();
        })
      });
    });
</script>


<script type="text/javascript">
$(document).ready(function() {
    $(".more-btn-block").click(function() {
        $(this).parent().find(".view-more-content").slideDown("slow");
        $(this).hide();
        $(this).parent().find(".hide-btn-block").show();
    });
    $(".hide-btn-block").click(function() {
        $(this).parent().find(".view-more-content").slideUp("slow");
        $(this).parent().find(".more-btn-block").show();
        $(this).hide();
    });

    // footer dropdown links start 
    var min_applicable_width = 767;

    $(document).ready(function() {
        applyResponsiveSlideUp($(this).width(), min_applicable_width);
    });

    function applyResponsiveSlideUp(current_width, min_applicable_width) {
        /* Set For Initial Screen */
        initResponsiveSlideUp(current_width, min_applicable_width);

        /* Listen Window Resize for further changes */
        $(window).bind('resize', function() {
            if ($(this).width() <= min_applicable_width) {
                unbindResponsiveSlideup();
                bindResponsiveSlideup();
            } else {
                unbindResponsiveSlideup();
            }
        });
    }

    function initResponsiveSlideUp(current_width, min_applicable_width) {
        if (current_width <= min_applicable_width) {
            unbindResponsiveSlideup();
            bindResponsiveSlideup();
        } else {
            unbindResponsiveSlideup();
        }
    }

    function bindResponsiveSlideup() {
        $(".menu_name2").hide();
        $(".footer_heading2").bind('click', function() {
            var $ans = $(this).parent().find(".menu_name2");
            $ans.slideToggle();
            $(".menu_name2").not($ans).slideUp();
            $('.menu_name2').removeClass('active');

            $('.footer_heading2').not($(this)).removeClass('active');
            $(this).toggleClass('active');
            $(this).parent().find(".menu_name2").toggleClass('active');
        });
    }

    function unbindResponsiveSlideup() {
        $(".footer_heading2").unbind('click');
        $(".menu_name2").show();
    }
    // footer dropdown links end

});

</script>
