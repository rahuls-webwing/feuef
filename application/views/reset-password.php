<style>
    .footer-col-block {display: none;}.copyright-block {margin-top: 0;}
    body{background: #f2f6f9;}
    .footer-block-main{position: fixed; bottom: 0; width: 100%;}
</style>
<div class="middle-login-content forgot-pass-block">
    <div class="container">
        <div class="forgot-pass-main">

        <?php $this->load->view('status-msg'); ?>
            
            <div ng-controller="ResetPassCntrl">
                <form 
                      class=""
                      ng-init="user.confirm_code = '<?php echo $conf_code; ?>'";
                      name="ResetPassForm" 
                      novalidate 
                      ng-submit="ResetPassForm.$valid && processResetPass();">


                <div class="login-form-block">
                    <div class="login-head-block">
                        Reset Password
                    </div>
                    <div class="login-content-block">
                        Reset your password.
                    </div>

                    <input type="hidden" name="confirm_code" ng-model="user.confirm_code" />

                    <div class="mobile-nu-block input-first" ng-class="{ 'has-error': ResetPassForm.newpwd.$touched && ResetPassForm.newpwd.$invalid }">
                        <input type="password" name="newpwd" ng-model="user.newpwd" ng-minlength="5" ng-maxlength="50" ng-required="true" />
                        <span class="highlight"></span>
                        <div class="error-new-block" ng-messages="ResetPassForm.newpwd.$error" ng-if="ResetPassForm.$submitted || ResetPassForm.newpwd.$touched">
                             <div>
                                <div class="err_msg_div" style="display:none;">
                                    <p ng-message="required"    class="error">  This field is required</p>
                                    <p ng-message="minlength"   class="error">  Please enter at least six character</p>
                                    <p ng-message="maxlength"   class="error">  Password to long</p>
                                    <p ng-message="pwd"         class="error">  Password is not valid</p>
                                    </div>

                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                      setTimeout(function(){
                                        $('.err_msg_div').removeAttr('style');
                                      },200);
                                    });
                                </script>
                        </div>
                        <label>New Password</label>
                    </div>

                    <div class="mobile-nu-block input-first" ng-class="{ 'has-error': ResetPassForm.cmf_pwd.$touched && ResetPassForm.cmf_pwd.$invalid }">
                        <input type="password" name="cmf_pwd" ng-model="user.cmf_pwd" ng-minlength="5" ng-maxlength="50" ng-required="true" />
                        <span class="highlight"></span>
                        <div class="error-new-block" ng-messages="ResetPassForm.cmf_pwd.$error" ng-if="ResetPassForm.$submitted || ResetPassForm.cmf_pwd.$touched">
                             <div>
                                <div class="err_msg_div" style="display:none;">
                                    <p ng-message="required"    class="error">  This field is required</p>
                                    <p ng-message="minlength"   class="error">  Please enter at least six character</p>
                                    <p ng-message="maxlength"   class="error">  Password to long</p>
                                    <p ng-message="pwd"         class="error">  Password is not valid</p>
                                    <p id="err_cmpass"          class="error"></p>
                                    </div>

                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                      setTimeout(function(){
                                        $('.err_msg_div').removeAttr('style');
                                      },200);
                                    });
                                </script>
                        </div>
                        <label>Confirm Password</label>
                    </div>

                    <div class="btn-block-main-login">
                        <div class="btn-login-block">
                            <button class="login-btn" type="submit">Submit</button>
                        </div>
                        <div class="clr"></div>
                    </div>
                </div>
                </form>
            </div>    
          

            <div class="clr"></div>
        </div>
    </div>
</div>