<style>
    .footer-col-block {display: none;}
    .copyright-block {margin-top: 0;}
    
/*body{background:#F2F6F9;margin: 0px;padding: 0;}
body::before {background:#F2F6F9;content: ""; display: block;height: 100%;position: absolute;width: 100%;}*/
.footer-block-main {bottom: 0;position: fixed !important;width: 100%;z-index: 9;}
body{background:#F2F6F9;margin: 0px;padding: 0;}
</style>
<div class="middle-login-content">
    <div class="container">
       
            
            <div class="col-sm-12 col-md-12 col-lg-12">

                <?php $this->load->view('status-msg'); ?>

                <div ng-controller="LoginCntrl" >
                   
                    <form  class=""
                       name="LoginForm" 
                       novalidate ng-submit="LoginForm.$valid && processLogin();"
                       >
                        <div class="login-form-block">
                            <div class="login-head-block">
                                Login
                            </div>
                            <div class="login-content-block">
                                If you have an account with us, please log in.
                            </div>
                            <input type="hidden" id="hidden_email" value="<?php if(isset($_COOKIE['remember_me_email'])) echo $_COOKIE['remember_me_email']; ?>">
                            <input type="hidden" id="hidden_pass"  value="<?php if(isset($_COOKIE['remember_me_password'])) echo $_COOKIE['remember_me_password']; ?>">

                     
                            <div class="mobile-nu-block input-first" ng-class="{ 'has-error': LoginForm.email.$touched && LoginForm.mobile.$invalid }">
                                <input class="beginningSpace_restrict" autocomplete="false" type="email" name="email" ng-model="user.email" id="email"  ng-required="true" />
                                <span class="highlight"></span>
                                <div class="error-new-block" ng-messages="LoginForm.email.$error" ng-if="LoginForm.$submitted || LoginForm.email.$touched">
                                        <?php $this->load->view('error-message'); ?> 
                                </div>
                                <label>Email Id</label>
                            </div>
                            <div class="mobile-nu-block input-first" ng-class="{ 'has-error': LoginForm.pwd.$touched && LoginForm.pwd.$invalid }">
                                <input autocomplete="off" class="beginningSpace_restrict CopyPast_restrict" type="password" name="pwd" id="pwd" ng-model="user.pwd" ng-required="true" />
                                <span class="highlight"></span>
                                <div class="error-new-block" ng-messages="LoginForm.pwd.$error" ng-if="LoginForm.$submitted || LoginForm.pwd.$touched">
                                        <?php $this->load->view('error-message'); ?>
                                </div>
                                <label>Password</label>
                            </div>
                            <div class="mobile-nu-block input-first" ng-class="{ 'has-error': LoginForm.pwd.$touched && LoginForm.pwd.$invalid }">
                                <div class="log-social">
                                     <ul>
                                        <li><a href="javascript:void(0);" id="facebook_login" tabindex="11"><i class="fa fa-facebook"></i> Facebook</a></li>
                                     </ul>
                                  </div>
                            </div>

                            <div class="main-forgot-pass">
                                <div class="check-bx">
                                    <input 
                                    class="css-checkbox" 
                                    id="radio5" 
                                    name="login_checkbx" 
                                    ng-model="user.login_checkbx" 
                                    type="checkbox">
                                    <label class="css-label radGroup2" for="radio5">Remember my details</label>
                                </div>
                                <div class="recover-pass-block">
                                    <a href="<?php echo base_url(); ?>login/forgot_password">Forgot Password?</a>
                                </div>
                                <div class="clr"></div>
                            </div>
                            <div class="btn-block-main-login">
                                <div class="btn-login-block">
                                    <button class="login-btn" type="submit">Log In</button>
                                </div>
                                <div class="clr"></div>
                            </div>
                            <div class="login-content-block">
                                Don't have an Account? <a href="<?php echo base_url(); ?>signup">Sign Up Now!</a>
                            </div>
                        </div>
                    </form>
                </div>    



            </div>
            <div class="clr"></div>
       
    </div>
</div>
<?php if(isset($_COOKIE['remember_me_email']) && isset($_COOKIE['remember_me_password'])) { ?>
    <script type="text/javascript">
    $(document).ready(function(){
        $('#email').val($('#hidden_email').val());
        $('#pwd').val($('#hidden_pass').val());
        $('#radio5').click();
    });
    </script>
<?php } ?>   


<!-- Facebook Login Starts Here  -->
  <script type="text/javascript">

     /*Load Facebook Base URL*/
    window.fbAsyncInit = function() {
        //Initiallize the facebook using the facebook javascript sdk
        FB.init({
            appId: '<?php $this->config->load("facebook"); echo $this->config->item("appID");?>', // App ID 
            // appId:'963588753682463',
            cookie: true, // enable cookies to allow the server to access the session
            status: true, // check login status
            xfbml: true, // parse XFBML
            oauth: true //enable Oauth 
        });
    };
    //Read the baseurl from the config.php file
    (function(d) {

        //console.log(d); 
        var js, id = 'facebook-jssdk',
            ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));

    jQuery('#facebook_login').click(function(e) {
      e.preventDefault();
       // alert('hello test'); die;
        FB.login(function(response) {
            if (response.authResponse) {
                //alert(response);
                FB.api('/me', 'get', {
                    fields: 'email,first_name,last_name,birthday'
                }, function(response) {

                   // alert(response);
                    var email = response.email;
                    var fname = response.first_name;
                    var lname = response.last_name;

                    if(email==undefined || email=="")
                    {
                        alert(lang.err_login_1);
                        return false;
                    }

                    FB.api('/me/picture?type=normal', function(response) {
                        var site_url = "<?php echo base_url(); ?>";
                        var datastr = "email="+email+"&fname="+fname+"&lname="+lname;
                        jQuery.ajax({
                            url: site_url + 'user/fb_login',
                            type: 'POST',
                            data: datastr,
                            dataType:'json',
                            success: function(response) {
                              if ($.trim(response.result) == "registration_success") 
                                {
                                    window.location.href = site_url+'user/update';
                                    //window.location.reload();
                                }
                                else if($.trim(response.result) == 'registration_error')
                                {
                                    $('#error_div').text(response.message);
                                    $('#error_div').show();
                                    $('#error_div').focus();
                                    //window.location.href = site_url;
                                    //window.location.reload();
                                }
                                else if($.trim(response.result) == "error")
                                {
                                     //fbLogoutUser_alt();
                                     $('#error_div').text(response.message);
                                     $('#error_div').show();
                                     //window.location.href = site_url;
                                     //window.location.reload();
                                }
                                else if($.trim(response.result) == 'login_success')
                                {
                                    window.location.href = site_url+'user/dashboard';
                                    //$('#logout_url').attr('href',response.logout_url);
                                    //window.location.href = site_url;
                                     //window.location.reload();
                                }
                                
                                return false;
                            }
                        });
                    });
                    return false;
                });
            }
        }, {
            scope: 'public_profile,email'
        }); //permissions for facebook
    });
  </script>

<!-- Facebook Login Ends Here -->
