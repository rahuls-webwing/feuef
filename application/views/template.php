<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta property="og:title" content="E-marketplace" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.webwingtechnologies.com/" />
    <meta property="og:image" content="images/logo.png" />
    <meta property="og:description" content="E-marketplace" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <title><?php echo $pageTitle; ?></title>
    <!-- ======================================================================== -->

    <script type="text/javascript">
        var site_url = "<?php echo base_url(); ?>";
    </script>


    <link rel="icon" href="<?php echo base_url(); ?>images/fevicon/favicon.png" type="image/x-icon" />
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>front-asset/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!--font-awesome-css-start-here-->
    <link href="<?php echo base_url(); ?>front-asset/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>front-asset/css/animate.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>front-asset/css/e-marketplace.css" rel="stylesheet" type="text/css" />
    <!--    <link href="css/ihover.css" rel="stylesheet" type="text/css" />-->
    <!--    <link href="css/rtl.css" rel="stylesheet" type="text/css" />-->    
    <link href="<?php echo base_url(); ?>front-asset/css/select2.min.css" rel="stylesheet" type="text/css" />
   
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>front-asset/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>front-asset/js/bootstrap.min.js"></script>
    <!--scrollbar plugins-->
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>front-asset/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <link href="<?php echo base_url(); ?>front-asset/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />   
    <!--   Angular Start Here-->


    <script src="<?php echo base_url(); ?>front-asset/js/angular/angular.min.js"></script>
    <script src="<?php echo base_url(); ?>front-asset/js/angular/angular-route.min.js"></script>
    <script src="<?php echo base_url(); ?>front-asset/js/angular/ui-bootstrap-tpls-2.4.0.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>front-asset/js/angular/angular-messages.js"></script>


    <script src="<?php echo base_url(); ?>front-asset/js/angular/main.js" type="text/javascript"></script>
      
 

    <!--   Angular End Here-->    
    <script src="<?php echo base_url();?>assets/T-validations/masterT-validations.js"></script>

    <!-- sweet alert -->
    <script src="<?php echo base_url(); ?>assets/sweetalert/sweetalert.min.js"></script> <script src="<?php echo base_url(); ?>assets/sweetalert/sweetalert_actions.js">
    </script> <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sweetalert/sweetalert.css">
    <style type="text/css">
    .sweet-alert {
        background-color: white;
        font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        width: 478px;
        padding: 17px;
        border-radius: 5px;
        text-align: center;
        position: fixed;
        left: 50%;
        top: 10%;
        margin-left: -256px;
        margin-top: 100px !important;
        overflow: hidden;
        display: none;
        z-index: 99999;
    }  
    .pac-container:after {
    /* Disclaimer: not needed to show 'powered by Google' if also a Google Map is shown */

    background-image: none !important;
    height: 0px;
    }
    </style>
    <!-- end sweet alert -->
     
</head>
<style type="text/css">
    .wrapDiv {
        font-size: 11px;
    }
</style>

<script type="text/javascript">
function ajaxindicatorstart(text)
{
  if(text == null || text == 'undefine' || text == ""){
    text = 'Please wait..';
  }
  if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
  jQuery('body').append('<div id="resultLoading" style="display:none"><div><img style="height:120px;width:120px;" src="<?php echo base_url(); ?>images/loader/preloader.gif"><div>'+text+'</div></div><div class="bg"></div></div>');
  }

  jQuery('#resultLoading').css({
    'width':'100%',
    'height':'100%',
    'position':'fixed',
    'z-index':'10000000',
    'top':'0',
    'left':'0',
    'right':'0',
    'bottom':'0',
    'margin':'auto'
  });

  jQuery('#resultLoading .bg').css({
    'background':'#000000',
    'opacity':'0.7',
    'width':'100%',
    'height':'100%',
    'position':'absolute',
    'top':'0'
  });

  jQuery('#resultLoading>div:first').css({
    'width': '250px',
    'height':'75px',
    'text-align': 'center',
    'position': 'fixed',
    'top':'0',
    'left':'0',
    'right':'0',
    'bottom':'0',
    'margin':'auto',
    'font-size':'16px',
    'z-index':'10',
    'color':'#ffffff'

  });

    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeIn(300);
    jQuery('body').css('cursor', 'wait');
}
function ajaxindicatorstop()
{
    jQuery('#resultLoading .bg').height('100%');
       jQuery('#resultLoading').fadeOut(300);
    jQuery('body').css('cursor', 'default');
}
</script>
<script type="text/javascript">

  $(window).on('beforeunload', function(){
        //ajaxindicatorstart('Please wait..');
  });

  $(document).ready(function(){
      //ajaxindicatorstart('Please wait..');
  });

  $(window).bind("load", function() { 
    //ajaxindicatorstop();
  });

  
</script>
<!-- Set TimeZhone -->
<?php $this->load->view('timezone'); ?>

<body ng-app="miniMe" scroll-to-top id="top" data-base-url="<?php echo base_url(); ?>" >

<?php 
if($this->session->userdata('user_name')!='' || $this->session->userdata('user_id')!='')
{
    $this->db->where('status !=','Delete');
    $this->db->where('status !=','Block');
    $this->db->where('id',$this->session->userdata('user_id'));
    $this->db->where('verification_status','Verified');
    $user   = $this->master_model->getRecords('tbl_user_master',FALSE,FALSE,array('id','DESC'));
    if(sizeof($user) <= 0){
        $this->session->sess_destroy();
        
        $this->session->set_flashdata('error' , 'Sorry, we logged you forcefully....');
        redirect(base_url()."login");
    }
}
?>       

    
<?php $get_site_status = $this->master_model->getRecords('admin_login'); ?>

    <?php if($get_site_status[0]['site_status'] == '1') { ?>


        <?php
        if($this->session->userdata('user_id') !=""){
            $this->load->view('inner-header',$page_title);
        }else{
            $this->load->view('home-header',$page_title);
        }


        $this->load->view($middle_content);
        $this->load->view('footer');
        ?>

    <?php } else {
    $this->load->view('underconstruction');
    } ?>

</body>


</html>
