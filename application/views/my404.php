<div class="banner-404">
    <div class="container">
        <div class="man-404"><img src="<?php echo  base_url(); ?>front-asset/images/man-404.png" class="img-responsive" alt="404-page" /></div>
        <div class="wrapper">
            <div class="img2-404page"><img src="<?php echo  base_url(); ?>front-asset/images/404-small-img.png" class="img-responsive" alt="404-page" /></div>
            <h1>404</h1>
            <h4>Something is wrong</h4>
            <h5>The pageyou are looking for was moved, removed or might never exested</h5>
            <a href="javascript:history.back()" class="back-btn"><span><i class="fa fa-long-arrow-left" aria-hidden="true"></i>
              </span>Go Back</a>
        </div>
    </div>
</div>