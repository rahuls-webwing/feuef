<style type="text/css">
.pagination>li>a, .pagination>li>span {
    position: relative;
    float: left;
    top: -40px;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #ddd;
}
.label-important, .badge-important {
        background-color: #fe1010;
}
body {
    letter-spacing: 0px;
}
</style>
<?php 
if($seller_count > 9){
    ?>
    <style>
        .block-filter-container {
        margin-bottom: -46px;
        }
    </style>
    <?php
}
?>
<!-- Multi Select CSS -->
<link href="<?php echo base_url();?>front-asset/css/select2.min.css" rel="stylesheet" type="text/css" />
<div id="listing-header"></div>

<div class="listing-section">
    <div class="container-fluid">
        <div class="row">
            <!--list view start here-->
            <form id="serach_critearea" action="<?php echo base_url().'search'; ?>" method="GET">

            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="box-listing space-top-gowed">
                 <div class="row">


                    <div class="col-sm-3 col-md-2 col-lg-2">
                        <div class="user-one locations-input">
                            <input type="hidden" id="hedden_search" value="<?php  if(isset($_REQUEST['seller']) && $_REQUEST['seller'] != ""){ echo $_REQUEST['seller']; } ?>" name="seller">
                            <input  class="con-input" name="seller" id="seller" value="<?php  if(isset($_REQUEST['seller']) && $_REQUEST['seller'] != ""){ echo $_REQUEST['seller']; } ?>" placeholder="Search Product Or Sellers......." type="text" />
                            <div class="locations-input"><i class="fa fa-user-o" aria-hidden="true"></i></div> 
                        </div>
                    </div>


                    <div class="col-sm-3 col-md-2 col-lg-2">
                        <div class="user-one locations-input">
                            <input  class="con-input" name="location" id="location" value="<?php  if(isset($_REQUEST['location']) && $_REQUEST['location'] != ""){ echo $_REQUEST['location']; } ?>"  placeholder="Enter the location" type="text" />
                            <div class="locations-input"><i class="fa fa-map-marker" aria-hidden="true"></i></div> 
                        </div>
                    </div>


                    <div class="col-sm-3 col-md-2 col-lg-2">
                            <div class="user-box revt-clas">
                                <div class="arrow-img-cls"><img src="<?php echo base_url();?>front-asset/images/select-droup.png" alt="" /></div>
                                <div class="assignment-gray-main">
                                   <select class="js-example-basic-multiple" multiple="multiple" name="cat_id[]" >
                                    <?php if(isset($getCategory) && sizeof($getCategory) > 0) { ?>
                                         <?php $cnt=0; foreach($getCategory as $category) { $cnt++;?>
                                           <option <?php if(isset($_REQUEST['cat_id']) &&  in_array($category['category_id'], $_REQUEST['cat_id'])) { echo 'selected'; } ?> value="<?php echo $category['category_id']; ?>"><?php echo $category['category_name']; ?></option>
                                    <?php } } ?>
                                </select>
                            </div>
                            <div class="clerfix"></div>
                        </div>
                    </div>
                

                    <div class="col-sm-3 col-md-2 col-lg-2">
                        <div class="gry-bx select-style">
                            <select class="frm-select fnt" name="selector">
                                <option>Select Rating</option>
                                <option value="all" <?php if(isset($_REQUEST['selector']) &&  $_REQUEST['selector']=="all") { echo 'selected'; } ?>>All</option>
                                <option value="lToh" <?php if(isset($_REQUEST['selector']) &&  $_REQUEST['selector']=="lToh") { echo 'selected'; } ?>>Low To High</option>
                                <option id="hTol" name="selector" value="hTol" <?php if(isset($_REQUEST['selector']) &&  $_REQUEST['selector']=="hTol") { echo 'selected'; } ?>>High To Low</option>
                            </select>
                        </div>
                    </div>

 
 
                    <input type="hidden" name="min-price" id="min-price" value="<?php if(isset($_REQUEST['min-price'])) { echo $_REQUEST['min-price']; } else { echo '0'; }  ?>" >
                    <input type="hidden" name="max-price" id="max-price" value="<?php if(isset($_REQUEST['max-price'])) { echo $_REQUEST['max-price']; } else { echo '0'; }  ?>" >
                    <div class="col-sm-2 col-md-2 col-lg-2">
                       <div class="price-slider seller-list-prices">
                           <div class="range-t input-bx" for="amount">
                               <div id="slider-price-range" class="slider-rang"></div>
                                  <div class="amount-no" id="slider_price_range_txt">
                               </div>
                           </div>
                       </div>
                    </div>


            <div class="col-sm-2 col-md-1 col-lg-1">
             <button type="submit" id="search_listing" class="more-f-bor"><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
            <?php  if(isset($_REQUEST['location'])){ ?>
            <div class="col-sm-2 col-md-1 col-lg-1">
             <a href="<?php echo base_url().'search'; ?>"  class="more-f-bor"><i class="fa fa-refresh" aria-hidden="true"></i></a>
            </div>
            <?php } ?>
     </div>
 </div>
</form>
</div>


<!-- seller prices -->
<?php
$max_price =[];
foreach ($seller_product_price as $price) {
    $max_price[] = strtok($price['price'],'.');
}
?>
<input type="hidden"  id="max_product_price" value="<?php echo MAX($max_price); ?>" ?>
<!-- end seller prices for slider -->



<div class="col-sm-7 col-md-7 col-lg-7">
    <div class="block-filter-container">
        <div class="sort-by-block">
            <span class="title-listing-block">Products</span>
        </div>
    </div>
    <div class="clr"></div>
     
    <div class="row mobile-block">
    <?php if(isset($get_seller) && sizeof($get_seller) > 0) { ?>   
         
        <?php  foreach ($get_seller as $seller) {
            if(isset($seller['seller_id'])) { $seller['seller_id']= $seller['seller_id']; } else { $seller['seller_id'] = $seller['id'];}

            /* get seller last uploaded product */
            $this->db->where('tbl_seller_upload_product.status' , 'Unblock');
            $this->db->where('tbl_seller_upload_product.id' , $seller['id']);
            $this->db->order_by('id' , 'desc');
            $this->db->group_by('tbl_seller_upload_product.seller_id');
            //$this->db->limit(1);
            $get_seller_last_uploaded_product  = $this->master_model->getRecords('tbl_seller_upload_product');

           
            if(sizeof($get_seller_last_uploaded_product) != 0) { 
            ?>
                <div class="col-sm-6 col-md-4 col-lg-4 over-block-new">
                    <div class="item-block-main">
                        <div class="item-img-block">

                            <?php if(!empty($get_seller_last_uploaded_product[0]['req_image']) && file_exists('images/seller_upload_product_image/'.$get_seller_last_uploaded_product[0]['req_image'])){?>
                            <img style="height:162px;width:262px;" src="<?php echo base_url().'images/seller_upload_product_image/'.$get_seller_last_uploaded_product[0]['req_image']; ?>" alt="list_img_1" class="" />
                            <?php } else { ?>
                            <img style="height:162px;width:262px;" src="<?php echo base_url().'images/seller_upload_product_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                            <?php } ?> 
                            
                            <div class="profile-pic-listing">
                                    <?php if(!empty($seller['user_image']) && file_exists('images/seller_image/'.$seller['user_image'])){?>
                                    <img src="<?php echo base_url().'images/seller_image/'.$seller['user_image']; ?>" alt="list_img_1" class="img-responsive list_view_img_1" />
                                    <?php } else { ?>
                                    <img src="<?php echo base_url().'images/seller_image/default/default-user-img.jpg'; ?>" alt="" />
                                    <?php } ?> 
                            </div>
                            
                            <div class="price-categorys"><?php echo CURRENCY ?> 
                            <?php  if(!empty($get_seller_last_uploaded_product[0]['price'])) { echo $get_seller_last_uploaded_product[0]['price'];} else{ echo "NA"; } ?>
                            </div>
                        </div>
                        <div class="item-content">
                            <div class="name-item-block">
                                    <?php if(!empty($seller['name'])) {
                                    echo $seller['name']; 
                                    } else { 
                                    echo " User name available"; 
                                    } 
                                    ?> 
                            </div>
                            <div class="semi-head-block">
                                <?php  if(!empty($get_seller_last_uploaded_product[0]['title'])) { echo $get_seller_last_uploaded_product[0]['title'];} else{ echo "<span class='label label-important'><i class='fa fa-ban' aria-hidden='true'></i></span></span>  Product name is not available."; } ?>
                            </div>
                            <div class="item-content-block">
                                 <?php if(!empty($get_seller_last_uploaded_product[0]['description'])) { echo substr($get_seller_last_uploaded_product[0]['description'], 0 , 52).'...'; } else { echo "<span class='label label-important'><i class='fa fa-ban' aria-hidden='true'></i></span> Product description is not available."; } ?>
                            </div>
                        </div>
                        <div class="star-rating-block">
                            <?php 
                            $get_seller_reviews = 0;
                            $ratings         = 0;
                            $arv_rating      = 0;
                            $compl_per       = 0;
                            $completion_per  = 0;


                            $arv_rating      = 0;       
                            $compl_per       = 0;           
                            $completion_per  = 0;

                            $this->db->where('tbl_seller_rating.seller_id', $seller['seller_id']);
                            $this->db->where('tbl_seller_rating.status !=' , 'Delete');
                            $this->db->where('tbl_seller_rating.review_for', 'seller');
                            $get_seller_reviews=$this->master_model->getRecords('tbl_seller_rating');
                            if(empty($get_seller_reviews))
                            {
                            }
                            else
                            {
                            foreach($get_seller_reviews as $field => $value)
                            {
                            $ratings       +=  $value['ratings'];
                            }
                            $arv_rating      = $ratings / count($get_seller_reviews);  
                            $compl_per       = $arv_rating  * 100 / 5 ;          
                            $completion_per  = mb_substr( $compl_per, 0, 4 ) ;  
                            }  
                            ?> 
                          
                                <?php
                                    $finalrate = $chekRating = mb_substr($arv_rating,  0, 4);
                                    for($i=1;$i<=5;$i++) {
                                    $selected = "";

                                    $point = $i - $finalrate;

                                    if(!empty($finalrate) && $i<=$finalrate) {
                                      ?>
                                       <img alt="img" src="<?php echo base_url();?>images/review/star-full.png" style="margin-top:11px;"/>
                                       <?php
                                     } else if($point == '0.5' || $point < '0.5' || $point < '1') {
                                      ?>
                                      <img alt="img" src="<?php echo base_url();?>images/review/star-half.png" style="margin-top:11px;"/>
                                       <?php 
                                    } else {
                                      ?>
                                      <img alt="img" src="<?php echo base_url();?>images/review/star-blank.png" style="margin-top:11px;"/>
                                      <?php 
                                    }
                                   } 
                                ?> 
                                
                                <?php if(mb_substr( $arv_rating, 0, 4 ) >= 4){
                                ?><span class="" style="color:#16a085;"><?php
                                }else if(mb_substr( $arv_rating, 0, 4 ) >= 3){
                                ?><span class="" style="color:#1c9aea;"><?php
                                }else{
                                ?><span class="" style="color:red;"><?php    
                                }?>
                                (<?php echo mb_substr($arv_rating, 0, 4 ); ?>)</span>
                        </div>
                        <div class="over-table-block">
                            
                            <?php
                            if($this->session->userdata('user_type')=='Buyer'){?>
                            <a data-backdrop="static"  data-toggle="modal" class="sellid" data-sellid="<?php echo $seller['seller_id']; ?>" data-target="#myModal" type="button">Contact seller</a>
                            <?php } ?>
                            <a href="<?php echo base_url().'seller/profile/'.$seller['seller_id']; ?>">View Profile</a>
                            <?php if(!empty($get_seller_last_uploaded_product[0]['id'])) { ?>
                            <a href="<?php echo base_url().'seller/product_details/'.$get_seller_last_uploaded_product[0]['id'].'/'.$seller['seller_id']; ?>">View Product</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            <?php
            }
         } ?>

    <?php } else {

        $this->load->view('no-data-found');

    } ?>

    <?php if(empty($get_seller_last_uploaded_product)) { ?>   

        <?php  foreach ($get_seller as $seller) {
                if(isset($seller['seller_id'])) { $seller['seller_id']= $seller['seller_id']; } else { $seller['seller_id'] = $seller['id'];}

                /* get seller last uploaded product */
                $this->db->where('tbl_seller_upload_product.status' , 'Unblock');
                $this->db->where('tbl_seller_upload_product.id' , $seller['id']);
                $this->db->order_by('id' , 'desc');
                $this->db->group_by('tbl_seller_upload_product.seller_id');
                //$this->db->limit(1);
                $get_seller_last_uploaded_product  = $this->master_model->getRecords('tbl_seller_upload_product');
                ?>
                    <div class="col-sm-6 col-md-4 col-lg-4 over-block-new">
                        <div class="item-block-main">
                            <div class="item-img-block">

                                <?php if(!empty($get_seller_last_uploaded_product[0]['req_image']) && file_exists('images/seller_upload_product_image/'.$get_seller_last_uploaded_product[0]['req_image'])){?>
                                <img style="height:162px;width:262px;" src="<?php echo base_url().'images/seller_upload_product_image/'.$get_seller_last_uploaded_product[0]['req_image']; ?>" alt="list_img_1" class="" />
                                <?php } else { ?>
                                <img style="height:162px;width:262px;" src="<?php echo base_url().'images/seller_upload_product_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                                <?php } ?> 
                                
                                <div class="profile-pic-listing">
                                        <?php if(!empty($seller['user_image']) && file_exists('images/seller_image/'.$seller['user_image'])){?>
                                        <img src="<?php echo base_url().'images/seller_image/'.$seller['user_image']; ?>" alt="list_img_1" class="img-responsive list_view_img_1" />
                                        <?php } else { ?>
                                        <img src="<?php echo base_url().'images/seller_image/default/default-user-img.jpg'; ?>" alt="" />
                                        <?php } ?> 
                                </div>
                                
                                <div class="price-categorys"><?php echo CURRENCY ?> 
                                <?php  if(!empty($get_seller_last_uploaded_product[0]['price'])) { echo $get_seller_last_uploaded_product[0]['price'];} else{ echo "NA"; } ?>
                                </div>
                            </div>
                            <div class="item-content">
                                <div class="name-item-block">
                                        <?php if(!empty($seller['name'])) {
                                        echo $seller['name']; 
                                        } else { 
                                        echo " User name available"; 
                                        } 
                                        ?> 
                                </div>
                                <div class="semi-head-block">
                                    <?php  if(!empty($get_seller_last_uploaded_product[0]['title'])) { echo $get_seller_last_uploaded_product[0]['title'];} else{ echo "<span class='label label-important'><i class='fa fa-ban' aria-hidden='true'></i></span></span>  Product name is not available."; } ?>
                                </div>
                                <div class="item-content-block">
                                     <?php if(!empty($get_seller_last_uploaded_product[0]['description'])) { echo substr($get_seller_last_uploaded_product[0]['description'], 0 , 52).'...'; } else { echo "<span class='label label-important'><i class='fa fa-ban' aria-hidden='true'></i></span> Product description is not available."; } ?>
                                </div>
                            </div>
                            <div class="star-rating-block">
                                <?php 
                                $get_seller_reviews = 0;
                                $ratings         = 0;
                                $arv_rating      = 0;
                                $compl_per       = 0;
                                $completion_per  = 0;


                                $arv_rating      = 0;       
                                $compl_per       = 0;           
                                $completion_per  = 0;

                                $this->db->where('tbl_seller_rating.seller_id', $seller['seller_id']);
                                $this->db->where('tbl_seller_rating.status !=' , 'Delete');
                                $this->db->where('tbl_seller_rating.review_for', 'seller');
                                $get_seller_reviews=$this->master_model->getRecords('tbl_seller_rating');
                                if(empty($get_seller_reviews))
                                {
                                }
                                else
                                {
                                foreach($get_seller_reviews as $field => $value)
                                {
                                $ratings       +=  $value['ratings'];
                                }
                                $arv_rating      = $ratings / count($get_seller_reviews);  
                                $compl_per       = $arv_rating  * 100 / 5 ;          
                                $completion_per  = mb_substr( $compl_per, 0, 4 ) ;  
                                }  
                                ?> 
                              
                                    <?php
                                        $finalrate = $chekRating = mb_substr($arv_rating,  0, 4);
                                        for($i=1;$i<=5;$i++) {
                                        $selected = "";

                                        $point = $i - $finalrate;

                                        if(!empty($finalrate) && $i<=$finalrate) {
                                          ?>
                                           <img alt="img" src="<?php echo base_url();?>images/review/star-full.png" style="margin-top:11px;"/>
                                           <?php
                                         } else if($point == '0.5' || $point < '0.5' || $point < '1') {
                                          ?>
                                          <img alt="img" src="<?php echo base_url();?>images/review/star-half.png" style="margin-top:11px;"/>
                                           <?php 
                                        } else {
                                          ?>
                                          <img alt="img" src="<?php echo base_url();?>images/review/star-blank.png" style="margin-top:11px;"/>
                                          <?php 
                                        }
                                       } 
                                    ?> 
                                    
                                    <?php if(mb_substr( $arv_rating, 0, 4 ) >= 4){
                                    ?><span class="" style="color:#16a085;"><?php
                                    }else if(mb_substr( $arv_rating, 0, 4 ) >= 3){
                                    ?><span class="" style="color:#1c9aea;"><?php
                                    }else{
                                    ?><span class="" style="color:red;"><?php    
                                    }?>
                                    (<?php echo mb_substr($arv_rating, 0, 4 ); ?>)</span>
                            </div>
                            <div class="over-table-block">
                                
                                <?php
                                if($this->session->userdata('user_type')=='Buyer'){?>
                                <a data-backdrop="static"  data-toggle="modal" class="sellid" data-sellid="<?php echo $seller['seller_id']; ?>" data-target="#myModal" type="button">Contact seller</a>
                                <?php } ?>
                                <a href="<?php echo base_url().'seller/profile/'.$seller['seller_id']; ?>">View Profile</a>
                                <?php if(!empty($get_seller_last_uploaded_product[0]['id'])) { ?>
                                <a href="<?php echo base_url().'seller/product_details/'.$get_seller_last_uploaded_product[0]['id'].'/'.$seller['seller_id']; ?>">View Product</a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                <?php
                break;
             } ?>

    <?php } ?>
    </div>
    <div class="">
        <?php echo $this->pagination->create_links(); ?>
    </div>
</div>


<!--map-box start here-->
<div class="col-sm-5 col-md-5 col-lg-5"><!--pad-r-less-->
    <div class="row">
        <div class="map-sec">
          <div  id="map" width="100%" height="1270"></div>
        </div>
    </div>
</div>
<!--map-box end here-->



</div>
</div>
</div>


<!-- Modal -->
<div id="myModal" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">Contact Seller</h4>
        </div>
        <div class="modal-body">
        <?php $this->load->view('status-msg'); ?>
         <div ng-controller="ContactSeller">  
            <form  
            id="contacttoseller"
            class=""
            name="contacttoseller" 
            enctype="multipart/form-data"
            novalidate 
            ng-submit="contacttoseller.$valid && contact_to_seller();"  >

            <input type="hidden" name="seller_id" id="seller_id" ng-model="user.seller_id" placeholder="Seller id">
           
            <div class="select-bock-container drop" ng-class="{'has-error':contacttoseller.category_id.$touched && contacttoseller.category_id.$invalid}" >
            <select name="category_id" 
                    ng-model="user.category_id"
                    ng-required="true">
                    <option value="">Category</option>
                    <?php foreach ($getCategory as $cate_value) {?>
                    <option value="<?php echo $cate_value['category_id']; ?>"><?php echo $cate_value['category_name']; ?></option>
                    <?php } ?>
            </select>
            <div class="error-new-block" ng-messages="contacttoseller.category_id.$error" ng-if="contacttoseller.$submitted || contacttoseller.category_id.$touched">
            <div>
            <div class="err_msg_div" style="display:none;">
                <p ng-message="required"    class="error">  This field is required</p>
                </div>
            </div>
            <script type="text/javascript">
                    $(document).ready(function(){
                      setTimeout(function(){
                        $('.err_msg_div').removeAttr('style');
                      },200);
                    });
                </script>
            </div>
           </div>
            <div class="mobile-nu-block enquiry" ng-class="{'has-error':contacttoseller.product_name.$touched && contacttoseller.product_name.$invalid}">
                <input type="text" 
                       name="product_name" 
                       class="beginningSpace_restrict" 
                       ng-model="user.product_name" 
                       placeholder="Enter product name"
                       ng-required="true"
                />
                <div class="error-new-block" ng-messages="contacttoseller.product_name.$error" ng-if="contacttoseller.$submitted || contacttoseller.product_name.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>
                <span class="highlight"></span>
                <label>Product Name</label>
            </div>
            <div class="mobile-nu-block enquiry" ng-class="{'has-error':contacttoseller.country.$touched && contacttoseller.country.$invalid}">
                <input type="text" 
                           name="country" 
                           class="beginningSpace_restrict" 
                           ng-model="user.country" 
                           placeholder="Enter country"
                           ng-required="true"
                           />
                <div class="error-new-block" ng-messages="contacttoseller.country.$error" ng-if="contacttoseller.$submitted || contacttoseller.country.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>

                <span class="highlight"></span>
                <label>Country</label>

            </div>
            
            <div class="mobile-nu-block input-first box-msg" ng-class="{'has-error':contacttoseller.description.$touched && contacttoseller.description.$invalid}">
                <textarea rows="" 
                              cols="" 
                              name="description" 
                              class="beginningSpace_restrict" 
                              ng-model="user.description"
                              ng-required="true"
                              ng-maxlength="1000"
                              placeholder="Enter offer description"></textarea>

                <span class="highlight"></span> 
                <label>Message</label>

                <div class="error-new-block" ng-messages="contacttoseller.description.$error" ng-if="contacttoseller.$submitted || contacttoseller.description.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    <p ng-message="maxlength"   class="error">  This field to long. Only 1000 characters allow.</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>

            </div>
            </div>
            <div class="modal-footer">
                <div class="btn-password changes enq">
                    <button class="change-btn-pass " type="submit">Send</button>
                </div>
            </div>
          </form>
       </div>
    </div>

</div>
</div>
<!-- end Modal -->





<!--multi selection-->
<script src="<?php echo base_url();?>front-asset/js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url();?>front-asset/js/select2.full.js"></script>
<script type="text/javascript">
$(".js-example-basic-multiple").select2();
$('#example-getting-started').multiselect();  
</script>




 

<!-- Autocomplete Sellers -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/giocomplete/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/giocomplete/jquery-1.12.4.js"></script>
<script src="<?php echo base_url(); ?>assets/giocomplete/jquery-ui.js"></script>
<script>
$( function() {
var availableTags = [
    <?php 
    $this->db->select('title');
    $this->db->where('tbl_seller_upload_product.status' , 'Unblock');;
    $this->db->group_by('title'); 
    $this->db->from('tbl_seller_upload_product');
    $product_list = $this->db->get();
    $product_res  = $product_list->result();


    $this->db->select('name');
    $this->db->where('tbl_user_master.status' , 'Unblock');
    $this->db->where('tbl_user_master.user_type' , 'Seller');
    $this->db->where('tbl_user_master.verification_status' , 'Verified');
    $this->db->group_by('name'); 
    $this->db->from('tbl_user_master');
    $user_list = $this->db->get();
    $user_res  = $user_list->result();


    if(count($product_list)>0 || count($user_list)>0 )
    {
      foreach($product_res as $key => $product)
      {
         echo '"'.$product->title.'",';
      }
      foreach($user_res as $key => $user)
      {
         echo '"'.$user->name.'",';
      }
    }
    ?>
];
$( "#seller" ).autocomplete({
  source: availableTags, minLength:1
});
});
</script>
<!-- End Autocomplete Sellers -->


<!-- location -autocomplete -->
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLKCmJEIrOrzdRklZHYer8q9qx2XLJ4Vs&sensor=false&libraries=places"></script>
<script  src="<?php echo base_url(); ?>assets/js/geolocator/jquery.geocomplete.min.js"></script>
<script type="text/javascript">
    
    $(document).ready(function(){
        var location = "";
          $("#location").geocomplete({
            details: ".geo-details",
            detailsAttribute: "data-geo",
            location:location,
            types:[]
        });
    });
</script>

<!-- end location -autocomplete -->







<!-- Clustering  MAP -->
<style>
#map {
height: 100%;
}
html, body {
height: 100%;
margin: 0;
padding: 0;
}
    
.content-img.address-urt{font-size: 14px;    color: #cacaca;}
.img-block-section {
  overflow: hidden;
  margin-bottom: 15px;
  margin-top: 0px;
  position: relative;
  box-shadow: 2px 0 5px rgba(0, 0, 0, 0.37);
  -webkit-box-shadow: 2px 0 5px rgba(0, 0, 0, 0.37);
  -moz-box-shadow: 2px 0 5px rgba(0, 0, 0, 0.37);
  cursor: pointer;
  transition: all 0.5s ease 0s;
  -webkit-transition: all 0.5s ease 0s;
  -moz-transition: all 0.5s ease 0s;
   height: 290px;
}
.details-bx {
  bottom: 0;
  position: absolute;
  width: 100%;padding-bottom: 10px;
}
.details-section {
  background: rgba(0, 0, 0, 0) url("../images/tras-bg.png") repeat scroll 0 0;
/*  height: 100px;*/
}
.img-circle {
  float: left;
  height: 10px;
  width: 10px;
}
.content-img.seller-cont-img {
font-weight: 600;
padding-top: 6px;
}
.content-img {
  color: #333;
  display: block;
  float: left;
  font-family: "ralewaysemibold",sans-serif;
  font-size: 16px;
  max-width: 122px;
  overflow: hidden;
  padding-left: 12px;
  padding-top: 6px;
  text-overflow: ellipsis;
  white-space: nowrap;
  width: 100%;
}
.price-cont {
  color: #fff;
  float: right;
  font-size: 22px;
  padding-top: 12px;
}

</style>



<script type="text/javascript">
        $(function() {

           var max_product_price          = $('#max_product_price').val();
           var max_product_price_constant = $('#max_product_price').val();
           var min_price                  = $('#min-price').val();
           var max_price                  = $('#max-price').val();

           if(max_price != 0)
           {
            max_product_price = max_price;
           }

           $("#slider-price-range").slider({
               range: true,
               min: 0,
               max: max_product_price_constant,
               values: [min_price, max_product_price],
               slide: function(event, ui) {
                   $('#min-price').val(ui.values[0]);
                   $('#max-price').val(ui.values[1]);
                   $("#slider_price_range_txt").html("<span class='slider_price_min'>$ " + ui.values[0] + "</span>  <span style='margin-left:50%;' class='slider_price_max'>$ " + ui.values[1] + " </span>");
               }
           });
           $("#slider_price_range_txt").html("<span class='slider_price_min'> $ " + $("#slider-price-range").slider("values", 0) + "</span>  <span style='margin-left:50%;' class='slider_price_max'>$ " + $("#slider-price-range").slider("values", 1) + "</span>");
         });      
</script> 





<!-- <div id="map" style="width: 1000px; height: 600px;"></div> -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyCccvQtzVx4aAt05YnfzJDSWEzPiVnNVsY&libraries=places" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/markerclustererplus/src/markerclusterer.js"></script>
<script src="<?php echo base_url(); ?>front-asset/js/no-country-place.js" type="text/javascript"></script>
<script>
var locations = [];
locations = [
    <?php   
        if(count($userLatLng))
        {
            foreach($userLatLng as $row) 
            {
                if(!empty($row['latitude']) && !empty($row['longitude']))     
                {  
                    if(!empty($row['user_image']) && file_exists('images/seller_image/'.$row['user_image']))
                    {
                        $img = base_url().'images/seller_image/'.$row['user_image'];
                    }
                    else 
                    {
                        $img = base_url().'images/default/default-user-img.jpg';
                    }
                    ?>
                      ['<div class="col-sm-12 col-sm-12 col-lg-12"><div class="img-block-section"><div class="details-bx"><div class="details-section"><a href="<?php echo base_url()."seller/profile/".$row["id"]; ?>"><div class="content-img"><?php echo $row["name"]; ?></div><div class="content-img address-urt"><?php echo $row["address"]; ?></div><div class="content-img seller-cont-img"><?php echo $row["user_type"]; ?></div></a></div></div><div class="img-grid"><img src="<?php echo $img; ?>" alt="" class="img-resize" style="max-width: none;height: 200px;width: 200px;"></div></div></div>',<?php echo $row['latitude']; ?>,<?php echo $row['longitude']; ?>],
                    <?php
                }
            }
        }
    ?>
];

<?php
    // find center   
    if(count($userLatLng))
    {
        foreach($userLatLng as $row) 
        {
            if(!empty($row['latitude']) && !empty($row['longitude']))     
            {  
                 $minlat = false;
                 $minlng = false;
                 $maxlat = false;
                 $maxlng = false;
                     if ($minlat === false) { $minlat = $row['latitude']; } else { $minlat = ($row['latitude'] < $minlat) ? $row['latitude'] : $minlat; }
                     if ($maxlat === false) { $maxlat = $row['latitude']; } else { $maxlat = ($row['latitude'] > $maxlat) ? $row['latitude'] : $maxlat; }
                     if ($minlng === false) { $minlng = $row['longitude']; } else { $minlng = ($row['longitude'] < $minlng) ? $row['longitude'] : $minlng; }
                     if ($maxlng === false) { $maxlng = $row['longitude']; } else { $maxlng = ($row['longitude'] > $maxlng) ? $row['longitude'] : $maxlng; }
                  // Calculate the center
                  $lat = $maxlat - (($maxlat - $minlat) / 2);
                  $lon = $maxlng - (($maxlng - $minlng) / 2);
            }
        }
    }
?>



var defaultLat =  <?php if(isset($lat)){ echo $lat; } else { echo '19.997453'; } ?>;
var defaultLng =  <?php if(isset($lon)){ echo $lon; } else { echo '73.789802'; } ?>;
var _address   =  "<?php  if(isset($_REQUEST['location']) && $_REQUEST['location'] != ""){ echo $_REQUEST['location']; } else { echo ""; }?>";
var map;       
var mc;/*marker clusterer*/
var mcOptions = {
      gridSize: 20,
      //maxZoom: 7,
      zoom:10,
      imagePath: "https://cdn.rawgit.com/googlemaps/v3-utility-library/master/markerclustererplus/images/m"
  };
/*global infowindow*/
var infowindow = new google.maps.InfoWindow();
/*geocoder*/

var _address = _address;
mapInitialize(defaultLat, defaultLng,_address);
function createMarker(latlng,text)
{
var marker = new google.maps.Marker({
    position: latlng,
    map: map,
    icon: "<?php echo base_url().'images/fevicon/map-icn.png'; ?>"
});
/*get array of markers currently in cluster*/
var allMarkers = mc.getMarkers();
 
/*check to see if any of the existing markers match the latlng of the new marker*/
if (allMarkers.length != 0) {
  for (i=0; i < allMarkers.length; i++) {
    var existingMarker = allMarkers[i];
    var pos = existingMarker.getPosition();
    if (latlng.equals(pos)) {
      text = text + " " + locations[i][0];
    }
  }
}

google.maps.event.addListener(marker, 'click', function(){
    infowindow.close();
    infowindow.setContent(text);
    infowindow.open(map,marker);
});
mc.addMarker(marker);
return marker;
}

function mapInitialize(lat,lng,_address)
{
var geocoder = new google.maps.Geocoder(); 
 geocoder.geocode( { 'address': _address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      /*map.setCenter(results[0].geometry.location);*/
      map.setOptions({ maxZoom: 15 });
      map.fitBounds(results[0].geometry.viewport);
    }
  });
  var options = {
      zoom: 3,
      center: new google.maps.LatLng(lat,lng), 
      mapTypeId: google.maps.MapTypeId.ROADMAP,
  };
  map = new google.maps.Map(document.getElementById('map'), options); 

  /*marker cluster*/
  var gmarkers = [];
  mc = new MarkerClusterer(map, [], mcOptions);
  for (i=0; i<locations.length; i++)
  {
      var latlng = new google.maps.LatLng(parseFloat(locations[i][1]),parseFloat(locations[i][2]));                
      gmarkers.push(createMarker(latlng,locations[i][0]));
  }
}
</script>
<!-- Clustering  MAP -->

<script type="text/javascript">
    $(document).ready(function() { 
      $('.close').on('click',function() {
        setTimeout(function(){
            location.reload();
        })
      });
    });
</script>

<script type="text/javascript">
    
    $(document).bind('keypress', function(e) {
      if(e.keyCode==13){
        $('#serach_critearea').submit();   
       }
    });  
    $('#search_listing').on('click', function() {
        setTimeout(function(){
          $('#serach_critearea').submit();   
        },0);
    });
    $('#seller').keyup(function() {
        var search_text = $(this).val();
        $('#hedden_search').val(search_text);
    });
    $('#seller').blur(function() {
        var search_text = $(this).val();
        $('#hedden_search').val(search_text);
    });
</script>

<script type="text/javascript">
$(document).ready(function(){
    $('.select2-search__field').attr('placeholder' , 'Select categories');
    $('.select2-search__field').attr('style' , 'width: 100em;');
});</script>