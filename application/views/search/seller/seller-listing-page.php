<style type="text/css">
.pagination>li>a, .pagination>li>span {
    position: relative;
    float: left;
    top: -40px;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #ddd;
}
.label-important, .badge-important {
        background-color: #fe1010;
}
body {
    letter-spacing: 0px;
}
</style>
<?php 
if($seller_count > 9){
    ?>
    <style>
        .block-filter-container {
        margin-bottom: -46px;
        }
    </style>
    <?php
}
?>
<div class="page-head-block listing-search">
<div class="page-head-overlay"></div>
<div class="textbx-block-search">

 
    <input type="text" name="seller" id="seller" value="<?php  if(isset($_REQUEST['seller']) && $_REQUEST['seller'] != ""){ echo $_REQUEST['seller']; } ?>" placeholder="Search Product Or Sellers......." />
    <button type="submit" id="search_listing" class="search-btn-home">Search</button>
    <span class="error-msg">Please enter what type of work.</span>
    <div class="clr"></div>

</div>
</div>
<div class="middel-container">
<div class="inner-content-block">
    <div class="container">
        <div class="row">
             <?php $this->load->view('search/seller/search-critearea'); ?>
             <div class="col-sm-8 col-md-9 col-lg-9">
                <div class="block-filter-container">
                    <div class="sort-by-block">
                        <span>Products</span>
                        
                    </div>
                </div>
                <div class="clr"></div>
                 <br>
                <div class="row mobile-block">
                
                <?php if(isset($get_seller) && sizeof($get_seller) > 0) { ?>   
                     
                    <?php  foreach ($get_seller as $seller) {
                        if(isset($seller['seller_id'])) { $seller['seller_id']= $seller['seller_id']; } else { $seller['seller_id'] = $seller['id'];}

                        /* get seller last uploaded product */
                        $this->db->where('tbl_seller_upload_product.status' , 'Unblock');
                        $this->db->where('tbl_seller_upload_product.id' , $seller['id']);
                        $this->db->order_by('id' , 'desc');
                        $this->db->group_by('tbl_seller_upload_product.seller_id');
                        //$this->db->limit(1);
                        $get_seller_last_uploaded_product  = $this->master_model->getRecords('tbl_seller_upload_product');

                       
                        if(sizeof($get_seller_last_uploaded_product) != 0) { 
                        ?>
                            <div class="col-sm-6 col-md-4 col-lg-4 over-block-new">
                                <div class="item-block-main">
                                    <div class="item-img-block">

                                        <?php if(!empty($get_seller_last_uploaded_product[0]['req_image']) && file_exists('images/seller_upload_product_image/'.$get_seller_last_uploaded_product[0]['req_image'])){?>
                                        <img style="height:162px;width:262px;" src="<?php echo base_url().'images/seller_upload_product_image/'.$get_seller_last_uploaded_product[0]['req_image']; ?>" alt="list_img_1" class="" />
                                        <?php } else { ?>
                                        <img style="height:162px;width:262px;" src="<?php echo base_url().'images/seller_upload_product_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                                        <?php } ?> 
                                        
                                        <div class="profile-pic-listing">
                                                <?php if(!empty($seller['user_image']) && file_exists('images/seller_image/'.$seller['user_image'])){?>
                                                <img src="<?php echo base_url().'images/seller_image/'.$seller['user_image']; ?>" alt="list_img_1" class="img-responsive list_view_img_1" />
                                                <?php } else { ?>
                                                <img src="<?php echo base_url().'images/seller_image/default/default-user-img.jpg'; ?>" alt="" />
                                                <?php } ?> 
                                        </div>
                                        
                                        <div class="price-categorys"><?php echo CURRENCY ?> 
                                        <?php  if(!empty($get_seller_last_uploaded_product[0]['price'])) { echo $get_seller_last_uploaded_product[0]['price'];} else{ echo "NA"; } ?>
                                        </div>
                                    </div>
                                    <div class="item-content">
                                        <div class="name-item-block">
                                                <?php if(!empty($seller['name'])) {
                                                echo $seller['name']; 
                                                } else { 
                                                echo " User name available"; 
                                                } 
                                                ?> 
                                        </div>
                                        <div class="semi-head-block">
                                            <?php  if(!empty($get_seller_last_uploaded_product[0]['title'])) { echo $get_seller_last_uploaded_product[0]['title'];} else{ echo "<span class='label label-important'><i class='fa fa-ban' aria-hidden='true'></i></span></span>  Product name is not available."; } ?>
                                        </div>
                                        <div class="item-content-block">
                                             <?php if(!empty($get_seller_last_uploaded_product[0]['description'])) { echo substr($get_seller_last_uploaded_product[0]['description'], 0 , 52).'...'; } else { echo "<span class='label label-important'><i class='fa fa-ban' aria-hidden='true'></i></span> Product description is not available."; } ?>
                                        </div>
                                    </div>
                                    <div class="star-rating-block">
                                        <?php 
                                        $get_seller_reviews = 0;
                                        $ratings         = 0;
                                        $arv_rating      = 0;
                                        $compl_per       = 0;
                                        $completion_per  = 0;


                                        $arv_rating      = 0;       
                                        $compl_per       = 0;           
                                        $completion_per  = 0;

                                        $this->db->where('tbl_seller_rating.seller_id', $seller['seller_id']);
                                        $this->db->where('tbl_seller_rating.status !=' , 'Delete');
                                        $this->db->where('tbl_seller_rating.review_for', 'seller');
                                        $get_seller_reviews=$this->master_model->getRecords('tbl_seller_rating');
                                        if(empty($get_seller_reviews))
                                        {
                                        }
                                        else
                                        {
                                        foreach($get_seller_reviews as $field => $value)
                                        {
                                        $ratings       +=  $value['ratings'];
                                        }
                                        $arv_rating      = $ratings / count($get_seller_reviews);  
                                        $compl_per       = $arv_rating  * 100 / 5 ;          
                                        $completion_per  = mb_substr( $compl_per, 0, 4 ) ;  
                                        }  
                                        ?> 
                                      
                                            <?php
                                                $finalrate = $chekRating = mb_substr($arv_rating,  0, 4);
                                                for($i=1;$i<=5;$i++) {
                                                $selected = "";

                                                $point = $i - $finalrate;

                                                if(!empty($finalrate) && $i<=$finalrate) {
                                                  ?>
                                                   <img alt="img" src="<?php echo base_url();?>images/review/star-full.png" style="margin-top:11px;"/>
                                                   <?php
                                                 } else if($point == '0.5' || $point < '0.5' || $point < '1') {
                                                  ?>
                                                  <img alt="img" src="<?php echo base_url();?>images/review/star-half.png" style="margin-top:11px;"/>
                                                   <?php 
                                                } else {
                                                  ?>
                                                  <img alt="img" src="<?php echo base_url();?>images/review/star-blank.png" style="margin-top:11px;"/>
                                                  <?php 
                                                }
                                               } 
                                            ?> 
                                            
                                            <?php if(mb_substr( $arv_rating, 0, 4 ) >= 4){
                                            ?><span class="" style="color:#16a085;"><?php
                                            }else if(mb_substr( $arv_rating, 0, 4 ) >= 3){
                                            ?><span class="" style="color:#1c9aea;"><?php
                                            }else{
                                            ?><span class="" style="color:red;"><?php    
                                            }?>
                                            (<?php echo mb_substr($arv_rating, 0, 4 ); ?>)</span>
                                    </div>
                                    <div class="over-table-block">
                                        
                                        <?php
                                        if($this->session->userdata('user_type')=='Buyer'){?>
                                        <a data-backdrop="static"  data-toggle="modal" class="sellid" data-sellid="<?php echo $seller['seller_id']; ?>" data-target="#myModal" type="button">Contact seller</a>
                                        <?php } ?>
                                        <a href="<?php echo base_url().'seller/profile/'.$seller['seller_id']; ?>">View Profile</a>
                                        <?php if(!empty($get_seller_last_uploaded_product[0]['id'])) { ?>
                                        <a href="<?php echo base_url().'seller/product_details/'.$get_seller_last_uploaded_product[0]['id'].'/'.$seller['seller_id']; ?>">View Product</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                        <?php
                        }
                     } ?>

                <?php } else {

                    $this->load->view('no-data-found');

                } ?>

                <?php if(empty($get_seller_last_uploaded_product)) { ?>   

                    <?php  foreach ($get_seller as $seller) {
                            if(isset($seller['seller_id'])) { $seller['seller_id']= $seller['seller_id']; } else { $seller['seller_id'] = $seller['id'];}

                            /* get seller last uploaded product */
                            $this->db->where('tbl_seller_upload_product.status' , 'Unblock');
                            $this->db->where('tbl_seller_upload_product.id' , $seller['id']);
                            $this->db->order_by('id' , 'desc');
                            $this->db->group_by('tbl_seller_upload_product.seller_id');
                            //$this->db->limit(1);
                            $get_seller_last_uploaded_product  = $this->master_model->getRecords('tbl_seller_upload_product');
                            ?>
                                <div class="col-sm-6 col-md-4 col-lg-4 over-block-new">
                                    <div class="item-block-main">
                                        <div class="item-img-block">

                                            <?php if(!empty($get_seller_last_uploaded_product[0]['req_image']) && file_exists('images/seller_upload_product_image/'.$get_seller_last_uploaded_product[0]['req_image'])){?>
                                            <img style="height:162px;width:262px;" src="<?php echo base_url().'images/seller_upload_product_image/'.$get_seller_last_uploaded_product[0]['req_image']; ?>" alt="list_img_1" class="" />
                                            <?php } else { ?>
                                            <img style="height:162px;width:262px;" src="<?php echo base_url().'images/seller_upload_product_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                                            <?php } ?> 
                                            
                                            <div class="profile-pic-listing">
                                                    <?php if(!empty($seller['user_image']) && file_exists('images/seller_image/'.$seller['user_image'])){?>
                                                    <img src="<?php echo base_url().'images/seller_image/'.$seller['user_image']; ?>" alt="list_img_1" class="img-responsive list_view_img_1" />
                                                    <?php } else { ?>
                                                    <img src="<?php echo base_url().'images/seller_image/default/default-user-img.jpg'; ?>" alt="" />
                                                    <?php } ?> 
                                            </div>
                                            
                                            <div class="price-categorys"><?php echo CURRENCY ?> 
                                            <?php  if(!empty($get_seller_last_uploaded_product[0]['price'])) { echo $get_seller_last_uploaded_product[0]['price'];} else{ echo "NA"; } ?>
                                            </div>
                                        </div>
                                        <div class="item-content">
                                            <div class="name-item-block">
                                                    <?php if(!empty($seller['name'])) {
                                                    echo $seller['name']; 
                                                    } else { 
                                                    echo " User name available"; 
                                                    } 
                                                    ?> 
                                            </div>
                                            <div class="semi-head-block">
                                                <?php  if(!empty($get_seller_last_uploaded_product[0]['title'])) { echo $get_seller_last_uploaded_product[0]['title'];} else{ echo "<span class='label label-important'><i class='fa fa-ban' aria-hidden='true'></i></span></span>  Product name is not available."; } ?>
                                            </div>
                                            <div class="item-content-block">
                                                 <?php if(!empty($get_seller_last_uploaded_product[0]['description'])) { echo substr($get_seller_last_uploaded_product[0]['description'], 0 , 52).'...'; } else { echo "<span class='label label-important'><i class='fa fa-ban' aria-hidden='true'></i></span> Product description is not available."; } ?>
                                            </div>
                                        </div>
                                        <div class="star-rating-block">
                                            <?php 
                                            $get_seller_reviews = 0;
                                            $ratings         = 0;
                                            $arv_rating      = 0;
                                            $compl_per       = 0;
                                            $completion_per  = 0;


                                            $arv_rating      = 0;       
                                            $compl_per       = 0;           
                                            $completion_per  = 0;

                                            $this->db->where('tbl_seller_rating.seller_id', $seller['seller_id']);
                                            $this->db->where('tbl_seller_rating.status !=' , 'Delete');
                                            $this->db->where('tbl_seller_rating.review_for', 'seller');
                                            $get_seller_reviews=$this->master_model->getRecords('tbl_seller_rating');
                                            if(empty($get_seller_reviews))
                                            {
                                            }
                                            else
                                            {
                                            foreach($get_seller_reviews as $field => $value)
                                            {
                                            $ratings       +=  $value['ratings'];
                                            }
                                            $arv_rating      = $ratings / count($get_seller_reviews);  
                                            $compl_per       = $arv_rating  * 100 / 5 ;          
                                            $completion_per  = mb_substr( $compl_per, 0, 4 ) ;  
                                            }  
                                            ?> 
                                          
                                                <?php
                                                    $finalrate = $chekRating = mb_substr($arv_rating,  0, 4);
                                                    for($i=1;$i<=5;$i++) {
                                                    $selected = "";

                                                    $point = $i - $finalrate;

                                                    if(!empty($finalrate) && $i<=$finalrate) {
                                                      ?>
                                                       <img alt="img" src="<?php echo base_url();?>images/review/star-full.png" style="margin-top:11px;"/>
                                                       <?php
                                                     } else if($point == '0.5' || $point < '0.5' || $point < '1') {
                                                      ?>
                                                      <img alt="img" src="<?php echo base_url();?>images/review/star-half.png" style="margin-top:11px;"/>
                                                       <?php 
                                                    } else {
                                                      ?>
                                                      <img alt="img" src="<?php echo base_url();?>images/review/star-blank.png" style="margin-top:11px;"/>
                                                      <?php 
                                                    }
                                                   } 
                                                ?> 
                                                
                                                <?php if(mb_substr( $arv_rating, 0, 4 ) >= 4){
                                                ?><span class="" style="color:#16a085;"><?php
                                                }else if(mb_substr( $arv_rating, 0, 4 ) >= 3){
                                                ?><span class="" style="color:#1c9aea;"><?php
                                                }else{
                                                ?><span class="" style="color:red;"><?php    
                                                }?>
                                                (<?php echo mb_substr($arv_rating, 0, 4 ); ?>)</span>
                                            </div>
                                            <div class="over-table-block">
                                            
                                            <?php
                                            if($this->session->userdata('user_type')=='Buyer'){?>
                                            <a data-backdrop="static"  data-toggle="modal" class="sellid" data-sellid="<?php echo $seller['seller_id']; ?>" data-target="#myModal" type="button">Contact seller</a>
                                            <?php } ?>
                                            <a href="<?php echo base_url().'seller/profile/'.$seller['seller_id']; ?>">View Profile</a>
                                            <?php if(!empty($get_seller_last_uploaded_product[0]['id'])) { ?>
                                            <a href="<?php echo base_url().'seller/product_details/'.$get_seller_last_uploaded_product[0]['id'].'/'.$seller['seller_id']; ?>">View Product</a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                            <?php
                            break;
                         } ?>

                <?php } ?>
                </div>
            </div>
             <div class="">
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>        
</div>    

<!-- Modal -->
<div id="myModal" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">Contact Seller</h4>
        </div>
        <div class="modal-body">
        <?php $this->load->view('status-msg'); ?>
         <div ng-controller="ContactSeller">  
            <form  
            id="contacttoseller"
            class=""
            name="contacttoseller" 
            enctype="multipart/form-data"
            novalidate 
            ng-submit="contacttoseller.$valid && contact_to_seller();"  >

            <input type="hidden" name="seller_id" id="seller_id" ng-model="user.seller_id" placeholder="Seller id">
           
            <div class="select-bock-container drop" ng-class="{'has-error':contacttoseller.category_id.$touched && contacttoseller.category_id.$invalid}" >
            <select name="category_id" 
                    ng-model="user.category_id"
                    ng-required="true">
                    <option value="">Category</option>
                    <?php foreach ($getCategory as $cate_value) {?>
                    <option value="<?php echo $cate_value['category_id']; ?>"><?php echo $cate_value['category_name']; ?></option>
                    <?php } ?>
            </select>
            <div class="error-new-block" ng-messages="contacttoseller.category_id.$error" ng-if="contacttoseller.$submitted || contacttoseller.category_id.$touched">
            <div>
            <div class="err_msg_div" style="display:none;">
                <p ng-message="required"    class="error">  This field is required</p>
                </div>
            </div>
            <script type="text/javascript">
                    $(document).ready(function(){
                      setTimeout(function(){
                        $('.err_msg_div').removeAttr('style');
                      },200);
                    });
                </script>
            </div>
           </div>
            <div class="mobile-nu-block enquiry" ng-class="{'has-error':contacttoseller.product_name.$touched && contacttoseller.product_name.$invalid}">
                <input type="text" 
                       name="product_name" 
                       class="beginningSpace_restrict" 
                       ng-model="user.product_name" 
                       placeholder="Enter product name"
                       ng-required="true"
                />
                <div class="error-new-block" ng-messages="contacttoseller.product_name.$error" ng-if="contacttoseller.$submitted || contacttoseller.product_name.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>
                <span class="highlight"></span>
                <label>Product Name</label>
            </div>
            <div class="mobile-nu-block enquiry" ng-class="{'has-error':contacttoseller.country.$touched && contacttoseller.country.$invalid}">
                <input type="text" 
                           name="country" 
                           class="beginningSpace_restrict" 
                           ng-model="user.country" 
                           placeholder="Enter country"
                           ng-required="true"
                           />
                <div class="error-new-block" ng-messages="contacttoseller.country.$error" ng-if="contacttoseller.$submitted || contacttoseller.country.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>

                <span class="highlight"></span>
                <label>Country</label>

            </div>
            
            <div class="mobile-nu-block input-first box-msg" ng-class="{'has-error':contacttoseller.description.$touched && contacttoseller.description.$invalid}">
                <textarea rows="" 
                              cols="" 
                              name="description" 
                              class="beginningSpace_restrict" 
                              ng-model="user.description"
                              ng-required="true"
                              ng-maxlength="1000"
                              placeholder="Enter offer description"></textarea>

                <span class="highlight"></span> 
                <label>Message</label>

                <div class="error-new-block" ng-messages="contacttoseller.description.$error" ng-if="contacttoseller.$submitted || contacttoseller.description.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    <p ng-message="maxlength"   class="error">  This field to long. Only 1000 characters allow.</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>

            </div>
            </div>
            <div class="modal-footer">
                <div class="btn-password changes enq">
                    <button class="change-btn-pass " type="submit">Send</button>
                </div>
            </div>
          </form>
       </div>
    </div>

</div>
</div>



<!-- Clustering  MAP -->
<?php
    if(count($userLatLng))
    {
        $phploopcnt =0;
        foreach($userLatLng as $row) 
        {
            if(!empty($row['latitude']) && !empty($row['longitude']))     
            {  
                echo "<input type='hidden' value=".'"'.$row['address'].'"'." id=".'addr_'.$phploopcnt.">";
                echo "<input type='hidden' value=".'"'.$row['user_image'].'"'." id=".'image_'.$phploopcnt.">";
            } 
            $phploopcnt++;
        }
    }
?>
<style>
#map {
height: 100%;
}
html, body {
height: 100%;
margin: 0;
padding: 0;
}
</style>
<div id="map" style="width: 1000px; height: 600px;"></div>
<script type="text/javascript" src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/markerclustererplus/src/markerclusterer.js"></script>
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCccvQtzVx4aAt05YnfzJDSWEzPiVnNVsY&callback=initMap">
</script>
<script>
var locations = [];
locations = [
    <?php   
        if(count($userLatLng))
        {
            foreach($userLatLng as $row) 
            {
                if(!empty($row['latitude']) && !empty($row['longitude']))     
                {  
                    ?>
                    {'lat':<?php echo $row['latitude']; ?>,'lng':<?php echo $row['longitude']; ?>},
                    <?php
                }
            }
        }
    ?>
];

<?php
    // find center   
    if(count($userLatLng))
    {
        foreach($userLatLng as $row) 
        {
            if(!empty($row['latitude']) && !empty($row['longitude']))     
            {  
                 $minlat = false;
                 $minlng = false;
                 $maxlat = false;
                 $maxlng = false;
                     if ($minlat === false) { $minlat = $row['latitude']; } else { $minlat = ($row['latitude'] < $minlat) ? $row['latitude'] : $minlat; }
                     if ($maxlat === false) { $maxlat = $row['latitude']; } else { $maxlat = ($row['latitude'] > $maxlat) ? $row['latitude'] : $maxlat; }
                     if ($minlng === false) { $minlng = $row['longitude']; } else { $minlng = ($row['longitude'] < $minlng) ? $row['longitude'] : $minlng; }
                     if ($maxlng === false) { $maxlng = $row['longitude']; } else { $maxlng = ($row['longitude'] > $maxlng) ? $row['longitude'] : $maxlng; }
                  // Calculate the center
                  $lat = $maxlat - (($maxlat - $minlat) / 2);
                  $lon = $maxlng - (($maxlng - $minlng) / 2);
            }
        }
    }
?>



function initMap() {
var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 3,
    center: {lat: <?php if(isset($lat)){ echo $lat; } else { echo '19.997453'; } ?>, lng: <?php if(isset($lon)){ echo $lon; } else { echo '73.789802'; } ?>}
});

var labels_arr = new Array('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
var infowindow = new google.maps.InfoWindow();

var loopcnt       = 0;
var markers = locations.map(function(location, i) {


    var text="";
    var marker = new google.maps.Marker({
        position: location,
        //icon: 'map-marker4.png',
        //label: labels_arr[i],
    });
    console.log(loopcnt);
    var address = $('#addr_'+loopcnt).val();
    var image   = $('#image_'+loopcnt).val();
    $.ajax ({
    url  :site_url+"search/check_file_exists",
    type :"post",
    async: false,
    data :{ user_profile : image} ,
    }).done(function(message){
   
        if(message == 'yes')
        {
            img = 'images/seller_image/'+image;
        }
        else if(message == 'no')
        {
            img = 'images/default/default-user-img.jpg';
        }

    });
    text = '<img src="'+img+'" style="height:200px;width:300px;"><br><div style="margin-top:15px;">'+address; 

    google.maps.event.addListener(marker, 'click', function(){
        infowindow.close();
        infowindow.setContent(text);
        infowindow.open(map,marker);
    });
    loopcnt++;
    return marker;
});

// Add a marker clusterer to manage the markers.
var markerCluster = new MarkerClusterer(map, markers,
    {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
}
</script>

<!-- END Clustering MAP -->









<script type="text/javascript">
$(document).ready(function() {
    $(".more-btn-block").click(function() {
        $(this).parent().find(".view-more-content").slideDown("slow");
        $(this).hide();
        $(this).parent().find(".hide-btn-block").show();
    });
    $(".hide-btn-block").click(function() {
        $(this).parent().find(".view-more-content").slideUp("slow");
        $(this).parent().find(".more-btn-block").show();
        $(this).hide();
    });

    // footer dropdown links start 
    var min_applicable_width = 767;

    $(document).ready(function() {
        applyResponsiveSlideUp($(this).width(), min_applicable_width);
    });

    function applyResponsiveSlideUp(current_width, min_applicable_width) {
        /* Set For Initial Screen */
        initResponsiveSlideUp(current_width, min_applicable_width);

        /* Listen Window Resize for further changes */
        $(window).bind('resize', function() {
            if ($(this).width() <= min_applicable_width) {
                unbindResponsiveSlideup();
                bindResponsiveSlideup();
            } else {
                unbindResponsiveSlideup();
            }
        });
    }

    function initResponsiveSlideUp(current_width, min_applicable_width) {
        if (current_width <= min_applicable_width) {
            unbindResponsiveSlideup();
            bindResponsiveSlideup();
        } else {
            unbindResponsiveSlideup();
        }
    }

    function bindResponsiveSlideup() {
        $(".menu_name2").hide();
        $(".footer_heading2").bind('click', function() {
            var $ans = $(this).parent().find(".menu_name2");
            $ans.slideToggle();
            $(".menu_name2").not($ans).slideUp();
            $('.menu_name2').removeClass('active');

            $('.footer_heading2').not($(this)).removeClass('active');
            $(this).toggleClass('active');
            $(this).parent().find(".menu_name2").toggleClass('active');
        });
    }

    function unbindResponsiveSlideup() {
        $(".footer_heading2").unbind('click');
        $(".menu_name2").show();
    }
    // footer dropdown links end

});
</script>
<script type="text/javascript">
    $(document).ready(function() { 
      $('.close').on('click',function() {
        setTimeout(function(){
            location.reload();
        })
      });
    });
</script>



<link rel="stylesheet" href="<?php echo base_url(); ?>assets/giocomplete/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/giocomplete/jquery-1.12.4.js"></script>
<script src="<?php echo base_url(); ?>assets/giocomplete/jquery-ui.js"></script>
<script>
$( function() {
var availableTags = [
    <?php 
    $this->db->select('title');
    $this->db->where('tbl_seller_upload_product.status' , 'Unblock');;
    $this->db->group_by('title'); 
    $this->db->from('tbl_seller_upload_product');
    $product_list = $this->db->get();
    $product_res  = $product_list->result();


    $this->db->select('name');
    $this->db->where('tbl_user_master.status' , 'Unblock');
    $this->db->where('tbl_user_master.user_type' , 'Seller');
    $this->db->where('tbl_user_master.verification_status' , 'Verified');
    $this->db->group_by('name'); 
    $this->db->from('tbl_user_master');
    $user_list = $this->db->get();
    $user_res  = $user_list->result();


    if(count($product_list)>0 || count($user_list)>0 )
    {
      foreach($product_res as $key => $product)
      {
         echo '"'.$product->title.'",';
      }
      foreach($user_res as $key => $user)
      {
         echo '"'.$user->name.'",';
      }
    }
    ?>
];
$( "#seller" ).autocomplete({
  source: availableTags, minLength:1
});
});
</script>



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLKCmJEIrOrzdRklZHYer8q9qx2XLJ4Vs&sensor=false&libraries=places"></script>
<script src="<?php echo base_url(); ?>assets/js/geolocator/jquery.geocomplete.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var location = "";
          $("#location").geocomplete({
            details: ".geo-details",
            detailsAttribute: "data-geo",
            location:location,
            types:[]
        });
    });
</script>    

