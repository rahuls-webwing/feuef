<style type="text/css">
.pagination>li>a, .pagination>li>span {
    position: relative;
    float: left;
    top: -40px;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #ddd;
}
.label-important, .badge-important {
        background-color: #fe1010;
}
body {
    letter-spacing: 0px;
}
.mobile-block {
     text-align: left; 
}


</style>
<div class="page-head-block listing-search">
<div class="page-head-overlay"></div>
<div class="textbx-block-search">

    <button type="submit" style="width: 97%;" id="search_listing" class="search-btn-home">
       <?php if(isset($getRequirmentsDetail[0]['title'])) 
       { echo substr($getRequirmentsDetail[0]['title'], 0 , 20); if(strlen($getRequirmentsDetail[0]['title']) > 20){ echo "..."; } } else { echo 'Unknown'; } ?> details
   </button>
   

</div>
</div>

<div class="middel-container">
<div class="inner-content-block">
    <div class="container">
        <div class="row">
             <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="block-filter-container">
                    <div class="sort-by-block">
                        <span></span>
                    </div>
                    <div class="clr"></div>
                </div>
                <div class="row mobile-block">
                
                <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="back-btn-main-block">
                    <!-- <div class="bck-btn">
                        <a href="#"><i class="fa fa-angle-double-left"></i> Back</a>
                    </div> -->
                    <div class="replay-btn">
                        <a class="btn-replay-block" href="javascript:history.back()"><i class="fa fa-reply"></i> back</a>
                        <!-- <a class="btn-delete-block" href="#"><i class="fa fa-trash-o"></i> Delete</a> -->
                    </div>
                    <div class="clr"></div>
                </div>
                    <div style="padding-left:10px;padding-right:10px; padding-top:10px;">
                    </div>
                    <div class="account-info-block">                        

                    <?php if(isset($getRequirmentsDetail) && sizeof($getRequirmentsDetail) > 0) {?>

                        <?php foreach ($getRequirmentsDetail as $value) {
                            ?>
                                <div class="main-img-block">
                                    <div class="row">
                                        <div class="col-sm-3 col-md-3 col-lg-3">
                                            <div class="">

                                                <?php if(!empty($value['req_image']) && file_exists('images/buyer_post_requirment_image/'.$value['req_image'])){?>
                                                    <img style="height:162px;width:260px;"  src="<?php echo base_url().'images/buyer_post_requirment_image/'.$value['req_image']; ?>" alt="list_img_1" class="img-responsive list_view_img_1" />
                                                <?php } else { ?>
                                                <img src="<?php echo base_url().'images/buyer_post_requirment_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                                                <?php } ?> 

                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-md-4 col-lg-9">
                                            <div class="event_news_txt ">
                                                <div class="descrip-head-block">
                                                    
                                                </div>
                                                 <div class="head_txt">
                                                  <a style="cursor:text;"><?php if(!empty($value['title'])) { echo ucfirst($value['title']); } else { echo "Not Available" ;} ?></a>
                                                </div>
                                                <div class="list_up"><span><i class="fa fa-folder-open"></i>
                                                    </span>
                                                         <?php if(!empty($value['category_name'])) { echo $value['category_name']; } else { echo "Not Available"; } ?> ->
                                                         <?php if(!empty($value['subcategory_name'])) { echo $value['subcategory_name']; } else { echo "Not Available"; } ?>
                                                </div>                                                
                                                <div class="list_add"><span><i class="fa fa-map-marker"></i>
                                                    </span>
                                                    <?php if(!empty($value['location'])) { echo $value['location']; } else { echo "Not Available"; } ?> 

                                                </div>

                                                <div class="lsit_text_content">
                                                <div class="list_up "><span><i class="fa fa-calendar" aria-hidden="true"></i> Posted Date :
                                                    </span>
                                                         <?php echo date('F j, Y', strtotime($value['created_date']))." at ".date("g:i a", strtotime($value['created_date'])); ?> 
                                                </div>
                                                </div>
                                                
                                                <div class="lsit_text_content">
                                                <div class="list_up up-list-price"><span> <?php echo CURRENCY; ?>
                                                    </span>
                                                         <?php if(!empty($value['price'])) { echo $value['price']; } else { echo "Not Available"; } ?> 
                                                </div>
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="lsit_text_content" style="text-align:justify;">
                                                   <?php if(!empty($value['description'])) { echo $value['description']; } else { echo "Not Available"; } ?>.. ...
                                                    <div class="clr"></div>
                                                </div>


                                           
                                                <?php if(!empty($this->session->userdata('user_id')) && $this->session->userdata('user_type') == "Seller"){ ?>
                                                <a data-toggle="modal"  data-backdrop="static" data-reqid="<?php echo $value['id'] ?>" data-target="#myReqModal" class="send-btn-request let_require pull-right"><span>Make Offer</span></a>
                                                <?php } else {?>
                                                <?php } ?>

                                            </div>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                </div>

                            <?php
                        }
                    } else {
                        $this->load->view('no-data-found');
                    }?>
                    </div>
                    </div>
                 </div>
                </div>
            </div>
        </div>
    </div>
</div>        
</div>    


<!-- Requirment Modal -->
<div id="myReqModal" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">Make Offer</h4>
        </div>
        <div class="modal-body">
        <?php $this->load->view('status-msg'); ?>
        <div ng-controller="OfferToRequirmentCntrl">  
            <form  
                id="makeofferForm"
                class=""
                name="makeofferForm" 
                enctype="multipart/form-data"
                novalidate 
                ng-submit="makeofferForm.$valid && requirment_offer();"  >

                    <input type="hidden" name="requirment_id" id="requirment_id" ng-model="user.requirment_id" placeholder="requirment_id">
                   
                    <div class="mobile-nu-block enquiry" ng-class="{'has-error':makeofferForm.price.$touched && makeofferForm.price.$invalid}">
                            <input type="text" 
                                   name="price" 
                                   class="beginningSpace_restrict " 
                                   ng-model="user.price"
                                   ng-required="true"
                                   placeholder="Enter price"/>
                        <div class="error-new-block" ng-messages="makeofferForm.price.$error" ng-if="makeofferForm.$submitted || makeofferForm.price.$touched">
                        <div>
                        <div class="err_msg_div" style="display:none;">
                            <p ng-message="required"    class="error">  This field is required</p>
                            </div>
                        </div>
                        <script type="text/javascript">
                                $(document).ready(function(){
                                  setTimeout(function(){
                                    $('.err_msg_div').removeAttr('style');
                                  },200);
                                });
                            </script>
                        </div>

                        <span class="highlight"></span>
                        <label>Unit price (<?php echo CURRENCY; ?>)</label>

                    </div>


                    <div class="mobile-nu-block enquiry" ng-class="{'has-error':makeofferForm.country.$touched && makeofferForm.country.$invalid}">
                            <input type="text" 
                                   name="country" 
                                   class="beginningSpace_restrict" 
                                   ng-model="user.country" 
                                   placeholder="Enter country"
                                   ng-required="true"
                                   />
                        <div class="error-new-block" ng-messages="makeofferForm.country.$error" ng-if="makeofferForm.$submitted || makeofferForm.country.$touched">
                        <div>
                        <div class="err_msg_div" style="display:none;">
                            <p ng-message="required"    class="error">  This field is required</p>
                            </div>
                        </div>
                        <script type="text/javascript">
                                $(document).ready(function(){
                                  setTimeout(function(){
                                    $('.err_msg_div').removeAttr('style');
                                  },200);
                                });
                            </script>
                        </div>

                        <span class="highlight"></span>
                        <label>Country</label>

                    </div>
                    
                    <div class="mobile-nu-block input-first box-msg" ng-class="{'has-error':makeofferForm.description.$touched && makeofferForm.description.$invalid}">
                        <textarea rows="" 
                                      cols="" 
                                      name="description" 
                                      class="beginningSpace_restrict" 
                                      ng-model="user.description"
                                      ng-required="true"
                                      ng-maxlength="500"
                                      placeholder="Enter offer description"></textarea>

                        <span class="highlight"></span> 
                        <label>Message</label>

                        <div class="error-new-block" ng-messages="makeofferForm.description.$error" ng-if="makeofferForm.$submitted || makeofferForm.description.$touched">
                        <div>
                        <div class="err_msg_div" style="display:none;">
                            <p ng-message="required"    class="error">  This field is required</p>
                            <p ng-message="maxlength"   class="error">  This field to long. Only 1000 characters allow.</p>
                            </div>
                        </div>
                        <script type="text/javascript">
                                $(document).ready(function(){
                                  setTimeout(function(){
                                    $('.err_msg_div').removeAttr('style');
                                  },200);
                                });
                            </script>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-password changes enq">
                        <button class="change-btn-pass" type="submit">Send</button>
                    </div>
                </div>
            </form>    
        </div>

    </div>
</div>
</div>
<!-- End Requirment Modal -->

<script type="text/javascript">
    $(document).ready(function() { 
      $('.close').on('click',function() {
        setTimeout(function(){
            location.reload();
        })
      });
    });
</script>


<script type="text/javascript">
$(document).ready(function() {
    $(".more-btn-block").click(function() {
        $(this).parent().find(".view-more-content").slideDown("slow");
        $(this).hide();
        $(this).parent().find(".hide-btn-block").show();
    });
    $(".hide-btn-block").click(function() {
        $(this).parent().find(".view-more-content").slideUp("slow");
        $(this).parent().find(".more-btn-block").show();
        $(this).hide();
    });

    // footer dropdown links start 
    var min_applicable_width = 767;

    $(document).ready(function() {
        applyResponsiveSlideUp($(this).width(), min_applicable_width);
    });

    function applyResponsiveSlideUp(current_width, min_applicable_width) {
        /* Set For Initial Screen */
        initResponsiveSlideUp(current_width, min_applicable_width);

        /* Listen Window Resize for further changes */
        $(window).bind('resize', function() {
            if ($(this).width() <= min_applicable_width) {
                unbindResponsiveSlideup();
                bindResponsiveSlideup();
            } else {
                unbindResponsiveSlideup();
            }
        });
    }

    function initResponsiveSlideUp(current_width, min_applicable_width) {
        if (current_width <= min_applicable_width) {
            unbindResponsiveSlideup();
            bindResponsiveSlideup();
        } else {
            unbindResponsiveSlideup();
        }
    }

    function bindResponsiveSlideup() {
        $(".menu_name2").hide();
        $(".footer_heading2").bind('click', function() {
            var $ans = $(this).parent().find(".menu_name2");
            $ans.slideToggle();
            $(".menu_name2").not($ans).slideUp();
            $('.menu_name2').removeClass('active');

            $('.footer_heading2').not($(this)).removeClass('active');
            $(this).toggleClass('active');
            $(this).parent().find(".menu_name2").toggleClass('active');
        });
    }

    function unbindResponsiveSlideup() {
        $(".footer_heading2").unbind('click');
        $(".menu_name2").show();
    }
    // footer dropdown links end

});

</script>
