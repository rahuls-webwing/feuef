<div class="col-sm-4 col-md-3 col-lg-3">
<form id="serach_critearea" action="<?php echo base_url().'search/requirments'; ?>" method="GET">
    <input type="hidden" id="hedden_search" value="<?php  if(isset($_REQUEST['requirment']) && $_REQUEST['requirment'] != ""){ echo $_REQUEST['requirment']; } ?>" name="requirment">
    

    <div class="by-location-block">
        <div class="block-head-txt">
            By Location
        </div>
        <div class="by-location-input">
            <input type="text" name="location" id="location" value="<?php  if(isset($_REQUEST['location']) && $_REQUEST['location'] != ""){ echo $_REQUEST['location']; } ?>" placeholder="Enter the location" />
            <span><i class="fa fa-map-marker"></i></span>
        </div>
    </div>


    <div class="by-location-block">
        <div class="footer_heading2 block-head-txt">
            By Category
        </div>
        

        <div class="menu_name2 check-bx-main">

            <?php if(isset($getCategory) && sizeof($getCategory) > 0) { ?>

                <?php $cnt=0; foreach($getCategory as $category) { $cnt++;?>

                    <?php if($cnt <= 6){ ?>
                        <div class="check-bx">
                            <input class="css-checkbox cate_chk" 
                                   <?php if(isset($_REQUEST['cat_id']) &&  in_array($category['category_id'], $_REQUEST['cat_id'])) { echo 'checked'; } ?>
                                   id="radio<?php echo $category['category_id']; ?>" 
                                   value="<?php echo $category['category_id']; ?>" 
                                   name="cat_id[]" 
                                   type="checkbox">

                            <label class="css-label radGroup2" 
                                   for="radio<?php echo $category['category_id']; ?>">
                                   <?php echo $category['category_name']; ?>
                            </label>
                        </div>
                    <?php } ?>

                            <?php if($cnt > 6){ ?>

                                <?php if(isset($_REQUEST['cat_id']) &&  in_array($category['category_id'], $_REQUEST['cat_id'])) {  } else { echo '<div class="view-more-content">';} ?>
                                    
                                        <div class="check-bx">
                                        <input class="css-checkbox cate_chk" 
                                               <?php if(isset($_REQUEST['cat_id']) &&  in_array($category['category_id'], $_REQUEST['cat_id'])) { echo 'checked'; } ?>
                                               id="radio<?php echo $category['category_id']; ?>" 
                                               value="<?php echo $category['category_id']; ?>" 
                                               name="cat_id[]" 
                                               type="checkbox">

                                        <label class="css-label radGroup2" 
                                               for="radio<?php echo $category['category_id']; ?>">
                                               <?php echo $category['category_name']; ?>
                                        </label>                                        </div>
                                    
                                        <?php if(isset($_REQUEST['cat_id']) &&  in_array($category['category_id'], $_REQUEST['cat_id'])) {  } else { echo '</div>'; } ?>

                                    
                            <?php } ?>

                <?php } ?>
                <?php if($cnt > 6){ echo '<a class="more-btn-block" href="javascript:void(0);">More..</a>
                                <a href="javascript:void(0);" class="hide-btn-block">Hide</a>
                                <div class="clr"></div>'; } ?>
                    
            <?php } else {

            $this->load->view('no-data-found');

            }?>


        </div>
    </div>


    <!-- <div class="by-location-block">
        <div class="footer_heading2 block-head-txt">
            By Rating
        </div>
        <div class="menu_name2 radio-btn-main">
            <div class="radio-btns">
                <div class="radio-btn">
                    <input type="radio" id="f-option" name="selector">
                    <label for="f-option">All</label>
                    <div class="check"></div>
                </div>
            </div>
            <div class="radio-btns">
                <div class="radio-btn">
                    <input type="radio" id="k-option" name="selector">
                    <label for="k-option">Low To High</label>
                    <div class="check"></div>
                </div>
            </div>
            <div class="radio-btns">
                <div class="radio-btn">
                    <input type="radio" id="l-option" name="selector">
                    <label for="l-option">High To Low</label>
                    <div class="check"></div>
                </div>
            </div>
        </div>
    </div> -->


</form>


</div>

<script type="text/javascript">
    
    $(document).bind('keypress', function(e) {
      if(e.keyCode==13){
        $('#serach_critearea').submit();   
       }
    });  
    $('#location').on('blur', function() {
        setTimeout(function(){
          if($('#location').val() != ""){
             $('#serach_critearea').submit();   
          }  
        },100);
    }); 
    $('.cate_chk').on('click', function() {
        setTimeout(function(){
          $('#serach_critearea').submit();   
        },0);
    });
    $('#search_listing').on('click', function() {
        setTimeout(function(){
          $('#serach_critearea').submit();   
        },0);
    });
    $('#requirment').keyup(function() {
        var search_text = $(this).val();
        $('#hedden_search').val(search_text);
    });
    $('#requirment').blur(function() {
        var search_text = $(this).val();
        $('#hedden_search').val(search_text);
    });
</script>
