<style type="text/css">
.pagination>li>a, .pagination>li>span {
    position: relative;
    float: left;
    top: -40px;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #ddd;
}
.label-important, .badge-important {
        background-color: #fe1010;
}
body {
    letter-spacing: 0px;
}
.mobile-block {
     text-align: left; 
}
</style>
<?php 
if($requirment_count > 9){
    ?>
    <style>
        .block-filter-container {
        margin-bottom: -46px;
        }
    </style>
    <?php
}
?>
<div class="page-head-block listing-search">
<div class="page-head-overlay"></div>
<div class="textbx-block-search">
 
    <input type="text" name="requirment" id="requirment" value="<?php  if(isset($_REQUEST['requirment']) && $_REQUEST['requirment'] != ""){ echo $_REQUEST['requirment']; } ?>" placeholder="Search Requirements......." />
    <button type="submit" id="search_listing" class="search-btn-home">Search</button>
    <span class="error-msg">Please enter what type of work.</span>
    <div class="clr"></div>

</div>
</div>
<div class="middel-container">
<div class="inner-content-block">
    <div class="container">
         <?php $this->load->view('status-msg'); ?>
        <div class="row">
             <?php $this->load->view('search/requirment/search-critearea'); ?>
             <div class="col-sm-8 col-md-9 col-lg-9">
                <div class="block-filter-container">
                    <div class="sort-by-block">
                        <span>Requirements</span>
                    </div>
                    <div class="">
                        <?php echo $this->pagination->create_links(); ?>
                    </div>
                    <div class="clr"></div>
                </div>
                <div class="row mobile-block">
                
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <?php $this->load->view('status-msg'); ?>
                    <div class="account-info-block">                        

                    <?php if(isset($getRequirments) && sizeof($getRequirments) > 0) {?>

                        <?php foreach ($getRequirments as $value) {
                            $get_buyer_info = $this->master_model->getRecords('tbl_user_master',array('id'=>$value['buyer_id']));
                            ?>
                                <div class="main-img-block">
                                    <div class="row">
                                        <div class="col-sm-4 col-md-4 col-lg-4">
                                            <div class="">

                                                <?php if(!empty($value['req_image']) && file_exists('images/buyer_post_requirment_image/'.$value['req_image'])){?>
                                                    <img style="width:260px;height:162px;" src="<?php echo base_url().'images/buyer_post_requirment_image/'.$value['req_image']; ?>" alt="list_img_1" class="img-responsive list_view_img_1" />
                                                <?php } else { ?>
                                                <img src="<?php echo base_url().'images/buyer_post_requirment_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                                                <?php } ?> 

                                            </div>
                                        </div>
                                        <div class="col-sm-8 col-md-8 col-lg-8">
                                            <div class="event_news_txt">
                                                <div class="head_txt">
                                                  <a href="<?php echo base_url().'search/requirment_detail/'.$value['id'];?>"><?php if(!empty($value['title'])) { echo ucfirst($value['title']); } else { echo "Not Available" ;} ?></a>
                                                </div>
                                                <div class="list_up"><span><i class="fa fa-folder-open"></i>
                                                    </span>
                                                         <?php if(!empty($value['category_name'])) { echo substr($value['category_name'], 0 , 50); } else { echo "Not Available"; } ?> ->
                                                         <?php if(!empty($value['subcategory_name'])) { echo substr($value['subcategory_name'], 0 , 50); } else { echo "Not Available"; } ?>
                                                </div>                                                
                                                <div class="list_add"><span><i class="fa fa-map-marker"></i>
                                                    </span>
                                                    <?php if(!empty($value['location'])) { echo substr($value['location'], 0 , 50); } else { echo "Not Available"; } ?> 

                                                </div>

                                                <div class="lsit_text_content">
                                                <div class="list_up "><span><i class="fa fa-calendar" aria-hidden="true"></i> Posted Date :
                                                    </span>
                                                         <?php echo date('F j, Y', strtotime($value['created_date']))." at ".date("g:i a", strtotime($value['created_date'])); ?> 
                                                </div>
                                                </div>

                                                <div class="lsit_text_content">
                                                <span><i class="fa fa-user" aria-hidden="true"></i> Buyer Name :
                                                </span>
                                                     <?php if(!empty($get_buyer_info[0]['name'])) { echo $get_buyer_info[0]['name']; } else { echo "Not Available"; } ?>
                                                </div>

                                                <!-- <div class="lsit_text_content">
                                                <span><i class="fa fa-envelope-o" aria-hidden="true"></i> Buyer Email :
                                                </span>
                                                     <?php if(!empty($get_buyer_info[0]['email'])) { 
                                                      ?><a href="mailto:<?php echo $get_buyer_info[0]['email']; ?>?Subject=Inquiry" class="stop_loader" target="_top"><?php echo $get_buyer_info[0]['email']; ?></a><?php
                                                     } else { 

                                                      echo "Not Available"; } ?>
                                                    
                                                </div> -->
                                                
                                                <div class="lsit_text_content">
                                                <div class="list_up up-list-price"><span> <?php echo CURRENCY; ?>
                                                    </span>
                                                         <?php if(!empty($value['price'])) { echo $value['price']; } else { echo "Not Available"; } ?> 
                                                </div>
                                                </div>

                                                <div class="lsit_text_content">
                                                   <?php if(!empty($value['description'])) { echo substr($value['description'], 0 , 300); } else { echo "Not Available"; } ?>.. ...
                                                    <div class="clr"></div>
                                                </div>
                                            </div>

                                            <?php if(!empty($this->session->userdata('user_id') && $this->session->userdata('user_type')=='Seller')) {?>
                                                <!-- <a data-backdrop="static" data-buyer_id="<?php echo $value['buyer_id']; ?>" data-toggle="modal" data-target="#add_to_contact_seller" type="button" class="send-inquiry-btn buyer_id">Add to Contact</a> -->
                                            
                                                <a data-buyerid="<?php echo $value['buyer_id']; ?>" type="button" class="send-inquiry-btn add_buyer_in_contact">Add to Contact</a>

                                            <?php } ?>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                </div>

                            <?php
                        }
                    } else {
                        $this->load->view('no-data-found');
                    }?>
                    </div>
                    </div>
                </div>


                </div>
            </div>
          

        </div>
    </div>
</div>        
</div>    
<script type="text/javascript">

$(document).ready(function(){
 
 $('.add_buyer_in_contact').click(function(){

    var buyer_id      = $(this).data('buyerid');

    ajaxindicatorstart();
    jQuery.ajax({
    url: site_url+'seller/add_buyer_to_my_contact',
    type:"post",
    dataType : 'json',
    data:{

          buyer_id      :buyer_id,
    },
    success:function(response){
      ajaxindicatorstop();


  
      if(response.contact_status == "success") {
        jQuery("#status_msg").html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.contact_msg+' </div>');
      } else if(response.contact_status == "error") {
        jQuery("#status_msg").html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.contact_msg+' </div>');
      }

    }
    });

 });

});
</script>
<!-- Modal -->
<div id="myModal" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">Send Enquiry</h4>
        </div>
        <div class="modal-body">
            <div class="mobile-nu-block enquiry">
                <input type="text" name="Mobile No" required />
                <span class="highlight"></span>
                <label>Name</label>
            </div>
            <div class="mobile-nu-block enquiry">
                <input type="text" name="Mobile No" required />
                <span class="highlight"></span>
                <label>Email</label>
            </div>
            <div class="mobile-nu-block enquiry">
                <input type="text" name="Mobile No" required />
                <span class="highlight"></span>
                <label>Phone</label>
            </div>
            <div class="select-bock-container drop">
                <select>
                    <option>Product Category</option>
                    <option>Fruite</option>
                    <option>dry fruits</option>
                </select>
            </div>
            <div class="mobile-nu-block enquiry">
                <input type="text" name="Mobile No" required />
                <span class="highlight"></span>
                <label>Product Name</label>
            </div>
            
            <div class="mobile-nu-block input-first box-msg">
                        <textarea cols="" rows=""> </textarea>
                        <span class="highlight"></span>
                        <label>Message</label>
                    </div>
        </div>
        <div class="modal-footer">
            <div class="btn-password changes enq">
                <button class="change-btn-pass " type="button">Send</button>
            </div>
        </div>
    </div>

</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $(".more-btn-block").click(function() {
        $(this).parent().find(".view-more-content").slideDown("slow");
        $(this).hide();
        $(this).parent().find(".hide-btn-block").show();
    });
    $(".hide-btn-block").click(function() {
        $(this).parent().find(".view-more-content").slideUp("slow");
        $(this).parent().find(".more-btn-block").show();
        $(this).hide();
    });

    // footer dropdown links start 
    var min_applicable_width = 767;

    $(document).ready(function() {
        applyResponsiveSlideUp($(this).width(), min_applicable_width);
    });

    function applyResponsiveSlideUp(current_width, min_applicable_width) {
        /* Set For Initial Screen */
        initResponsiveSlideUp(current_width, min_applicable_width);

        /* Listen Window Resize for further changes */
        $(window).bind('resize', function() {
            if ($(this).width() <= min_applicable_width) {
                unbindResponsiveSlideup();
                bindResponsiveSlideup();
            } else {
                unbindResponsiveSlideup();
            }
        });
    }

    function initResponsiveSlideUp(current_width, min_applicable_width) {
        if (current_width <= min_applicable_width) {
            unbindResponsiveSlideup();
            bindResponsiveSlideup();
        } else {
            unbindResponsiveSlideup();
        }
    }

    function bindResponsiveSlideup() {
        $(".menu_name2").hide();
        $(".footer_heading2").bind('click', function() {
            var $ans = $(this).parent().find(".menu_name2");
            $ans.slideToggle();
            $(".menu_name2").not($ans).slideUp();
            $('.menu_name2').removeClass('active');

            $('.footer_heading2').not($(this)).removeClass('active');
            $(this).toggleClass('active');
            $(this).parent().find(".menu_name2").toggleClass('active');
        });
    }

    function unbindResponsiveSlideup() {
        $(".footer_heading2").unbind('click');
        $(".menu_name2").show();
    }
    // footer dropdown links end

});

</script>



<script type="text/javascript">
$(document).ready(function(){
$('.stop_loader').click(function(){
setTimeout(function(){
ajaxindicatorstop();
},2000)
});
}); 
</script>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/giocomplete/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/giocomplete/jquery-1.12.4.js"></script>
<script src="<?php echo base_url(); ?>assets/giocomplete/jquery-ui.js"></script>
<script>
$( function() {
var availableTags = [
    <?php 
    $this->db->select('title');
    $this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
    $this->db->where('tbl_buyer_post_requirement.status <>' , 'Delete');
    $this->db->where('tbl_buyer_post_requirement.requirment_status' , 'open');
    $this->db->group_by('title'); 
    $this->db->from('tbl_buyer_post_requirement');
    //$this->db->limit(2);
    $user_list = $this->db->get();
    $user_res  = $user_list->result();
    if(count($user_list)>0)
    {
      foreach($user_res as $key => $user)
      {
         echo '"'.$user->title.'",';
      }
    }
    ?>
];
$( "#requirment" ).autocomplete({
  source: availableTags, minLength:1
});
});
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLKCmJEIrOrzdRklZHYer8q9qx2XLJ4Vs&sensor=false&libraries=places"></script>
<script src="<?php echo base_url(); ?>assets/js/geolocator/jquery.geocomplete.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var location = "";
          $("#location").geocomplete({
            details: ".geo-details",
            detailsAttribute: "data-geo",
            location:location,
            types:[]
        });
    });
</script>

<!-- Contact Button Popup form starts -->
<!-- Modal -->
<div id="add_to_contact_seller" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">Add to Contact</h4>
        </div>
        <div class="modal-body">
        <?php $this->load->view('status-msg1'); ?>
         <div ng-controller="AddtoContactSeller">  
            <form  
            id="addtocontactsellerseller"
            class=""
            name="addtocontactseller" 
            enctype="multipart/form-data"
            novalidate 
            ng-submit="addtocontactseller.$valid && add_to_contact_seller();"  >

            <input type="hidden" name="buyer_id" id="buyer_id"  ng-model="user.buyer_id">
           
            <div class="mobile-nu-block enquiry" ng-class="{'has-error':addtocontactseller.name.$touched && addtocontactseller.name.$invalid}">
                <input type="text" 
                       name="name" 
                       class="beginningSpace_restrict" 
                       ng-model="user.name" 
                       placeholder="Enter Your Name"
                       ng-required="true"
                />
                <div class="error-new-block" ng-messages="addtocontactseller.name.$error" ng-if="addtocontactseller.$submitted || addtocontactseller.name.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>
                <span class="highlight"></span>
                <label>Name</label>
            </div>

            <div class="mobile-nu-block enquiry" ng-class="{'has-error':addtocontactseller.email.$touched && addtocontactseller.email.$invalid}">
                <input type="text" 
                       name="email" 
                       class="beginningSpace_restrict" 
                       ng-model="user.email" 
                       placeholder="Enter Your Email"
                       ng-required="true"
                />
                <div class="error-new-block" ng-messages="addtocontactseller.email.$error" ng-if="addtocontactseller.$submitted || addtocontactseller.email.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>
                <span class="highlight"></span>
                <label>Email</label>
            </div>

            <div class="mobile-nu-block enquiry" ng-class="{'has-error':addtocontactseller.mobile_number.$touched && addtocontactseller.mobile_number.$invalid}">
                <input type="text" 
                       name="mobile_number" 
                       class="beginningSpace_restrict" 
                       ng-model="user.mobile_number" 
                       placeholder="Enter Your Contact No."
                       ng-required="true"
                />
                <div class="error-new-block" ng-messages="addtocontactseller.mobile_number.$error" ng-if="addtocontactseller.$submitted || addtocontactseller.mobile_number.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>
                <span class="highlight"></span>
                <label>Contact No.</label>
            </div>

            <div class="mobile-nu-block enquiry" ng-class="{'has-error':addtocontactseller.country.$touched && addtocontactseller.country.$invalid}">
                <input type="text" 
                           name="country" 
                           class="beginningSpace_restrict" 
                           ng-model="user.country" 
                           placeholder="Enter Your Country"
                           ng-required="true"
                           />
                <div class="error-new-block" ng-messages="addtocontactseller.country.$error" ng-if="addtocontactseller.$submitted || addtocontactseller.country.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>

                <span class="highlight"></span>
                <label>Country</label>
            </div>

            </div>
            <div class="modal-footer">
                <div class="btn-password changes enq">
                    <button class="change-btn-pass " type="submit">Send</button>
                </div>
            </div>
          </form>
       </div>
    </div>

</div>
</div>
</div>
