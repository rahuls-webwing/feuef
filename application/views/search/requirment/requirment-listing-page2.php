<style type="text/css">
.pagination>li>a, .pagination>li>span {
    position: relative;
    float: left;
    top: -40px;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #ddd;
}
.label-important, .badge-important {
        background-color: #fe1010;
}
body {
    letter-spacing: 0px;
}
</style>
</style>
<?php 
if($requirment_count > 9){
    ?>
    <style>
        .block-filter-container {
        margin-bottom: -46px;
        }
    </style>
    <?php
}
?>
<!-- Multi Select CSS -->
<link href="<?php echo base_url();?>front-asset/css/select2.min.css" rel="stylesheet" type="text/css" />
<div id="listing-header"></div>

<div class="listing-section">
    <div class="container-fluid">
        <div class="row">
            <!--list view start here-->
            <form id="serach_critearea" action="<?php echo base_url().'search/requirments'; ?>" method="GET">

            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="box-listing space-top-gowed">
                 <div class="row">


                    <div class="col-sm-3 col-md-2 col-lg-2">
                        <div class="user-one locations-input">
                            <input type="hidden" id="hedden_search" value="<?php  if(isset($_REQUEST['requirment']) && $_REQUEST['requirment'] != ""){ echo $_REQUEST['requirment']; } ?>" name="requirment">
                            <input  class="con-input" name="requirment" id="requirment" value="<?php  if(isset($_REQUEST['requirment']) && $_REQUEST['requirment'] != ""){ echo $_REQUEST['requirment']; } ?>" placeholder="Search Requirment......." type="text" />
                            <div class="locations-input"><i class="fa fa-user-o" aria-hidden="true"></i></div> 
                        </div>
                    </div>


                    <div class="col-sm-3 col-md-2 col-lg-2">
                        <div class="user-one locations-input">
                            <input  class="con-input" name="location" id="location" value="<?php  if(isset($_REQUEST['location']) && $_REQUEST['location'] != ""){ echo $_REQUEST['location']; } ?>"  placeholder="Enter the location" type="text" />
                            <div class="locations-input"><i class="fa fa-map-marker" aria-hidden="true"></i></div> 
                        </div>
                    </div>


                    <div class="col-sm-3 col-md-2 col-lg-2">
                            <div class="user-box revt-clas">
                                <div class="arrow-img-cls"><img src="<?php echo base_url();?>front-asset/images/select-droup.png" alt="" /></div>
                                <div class="assignment-gray-main">
                                   <select class="js-example-basic-multiple" multiple="multiple" name="cat_id[]" >
                                    <?php if(isset($getCategory) && sizeof($getCategory) > 0) { ?>
                                         <?php $cnt=0; foreach($getCategory as $category) { $cnt++;?>
                                           <option <?php if(isset($_REQUEST['cat_id']) &&  in_array($category['category_id'], $_REQUEST['cat_id'])) { echo 'selected'; } ?> value="<?php echo $category['category_id']; ?>"><?php echo $category['category_name']; ?></option>
                                    <?php } } ?>
                                </select>
                            </div>
                            <div class="clerfix"></div>
                        </div>
                    </div>
                

                    <input type="hidden" name="min-price" id="min-price" value="<?php if(isset($_REQUEST['min-price'])) { echo $_REQUEST['min-price']; } else { echo '0'; }  ?>" >
                    <input type="hidden" name="max-price" id="max-price" value="<?php if(isset($_REQUEST['max-price'])) { echo $_REQUEST['max-price']; } else { echo '0'; }  ?>" >
                    <div class="col-sm-2 col-md-2 col-lg-2">
                       <div class="price-slider seller-list-prices">
                           <div class="range-t input-bx" for="amount">
                               <div id="slider-price-range" class="slider-rang"></div>
                                  <div class="amount-no" id="slider_price_range_txt">
                               </div>
                           </div>
                       </div>
                    </div>

            <div class="col-sm-2 col-md-1 col-lg-1">
             <button type="submit" id="search_listing" class="more-f-bor"><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
            <?php  if(isset($_REQUEST['location'])){ ?>
            <div class="col-sm-2 col-md-1 col-lg-1">
             <a href="<?php echo base_url().'search/requirments'; ?>"  class="more-f-bor"><i class="fa fa-refresh" aria-hidden="true"></i></a>
            </div>
            <?php } ?>
     </div>
 </div>
</form>
</div>


<!-- requirement prices -->
<?php
$max_price =[];
foreach ($requirments_price as $price) {
    $max_price[] = strtok($price['price'],'.');
}
?>
<input type="hidden"  id="max_product_price" value="<?php echo MAX($max_price); ?>" ?>
<!-- end requirement prices -->

<div class="col-sm-7 col-md-7 col-lg-7">
                <div class="block-filter-container">
                    <div class="sort-by-block">
                        <span class="title-listing-block">Requirements</span>
                    </div>
                </div>
                <div class="row mobile-block">
                
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <?php $this->load->view('status-msg'); ?>
                    <div class="account-info-block">                        

                    <?php if(isset($getRequirments) && sizeof($getRequirments) > 0) {?>

                        <?php foreach ($getRequirments as $value) {
                            $get_buyer_info = $this->master_model->getRecords('tbl_user_master',array('id'=>$value['buyer_id']));
                            ?>
                                <div class="main-img-block">
                                    <div class="row">
                                        <div class="col-sm-4 col-md-4 col-lg-4">
                                            <div class="">

                                                <?php if(!empty($value['req_image']) && file_exists('images/buyer_post_requirment_image/'.$value['req_image'])){?>
                                                    <img style="width:260px;height:162px;" src="<?php echo base_url().'images/buyer_post_requirment_image/'.$value['req_image']; ?>" alt="list_img_1" class="img-responsive list_view_img_1" />
                                                <?php } else { ?>
                                                <img src="<?php echo base_url().'images/buyer_post_requirment_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                                                <?php } ?> 

                                            </div>
                                        </div>
                                        <div class="col-sm-8 col-md-8 col-lg-8">
                                            <div class="event_news_txt">
                                                <div class="head_txt">
                                                  <a href="<?php echo base_url().'search/requirment_detail/'.$value['id'];?>"><?php if(!empty($value['title'])) { echo ucfirst($value['title']); } else { echo "Not Available" ;} ?></a>
                                                </div>
                                                <div class="list_up"><span><i class="fa fa-folder-open"></i>
                                                    </span>
                                                         <?php if(!empty($value['category_name'])) { echo substr($value['category_name'], 0 , 50); } else { echo "Not Available"; } ?> ->
                                                         <?php if(!empty($value['subcategory_name'])) { echo substr($value['subcategory_name'], 0 , 50); } else { echo "Not Available"; } ?>
                                                </div>                                                
                                                <div class="list_add"><span><i class="fa fa-map-marker"></i>
                                                    </span>
                                                    <?php if(!empty($value['location'])) { echo substr($value['location'], 0 , 50); } else { echo "Not Available"; } ?> 

                                                </div>

                                                <div class="lsit_text_content">
                                                <div class="list_up "><span><i class="fa fa-calendar" aria-hidden="true"></i> Posted Date :
                                                    </span>
                                                         <?php echo date('F j, Y', strtotime($value['created_date']))." at ".date("g:i a", strtotime($value['created_date'])); ?> 
                                                </div>
                                                </div>

                                                <div class="lsit_text_content">
                                                <span><i class="fa fa-user" aria-hidden="true"></i> Buyer Name :
                                                </span>
                                                     <?php if(!empty($get_buyer_info[0]['name'])) { echo $get_buyer_info[0]['name']; } else { echo "Not Available"; } ?>
                                                </div>

                                                <!-- <div class="lsit_text_content">
                                                <span><i class="fa fa-envelope-o" aria-hidden="true"></i> Buyer Email :
                                                </span>
                                                     <?php if(!empty($get_buyer_info[0]['email'])) { 
                                                      ?><a href="mailto:<?php echo $get_buyer_info[0]['email']; ?>?Subject=Inquiry" class="stop_loader" target="_top"><?php echo $get_buyer_info[0]['email']; ?></a><?php
                                                     } else { 

                                                      echo "Not Available"; } ?>
                                                    
                                                </div> -->
                                                
                                                <div class="lsit_text_content">
                                                <div class="list_up up-list-price"><span> <?php echo CURRENCY; ?>
                                                    </span>
                                                         <?php if(!empty($value['price'])) { echo $value['price']; } else { echo "Not Available"; } ?> 
                                                </div>
                                                </div>

                                                <div class="lsit_text_content">
                                                   <?php if(!empty($value['description'])) { echo substr($value['description'], 0 , 300); } else { echo "Not Available"; } ?>.. ...
                                                    <div class="clr"></div>
                                                </div>
                                            </div>

                                            <?php if(!empty($this->session->userdata('user_id') && $this->session->userdata('user_type')=='Seller')) {?>
                                                <!-- <a data-backdrop="static" data-buyer_id="<?php echo $value['buyer_id']; ?>" data-toggle="modal" data-target="#add_to_contact_seller" type="button" class="send-inquiry-btn buyer_id">Add to Contact</a> -->
                                            
                                                <a data-buyerid="<?php echo $value['buyer_id']; ?>" type="button" class="send-inquiry-btn add_buyer_in_contact">Add to Contact</a>

                                            <?php } ?>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                </div>

                            <?php
                        }
                    } else {
                        $this->load->view('no-data-found');
                    }?>
                    </div>
                    </div>
                </div>
             <div class="">
         <?php echo $this->pagination->create_links(); ?>
        </div>

    </div>


<!--map-box start here-->
<div class="col-sm-5 col-md-5 col-lg-5"><!--pad-r-less-->
    <div class="row">
        <div class="map-sec">
          <div id="map" width="100%" height="1270"></div>
        </div>
    </div>
</div>
<!--map-box end here-->



</div>
</div>
</div>







<!--multi selection-->
<script src="<?php echo base_url();?>front-asset/js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url();?>front-asset/js/select2.full.js"></script>
<script type="text/javascript">
$(".js-example-basic-multiple").select2();
$('#example-getting-started').multiselect();  
</script>




 




<script type="text/javascript">

$(document).ready(function(){
 
 $('.add_buyer_in_contact').click(function(){

    var buyer_id      = $(this).data('buyerid');

    ajaxindicatorstart();
    jQuery.ajax({
    url: site_url+'seller/add_buyer_to_my_contact',
    type:"post",
    dataType : 'json',
    data:{

          buyer_id      :buyer_id,
    },
    success:function(response){
      ajaxindicatorstop();


  
      if(response.contact_status == "success") {
        jQuery("#status_msg").html('<div class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.contact_msg+' </div>');
      } else if(response.contact_status == "error") {
        jQuery("#status_msg").html('<div class="alert-box warning alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>&nbsp;'+response.contact_msg+' </div>');
      }

    }
    });

 });

});
</script>
<!-- Modal -->
<div id="myModal" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">Send Enquiry</h4>
        </div>
        <div class="modal-body">
            <div class="mobile-nu-block enquiry">
                <input type="text" name="Mobile No" required />
                <span class="highlight"></span>
                <label>Name</label>
            </div>
            <div class="mobile-nu-block enquiry">
                <input type="text" name="Mobile No" required />
                <span class="highlight"></span>
                <label>Email</label>
            </div>
            <div class="mobile-nu-block enquiry">
                <input type="text" name="Mobile No" required />
                <span class="highlight"></span>
                <label>Phone</label>
            </div>
            <div class="select-bock-container drop">
                <select>
                    <option>Product Category</option>
                    <option>Fruite</option>
                    <option>dry fruits</option>
                </select>
            </div>
            <div class="mobile-nu-block enquiry">
                <input type="text" name="Mobile No" required />
                <span class="highlight"></span>
                <label>Product Name</label>
            </div>
            
            <div class="mobile-nu-block input-first box-msg">
                        <textarea cols="" rows=""> </textarea>
                        <span class="highlight"></span>
                        <label>Message</label>
                    </div>
        </div>
        <div class="modal-footer">
            <div class="btn-password changes enq">
                <button class="change-btn-pass " type="button">Send</button>
            </div>
        </div>
    </div>

</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $(".more-btn-block").click(function() {
        $(this).parent().find(".view-more-content").slideDown("slow");
        $(this).hide();
        $(this).parent().find(".hide-btn-block").show();
    });
    $(".hide-btn-block").click(function() {
        $(this).parent().find(".view-more-content").slideUp("slow");
        $(this).parent().find(".more-btn-block").show();
        $(this).hide();
    });

    // footer dropdown links start 
    var min_applicable_width = 767;

    $(document).ready(function() {
        applyResponsiveSlideUp($(this).width(), min_applicable_width);
    });

    function applyResponsiveSlideUp(current_width, min_applicable_width) {
        /* Set For Initial Screen */
        initResponsiveSlideUp(current_width, min_applicable_width);

        /* Listen Window Resize for further changes */
        $(window).bind('resize', function() {
            if ($(this).width() <= min_applicable_width) {
                unbindResponsiveSlideup();
                bindResponsiveSlideup();
            } else {
                unbindResponsiveSlideup();
            }
        });
    }

    function initResponsiveSlideUp(current_width, min_applicable_width) {
        if (current_width <= min_applicable_width) {
            unbindResponsiveSlideup();
            bindResponsiveSlideup();
        } else {
            unbindResponsiveSlideup();
        }
    }

    function bindResponsiveSlideup() {
        $(".menu_name2").hide();
        $(".footer_heading2").bind('click', function() {
            var $ans = $(this).parent().find(".menu_name2");
            $ans.slideToggle();
            $(".menu_name2").not($ans).slideUp();
            $('.menu_name2').removeClass('active');

            $('.footer_heading2').not($(this)).removeClass('active');
            $(this).toggleClass('active');
            $(this).parent().find(".menu_name2").toggleClass('active');
        });
    }

    function unbindResponsiveSlideup() {
        $(".footer_heading2").unbind('click');
        $(".menu_name2").show();
    }
    // footer dropdown links end

});

</script>



<script type="text/javascript">
$(document).ready(function(){
$('.stop_loader').click(function(){
setTimeout(function(){
ajaxindicatorstop();
},2000)
});
}); 
</script>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/giocomplete/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/giocomplete/jquery-1.12.4.js"></script>
<script src="<?php echo base_url(); ?>assets/giocomplete/jquery-ui.js"></script>
<script>
$( function() {
var availableTags = [
    <?php 
    $this->db->select('title');
    $this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
    $this->db->where('tbl_buyer_post_requirement.status <>' , 'Delete');
    $this->db->where('tbl_buyer_post_requirement.requirment_status' , 'open');
    $this->db->group_by('title'); 
    $this->db->from('tbl_buyer_post_requirement');
    //$this->db->limit(2);
    $user_list = $this->db->get();
    $user_res  = $user_list->result();
    if(count($user_list)>0)
    {
      foreach($user_res as $key => $user)
      {
         echo '"'.$user->title.'",';
      }
    }
    ?>
];
$( "#requirment" ).autocomplete({
  source: availableTags, minLength:1
});
});
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLKCmJEIrOrzdRklZHYer8q9qx2XLJ4Vs&sensor=false&libraries=places"></script>
<script src="<?php echo base_url(); ?>assets/js/geolocator/jquery.geocomplete.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var location = "";
          $("#location").geocomplete({
            details: ".geo-details",
            detailsAttribute: "data-geo",
            location:location,
            types:[]
        });
    });
</script>

<!-- Contact Button Popup form starts -->
<!-- Modal -->
<div id="add_to_contact_seller" class="modal fade main" role="dialog">
<div class="modal-dialog Change-Password">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header Password">
            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url() ?>front-asset/images/cross-icon.png" alt="cross" /> </button>
            <h4 class="modal-title">Add to Contact</h4>
        </div>
        <div class="modal-body">
        <?php $this->load->view('status-msg1'); ?>
         <div ng-controller="AddtoContactSeller">  
            <form  
            id="addtocontactsellerseller"
            class=""
            name="addtocontactseller" 
            enctype="multipart/form-data"
            novalidate 
            ng-submit="addtocontactseller.$valid && add_to_contact_seller();"  >

            <input type="hidden" name="buyer_id" id="buyer_id"  ng-model="user.buyer_id">
           
            <div class="mobile-nu-block enquiry" ng-class="{'has-error':addtocontactseller.name.$touched && addtocontactseller.name.$invalid}">
                <input type="text" 
                       name="name" 
                       class="beginningSpace_restrict" 
                       ng-model="user.name" 
                       placeholder="Enter Your Name"
                       ng-required="true"
                />
                <div class="error-new-block" ng-messages="addtocontactseller.name.$error" ng-if="addtocontactseller.$submitted || addtocontactseller.name.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>
                <span class="highlight"></span>
                <label>Name</label>
            </div>

            <div class="mobile-nu-block enquiry" ng-class="{'has-error':addtocontactseller.email.$touched && addtocontactseller.email.$invalid}">
                <input type="text" 
                       name="email" 
                       class="beginningSpace_restrict" 
                       ng-model="user.email" 
                       placeholder="Enter Your Email"
                       ng-required="true"
                />
                <div class="error-new-block" ng-messages="addtocontactseller.email.$error" ng-if="addtocontactseller.$submitted || addtocontactseller.email.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>
                <span class="highlight"></span>
                <label>Email</label>
            </div>

            <div class="mobile-nu-block enquiry" ng-class="{'has-error':addtocontactseller.mobile_number.$touched && addtocontactseller.mobile_number.$invalid}">
                <input type="text" 
                       name="mobile_number" 
                       class="beginningSpace_restrict" 
                       ng-model="user.mobile_number" 
                       placeholder="Enter Your Contact No."
                       ng-required="true"
                />
                <div class="error-new-block" ng-messages="addtocontactseller.mobile_number.$error" ng-if="addtocontactseller.$submitted || addtocontactseller.mobile_number.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>
                <span class="highlight"></span>
                <label>Contact No.</label>
            </div>

            <div class="mobile-nu-block enquiry" ng-class="{'has-error':addtocontactseller.country.$touched && addtocontactseller.country.$invalid}">
                <input type="text" 
                           name="country" 
                           class="beginningSpace_restrict" 
                           ng-model="user.country" 
                           placeholder="Enter Your Country"
                           ng-required="true"
                           />
                <div class="error-new-block" ng-messages="addtocontactseller.country.$error" ng-if="addtocontactseller.$submitted || addtocontactseller.country.$touched">
                <div>
                <div class="err_msg_div" style="display:none;">
                    <p ng-message="required"    class="error">  This field is required</p>
                    </div>
                </div>
                <script type="text/javascript">
                        $(document).ready(function(){
                          setTimeout(function(){
                            $('.err_msg_div').removeAttr('style');
                          },200);
                        });
                    </script>
                </div>

                <span class="highlight"></span>
                <label>Country</label>
            </div>

            </div>
            <div class="modal-footer">
                <div class="btn-password changes enq">
                    <button class="change-btn-pass " type="submit">Send</button>
                </div>
            </div>
          </form>
       </div>
    </div>

</div>
</div>
</div>
<!-- Contact Button Popup form ends -->


<!-- Clustering  MAP -->
<style>
#map {
height: 100%;
}
html, body {
height: 100%;
margin: 0;
padding: 0;
}
    
    .content-img.address-urt{font-size: 14px;    color: #cacaca;}
.img-block-section {
  overflow: hidden;
  margin-bottom: 15px;
  margin-top: 0px;
  position: relative;
  box-shadow: 2px 0 5px rgba(0, 0, 0, 0.37);
  -webkit-box-shadow: 2px 0 5px rgba(0, 0, 0, 0.37);
  -moz-box-shadow: 2px 0 5px rgba(0, 0, 0, 0.37);
  cursor: pointer;
  transition: all 0.5s ease 0s;
  -webkit-transition: all 0.5s ease 0s;
  -moz-transition: all 0.5s ease 0s;
   height: 290px;
}
.details-bx {
  bottom: 0;
  position: absolute;
  width: 100%;padding-bottom: 10px;
}
.details-section {
  background: rgba(0, 0, 0, 0) url("../images/tras-bg.png") repeat scroll 0 0;
/*  height: 100px;*/
}
.img-circle {
  float: left;
  height: 10px;
  width: 10px;
}
.content-img.seller-cont-img {
font-weight: 600;
padding-top: 6px;
}
.content-img {
  color: #333;
  display: block;
  float: left;
  font-family: "ralewaysemibold",sans-serif;
  font-size: 16px;
  max-width: 122px;
  overflow: hidden;
  padding-left: 12px;
  padding-top: 6px;
  text-overflow: ellipsis;
  white-space: nowrap;
  width: 100%;
}
.price-cont {
  color: #fff;
  float: right;
  font-size: 22px;
  padding-top: 12px;
}
</style>




<script type="text/javascript">
        $(function() {

           var max_product_price          = $('#max_product_price').val();
           var max_product_price_constant = $('#max_product_price').val();
           var min_price                  = $('#min-price').val();
           var max_price                  = $('#max-price').val();

           if(max_price != 0)
           {
            max_product_price = max_price;
           }

           $("#slider-price-range").slider({
               range: true,
               min: 0,
               max: max_product_price_constant,
               values: [min_price, max_product_price],
               slide: function(event, ui) {
                   $('#min-price').val(ui.values[0]);
                   $('#max-price').val(ui.values[1]);
                   $("#slider_price_range_txt").html("<span class='slider_price_min'>$ " + ui.values[0] + "</span>  <span style='margin-left:50%;' class='slider_price_max'>$ " + ui.values[1] + " </span>");
               }
           });
           $("#slider_price_range_txt").html("<span class='slider_price_min'> $ " + $("#slider-price-range").slider("values", 0) + "</span>  <span style='margin-left:50%;' class='slider_price_max'>$ " + $("#slider-price-range").slider("values", 1) + "</span>");
         });      
</script> 







<!-- <div id="map" style="width: 1000px; height: 600px;"></div> -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyCccvQtzVx4aAt05YnfzJDSWEzPiVnNVsY&libraries=places" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/markerclustererplus/src/markerclusterer.js"></script>
<script src="<?php echo base_url(); ?>front-asset/js/no-country-place.js" type="text/javascript"></script>
<script>
var locations = [];
locations = [
    <?php   
        if(count($userLatLng))
        {
            foreach($userLatLng as $row) 
            {
                if(!empty($row['latitude']) && !empty($row['longitude']))     
                {  
                    if(!empty($row['user_image']) && file_exists('images/buyer_image/'.$row['user_image']))
                    {
                        $img = base_url().'images/buyer_image/'.$row['user_image'];
                    }
                    else 
                    {
                        $img = base_url().'images/default/default-user-img.jpg';
                    }
                    ?>
                      ['<div class="col-sm-12 col-sm-12 col-lg-12"><div class="img-block-section"><div class="details-bx"><div class="details-section"><a style="cursor:text" href="#"><div class="content-img"><?php echo $row["name"]; ?></div><div class="content-img address-urt"><?php echo $row["address"]; ?></div><div class="content-img seller-cont-img"><?php echo $row["user_type"]; ?></div></a></div></div><div class="img-grid"><img src="<?php echo $img; ?>" alt="" class="img-resize" style="max-width: none;height: 200px;width: 200px;"></div></div></div>',<?php echo $row['latitude']; ?>,<?php echo $row['longitude']; ?>],
                    <?php
                }
            }
        }
    ?>
];

<?php
    // find center   
    if(count($userLatLng))
    {
        foreach($userLatLng as $row) 
        {
            if(!empty($row['latitude']) && !empty($row['longitude']))     
            {  
                 $minlat = false;
                 $minlng = false;
                 $maxlat = false;
                 $maxlng = false;
                     if ($minlat === false) { $minlat = $row['latitude']; } else { $minlat = ($row['latitude'] < $minlat) ? $row['latitude'] : $minlat; }
                     if ($maxlat === false) { $maxlat = $row['latitude']; } else { $maxlat = ($row['latitude'] > $maxlat) ? $row['latitude'] : $maxlat; }
                     if ($minlng === false) { $minlng = $row['longitude']; } else { $minlng = ($row['longitude'] < $minlng) ? $row['longitude'] : $minlng; }
                     if ($maxlng === false) { $maxlng = $row['longitude']; } else { $maxlng = ($row['longitude'] > $maxlng) ? $row['longitude'] : $maxlng; }
                  // Calculate the center
                  $lat = $maxlat - (($maxlat - $minlat) / 2);
                  $lon = $maxlng - (($maxlng - $minlng) / 2);
            }
        }
    }
?>



var defaultLat =  <?php if(isset($lat)){ echo $lat; } else { echo '19.997453'; } ?>;
var defaultLng =  <?php if(isset($lon)){ echo $lon; } else { echo '73.789802'; } ?>;
var _address   =  "<?php  if(isset($_REQUEST['location']) && $_REQUEST['location'] != ""){ echo $_REQUEST['location']; } else { echo ""; }?>";
var map;       
var mc;/*marker clusterer*/
var mcOptions = {
      gridSize: 20,
      //maxZoom: 7,
      zoom:10,
      imagePath: "https://cdn.rawgit.com/googlemaps/v3-utility-library/master/markerclustererplus/images/m"
  };
/*global infowindow*/
var infowindow = new google.maps.InfoWindow();
/*geocoder*/

var _address = _address;
mapInitialize(defaultLat, defaultLng,_address);
function createMarker(latlng,text)
{
var marker = new google.maps.Marker({
    position: latlng,
    map: map,
    icon: "<?php echo base_url().'images/fevicon/map-icn.png'; ?>"

});
/*get array of markers currently in cluster*/
var allMarkers = mc.getMarkers();
 
/*check to see if any of the existing markers match the latlng of the new marker*/
if (allMarkers.length != 0) {
  for (i=0; i < allMarkers.length; i++) {
    var existingMarker = allMarkers[i];
    var pos = existingMarker.getPosition();
    if (latlng.equals(pos)) {
      text = text + " " + locations[i][0];
    }
  }
}

google.maps.event.addListener(marker, 'click', function(){
    infowindow.close();
    infowindow.setContent(text);
    infowindow.open(map,marker);
});
mc.addMarker(marker);
return marker;
}

function mapInitialize(lat,lng,_address)
{
var geocoder = new google.maps.Geocoder(); 
 geocoder.geocode( { 'address': _address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      /*map.setCenter(results[0].geometry.location);*/
      map.setOptions({ maxZoom: 15 });
      map.fitBounds(results[0].geometry.viewport);
    }
  });
  var options = {
      zoom: 3,
      center: new google.maps.LatLng(lat,lng), 
      mapTypeId: google.maps.MapTypeId.ROADMAP,
  };
  map = new google.maps.Map(document.getElementById('map'), options); 

  /*marker cluster*/
  var gmarkers = [];
  mc = new MarkerClusterer(map, [], mcOptions);
  for (i=0; i<locations.length; i++)
  {
      var latlng = new google.maps.LatLng(parseFloat(locations[i][1]),parseFloat(locations[i][2]));                
      gmarkers.push(createMarker(latlng,locations[i][0]));
  }
}
</script>
<!-- Clustering  MAP -->



<script type="text/javascript">
    
    $(document).bind('keypress', function(e) {
      if(e.keyCode==13){
        $('#serach_critearea').submit();   
       }
    });  
    
    
    $('#search_listing').on('click', function() {
        setTimeout(function(){
          $('#serach_critearea').submit();   
        },0);
    });
    $('#requirment').keyup(function() {
        var search_text = $(this).val();
        $('#hedden_search').val(search_text);
    });
    $('#requirment').blur(function() {
        var search_text = $(this).val();
        $('#hedden_search').val(search_text);
    });
</script>

<script type="text/javascript">
$(document).ready(function(){
    $('.select2-search__field').attr('placeholder' , 'Select categories');
    $('.select2-search__field').attr('style' , 'width: 100em;');
});</script>