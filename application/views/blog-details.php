<div class="page-head-block">
    <div class="page-head-overlay"></div>
    <div class="container">
        <div class="head-txt-block">
            Market News Details
        </div>
    </div>
</div>
<div class="container">
    <div class="col-sm-8 col-md-8 col-lg-8">

      <?php if(isset($blogs_data) && sizeof($blogs_data)>0){

          $this->db->where('id' , $blogs_data[0]['Bolg_user_id']);
          $get_user = $this->master_model->getRecords('tbl_user_master');
          ?>



          <div class="block-one-main">
            <?php 
            if($this->session->flashdata('error')!=''){  ?>
            <div style="margin-top: 42px;" class="alert-box errors alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button> &nbsp; <?php echo  $this->session->flashdata('error'); ?></div>
            <?php } ?>
            <?php  
            if($this->session->flashdata('success')!=''){  ?>
            <div style="margin-top: 42px;" class="alert-box success alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span>×</span></button>  &nbsp; <?php echo  $this->session->flashdata('success'); ?></div>
            <?php } ?>


            <div class="blog-one-head">
                <a href="<?php echo base_url().'blogs/details/'.$blogs_data[0]['blogs_id'];?>"> <?php echo $blogs_data[0]['blogs_name_en']; ?></a>
            </div>
            <div class="blog-img-one">
                <?php if(!empty($blogs_data[0]['blogs_img']) && file_exists('uploads/blogs_images/'.$blogs_data[0]['blogs_img'])){?>
                <img class="event_pic " src="<?php echo base_url().'timthumb.php?src='.base_url().'uploads/blogs_images/'.$blogs_data[0]['blogs_img'].'&h=300&w=555&zc=0'; ?>" alt="" data-description="">
                <?php } else { ?>
                <!-- <img src="<?php echo base_url().'images/large_no_image.jpg'; ?>" alt="" /> -->
                <?php } ?> 
                <?php  if(!empty($blogs_row['blogs_img']) && file_exists('uploads/blogs_images/'.$blogs_row['blogs_img'])){?>    
                <div class="event_icon">
                    <strong><?php echo date( "d", strtotime( $blogs_data[0]['blogs_added_date'] ) ); ?></strong>
                    <span><?php echo date( "F", strtotime( $blogs_data[0]['blogs_added_date'] ) ); ?></span>
                </div>
                <?php } else { ?>

                <?php } ?> 



                <?php 
                $where_arr = array('message_read'=>'1','comm_blog_id'=>$blogs_data[0]['blogs_id']);
                $data['blogs_comments_data'] = $this->master_model->getRecords('tbl_blogs_comments',$where_arr);
                ?>
                <?php  if(!empty($blogs_data[0]['blogs_img']) && file_exists('uploads/blogs_images/'.$blogs_data[0]['blogs_img'])){?>

                <div class="comm-add-lab">
                    <div class="admin-block">
                        <span><img src="<?php echo base_url(); ?>front-asset/images/blog-user.png" alt="" /></span> <span>By <?php if(!empty($get_user[0]['name'])){ echo $get_user[0]['name']; } else { echo $blogs_data[0]['blogs_added_by']; } ?></span>
                    </div>
                    <div class="admin-block">
                        <span><img src="<?php echo base_url(); ?>front-asset/images/blog-chat.png" alt="" /></span> <span><?php echo count($data['blogs_comments_data']); ?></span>
                    </div>

                    <?php } else { ?>
                    <?php } ?> 
                    
                    <div class="clr"></div>

                </div>            
                <div class="blog-content">
                    <p><?php echo $blogs_data[0]['blogs_description_en']; ?> </p> 
                </div>
                <div class="blog-content">
                    <?php  if(!empty($blogs_data[0]['blogs_img']) && file_exists('uploads/blogs_images/'.$blogs_data[0]['blogs_img'])){?>

                    <?php } else { ?>

                    <div class="content-blog-no-img">
                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                        <span class="calndr-blogs">
                            <?php echo date( "d", strtotime( $blogs_data[0]['blogs_added_date'] ) ); ?>
                            <?php echo date( "F", strtotime( $blogs_data[0]['blogs_added_date'] ) ); ?>
                        </span>       
                        <span><i class="fa fa-user" aria-hidden="true"></i></span>  
                        <span>By <?php if(!empty($get_user[0]['name'])){ echo $get_user[0]['name']; } else { echo $blogs_data[0]['blogs_added_by']; } ?>
                        </span>  
                        <span>
                            <i class="fa fa-comment" aria-hidden="true"></i>
                        </span>
                        <span><?php echo count($data['blogs_comments_data']); ?></span>         
                    </div>

                    <?php } ?> 
                </div>





                <?php 
                $where_arr=array('message_read'=>'1','comm_blog_id'=>$blogs_data[0]['blogs_id']);
                $blogs_comments_count=$this->master_model->getRecords('tbl_blogs_comments',$where_arr);
                ?>

                <div class="comment-head"> <?php echo count($blogs_comments_count); ?> Comments <?php echo $this->pagination->create_links(); ?></div>
                <div class="comee-bx">


                    <!-- comment string-->

                    <div id='wrapper' class="wrap-comment">
                        <ul>
                            <?php echo $Comments_string; ?>
                        </ul>
                    </div>

                    <hr/>
                    <form action="<?php echo base_url().'blogs/details/'.$blogs_data[0]['blogs_id']; ?>" method="post" class="form target">
                        <div class="comment-head">Leave a Reply</div>
                        <input type="hidden" id="comm_id" name="comm_id" value="0">
                        <input type="hidden" id="comm_blog_id" name="comm_blog_id" value="<?php echo $blogs_data[0]['blogs_id']; ?>">
                        <div class="row">


                            <div class="col-sm-4 col-md-4 col-lg-6"> 
                                <div class="user-two">
                                    <div class="Name-block-frm"> Name<span class="error">*</span> </div>
                                    <input type="text" class="comment-in" placeholder="Enter Name" id="con_name" name="con_name" value="<?php if(!empty($this->session->userdata('user_name'))) { echo $this->session->userdata('user_name'); } ?>" /> 
                                    <div style="display: block;" class="error" id="error_name"><?php echo form_error('con_name');?> </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-md-4 col-lg-6"> 
                                <div class="user-two">
                                    <div class="Name-block-frm"> Email<span class="error">*</span> </div>
                                    <input type="text" class="comment-in" placeholder="Enter a valid email address" id="cont_email" name="cont_email" value="<?php if(!empty($this->session->userdata('user_email'))) { echo $this->session->userdata('user_email'); } ?>"/> 
                                    <div style="display: block;" class="error" id="error_email"><?php echo form_error('cont_message');?></div>
                                </div>
                            </div>

                            <div class="clr"></div>

                            <div class="col-sm-8 col-md-8 col-lg-12" style="display:none;"> 
                                <div class="user-two">
                                    <div class="Name-block-frm"> Phone </div>
                                    <input type="text" class="comment-in" id="con_phone" name="con_phone" placeholder="Enter Phone" value="<?php if(!empty($this->session->userdata('user_mobile'))) { echo $this->session->userdata('user_mobile');} ?>"/> 
                                    <div style="display: block;"  class="error" id="error_phone"><?php echo form_error('cont_phone');?></div>
                                </div>
                            </div>


                            <div class="col-sm-12 col-md-12 col-lg-12"> 
                                <div class="user-two">
                                    <div class="Name-block-frm"> Comment <span class="error" >*</span> </div>
                                    <textarea cols="" rows="" class="text-com" placeholder="Enter Comment" id="cont_message" name="cont_message" minlength="5" ></textarea>
                                    <div style="display: block;" class="error" id="error_message"><?php echo form_error('cont_message');?></div>
                                </div>
                            </div>
                            <div class="clr"></div>
                            <div class="col-sm-12 col-md-12 col-lg-12" style="padding: 2px;"> 
                                <div class="user-two">
                                    <button type="submit" id="comments_submit" name="comments_submit" value="comments_submit" class="post-com-btn">Post Comment </button> 
                                </div>
                            </div>
                        </form>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>  

        <?php if(count($blogs_comments_count) == 0){ ?>     
        </div>
        <?php } ?>     


            <?php } else {?> 
            <div class="block-one-main" style="margin-top: 65px;">
               <?php 
               $this->load->view('no-data-found');
               ?>
           </div>      
           <?php 
       } ?>
       

   </div>
   <div class="col-sm-4 col-md-4 col-lg-4">

    <div class="">
        <?php 
        if(!empty($this->session->userdata('user_type')) && $this->session->userdata('user_type') == "Seller") {?>
        <a href="<?php echo base_url().'blogs/add';?>"><button class="change-btn-pass ">Add Post</button></a>
        <?php } else  if(!empty($this->session->userdata('user_type')) && $this->session->userdata('user_type') == "Buyer") { ?>
        <a href="<?php echo base_url().'blogs/add';?>"><button class="change-btn-pass ">Add Post</button></a>
        <?php } else {?>
        <a href="<?php echo base_url().'login';?>"><button class="change-btn-pass ">Add Post</button></a>
        <?php } ?>
    </div>
    <div class="res-posts-block">
        <div class="cate-right-head">
            Recent Posts
        </div>

        <?php 
        if(count($blogs)>0) { 
          $cnt=0; foreach ($blogs as $blogs_row) { $cnt++;
            if($cnt <=5){
                ?>
                <div class="posts-block">
                    <div class="post-img-block">
                        <?php if(!empty($blogs_row['blogs_img']) && file_exists('uploads/blogs_images/'.$blogs_row['blogs_img'])){ ?>
                        <img style="height:50px;width:70px;" src="<?php echo base_url().'timthumb.php?src='.base_url().'uploads/blogs_images/'.$blogs_row['blogs_img'].'&h=300&w=555&zc=0'; ?>" alt="" data-description="">
                        <?php } else { ?>
                        <img style="height:50px;width:70px;" src="<?php echo base_url().'images/large_no_image.jpg'; ?>" alt="" />
                        <?php } ?> 
                    </div>
                    <div class="post-content">
                        <div class="post-content-head">
                         <a href="<?php echo base_url().'blogs/details/'.$blogs_row['blogs_id'];?>"> <?php echo $blogs_row['blogs_name_en']; ?></a>
                     </div>
                     <div class="post-date">
                       <?php echo date( "F j, Y", strtotime( $blogs_row['blogs_added_date'] ) ); ?>
                   </div>
               </div>                                                                                          
               <div class="clr"></div>
           </div>
           <?php } } }  else { ?> 
           <div class="block-one-main" style="margin-top: 65px;">
               <?php 
               $this->load->view('no-data-found');
               ?>
           </div>      
           <?php  } ?>
       </div>
   </div>
</div>
<script type="text/javascript">

$(document).ready(function(){
    //Blog details Page 
    $('#comments_submit').on('click',function(){
        var con_name=$('#con_name').val();
        var cont_email=$('#cont_email').val();
        var con_phone=$('#con_phone').val();
        var cont_message=$('#cont_message').val();
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var phn_num = /^-?\d*(\.\d+)?$/;
        var flag=1;
        $('#error_name').html('');
        $('#error_email').html('');
        $('#error_phone').html('');
        $('#error_message').html('');

        if(con_name.trim()=='')
        {
          $('#error_name').html('Please Enter Your Name');
          flag=0;
      }
      if(cont_email.trim()=='')
      {
          $('#error_email').html('Please Enter Your Email');
          flag=0;
      }
      else if(!filter.test(cont_email))
      {
          $('#error_email').html('Please Enter Valid Email');
          flag=0;
      }
      if(cont_message.trim()=="")
      {
         $('#error_message').html('Please Enter Comment');
         flag=0;
     }

    /*if(con_phone.trim()=="")
    {
      $('#error_phone').html('Please Enter Phone Number');
      flag=0;
    }
    else if((con_phone.length < 10))
     {
       $('#error_phone').html('Phone no must be greater than 10 digits');
       flag=0;
     }
     else  if((con_phone.length > 16))
     {
       $('#error_phone').html('Phone no must be less than 16 digits');
       flag=0;
     }
     else if(!phn_num.test(con_phone))
    {
      $('#error_phone').html('Please Enter valid Phone Number');
      flag=0;
  }*/
  if(flag==1)
  {
      return true;
  }
  else
  {
      return false;
  }

});


//Blog details Page 

$(".com_id").on('click',function(){

    var id = $(this).attr('data-id');
    $('#comm_id1').val(id);
});

$('#comments_submit1').on('click',function(){

    var con_name=$('#con_name1').val();
    var cont_email=$('#cont_email1').val();
    var con_phone=$('#con_phone1').val();
    var cont_message=$('#cont_message1').val();
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var phn_num = /^-?\d*(\.\d+)?$/;
    var flag=1;
    $('#error_name1').html('');
    $('#error_email1').html('');
    $('#error_phone1').html('');
    $('#error_message1').html('');

    if(con_name.trim()=='')
    {
      $('#error_name1').html('Please Enter Your Name');
      flag=0;
  }
  if(cont_email.trim()=='')
  {
      $('#error_email1').html('Please Enter Your Email');
      flag=0;
  }
  else if(!filter.test(cont_email))
  {
      $('#error_email1').html('Please Enter Valid Email');
      flag=0;
  }
  if(cont_message.trim()=="")
  {
     $('#error_message1').html('Please Enter Comment');
     flag=0;
 }

 if(con_phone.trim()=="")
 {
  $('#error_phone1').html('Please Enter Phone Number');
  flag=0;
}
else if(!phn_num.test(con_phone) || con_phone.length !=10 )
{
  $('#error_phone1').html('Please Enter valid Phone Number');
  flag=0;
}
if(flag==1)
{
  return true;
}
else
{
  return false;
}

});

});

</script>
