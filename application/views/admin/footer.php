    <!-- END Main Content -->
    <footer>
      <p>
        <img class="" src="<?php echo base_url(); ?>images/fevicon/favicon.png" alt="" 
             style="height:55px; margin-top:-8px" />
        <?php echo date('Y'); ?> ©  
        <?php echo PROJECT_NAME; ?> Admin.
      </p>
    </footer>
    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a> </div>
  <!-- END Content --> 
</div>

</body>
</html>