<!-- BEGIN Navbar -->
<div id="navbar" class="navbar">
  <?php $this->load->view(ADMIN_PANEL_NAME.'top-navigation'); ?>
</div>
<!-- END Navbar -->

<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <div id="sidebar" class="navbar-collapse collapse">
    <?php $this->load->view(ADMIN_PANEL_NAME.'left-navigation'); ?>
  </div>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>
         <h1><i class="fa fa-inbox" aria-hidden="true"></i><?php echo $pageTitle; ?></h1>
        <h4></h4>
      </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url().ADMIN_PANEL_NAME.'dashboard/' ?>">Home</a> <span class="divider">
                <i class="fa fa-angle-right"></i></span>
                </li>
                <li><a href="<?php echo base_url().ADMIN_PANEL_NAME.'seller_requests/for_product' ?>">Manage Product Requests</a> <span class="divider">
                <i class="fa fa-angle-right"></i></span>
                </li>
            <li class="active"><?php echo $pageTitle; ?></li>
          </ul>
    <!-- END Breadcrumb -->
    <!-- BEGIN Main Content -->


    <?php

    $select = '
            tbl_user_master.name,
            tbl_user_master.email,
            tbl_user_master.mobile_number
    ';
    $get_user_data = $this->master_model->getRecords('tbl_user_master',array('id'=>$fetchdata[0]['seller_id'],$select));
    if(isset($get_user_data[0]['name'])) 
    { $name  = $get_user_data[0]['name']; } 
    else {
      $name  = 'Not Available'; 
    }
    if(isset($get_user_data[0]['email'])) 
    { $email = $get_user_data[0]['email']; } 
    else {
      $email = 'Not Available'; 
    }
    ?>
    <div class="row">
      <div class="col-md-12">
          <div class="box box-magenta ">
              <div class="box-title">
                  <h3><i class="fa fa-inbox" aria-hidden="true"></i><?php echo ucfirst($pageTitle); ?></h3>
                  <div class="box-tool">
                  </div>
              </div>
              <div class="box-content">
                <form method="post" class="form-horizontal" id="validation-form" enctype="multipart/form-data">
                
                    <div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label">Name:</label>
                        <div class="col-sm-9 col-lg-10 ">
                           <?php if(!empty($name)){echo $name; }?>
                         </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Contact Email:</label>
                        <div class="col-sm-9 col-lg-10 controls">
                            <?php if(!empty($email)){echo $email; }?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Phone:</label>
                        <div class="col-sm-9 col-lg-10 controls">
                            <?php if(!empty($get_user_data[0]['mobile_number'])){echo $get_user_data[0]['mobile_number']; }?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Request Description:</label>
                        <div class="col-sm-9 col-lg-10 controls">
                          <?php if(!empty($fetchdata[0]['description'])){echo stripslashes($fetchdata[0]['description']);} ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Requested Date:</label>
                        <div class="col-sm-9 col-lg-10 controls">
                          <?php if(isset($fetchdata[0]['requiested_date'])){ echo date('F j, Y', strtotime($fetchdata[0]['requiested_date']))." at ".date("g:i a", strtotime($fetchdata[0]['requiested_date'])); } else { echo "Not Available"; }; ?>
                        </div>
                    </div>
                    <div class="form-group">

                        <label class="col-sm-3 col-lg-2 control-label">Action:</label>
                        <div class="col-sm-9 col-lg-10 controls">
                          <a class='btn btn-success accept_request' data-reqid="<?php echo $fetchdata[0]['id'] ?>" title='Click here accept request'
                             href='#'>
                               Accept
                          </a>
                        </div>
                    </div>
                    
                    <div class="form-group">
                       <label class="col-sm-3 col-lg-2 control-label">&nbsp;</label>
                        <div class="col-sm-9 col-lg-10 controls">
                        <a href="<?php echo base_url().ADMIN_PANEL_NAME.'seller_requests/for_products';?>" class="btn btn-primary">Go Back</a>
                        </div>
                    </div>
                 </form>
              </div>
          </div>
      </div>
  </div>
  <?php $this->load->view(ADMIN_PANEL_NAME.'top-footer'); ?>

<!-- END Main Content -->
<script type="text/javascript">
  
  $('.accept_request').click(function(){
        var req_id = jQuery(this).data("reqid");
        swal({   
             title: "Are you sure?",   
             text: "You want to accept this request ?",  
             type: "warning",   
             showCancelButton: true,   
             confirmButtonColor: "#8cc63e",  
             confirmButtonText: "Yes",  
             cancelButtonText: "No",   
             closeOnConfirm: false,   
             closeOnCancel: false }, function(isConfirm){   
              if (isConfirm) 
              { 
                     swal("Accepted!", "You accept this request successfully.", "success"); 
                           location.href=site_url+"admin/seller_requests/toggle_visibility/"+req_id;
              } 
              else
              { 
                     swal("Cancelled", "Cancelled :)", "error");          
                } 
            });
    }); 

</script>  