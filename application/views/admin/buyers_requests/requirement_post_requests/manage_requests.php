<!-- BEGIN Navbar -->
<div id="navbar" class="navbar"> <?php $this->load->view('admin/top-navigation'); ?> </div>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <div id="sidebar" class="navbar-collapse collapse"> <?php $this->load->view('admin/left-navigation'); ?> </div>
  <!-- END Sidebar -->
<div id="main-content">
    <!-- BEGIN Page Title -->

<div class="page-title">
  <div>
    <h1><i class="fa fa-inbox" aria-hidden="true"></i><?php echo $page_title;?></h1>
  </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
  <ul class="breadcrumb">
    <li> <i class="fa fa-home"></i> <a href="<?php echo base_url().ADMIN_PANEL_NAME.'dashboard/'; ?>">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
    <li class="active"><?php echo $page_title;?></li>
  </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<style type="text/css">
  .hide_th
  {
    background-color:  transparent;
  }
</style>
<?php
      $attributes1 = array('class' => 'form-horizontal','id'=>'frm-manage-contact','name'=>"frm-manage-contact",'method'=>"post");
echo form_open(base_url().ADMIN_PANEL_NAME."buyer_requests/for_requirements/",$attributes1 );
?>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-magenta">
        <div class="box-title">
          <h3><i class="fa fa-inbox" aria-hidden="true"></i> <?php echo $page_title;?></h3>
          <div class="box-tool">
          </div>
        </div>
        <div class="box-content">
          <?php
              if($this->session->flashdata('error')!=''){  ?>
          <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
          <?php }
               if($this->session->flashdata('success')!=''){?>
          <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
          <?php } ?>
          <div class="col-md-10">

            <div id='opt_status_service'>
            </div>
            <div class="alert alert-danger" id="no_select" style="display:none;"></div>
            <div class="alert alert-warning" id="warning_msg" style="display:none;"></div>
          </div>
           <div class="btn-toolbar pull-right clearfix">
            <div class="btn-group"> <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Refresh" href="<?php echo base_url().ADMIN_PANEL_NAME.'/buyer_requests/for_requirementss'; ?>"style="text-decoration:none;"><i class="fa fa-repeat"></i></a> </div>
          </div>
          <br/>
          <br/>
          <div class="clearfix"></div>
          <div class="table-responsive" style="border:0" id='manage_service_tbl_data'>
            <?php
            ?>
            <input type="hidden" name="act_status" id="act_status" value="" />
            <table class="table table-advance"  id="table1" >
              <thead>
                <tr>
                  <th>Seller name</th>
                  <th>Seller email</th>
                  <th>Description</th>
                  <th>Status</th>
                  <th>View</th>
                </tr>
              </thead>
              <tbody>
              <?php
                  if($requestdata)
                  {
                      foreach ($requestdata as $request)
                      {

                      $select = '
                              tbl_user_master.name,
                              tbl_user_master.email,
                              tbl_user_master.mobile_number
                      ';
                      $get_user_data = $this->master_model->getRecords('tbl_user_master',array('id'=>$request['buyer_id'],$select));
                     

                      if(isset($get_user_data[0]['name'])) 
                      { $name = $get_user_data[0]['name']; } 
                      else {
                        $name = 'Not Available'; 
                      }
                      if(isset($get_user_data[0]['email'])) 
                      { $email = $get_user_data[0]['email']; } 
                      else {
                        $email = 'Not Available'; 
                      }

                      $adv_actions ='<a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="View contact" href="'.base_url().ADMIN_PANEL_NAME.'buyer_requests/details/'.$request["id"].'"  style="text-decoration:none; " title="View"><i class="fa fa-eye"></i>
                                    </a>
                                    ';
                      echo "<tr>";
                      echo "<td> ".$name."</td>";
                      echo "<td> ".$email."</td>";
                      echo "<td> ".substr($request['description'], 0, 50).".... </td>";
                      echo "<td>
                                <a class='btn btn-success accept_request' data-req_id=".$request['id']." title='Click here accept request'
                                   href='#'>
                                     Accept
                                </a>
                           </td>";
                      echo "<td> {$adv_actions} </td>";
                      echo "</tr>";
                      }// foreach ends here
                   }/* if ends here*/
              ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php echo form_close();?>
<!-- END Main Content -->
<?php $this->load->view(ADMIN_PANEL_NAME.'top-footer'); ?>
<script type="text/javascript">
  
  $('.accept_request').click(function(){
        var req_id = jQuery(this).data("req_id");
        swal({   
             title: "Are you sure?",   
             text: "You want to accept this request ?",  
             type: "warning",   
             showCancelButton: true,   
             confirmButtonColor: "#8cc63e",  
             confirmButtonText: "Yes",  
             cancelButtonText: "No",   
             closeOnConfirm: false,   
             closeOnCancel: false }, function(isConfirm){   
              if (isConfirm) 
              { 
                     swal("Accepted!", "You accept this request successfully.", "success"); 
                           location.href=site_url+"admin/buyer_requests/toggle_visibility/"+req_id;
              } 
              else
              { 
                     swal("Cancelled", "Cancelled :)", "error");          
                } 
            });
    }); 

</script>  
