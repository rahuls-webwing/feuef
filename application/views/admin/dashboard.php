<!-- BEGIN Navbar -->
<div id="navbar" class="navbar">
  <?php $this->load->view(ADMIN_PANEL_NAME.'top-navigation'); ?>
</div>
<!-- END Navbar --> 

<!-- BEGIN Container -->
<div class="container" id="main-container"> 
  <!-- BEGIN Sidebar -->
  <div id="sidebar" class="navbar-collapse collapse"> 
     <?php $this->load->view(ADMIN_PANEL_NAME.'left-navigation'); ?>
 </div>
 <!-- END Sidebar --> 

 <!-- BEGIN Content -->
 <div id="main-content"> 
  <!-- BEGIN Page Title -->
  <div class="page-title">
    <div>
      <h1><img class="" src="<?php echo base_url(); ?>images/fevicon/favicon.png" alt="" style="height:30px; margin-top:-8px" /> 
         <?php echo PROJECT_NAME; ?> Dashboard</h1>
      <h4>Overview, stats, chat and more</h4>
  </div>
</div>
<!-- END Page Title --> 



<!-- BEGIN Tiles -->
<?php  $color_array=array('tile tile-light-sky',

    'tile tile-red',
    'tile tile-light-red',
    'tile tile-magenta',
    'tile tile-light-pink',
    'tile tile-light-yellow',
    'tile tile-dark-magento',
    'tile tile-lgt-magento');?>

    <!-- END Tiles -->
    <!-- BEGIN Tiles -->



    <div class="row">
    <div class="col-md-12">
    <div class="row">


    <div class="col-md-3">
      <div class="row">
        <div class="col-md-12 tile-active">
            <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>dashboard/">
                <div class="tile tile-yellow">
                    <div class="img img-center">
                       <i class="fa fa-home"></i>
                   </div>
                   <p class="title text-center">Dashboard</p>
               </div></a>
               <div class="tile tile-magenta  ">
                <p class="title">Dashboard</p>
                <p>See Your Dashboard</p>
                <div class="img img-bottom">
                    <i class="fa fa-home"></i>
                </div>
            </div>

        </div>
      </div>
    </div>


    <div class="col-md-3">
    <div class="row">
    <div class="col-md-12 tile-active">
        <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>profile/edit/">
            <div class="tile tile-yellow">
                <div class="img img-center">
                   <i class="fa fa-cog"></i>
               </div>
               <p class="title text-center">Edit Profile</p>
           </div></a>
           <div class="tile tile-magenta ">
            <p class="title">Edit Profile</p>
            <p>See Your Profile</p>
            <div class="img img-bottom">
                <i class="fa fa-cog"></i>
            </div>
        </div>

    </div>
    </div>
    </div>

    <div class="col-md-3">
    <div class="row">
    <div class="col-md-12 tile-active">
        <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>profile/change_password/">
            <div class="tile tile-yellow">
                <div class="img img-center">
                   <i class="fa fa-key" aria-hidden="true"></i>
               </div>
               <p class="title text-center">Change Password</p>
           </div></a>
           <div class="tile tile-magenta">
            <p class="title">Change Password</p>
            <p>Change Your Password</p>
            <div class="img img-bottom">
                <i class="fa fa-key" aria-hidden="true"></i>
            </div>
        </div>

    </div>
    </div>
    </div>

    <div class="col-md-3" >
    <div class="row">
    <div class="col-md-12 tile-active">
        <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>category/manage/">
            <div class="tile tile-yellow">
                <div class="img img-center">
                   <i class="fa fa-tag" aria-hidden="true"></i>
               </div>
               <p class="title text-center">Category</p>
           </div></a>
           <div class="tile tile-magenta">
            <p class="title">Category</p>
            <p>Add Your Category</p>
            <div class="img img-bottom">
                <i class="fa fa-tag" aria-hidden="true"></i>
            </div>
        </div>

    </div>
    </div>
    </div>

    <div class="col-md-3" >
    <div class="row">
    <div class="col-md-12 tile-active">
        <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>subcategory/manage/">
            <div class="tile tile-yellow">
                <div class="img img-center">
                   <i class="fa fa-tags" aria-hidden="true"></i>
               </div>
               <p class="title text-center">Sub-Category</p>
           </div></a>
           <div class="tile tile-magenta">
            <p class="title">Sub-Category</p>
            <p>Add Your Sub-Category</p>
            <div class="img img-bottom">
                <i class="fa fa-tags" aria-hidden="true"></i>
            </div>
        </div>

    </div>
    </div>
    </div>


    <div class="col-md-3">
    <div class="row">
    <div class="col-md-12 tile-active">
        <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>pages/manage/">
            <div class="tile tile-yellow">
                <div class="img img-center">
                   <i class="fa fa-file-o" aria-hidden="true"></i>
               </div>
               <p class="title text-center">Front Pages</p>
           </div></a>
           <div class="tile tile-magenta">
            <p class="title">Front Pages</p>
            <p>Manage Your Front Pages</p>
            <div class="img img-bottom">
                <i class="fa fa-file-o" aria-hidden="true"></i>
            </div>
        </div>

    </div>
    </div>
    </div>

    <div class="col-md-3">
    <div class="row">
    <div class="col-md-12 tile-active">
        <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>sellers/manage/">
            <div class="tile tile-yellow">
                <div class="img img-center">
                   <i class="fa fa-users" aria-hidden="true"></i>
               </div>
               <p class="title text-center">Sellers</p>
           </div></a>
           <div class="tile tile-magenta">
            <p class="title">Sellers</p>
            <p>Manage Your Sellers</p>
            <div class="img img-bottom">
                <i class="fa fa-users" aria-hidden="true"></i>
            </div>
        </div>

    </div>
    </div>
    </div>

    <div class="col-md-3">
    <div class="row">
    <div class="col-md-12 tile-active">
        <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>buyers/manage/">
            <div class="tile tile-yellow">
                <div class="img img-center">
                   <i class="fa fa-users" aria-hidden="true"></i>
               </div>
               <p class="title text-center">Buyers</p>
           </div></a>
           <div class="tile tile-magenta">
            <p class="title">Buyers</p>
            <p>Manage Your Buyers</p>
            <div class="img img-bottom">
                <i class="fa fa-users" aria-hidden="true"></i>
            </div>
        </div>

    </div>
    </div>
    </div>


    <div class="col-md-3">
    <div class="row">
    <div class="col-md-12 tile-active">
        <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>pricing/manage/">
            <div class="tile tile-yellow">
                <div class="img img-center">
                   <i class="fa fa-eur" aria-hidden="true"></i>
               </div>
               <p class="title text-center">Price Setting</p>
           </div></a>
           <div class="tile tile-magenta">
            <p class="title">Price Setting</p>
            <p>Manage Your Price Setting</p>
            <div class="img img-bottom">
                <i class="fa fa-eur" aria-hidden="true"></i>
            </div>
        </div>

    </div>
    </div>
    </div>

   <!--  <div class="col-md-3">
      <div class="row">
        <div class="col-md-12 tile-active">
            <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>seller_requests/for_products/">
                <div class="tile tile-yellow">
                    <div class="img img-center">
                       <i class="fa fa-users" aria-hidden="true"></i>
                   </div>
                   <p class="title text-center">Seller Product Upload Request</p>
               </div></a>
               <div class="tile tile-magenta">
                <p class="title">Seller Product Upload Request</p>
                <p>Manage Your Product Upload Request's</p>
                <div class="img img-bottom">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </div>
            </div>

        </div>
      </div>
    </div>

    <div class="col-md-3">
      <div class="row">
        <div class="col-md-12 tile-active">
            <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>seller_requests/for_offers/">
                <div class="tile tile-yellow">
                    <div class="img img-center">
                       <i class="fa fa-users" aria-hidden="true"></i>
                   </div>
                   <p class="title text-center">Seller Make Offer Request's</p>
               </div></a>
               <div class="tile tile-magenta">
                <p class="title">Seller Make Offer Request's</p>
                <p>Manage Your Make Offer Request's</p>
                <div class="img img-bottom">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </div>
            </div>

        </div>
      </div>
    </div>


    <div class="col-md-3">
      <div class="row">
        <div class="col-md-12 tile-active">
            <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>buyer_requests/for_requirements/">
                <div class="tile tile-yellow">
                    <div class="img img-center">
                       <i class="fa fa-users" aria-hidden="true"></i>
                   </div>
                   <p class="title text-center">Buyer Requirement Post Request's</p>
               </div></a>
               <div class="tile tile-magenta">
                <p class="title">Buyer Requirement Post Request's</p>
                <p>Manage Your Requirement Post Request's</p>
                <div class="img img-bottom">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </div>
            </div>

        </div>
      </div>
    </div>

    <div class="col-md-3">
      <div class="row">
        <div class="col-md-12 tile-active">
            <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>buyer_requests/for_offers/">
                <div class="tile tile-yellow">
                    <div class="img img-center">
                       <i class="fa fa-users" aria-hidden="true"></i>
                   </div>
                   <p class="title text-center">Buyer Make Offer Request's</p>
               </div></a>
               <div class="tile tile-magenta">
                <p class="title">Buyer Make Offer Request's</p>
                <p>Manage Your Make Offer Request's</p>
                <div class="img img-bottom">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </div>
            </div>

        </div>
      </div>
    </div>
 -->
    <div class="col-md-3">
    <div class="row">
    <div class="col-md-12 tile-active">
        <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>subscribe/manage/">
            <div class="tile tile-yellow">
                <div class="img img-center">
                   <i class="fa fa-envelope-o" aria-hidden="true"></i>
               </div>
               <p class="title text-center">Subscribers</p>
           </div></a>
           <div class="tile tile-magenta">
            <p class="title">Subscribers</p>
            <p>Manage Your Subscribers</p>
            <div class="img img-bottom">
                <i class="fa fa-envelope-o" aria-hidden="true"></i>
            </div>
        </div>

    </div>
    </div>
    </div>

    <div class="col-md-3">
    <div class="row">
    <div class="col-md-12 tile-active">
        <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>enquiry/manage">
            <div class="tile tile-yellow">
                <div class="img img-center">
                   <i class="fa fa-phone-square" aria-hidden="true"></i>
               </div>
               <p class="title text-center">Enquiry</p>
           </div></a>
           <div class="tile tile-magenta">
            <p class="title">Enquiry</p>
            <p>Manage Your Enquiry</p>
            <div class="img img-bottom">
               <i class="fa fa-phone-square" aria-hidden="true"></i>
            </div>
        </div>

    </div>
    </div>
    </div>

    

    
    
</div>

<!-- end reports -->     

</div>
<!-- content end -->

</div>


