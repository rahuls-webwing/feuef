<!-- BEGIN Theme Setting -->
<!-- END Theme Setting --> 

<!-- BEGIN Navbar -->
<div id="navbar" class="navbar">
  <?php $this->load->view(ADMIN_PANEL_NAME.'top-navigation'); ?>
</div>
<!-- END Navbar --> 

<!-- BEGIN Container -->
<div class="container" id="main-container"> 
  <!-- BEGIN Sidebar -->
  <div id="sidebar" class="navbar-collapse collapse">
    <?php $this->load->view(ADMIN_PANEL_NAME.'left-navigation'); ?>
  </div>
  <!-- END Sidebar --> 
  <div id="main-content">
    <div class="page-title">
      <div><h1><i class="fa fa-star" aria-hidden="true"></i> <?php echo $pageTitle; ?></h1></div>
    </div>
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?php echo base_url();?>admin/dashboard">Home</a> <span class="divider"> <i class="fa fa-angle-right"></i></span> </li>
        <li class="active"><?php echo $pageTitle; ?></li>
      </ul>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-magenta">
          <div class="box-title"><h3><i class="fa fa-star" aria-hidden="true"></i> <?php echo $pageTitle; ?></h3></div>
          <form name="frm-manage" id="frm-manage" method="post" action="<?php echo base_url().ADMIN_PANEL_NAME.'reviews/manage';?>">
            <div class="box-content">
            <span id="show_success" ></span>    
             <?php if($this->session->flashdata('error')!=''){  ?>
                <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php }if($this->session->flashdata('success')!=''){?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                <?php } ?>
                <div class="alert alert-danger" id="no_select" style="display:none;"></div>
                <div class="alert alert-warning" id="warning_msg" style="display:none;"></div>
              <div class="col-md-10">
                
              </div>
              <div class="btn-toolbar pull-right clearfix">
                <div class="btn-group">
                  
                  <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Active Review" href="javascript:void(0);" onclick="javascript : return checkmultiaction('frm-manage','active');" style="text-decoration:none;"><i class="fa fa-unlock"></i></a>
                  <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Block  Review" href="javascript:void(0);" onclick="javascript : return checkmultiaction('frm-manage','block');"  style="text-decoration:none;"><i class="fa fa-lock"></i></a>
                  <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Delete Review" href="javascript:void(0);" onclick="javascript : return checkmultiaction('frm-manage','delete');"  style="text-decoration:none;"><i class="fa fa-trash-o"></i></a>
                </div>
                <div class="btn-group"> <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Refresh" href="<?php echo base_url().ADMIN_PANEL_NAME.'sellers/manage'; ?>"style="text-decoration:none;"><i class="fa fa-repeat"></i></a> </div>
              </div>
              <br/><br/>
              <div class="clearfix"></div>
              <div class="table-responsive" style="border:0" id="showBlockUI">
                <input type="hidden" name="act_status" id="act_status" value="" />
                <table class="table table-condensed" <?php if(count($getreview)>0){?> id="table1"<?php } ?>>
                  <thead>
                    <tr>
                      <th style="width:18px"><input type="checkbox" name="mult_change" id="mult_change" value="delete" /></th>
                      <th>from buyer</th>
                      <th>To   seller</th>
                      <th>Rating</th>
                      <th>Review</th>
                      <th>commented at</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="catList" >
                    <?php

                    if(count($getreview)>0) {
                    foreach ($getreview as  $review) { 
                    $get_buyer_details  = $this->master_model->getRecords('tbl_user_master',array('id'=>$review['buyer_id']));
                    $get_seller_details = $this->master_model->getRecords('tbl_user_master',array('id'=>$review['seller_id']));
                    ?>

                    <tr>
                      
                      <td style="width:18px">
                         <input type="checkbox" name="checkbox_del[]" id="checkbox_del" value="<?php echo $review['review_id']; ?>"/>
                      </td>
                      <td><?php if(isset($get_buyer_details[0]['name'])) { echo $get_buyer_details[0]['name'];} else { echo "Not Available" ;} ?></td>
                      <td><?php if(isset($get_seller_details[0]['name'])) { echo $get_seller_details[0]['name'];} else { echo "Not Available" ;} ?></td>
                      <td><?php 
                            $ratings         = 0;
                            $arv_rating      = 0;       
                            $arv_rating      = $review['ratings'];  // find rating avarage
                            ?> 
                            <span class="product-satrs-block">
                            <?php
                                $finalrate = $chekRating = mb_substr($arv_rating,  0, 4);
                                for($i=1;$i<=5;$i++) {
                                $selected = "";

                                $point = $i - $finalrate;

                                if(!empty($finalrate) && $i<=$finalrate) {
                                  ?>
                                   <img alt="img" src="<?php echo base_url();?>images/review/star-full.png" style="margin-top:11px;"/>
                                   <?php
                                 } else if($point == '0.5' || $point < '0.5' || $point < '1') {
                                  ?>
                                  <img alt="img" src="<?php echo base_url();?>images/review/star-half.png" style="margin-top:11px;"/>
                                   <?php 
                                } else {
                                  ?>
                                  <img alt="img" src="<?php echo base_url();?>images/review/star-blank.png" style="margin-top:11px;"/>
                                  <?php 
                                }
                            } 
                          ?>
                      </td>
                      <td style="width:400px;"><?php if(isset($review['comment'])) { echo $review['comment'];} else { echo "Not Available" ;} ?></td>
                      <td>
                         <?php echo date('F j, Y', strtotime($review['commented_at']))." at ".date("g:i a", strtotime($review['commented_at'])); ?>
                      </td>

                      <td>
                      <?php if($review['status']=='Unblock'){ ?>
                      <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Active" href="<?php echo base_url('admin/reviews/status/Block/'.$review['review_id']); ?>"  style="text-decoration:none;"><i class="fa fa-unlock"></i></a>
                      <?php } else { ?>
                      <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Block" href="<?php echo base_url('admin/reviews/status/Unblock/'.$review['review_id']); ?>" style="text-decoration:none;"><i class="fa fa-lock"></i></a>
                      <?php } ?>
                       <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip delete_review" title="Delete " data-review="<?php echo $review['review_id'];  ?>"  style="text-decoration:none;" href="javascript:void(0);"><i class="fa fa-trash-o"></i></a>
                      </td>
                    </tr>
                    <?php
                      }
                    }
                    else
                      echo '<tr><td colspan="6"><div class="alert alert-danger"><button data-dismiss="alert" class="close">×</button><strong>Error!</strong> Sorry no records found !</div></td></tr>';
                    ?>
                  </tbody>
                </table>
              </div>
              <div class="clear"></div>
            </div>
          </form>
        </div>
      </div>
    </div>
    


<script type="text/javascript">
  $('.delete_review').click(function(){
        var review_id = jQuery(this).data("review");
        swal({   
             title: "Are you sure?",   
             text : "You want to delete this review ?",  
             type : "warning",   
             showCancelButton: true,   
             confirmButtonColor: "#8cc63e",  
             confirmButtonText: "Yes",  
             cancelButtonText: "No",   
             closeOnConfirm: false,   
             closeOnCancel: false }, function(isConfirm){   
              if (isConfirm) 
              { 
                     swal("Deleted!", "Review has been deleted.", "success"); 
                           location.href=site_url+"admin/reviews/delete/"+review_id;
              } 
              else
              { 
                     swal("Cancelled", "Reviews is safe :)", "error");          
              } 
            });
    }); 
</script>  