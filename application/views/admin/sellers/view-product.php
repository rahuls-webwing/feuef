<!-- BEGIN Theme Setting -->
<div id="theme-setting">
  <?php //$this->load->view('admin/theme-setting'); ?>
</div>
<!-- END Theme Setting --> 

<!-- BEGIN Navbar -->
<div id="navbar" class="navbar">
  <?php $this->load->view('admin/top-navigation'); ?>
</div>
<!-- END Navbar --> 

<!-- BEGIN Container -->
<div class="container" id="main-container"> 
  <!-- BEGIN Sidebar -->
  <div id="sidebar" class="navbar-collapse collapse">
    <?php $this->load->view('admin/left-navigation'); ?>
  </div>
  <!-- END Sidebar --> 
  
  <!-- BEGIN Content -->
  <div id="main-content"> 
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>

        <h1><i class="fa fa-user"></i> <?php echo $pageTitle; ?></h1>
        <h4></h4>
      </div>
    </div>
    <!-- END Page Title --> 
    
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>dashboard">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
        
            <li ><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>sellers/manage">Manage sellers</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
       
          
        <li class="active"><?php echo $pageTitle; ?></li>
      </ul>
    </div>
    <!-- END Breadcrumb --> 
    
    <!-- BEGIN Main Content -->   
    
    <div class="row">
      <div class="col-md-12">
        <div class="box box-magenta">
          <div class="box-title">
            <h3><i class="fa fa-user"></i> <?php echo $pageTitle; ?> </h3>
          </div>
          <div class="box-content">
          
            <style type="text/css">
              .unverify-class {
                  padding: 2px 20px;
                  line-height: 20px;
                  text-align: center;
                  background: #FF0000;
                  color: #fff;
                  font-weight: 600;
              }
              .verify-class {
                  padding: 2px 20px;
                  line-height: 20px;
                  text-align: center;
                  background: #15b74e;
                  color: #fff;
                  font-weight: 600;
              }
              .padding-left {
                  margin-right: 5px;
              }
            </style>
            <form class="form-horizontal" id="" action="#">
            <?php
              if(count($getProductsDetail) > 0) 
              {
                foreach ($getProductsDetail as $detail) 
                {
                  ?>
                   
                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Product Title : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;">
                           <?php if(!empty($detail['title'])){ echo $detail['title']; } else { echo "Not Available"; } ?>
                        </label>
                      </div>
                    </div>
                   
                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Price : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;"><?php if(!empty($detail['price'])){ echo CURRENCY.$detail['price']; } else { echo "Not Available"; } ?></label>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Address: </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;"><?php if(!empty($detail['location'])){ echo $detail['location']; } else { echo "Not Available"; } ?></label>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Posted Date : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;"><?php if(isset($detail['created_date'])){ echo date('F j, Y', strtotime($detail['created_date']))." at ".date("g:i a", strtotime($detail['created_date'])); } else { echo "Not Available"; }; ?> </label>
                      </div>
                    </div>
                      
                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Description : </label>
                      <div class="col-sm-9 col-lg-9 controls">
                        <label style="font-weight:700; text-align:justify;"><?php if(!empty($detail['description'])){ echo $detail['description']; } else { echo "Not Available"; } ?></label>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Offer Image : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;">
                           <?php if(!empty($detail['req_image']) && file_exists('images/seller_upload_product_image/'.$detail['req_image'])){?>
                            <img style="height:162px;width:260px;" src="<?php echo base_url().'images/seller_upload_product_image/'.$detail['req_image']; ?>" alt="list_img_1" class="img-responsive list_view_img_1" />
                            <?php } else { ?>
                            <img src="<?php echo base_url().'images/seller_upload_product_image/re_no_image/no.image.icon160X260.jpg'; ?>" alt="" />
                            <?php } ?> 
                        </label>
                      </div>
                    </div>

                     <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">&nbsp;</label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;">
                           <a href="javascript:history.back()" class="btn btn-primary">Go Back</a>
                        </label>
                      </div>
                    </div>
                    
                  <?php
                }
              }
              else{ ?>
                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Sorry : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;">
                           No Record Found.....
                        </label>
                      </div>
                    </div>
              <?php }
            ?>
         </form> 
      </div>
    </div>
  </div>
</div>