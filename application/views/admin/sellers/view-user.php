<!-- BEGIN Theme Setting -->
<div id="theme-setting">
  <?php //$this->load->view('admin/theme-setting'); ?>
</div>
<!-- END Theme Setting --> 

<!-- BEGIN Navbar -->
<div id="navbar" class="navbar">
  <?php $this->load->view('admin/top-navigation'); ?>
</div>
<!-- END Navbar --> 

<!-- BEGIN Container -->
<div class="container" id="main-container"> 
  <!-- BEGIN Sidebar -->
  <div id="sidebar" class="navbar-collapse collapse">
    <?php $this->load->view('admin/left-navigation'); ?>
  </div>
  <!-- END Sidebar --> 
  
  <!-- BEGIN Content -->
  <div id="main-content"> 
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div>

        <h1><i class="fa fa-user"></i> <?php echo $pageTitle; ?></h1>
        <h4></h4>
      </div>
    </div>
    <!-- END Page Title --> 
    
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i> <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>dashboard">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
        
            <li ><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>sellers/manage">Manage sellers</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
       
          
        <li class="active"><?php echo $pageTitle; ?></li>
      </ul>
    </div>
    <!-- END Breadcrumb --> 
    
    <!-- BEGIN Main Content -->   
    
    <div class="row">
      <div class="col-md-12">
        <div class="box box-magenta">
          <div class="box-title">
            <h3><i class="fa fa-user"></i> <?php echo $pageTitle; ?> </h3>
          </div>
          <div class="box-content">
          
            <style type="text/css">
              .unverify-class {
                  padding: 2px 20px;
                  line-height: 20px;
                  text-align: center;
                  background: #FF0000;
                  color: #fff;
                  font-weight: 600;
              }
              .verify-class {
                  padding: 2px 20px;
                  line-height: 20px;
                  text-align: center;
                  background: #15b74e;
                  color: #fff;
                  font-weight: 600;
              }
              .padding-left {
                  margin-right: 5px;
              }
            </style>
            <form class="form-horizontal" id="" action="#">
            <?php
              if(count($user_info) > 0) 
              {
                foreach ($user_info as $user) 
                {
                  ?>

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Seller profile : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;">
                        <?php if(!empty($user['user_image']) && file_exists('images/seller_image/'.$user['user_image'])){?>
                        <img src="<?php echo base_url().'images/seller_image/'.$user['user_image']; ?>" alt="" />
                        <?php } else { ?>
                        <img src="<?php echo base_url().'images/default/default-user-img.jpg'; ?>" alt="" />
                        <?php } ?>
                        </label>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Seller Name : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;">
                           <?php if(!empty($user['name'])){ echo $user['name']; } else { echo "Not Available"; } ?>
                        </label>
                      </div>
                    </div>
                   
                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Gender : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;"><?php if(!empty($user['gender'])){ echo ucfirst($user['gender']); } else { echo "Not Available"; } ?></label>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Birth Date : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;">
                        <?php if(!empty($user['day'])){ echo $user['day'].'/';      } else { echo "Not Available"; }?>
                        <?php if(!empty($user['month'])){ echo $user['month'].'/';  } ?>
                        <?php if(!empty($user['year'])){ echo $user['year'];        } ?>
                        </label>
                      </div>
                    </div>
                    
                      
                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Email ID : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;"><?php if(!empty($user['email'])){ echo $user['email']; } else { echo "Not Available"; } ?></label>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Optional Email: </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;"><?php if(!empty($user['optional_email'])){ echo $user['optional_email']; } else { echo "Not Available"; } ?></label>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Contact Number : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;"><?php if(!empty($user['mobile_number'])){ echo $user['mobile_number']; } else { echo "Not Available"; }  ?> </label>
                      </div>
                    </div>


                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Address : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;"><?php if(!empty($user['address'])){ echo $user['address']; } else { echo "Not Available"; } ?></label>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">City : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;"><?php if(!empty($user['city'])){ echo $user['city']; } else { echo "Not Available"; } ?></label>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Country : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;"><?php if(!empty($user['country'])){ echo $user['country']; } else { echo "Not Available"; } ?></label>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Pincode : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;"><?php if(!empty($user['pincode'])){ echo $user['pincode']; } else { echo "Not Available"; } ?></label>
                      </div>
                    </div>
                    

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Company regi number : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;"><?php if(!empty($user['com_national_registration_number'])){ echo $user['com_national_registration_number']; } else { echo "Not Available"; } ?></label>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Company website : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;"><?php if(!empty($user['company_website'])){ echo $user['company_website']; } else { echo "Not Available"; } ?></label>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Company Description : </label>
                      <div class="col-sm-9 col-lg-9 controls">
                        <label style="font-weight:700; text-align:justify;"><?php if(!empty($user['company_description'])){ echo $user['company_description']; } else { echo "Not Available"; } ?></label>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">&nbsp;</label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;">
                           <a href="javascript:history.back()" class="btn btn-primary">Go Back</a>
                        </label>
                      </div>
                    </div>



                  <?php
                }
              }
              else{ ?>

                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Sorry : </label>
                      <div class="col-sm-9 col-lg-4 controls">
                        <label style="font-weight:700;">
                           No Record Found.....
                        </label>
                      </div>
                    </div>
              <?php }
            ?>
         </form> 
      </div>
    </div>
  </div>
</div>