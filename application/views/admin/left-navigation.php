<!-- BEGIN Navlist -->
<?php $pagename=$this->router->fetch_class()."/".$this->router->fetch_method(); ?>
<ul class="nav nav-list">
  


  <li  <?php if($pagename=='dashboard/')echo 'class="active"'; ?>>
    <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>dashboard/">
       <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
    </a> 
  </li>


  <li  <?php if($this->router->fetch_class()=='profile' ){?> class="active" <?php }?>> 
    <a href="javascript:void(0);" class="dropdown-toggle">
      <i class="fa fa-cog"></i><span>&nbsp;&nbsp;Profile</span> <b class="arrow fa fa-angle-right"></b>
    </a> 
      <ul class="submenu">
        <li <?php if($this->router->fetch_class()=='profile' && $this->router->fetch_method()=='change_password'){?> class="active" <?php }?>><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>profile/change_password/">Change Password</a></li>

        <li <?php if($this->router->fetch_class()=='profile' && $this->router->fetch_method()=='edit'){?> class="active" <?php }?>><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>profile/edit/">Edit Profile</a></li>

        <li <?php if($this->router->fetch_class()=='profile' && $this->router->fetch_method()=='social'){?> class="active" <?php }?>><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>profile/social/">Social Links</a></li>

        <!-- <li <?php /* if($this->router->fetch_class()=='profile' && $this->router->fetch_method()=='payment'){ */?> class="active" <?php /* } */?>><a href="<?php /* echo base_url().ADMIN_PANEL_NAME; */ ?>profile/payment/">Payment</a></li> -->
      </ul>
  </li>
 

  <!--CATEGORY-->
  
     <li  <?php if($this->router->fetch_class()=='category' ){?> class="active" <?php }?>> 
      <a href="javascript:void(0);" class="dropdown-toggle">
        <i class="fa fa-tag" aria-hidden="true"></i><span>&nbsp;&nbsp;Category</span> <b class="arrow fa fa-angle-right"></b>
      </a> 
        <ul class="submenu">
          
          <li <?php if($this->router->fetch_class()=='category' && $this->router->fetch_method()=='add'){?> class="active" <?php }?>><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>category/add/">Add</a></li>

          <li <?php if($this->router->fetch_class()=='category' && $this->router->fetch_method()=='manage'){?> class="active" <?php }?>><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>category/manage/">Manage</a></li>
          
        </ul>
    </li> 


  <!--SUB-CATEGORY-->   

    <li  <?php if($this->router->fetch_class()=='subcategory' ){?> class="active" <?php }?>> 
      <a href="javascript:void(0);" class="dropdown-toggle">
        <i class="fa fa-tags" aria-hidden="true"></i><span>&nbsp;&nbsp;Sub-category</span> <b class="arrow fa fa-angle-right"></b>
      </a> 
        <ul class="submenu">
          
          <li <?php if($this->router->fetch_class()=='subcategory' && $this->router->fetch_method()=='add'){?> class="active" <?php }?>><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>subcategory/add/">Add</a></li>

          <li <?php if($this->router->fetch_class()=='subcategory' && $this->router->fetch_method()=='manage'){?> class="active" <?php }?>><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>subcategory/manage/">Manage</a></li>
          
        </ul>
    </li>


  <!--FRONT -PAGES -->   

  <li   <?php if($this->router->fetch_class()=='pages' ){?> class="active" <?php }?>> 
    <a href="javascript:void(0);" class="dropdown-toggle">
      <i class="fa fa-file-o" aria-hidden="true"></i><span>&nbsp;&nbsp;Front Pages</span> <b class="arrow fa fa-angle-right"></b>
    </a> 
      <ul class="submenu">
        
        <li <?php if($this->router->fetch_class()=='pages' && $this->router->fetch_method()=='manage'){?> class="active" <?php }?>><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>pages/manage/">Manage</a></li>
        
      </ul>
  </li>

<!-- Training Course -->

    <li <?php if($this->router->fetch_class()=='course' || $this->router->fetch_class()=='coursecategory' || $this->router->fetch_class()=='coursecomments'){ ?> class="active" <?php } ?>>
        <a href="#" class="dropdown-toggle" >
          <i class="fa fa-rss"></i>
          <span>Training Course</span>
          <b class="arrow fa <?php if($this->router->fetch_class()=='course' ){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b>
        </a>
        <ul class="submenu">
            <li <?php if($this->uri->segment(3)=='add' && $this->uri->segment(2)=='course'){ ?> class="active" <?php } ?> ><a href="<?php echo base_url().'admin/course/add'; ?>">Add Course</a></li>
            <li <?php if($this->uri->segment(3)=='manage' && $this->uri->segment(2)=='course'){ ?> class="active" <?php } ?>><a href="<?php echo base_url().'admin/course/manage'; ?>">Manage Course</a></li>
            <li <?php if($this->uri->segment(3)=='manage' && $this->uri->segment(2)=='coursetraining'){ ?> class="active" <?php } ?> ><a href="<?php echo base_url().'admin/coursetraining/manage'; ?>">Course Approval </a></li>
        </ul>
      </li>
  
  <!--SELLERS-->
  
     <li <?php if($this->router->fetch_class()=='sellers' || $this->router->fetch_class()=='reviews'){?> class="active" <?php }?>> 
      <a href="javascript:void(0);" class="dropdown-toggle">
        <i class="fa fa-users" aria-hidden="true"></i><span>&nbsp;&nbsp;Training Providers</span> <b class="arrow fa fa-angle-right"></b>
      </a> 
        <ul class="submenu">

          <li <?php if($this->router->fetch_class()=='sellers' && $this->router->fetch_method()=='manage'){?> class="active" <?php }?>><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>sellers/manage/">Manage</a></li>
          <li <?php if($this->router->fetch_class()=='reviews' && $this->router->fetch_method()=='manage'){?> class="active" <?php }?>><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>reviews/manage/">Reviews</a></li>

          <li class="active" <?php if($this->router->fetch_method()=='offers'){ echo 'style="display:block;"'; } else {
            echo 'style="display:none;"';
            }?>><a href="#">Offers</a></li>

          <li class="active" <?php if($this->router->fetch_method()=='offer_detail'){ echo 'style="display:block;"'; } else {
            echo 'style="display:none;"';
            }?>><a href="#">Offers details</a></li>


          <li class="active" <?php if($this->router->fetch_method()=='products'){ echo 'style="display:block;"'; } else {
            echo 'style="display:none;"';
            }?>><a href="#">Products</a></li>

          <li class="active" <?php if($this->router->fetch_method()=='product_detail'){ echo 'style="display:block;"'; } else {
            echo 'style="display:none;"';
            }?>><a href="#">Products details</a></li>  
          
        </ul>
    </li> 

  <!--BUYERS-->
  
     <li <?php if($this->router->fetch_class()=='buyers' ){?> class="active" <?php }?>> 
      <a href="javascript:void(0);" class="dropdown-toggle">
        <i class="fa fa-users" aria-hidden="true"></i><span>&nbsp;&nbsp;End Users</span> <b class="arrow fa fa-angle-right"></b>
      </a> 
        <ul class="submenu">
          <li <?php if($this->router->fetch_class()=='buyers' && $this->router->fetch_method()=='manage'){?> class="active" <?php }?>><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>buyers/manage/">Manage</a></li>
          
        </ul>
    </li>   
  <!--Start Orders Setting-->  

    <li   <?php if($this->router->fetch_class()=='order_forms' ){?> class="active" <?php }?>> 
      <a href="javascript:void(0);" class="dropdown-toggle">
        <i class="fa fa-file-o" aria-hidden="true"></i><span>&nbsp;&nbsp;Order Forms</span> <b class="arrow fa fa-angle-right"></b>
      </a> 
        <ul class="submenu">
          <li <?php if($this->router->fetch_class()=='order_forms'){?> class="active" <?php }?>><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>order_forms/all/">all</a></li>
        </ul>
    </li>

  <!-- End Orders Setting-->

  <!--Price Setting-->
  
    <li <?php if($this->router->fetch_class()=='pricing' ){ ?> class="active" <?php } ?> > 
      <a href="javascript:void(0);" class="dropdown-toggle">
        <i class="fa fa-eur" aria-hidden="true"></i><span>&nbsp;&nbsp;Price Setting</span> <b class="arrow fa fa-angle-right"></b>
      </a> 
        <ul class="submenu">
          <li <?php if($this->router->fetch_class()=='pricing' && $this->router->fetch_method()=='manage'){?> class="active" <?php } ?> ><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>pricing/manage/">Manage</a></li>
        </ul>
    </li>   
 
    <li  <?php if($this->router->fetch_class()=='subscribe' ){?> class="active" <?php }?>> 
      <a href="javascript:void(0);" class="dropdown-toggle">
        <i class="fa fa-envelope-o" aria-hidden="true"></i><span>&nbsp;&nbsp;Subscribers</span> <b class="arrow fa fa-angle-right"></b>
      </a> 
        <ul class="submenu">
          <li <?php if($this->router->fetch_class()=='subscribe' && $this->router->fetch_method()=='manage'){?> class="active" <?php }?>><a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>subscribe/manage/">manage</a></li>
        </ul>
    </li>

    <li <?php if($this->router->fetch_class()=='enquiry'){?> class="active" <?php }?> >
      <a href="<?php echo base_url().ADMIN_PANEL_NAME; ?>enquiry/manage">
       <i class="fa fa-phone"></i>
       <span>Contact Enquiry</span>
     </a>
    </li>


</ul>
<!-- END Navlist --> 

<!-- BEGIN Sidebar Collapse Button -->
<div id="sidebar-collapse" class="visible-lg"> <i class="fa fa-angle-double-left"></i> </div>
<!-- END Sidebar Collapse Button --> 