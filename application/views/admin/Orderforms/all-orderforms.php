<!-- BEGIN Theme Setting -->
<!-- END Theme Setting --> 

<!-- BEGIN Navbar -->
<div id="navbar" class="navbar">
  <?php $this->load->view(ADMIN_PANEL_NAME.'top-navigation'); ?>
</div>
<!-- END Navbar --> 

<!-- BEGIN Container -->
<div class="container" id="main-container"> 
  <!-- BEGIN Sidebar -->
  <div id="sidebar" class="navbar-collapse collapse">
    <?php $this->load->view(ADMIN_PANEL_NAME.'left-navigation'); ?>
  </div>
  <!-- END Sidebar --> 
  <div id="main-content">
    <div class="page-title">
      <div><h1><i class="fa fa-user" aria-hidden="true"></i> <?php echo $pageTitle; ?></h1></div>
    </div>
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?php echo base_url();?>admin/dashboard">Home</a> <span class="divider"> <i class="fa fa-angle-right"></i></span> </li>
        <li class="active"><?php echo $pageTitle; ?></li>
      </ul>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-magenta">
          <div class="box-title"><h3><i class="fa fa-user" aria-hidden="true"></i> <?php echo $pageTitle; ?></h3></div>
          <form name="frm-manage" id="frm-manage" method="post" action="">
            <div class="box-content">
            <span id="show_success" ></span>    
             <?php if($this->session->flashdata('error')!=''){  ?>
                <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php }if($this->session->flashdata('success')!=''){?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                <?php } ?>
                <div class="alert alert-danger" id="no_select" style="display:none;"></div>
                <div class="alert alert-warning" id="warning_msg" style="display:none;"></div>
              <div class="col-md-10">
                
              </div>
              <div class="btn-toolbar pull-right clearfix">
                <!-- <div class="btn-group">
                  
                  <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Active Subcategory" href="javascript:void(0);" onclick="javascript : return checkmultiaction('frm-manage','active');" style="text-decoration:none;"><i class="fa fa-unlock"></i></a>
                  <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Block Subcategory" href="javascript:void(0);" onclick="javascript : return checkmultiaction('frm-manage','block');"  style="text-decoration:none;"><i class="fa fa-lock"></i></a>
                  <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Delete Subcategory" href="javascript:void(0);" onclick="javascript : return checkmultiaction('frm-manage','delete');"  style="text-decoration:none;"><i class="fa fa-trash-o"></i></a>
                </div> -->
                <div class="btn-group"> <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Refresh" href="<?php echo base_url().ADMIN_PANEL_NAME.'buyers/reuirments/'.$this->uri->segment(4); ?>"style="text-decoration:none;"><i class="fa fa-repeat"></i></a> </div>
              </div>
              <br/><br/>
              <div class="clearfix"></div>
              <div class="table-responsive" style="border:0" id="showBlockUI">
                <input type="hidden" name="act_status" id="act_status" value="" />
                <table class="table table-condensed">
                  <thead>
                    <tr>
                      <td style="text-indent:5px;">Sender</td>
                      <td style="text-indent:5px;">Receiver</td>
                      <td style="text-indent:5px;">Offer Name</td>
                      <td style="text-indent:5px;">Type</td>
                      <td style="text-indent:5px;">Order Form</td>
                      <td style="text-indent:5px;">Send/Received</td>
                      <td style="text-indent:5px;">Status</td>
                      <td style="text-indent:5px;">Date</td>

                    </tr>
                  </thead>
                  <tbody id="catList" >
                    <?php 
                           $total=0;

                           if(!empty($getOffers)){ 
                                
                                 foreach($getOffers as $value) {
                                  if(!empty($value['order_form']) && file_exists('uploads/invoice/'.$value['order_form'])){
                                  ?>
                                  <tr class="pay_row_one">
                                   
                                    <?php if($value['from_id']) 
                                      { 
                                        $this->db->where('id' , $value['from_id']);
                                        $get_receiver_name = $this->master_model->getRecords('tbl_user_master');
                                      ?>
                                            <td style="padding:12px!important;"> 
                                               <?php if(!empty($get_receiver_name[0]['name'])) { echo $get_receiver_name[0]['name'].'('.$get_receiver_name[0]['user_type'].')'; } else { echo "Not Available"; } ?>  
                                            </td>

                                    <?php } ?>

                                    <?php if($value['to_id']) 
                                      { 
                                        $this->db->where('id' , $value['to_id']);
                                        $get_receiver_name = $this->master_model->getRecords('tbl_user_master');
                                      ?>
                                            <td style="padding:12px!important;"> 
                                               <?php if(!empty($get_receiver_name[0]['name'])) { echo $get_receiver_name[0]['name'].'('.$get_receiver_name[0]['user_type'].')'; } else { echo "Not Available"; } ?>  
                                            </td>

                                    <?php } ?>

                                    <td style="padding:12px!important;"> 
                                         <?php if(!empty($value['order_name'])) { echo $value['order_name']; } else { echo "Not Available"; } ?>
                                    </td>
                                    <td style="padding:12px!important;"> 
                                         <?php if(!empty($value['type'])) { echo $value['type']; } else { echo "Not Available"; } ?>
                                    </td>

                                    <td style="padding:12px!important;"> 
                                       <a class="btn-delete-block" href="<?php echo base_url().'uploads/invoice/'.$value['order_form'] ?>" download><i class="fa fa-cloud-download" aria-hidden="true"></i> Download pdf</a>
                                      
                                    </td>
                                    
                                    <?php if($value['from_id'] == $this->session->userdata('user_id')) 
                                      { ?>
                                            <td style="padding:12px!important;"> 
                                             send 
                                            </td>
                                      <?php
                                      } 
                                      else 
                                      { ?>
                                            <td style="padding:12px!important;"> 
                                               received 
                                            </td>

                                    <?php } ?>

                                      <td style="padding:12px!important;">  
                                          <?php 
                                            if(!empty($value['type']))
                                            {
                                                  if($value['type']=='offer')
                                                  {
                                                    $this->db->select('status');
                                                    $this->db->where('id' , $value['apply_offer_id']);
                                                    $offer_status = $this->master_model->getRecords('tbl_apply_for_offers');
                                                    
                                                    if(isset($offer_status[0]['status']) && !empty($offer_status[0]['status']) && $offer_status[0]['status']=='Accepted')
                                                    {
                                                      echo $offer_status[0]['status'];  
                                                    } 
                                                    else
                                                    {
                                                      echo "Pending";
                                                    } 
                                                  }
                                                  else if($value['type']=='product')
                                                  {
                                                    $this->db->select('status');
                                                    $this->db->where('id' , $value['apply_offer_id']);
                                                    $offer_status = $this->master_model->getRecords('tbl_apply_for_order');
                                                    
                                                    if(isset($offer_status[0]['status']) && !empty($offer_status[0]['status']) && $offer_status[0]['status']=='Accepted')
                                                    {
                                                      echo $offer_status[0]['status'];  
                                                    } 
                                                    else
                                                    {
                                                      echo "Pending";
                                                    } 
                                                  }
                                                  else if($value['type']=='requirement')
                                                  {
                                                    $this->db->select('status');
                                                    $this->db->where('id' , $value['apply_offer_id']);
                                                    $offer_status = $this->master_model->getRecords('tbl_apply_for_requirment');
                                                    
                                                    if(isset($offer_status[0]['status']) && !empty($offer_status[0]['status']) && $offer_status[0]['status']=='Accepted')
                                                    {
                                                      echo $offer_status[0]['status'];  
                                                    } 
                                                    else
                                                    {
                                                      echo "Pending";
                                                    } 
                                                  }


                                            } 
                                            ?>
                                    </td>

                                    <td style="padding:12px!important;">  <?php echo date('F j, Y', strtotime($value['date']));?>  
                                    </td>
                                   

                                  </tr>
                                  <?php  } } }
                                  else
                                  { ?>
                                    <tr class="pay_row_one">
                                      <td align="center" style="padding:3px!important;" colspan="8"> 
                                        <div >
                                              <?php  $this->load->view('no-data-found'); ?>
                                        </div>
                                      </td>
                                    </tr>
                                  <?php } ?>
                  </tbody>
                </table>
              </div>
              <div class="clear"></div>
            </div>

          </form>

        </div>
         <div class="product-pagi-block">
                <?php echo $this->pagination->create_links(); ?>
               </div>
      </div>
    </div>
    


<script type="text/javascript">
  $('.delete_buyers').click(function(){
        var seller_id = jQuery(this).data("seller");
        swal({   
             title: "Are you sure?",   
             text : "You want to delete this seller ?",  
             type : "warning",   
             showCancelButton: true,   
             confirmButtonColor: "#8cc63e",  
             confirmButtonText: "Yes",  
             cancelButtonText: "No",   
             closeOnConfirm: false,   
             closeOnCancel: false }, function(isConfirm){   
              if (isConfirm) 
              { 
                     swal("Deleted!", "Seller has been deleted.", "success"); 
                           location.href=site_url+"admin/buyers/delete/"+seller_id;
              } 
              else
              { 
                     swal("Cancelled", "Seller is safe :)", "error");          
              } 
            });
    }); 
</script>           