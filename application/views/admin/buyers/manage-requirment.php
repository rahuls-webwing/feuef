<!-- BEGIN Theme Setting -->
<!-- END Theme Setting --> 

<!-- BEGIN Navbar -->
<div id="navbar" class="navbar">
  <?php $this->load->view(ADMIN_PANEL_NAME.'top-navigation'); ?>
</div>
<!-- END Navbar --> 

<!-- BEGIN Container -->
<div class="container" id="main-container"> 
  <!-- BEGIN Sidebar -->
  <div id="sidebar" class="navbar-collapse collapse">
    <?php $this->load->view(ADMIN_PANEL_NAME.'left-navigation'); ?>
  </div>
  <!-- END Sidebar --> 
  <div id="main-content">
    <div class="page-title">
      <div><h1><i class="fa fa-user" aria-hidden="true"></i> <?php echo $pageTitle; ?></h1></div>
    </div>
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li><i class="fa fa-home"></i><a href="<?php echo base_url();?>admin/dashboard">Home</a> <span class="divider"> <i class="fa fa-angle-right"></i></span> </li>
        <li class="active"><?php echo $pageTitle; ?></li>
      </ul>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-magenta">
          <div class="box-title"><h3><i class="fa fa-user" aria-hidden="true"></i> <?php echo $pageTitle; ?></h3></div>
          <form name="frm-manage" id="frm-manage" method="post" action="">
            <div class="box-content">
            <span id="show_success" ></span>    
             <?php if($this->session->flashdata('error')!=''){  ?>
                <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php }if($this->session->flashdata('success')!=''){?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                <?php } ?>
                <div class="alert alert-danger" id="no_select" style="display:none;"></div>
                <div class="alert alert-warning" id="warning_msg" style="display:none;"></div>
              <div class="col-md-10">
                
              </div>
              <div class="btn-toolbar pull-right clearfix">
                <!-- <div class="btn-group">
                  
                  <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Active Subcategory" href="javascript:void(0);" onclick="javascript : return checkmultiaction('frm-manage','active');" style="text-decoration:none;"><i class="fa fa-unlock"></i></a>
                  <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Block Subcategory" href="javascript:void(0);" onclick="javascript : return checkmultiaction('frm-manage','block');"  style="text-decoration:none;"><i class="fa fa-lock"></i></a>
                  <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Delete Subcategory" href="javascript:void(0);" onclick="javascript : return checkmultiaction('frm-manage','delete');"  style="text-decoration:none;"><i class="fa fa-trash-o"></i></a>
                </div> -->
                <div class="btn-group"> <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Refresh" href="<?php echo base_url().ADMIN_PANEL_NAME.'buyers/reuirments/'.$this->uri->segment(4); ?>"style="text-decoration:none;"><i class="fa fa-repeat"></i></a> </div>
              </div>
              <br/><br/>
              <div class="clearfix"></div>
              <div class="table-responsive" style="border:0" id="showBlockUI">
                <input type="hidden" name="act_status" id="act_status" value="" />
                <table class="table table-condensed" <?php if(count($getrequirments)>0){?> id="table1"<?php } ?>>
                  <thead>
                    <tr>
                      <!-- <th style="width:18px"><input type="checkbox" name="mult_change" id="mult_change" value="delete" /></th> -->
                      <th>Requirment title</th>
                      <th>Requirment Price ( <?php echo CURRENCY; ?> )</th>
                      <th>Requirment Description</th>
                      <th>Address</th>
                      <th>Posted Date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="catList" >
                    <?php

                    if(count($getrequirments)>0) {
                     foreach($getrequirments as $row) {
                    ?>

                    <tr>
                      
                      <?php /*
                      <td style="width:18px">
                         <input type="checkbox" name="checkbox_del[]" id="checkbox_del" value="<?php echo $row['id']; ?>"/>
                      </td>
                      */?>
                      <td><?php if(isset($row['title'])) { echo stripslashes($row['title']);  } else { echo "Not Available"; }; ?></td>
                      <td><?php if(isset($row['price'])){ echo stripslashes(CURRENCY.$row['price']); } else { echo "Not Available"; }; ?></td>
                      <td><?php if(isset($row['description'])){ echo substr($row['description'], 0 , 50).'...'; } else { echo "Not Available"; }; ?></td>
                      <td><?php if(isset($row['location'])){ echo substr($row['location'], 0 , 50); } else { echo "Not Available"; }; ?></td>
                      <td><?php if(isset($row['created_date'])){ echo date('F j, Y', strtotime($row['created_date']))." at ".date("g:i a", strtotime($row['created_date'])); } else { echo "Not Available"; }; ?></td>
                      
                      <?php /*
                      <td>
                      <?php if($row['status']=='Unblock'){ ?>
                      <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Active" href="<?php echo base_url('admin/buyers/status/Block/'.$row['id']); ?>"  style="text-decoration:none;"><i class="fa fa-unlock"></i></a>
                      <?php } else { ?>
                      <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Block" href="<?php echo base_url('admin/buyers/status/Unblock/'.$row['id']); ?>" style="text-decoration:none;"><i class="fa fa-lock"></i></a>
                      <?php } ?>
                      </td>
                      */ ?>
                      

                      <td>
                       <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="View Details" href="<?php echo base_url('admin/buyers/requirment_detail/'.$row['id']); ?>" style="text-decoration:none;"><i class="fa fa-eye" aria-hidden="true"></i></a>

                       <?php /* <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip delete_buyers" title="Delete " data-seller="<?php echo $row['id'];  ?>"  style="text-decoration:none;" href="javascript:void(0);"><i class="fa fa-trash-o"></i></a> */ ?>

                      </td>
                    </tr>
                    <?php
                      }
                    }
                    else
                      echo '<tr><td colspan="6"><div class="alert alert-danger"><button data-dismiss="alert" class="close">×</button><strong>Error!</strong> Sorry no records found !</div></td></tr>';
                    ?>
                  </tbody>
                </table>
              </div>
              <div class="clear"></div>
            </div>
          </form>
        </div>
      </div>
    </div>
    


<script type="text/javascript">
  $('.delete_buyers').click(function(){
        var seller_id = jQuery(this).data("seller");
        swal({   
             title: "Are you sure?",   
             text : "You want to delete this seller ?",  
             type : "warning",   
             showCancelButton: true,   
             confirmButtonColor: "#8cc63e",  
             confirmButtonText: "Yes",  
             cancelButtonText: "No",   
             closeOnConfirm: false,   
             closeOnCancel: false }, function(isConfirm){   
              if (isConfirm) 
              { 
                     swal("Deleted!", "Seller has been deleted.", "success"); 
                           location.href=site_url+"admin/buyers/delete/"+seller_id;
              } 
              else
              { 
                     swal("Cancelled", "Seller is safe :)", "error");          
              } 
            });
    }); 
</script>           