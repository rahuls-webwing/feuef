<!-- BEGIN Theme Setting -->
<!-- <div id="theme-setting"> <?php //$this->load->view('admin/theme-setting'); ?> </div> -->
<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<div id="navbar" class="navbar"> <?php $this->load->view('admin/top-navigation'); ?> </div>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <div id="sidebar" class="navbar-collapse collapse"> <?php $this->load->view('admin/left-navigation'); ?> </div>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div> <h1><i class="fa fa-star"></i><?php echo $page_title; ?></h1> <h4></h4> </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i><a href="<?php echo base_url();?>admin/dashboard">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
        <li class=""><a href="<?php echo base_url();?>admin/course/manage">Manage Course</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active"><?php echo $page_title; ?></li>
      </ul>
    </div>

    <!-- END Breadcrumb -->
    <!-- BEGIN Main Content -->
     <div class="row">
        <div class="col-md-12">
            <?php if($this->session->flashdata('success')!=""){ ?>
            <div class="alert alert-success">
                <button class="close" data-dismiss="alert">×</button>
                <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php }if($this->session->flashdata('error')!=""){ ?>
            <div class="alert alert-danger">
                <button class="close" data-dismiss="alert">×</button>
                <strong>Error! </strong><?php echo strip_tags($this->session->flashdata('error')); ?>
            </div>
            <?php } ?>
            <div id="form-validation-error" style="display:none">
                <div class="alert alert-danger" >
                    <button class="close" data-dismiss="alert">×</button>
                    <!-- <strong>Error! </strong> Please fill required fields in both English and Arabic Tabs  -->
                    <strong>Error! </strong> Please fill required fields.

                </div>
            </div>
            <div class="box box-magenta">
                <div class="box-title">
                    <h3><i class="fa fa-th-list"></i><?php echo isset($page_title)?$page_title:"" ;?></h3>
                    <div class="box-tool">
                        <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                       <!--  <a data-action="close" href="#"><i class="fa fa-times"></i></a> -->
                    </div>
                </div>
                <div class="box-content">
                   <?php
                            $attr=array( "class"=>"form-horizontal", "id"=>"validation-form", "name"=>"frm-add-page" , "method"=>"post" , "enctype"=>"multipart/form-data");
                            echo form_open_multipart(base_url()."admin/course/edit/".$this->uri->segment(4)."",$attr);
                                        ?>
                    <div class="tabbable tabs-left" >
                        <div class="tab-content" id="myTabContent3" >
                            <div id="english" class="tab-pane fade active in">
                                <div class="form-group" style="margin-right:0 !important; margin-left:0 !important; ">
                                    <label class="col-sm-3 col-lg-2 control-label" for="page_title">Name<i class="red">*</i></label>
                                    <div class="col-sm-6 col-lg-4 controls">
                                        <input type="text"
                                               name="course_name_en"
                                               id="course_name_en"
                                               class="form-control"
                                               data-rule-minlength="3"
                                               placeholder="Please enter course name"
                                               value="<?php echo isset($course_details['course_name_en'])?$course_details['course_name_en']:'' ?>"/>
                                        <div class="error" id="err_course_name"><?php if(form_error('course_name_en')!=""){echo form_error('course_name_en');} ?></div>
                                    </div>
                                </div>
                                
                                <div class="form-group" style="margin-right:0 !important; margin-left:0 !important; ">
                                    <label class="col-sm-3 col-lg-2 control-label">Category Name<span style="color:#F00;">*</span></label>
                                    <div class="col-sm-6 col-lg-4 controls">
                                        <select class="form-control" id="category_name" name="category_name" data- tabindex="1" data-rule-required="true">
                                              <option value="">Select Category</option>
                                              <?php
                                              if(count($fetchcategory) > 0)
                                              {
                                                foreach ($fetchcategory as $fetchcategoryVal)
                                                {
                                                  ?>
                                                  <option value="<?php if(isset($fetchcategoryVal['category_id']) && $fetchcategoryVal['category_id']!=''){ echo $fetchcategoryVal['category_id']; } ?>" <?php if(isset($course_details['category_id']) && $course_details['category_id']!=''){ if($course_details['category_id'] == $fetchcategoryVal['category_id']){ echo "selected"; } } ?> > <?php if(isset($fetchcategoryVal['category_name']) && $fetchcategoryVal['category_name']!=''){ echo ucfirst($fetchcategoryVal['category_name']); } ?> </option>
                                                  <?php
                                                }
                                              }
                                              ?>
                                        </select>
                                        <div class="error" id="error_category_name" ><?php echo form_error('category_name'); ?></div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-right:0 !important; margin-left:0 !important; ">
                                    <label class="col-sm-3 col-lg-2 control-label" for="course_added_by">Added by<i class="red">*</i></label>
                                    <div class="col-sm-6 col-lg-4 controls">
                                        <input data-rule-ckvalidation="true"
                                                  name="course_added_by"
                                                  id="course_added_by"
                                                  rows="5"
                                                  class="form-control ckeditor"
                                                  data-rule-minlength="3"
                                                  placeholder="Please enter name "
                                                  value="<?php echo $course_details['course_added_by'];?>"/>
                                        <div class="error"  id="err_added_by"><?php if(form_error('course_added_by')!=""){echo form_error('course_added_by');} ?></div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-right:0 !important; margin-left:0 !important; ">
                                    <label class="col-sm-3 col-lg-2 control-label" for="course_description_en">Description<i class="red">*</i></label>
                                    <div class="col-sm-9 controls">
                                        <textarea data-rule-ckvalidation="true"
                                                  name="course_description_en"
                                                  id="course_description_en"
                                                  rows="5"
                                                  class="form-control ckeditor"
                                                  data-rule-minlength="10"
                                                  placeholder=" Please select description ">
                                                 <?php echo isset($course_details['course_description_en'])?$course_details['course_description_en']:'' ?>
                                        </textarea>
                                        <div class="error"  id="err_desc"><?php if(form_error('course_description_en')!=""){echo form_error('course_description_en');} ?></div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-right:0 !important; margin-left:0 !important; ">
                                  <label class="col-sm-3 col-lg-2 control-label">Image Upload</label>
                                  <div class="col-sm-9  controls">
                                   <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <input type='hidden' name='oldimage' id='oldimage' value="<?php echo $course_details['course_img']?>">
                                    <div class="fileupload-new img-thumbnail"    >
                                     <?php
                                               if(!empty($course_details['course_img']))
                                                {
                                                    ?>
                                                        <img src="<?php echo base_url().'timthumb.php?src='.base_url().'uploads/course_images/'.$course_details['course_img'].'&h=400&w=500&zc=0'; ?>">
                                                        <!-- <img   src="<?php echo base_url().'uploads/course_images/'.$course_details['course_img'];?>" alt="" /> -->
                                                    <?php
                                                }
                                                else
                                                {
                                                    ?>
                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                    <?php
                                                }
                                     ?>
                                    </div>
                                           <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div class="" id="photo_error">
                                              </div>
                                           <div>
                                               <span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span>
                                               <span class="fileupload-exists">Change</span>
                                               <input type="file" id="fileUpload" class="file-input" name="course_logo" /></span>
                                               <a  id="removeButton" href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                    </div>
                     <?php
                    $course_data=$course_details['course_added_date'];
                    
                    $date=date('m/d/Y',strtotime($course_data));
                    ?>
                    <div class="form-group" style="margin-right:0 !important; margin-left:0 !important; ">
                                    <label class="col-sm-3 col-lg-2 control-label" for="date_en">Date<i class="red">*</i></label>
                                    <div class="col-sm-6 col-lg-3 controls">
                                        <input data-rule-ckvalidation="true"
                                                  name="course_date"
                                                  id="course_date"
                                                  rows="5"
                                                  class="form-control ckeditor"
                                                  data-rule-minlength="10"
                                                  placeholder="Please select date"
                                                  value="<?php echo $date; ?>"/>
                                        <div class="error" id="err_date"><?php if(form_error('course_date')!=""){echo form_error('course_date');} ?></div>
                                    </div>
                                </div>
                            </div>
                          </div>
                        <div class="row">
                        <div class="col-md-12">
                            <div class="col-sm-6  col-lg-5">
                            </div>
                            <div class="col-sm-6  col-lg-7">
                                    <input type="submit" name="course_edit" id="course_edit" class="btn btn-primary" value="Update" >
                            </div>
                        </div>
                    </div>
                    <?php
                        echo form_close();
                    ?>
                    </div>
                  </div>
        </div>
    </div>
<?php $this->load->view('admin/top-footer'); ?>
</div>
</div>

<script type="text/javascript">
  $("#course_date" ).datepicker({
   changeYear: true,
   changeMonth: true,
   inline: true,
});


//update Course Page 
  $('#course_edit').on('click',function(){
    var course_name_en=$('#course_name_en').val();
    var course_date=$('#course_date').val();
    var course_added_by=$('#course_added_by').val();
    var desc=CKEDITOR.instances['course_description_en'].getData().replace(/<[^>]*>/gi, '').length;
    var fileUpload=$('#fileUpload').val();
    var ext_a=fileUpload.substring(fileUpload.lastIndexOf('.') + 1);
    
    var flag=1;
     $('#err_course_name').html('');
     $('#err_date').html('');
     $('#err_image').html('');
     $('#err_desc').html('');
     $('#err_added_by').html('');
    if(course_name_en.trim()=='')
    {
      $('#err_course_name').html('Please Enter Course Name.');
      flag=0;
    }
    if(course_date.trim()=='')
    {
      $('#err_date').html('Please Select Date.');
      flag=0;
    }
    if(course_added_by.trim()=='')
    {
      $('#err_added_by').html('Please enter name.');
      flag=0;
    }
    if(desc=='')
    {
      $('#err_desc').html('Please Enter Description.');
      flag=0;
    }
    if(flag==1)
    {
      return true;
    }
    else
    {
      return false;
    }

  });
</script>