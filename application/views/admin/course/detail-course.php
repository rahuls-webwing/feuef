<!-- BEGIN Theme Setting -->
<!-- <div id="theme-setting"> <?php //$this->load->view('admin/theme-setting'); ?> </div> -->
<!-- END Theme Setting -->
<!-- BEGIN Navbar -->
<div id="navbar" class="navbar"> <?php $this->load->view('admin/top-navigation'); ?> </div>
<!-- END Navbar -->
<!-- BEGIN Container -->
<div class="container" id="main-container">
  <!-- BEGIN Sidebar -->
  <div id="sidebar" class="navbar-collapse collapse"> <?php $this->load->view('admin/left-navigation'); ?> </div>
  <!-- END Sidebar -->
  <!-- BEGIN Content -->
  <div id="main-content">
    <!-- BEGIN Page Title -->
    <div class="page-title">
      <div> <h1><i class="fa fa-star"></i><?php echo $page_title; ?></h1> <h4></h4> </div>
    </div>
    <!-- END Page Title -->
    <!-- BEGIN Breadcrumb -->
    <div id="breadcrumbs">
      <ul class="breadcrumb">
        <li> <i class="fa fa-home"></i><a href="<?php echo base_url();?>admin/dashboard">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
         <li class=""><a href="<?php echo base_url();?>admin/course/manage">Manage Course</a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
        <li class="active"><?php echo $page_title; ?></li>
      </ul>
    </div>

    <!-- END Breadcrumb -->
    <!-- BEGIN Main Content -->
    <div class="row">
    <div class="col-md-12">
        <div class="box box-magenta ">
            <div class="box-title">
                <h3><i class="fa fa-th-large"></i><?php echo ucfirst($page_title); ?></h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
              <form method="post" class="form-horizontal" id="validation-form" enctype="multipart/form-data">
                  <div class="form-group">
                    <div class="col-sm-12">
                    <?php if($this->session->flashdata('error')!=''){  ?>
                     <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                     <?php }
                     if($this->session->flashdata('success')!=''){?>
                     <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
             <?php } ?>
                     </div>
          </div>
                  <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Image:</label>
                      <div class="col-sm-9 col-lg-10 controls">
                        <img style="height:300px;width:500px;" src="<?php echo base_url().'uploads/course_images/'.$fetchdata[0]['course_img']?>">
                      </div>
                  </div>
                  <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Course Name:</label>
                      <div class="col-sm-9 col-lg-10 controls">
                         <?php if(!empty($fetchdata[0]['course_name_en'])){echo ucwords($fetchdata[0]['course_name_en']); }?>
                       </div>
                  </div>
                   <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Added By:</label>
                      <div class="col-sm-9 col-lg-10 controls">
                         <?php if(!empty($fetchdata[0]['course_added_by'])){echo ucwords($fetchdata[0]['course_added_by']); }?>
                       </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Course Description:</label>
                      <div class="col-sm-9 col-lg-10 controls">
                          <?php if(!empty($fetchdata[0]['course_description_en'])){   echo $mytext = chunk_split( preg_replace('/(<a.*?)(class="something")\s(.*?data-class="false".*?>)|(<a.*?)(data-class="false".*?)\s(class="something")(.*?>)/', '$1$3$4$5$7', strip_tags($fetchdata[0]['course_description_en']) ) ); }?>


                      </div>
                  </div>
                 <?php $date=$fetchdata[0]['course_added_date'];
                  $date=date('Y-m-d',strtotime($date));?>
                  <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Date:</label>
                      <div class="col-sm-9 col-lg-10 controls">
                          <?php if(!empty($fetchdata[0]['course_added_date'])){echo $date;} ?>
                      </div>
                  </div>
                  <div class="form-group">
                     <label class="col-sm-3 col-lg-2 control-label">&nbsp;</label>
                      <div class="col-sm-9 col-lg-10 controls">
                      <a class="btn" align="right"  href="<?php echo base_url().'admin/course/manage';?>" >Back </a>
                      </div>
                  </div>
               </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/top-footer'); ?>
</div>
</div>
<script type="text/javascript">
function checkValidation()
{
   if($("#validation-form").valid()==false)
   {
        $("#form-validation-error").show();
        setTimeout(function()
        {
           $("#form-validation-error").hide();
        }, 8000);
   }
}
</script>
<script type="text/javascript">
  $("#course_date" ).datepicker({
   changeYear: true,
   changeMonth: true,
   inline: true,
});

</script>