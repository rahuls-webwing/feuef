<script type="text/javascript" src="<?php echo base_url(); ?>/assets/ckeditor/ckeditor.js"></script> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/back-panel/bootstrap-fileupload.css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/back-panel/bootstrap-fileupload.min.js"></script>

<?php $this->load->view(lcfirst($this->session->userdata('user_type')).'/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3><?php echo isset($page_title)?$page_title:"" ;?></h3>
        </div>
    </div>
</div>
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
             <div class="row">
                <?php $this->load->view(lcfirst($this->session->userdata('user_type')).'/left-bar'); ?>
                <div class="col-sm-9 col-md-9 col-lg-9">
                    <div class="account-info-block">
                    <?php $this->load->view('status-msg'); ?>
                    


                    <div class="">
                     <?php
                              $attr=array( "class"=>"form-horizontal", "id"=>"validation-form", "name"=>"frm-add-page" , "method"=>"post" , "enctype"=>"multipart/form-data");
                              echo form_open_multipart(base_url()."blogs/edit/".$this->uri->segment(3)."",$attr);
                                          ?>
                      <div class="" >
                          <div class="" id="myTabContent3" >
                              <div id="english" class="tab-pane fade active in">
                                  <div class="form-group" style="margin-right:0 !important; margin-left:0 !important; ">
                                      <label class="col-sm-3 col-lg-2 control-label" for="page_title">Name<i style="color:red;">*</i></label>
                                      <div class="col-sm-6 col-lg-6 controls">
                                          <input type="text"
                                                 name="blogs_name_en"
                                                 id="blogs_name_en"
                                                 class="form-control"
                                                 data-rule-minlength="3"
                                                 placeholder="Please enter blog name"
                                                 value="<?php echo isset($blogs_details['blogs_name_en'])?$blogs_details['blogs_name_en']:'' ?>"/>
                                          <div class="error" id="err_blogs_name"><?php if(form_error('blogs_name_en')!=""){echo form_error('blogs_name_en');} ?></div>
                                      </div>
                                  </div>
                                  <div class="form-group" style="margin-right:0 !important; margin-left:0 !important; display:none;">
                                      <label class="col-sm-3 col-lg-2 control-label" for="blogs_added_by">Added by<i style="color:red;">*</i></label>
                                      <div class="col-sm-6 col-lg-6 controls">
                                          <input data-rule-ckvalidation="true"
                                                    name="blogs_added_by"
                                                    id="blogs_added_by"
                                                    rows="5"
                                                    class="form-control ckeditor"
                                                    data-rule-minlength="3"
                                                    placeholder="Please enter name "
                                                    value="<?php echo $blogs_details['blogs_added_by'];?>"/>
                                          <div class="error"  id="err_added_by"><?php if(form_error('blogs_added_by')!=""){echo form_error('blogs_added_by');} ?></div>
                                      </div>
                                  </div>
                                  <div class="form-group" style="margin-right:0 !important; margin-left:0 !important; ">
                                      <label class="col-sm-3 col-lg-2 control-label" for="blogs_description_en">Description<i style="color:red;">*</i></label>
                                      <div class="col-sm-9 controls">
                                          <textarea data-rule-ckvalidation="true"
                                                    name="blogs_description_en"
                                                    id="blogs_description_en"
                                                    rows="5"
                                                    class="form-control ckeditor"
                                                    data-rule-minlength="10"
                                                    placeholder=" Please select description ">
                                                   <?php echo isset($blogs_details['blogs_description_en'])?$blogs_details['blogs_description_en']:'' ?>
                                          </textarea>
                                          <div class="error"  id="err_desc"><?php if(form_error('blogs_description_en')!=""){echo form_error('blogs_description_en');} ?></div>
                                      </div>
                                  </div>



                                  <div class="form-group" style="margin-right:0 !important; margin-left:0 !important; " >
                                    <label class="col-sm-3 col-lg-2 control-label">Image Upload</label>
                                    <div class="col-sm-9  controls">
                                     <div class="fileupload fileupload-new" data-provides="fileupload">
                                      <input type='hidden' name='oldimage' id='oldimage' value="<?php echo $blogs_details['blogs_img']?>">
                                      <div class="fileupload-new img-thumbnail"    >
                                       <?php
                                              if(!empty($blogs_details['blogs_img']))
                                              {
                                                  ?>
                                                      <img src="<?php echo base_url().'timthumb.php?src='.base_url().'uploads/blogs_images/'.$blogs_details['blogs_img'].'&h=150&w=200&zc=0'; ?>">
                                                  <?php
                                              }
                                              else
                                              {
                                                  ?>
                                                      <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                  <?php
                                              }
                                       ?>
                                      </div>
                                             <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                              <div class="" id="photo_error">
                                                </div>
                                             <div>
                                                 <span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span>
                                                 <span class="fileupload-exists">Change</span>
                                                 <input type="file" id="fileUpload" class="file-input" name="blogs_logo" /></span>
                                                 <a  id="removeButton" href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                             </div>
                                         </div>
                                     </div>
                                 </div>



                      </div>
                       <?php
                       $blogs_data=$blogs_details['blogs_added_date'];
                       $date=date('m/d/Y',strtotime($blogs_data));
                       ?>
                       <div class="form-group" style="margin-right:0 !important; margin-left:0 !important; display:none;">
                                      <label class="col-sm-3 col-lg-2 control-label" for="date_en">Date<i style="color:red;">*</i></label>
                                      <div class="col-sm-6 col-lg-3 controls">
                                          <input data-rule-ckvalidation="true"
                                                    name="blogs_date"
                                                    id="blogs_date"
                                                    rows="5"
                                                    class="form-control ckeditor"
                                                    data-rule-minlength="10"
                                                    placeholder="Please select date"
                                                    value="<?php echo $date; ?>"/>
                                          <div class="error" id="err_date"><?php if(form_error('blogs_date')!=""){echo form_error('blogs_date');} ?></div>
                                      </div>
                                  </div>
                              </div>
                            </div>
                          <div class="row">
                          <div class="col-md-12">
                              <div class="col-sm-6  col-lg-5">
                              </div>
                              <div class="col-sm-6  col-lg-7">
                                      <input type="submit" name="blogs_edit" id="blogs_edit" class="change-btn-pass pull-right" value="Update" >
                              </div>
                          </div>
                       </div>
                       <?php
                          echo form_close();
                       ?>
                       </div>
                    </div>   


             <div class="clr"></div>
           </div>
          </div>
        </div>
    </div>    
</div>

<!-- DatePiker -->

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepiker/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/datepiker/jquery-1.12.4.js"></script>
<script src="<?php echo base_url(); ?>assets/datepiker/1.12.1.js"></script>
<script>
$( function() {
$( "#blogs_date" ).datepicker().datepicker("setDate", new Date());
} );
</script>

<!-- End DatePiker -->

<script type="text/javascript">
//add Blog  Page 
  $('#blogs_add').on('click',function(){

    var blogs_name_en=$('#blogs_name_en').val();
    var blogs_date=$('#blogs_date').val();
    var blogs_added_by=$('#blogs_added_by').val();
    var desc=CKEDITOR.instances['blogs_description_en'].getData().replace(/<[^>]*>/gi, '').length;
    var fileUpload=$('#fileUpload').val();
    var ext_a=fileUpload.substring(fileUpload.lastIndexOf('.') + 1);
    
    var flag=1;
     $('#err_blogs_name').html('');
     $('#err_date').html('');
     $('#err_image').html('');
     $('#err_desc').html('');
     $('#err_added_by').html('');
     

    if(blogs_name_en.trim()=='')
    {
      $('#err_blogs_name').html('Please Enter Blog  Name.');
      flag=0;
    }
    /*if(blogs_date.trim()=='')
    {
      $('#err_date').html('Please Select Date.');
      flag=0;
    }*/
    /*if(blogs_added_by.trim()=='')
    {
      $('#err_added_by').html('Please enter name.');
      flag=0;
    }*/
    if(desc=='')
    {
      $('#err_desc').html('Please Enter Description.');
      flag=0;
    }
    /* if(fileUpload.trim()=='')
    {
      $('#err_image').html('Please Select Image.');
      flag= 0;
    }
    else
    if(!(ext_a == "jpg" || ext_a == "jpeg" || ext_a == "gif" || ext_a == "png" || ext_a == "GIF" || ext_a == "JPG" || ext_a == "JPEG" || ext_a == "PNG"))
    {
        $('#err_image').html('Only jpg, png, gif, jpeg type images is allowed');
      flag=0;
    }*/
    
    if(flag==1)
    {
      return true;
    }
    else
    {
      return false;
    }

  });
</script>        