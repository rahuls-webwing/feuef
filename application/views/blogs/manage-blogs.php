<script type="text/javascript" src="<?php echo base_url(); ?>/assets/ckeditor/ckeditor.js"></script> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/back-panel/bootstrap-fileupload.css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/back-panel/bootstrap-fileupload.min.js"></script>

<?php $this->load->view(lcfirst($this->session->userdata('user_type')).'/top-bar'); ?>
<div class="page-head-name">
    <div class="container">
        <div class="name-container-dash">
            <h3><?php echo isset($page_title)?$page_title:"" ;?></h3>
        </div>
    </div>
</div>
<div class="middel-container">
    <div class="inner-content-block">
        <div class="container">
             <div class="row">
                <?php $this->load->view(lcfirst($this->session->userdata('user_type')).'/left-bar'); ?>
                <div class="col-sm-9 col-md-9 col-lg-9">
                    <div class="account-info-block">
                    <?php $this->load->view('status-msg'); ?>
                    
                      <div class="box-content">
                        <div class="clearfix"></div>
                        <div class="table-responsive" style="border:0" id='manage_service_tbl_data'>
                          <?php
                          ?>
                          <input type="hidden" name="act_status" id="act_status" value="" />
                          <table class="table table-advance"  id="table1" >
                            <thead>
                              <tr>
                                <!-- <th style="width:18px" > <input type="checkbox" name="mult_change" id="mult_change" value="delete" /></th> -->
                               <!--  <th>Image</th> -->
                                <th>Title  </th>
                                <!-- <th>Added By  </th> -->
                                <th>Description </th>
                                <!-- <th>Front Status</th> -->
                                <th>Status</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                            <?php if($fetchdata)
                                  {
                                    foreach ($fetchdata as $blogs)
                                    {
                                      $status = ($blogs['blogs_status']=="1")?"Active":"In-Active";
                                      $front_status = "";

                                        if($blogs['blogs_front_status']=="1")
                                        {
                                            $front_status = "<a class='btn btn-success' title='Click here to Make it Hidden'
                                                                href='".base_url()."blogs/toggle_visibility/".base64_encode($blogs['blogs_id'])."/0'
                                                                >
                                                                 Visible
                                                            </a>";
                                        }
                                        elseif($blogs['blogs_front_status']=="0")
                                        {
                                            $front_status = "<a class='btn btn-danger' title='Click here to Make it Visible'
                                                                href='".base_url()."blogs/toggle_visibility/".base64_encode($blogs['blogs_id'])."/1'
                                                                >
                                                                 Hidden
                                                            </a>";
                                        }
                                        /* blogs Visibility*/
                                        
                                          $op_panel ='<a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Active blog"   style="cursor:text;text-decoration:none;">
                                                         <span class="label label-success">Approved</span>
                                                    </a>';

                                            
                                          $edit ='<a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip" title="Edit blog" href="'.base_url().'blogs/edit/'.base64_encode($blogs["blogs_id"]).'"  style="text-decoration:none;">
                                          <span class="label label-success"><i class="fa fa-edit"></i></span>
                                          </a>'; 
                                        

                                        $adv_actions='

                                        '.$edit.'

                                        <a class="btn btn-circle btn-to-success btn-bordered btn-fill show-tooltip delete_selected_blog" data-blogsid='.base64_encode($blogs["blogs_id"]).' href="javascript:void(0);" title="Delete blog"  style="text-decoration:none;" >
                                                        <span class="label label-danger"><i class="fa fa-trash-o" ></i></span>
                                                        </a>';
                                         echo "<tr>";?>
                                          <?php  if(!empty($blogs['blogs_img']) && file_exists('uploads/blogs_images/'.$blogs['blogs_img'])){?>
                                            <td> <img width='100px' height='100px'  src='<?php echo base_url().'uploads/blogs_images/'.$blogs['blogs_img'];?>' alt="" /></td>
                                          <?php } else { ?>
                                            <td><img width='100px' height='100px' src="<?php echo base_url().'images/large_no_image.jpg'; ?>" alt="" /></td> 
                                          <?php } ?> 
                                         <?php
                                         echo "<td> {$blogs['blogs_name_en']} </td>";
                                         echo "<td> ".substr(strip_tags($blogs['blogs_description_en']),0,60)."..." ."</td>";
                                         echo "<td> {$op_panel} </td>";
                                         echo  "<td> {$adv_actions} </td>";
                                         echo "</tr>";
                                    }// foreach ends here
                                 }else{
                                  ?>
                                    <tr>
                                      <td colspan="5"><?php $this->load->view('no-data-found'); ?></td>
                                    </tr>
                                  <?php
                                 }
                               ?>
                            </tbody>
                          </table>
                       </div>
                      </div>   
                    <div class="clr"></div>
                  </div>
                    <!--pagigation start here-->
                    <div style="margin-top:-12px;">
                      <?php echo $this->pagination->create_links(); ?>
                    </div>
                    <!--pagigation end here-->
              </div>
            </div>
          </div>    
       </div>

<script type="text/javascript">
  
  $('.delete_selected_blog').click(function(){
        var blogsid = jQuery(this).data("blogsid");
        swal({   
             title: "Are you sure?",   
             text: "You want to delete this blog ?",  
             type: "warning",   
             showCancelButton: true,   
             confirmButtonColor: "#8cc63e",  
             confirmButtonText: "Yes",  
             cancelButtonText: "No",   
             closeOnConfirm: false,   
             closeOnCancel: false }, function(isConfirm){   
              if (isConfirm) 
              { 
                     swal("Deleted!", "Your blog has been deleted.", "success"); 
                           location.href=site_url+"blogs/toggle_status/"+blogsid+'/'+'2';
              } 
              else
              { 
                     swal("Cancelled", "Your blog is safe :)", "error");          
                } 
            });
    }); 

</script>     
<!-- DatePiker -->

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepiker/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/datepiker/jquery-1.12.4.js"></script>
<script src="<?php echo base_url(); ?>assets/datepiker/1.12.1.js"></script>
<script>
$( function() {
$( "#blogs_date" ).datepicker();
} );
</script>

<!-- End DatePiker -->

<script type="text/javascript">
//add Blog  Page 
  $('#blogs_add').on('click',function(){

    var blogs_name_en=$('#blogs_name_en').val();
    var blogs_date=$('#blogs_date').val();
    var blogs_added_by=$('#blogs_added_by').val();
    var desc=CKEDITOR.instances['blogs_description_en'].getData().replace(/<[^>]*>/gi, '').length;
    var fileUpload=$('#fileUpload').val();
    var ext_a=fileUpload.substring(fileUpload.lastIndexOf('.') + 1);
    
    var flag=1;
     $('#err_blogs_name').html('');
     $('#err_date').html('');
     $('#err_image').html('');
     $('#err_desc').html('');
     $('#err_added_by').html('');
     

    if(blogs_name_en.trim()=='')
    {
      $('#err_blogs_name').html('Please Enter Blog  Name.');
      flag=0;
    }
    if(blogs_date.trim()=='')
    {
      $('#err_date').html('Please Select Date.');
      flag=0;
    }
    if(blogs_added_by.trim()=='')
    {
      $('#err_added_by').html('Please enter name.');
      flag=0;
    }
    if(desc=='')
    {
      $('#err_desc').html('Please Enter Description.');
      flag=0;
    }
     if(fileUpload.trim()=='')
    {
      $('#err_image').html('Please Select Image.');
      flag= 0;
    }
    else
    if(!(ext_a == "jpg" || ext_a == "jpeg" || ext_a == "gif" || ext_a == "png" || ext_a == "GIF" || ext_a == "JPG" || ext_a == "JPEG" || ext_a == "PNG"))
    {
        $('#err_image').html('Only jpg, png, gif, jpeg type images is allowed');
      flag=0;
    }
    
    if(flag==1)
    {
      return true;
    }
    else
    {
      return false;
    }

  });
</script>        