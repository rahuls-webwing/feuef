<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

	public function __construct() {

		parent::__construct();
		$this->load->model('email_sending');
		if($this->session->userdata('user_id') !=""){
			redirect(base_url().lcfirst($this->session->userdata('user_type')).'/dashboard');
		}
	}
	public function index()
	{
       // / print_r($_POST()); die();

        $data['pageTitle']       ='Sign-up - '.PROJECT_NAME;
   	    $data['page_title']      ='Sign-up - '.PROJECT_NAME;
   	    $data['middle_content']  ='sign-up';
	    $this->load->view('template',$data);
	}
  
	public function register()
	{

	    	$name                              = $this->input->post('name');
        $lname                             = $this->input->post('lname');
        $email                             = $this->input->post('email');
        $mobile_number                     = $this->input->post('mobile');
        $address                           = $this->input->post('address');
        $city                              = $this->input->post('city');
        $longitude                         = $this->input->post('longitude');
        $latitude                          = $this->input->post('latitude');
        $state                             = $this->input->post('state');
        $country                           = $this->input->post('country');
        $password                          = $this->input->post('pwd');
        $user_type                         = $this->input->post('type');

        if(isset($longitude) && !empty($longitude) ) 
        {
          $lng  = $longitude;
        }
        else{
          $lng  = '';   
        }

        if(isset($latitude) && !empty($latitude) ) 
        {
          $lat  = $latitude;
        }
        else{
          $lat  = '';   
        }


        $confirm_code = sha1(uniqid().$email);
        $arr_Data  =array(

                    'name'                              => $name,
                    'lname'                              => $lname,
                    'email'                             => $email,
                    'mobile_number'                     => $mobile_number,
                    'address'                           => $address,
                    'latitude'                          => $lat,
                    'longitude'                         => $lng,
                    'city'                              => $city,
                    'state'                             => $this->input->post('state'),
                    'country'                           => $country,
                    'password'                          => sha1($password),
                    'user_type'                         => $user_type,
                    'verification_status'               => 'Unverified',
                    'status'                            => 'Unblock',
                    'confirm_code'                      => $confirm_code,
                    'created_date'                      => date('Y-m-d H:m:s')
        );


        /* Check User Already Exist */
        $arr_email_dump=$this->master_model->getRecords('tbl_user_master',array('email'=>trim($email)));
    		if(sizeof($arr_email_dump) > 0)
    		{
    			if($arr_email_dump[0]["verification_status"] == "Unverified")
    			{
    				$arr_response['status'] = "error";
    				$arr_response['msg']    = "User already exits. your verification is pending. Please check verification mail in your email";
    				echo json_encode($arr_response);
    				exit;
    			}
    			else
    			{
    				$arr_response['status'] = "error";
    				$arr_response['msg']    = "Email Already Exits";
    				echo json_encode($arr_response);
    				exit;
    			}
    		}
            /* Insert DaTa */
        if($this->master_model->insertRecord('tbl_user_master',$arr_Data)){

        	$last_inserted_id  =  $this->db->insert_id();
            /* mail send */
        	$admin_email   	   =  $this->master_model->getRecords('admin_login', array('id'=>'1'));
        	$other_info        =  array(
								  "user_name"    => $name,
								  "user_email"   => $email,
								  "confirm_code" => $confirm_code,
								  "message"      =>"You Are Registered Successfully.",
    			);
    			$info_arr          = array(
    								  'from' 		 => $admin_email[0]['admin_email'],
    								  'to'		     => $email,
    								  'subject'	     => PROJECT_NAME.' - Account Creation',
    								  'view'		 => 'user-activation'
    			);
          $send_mail = $this->email_sending->sendmail($info_arr,$other_info);


          

  			if($send_mail == 'send'){
                $admin_email       =  $this->master_model->getRecords('admin_login', array('id'=>'1'));
                $other_info        =  array(
                        "user_name"    => $name,
                        "user_email"   => $email
                );
                $info_arr          = array(
                              'from'         => $admin_email[0]['admin_email'],
                              'to'           => $email,
                              'subject'      => PROJECT_NAME.' - Wellcome',
                              'view'         => 'wellcome-message'
                );
                $send_mail = $this->email_sending->sendmail($info_arr,$other_info);


                /* buyer 3 free requirments free entry */

                   if($this->input->post('type')=="Buyer"){

                   	 $arr_post_requirmnet_data = array(

                   	 	'buyer_id'   => $last_inserted_id,
                   	 	'total'      => '3',
                   	 	'available'  => '3',
                   	 	'competed'   => '0',
                   	 	'status'     => 'Unblock',
                   	 );
                   	 $this->db->insert('tbl_buyer_post_requirement_count',$arr_post_requirmnet_data);

                   	 $arr_offer_for_sellers_offer_data = array(

                   	 	'buyer_id'   => $last_inserted_id,
                   	 	'total'      => '3',
                   	 	'available'  => '3',
                   	 	'competed'   => '0',
                   	 	'status'     => 'Unblock',
                   	 );
                   	 $this->db->insert('tbl_buyer_offer_count',$arr_offer_for_sellers_offer_data);
                   }


                /* end buyer 3 free requirments free entry */
                /* seller 3 free product free entry */

                   if($this->input->post('type')=="Seller"){

                   	 $arr_post_requirmnet_data = array(

                   	 	'seller_id'   => $last_inserted_id,
                   	 	'total'      => '3',
                   	 	'available'  => '3',
                   	 	'competed'   => '0',
                   	 	'status'     => 'Unblock',
                   	 );
                   	 $this->db->insert('tbl_seller_upload_product_count',$arr_post_requirmnet_data);

                   	 $arr_offer_for_requirmnet_data = array(

                   	 	'seller_id'   => $last_inserted_id,
                   	 	'total'      => '3',
                   	 	'available'  => '3',
                   	 	'competed'   => '0',
                   	 	'status'     => 'Unblock',
                   	 );
                   	 $this->db->insert('tbl_seller_offer_requirments_count',$arr_offer_for_requirmnet_data);

                   }
                /* end seller 3 free product free entry */


                /* Mail To  Admin */
                $this->db->where('id',1);
                $email_info=$this->master_model->getRecords('admin_login');
                if(isset($email_info) && sizeof($email_info)>0)
                {
                $admin_contact_email=$email_info[0]['admin_email'];
                $user_info = array(
                          "name"               => $name,
                          "email"              => $email,
                          "subject"            => 'New user registration',
                          "mobile_no"          => $mobile_number,

                );
                $info_arr   =array(
                          'from'             => $admin_contact_email,
                          'to'               => $admin_contact_email,
                          'subject'          => 'New user registration - '.PROJECT_NAME,
                          'view'             => 'new-user-register-mail-to-admin'
                );
                $this->email_sending->sendmail($info_arr,$user_info);
                }
              /* End Mail To  Admin */
               
				$arr_response['status'] = "success";
				$arr_response['msg']    = "Thanks for creating your ".PROJECT_NAME." account. A verification email is on its way to ".$email." Please check your email and verify your account using the link provided in the email.";
				echo json_encode($arr_response);
				exit;
			} else  if($send_mail == 'not send'){

        $this->db->where('email' ,$email);
				$this->db->delete('tbl_user_master');
				$arr_response['status'] = "error";
				$arr_response['msg']    = "Network problem occured please try again";
				echo json_encode($arr_response);
				exit;
			} 	

        } else {

        	$arr_response['status'] = "error";
			$arr_response['msg']    = "Problem occured please try again";
			echo json_encode($arr_response);
			exit;

        }
	}

  public function fb_login()
  {
   
      $email = $this->input->post('email');
      $name = $this->input->post('name');
      $lname = $this->input->post('lname');
      
      $user_cnt = $this->master_model->getRecordCount('tbl_user_master',array('email'=>$email));

      //print_r($user_cnt); die;

      /*User Does not exists  and register user on website*/
      if($user_cnt == 0)
      {
        $string = '0123456789';
        $string_shuffled = str_shuffle($string);
        $password = substr($string_shuffled, 1, 4);
        $insert_array = array('name'=>$name,'lname'=>$lname,'email'=>$email,'password'=>$password,'verification_status' =>'Verified','status'=>'Unblock');

        //print_r($insert_array); die;
        
       // $logout_url = $this->facebook->getLogoutUrl(array('next' => base_url() . 'signup/fblogout'));
        if($last_insert_id = $this->master_model->insertRecord('tbl_user_master',$insert_array,true))
        {

          // /echo'hi mayur'; die;
          $admin_info = $this->master_model->getRecords('tbl_user_master',array('id'=>1));
          $email_format = $this->master_model->getRecords('tbl_email_templates',array('format_id'=>4));

          
          $info_arr = array('from'=>$admin_info[0]['email'],'to'=>$email,'view'=>'social_login_emailer','subject'=>$email_format[0]['email_subject']);
          $other_arr = array('name'=>$name,'lname'=>$lname,'email_content'=>$email_format[0]['email_content'],'email_signature'=>$email_format[0]['email_signature']);

         // print_r($other_arr); die;
          
          if($this->email_sending->sendmail($info_arr,$other_arr))
          {
            $resp['result'] = 'registration_success';
            $resp['message'] = $this->lang->line('msg_your_registration_has_been_done_successfully');
            //$resp['logout_url'] = $logout_url;
            $userdata = array('user_id'=>$last_insert_id,
                     'name'=>$name,
                     'lname'=>$lname,
                     'email'=>$email,
                     );

            $this->session->set_userdata($userdata);
          }
          else 
          {
            $resp['result'] = 'registration_error';
            $resp['message'] = $this->lang->line('msg_failed_to_register_please_try_after_sometime') ;
            //$this->session->set_flashdata('error','Failed to register.Please try after sometime');
          }
        }
        else 
        {
          $resp['result'] = 'registration_error';
          $resp['message'] = $this->lang->line('msg_failed_to_register_please_try_after_sometime') ;
          //$this->session->set_flashdata('error','Failed to register.Please try after sometime');
        }
      }
      else 
      {
        $user_info = $this->master_model->getRecords('tbl_user_master',array('email'=>$email));
        if(count($user_info)>0)
        {
          if($user_info[0]['status'] == 'Block')
          {
            $resp['result'] = 'error';
            $resp['message'] = $this->lang->line('msg_oooppss_unfortunately_you_are_blocked_by_admin');
          }
          else if($user_info[0]['status'] == 'Delete')
          {
            $resp['result'] = 'error';
            $resp['message'] = $this->lang->line('msg_oooppss_you_are_no_longer_registered_member_of_this_website');
          }
          else if($user_info[0]['verification_status'] == 'Unverified')
          {
            $resp['result'] = 'error';
            $resp['message'] = $this->lang->line('msg_please_verify_your_account_before_login');
          }
          else 
          {
            $userdata = array('id'=>$user_info[0]['id'],
                     'name'=>$user_info[0]['name'],
                     'lname'=>$user_info[0]['lname'],
                     'email'=>$user_info[0]['email']
                     );

            $this->session->set_userdata($userdata);
            $resp['result'] = 'login_success';
            $resp['message'] = $this->lang->line('msg_login_successfull') ;
           // $resp['logout_url'] = $logout_url;
          }
        }
        else 
        {
          $resp['error'] = 'error';
          $resp['message'] = $this->lang->line('msg_user_not_found');
        }
      }
      echo json_encode($resp);exit;
  }
  public function fblogout() 
  {
    // Destroy session
    session_destroy();
    $this->facebook->destroySession();
    $userdata = array('id'=>'',
             'name'=>'',
             'lname'=>'',
             'email'=>''
             );
    $this->session->set_userdata($userdata);
    // Redirect to baseurl
    redirect(base_url());
  }
  
  public function gplogin()
  {
    $email = $this->input->post('email');
    $name = $this->input->post('name');
    //$profile = $this->input->post('picture');
    //$user = $this->input->post('user');
    if($name != '')
    {
      //$user_name = explode(' ',$first_name);
      $name = $user_name[0];
      $lname = $user_name[1]; 
    }
    else 
    {
      $first_name = '';
      $last_name = '';
    }
    $rec_cnt = $this->master_model->getRecordCount('tbl_customer_info',array('email'=>$email,'is_delete'=>'0'));
    if($rec_cnt == 0)
    {
      $insert_array = array('first_name'=>$first_name,'last_name'=>$last_name,'email'=>$email,'register_type'=>'gmail','registered_date'=>date('Y-m-d'),'status'=>'1','is_verified'=>'1','is_delete'=>'0');
      $admin_info = $this->master_model->getRecords('tbl_users',array('id'=>1));
      $email_format = $this->master_model->getRecords('tbl_email_templates',array('format_id'=>4));
      if($last_insert_id = $this->master_model->insertRecord('tbl_customer_info',$insert_array,true))
      {
        $info_arr = array('from'=>$admin_info[0]['email'],'to'=>$email,'view'=>'social_login_emailer','subject'=>$email_format[0]['email_subject']);
        $other_arr = array('first_name'=>$first_name,'last_name'=>$last_name,'email_content'=>$email_format[0]['email_content'],'email_signature'=>$email_format[0]['email_signature']);
        
        if($this->email_sending->sendmail($info_arr,$other_arr))
        {
          $userdata = array('user_id'=>$last_insert_id,
                   'first_name'=>$first_name,
                   'last_name'=>$last_name,
                   'email'=>$email,
                   'register_type'=>'gmail',
                   'logout_url' =>$this->gp_logout_url.base_url().'user/gplogout'
                   );
          $this->session->set_userdata($userdata);
          $resp['result'] = 'registration_success';
          $resp['message'] = $this->lang->line('msg_your_registration_have_been_done_successfully');
          $resp['logout_url'] = $this->gp_logout_url.base_url().'user/gplogout';
        }
        
        else
        {
          $resp['result'] = 'registration_error';
          $resp['message'] = $this->lang->line('msg_failed_to_register_please_try_after_sometime') ;
          $resp['logout_url'] = $this->gp_logout_url.base_url().'user/gplogout';
        }
      }
      else 
      {
        $resp['result'] = 'registration_error';
        $resp['message'] = $this->lang->line('msg_failed_to_register_please_try_after_sometime') ;
        $resp['logout_url'] = $this->gp_logout_url.base_url().'user/gplogout';
      }
    }
    else 
    {
      $user_info = $this->master_model->getRecords('tbl_customer_info',array('email'=>$email,'is_delete'=>'0'));
      if(count($user_info)>0)
      {
        if($user_info[0]['status'] == '0')
        {
          $resp['result'] = 'error';
          $resp['message'] = $this->lang->line('msg_oooppss_unfortunately_you_are_blocked_by_admin') ;
        }
        else if($user_info[0]['is_delete'] == '1')
        {
          $resp['result'] = 'error';
          $resp['message'] = $this->lang->line('msg_oooppss_you_are_no_longer_registered_member_of_this_website');
        }
        else if($user_info[0]['is_verified'] == '0')
        {
          $resp['result'] = 'error';
          $resp['message'] = $this->lang->line('msg_please_verify_your_account_before_login');
        }
        else 
        {
          $userdata = array('user_id'=>$user_info[0]['user_id'],
                   'first_name'=>$user_info[0]['first_name'],
                   'last_name'=>$user_info[0]['last_name'],
                   'email'=>$user_info[0]['email'],
                   'register_type'=>'gmail',
                   'logout_url' =>$this->gp_logout_url.base_url().'user/gplogout'
                   );
          $this->session->set_userdata($userdata);
          $resp['result'] = 'login';
          $resp['response'] = 'login_successful';
          $resp['logout_url'] = $this->gp_logout_url.base_url().'user/gplogout';
        }
        
      }
      else 
      {
        $resp['error'] = 'error';
        $resp['message'] = $this->lang->line('msg_user_not_found');
      }
    }
    echo str_replace('\/','/',json_encode($resp));
  }
  /*Google Plus Login*/

  /*Google Logout*/
  public function gplogout()
  {
    $userdata = array('user_id'=>'',
             'first_name'=>'',
             'last_name'=>'',
             'email'=>$user_info[0]['email'],
             'register_type'=>'',
             'logout_url' =>''
             );
    $this->session->set_userdata($userdata);
    redirect(base_url());
  }
  /*Google Logout*/  

  public function update()
  {
    $user_id = $this->session->userdata('id');
    if($user_id=='')
    {
      redirect(base_url());
    }
    else
    {
      if(isset($_POST['btn_myaccount']))
      {
        $this->form_validation->set_rules('name','first name','required|trim');
        $this->form_validation->set_rules('lname','Last name','required|trim');
        $this->form_validation->set_rules('email','Email Id','required|trim|valid_email');
        $this->form_validation->set_rules('gender','Gender','required|trim');
        $this->form_validation->set_rules('mobile_number','Contact Code','required|trim');
        $this->form_validation->set_rules('country','Country','required|trim');
        $this->form_validation->set_rules('address','Address','required|trim');
        /*$this->form_validation->set_rules('pin_code','Pin Code','required|trim'); */
        
        if($this->form_validation->run()) 
        {
          $name   = $this->input->post('firstname');
          $lname    = $this->input->post('lastname');
          $email   = $this->input->post('email');
          $gender    = $this->input->post('gender');
          $mobile_number = $this->input->post('mobile_number');
          $country   = $this->input->post('country');
          $address   = $this->input->post('address');/*
          $address2    = $this->input->post('address2');
          $address3    = $this->input->post('address3');*/
          /*$pin_code    = $this->input->post('pin_code');*/
          $id        = $this->input->post('id');

          $update_array = array('name'    =>$name,
                    'lname'     =>$lname,
                    'display_name'    =>$displayname,
                    'email'       =>$email,
                    'gender'      =>$gender,
                    'mobile_number' =>$mobile_number,
                    'country'     =>$country,
                    'address'     =>$address,
                    
                    /*'pin_code'      =>$pin_code,*/
                    'updated_date'    =>date('Y-m-d h:i:s'));

          $duplicate_email_check = $this->master_model->getRecordCount('tbl_user_master',array('email'=>$emailid,'id !='=>$id,'is_delete'=>'0'));
          if($duplicate_email_check >0)
          {
            $this->session->set_flashdata('error',$this->lang->line('msg_email_already_exist'));
            redirect(base_url().'signup/update/');
          }
          else
          {
            if($this->master_model->updateRecord('tbl_user_master',$update_array,array('user_id'=>$user_id)))
            {
              $userdata = array(
                   'name'=>$name,
                   'lname'=>$lname
                   );
              $this->session->set_userdata($userdata);
              $this->session->set_flashdata('success',$this->lang->line('msg_user_profile_updated_successfully'));
              redirect(base_url().'signup/update/');
            }
            else
            {
              $this->session->set_flashdata('error',$this->lang->line('msg_error_while_updating_user_profile'));
              redirect(base_url().'signup/update/');
            }
          } 
        } 
        else
        {
          $data['error']=$this->form_validation->error_string();
        }
      
      }
      $userprofile_array = $this->master_model->getRecords('tbl_customer_info',array('user_id'=>$user_id));
      $number_code     = $this->master_model->getRecords('tbl_country_codes',false,'id,phonecode,name');
      $country       = $this->master_model->getRecords('tbl_countries',array('status'=>'1'));
      $data = array('page_title'        => $this->lang->line('cmn_my_profile'),
              'middle_content'    => 'msg_error_while_updating_user_profile',
              'userprofile_array' => $userprofile_array,
              'number_code'       => $number_code,
              'country'       => $country);
      $this->load->view('template', $data);
    }
  }

	public function activate_user($confirm_code = FALSE){

        if($confirm_code != "")
		{
			$arr_chk_confirmation=$this->master_model->getRecords('tbl_user_master',array('confirm_code'=>trim($confirm_code)));
			if(sizeof($arr_chk_confirmation)>0)
			{
				if($this->master_model->updateRecord('tbl_user_master',array("verification_status"=>"Verified",'confirm_code'=>NULL),array('confirm_code'=>"'".$confirm_code."'")))
				{
                    $this->session->set_flashdata('success','Your account is activated successfully.');
					redirect(base_url().'login');
				}
				else
				{
					$this->session->set_flashdata('error','Your account is already activated.');
					redirect(base_url().'login');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Your account is already activated.');
				redirect(base_url().'login');
			}
		}
		else
		{
			$this->session->set_flashdata('error','Error Occured.');
			redirect(base_url().'signup');
		}
	}

} //  end class
