<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Buyer extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('email_sending');
		$this->master_model->IsLogged();
		if($this->session->userdata('user_type')!='Buyer')
        {
           $this->session->set_flashdata('error' , 'Sorry!!, you cant access this page....');
           redirect(base_url().lcfirst($this->session->userdata('user_type'))."/dashboard");
        }
	}
	public function dashboard()
	{
        $data['pageTitle']       = 'Buyer dashboard - '.PROJECT_NAME;
   	    $data['page_title']      = 'Buyer dashboard - '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/dashboard';
	    $this->load->view('template',$data);
	}
	public function edit()
	{
        $data['pageTitle']       = 'Edit Profile - '.PROJECT_NAME;
   	    $data['page_title']      = 'Edit Profile - '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/edit-profile';
	    $this->load->view('template',$data);

	}
	public function get_profile()
	{
        $get_profile_data = $this->master_model->getRecords('tbl_user_master',array('id'=>$this->session->userdata('user_id')));
		$arr_response['status']        = "success";
        $arr_response['profile_data']  = isset($get_profile_data[0])?$get_profile_data[0]:[];
       	echo json_encode($arr_response);
		exit;
	}
	public function update()
	{
        $logo_name = NULL;
        $arr_get_user = $this->master_model->getRecords('tbl_user_master',array('id' => $this->session->userdata('user_id')));
		if(isset($_FILES['user_image']))
		{
			$logo_name = $_FILES['user_image']['name'];
			if($arr_get_user[0]["user_image"] != $logo_name)
			{
				$this->load->library('upload');

				$config['upload_path']   = "images/buyer_image/";
				$config['allowed_types'] = '*'; //'*';//;
				
				$this->upload->initialize($config);

				if ( ! $this->upload->do_upload("user_image"))
				{
					echo $this->upload->display_errors();
					$error = array('error' => $this->upload->display_errors());
					exit;
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());
					$logo_name = $data["upload_data"]["file_name"];
					$uploaddir = 'images/buyer_image/';
					if (file_exists($uploaddir.$arr_get_user[0]["user_image"]) && $arr_get_user[0]["user_image"]!='') {
					    unlink($uploaddir.$arr_get_user[0]["user_image"]);
					}
				}
			}
		}
		else
		{
				$logo_name = $arr_get_user[0]["user_image"];
		}
      
      //echo'hello mas'; die;
        $name                              = $this->input->post('name');
        $lname                             = $this->input->post('lname');
        //$email                             = $this->input->post('email');
        $user_image                        = $logo_name;
       // $optional_email                    = $this->input->post('optional_email');
        $mobile_number                     = $this->input->post('mobile_number');
       // $gender                            = $this->input->post('gender');
        //$day                               = $this->input->post('day');
        //$month                             = $this->input->post('month');
        //$year                              = $this->input->post('year');
        $pincode                           = $this->input->post('pincode');
        //$com_national_registration_number  = $this->input->post('com_national_registration_number');
        $address                           = $this->input->post('address');
        $longitude                         = $this->input->post('longitude');
        $latitude                          = $this->input->post('latitude');	
        $city                              = $this->input->post('city');
        $state                             = $this->input->post('state');
        $country                           = $this->input->post('country');
        //$company_website                   = $this->input->post('company_website');
        //$company_description               = $this->input->post('company_description');

        if(isset($longitude) && !empty($longitude) ) 
        {
          $lng  = $longitude;
        }
        else{
          $lng  = '';   
        }

        if(isset($latitude) && !empty($latitude) ) 
        {
          $lat  = $latitude;
        }
        else{
          $lat  = '';   
        }

        $arr_Data  =array(
                    'name'                              => $name,
                    'lname'                             => $lname,
                   // 'email'                             => $email,
                    'user_image'                        => $user_image,
                    //'optional_email'                    => $optional_email,
                    'mobile_number'                     => $mobile_number,
                    //'day'                               => $day,
                    //'month'                             => $month,
                    //'year'                              => $year,
                    //'gender'                            => $gender,
                    'pincode'                           => $pincode,
                    //'com_national_registration_number'  => $com_national_registration_number,
                    'address'                           => $address,
                    'latitude'                          => $lat,
                    'longitude'                         => $lng,
                    'city'                              => $city,
                    'state'                             => $state,
                    'country'                           => $country,
                    //'company_website'                   => $company_website,
                    'update_date'                       => date('Y-m-d H:m:s'),
                    //'company_description'               => $company_description,
                    
        );
        $this->db->where('id' , $this->session->userdata('user_id'));
        if($this->db->update('tbl_user_master',$arr_Data)){

            $user_data = array(
                   'user_name'      => $name,
                   'user_state'     => $state,
                   'user_city'      => $city,
                   'user_country'   => $country,
                   'user_address'   => $address,
                   'user_mobile'    => $mobile_number,
                   'user_image'     => $logo_name
            );
			$this->session->set_userdata($user_data);

        	
        	if(isset($_FILES['user_image']))
		    {
		    	$arr_response['status'] = "success_with_reload";
		    }
		    else {
		    	$arr_response['status'] = "success";
		    }
	        $arr_response['msg']    = "Profile data updated successfully.";
	       	echo json_encode($arr_response);
			exit;
        } else {
        	$arr_response['status'] = "error";
	        $arr_response['msg']    = "Something was wrong,Please try again.";
	       	echo json_encode($arr_response);
			exit;
        }

	}
	public function change_password()
	{

	    if(isset($_POST['oldpwd'])){

	        	$newpassword      = $this->input->post('newpwd');
	        	$oldpassword      = $this->input->post('oldpwd');


                $check_old_pass_is_correct = $this->master_model->getRecords('tbl_user_master',array('id'=>$this->session->userdata('user_id') ,'password' => sha1($oldpassword)));

                if(sizeof($check_old_pass_is_correct) <= 0){

	        		$arr_response['status'] = "error";
			        $arr_response['msg']    = "Sorry , your old password does not match.";
			       	echo json_encode($arr_response);
					exit;
	        	}

	        	$get_current_pass =     $this->master_model->getRecords('tbl_user_master',array('id'=>$this->session->userdata('user_id') ,'password' => sha1($newpassword)));

	        	if(sizeof($get_current_pass) > 0){

	        		$arr_response['status'] = "error";
			        $arr_response['msg']    = "Sorry , Please do not enter old password as current password.";
			       	echo json_encode($arr_response);
					exit;
	        	}

	            $arr_Data = array(

	                        'password'  => sha1($newpassword)
	        );
        	$this->db->where('id' , $this->session->userdata('user_id'));
            if($this->db->update('tbl_user_master',$arr_Data)){

                $arr_response['status'] = "success";
            	$arr_response['msg']    = "Password updated successfully.";
		       	echo json_encode($arr_response);
				exit;
            } 
            else {
	        	$arr_response['status'] = "error";
		        $arr_response['msg']    = "Something was wrong,Please try again.";
		       	echo json_encode($arr_response);
				exit;
            }
        }

		$data['pageTitle']       = 'Change Password - '.PROJECT_NAME;
   	    $data['page_title']      = 'Change Password - '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/change-password';
	    $this->load->view('template',$data);
	}
    
    public function post_requirment($requirement_id=FALSE)
	{
        if(isset($_POST['title'])) {

            $this->db->where('buyer_id' , $this->session->userdata('user_id'));
            $this->db->where('status'   , 'Unblock');
            $getAvailablePostCount = $this->master_model->getRecords('tbl_buyer_post_requirement_count');

	        if(empty($requirement_id)){   
	            if(sizeof($getAvailablePostCount) > 0){

	            	if($getAvailablePostCount[0]['available'] <= 0){
	                   
	                    $url=base_url().'purchase/post_requirements';
						$arr_response['status'] = "error";
						$arr_response['msg']    = "Sorry!!, you not able to post this requirement. your post requirement limit has expire, if you post this requirement then purchase ".PROJECT_NAME." premium membership. <a href='$url'><button style='height: 30px; max-width: 122px; margin:16px 0 2px 0' class='change-btn-pass'>Purchase</button></a>";
						echo json_encode($arr_response);
						exit;
	            	}
	            }
	            else 
	            {
	            	$arr_response['status'] = "error";
		        	$arr_response['msg']    = "Sorry, you not able to post requirment.";
			       	echo json_encode($arr_response);
					exit;
	            }
            }


            $logo_name ="";

            if(isset($requirement_id) && !empty($requirement_id)){
	        	if(isset($_FILES['req_image']))
				{
					$logo_name = $_FILES['req_image']['name'];
					
					$this->load->library('upload');

					$config['upload_path']   = "images/buyer_post_requirment_image/";
					$config['allowed_types'] = '*'; //'*';//;
					
					$this->upload->initialize($config);

					if ( ! $this->upload->do_upload("req_image"))
					{
						echo $this->upload->display_errors();
						$error = array('error' => $this->upload->display_errors());

						$arr_response['status'] = "error";
			        	$arr_response['msg']    = "Please give permition to upload folder.";
				       	echo json_encode($arr_response);
						exit;
					}
					else
					{
						$data      = array('upload_data' => $this->upload->data());
						$logo_name = $data["upload_data"]["file_name"];
						$uploaddir = 'images/buyer_post_requirment_image/';
						
					}
					
				}
		    }
		    else{
              
                if(isset($_FILES['req_image']))
				{
					$logo_name = $_FILES['req_image']['name'];
					
					$this->load->library('upload');

					$config['upload_path']   = "images/buyer_post_requirment_image/";
					$config['allowed_types'] = '*'; //'*';//;
					
					$this->upload->initialize($config);

					if ( ! $this->upload->do_upload("req_image"))
					{
						echo $this->upload->display_errors();
						$error = array('error' => $this->upload->display_errors());

						$arr_response['status'] = "error";
			        	$arr_response['msg']    = "Please give permition to upload folder.";
				       	echo json_encode($arr_response);
						exit;
					}
					else
					{
						$data      = array('upload_data' => $this->upload->data());
						$logo_name = $data["upload_data"]["file_name"];
						$uploaddir = 'images/buyer_post_requirment_image/';
						
					}
					
				}
				else
				{
					$arr_response['status'] = "error";
		        	$arr_response['msg']    = "Please select requirment image.";
			       	echo json_encode($arr_response);
					exit;
				}

		    }		


			$req_title         = $this->input->post('title');
			$req_type          = $this->input->post('req_type');
			$req_subcat        = $this->input->post('subcategory_id');
			$price             = $this->input->post('price');
			$req_address       = $this->input->post('location');
			$longitude         = $this->input->post('longitude');
            $latitude          = $this->input->post('latitude');
			$req_desc          = $this->input->post('description');
			$image             = $logo_name;

			if(isset($longitude) && !empty($longitude) ) 
	        {
	          $lng  = $longitude;
	        }
	        else{
	          $lng  = '';   
	        }

	        if(isset($latitude) && !empty($latitude) ) 
	        {
	          $lat  = $latitude;
	        }
	        else{
	          $lat  = '';   
	        }

			/*Category*/
			$this->db->where('subcategory_status' , '1');
			$this->db->where('is_delete' , '0');
			$this->db->where('subcategory_id' , $req_subcat);
			$getCategory = $this->master_model->getRecords('tbl_subcategory_master');
	        /**/

			$arr_req  = array(
				'buyer_id'         => $this->session->userdata('user_id'),
				'category_id'      => $getCategory[0]['category_id'],
				'subcategory_id'   => $req_subcat,
				'title'            => $req_title, 
				'req_type'         => $req_type, 
				'price'            => $price, 
				'description'      => $req_desc, 
				'req_image'        => $image,
				'location'         => $req_address,
				'latitude'                          => $lat,
				'longitude'                         => $lng,
				'created_date'     => date('Y-m-d H:m:s'),
				'status'           => 'Unblock'
		    );


		    if(isset($requirement_id) && !empty($requirement_id)){

		    	if($logo_name != ""){
	                $update_req  = array(
						'category_id'      => $getCategory[0]['category_id'],
						'subcategory_id'   => $req_subcat,
						'title'            => $req_title, 
						'req_type'         => $req_type,
						'price'            => $price, 
						'req_image'        => $image,
						'description'      => $req_desc, 
						'location'         => $req_address,
						'latitude'                          => $lat,
                        'longitude'                         => $lng,
						'updated_date'     => date('Y-m-d H:m:s')
			    	);
				}
				else {
					$update_req  = array(
						'category_id'      => $getCategory[0]['category_id'],
						'subcategory_id'   => $req_subcat,
						'title'            => $req_title, 
						'req_type'         => $req_type,
						'price'            => $price, 
						'description'      => $req_desc, 
						'location'         => $req_address,
						'latitude'                          => $lat,
                        'longitude'                         => $lng,
						'updated_date'     => date('Y-m-d H:m:s')
		    	    );
				}
		    	
		    	$this->db->where('tbl_buyer_post_requirement.id' , $requirement_id);
		    	if($this->db->update('tbl_buyer_post_requirement' , $update_req)){
	                $arr_response['status']   = "success";
	                $arr_response['redirect'] = "yes";
		        	$arr_response['msg']      = "Your requirement successfully updated.";
			       	echo json_encode($arr_response);
					exit;

			    }else {
			    	$arr_response['status'] = "error";
			        $arr_response['msg']    = "Something was wrong,Please try again.";
			       	echo json_encode($arr_response);
					exit;
			    }

		    }else{

		    	if($this->db->insert('tbl_buyer_post_requirement' , $arr_req)){
              
	                $minus_availble_post_count = $getAvailablePostCount[0]['available'] - 1;
	                $plus_completed_post_count = $getAvailablePostCount[0]['competed']  + 1;

	                $arr_post_update_data = array(
	                   	 	'available'  => $minus_availble_post_count,
	                   	 	'competed'   => $plus_completed_post_count,
	               	);
	               	$this->db->where('buyer_id' , $this->session->userdata('user_id'));
	               	$this->db->update('tbl_buyer_post_requirement_count',$arr_post_update_data);


	                $arr_response['status'] = "success";
		        	$arr_response['msg']    = "Your requirement successfully posted.";
			       	echo json_encode($arr_response);
					exit;

			    }else {
			    	$arr_response['status'] = "error";
			        $arr_response['msg']    = "Something was wrong,Please try again.";
			       	echo json_encode($arr_response);
					exit;
			    }
		    }
        }

		$data['pageTitle']       = 'Post requirements - '.PROJECT_NAME;
   	    $data['page_title']      = 'Post requirements - '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/buyer-post-requirement';

        
        $this->db->where('subcategory_status' , '1');
        $this->db->where('is_delete' , '0');
        $data['getSubcat'] = $this->master_model->getRecords('tbl_subcategory_master');

        $this->db->where('tbl_category_master.category_status' , '1');
        $this->db->where('tbl_category_master.is_delete' , '0');
        $this->db->where('subcategory_status' , '1');
        $this->db->where('tbl_subcategory_master.is_delete' , '0');
        $this->db->where('tbl_subcategory_master.is_delete' , '0');
        $this->db->group_by('tbl_category_master.category_id');
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.category_id=tbl_category_master.category_id');
        $data['getCat'] = $this->master_model->getRecords('tbl_category_master');

	    $this->load->view('template',$data);
	}



	public function edit_requirement($requirement_id=FALSE)
	{
		$data['pageTitle']       = 'Edit requirements - '.PROJECT_NAME;
   	    $data['page_title']      = 'Edit requirements - '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/buyer-post-requirement-edit';
   	    $data['requirement_id']  = $requirement_id;

        $this->db->where('subcategory_status' , '1');
        $this->db->where('is_delete' , '0');
        $data['getSubcat'] = $this->master_model->getRecords('tbl_subcategory_master');

        $this->db->where('tbl_category_master.category_status' , '1');
        $this->db->where('tbl_category_master.is_delete' , '0');
        $this->db->where('subcategory_status' , '1');
        $this->db->where('tbl_subcategory_master.is_delete' , '0');
        $this->db->where('tbl_subcategory_master.is_delete' , '0');
        $this->db->group_by('tbl_category_master.category_id');
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.category_id=tbl_category_master.category_id');
        $data['getCat'] = $this->master_model->getRecords('tbl_category_master');
	    $this->load->view('template',$data);
	}

    public function delete_requirement($requirement_id=FALSE){

    	if($this->master_model->updateRecord('tbl_buyer_post_requirement',array('status'=>'Delete'),array('id'=>$requirement_id)))
	  	{
 
            $this->db->where('buyer_id' , $this->session->userdata('user_id'));
            $this->db->where('status'   , 'Unblock');
            $getAvailablePostCount = $this->master_model->getRecords('tbl_buyer_post_requirement_count');

            $minus_availble_post_count = $getAvailablePostCount[0]['available'] + 1;
            $plus_completed_post_count = $getAvailablePostCount[0]['competed']  - 1;

            $arr_post_update_data = array(
               	 	'available'   => $minus_availble_post_count,
               	 	'competed'    => $plus_completed_post_count,
           	);
           	$this->db->where('buyer_id' , $this->session->userdata('user_id'));
           	$this->db->update('tbl_buyer_post_requirement_count',$arr_post_update_data);

	  		$this->session->set_flashdata('success','Requirement deleted successfully.');
	  		redirect(base_url().'buyer/requirments');
	  	}
	  	else
	  	{
	  		$this->session->set_flashdata('error','Error while deleting requirement.');
	  		redirect(base_url().'buyer/requirments');
	  	}

    }

	public function get_requirement($requirement_id=FALSE)
	{
        $this->db->where('tbl_buyer_post_requirement.buyer_id' , $this->session->userdata('user_id'));
        $this->db->where('tbl_buyer_post_requirement.id' , $requirement_id);
        $this->db->where('tbl_buyer_post_requirement.requirment_status' , 'open');
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_buyer_post_requirement.subcategory_id');
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
        $data['getRequirmentsDetail']  = $this->master_model->getRecords('tbl_buyer_post_requirement');

		$arr_response['status']        = "success";
        $arr_response['requirement']   = $data['getRequirmentsDetail'][0];
       	echo json_encode($arr_response);
		exit;
	}
	

	public function requirments()
	{
    	$data['pageTitle']       = 'Posted requirements - '.PROJECT_NAME;
   	    $data['page_title']      = 'Posted requirements - '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/buyer-posted-requirement';


        $this->db->where('tbl_buyer_post_requirement.buyer_id' , $this->session->userdata('user_id'));
        $this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
        $this->db->where('tbl_buyer_post_requirement.status <>' , 'Delete');
        $this->db->where('tbl_buyer_post_requirement.requirment_status' , 'open');
        //$this->db->where('tbl_subcategory_master.is_delete <>','1');
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_buyer_post_requirement.subcategory_id');
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
        $data['getRequirments']  = $this->master_model->getRecords('tbl_buyer_post_requirement');
        $Count = count($data['getRequirments']);


        /* create pagination */
	    $this->load->library('pagination');
	    $config1['total_rows']           = $Count;
	    $config1['base_url']             = base_url().'buyer/requirments';
	    $config1['per_page']             = 8;
	    $config1['uri_segment']          = '3';
	    $config1['full_tag_open']        = '<div class="pagination pull-right"><ul class="pagination pagination-blog">';
	    $config1['full_tag_close']       = '</ul></div>';

	    $config1['first_link']           = '<i class="fa fa-angle-double-left" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['first_tag_open']       = '<li class="prev page">';
	    $config1['first_tag_close']      = '</li>';

	    $config1['last_link']            = '<i class="fa fa-angle-double-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['last_tag_open']        = '<li class="next page">';
	    $config1['last_tag_close']       = '</li>';

	    $config1['next_link']            = '<i class="fa fa-angle-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['next_tag_open']        = '<li class="next page">';
	    $config1['next_tag_close']       = '</li>';

	    $config1['prev_link']            = '<i class="fa fa-angle-left" style="font-size: 1.4em;"></i>';
	    $config1['prev_tag_open']        = '<li class="prev page">';
	    $config1['prev_tag_close']       = '</li>';

	    $config1['cur_tag_open']         = '<li ><a href="" class="act" style="color: rgb(242, 246, 249);background-color: #034A7B;" >';
	    $config1['cur_tag_close']        = '</a></li>';

	    $config1['num_tag_open']         = '<li class="page">';
	    $config1['num_tag_close']        = '</li>'; 
	    

	    $this->pagination->initialize($config1);
	    $page        = ($this->uri->segment(3));
	    /* end create pagination */

        $this->db->where('tbl_buyer_post_requirement.buyer_id' , $this->session->userdata('user_id'));
        $this->db->order_by('tbl_buyer_post_requirement.id' , 'Desc');
        $this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
        $this->db->where('tbl_buyer_post_requirement.status <>' , 'Delete');
        $this->db->where('tbl_buyer_post_requirement.requirment_status' , 'open');
        //$this->db->where('tbl_subcategory_master.is_delete <>','1');
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_buyer_post_requirement.subcategory_id');
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
        $data['getRequirments'] = $this->master_model->getRecords('tbl_buyer_post_requirement',FALSE,FALSE,FALSE,$page,$config1["per_page"]);


	    $this->load->view('template',$data);
	}

	public function requirment_detail($req_id=FALSE)
	{ 
    	$data['pageTitle']       = 'Requirements detail- '.PROJECT_NAME;
   	    $data['page_title']      = 'Requirements detail - '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/buyer-post-requirement-details';

        $this->db->where('tbl_buyer_post_requirement.buyer_id' , $this->session->userdata('user_id'));
        $this->db->where('tbl_buyer_post_requirement.id' , $req_id);
        //$this->db->where('tbl_subcategory_master.is_delete <>','1');
        $this->db->where('tbl_buyer_post_requirement.requirment_status' , 'open');
        $this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_buyer_post_requirement.subcategory_id');
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
        $data['getRequirmentsDetail']  = $this->master_model->getRecords('tbl_buyer_post_requirement');
        $Count = count($data['getRequirmentsDetail']);


        /* offered sellers */ 
        $this->db->where('tbl_apply_for_requirment.requirment_id' , $req_id);
        $this->db->where('tbl_apply_for_requirment.status'   , 'Unblock');
        $this->db->where('tbl_apply_for_requirment.status !='   , 'Accepted');
        $this->db->order_by('tbl_apply_for_requirment.id' ,  'desc');
        $this->db->where('tbl_user_master.status'   , 'Unblock');
        $select = '
                   tbl_apply_for_requirment.*,
                   tbl_user_master.user_image,
                   tbl_user_master.name,
                   tbl_user_master.email,
                   tbl_user_master.city,
                   tbl_user_master.country,
                   tbl_user_master.state
        ';
        $this->db->join('tbl_user_master' , 'tbl_user_master.id=tbl_apply_for_requirment.offered_seller_id');
        $data['offered_sellers'] = $this->master_model->getRecords('tbl_apply_for_requirment',FALSE,$select);

        $Count = count($data['offered_sellers']);


        /* create pagination */
	    $this->load->library('pagination');
	    $config1['total_rows']           = $Count;
	    $config1['base_url']             = base_url().'buyer/requirment_detail/'.$req_id;
	    $config1['per_page']             = 8;
	    $config1['uri_segment']          = '4';
	    $config1['full_tag_open']        = '<div class="pagination pull-right"><ul class="pagination pagination-blog">';
	    $config1['full_tag_close']       = '</ul></div>';

	    $config1['first_link']           = '<i class="fa fa-angle-double-left" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['first_tag_open']       = '<li class="prev page">';
	    $config1['first_tag_close']      = '</li>';

	    $config1['last_link']            = '<i class="fa fa-angle-double-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['last_tag_open']        = '<li class="next page">';
	    $config1['last_tag_close']       = '</li>';

	    $config1['next_link']            = '<i class="fa fa-angle-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['next_tag_open']        = '<li class="next page">';
	    $config1['next_tag_close']       = '</li>';

	    $config1['prev_link']            = '<i class="fa fa-angle-left" style="font-size: 1.4em;"></i>';
	    $config1['prev_tag_open']        = '<li class="prev page">';
	    $config1['prev_tag_close']       = '</li>';

	    $config1['cur_tag_open']         = '<li ><a href="" class="act" style="color: rgb(242, 246, 249);background-color: #034A7B;" >';
	    $config1['cur_tag_close']        = '</a></li>';

	    $config1['num_tag_open']         = '<li class="page">';
	    $config1['num_tag_close']        = '</li>'; 
	    

	    $this->pagination->initialize($config1);
	    $page        = ($this->uri->segment(4));
	    /* end create pagination */

	    $this->db->where('tbl_apply_for_requirment.requirment_id' , $req_id);
        $this->db->where('tbl_apply_for_requirment.status'   , 'Unblock');
        $this->db->where('tbl_apply_for_requirment.status !='   , 'Accepted');
        $this->db->order_by('tbl_apply_for_requirment.id' ,  'desc');
        $this->db->where('tbl_user_master.status'   , 'Unblock');
        $select = '
                   tbl_apply_for_requirment.*,
                   tbl_user_master.user_image,
                   tbl_user_master.name,
                   tbl_user_master.email,
                   tbl_user_master.city,
                   tbl_user_master.country,
                   tbl_user_master.state
        ';
        $this->db->join('tbl_user_master' , 'tbl_user_master.id=tbl_apply_for_requirment.offered_seller_id');
        $data['offered_sellers'] = $this->master_model->getRecords('tbl_apply_for_requirment',FALSE,$select,FALSE,$page,$config1["per_page"]);
        /* end offered sellers */ 


	    $this->load->view('template',$data);
	}

	public function requirement_payment_history()/*----------T.A.. */
	{
		if($this->session->userdata('user_id')=="")
		{
			redirect(base_url().'login');
		}
        $day="";
        if(!empty($_REQUEST["sort_by"]))
        {
       	  if($_REQUEST["sort_by"]      =="Daily")
       	   {
       	   	 $day=0;
       	   }
       	  else if($_REQUEST["sort_by"] =="Weekly")
       	   {
       	   	 $day=7;
       	   }
       	  else if($_REQUEST["sort_by"] =="Monthly")
       	   {
       	   	 $day=30;
       	   }
       	  else if($_REQUEST["sort_by"] =="Yearly")
       	   {
       	   	 $day=365;
       	   }
       	  	
       	$this->db->where('LEFT(payment_date , 10) <=', date('Y-m-d')); 
       	$this->db->where('LEFT(payment_date , 10) >=',date('Y-m-d', strtotime('-'.$day.' days')));	
        }

	    $this->db->where('tbl_buyer_primum_membership_for_requirements_purchase_history.buyer_id' , $this->session->userdata('user_id'));
	    $this->db->order_by('tbl_buyer_primum_membership_for_requirements_purchase_history.id', 'desc');
		$getPaymentHistory['getPaymentHistory']  = $this->master_model->getRecords('tbl_buyer_primum_membership_for_requirements_purchase_history');
        $Count=count($getPaymentHistory['getPaymentHistory']);

        /* create pagination */
	    $this->load->library('pagination');
	    $config1['total_rows']           = $Count;
	    $config['first_url'] = $_SERVER['QUERY_STRING']!="" ? base_url('buyer/requirement_payment_history/').'/?'.$_SERVER['QUERY_STRING'] : base_url('buyer/requirement_payment_history/');
	    $config['suffix'] = "/?".$_SERVER['QUERY_STRING'];
	    $config1['base_url']             = base_url().'buyer/requirement_payment_history';
	    $config1['per_page']             = 10;
	    $config1['uri_segment']          = '3';
	    $config1['full_tag_open']        = '<div class="pagination pull-right"><ul class="pagination pagination-blog">';
	    $config1['full_tag_close']       = '</ul></div>';

	    $config1['first_link']           = '<i class="fa fa-angle-double-left" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['first_tag_open']       = '<li class="prev page">';
	    $config1['first_tag_close']      = '</li>';

	    $config1['last_link']            = '<i class="fa fa-angle-double-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['last_tag_open']        = '<li class="next page">';
	    $config1['last_tag_close']       = '</li>';

	    $config1['next_link']            = '<i class="fa fa-angle-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['next_tag_open']        = '<li class="next page">';
	    $config1['next_tag_close']       = '</li>';

	    $config1['prev_link']            = '<i class="fa fa-angle-left" style="font-size: 1.4em;"></i>';
	    $config1['prev_tag_open']        = '<li class="prev page">';
	    $config1['prev_tag_close']       = '</li>';

	    $config1['cur_tag_open']         = '<li ><a href="" class="act" style="color: rgb(242, 246, 249);background-color: #034A7B;" >';
	    $config1['cur_tag_close']        = '</a></li>';

	    $config1['num_tag_open']         = '<li class="page">';
	    $config1['num_tag_close']        = '</li>'; 
	    

	    $this->pagination->initialize($config1);
	    $page        = ($this->uri->segment(3));
	    /* end create pagination */
   
        if(!empty($_REQUEST["sort_by"]))
        {
       	  if($_REQUEST["sort_by"]      =="Daily")
       	   {
       	   	 $day=0;
       	   }
       	  else if($_REQUEST["sort_by"] =="Weekly")
       	   {
       	   	 $day=7;
       	   }
       	  else if($_REQUEST["sort_by"] =="Monthly")
       	   {
       	   	 $day=30;
       	   }
       	  else if($_REQUEST["sort_by"] =="Yearly")
       	   {
       	   	 $day=365;
       	   }
       	  	
        $this->db->where('LEFT(payment_date , 10) <=', date('Y-m-d')); 
       	$this->db->where('LEFT(payment_date , 10) >=', date('Y-m-d', strtotime('-'.$day.' days')));		
        }

        $this->db->where('tbl_buyer_primum_membership_for_requirements_purchase_history.buyer_id' , $this->session->userdata('user_id'));
	    $this->db->order_by('tbl_buyer_primum_membership_for_requirements_purchase_history.id', 'desc');
		$getPaymentHistory['getPaymentHistory']  = $this->master_model->getRecords('tbl_buyer_primum_membership_for_requirements_purchase_history',FALSE,FALSE,FALSE,$page,$config1["per_page"]);
        //echo $this->db->last_query();
         
		$data   =  array('middle_content' => 'buyer/buyer-requirement-transactions' , 'page_title' => 'Requirement Payment History' , 'pageTitle' => 'Product Payment History' , 'HistoryData'=> $getPaymentHistory['getPaymentHistory']);
		$this->load->view('template',$data);
	}
    public function make_offer_payment_history()/*----------T.A.. */
	{
		if($this->session->userdata('user_id')=="")
		{
			redirect(base_url().'login');
		}
        $day="";
        if(!empty($_REQUEST["sort_by"]))
        {
       	  if($_REQUEST["sort_by"]      =="Daily")
       	   {
       	   	 $day=0;
       	   }
       	  else if($_REQUEST["sort_by"] =="Weekly")
       	   {
       	   	 $day=7;
       	   }
       	  else if($_REQUEST["sort_by"] =="Monthly")
       	   {
       	   	 $day=30;
       	   }
       	  else if($_REQUEST["sort_by"] =="Yearly")
       	   {
       	   	 $day=365;
       	   }
       	  	
       	$this->db->where('LEFT(payment_date , 10) <=', date('Y-m-d')); 
       	$this->db->where('LEFT(payment_date , 10) >=',date('Y-m-d', strtotime('-'.$day.' days')));	
        }

	    $this->db->where('tbl_buyer_primum_membership_for_make_offer_purchase_history.buyer_id' , $this->session->userdata('user_id'));
	    $this->db->order_by('tbl_buyer_primum_membership_for_make_offer_purchase_history.id', 'desc');
		$getPaymentHistory['getPaymentHistory']  = $this->master_model->getRecords('tbl_buyer_primum_membership_for_make_offer_purchase_history');
        $Count=count($getPaymentHistory['getPaymentHistory']);

        /* create pagination */
	    $this->load->library('pagination');
	    $config1['total_rows']           = $Count;
	    $config['first_url'] = $_SERVER['QUERY_STRING']!="" ? base_url('buyer/make_offer_payment_history/').'/?'.$_SERVER['QUERY_STRING'] : base_url('buyer/make_offer_payment_history/');
	    $config['suffix'] = "/?".$_SERVER['QUERY_STRING'];
	    $config1['base_url']             = base_url().'buyer/make_offer_payment_history';
	    $config1['per_page']             = 10;
	    $config1['uri_segment']          = '3';
	    $config1['full_tag_open']        = '<div class="pagination pull-right"><ul class="pagination pagination-blog">';
	    $config1['full_tag_close']       = '</ul></div>';

	    $config1['first_link']           = '<i class="fa fa-angle-double-left" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['first_tag_open']       = '<li class="prev page">';
	    $config1['first_tag_close']      = '</li>';

	    $config1['last_link']            = '<i class="fa fa-angle-double-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['last_tag_open']        = '<li class="next page">';
	    $config1['last_tag_close']       = '</li>';

	    $config1['next_link']            = '<i class="fa fa-angle-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['next_tag_open']        = '<li class="next page">';
	    $config1['next_tag_close']       = '</li>';

	    $config1['prev_link']            = '<i class="fa fa-angle-left" style="font-size: 1.4em;"></i>';
	    $config1['prev_tag_open']        = '<li class="prev page">';
	    $config1['prev_tag_close']       = '</li>';

	    $config1['cur_tag_open']         = '<li ><a href="" class="act" style="color: rgb(242, 246, 249);background-color: #034A7B;" >';
	    $config1['cur_tag_close']        = '</a></li>';

	    $config1['num_tag_open']         = '<li class="page">';
	    $config1['num_tag_close']        = '</li>'; 
	    

	    $this->pagination->initialize($config1);
	    $page        = ($this->uri->segment(3));
	    /* end create pagination */
   
        if(!empty($_REQUEST["sort_by"]))
        {
       	  if($_REQUEST["sort_by"]      =="Daily")
       	   {
       	   	 $day=0;
       	   }
       	  else if($_REQUEST["sort_by"] =="Weekly")
       	   {
       	   	 $day=7;
       	   }
       	  else if($_REQUEST["sort_by"] =="Monthly")
       	   {
       	   	 $day=30;
       	   }
       	  else if($_REQUEST["sort_by"] =="Yearly")
       	   {
       	   	 $day=365;
       	   }
       	  	
        $this->db->where('LEFT(payment_date , 10) <=', date('Y-m-d')); 
       	$this->db->where('LEFT(payment_date , 10) >=', date('Y-m-d', strtotime('-'.$day.' days')));		
        }

        $this->db->where('tbl_buyer_primum_membership_for_make_offer_purchase_history.buyer_id' , $this->session->userdata('user_id'));
	    $this->db->order_by('tbl_buyer_primum_membership_for_make_offer_purchase_history.id', 'desc');
		$getPaymentHistory['getPaymentHistory']  = $this->master_model->getRecords('tbl_buyer_primum_membership_for_make_offer_purchase_history',FALSE,FALSE,FALSE,$page,$config1["per_page"]);
        //echo $this->db->last_query();
         
		$data   =  array('middle_content' => 'buyer/make-offer-transactions' , 'page_title' => 'Make offer transactions' , 'pageTitle' => 'Make offer transactions' , 'HistoryData'=> $getPaymentHistory['getPaymentHistory']);
		$this->load->view('template',$data);
	}
	public function accept_offer($req_id=FALSE,$offer_id=FALSE)
	{ 
		$req_update_array = array(
          'requirment_status' => 'closed'
		);
		$this->db->where('tbl_buyer_post_requirement.id' , $req_id);
		$req_update = $this->db->update('tbl_buyer_post_requirement' , $req_update_array);



		$offer_update_array = array(
          'status' => 'Accepted',
          'Merchandise_Description' => $this->input->post('Merchandise_Description'),
          'Transport'               => $this->input->post('Transport'),
          'Delivery_Date'           => $this->input->post('Delivery_Date'),
          'Order_type'              => $this->input->post('Order_type'),
          'Final_price'             => $this->input->post('Final_price'),
          'Quantity'              	=> $this->input->post('Quantity'),
          'Payment_Currency'        => $this->input->post('Payment_Currency'),
          'Payment_Conditions'      => $this->input->post('Payment_Conditions'),
          'Packing_Weight'          => $this->input->post('Packing_Weight'),
          'Trade_confirmed_by'      => $this->input->post('Trade_confirmed_by'),
		);
		//print_r($offer_update_array);exit;
		$this->db->where('tbl_apply_for_requirment.id' , $offer_id);
		$this->db->where('tbl_apply_for_requirment.requirment_id' , $req_id);
		$offer_update = $this->db->update('tbl_apply_for_requirment' , $offer_update_array);



        if($offer_update){

        	/* insert notification */
            $this->db->where('tbl_buyer_post_requirement.id' , $req_id);
            $getreqdetails = $this->master_model->getRecords('tbl_buyer_post_requirement');
            if(isset($getreqdetails[0]['title'])) { $requirement = $getreqdetails[0]['title']; } else { $offer ="Not Available"; }


            $this->db->where('tbl_apply_for_requirment.id' , $offer_id);
            $getofferdetails = $this->master_model->getRecords('tbl_apply_for_requirment');

            $arr_noti  = array(
            'seller_id'          => $getofferdetails[0]['offered_seller_id'],
            'buyer_id'           => $this->session->userdata('user_id'),
            'notification'       => 'Your requirement request of  - <b>'.$requirement.'</b> is accepted.',
            'url'                => 'seller/sent_offer_detail/'.$req_id,
            'details'            => '',
            'created_date'       => date('Y-m-d H:m:s'),
            'status'             =>'Unblock',
            'is_read'            =>'no'
            );
            $this->db->insert('tbl_seller_notifications' , $arr_noti);



            $this->db->where('id',1);
            $email_info=$this->master_model->getRecords('admin_login');
            $admin_contact_email = $email_info[0]['admin_email'];

            $get_user_data                       = $this->master_model->getRecords('tbl_user_master',array('id'=>$getofferdetails[0]['offered_seller_id']));
            $user_info = array(
                            'url'                => 'seller/notifications',
            );
            $info_arr   =array(
                            'from'               => $admin_contact_email,
                            'to'                 => $get_user_data[0]['email'],
                            'subject'            => 'notification received -'.PROJECT_NAME,
                            'view'               => 'notification-receive'
            );
            $this->email_sending->sendmail($info_arr,$user_info);

            $this->db->where('tbl_buyer_post_requirement.id' , $req_id);
            $getreqdetails = $this->master_model->getRecords('tbl_buyer_post_requirement');

            $data['get_user_data']   = $get_user_data;
		    $data['getofferdetails'] = $getofferdetails;
		    $data['getreqdetails']   = $getreqdetails;
		    $data['Final_price']     = $this->input->post('Final_price');
            ob_start();
            /* Generate */
		            $htmlview= $this->load->view('invoice/buyer-to-seller-invoice-pdf' , $data,true);
			        $this->load->library('pdf');
			        $test = new TCPDF();
					$this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 12));
					$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
					$this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
					$this->pdf->SetPrintHeader(false);
					$this->pdf->SetPrintFooter(false);
					$this->pdf->SetMargins(PDF_MARGIN_LEFT, 0, PDF_MARGIN_RIGHT);
					$this->pdf->SetHeaderMargin(-11);
					$this->pdf->SetFooterMargin(20);
					$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
					//$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
					$this->pdf->AddPage();
					$invoice_file='OrderfromPDF-'.$this->session->userdata('user_id').$this->session->userdata('user_email').date('F j, Y', strtotime(date('Y-m-d H:m:s'))).$requirement.'.pdf';
					$html =$htmlview;
					//$this->pdf->SetFont('helvetica', 'B', 20);
					$html_write=$this->pdf->writeHTML($html, true, true, true, true);
					ob_end_clean();
					//$getcontent=$this->pdf->Output($invoice_file,'D');/*Download*/
					//$getcontent=$this->pdf->Output("uploads/".$invoice_file,'I');/*OPEN IN BROWSER*/
					$getcontent=$this->pdf->Output("uploads/invoice/".$invoice_file,'F');/*UPLOAD*/
					/*return $invoice_file;*/
            /* Generate */

			$req_update_array = array(
	          'order_from'    => $invoice_file
			);
			$this->db->where('tbl_buyer_post_requirement.id' , $req_id);
			$req_update = $this->db->update('tbl_buyer_post_requirement' , $req_update_array);


            /* send invoice */

            $user_info = array(
                            'sender_name'        => $this->session->userdata('user_name'),
                            'reciever_name'      => $get_user_data[0]['name'],
                            'sender_email'       => $this->session->userdata('user_email'),
                            'order_name'         => $requirement,
                            'subject'            => 'Accept offer order form -'.PROJECT_NAME,
                            'order_from'         => $invoice_file
            );
            $info_arr   =array(
                            'from'               => $this->session->userdata('user_email'),
                            'to'                 => $get_user_data[0]['email'],
                            'subject'            => 'Accept offer order form -'.PROJECT_NAME,
                            'view'               => 'send_order_form'
            );
            $this->email_sending->sendmail($info_arr,$user_info);

            /* end send invoice */



            /* entry in order table */

            $order_array = array(
	          'from_id'                 => $this->session->userdata('user_id'),
	          'to_id'                   => $getofferdetails[0]['offered_seller_id'],
	          'order_id'                => $req_id,
	          'type'                    => 'requirement',
	          'apply_offer_id'          => $offer_id,
	          'merchadise_description'  => $this->input->post('Merchandise_Description'),
	          'transport'               => $this->input->post('Transport'),
	          'delivery_date'           => $this->input->post('Delivery_Date'),
	          'order_type'              => $this->input->post('Order_type'),
	          'order_name'              => $requirement,
	          'final_price'             => $this->input->post('Final_price'),
	          'quantity'              	=> $this->input->post('Quantity'),
	          'payment_currency'        => $this->input->post('Payment_Currency'),
	          'payment_conditions'      => $this->input->post('Payment_Conditions'),
	          'package_weight'          => $this->input->post('Packing_Weight'),
	          'trade_confirmed_by'      => $this->input->post('Trade_confirmed_by'),
	          'order_form'              => $invoice_file,
	          'date'                    => date('Y-m-d H:m:s')
			);
            $this->db->insert('tbl_order_forms', $order_array);




            /* end entry in order table */
            

        	$arr_response['status'] = "success";
	    	$arr_response['msg']    = "You successfully accepted  offer.";
	       	echo json_encode($arr_response);
			exit;
        }
        else {
        	$arr_response['status'] = "error";
	        $arr_response['msg']    = "Something was wrong,Please try again.";
	       	echo json_encode($arr_response);
			exit;
        }
		
	}

	public function buyer_order($req_id=FALSE,$offer_id=FALSE)
	{ 
	
		$order_add_array = array(
		  'from_id'                 =>  $this->session->userdata('user_id'),
		  'to_id'                   =>  $this->input->post('to_id'),
		  'order_id'                =>  $this->input->post('offer_id'),
		  'apply_offer_id'          =>  $this->input->post('apply_offer_id'),
          'merchadise_description'  =>  $this->input->post('Merchandise_Description'),
          'transport'               =>  $this->input->post('Transport'),
          'delivery_date'           =>  $this->input->post('Delivery_Date'),
          'order_type'              =>  $this->input->post('Order_type'),
          'type'                    =>  'offer',
          'order_name'                  => $this->input->post('offer_title'),
          'final_price'             =>  $this->input->post('Final_price'),
          'quantity'              	=>  $this->input->post('Quantity'),
          'payment_currency'        =>  $this->input->post('Payment_Currency'),
          'payment_conditions'      =>  $this->input->post('Payment_Conditions'),
          'package_weight'          =>  $this->input->post('Packing_Weight'),
          'trade_confirmed_by'      =>  $this->input->post('Trade_confirmed_by'),
		);

		$this->db->insert('tbl_order_forms' , $order_add_array);
		$insert_id = $this->db->insert_id();

		$arr_noti  = array(
		'seller_id'          => $this->input->post('to_id'),
		'order_form_id'      => $insert_id,
		'buyer_id'           => $this->session->userdata('user_id'),
		'apply_offer_id'     =>$this->input->post('apply_offer_id'),
		'notification'       => 'Order request by Buyer ',
		'url'                => 'seller/offer_detail/'.$this->input->post('offer_id'),
		'details'            => '',
		'created_date'       => date('Y-m-d H:m:s'),
		'status'             =>'Unblock',
		'is_read'            =>'no'
		);
		$this->db->insert('tbl_seller_notifications' , $arr_noti);

		$arr_response['status'] = "success";
    	$arr_response['msg']    = "Order request sent successfully.";

       	$user_info = array(
                        'url'                => 'seller/notifications',
        );

       	$this->db->select("email");
        $this->db->where('id',$this->input->post('to_id'));
        $email=$this->master_model->getRecords("tbl_user_master");
        $to_email=$email[0]['email'];

        $info_arr   =array(
                        'from'               => $this->session->userdata('user_email'),
                        'to'                 => $to_email,
                        'subject'            => 'notification received -'.PROJECT_NAME,
                        'view'               => 'notification-receive'
        );
           
        $this->email_sending->sendmail($info_arr,$user_info);

		// Create and upload Pdf
        $arr_response['offer_id']=$this->input->post('offer_id');
        $this->db->where('tbl_seller_products_offers.id' , $this->input->post('offer_id'));
        $getofferdetails = $this->master_model->getRecords('tbl_seller_products_offers');

        $get_user_data= $this->master_model->getRecords('tbl_user_master',array('id'=>$this->input->post('to_id')));

        $this->db->where('tbl_buyer_post_requirement.id' , $this->input->post('offer_id'));
        $getreqdetails = $this->master_model->getRecords('tbl_buyer_post_requirement');
	
		$requirement="abc";

		$order_add_array['type_order']=$this->input->post('offer_type');

       $data['get_user_data']   = $get_user_data;
	   $data['getofferdetails'] = $getofferdetails;
	     $data['getreqdetails']   = $order_add_array;
	    $data['Final_price']     = $this->input->post('Final_price');
        ob_start();
        /* Generate */
	            $htmlview= $this->load->view('invoice/buyer-to-seller-order-invoice-pdf' , $data,true);
		        $this->load->library('pdf');
		        $test = new TCPDF();
				$this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 12));
				$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
				$this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
				$this->pdf->SetPrintHeader(false);
				$this->pdf->SetPrintFooter(false);
				$this->pdf->SetMargins(PDF_MARGIN_LEFT, 0, PDF_MARGIN_RIGHT);
				$this->pdf->SetHeaderMargin(-11);
				$this->pdf->SetFooterMargin(20);
				$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
				//$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
				$this->pdf->AddPage();
				//$invoice_file="deepak.pdf";
				$strtotime = strtotime("now");
				$invoice_file='OrderfromPDF-'.$this->session->userdata('user_id').$this->session->userdata('user_email').$strtotime.'.pdf';
				$html =$htmlview;
				//$this->pdf->SetFont('helvetica', 'B', 20);
				$html_write=$this->pdf->writeHTML($html, true, true, true, true);
				ob_end_clean();
				//$getcontent=$this->pdf->Output($invoice_file,'D');/*Download*/
				//$getcontent=$this->pdf->Output("uploads/".$invoice_file,'I');/*OPEN IN BROWSER*/
				$getcontent=$this->pdf->Output("uploads/invoice/".$invoice_file,'F');/*UPLOAD*/
				/*return $invoice_file;*/
        /* Generate */

        	$upd_arr=array('order_form'=>$invoice_file);
			$this->db->where('order_id',$this->input->post('offer_id'));
			$this->db->update('tbl_order_forms',$upd_arr);


			$arr=array('status'=>'Unblock');
			$this->db->where('id',$this->input->post('apply_offer_id'));
			$this->db->update('tbl_apply_for_offers',$arr);
      //  $arr_response['offer']=$getreqdetails[0]['title'];

		//////////////////////// 		

 	echo json_encode($arr_response);
		
		
	}
	
    public function closed_requirments()
	{
    	$data['pageTitle']       = 'Closed requirements - '.PROJECT_NAME;
   	    $data['page_title']      = 'Closed requirements - '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/buyer-closed-requirement';


        $this->db->where('tbl_buyer_post_requirement.buyer_id' , $this->session->userdata('user_id'));
        $this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
        $this->db->where('tbl_buyer_post_requirement.status <>' , 'Delete');
        $this->db->where('tbl_buyer_post_requirement.requirment_status' , 'closed');
        //$this->db->where('tbl_subcategory_master.is_delete <>','1');
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_buyer_post_requirement.subcategory_id');
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
        $data['getRequirments']  = $this->master_model->getRecords('tbl_buyer_post_requirement');
        $Count = count($data['getRequirments']);


        /* create pagination */
	    $this->load->library('pagination');
	    $config1['total_rows']          = $Count;
	    $config1['base_url']            = base_url().'buyer/closed_requirments';
	    $config1['per_page']            = 8;
	    $config1['uri_segment']         = '3';
	    $config1['full_tag_open']        = '<div class="pagination pull-right"><ul class="pagination pagination-blog">';
	    $config1['full_tag_close']       = '</ul></div>';

	    $config1['first_link']           = '<i class="fa fa-angle-double-left" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['first_tag_open']       = '<li class="prev page">';
	    $config1['first_tag_close']      = '</li>';

	    $config1['last_link']            = '<i class="fa fa-angle-double-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['last_tag_open']        = '<li class="next page">';
	    $config1['last_tag_close']       = '</li>';

	    $config1['next_link']            = '<i class="fa fa-angle-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['next_tag_open']        = '<li class="next page">';
	    $config1['next_tag_close']       = '</li>';

	    $config1['prev_link']            = '<i class="fa fa-angle-left" style="font-size: 1.4em;"></i>';
	    $config1['prev_tag_open']        = '<li class="prev page">';
	    $config1['prev_tag_close']       = '</li>';

	    $config1['cur_tag_open']         = '<li ><a href="" class="act" style="color: rgb(242, 246, 249);background-color: #034A7B;" >';
	    $config1['cur_tag_close']        = '</a></li>';

	    $config1['num_tag_open']         = '<li class="page">';
	    $config1['num_tag_close']        = '</li>'; 
	    

	    $this->pagination->initialize($config1);
	    $page        = ($this->uri->segment(3));
	    /* end create pagination */

        $this->db->where('tbl_buyer_post_requirement.buyer_id' , $this->session->userdata('user_id'));
        $this->db->order_by('tbl_buyer_post_requirement.id' , 'Desc');
        $this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
        $this->db->where('tbl_buyer_post_requirement.status <>' , 'Delete');
        $this->db->where('tbl_buyer_post_requirement.requirment_status' , 'closed');
        //$this->db->where('tbl_subcategory_master.is_delete <>','1');
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_buyer_post_requirement.subcategory_id');
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
        $data['getRequirments'] = $this->master_model->getRecords('tbl_buyer_post_requirement',FALSE,FALSE,FALSE,$page,$config1["per_page"]);


	    $this->load->view('template',$data);
	}

	public function closed_requirment_detail($req_id=FALSE)
	{
    	$data['pageTitle']       = 'Closed requirements detail- '.PROJECT_NAME;
   	    $data['page_title']      = 'Closed requirements detail - '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/buyer-closed-requirement-details';

        $this->db->where('tbl_buyer_post_requirement.buyer_id' , $this->session->userdata('user_id'));
        $this->db->where('tbl_buyer_post_requirement.id' , $req_id);
        $this->db->where('tbl_buyer_post_requirement.requirment_status' , 'closed');
        //$this->db->where('tbl_subcategory_master.is_delete <>','1');
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_buyer_post_requirement.subcategory_id');
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
        $data['getRequirmentsDetail']  = $this->master_model->getRecords('tbl_buyer_post_requirement');
        $Count = count($data['getRequirmentsDetail']);

        /* offered sellers */ 
        $this->db->where('tbl_apply_for_requirment.requirment_id' , $req_id);
        $this->db->where('tbl_apply_for_requirment.status'   , 'Accepted');
        $this->db->order_by('tbl_apply_for_requirment.id' ,  'desc');
        $this->db->where('tbl_user_master.status'   , 'Unblock');
        $select = '
                   tbl_apply_for_requirment.*,
                   tbl_user_master.user_image,
                   tbl_user_master.name,
                   tbl_user_master.email,
                   tbl_user_master.city,
                   tbl_user_master.country,
                   tbl_user_master.state
        ';
        $this->db->join('tbl_user_master' , 'tbl_user_master.id=tbl_apply_for_requirment.offered_seller_id');
        $data['offered_sellers'] = $this->master_model->getRecords('tbl_apply_for_requirment',FALSE,$select);

        /* end offered sellers */ 
	    $this->load->view('template',$data);
	}

	
	public function contact_to_seller()
	{
		$seller_id    = $this->input->post('seller_id');
		$category_id  = $this->input->post('category_id');
		$product_name = $this->input->post('product_name');
		$country      = $this->input->post('country');
		$description  = $this->input->post('description');
 
		$arr_inq   = array(
		'buyer_id'         => $this->session->userdata('user_id'),
		'seller_id'        => $seller_id,
		'category_id'      => $category_id,
		'product_name'     => $product_name,
		'description'      => $description, 
		'country'          => $country, 
		'posted_date'      => date('Y-m-d H:m:s'),
		'status'           => 'Unblock'
		);

		if($this->db->insert('tbl_seller_contact_inquires' , $arr_inq)){


               $get_seller_data              = $this->master_model->getRecords('tbl_user_master',array('id'=>$seller_id));
               $user_info = array(
				  				"name"               => $this->session->userdata('user_name'),
				  				"email"              => $this->session->userdata('user_email'),
				  				"subject"            => 'Contact To Seller',
				  				"product_name"       => $product_name,
				  				"country"            => $country,
				  				"message"            => $this->input->post('description'),

	  			);
	  			$info_arr   =array(
						        'from'    		     => $this->session->userdata('user_email'),
						        'to'	 	         => $get_seller_data[0]['email'],
						        'subject'	         => 'Contact To Seller - '.PROJECT_NAME,
						        'view'		         => 'contact-to-seller'
				);
				$this->email_sending->sendmail($info_arr,$user_info);
  		    



            $arr_response['status'] = "success";
	    	$arr_response['msg']    = "Your contact request send successfully to this seller.waiting for sellers responce.";
	       	echo json_encode($arr_response);
			exit;
		}
		else {
			$arr_response['status'] = "error";
			$arr_response['msg']    = "Please give permition to upload folder.";
			echo json_encode($arr_response);
			exit;
		}
	}

    public function notifications(){

    	$data['pageTitle']       = 'Buyer notifications - '.PROJECT_NAME;
   	    $data['page_title']      = 'Buyer notifications - '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/notifications';
   	    

        $this->db->where('status' , 'Unblock');
        $this->db->order_by('id' , 'desc');
        $this->db->where('buyer_id' , $this->session->userdata('user_id'));
   	    $data['get_notifications']  = $this->master_model->getRecords('tbl_buyer_notifications');
        $Count = count($data['get_notifications']);


   	    /* create pagination */
	    $this->load->library('pagination');
	    $config1['total_rows']           = $Count;
	    $config1['base_url']             = base_url().'buyer/notifications';
	    $config1['per_page']             = 10;
	    $config1['uri_segment']          = '3';
	    $config1['full_tag_open']        = '<div class="pagination pull-right"><ul class="pagination pagination-blog">';
	    $config1['full_tag_close']       = '</ul></div>';

	    $config1['first_link']           = '<i class="fa fa-angle-double-left" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['first_tag_open']       = '<li class="prev page">';
	    $config1['first_tag_close']      = '</li>';

	    $config1['last_link']            = '<i class="fa fa-angle-double-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['last_tag_open']        = '<li class="next page">';
	    $config1['last_tag_close']       = '</li>';

	    $config1['next_link']            = '<i class="fa fa-angle-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['next_tag_open']        = '<li class="next page">';
	    $config1['next_tag_close']       = '</li>';

	    $config1['prev_link']            = '<i class="fa fa-angle-left" style="font-size: 1.4em;"></i>';
	    $config1['prev_tag_open']        = '<li class="prev page">';
	    $config1['prev_tag_close']       = '</li>';

	    $config1['cur_tag_open']         = '<li ><a href="" class="act" style="color: rgb(242, 246, 249);background-color: #034A7B;" >';
	    $config1['cur_tag_close']        = '</a></li>';

	    $config1['num_tag_open']         = '<li class="page">';
	    $config1['num_tag_close']        = '</li>'; 
	    

	    $this->pagination->initialize($config1);
	    $page        = ($this->uri->segment(3));
	    /* end create pagination */

	    $this->db->where('status' , 'Unblock');
	    $this->db->order_by('id' , 'desc');
        $this->db->where('buyer_id' , $this->session->userdata('user_id'));
   	    $data['get_notifications']  = $this->master_model->getRecords('tbl_buyer_notifications',FALSE,FALSE,FALSE,$page,$config1["per_page"]);
   	    $this->load->view('template',$data);

    }

    public function notification_details($noti_id=FALSE){

    	$data['pageTitle']       = 'Notifications Detail- '.PROJECT_NAME;
   	    $data['page_title']      = 'Notifications Detail- '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/notifications-detail';
		
		

		$arg = explode('_', $noti_id);

		if($arg[0]=='id')
		{
			//echo "Id is here ";
			$noti_id = substr($noti_id, strpos($noti_id, "_") + 1);
			
			/*$this->db->select("n.details,n.inq_reply_id,n.created_date, r.reply_desc,f.offer_description,p.title,u.name");
			$this->db->from("tbl_buyer_notifications n, tbl_inquery_reply_to_buyer r,tbl_apply_for_offers f,tbl_seller_products_offers p,tbl_user_master u");
			$this->db->where("n.id",$noti_id);
			$this->db->where("n.inq_reply_id=r.inq_reply_id");
			$this->db->where('f.id=r.apply_offer_id');

			$this->db->where("p.id=r.inquery_id");
			$this->db->where("p.seller_id=u.id");
			$query=$this->db->get();
			$data['get_notifications']=$query->result_array();*/


			$this->db->select('
								tbl_buyer_notifications.details,
								tbl_buyer_notifications.inq_reply_id,
								tbl_buyer_notifications.created_date,
								tbl_inquery_reply_to_buyer.reply_desc,
								tbl_apply_for_offers.offer_description,
								tbl_apply_for_offers.id as apply_offer_id,
								tbl_seller_products_offers.title,
								tbl_seller_products_offers.id,
								tbl_user_master.name,
								tbl_user_master.id as user_id
							');
			$this->db->where("tbl_buyer_notifications.id",$noti_id);
			$this->db->join(
								'tbl_inquery_reply_to_buyer',
								'tbl_inquery_reply_to_buyer.inq_reply_id=tbl_buyer_notifications.inq_reply_id'
							);

			$this->db->join(
								'tbl_apply_for_offers',
								'tbl_apply_for_offers.id=tbl_inquery_reply_to_buyer.apply_offer_id'
							);

			$this->db->join(
								'tbl_seller_products_offers',
								'tbl_seller_products_offers.id=tbl_inquery_reply_to_buyer.inquery_id'
							);

			$this->db->join(
								'tbl_user_master',
								'tbl_user_master.id=tbl_seller_products_offers.seller_id'
							);

			$data['get_notifications']  = $this->master_model->getRecords('tbl_buyer_notifications');	

			
		}
		else
		{
			$this->db->where('status' , 'Unblock');
	        $this->db->where('id' , $noti_id);
	   	    $data['get_notifications']  = $this->master_model->getRecords('tbl_buyer_notifications');
	   	    //echo "<pre>"; print_r($data['get_notifications']);exit;
	        $this->db->where('id' , $noti_id);
		}   

		/*echo "<pre>";
			print_r($data);
		echo "</pre>";	    */

        
        $this->db->update('tbl_buyer_notifications',array('is_read' => 'yes'));

      $this->load->view('template',$data);
    }

    public function notification_delete($noti_id=FALSE){

		if($this->master_model->updateRecord('tbl_buyer_notifications',array('status'=>'Delete'),array('id'=>$noti_id)))
	  	{
	  		$this->session->set_flashdata('success','Notification deleted successfully.');
	  		redirect(base_url().'buyer/notifications');
	  	}
	  	else
	  	{
	  		$this->session->set_flashdata('error','Error while deleting Notification.');
	  		redirect(base_url().'buyer/notifications');
	  	}

	}
	public function rating_for_seller(){
       
		$outputData="";
    	$data_arr=array(
         
             'seller_id'     => $this->input->post('seller_id'),
             'buyer_id'      => $this->session->userdata('user_id'),
             'review_for'    => 'seller',
             'ratings'       => $this->input->post('review_cnt'),
             'comment'       => $this->input->post('txt_comment'),
             'commented_at'  => date('Y-m-d H:m:s'),
             'status'             =>'Unblock',
             'is_read'            =>'no'

    		);
    	$this->db->insert('tbl_seller_rating',$data_arr);
    	$insert_id = $this->db->insert_id(); 	


        /* insert notification */
        $arr_noti  = array(
        'seller_id'          => $this->input->post('seller_id'),
        'buyer_id'           => $this->session->userdata('user_id'),
        'notification'       => $this->session->userdata('user_name').' give review on your profile.',
        'url'                => 'seller/reviews_detail/'.$insert_id,
        'details'            => '',
        'created_date'       => date('Y-m-d H:m:s'),
        'status'             =>'Unblock',
        'is_read'            =>'no'
        );
        $this->db->insert('tbl_seller_notifications' , $arr_noti);



        $this->db->where('id',1);
        $email_info=$this->master_model->getRecords('admin_login');
        $admin_contact_email=$email_info[0]['admin_email'];

        $get_user_data              = $this->master_model->getRecords('tbl_user_master',array('id'=>$this->input->post('seller_id')));
        $user_info = array(
                        'url'                => 'seller/notifications',
        );
        $info_arr   =array(
                        'from'               => $admin_contact_email,
                        'to'                 => $get_user_data[0]['email'],
                        'subject'            => 'notification received -'.PROJECT_NAME,
                        'view'               => 'notification-receive'
        );
        $this->email_sending->sendmail($info_arr,$user_info);



        $get_seller_reviews = 0;
        $ratings            = 0;
        $arv_rating         = 0;       
        $this->db->where('tbl_seller_rating.seller_id', $this->input->post('seller_id'));
        $this->db->where('tbl_seller_rating.status !=' , 'Delete');
        $this->db->where('tbl_seller_rating.review_for', 'seller');
        $get_seller_reviews=$this->master_model->getRecords('tbl_seller_rating');
        if(empty($get_seller_reviews))
        {
        }
        else
        {
        foreach($get_seller_reviews as $field => $value)
        {
        $ratings         +=  $value['ratings'];
        }
        $arv_rating      = $ratings / count($get_seller_reviews);  // find rating avarage
        }  

        $arr_Data = array('rating_avg' => $arv_rating);
        $this->db->where('id' , $this->input->post('seller_id'));
        $this->db->update('tbl_user_master',$arr_Data);

    	echo $outputData="done";
	}
	public function sent_inquiry_detail($inq_id=FALSE)
	{
		$this->master_model->IsLogged();
		if($this->session->userdata('user_type')!='Buyer')
        {
           $this->session->set_flashdata('error' , 'Sorry!!, you cant access this page....');
           redirect(base_url().lcfirst($this->session->userdata('user_type'))."/dashboard");
        }
    	$data['pageTitle']       = 'Inquires Details- '.PROJECT_NAME;
   	    $data['page_title']      = 'Inquires Details- '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/sent-inquiries-details';
        
        $this->db->order_by('tbl_seller_contact_inquires.id' , 'desc');
        $this->db->where('tbl_seller_contact_inquires.id' , $inq_id);
        $this->db->where('tbl_seller_contact_inquires.buyer_id' , $this->session->userdata('user_id'));
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_seller_contact_inquires.category_id');
        $data['inquires_detail']  = $this->master_model->getRecords('tbl_seller_contact_inquires');
   		//echo "<pre>"; print_r($data['inquires_detail']); exit;
	    $this->load->view('template',$data);
	}
	public function logout()
	{
		$this->session->sess_destroy();
		$this->session->set_flashdata('success' , 'Logout successfully');
		redirect(base_url().'login');
	}
	public function generate_pdf(){
            
            $get_user_data                       = $this->master_model->getRecords('tbl_user_master',array('id'=>1));
            $this->db->where('tbl_apply_for_requirment.id' , 1);
            $getofferdetails = $this->master_model->getRecords('tbl_apply_for_requirment');

            $this->db->where('tbl_buyer_post_requirement.id' , 1);
            $getreqdetails = $this->master_model->getRecords('tbl_buyer_post_requirement');
            
		    $data['get_user_data']   = $get_user_data;
		    $data['getofferdetails'] = $getofferdetails;
		    $data['getreqdetails']   = $getreqdetails;
            ob_start();
            /* Generate */
		            $htmlview= $this->load->view('invoice/buyer-to-seller-invoice-pdf' , $data,true);
			        $this->load->library('pdf');
			        $test = new TCPDF();
					$this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 12));
					$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
					$this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
					$this->pdf->SetPrintHeader(false);
					$this->pdf->SetPrintFooter(false);
					$this->pdf->SetMargins(PDF_MARGIN_LEFT, 0, PDF_MARGIN_RIGHT);
					$this->pdf->SetHeaderMargin(-11);
					$this->pdf->SetFooterMargin(20);
					$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
					//$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
					$this->pdf->AddPage();
					$invoice_file='ReceiptPDF-000'.'dasda'.'.pdf';
					$html =$htmlview;
					//$this->pdf->SetFont('helvetica', 'B', 20);
					$html_write=$this->pdf->writeHTML($html, true, true, true, true);
					ob_end_clean();
					$getcontent=$this->pdf->Output($invoice_file,'D');/*Download*/
					//$getcontent=$this->pdf->Output("uploads/".$invoice_file,'I');/*OPEN IN BROWSER*/
					//$getcontent=$this->pdf->Output("uploads/invoice/".$invoice_file,'F');/*UPLOAD*/
					/*return $invoice_file;*/
            /* Generate */
	}
	public function order_forms()
	{
    	$data['pageTitle']       = 'Order forms - '.PROJECT_NAME;
   	    $data['page_title']      = 'Order forms - '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/buyer-order-forms';


        /*$this->db->where('tbl_buyer_post_requirement.buyer_id' , $this->session->userdata('user_id'));
        $this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
        $this->db->where('tbl_buyer_post_requirement.status <>' , 'Delete');
        $this->db->where('tbl_buyer_post_requirement.requirment_status' , 'closed');
        $this->db->where('tbl_buyer_post_requirement.order_from !=' , '');
        //$this->db->where('tbl_subcategory_master.is_delete <>','1');
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_buyer_post_requirement.subcategory_id');
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
        $data['getRequirments']  = $this->master_model->getRecords('tbl_buyer_post_requirement');*/

        $this->db->where('from_id' , $this->session->userdata('user_id'));
        $this->db->or_where('to_id' , $this->session->userdata('user_id'));
        $data['getOffers']  = $this->master_model->getRecords('tbl_order_forms');
        $Count = count($data['getOffers']);


        /* create pagination */
	    $this->load->library('pagination');
	    $config1['total_rows']          = $Count;
	    $config1['base_url']            = base_url().'buyer/order_forms';
	    $config1['per_page']            = 8;
	    $config1['uri_segment']         = '3';
	    $config1['full_tag_open']        = '<div class="pagination pull-right"><ul class="pagination pagination-blog">';
	    $config1['full_tag_close']       = '</ul></div>';

	    $config1['first_link']           = '<i class="fa fa-angle-double-left" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['first_tag_open']       = '<li class="prev page">';
	    $config1['first_tag_close']      = '</li>';

	    $config1['last_link']            = '<i class="fa fa-angle-double-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['last_tag_open']        = '<li class="next page">';
	    $config1['last_tag_close']       = '</li>';

	    $config1['next_link']            = '<i class="fa fa-angle-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['next_tag_open']        = '<li class="next page">';
	    $config1['next_tag_close']       = '</li>';

	    $config1['prev_link']            = '<i class="fa fa-angle-left" style="font-size: 1.4em;"></i>';
	    $config1['prev_tag_open']        = '<li class="prev page">';
	    $config1['prev_tag_close']       = '</li>';

	    $config1['cur_tag_open']         = '<li ><a href="" class="act" style="color: rgb(242, 246, 249);background-color: #034A7B;" >';
	    $config1['cur_tag_close']        = '</a></li>';

	    $config1['num_tag_open']         = '<li class="page">';
	    $config1['num_tag_close']        = '</li>'; 
	    

	    $this->pagination->initialize($config1);
	    $page        = ($this->uri->segment(3));
	    /* end create pagination */

   /*     $this->db->where('tbl_buyer_post_requirement.buyer_id' , $this->session->userdata('user_id'));
        $this->db->order_by('tbl_buyer_post_requirement.id' , 'Desc');
        $this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
        $this->db->where('tbl_buyer_post_requirement.status <>' , 'Delete');
        $this->db->where('tbl_buyer_post_requirement.order_from !=' , '');
        $this->db->where('tbl_buyer_post_requirement.requirment_status' , 'closed');
        //$this->db->where('tbl_subcategory_master.is_delete <>','1');
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_buyer_post_requirement.subcategory_id');
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');*/
        $this->db->where('from_id' , $this->session->userdata('user_id'));
        $this->db->or_where('to_id' , $this->session->userdata('user_id'));
        $this->db->order_by('tbl_order_forms.id' , 'Desc');
        $data['getOffers'] = $this->master_model->getRecords('tbl_order_forms',FALSE,FALSE,FALSE,$page,$config1["per_page"]);


	    $this->load->view('template',$data);
	}


	/*
    | Function : Store data in tbl_contact_info_in_dashboard
    | Author   : Deepak Arvind Salunke
    | Date     : 19/05/2017
    | Output   : Success or Failure
    */

	public function add_to_contact()
	{
		$receiver_id 			= $this->input->post('seller_id');
		$name 					= $this->input->post('name');
		$email 					= $this->input->post('email');
		$mobile_number  		= $this->input->post('mobile_number');
		$country 				= $this->input->post('country');
 
		$arr_contact_data   = array(
		'sender_id' 			=> $this->session->userdata('user_id'),
		'receiver_id' 			=> $receiver_id,
		'sender_type' 			=> $this->session->userdata('user_type'),
		'name' 					=> $name,
		'email' 				=> $email,
		'mobile_number' 		=> $mobile_number, 
		'country' 				=> $country, 
		);

		// get record from data using same user_id and receiver_id
		$where = array('email' => $email, 'mobile_number' => $mobile_number);
		$check_contact = $this->master_model->getRecords('tbl_contact_info_in_dashboard', $where);

		if(count($check_contact) > 0)
		{
			$arr_response['contact_status'] = "error";
	    	$arr_response['contact_msg']    = "This Seller is already exists in Your Contact List.";
	       	echo json_encode($arr_response);
			exit;
		}

		if($this->db->insert('tbl_contact_info_in_dashboard' , $arr_contact_data)){
            $arr_response['contact_status'] = "success";
	    	$arr_response['contact_msg']    = "This Seller is added to Your Contact List.";
	       	echo json_encode($arr_response);
			exit;
		}
		else {
			$arr_response['contact_status'] = "error";
			$arr_response['contact_msg']    = "Something went Wrong!!!. This Seller was not added to Your Contact List.";
			echo json_encode($arr_response);
			exit;
		}
	} // end add_to_contact

	/*
    | Function : Display contact list of this buyer_id
    | Author   : Deepak Arvind Salunke
    | Date     : 19/05/2017
    | Output   : Success or Failure
    */



    public function add_seller_to_my_contact()
	{
		$receiver_id 	= $this->input->post('seller_id');
        $buyer_id      = $this->session->userdata('user_id');
  
        $this->db->where('tbl_user_master.id' , $receiver_id);
        $getSellerInfo      = $this->master_model->getRecords('tbl_user_master');

       
        $name = $email = $mobile_number = $country = $sender_type = '';

        if(!empty($getSellerInfo[0]['name'])) { $name = $getSellerInfo[0]['name']; }
        if(!empty($getSellerInfo[0]['email'])) { $email = $getSellerInfo[0]['email']; }
        if(!empty($getSellerInfo[0]['mobile_number'])) { $mobile_number = $getSellerInfo[0]['mobile_number']; }
        if(!empty($getSellerInfo[0]['mobile_number'])) { $country = $getSellerInfo[0]['mobile_number']; }
        if(!empty($getSellerInfo[0]['user_type'])) { $sender_type = $getSellerInfo[0]['user_type']; }

		$name          = $name;
		$email         = $email;
		$mobile_number = $mobile_number;
		$country       = $country;
		$sender_type   = $sender_type;
 
		$arr_contact_data   = array(
		/*'sender_id' 			=> $this->session->userdata('user_id'),
		'receiver_id' 			=> $receiver_id,*/
		'sender_id' 			=> $receiver_id,
		'receiver_id' 			=> $this->session->userdata('user_id'),
		'sender_type' 			=> $sender_type,
		'name' 					=> $name,
		'email' 				=> $email,
		'mobile_number' 		=> $mobile_number, 
		'country' 				=> $country, 
		);

		// get record from data using same user_id and receiver_id
		$where = array('email' => $email, 'mobile_number' => $mobile_number);
		$check_contact = $this->master_model->getRecords('tbl_contact_info_in_dashboard', $where);

		if(count($check_contact) > 0)
		{
			$arr_response['contact_status'] = "error";
	    	$arr_response['contact_msg']    = "This Seller is already exists in Your Contact List.";
	       	echo json_encode($arr_response);
			exit;
		}

		if($this->db->insert('tbl_contact_info_in_dashboard' , $arr_contact_data)){
            $arr_response['contact_status'] = "success";
	    	$arr_response['contact_msg']    = "This Seller is added to Your Contact List.";
	       	echo json_encode($arr_response);
			exit;
		}
		else {
			$arr_response['contact_status'] = "error";
			$arr_response['contact_msg']    = "Something went Wrong!!!. This Seller was not added to Your Contact List.";
			echo json_encode($arr_response);
			exit;
		}
	} 
    




	public function contact_list()
	{
		$data['pageTitle']       = 'Contact List - '.PROJECT_NAME;
   	    $data['page_title']      = 'Contact List - '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/contact_list';

        $this->db->order_by('id' , 'desc');
        $this->db->where('receiver_id' , $this->session->userdata('user_id'));
        $this->db->where('sender_type' , "Seller");
   	    $data['get_contact_list']  = $this->master_model->getRecords('tbl_contact_info_in_dashboard');
        $Count = count($data['get_contact_list']);


   	    /* create pagination */
	    $this->load->library('pagination');
	    $config1['total_rows']           = $Count;
	    $config1['base_url']             = base_url().'buyer/contact_list';
	    $config1['per_page']             = 10;
	    $config1['uri_segment']          = '3';
	    $config1['full_tag_open']        = '<div class="pagination pull-right"><ul class="pagination pagination-blog">';
	    $config1['full_tag_close']       = '</ul></div>';

	    $config1['first_link']           = '<i class="fa fa-angle-double-left" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['first_tag_open']       = '<li class="prev page">';
	    $config1['first_tag_close']      = '</li>';

	    $config1['last_link']            = '<i class="fa fa-angle-double-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['last_tag_open']        = '<li class="next page">';
	    $config1['last_tag_close']       = '</li>';

	    $config1['next_link']            = '<i class="fa fa-angle-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['next_tag_open']        = '<li class="next page">';
	    $config1['next_tag_close']       = '</li>';

	    $config1['prev_link']            = '<i class="fa fa-angle-left" style="font-size: 1.4em;"></i>';
	    $config1['prev_tag_open']        = '<li class="prev page">';
	    $config1['prev_tag_close']       = '</li>';

	    $config1['cur_tag_open']         = '<li ><a href="" class="act" style="color: rgb(242, 246, 249);background-color: #034A7B;" >';
	    $config1['cur_tag_close']        = '</a></li>';

	    $config1['num_tag_open']         = '<li class="page">';
	    $config1['num_tag_close']        = '</li>'; 
	    

	    $this->pagination->initialize($config1);
	    $page        = ($this->uri->segment(3));
	    /* end create pagination */

	    $this->db->order_by('id' , 'desc');
        $this->db->where('receiver_id' , $this->session->userdata('user_id'));
        $this->db->where('sender_type' , "Seller");
   	    $data['get_contact_list']  = $this->master_model->getRecords('tbl_contact_info_in_dashboard',FALSE,FALSE,FALSE,$page,$config1["per_page"]);
   	    $this->load->view('template',$data);
	}// end contact_list

	/*
    | Function : Delete contact list
    | Author   : Deepak Arvind Salunke
    | Date     : 19/05/2017
    | Output   : Success or Failure
    */
	
	public function contact_delete($contact_id=FALSE)
	{
		$this->db->where('id', $contact_id);
		$query = $this->db->delete('tbl_contact_info_in_dashboard');
		if($query)
	  	{
	  		$this->session->set_flashdata('success','Contact deleted successfully.');
	  		redirect(base_url().'buyer/contact_list');
	  	}
	  	else
	  	{
	  		$this->session->set_flashdata('error','Error while deleting Contact.');
	  		redirect(base_url().'buyer/contact_list');
	  	}
	} // end contact_delete

	/*
    | Function : Order send to seller by buyer from message section and insert order data into tbl_apply_for_order
    | Author   : Deepak Arvind Salunke
    | Date     : 19/05/2017
    | Output   : Send email and notification along with pdf
    */

	public function order_report()
	{
		$product_name            = $this->input->post('product_name');
        $seller_id         		 = $this->input->post('seller_id');

		$order_update_array = array(
          'status' => 'Accepted',
          'Merchandise_Description' => $this->input->post('Merchandise_Description'),
          'Transport'               => $this->input->post('Transport'),
          'Delivery_Date'           => $this->input->post('Delivery_Date'),
          'Order_type'              => $this->input->post('Order_type'),
          'Final_price'             => $this->input->post('Final_price'),
          'Quantity'              	=> $this->input->post('Quantity'),
          'Payment_Currency'        => $this->input->post('Payment_Currency'),
          'Payment_Conditions'      => $this->input->post('Payment_Conditions'),
          'Packing_Weight'          => $this->input->post('Packing_Weight'),
          'Trade_confirmed_by'      => $this->input->post('Trade_confirmed_by'),
          'product_name'            => $product_name,
          'order_seller_id'         => $seller_id,
          'order_buyer_id'			=> $this->session->userdata('user_id'),
		);

        $order_insert 		= $this->db->insert('tbl_apply_for_order' , $order_update_array);
        $order_id 			= $this->db->insert_id();
		$get_user_data      = $this->master_model->getRecords('tbl_user_master',array('id'=>$seller_id));

        if($order_insert){
        	
            $data['get_user_data']   = $get_user_data;
            $data['product_name']    = $product_name;
            $data['pdf_data']        = $order_update_array;
            ob_start();
            /* Generate */
		            $htmlview= $this->load->view('invoice/seller_to_buyer_product_order_invoice_pdf' , $data,true);
			        $this->load->library('pdf');
			        $test = new TCPDF();
					$this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 12));
					$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
					$this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
					$this->pdf->SetPrintHeader(false);
					$this->pdf->SetPrintFooter(false);
					$this->pdf->SetMargins(PDF_MARGIN_LEFT, 0, PDF_MARGIN_RIGHT);
					$this->pdf->SetHeaderMargin(-11);
					$this->pdf->SetFooterMargin(20);
					$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
					//$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
					$this->pdf->AddPage();
					$invoice_file='OrderfromPDF-'.$this->session->userdata('user_id').$this->session->userdata('user_email').date('F j, Y', strtotime(date('Y-m-d H:m:s'))).$requirement.'.pdf';
					$html =$htmlview;
					//$this->pdf->SetFont('helvetica', 'B', 20);
					$html_write=$this->pdf->writeHTML($html, true, true, true, true);
					ob_end_clean();
					//$getcontent=$this->pdf->Output($invoice_file,'D');/*Download*/
					//$getcontent=$this->pdf->Output("uploads/".$invoice_file,'I');/*OPEN IN BROWSER*/
					$getcontent=$this->pdf->Output("uploads/invoice/".$invoice_file,'F');/*UPLOAD*/
					/*return $invoice_file;*/
            /* Generate */

			
            /* end send invoice */

            /* send email */
 			$this->db->where('id',1);
            $email_info=$this->master_model->getRecords('admin_login');
            $admin_contact_email = $email_info[0]['admin_email'];

            $user_info = array(
                            'url'                => 'seller/notifications',
            );
            $info_arr   =array(
                            'from'               => $admin_contact_email,
                            'to'                 => $get_user_data[0]['email'],
                            'subject'            => 'notification received -'.PROJECT_NAME,
                            'view'               => 'notification-receive'
            );
            $this->email_sending->sendmail($info_arr,$user_info);

            /* insert notification */
            $arr_noti  = array(
            'seller_id'          => $seller_id,
            'buyer_id'           => $this->session->userdata('user_id'),
            'notification'       => 'you have new notification for  - <b>'.$product_name.'</b>.',
            'url'                => '',
            'details'            => 'You recieved order from buyer <p style="margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;"><a href="'.base_url()."uploads/invoice/".$invoice_file.'" download style=" background: #1C9AEA ;border-radius: 5px;color: #fff;display: block;margin: 0 auto; max-width: 120px;padding: 10px;text-align: center;text-decoration: none;    width: 100%"> <i class="fa fa-cloud-download" aria-hidden="true"></i> Order Form</a> <span style="color:#0000FF"></span></p>',
            'created_date'       => date('Y-m-d H:m:s'),
            'status'             =>'Unblock',
            'is_read'            =>'no'
            );
            $this->db->insert('tbl_seller_notifications' , $arr_noti);

            $this->db->where('id', $order_id);
            $this->db->update('tbl_apply_for_order' , array('order_from' => $invoice_file));



            $order_add_array = array(
				  'from_id'                 =>  $this->session->userdata('user_id'),
				  'to_id'                   =>  $seller_id,
				  'order_id'                =>  '',
				  'order_name'              =>  $this->input->post('product_name'),
				  'apply_offer_id'          =>  '',//$this->input->post('apply_offer_id'),
		          'merchadise_description'  =>  $this->input->post('Merchandise_Description'),
		          'transport'               =>  $this->input->post('Transport'),
		          'delivery_date'           =>  $this->input->post('Delivery_Date'),
		          'order_type'              =>  $this->input->post('Order_type'),
		          'type'                    =>  'product',
		          'final_price'             =>  $this->input->post('Final_price'),
		          'quantity'              	=>  $this->input->post('Quantity'),
		          'payment_currency'        =>  $this->input->post('Payment_Currency'),
		          'payment_conditions'      =>  $this->input->post('Payment_Conditions'),
		          'package_weight'          =>  $this->input->post('Packing_Weight'),
		          'trade_confirmed_by'      =>  $this->input->post('Trade_confirmed_by'),
		          'order_form'              =>  $invoice_file,
	              'date'                    =>  date('Y-m-d H:m:s')
				);

		    $this->db->insert('tbl_order_forms' , $order_add_array);
		    $insert_id = $this->db->insert_id();
 


        	$arr_response['status'] = "success";
	    	$arr_response['msg']    = "You successfully accepted  offer.";
	       	echo json_encode($arr_response);
			exit;
        }
        else {
        	$arr_response['status'] = "error";
	        $arr_response['msg']    = "Something was wrong,Please try again.";
	       	echo json_encode($arr_response);
			exit;
        }
		
	} // end order_report

	/*
    | Function : 
    | Author   : Deepak Arvind Salunke
    | Date     : 20/05/2017
    | Output   : 
    */

	public function order_received()
	{
		$data['pageTitle']       = 'Order Forms Received - '.PROJECT_NAME;
   	    $data['page_title']      = 'Order Forms Received - '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/buyer_order_forms_received';

        $this->db->order_by('tbl_apply_for_offers.id' , 'desc');
        $this->db->select('tbl_apply_for_offers.*,tbl_user_master.name,tbl_seller_products_offers.order_from,tbl_seller_products_offers.title');
        $this->db->where('tbl_apply_for_offers.offered_buyer_id' , $this->session->userdata('user_id'));
        $this->db->where('tbl_seller_products_offers.order_from !=' , '');
        $this->db->join('tbl_user_master', 'tbl_user_master.id=tbl_apply_for_offers.offered_buyer_id');
        $this->db->join('tbl_seller_products_offers', 'tbl_seller_products_offers.id=tbl_apply_for_offers.offer_id');
   	    $data['get_offers_form']  = $this->master_model->getRecords('tbl_apply_for_offers');

   	    //echo "<pre>"; print_r($data['get_offers_form']);exit;
        
        $Count = count($data['get_offers_form']);

   	    /* create pagination */
	    $this->load->library('pagination');
	    $config1['total_rows']           = $Count;
	    $config1['base_url']             = base_url().'buyer/order_received';
	    $config1['per_page']             = 10;
	    $config1['uri_segment']          = '3';
	    $config1['full_tag_open']        = '<div class="pagination pull-right"><ul class="pagination pagination-blog">';
	    $config1['full_tag_close']       = '</ul></div>';

	    $config1['first_link']           = '<i class="fa fa-angle-double-left" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['first_tag_open']       = '<li class="prev page">';
	    $config1['first_tag_close']      = '</li>';

	    $config1['last_link']            = '<i class="fa fa-angle-double-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['last_tag_open']        = '<li class="next page">';
	    $config1['last_tag_close']       = '</li>';

	    $config1['next_link']            = '<i class="fa fa-angle-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['next_tag_open']        = '<li class="next page">';
	    $config1['next_tag_close']       = '</li>';

	    $config1['prev_link']            = '<i class="fa fa-angle-left" style="font-size: 1.4em;"></i>';
	    $config1['prev_tag_open']        = '<li class="prev page">';
	    $config1['prev_tag_close']       = '</li>';

	    $config1['cur_tag_open']         = '<li ><a href="" class="act" style="color: rgb(242, 246, 249);background-color: #034A7B;" >';
	    $config1['cur_tag_close']        = '</a></li>';

	    $config1['num_tag_open']         = '<li class="page">';
	    $config1['num_tag_close']        = '</li>'; 
	    

	    $this->pagination->initialize($config1);
	    $page        = ($this->uri->segment(3));
	    /* end create pagination */

	    $this->db->order_by('tbl_apply_for_offers.id' , 'desc');
        $this->db->select('tbl_apply_for_offers.*,tbl_user_master.name,tbl_seller_products_offers.order_from,tbl_seller_products_offers.title');
        $this->db->where('tbl_apply_for_offers.offered_buyer_id' , $this->session->userdata('user_id'));
        $this->db->where('tbl_seller_products_offers.order_from !=' , '');
        $this->db->join('tbl_user_master', 'tbl_user_master.id=tbl_apply_for_offers.offered_buyer_id');
        $this->db->join('tbl_seller_products_offers', 'tbl_seller_products_offers.id=tbl_apply_for_offers.offer_id');
   	    $data['get_offers_form']  = $this->master_model->getRecords('tbl_apply_for_offers',FALSE,FALSE,FALSE,$page,$config1["per_page"]);

   	    $this->load->view('template',$data);
	}// end order_received


	/*
    | Function : get all the order send forms for buyer (current user)
    | Author   : Deepak Arvind Salunke
    | Date     : 20/05/2017
    | Output   : Order data along seller name from user table
    */

	public function product_order_send()
	{
		$this->master_model->IsLogged();
		if($this->session->userdata('user_type')!='Buyer')
        {
           $this->session->set_flashdata('error' , 'Sorry!!, you cant access this page....');
           redirect(base_url().lcfirst($this->session->userdata('user_type'))."/dashboard");
        }
    	$data['pageTitle']       = 'Order forms - '.PROJECT_NAME;
   	    $data['page_title']      = 'Order forms - '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/buyer_product_order_forms_send';

        $this->db->order_by('tbl_apply_for_order.id' , 'desc');
        $this->db->select('tbl_apply_for_order.*,tbl_user_master.name');
        $this->db->where('tbl_apply_for_order.order_buyer_id' , $this->session->userdata('user_id'));
        $this->db->where('tbl_apply_for_order.order_from !=' , '');
        $this->db->join('tbl_user_master', 'tbl_user_master.id=tbl_apply_for_order.order_seller_id');
   	    $data['get_order_form']  = $this->master_model->getRecords('tbl_apply_for_order');
        
        $Count = count($data['get_order_form']);
        //echo count($data['get_order_form']).' + '.count($data['getRequirments']).' = '.$Count;exit;
        //echo "<pre>"; print_r($data['get_order_form']);exit;
        //echo "<pre>"; print_r($_SESSION);exit;

        /* create pagination */
	    $this->load->library('pagination');
	    $config1['total_rows']           = $Count;
	    $config1['base_url']             = base_url().'buyer/product_order_send';
	    $config1['per_page']             = '12';
	    $config1['uri_segment']          = '3';
	    $config1['full_tag_open']        = '<div class="pagination pull-right"><ul class="pagination pagination-blog">';
	    $config1['full_tag_close']       = '</ul></div>';

	    $config1['first_link']           = '<i class="fa fa-angle-double-left" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['first_tag_open']       = '<li class="prev page">';
	    $config1['first_tag_close']      = '</li>';

	    $config1['last_link']            = '<i class="fa fa-angle-double-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['last_tag_open']        = '<li class="next page">';
	    $config1['last_tag_close']       = '</li>';

	    $config1['next_link']            = '<i class="fa fa-angle-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['next_tag_open']        = '<li class="next page">';
	    $config1['next_tag_close']       = '</li>';

	    $config1['prev_link']            = '<i class="fa fa-angle-left" style="font-size: 1.4em;"></i>';
	    $config1['prev_tag_open']        = '<li class="prev page">';
	    $config1['prev_tag_close']       = '</li>';

	    $config1['cur_tag_open']         = '<li ><a href="" class="act" style="color: rgb(242, 246, 249);background-color: #034A7B;" >';
	    $config1['cur_tag_close']        = '</a></li>';

	    $config1['num_tag_open']         = '<li class="page">';
	    $config1['num_tag_close']        = '</li>'; 
	    

	    $this->pagination->initialize($config1);
	    $page        = ($this->uri->segment(3));
	    /* end create pagination */

        $this->db->order_by('tbl_apply_for_order.id' , 'desc');
        $this->db->select('tbl_apply_for_order.*,tbl_user_master.name');
        $this->db->where('tbl_apply_for_order.order_buyer_id' , $this->session->userdata('user_id'));
        $this->db->where('tbl_apply_for_order.order_from !=' , '');
        $this->db->join('tbl_user_master', 'tbl_user_master.id=tbl_apply_for_order.order_seller_id');
        $data['get_order_form'] = $this->master_model->getRecords('tbl_apply_for_order',FALSE,FALSE,FALSE,$page,$config1["per_page"]);

	    $this->load->view('template',$data);
	} // end product_order_send

	/*
    | Function : get all the order send forms for buyer (current user)
    | Author   : Deepak Arvind Salunke
    | Date     : 20/05/2017
    | Output   : Order data along seller name from user table
    */

	public function requirement_order_send()
	{
		$this->master_model->IsLogged();
		if($this->session->userdata('user_type')!='Buyer')
        {
           $this->session->set_flashdata('error' , 'Sorry!!, you cant access this page....');
           redirect(base_url().lcfirst($this->session->userdata('user_type'))."/dashboard");
        }
    	$data['pageTitle']       = 'Order forms - '.PROJECT_NAME;
   	    $data['page_title']      = 'Order forms - '.PROJECT_NAME;
   	    $data['middle_content']  = 'buyer/buyer_requirement_order_forms_send';

   	    $this->db->order_by('tbl_buyer_post_requirement.id','Desc');
   	    $this->db->select('tbl_buyer_post_requirement.*,tbl_user_master.name');
   	    $this->db->where('tbl_buyer_post_requirement.buyer_id' , $this->session->userdata('user_id'));
        $this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
        $this->db->where('tbl_buyer_post_requirement.status <>' , 'Delete');
        $this->db->where('tbl_buyer_post_requirement.requirment_status' , 'closed');
        $this->db->where('tbl_buyer_post_requirement.order_from !=' , '');
        $this->db->join('tbl_user_master' , 'tbl_user_master.id = tbl_buyer_post_requirement.buyer_id');
        $data['getRequirments']  = $this->master_model->getRecords('tbl_buyer_post_requirement');
        
        $Count = count($data['getRequirments']);
        //echo count($data['get_order_form']).' + '.count($data['getRequirments']).' = '.$Count;exit;
        //echo "<pre>"; print_r($data['getRequirments']);exit;

        /* create pagination */
	    $this->load->library('pagination');
	    $config1['total_rows']           = $Count;
	    $config1['base_url']             = base_url().'buyer/requirement_order_send';
	    $config1['per_page']             = '12';
	    $config1['uri_segment']          = '3';
	    $config1['full_tag_open']        = '<div class="pagination pull-right"><ul class="pagination pagination-blog">';
	    $config1['full_tag_close']       = '</ul></div>';

	    $config1['first_link']           = '<i class="fa fa-angle-double-left" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['first_tag_open']       = '<li class="prev page">';
	    $config1['first_tag_close']      = '</li>';

	    $config1['last_link']            = '<i class="fa fa-angle-double-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['last_tag_open']        = '<li class="next page">';
	    $config1['last_tag_close']       = '</li>';

	    $config1['next_link']            = '<i class="fa fa-angle-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['next_tag_open']        = '<li class="next page">';
	    $config1['next_tag_close']       = '</li>';

	    $config1['prev_link']            = '<i class="fa fa-angle-left" style="font-size: 1.4em;"></i>';
	    $config1['prev_tag_open']        = '<li class="prev page">';
	    $config1['prev_tag_close']       = '</li>';

	    $config1['cur_tag_open']         = '<li ><a href="" class="act" style="color: rgb(242, 246, 249);background-color: #034A7B;" >';
	    $config1['cur_tag_close']        = '</a></li>';

	    $config1['num_tag_open']         = '<li class="page">';
	    $config1['num_tag_close']        = '</li>'; 
	    

	    $this->pagination->initialize($config1);
	    $page        = ($this->uri->segment(3));
	    /* end create pagination */

        $this->db->order_by('tbl_buyer_post_requirement.id','Desc');
        $this->db->select('tbl_buyer_post_requirement.*,tbl_user_master.name');
   	    $this->db->where('tbl_buyer_post_requirement.buyer_id' , $this->session->userdata('user_id'));
        $this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
        $this->db->where('tbl_buyer_post_requirement.status <>' , 'Delete');
        $this->db->where('tbl_buyer_post_requirement.requirment_status' , 'closed');
        $this->db->where('tbl_buyer_post_requirement.order_from !=' , '');
        $this->db->join('tbl_user_master' , 'tbl_user_master.id = tbl_buyer_post_requirement.buyer_id');
        $data['getRequirments']  = $this->master_model->getRecords('tbl_buyer_post_requirement',FALSE,FALSE,FALSE,$page,$config1["per_page"]);

	    $this->load->view('template',$data);
	} // end requirement_order_send

} //  end class
