<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {

	public function __construct() {

		parent::__construct();
		$this->load->model('email_sending');

		if($this->session->userdata('user_id') !=""){
			redirect(base_url().lcfirst($this->session->userdata('user_type')).'/dashboard');
		}
	}

	public function index() {
		
        $data['pageTitle']       ='Login - '.PROJECT_NAME;
   	    $data['page_title']      ='Login - '.PROJECT_NAME;
   	    $data['middle_content']  ='login';
	    $this->load->view('template',$data);
	}

	public function process() {

		$email         = $this->input->post('email');
		$pass          = $this->input->post('pwd');
		$login_checkbx = $this->input->post('login_checkbx',true);
		if($login_checkbx == "true") {	// if user check the remember me checkbox		
	   		setcookie('remember_me_email', $email, time()+60*60*24*100, "/");
	   		setcookie('remember_me_password', $pass, time()+60*60*24*100, "/");
		}
		else 
		{   
			// if user not check the remember me checkbox
	   		setcookie('remember_me_email', 'gone', time()-60*60*24*100, "/");			
	   		setcookie('remember_me_password', 'gone', time()-60*60*24*100, "/");			
		}
		$arr_user_check = $this->master_model->getRecords('tbl_user_master',array('email'=>trim($email),"password"=>sha1($pass)));

			if(sizeof($arr_user_check)>0)
			{
				if($arr_user_check[0]['status'] == "Unblock")
				{
					if($arr_user_check[0]['verification_status'] == "Verified")
					{
						    $user_data = array(
				                   'user_id'        => $arr_user_check[0]['id'],
				                   'user_name'      => $arr_user_check[0]['name'],
				                   'user_email'     => $arr_user_check[0]['email'],
				                   'user_state'     => $arr_user_check[0]['state'],
				                   'user_city'      => $arr_user_check[0]['city'],
				                   'user_country'   => $arr_user_check[0]['country'],
				                   'user_address'   => $arr_user_check[0]['address'],
				                   'user_mobile'    => $arr_user_check[0]['mobile_number'],
				                   'user_image'     => $arr_user_check[0]['user_image'],
				                   'user_type'      => $arr_user_check[0]['user_type'],
				            );
							$this->session->set_userdata($user_data);
						

						setcookie('remember_loggedin_user', $arr_user_check[0]['id'], time()+60*60*24*100, "/");
						setcookie('remember_loggedin_type', $arr_user_check[0]['user_type'], time()+60*60*24*100, "/");
						setcookie('remember_loggedin_user_image', base_url().'images/'.lcfirst($arr_user_check[0]['user_type']).'_image/'.$arr_user_check[0]['user_image'], time()+60*60*24*100, "/");	
						

						$arr_response['status']       = "LoginSuccess";
						$arr_response['msg']          = "Login successful";
						$arr_response['user_type']    = $arr_user_check[0]['user_type'];
						echo json_encode($arr_response);
						exit;
					}
					else 
					{
						$arr_response['status'] = "UnverifyUser";
						$arr_response['msg']    = "Please verify your account first";
						echo json_encode($arr_response);
						exit;
					}
				}
				else 
				{
					$arr_response['status'] = "BlockUser";
					$arr_response['msg']    = "You are blocked by admin";
					echo json_encode($arr_response);
					exit;
				}

			}
			else
			{
				$arr_response['status'] = "notFound";
				$arr_response['msg']    = "Please enter correct password.";
				echo json_encode($arr_response);
				exit;
			}
    }

	public function forgot_password() {
		
        if(isset($_POST['email'])) {
			
				$user_email = $this->input->post('email',true);
				$arr_user   = $this->master_model->getRecords('tbl_user_master',array('email'=>trim($user_email)));
				if(sizeof($arr_user) <= 0)
				{
					$arr_response['status'] = "error";
					$arr_response['msg']    = "User does not exits.";
					echo json_encode($arr_response);
					exit;
				}
				if($arr_user[0]["verification_status"] == "Verified")
				{
					$confirm_code = sha1(uniqid().$arr_user[0]['email']);
					$user_id      = $arr_user[0]['id'];
					$this->master_model->updateRecord('tbl_user_master',array('confirm_code'=>$confirm_code),array('id'=>$user_id));

					$admin_data=$this->master_model->getRecords('admin_login', array('id'=>'1'));
					
					    $other_info =array(
				  				           "user_name"    => $arr_user[0]['name'],
				  				           "user_email"   => $user_email,
				  				           'confirm_code' => $confirm_code
			  			);
			  		
			  			$info_arr  = array(
								           'from' 	      => $admin_data[0]['admin_email'],
								           'to'		      => $user_email,
								           'subject'      => PROJECT_NAME.' - Password Reset',
								           'view'	      => 'forgot-password-mail-to-user'
						);

		  				$this->email_sending->sendmail($info_arr,$other_info);

						$arr_response['status'] = "success";
						$arr_response['msg']    = "Password recovery mail sent to your email successfully.";
						echo json_encode($arr_response);
						exit;
				}
				else
				{
					
					$arr_response['status'] = "error";
					$arr_response['msg']    = "Your account verification is pending. please check your email to verify your account.";
					echo json_encode($arr_response);
					exit;
				}
			
		}

        $data['pageTitle']       ='Forgot Password - '.PROJECT_NAME;
   	    $data['page_title']      ='Forgot Password - '.PROJECT_NAME;
   	    $data['middle_content']  ='forgot-password';
	    $this->load->view('template',$data);
	}
	public function reset_password($confirm_code=FALSE) {

		if(isset($_POST['newpwd']))
		{

			$new_password = $this->input->post('newpwd',true);
			$conf_code    = $this->input->post('confirm_code',true);
		
			$this->master_model->updateRecord('tbl_user_master',array("password"=>sha1($new_password),'confirm_code'=>NULL),array('confirm_code'=>"'".$conf_code."'"));

			$arr_response['status'] = "success";
			$arr_response['msg']    = "Password change successfully.";
			echo json_encode($arr_response);
			exit;
		}

        
        $conf_code = trim($confirm_code);
        if(!empty($conf_code))
		{
			$arr_confcode_check = $this->master_model->getRecords('tbl_user_master',array('confirm_code'=>trim($conf_code)));
			if(sizeof($arr_confcode_check) <= 0)
			{
                $this->session->set_flashdata('Error' ,'Reset password link expire');
				redirect(base_url().'login/');
			}
			else
			{
				$db_conf_code = $arr_confcode_check[0]['confirm_code'];
				if($conf_code != $db_conf_code)
				{
					$this->session->set_flashdata('Error' ,'Reset password link expire');
				    redirect(base_url().'login/');
				}
				else
				{
					$data['conf_code'] = $db_conf_code;
					$data['pageTitle']       ='Reset Password - '.PROJECT_NAME;
			   	    $data['page_title']      ='Reset Password - '.PROJECT_NAME;
			   	    $data['middle_content']  ='reset-password';
				    $this->load->view('template',$data);
				}
			}
		}
		else {
			
			$this->session->set_flashdata('error' ,'Reset password link expire');
			redirect(base_url().'login/');
		}
	}

} //  end class
