<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Category extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->session_validator->IsLogged();

		
	}

	/* category listing and added the links for  add/update/delete/block/unbloack */
    public function manage()
	{
	   	$data['pageTitle'] ='Manage category ';
	   	$data['page_title']='Manage Category';
	   	if(isset($_REQUEST['checkbox_del']) && $_REQUEST['checkbox_del']!="")
	   	{
			$chkbox_count=count($_REQUEST['checkbox_del']);
			$action=$_REQUEST['act_status'];

			#----- delete ----#
			if($action=='delete')
			{
			   	for($i=0;$i<$chkbox_count;$i++)
			   	{
					$this->master_model->updateRecord('tbl_category_master',array('is_delete'=>'1'),array('category_id'=>$_REQUEST['checkbox_del'][$i]));
				}
			   	redirect(base_url().ADMIN_PANEL_NAME.'category/manage/');
			}

			#-----block -----#
			if($action=='block')
			{
			   	for($i=0;$i<$chkbox_count;$i++)
			   	{
				   $this->master_model->updateRecord('tbl_category_master',array('category_status'=>'0'),array('category_id'=>$_REQUEST['checkbox_del'][$i]));
				}
				$this->session->set_flashdata('success','Record(s) status updated successfully.');
			   	redirect(base_url().ADMIN_PANEL_NAME.'category/manage/');
			}

			#-----unblock -----#
			if($action=='active')
			{
			   	for($i=0;$i<$chkbox_count;$i++)
			   	{
				   $this->master_model->updateRecord('tbl_category_master',array('category_status'=>'1'),array('category_id'=>$_REQUEST['checkbox_del'][$i]));
			   	}
			   	$this->session->set_flashdata('success','Record(s) status updated successfully.');
			   	redirect(base_url().ADMIN_PANEL_NAME.'category/manage/');
			}
	   	}
	   	$this->db->where('is_delete <>','1');
		$data['fetchcategory']=$this->master_model->getRecords('tbl_category_master',FALSE,FALSE,array('category_id','DESC'));


		
		
		$config['base_url']=base_url().ADMIN_PANEL_NAME."category/manage/";
		$data['middle_content']='category/manage-category';
		$this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}

	/*Add Category Function Start Here*/
	public function add()
	{
		$data['pageTitle']  ='Add Category ';
		$data['page_title'] ='Add Category';

		if(isset($_POST['btn_add_category']))
		{
			//
			$this->form_validation->set_rules('category_name','Category Name','required');
			$this->form_validation->set_rules('page_img','Category image','required');

			if($this->form_validation->run())
			{
				
				$name = $this->input->post('category_name');
				$page_img = $this->input->post('page_img');

				if($this->master_model->getRecords('tbl_category_master',array('category_name'=>$name,'is_delete'=>'0')))
				{
					$this->session->set_flashdata('error','Category name already exist');
					redirect(base_url().ADMIN_PANEL_NAME.'category/add/');
				}
				else
				{

					$config=array(
					'upload_path'    => 'uploads/cat_logo',
					'allowed_types'  => 'jpg|jpeg|png',
					'file_name'      => rand(1,9999999),
					'max_size'       => 5120
					);

					$this->load->library('upload',$config);
					$this->upload->initialize($config);  

					//$default_img = 	"noimage.jpg";
					$default_img = 	"";
					if(isset($_FILES['page_img']) && $_FILES['page_img']['error']==0)
					{
						if($this->upload->do_upload('page_img'))
						{
							$dt=$this->upload->data();
							$file=$dt['file_name'];
							$default_img = $file;
						}
						else
						{
							$this->session->set_flashdata('error',$this->upload->display_errors());
							redirect(base_url().ADMIN_PANEL_NAME."category/add");
						}
					}
					else
					{
						$this->session->set_flashdata('error',"Category image field is required");
						redirect(base_url().ADMIN_PANEL_NAME."category/add");
					}
				
					$insert_array=array('category_name'=>$name,
										'profile_image'=>$default_img,
										'category_status'=>'1'
							);

					$res=$this->master_model->insertRecord('tbl_category_master',$insert_array);

					if($res)
						$this->session->set_flashdata('success','Category added successfully.');
					else
						$this->session->set_flashdata('error','Error while adding category.');
					redirect(base_url().ADMIN_PANEL_NAME.'category/add/');
				}
			}
		}
		$data['middle_content']='category/add-category';
		$this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}

	/*Update Category Function Start Here*/
	public function update()
	{

		$data['page_title'] = 'Update Category ';
		$id                 = $this->uri->segment(4);

		if($id=="")
		{
			redirect(base_url().ADMIN_PANEL_NAME.'category/manage');
		}

		if(isset($_POST['btn_add_category']))
		{
			
			$this->form_validation->set_rules('category_name','Category Name','required');
		
			if($this->form_validation->run())
			{
				$name=$this->input->post("category_name");
				if($this->master_model->getRecords('tbl_category_master',array('category_name'=>$name,'category_id != '=>$id,'is_delete'=>'0')))
				{
					$this->session->set_flashdata('error','Category name already exist');
					redirect(base_url().ADMIN_PANEL_NAME.'category/update/'.$id);
				}
				else
				{
					$config=array(
							'upload_path'   => 'uploads/cat_logo',
							'allowed_types' => 'jpg|jpeg|png|gif|GIF|JPG|JPEG|PNG|svg|SVG', 
							'file_name'     => rand(1,9999999),
							'max_size'      => 5120
							);
						$this->load->library('upload',$config);
						$this->upload->initialize($config); 

						$default_img = $this->input->post("oldimage");

					if(isset($_FILES['page_img']) && $_FILES['page_img']['error']==0 )
					{
						if($this->upload->do_upload('page_img'))
						{
							$dt=$this->upload->data();
							$file=$dt['file_name'];
							@unlink('uploads/cat_logo/'.$default_img);
							$default_img = $file;
						}
						else
						{
							$this->session->set_flashdata('error',$this->upload->display_errors());
							redirect(base_url().ADMIN_PANEL_NAME.'category/update/'.$id);
						}
					}
					$update_array=array('category_name'=>$name,
										'profile_image'=>$default_img

									);

					$update_result=$this->db->update('tbl_category_master',$update_array, array('category_id' => $id));

					if($update_result)
						$this->session->set_flashdata('success','Category updated successfully.');
					else
						$this->session->set_flashdata('error','Error while updating category.');
					redirect(base_url().ADMIN_PANEL_NAME.'category/update/'.$id);
				}
			}
			else
			{
				$this->session->set_flashdata('error',$this->form_validation->error_string());
				redirect(base_url().ADMIN_PANEL_NAME.'category/update/'.$id);
			}
		}

		$data['category_info']=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$id));


        $data['pageTitle']='Manage Category';
		$data['middle_content']='category/edit-category';
		$this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}

	/*Category Status Function Start Here*/
	public function status($sts='',$id='')
	{
		$data['success']=$data['error']='';
		$input_array = array('category_status'=>$sts);
		if($this->master_model->updateRecord('tbl_category_master',$input_array,array('category_id'=>$id)))
		{
			$this->session->set_flashdata('success','Record status updated successfully.');
			redirect(base_url().ADMIN_PANEL_NAME.'category/manage');
		}
		else
		{
			$this->session->set_flashdata('error','Error while updating status.');
			redirect(base_url().ADMIN_PANEL_NAME.'category/manage');
		}
	}

	/*Category Delete Function Start Here*/
	public function delete($id2=FALSE)
	{

		$data['success']=$data['error']='';
	  	if($this->master_model->updateRecord('tbl_category_master',array('is_delete'=>'1'),array('category_id'=>$id2)))
	  	{
	  		$this->session->set_flashdata('success','Category Deleted Successfully.');
	  		redirect(base_url().ADMIN_PANEL_NAME.'category/manage');
	  	}
	  	else
	  	{
	  		$this->session->set_flashdata('error','Error while deleting Record.');
	  		redirect(base_url().ADMIN_PANEL_NAME.'category/manage');
	  	}
	}

} //  end class