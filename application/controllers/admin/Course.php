<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Course extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
	}
	public function index()
	{
		redirect(base_url().'admin/course/manage/');
	}

	public function manage()
	{
        
		$data = array();
		$data['page_title'] = 'Manage Training';
		$data['middle_content']='course/manage-course';
		
		$where=array('is_delete'=>'0','category_status'=>'1');
		$data['fetchcategory']=$this->master_model->getRecords('tbl_category_master',$where);

		//echo'<pre>';
		//print_r($data['fetchcategory']); die;

		if(isset($_REQUEST['checkbox_del']) && $_REQUEST['checkbox_del']!="")
		{
			$chkbox_count=count($_REQUEST['checkbox_del']);
			$action=$_REQUEST['act_status'];
			#-----block -----#
			if($action=='block')
			{
				for($i=0;$i<$chkbox_count;$i++)
				{
					$id = base64_decode($_REQUEST['checkbox_del'][$i]);
					if($this->master_model->updateRecord('tbl_training_course',array('course_status'=>'0','course_front_status'=>'1'),array('course_id'=>$id)))
					{
						$this->session->set_flashdata('success','Service blocked successfully');
					}
				}
				redirect(base_url().'admin/course/manage/');
			}
			  #-----unblock -----#
			elseif($action=='active')
			{
				for($i=0;$i<$chkbox_count;$i++)
				{
					$id = base64_decode($_REQUEST['checkbox_del'][$i]);
					if($this->master_model->updateRecord('tbl_training_course',array('course_status'=>'1','course_front_status'=>'1'),array('course_id'=>$id)))
					{
						$this->session->set_flashdata('success','Service activated successfully');
					}
				}
				redirect(base_url().'admin/course/manage/');
			}
				  #-----delete -----#
			elseif($action=='delete')
			{
				for($i=0;$i<$chkbox_count;$i++)
				{
					$id = base64_decode($_REQUEST['checkbox_del'][$i]);
					if($this->master_model->updateRecord('tbl_training_course',array('course_status'=>'2','course_front_status'=>'0'),array('course_id'=>$id)))
					{
						$this->session->set_flashdata('success','Record(s) deleted successfully');
					}
				}
				redirect(base_url().'admin/course/manage/');
			}
		}

		$this->db->order_by('course_added_date','DESC');
		$data['fetchdata']=$this->master_model->getRecords('tbl_training_course',array('course_status !='=>'2'));
		$this->load->view('admin/template',$data);
	}

	public function add()
	{
		$data = array();
		$ajax_response =array();
		$data['page_title'] = 'Add course';
		$data['middle_content']='course/add-course';

		$where=array('is_delete'=>'0','category_status'=>'1');
		$data['fetchcategory']=$this->master_model->getRecords('tbl_category_master',$where);

		//print_r($data['fetchcategory']); die;	


		if(isset($_POST['course_add']) && $_POST['course_add']==TRUE)
		{
			$this->form_validation->set_rules('course_name_en','course Name','trim|required');
			$this->form_validation->set_rules('course_description_en','course Description','trim|required');
			$this->form_validation->set_rules('course_date','course Date','required');
			$this->form_validation->set_rules('category_name','category  Name','required');
			$this->form_validation->set_rules('course_added_by','course Added By','required');
			if($this->form_validation->run()==FALSE)
			{
				/*Validation Failed*/
				$this->session->set_flashdata('error','Validation Failed!!Enter proper values');
				redirect(base_url()."admin/course/add");
			}
			else
			{
				$course_name_en = $this->input->post('course_name_en');
				$course_category_id = $this->input->post('course_category_id');
				$category_name = $this->input->post('category_name');
				$course_description_en = $this->input->post('course_description_en');
				$course_date=$this->input->post('course_date');
				$course_added_by=$this->input->post('course_added_by');

					/* Check Image Uploaded or not */
					$config=array(
						'upload_path'=>'uploads/course_images',
						'allowed_types'=>'jpg|jpeg|png',
						'file_name'=>rand(1,9999999),
						'max_size'=>5120
						);
					$this->load->library('upload',$config);
					$this->upload->initialize($config); //if missing this file error occured "cannot find valid upload path"
					$default_img = 	"noimage.jpg";
					if(isset($_FILES['sv_logo']) && $_FILES['sv_logo']['error']==0)
					{
						if($this->upload->do_upload('sv_logo'))
						{
							$dt=$this->upload->data();
							$file=$dt['file_name'];
							$default_img = $file;
						}
						else
						{
							$this->session->set_flashdata('error',$this->upload->display_errors());
							redirect(base_url()."admin/course/add");
						}
					}
					else
					{
						$default_img = 	"";
					}


					$arr_details = array(
						"course_name_en"=>$course_name_en,
						'category_id'=>$category_name,
						"course_description_en"=>$course_description_en,
						"course_added_by"=>$course_added_by,
						"course_added_date"=>date('Y-m-d',strtotime($course_date)),
						"course_status"         => '1',
						"course_img"=>$default_img
						);
					 $rec_cnt=$this->master_model->getRecordCount('tbl_training_course',array('course_name_en'=>$course_name_en,'course_status'=>1));
				//print_r($rec_cnt); exit;
					 //echo'hi mayur'; die;

					if($rec_cnt==0)

					{
						if($this->master_model->insertRecord('tbl_training_course',$arr_details))
							//print_r($this); die;
						{
							//print_r($data); die;	
							/* Record Insertion Success */
							/* If ajax response then set store operation status in variable else in flashdata */
							if(isset($_POST['ajax_request']) && $_POST['ajax_request']==TRUE)
							{
								$ajax_response['success'] =  'course added successfully';
							}
							else
							{
								$this->session->set_flashdata('success','course added successfully');
							}
						}
						else
						{
							/* Record Insertion Failed */
							/* If ajax response then set store operation status in variable else in flashdata */
							if(isset($_POST['ajax_request']) && $_POST['ajax_request']==TRUE)
							{
								$ajax_response['error'] =  'Failed to add course ';
							}
							else
							{
								$this->session->set_flashdata('error','Failed to add course');
							}
						}
					}
					else
					{
						$this->session->set_flashdata('error',"Name already existes.Enter some other name");
					}

				/* If request from ajax call */
				if(isset($_POST['ajax_request']) && $_POST['ajax_request']==TRUE)
				{
					if(array_key_exists('success', $ajax_response))
					{
						echo json_encode(array('sts'=>"OK",'msg'=>$ajax_response['success']))	;
						exit;
					}
					elseif (array_key_exists('error', $ajax_response))
					{
						echo json_encode(array('sts'=>"ERROR",'msg'=>$ajax_response['error']))	;
						exit;
					}
					exit;
				}
				else
				{
					redirect(base_url()."admin/course/add");
				}
			}
		}
		
		$this->load->view('admin/template',$data);
	}

	public function edit($course_id)
	{
		$data = array();
		$data['page_title'] = 'Edit course';
		$data['middle_content']='course/edit-course';

		$where=array('is_delete'=>'0','category_status'=>'1');
		$data['fetchcategory']=$this->master_model->getRecords('tbl_category_master',$where);

		//echo'<pre>';
		//print_r($data['fetchcategory']); die;	

		if($course_id!='')
		{

			$course_id = base64_decode($course_id);
			/* Retrieving services Details */
			$arr_cond= array('course_status <>'=>'2','course_id'=>$course_id);
			$arr_details = $this->master_model->getRecords('tbl_training_course',$arr_cond);
			if(sizeof($arr_details)>0)
			{
			    // Get first record specifically
				$arr_main_details = reset($arr_details);
			    // Deallosve $arr_details
				unset($arr_details);
				$data['course_details']= $arr_main_details;
			}
			else
			{
				// No records Found
				redirect(base_url()."admin/course/");
			}
			/* Following Code block excecutes while saving update record */
			if(isset($_POST['course_edit']) && $_POST['course_edit']==TRUE)
			{

				$this->form_validation->set_rules('course_name_en','course Name','trim|required');
				$this->form_validation->set_rules('course_description_en','course Discription','trim|required');
				$this->form_validation->set_rules('category_name','category  Name','required');
				$this->form_validation->set_rules('course_added_by','course Added By','required');
				
				if($this->form_validation->run()==FALSE)
				{
						// Record Updation Failed
					$this->session->set_flashdata('error','Validation Failed');
					redirect(base_url()."admin/course/edit/".$course_id);
					/*Validation Failed*/
				}
				else
				{
					$course_name_en = $this->input->post('course_name_en');
					$course_description_en = $this->input->post('course_description_en');
					$category_name = $this->input->post('category_name');
					$course_added_by=$this->input->post('course_added_by');
					$course_date=$this->input->post('course_date');
					

						/* Check Image Uploaded or not */
						$config=array(
							'upload_path'=>'uploads/course_images',
							'allowed_types'=>'jpg|jpeg|png',
							'file_name'=>rand(1,9999999),
							'max_size'=>5120
							);
						$this->load->library('upload',$config);
						$this->upload->initialize($config); //if missing this file error occured "cannot find valid upload path"
						 $default_img = 	isset($arr_main_details['course_img'])?$arr_main_details['course_img']:"noimage.jpg";
						if(isset($_FILES['course_logo']) && $_FILES['course_logo']['error']==0)
						{
							if($this->upload->do_upload('course_logo'))
							{
								$dt=$this->upload->data();
								$file=$dt['file_name'];
								$default_img = $file;
								if($arr_main_details['course_img']!="noimage.jpg")
								{
									@unlink($this->config->item('course_img').$arr_main_details['course_img']);
									@unlink($this->config->item('course_img').'thumb/'.$arr_main_details['course_img']);
								}
							}
							else
							{
								$default_img = 	"";
							}
						}
						$arr_details = array('course_name_en'=>$course_name_en,
											 'course_description_en'=>$course_description_en,
											 'category_id'=>$category_name,
						                     'course_img'=>$default_img,
						                     'course_added_by'=>$course_added_by,
						                     'course_added_date'=>date('Y-m-d',strtotime($course_date)));
						if($this->master_model->updateRecord('tbl_training_course',$arr_details,array('course_id'=>$course_id)))
						{
							// Record Updation Success
							$this->session->set_flashdata('success','Course Update successfully');
							redirect(base_url().'admin/course/manage');
						}
						else
						{
							// Record Updation Failed
							$this->session->set_flashdata('error','Failed to update service');
						}


					redirect(base_url()."admin/course/edit/".base64_encode($course_id));
				}
			}

			$this->load->view('admin/template',$data);
		}
		else
		{
			//echo "Step three";
			exit;
			// Parameter missing
			redirect(base_url()."admin/course/");
		}
	}

	public function toggle_status($course_id,$status)
	{
		if($course_id!='' && $status!='' )
		{
			if($status=="0")
			{
				$id = base64_decode($course_id);
				if($this->master_model->updateRecord('tbl_training_course',array('course_status'=>'0','course_front_status'=>'1'),array('course_id'=>$id)))
				{
					$this->session->set_flashdata('success','Status update successfully');
				}
			}
			elseif($status=="1")
			{
				$id = base64_decode($course_id);
				if($this->master_model->updateRecord('tbl_training_course',array('course_status'=>'1','course_front_status'=>'1'),array('course_id'=>$id)))
				{
					$this->session->set_flashdata('success','Status updated successfully ');
				}
			}
			elseif($status=="2")
			{
				$id = base64_decode($course_id);

					if($this->master_model->updateRecord('tbl_training_course',array('course_status'=>'2','course_front_status'=>'0'),array('course_id'=>$id)))
					{
						$this->session->set_flashdata('success','Record(s) Deleted Successfully');
					}

			}
			redirect(base_url()."admin/course/manage/");
		}
		else
		{
			// Parameter missing
			redirect(base_url()."admin/services/");
		}
	}

	public function toggle_visibility($course_id,$status)
	{
		if(strlen($course_id)>0 && strlen($status)>0)
		{
			$safe_course_id = base64_decode($course_id);
			$arr_update_data = array();
			if($status=="0")
			{
				$arr_update_data  = array('course_front_status'=>'0');
			}
			elseif($status=="1")
			{
				$arr_update_data  = array('course_front_status'=>'1');
			}
			if(sizeof($arr_update_data)>0)
			{
				$this->master_model->updateRecord('tbl_training_course',$arr_update_data,array('course_id'=>"'".$safe_course_id."'"));
				if($this->db->affected_rows()>0)
				{
					$this->session->set_flashdata('success','Course Visibility Updated Succesfully');
				}
				else
				{
					$this->session->set_flashdata('error','Problem Occured , Please try again');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Problem Occured , Please try again');
			}
			redirect(base_url()."admin/course/");
		}
		else
		{
			$this->session->set_flashdata('error','Problem Occured , Please try again');
			redirect(base_url()."admin/course/");
		}
	}

	public function details($course_id)
	{
		$id=base64_decode($course_id);
		$data['page_title']='course Details';
		$data['fetchdata']=$this->master_model->getRecords('tbl_training_course',array('course_id'=>$id));
		$data['middle_content']='course/detail-course';
		$this->load->view('admin/template',$data);
	}

}
?>