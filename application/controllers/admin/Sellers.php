<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sellers extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->session_validator->IsLogged();
	}

	/* subcategory listing and added the links for  add/update/delete/block/unbloack */
    public function manage()
	{
	   	$data['pageTitle']  = 'Training Providers';
	   	$data['page_title'] = 'Training Providers';
	   	if(isset($_REQUEST['checkbox_del']) && $_REQUEST['checkbox_del']!="")
	   	{
			$chkbox_count =count($_REQUEST['checkbox_del']);
			$action       =$_REQUEST['act_status'];

			#----- delete ----#
			if($action=='delete')
			{
			   	for($i=0;$i<$chkbox_count;$i++)
			   	{
			   		$this->session->set_flashdata('success','Record(s) deleted successfully.');
					$this->master_model->updateRecord('tbl_user_master',array('status'=>'Delete'),array('id'=>$_REQUEST['checkbox_del'][$i]));
				}
			   	redirect(base_url().ADMIN_PANEL_NAME.'sellers/manage/');
			}
			#-----block -----#
			if($action=='block')
			{
			   	for($i=0;$i<$chkbox_count;$i++)
			   	{
				   $this->master_model->updateRecord('tbl_user_master',array('status'=>'Block'),array('id'=>$_REQUEST['checkbox_del'][$i]));
				}
				$this->session->set_flashdata('success','Record(s) status updated successfully.');
			   	redirect(base_url().ADMIN_PANEL_NAME.'sellers/manage/');
			}
			#-----unblock -----#
			if($action=='active')
			{
			   	for($i=0;$i<$chkbox_count;$i++)
			   	{
				   $this->master_model->updateRecord('tbl_user_master',array('status'=>'Unblock'),array('id'=>$_REQUEST['checkbox_del'][$i]));
			   	}
			   	$this->session->set_flashdata('success','Record(s) status updated successfully.');
			   	redirect(base_url().ADMIN_PANEL_NAME.'sellers/manage/');
			}
	   	}
	   	$this->db->where('status !=','Delete');
	   	$this->db->where('user_type','Seller');
	   	$this->db->where('verification_status','Verified');
		$data['fetchsellers']   = $this->master_model->getRecords('tbl_user_master',FALSE,FALSE,array('id','DESC'));
    
		$data['middle_content'] ='sellers/manage-sellers';
		$this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}

	/* Add Training Provider function start here */

	public function add()
	{
		$data = array();
		$ajax_response =array();
		$data['page_title'] = 'Add Course Provider';
		$data['middle_content']='Course Provider/add-Course Provider';
		if(isset($_POST['blogs_add']) && $_POST['blogs_add']==TRUE)
		{
			$this->form_validation->set_rules('blogs_name_en','Blogs Name','trim|required');
			$this->form_validation->set_rules('blogs_description_en','Blogs Description','trim|required');
			$this->form_validation->set_rules('blogs_date','Blogs Date','required');
			$this->form_validation->set_rules('blogs_added_by','Blogs Added By','required');
			if($this->form_validation->run()==FALSE)
			{
				/*Validation Failed*/
				$this->session->set_flashdata('error','Validation Failed!!Enter proper values');
				redirect(base_url()."admin/blogs/add");
			}
			else
			{
				$blogs_name_en = $this->input->post('blogs_name_en');
				$blogs_category_id = $this->input->post('blogs_category_id');
				$blogs_description_en = $this->input->post('blogs_description_en');
				$blogs_date=$this->input->post('blogs_date');
				$blogs_added_by=$this->input->post('blogs_added_by');

					/* Check Image Uploaded or not */
					$config=array(
						'upload_path'=>'uploads/blogs_images',
						'allowed_types'=>'jpg|jpeg|png',
						'file_name'=>rand(1,9999999),
						'max_size'=>5120
						);
					$this->load->library('upload',$config);
					$this->upload->initialize($config); //if missing this file error occured "cannot find valid upload path"
					$default_img = 	"noimage.jpg";
					if(isset($_FILES['sv_logo']) && $_FILES['sv_logo']['error']==0)
					{
						if($this->upload->do_upload('sv_logo'))
						{
							$dt=$this->upload->data();
							$file=$dt['file_name'];
							$default_img = $file;
						}
						else
						{
							$this->session->set_flashdata('error',$this->upload->display_errors());
							redirect(base_url()."admin/blogs/add");
						}
					}
					else
					{
						$default_img = 	"";
					}


					$arr_details = array(
						"blogs_name_en"=>$blogs_name_en,
						"blogs_description_en"=>$blogs_description_en,
						"blogs_added_by"=>$blogs_added_by,
						"blogs_added_date"=>date('Y-m-d',strtotime($blogs_date)),
						"blogs_status"         => '1',
						"blogs_img"=>$default_img
						);
					 $rec_cnt=$this->master_model->getRecordCount('tbl_blogs_master',array('blogs_name_en'=>$blogs_name_en,'blogs_status'=>1));
					if($rec_cnt==0)
					{
						if($this->master_model->insertRecord('tbl_blogs_master',$arr_details))
						{
							/* Record Insertion Success */
							/* If ajax response then set store operation status in variable else in flashdata */
							if(isset($_POST['ajax_request']) && $_POST['ajax_request']==TRUE)
							{
								$ajax_response['success'] =  'Blogs added successfully';
							}
							else
							{
								$this->session->set_flashdata('success','Blogs added successfully');
							}
						}
						else
						{
							/* Record Insertion Failed */
							/* If ajax response then set store operation status in variable else in flashdata */
							if(isset($_POST['ajax_request']) && $_POST['ajax_request']==TRUE)
							{
								$ajax_response['error'] =  'Failed to add Blogs ';
							}
							else
							{
								$this->session->set_flashdata('error','Failed to add Blogs');
							}
						}
					}
					else
					{
						$this->session->set_flashdata('error',"Name already existes.Enter some other name");
					}

				/* If request from ajax call */
				if(isset($_POST['ajax_request']) && $_POST['ajax_request']==TRUE)
				{
					if(array_key_exists('success', $ajax_response))
					{
						echo json_encode(array('sts'=>"OK",'msg'=>$ajax_response['success']))	;
						exit;
					}
					elseif (array_key_exists('error', $ajax_response))
					{
						echo json_encode(array('sts'=>"ERROR",'msg'=>$ajax_response['error']))	;
						exit;
					}
					exit;
				}
				else
				{
					redirect(base_url()."admin/blogs/add");
				}
			}
		}
	}	

	/*Subcategory Status Function Start Here*/
	public function status($sts='',$id='')
	{
		$data['success'] = $data['error']='';
		$input_array     = array('status'=>$sts);

		if($this->master_model->updateRecord('tbl_user_master',$input_array,array('id'=>$id)))
		{
			$this->session->set_flashdata('success','Record status updated successfully.');
			redirect(base_url().ADMIN_PANEL_NAME.'sellers/manage');
		}
		else
		{
			$this->session->set_flashdata('error','Error while updating status.');
			redirect(base_url().ADMIN_PANEL_NAME.'sellers/manage');
		}
	}

	/*Subcategory Delete Function Start Here*/
	public function delete($id2='')
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->updateRecord('tbl_user_master',array('status'=>'Delete'),array('id'=>$id2)))
	  	{
	  		$this->session->set_flashdata('success','Seller deleted successfully.');
	  		redirect(base_url().ADMIN_PANEL_NAME.'sellers/manage');
	  	}
	  	else
	  	{
	  		$this->session->set_flashdata('error','Error while deleting seller.');
	  		redirect(base_url().ADMIN_PANEL_NAME.'sellers/manage');
	  	}
	}

	public function view($user_id='')
	{
		/* VIew User Info */
		$data['pageTitle']  = 'Seller Details';
	   	$data['page_title'] = 'Seller Details';
		$user_id            = $user_id;
		$data['user_info']  = $this->master_model->getRecords('tbl_user_master',array('id'=>$user_id));

		$data['middle_content']='sellers/view-user';
        $this->load->view(ADMIN_PANEL_NAME.'template',$data);	
    }
    public function offers($seller_id=FALSE)
	{

        $user_info  = $this->master_model->getRecords('tbl_user_master',array('id'=>$seller_id));

        if(isset($user_info[0]['name']) && !empty($user_info[0]['name'])) {

        	$user_info[0]['name'] =$user_info[0]['name'];
        }
        else{

        	$user_info[0]['name'] = "unknown";
        }


	   	$data['pageTitle']  = 'Offers of '.$user_info[0]['name'];
	   	$data['page_title'] = 'Offers of '.$user_info[0]['name'];
	   	
        $this->db->where('tbl_seller_products_offers.seller_id' , $seller_id);
	   	$this->db->where('tbl_seller_products_offers.status' , 'Unblock');
        $this->db->where('tbl_seller_products_offers.status <>' , 'Delete');
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_seller_products_offers.subcategory_id');
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
        $data['getOffers']  = $this->master_model->getRecords('tbl_seller_products_offers');
    
		$data['middle_content'] ='sellers/manage-offers';
		$this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}
	public function offer_detail($offer_id=FALSE)
	{

        $user_info  = $this->master_model->getRecords('tbl_seller_products_offers',array('id'=>$offer_id));

        if(isset($user_info[0]['title']) && !empty($user_info[0]['title'])) {

        	$user_info[0]['title'] =$user_info[0]['title'];
        }
        else{

        	$user_info[0]['title'] = "unknown";
        }

	   	$data['pageTitle']  = 'Details of '.$user_info[0]['title'];
	   	$data['page_title'] = 'Details of '.$user_info[0]['title'];
	   	
        $this->db->where('tbl_seller_products_offers.id' , $offer_id);
	   	$this->db->where('tbl_seller_products_offers.status' , 'Unblock');
        $this->db->where('tbl_seller_products_offers.status <>' , 'Delete');
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_seller_products_offers.subcategory_id');
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
        $data['getOfferDetail']  = $this->master_model->getRecords('tbl_seller_products_offers');
		$data['middle_content'] ='sellers/view-offer';
		$this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}
	public function products($seller_id=FALSE)
	{

        $user_info  = $this->master_model->getRecords('tbl_user_master',array('id'=>$seller_id));

        if(isset($user_info[0]['name']) && !empty($user_info[0]['name'])) {

        	$user_info[0]['name'] =$user_info[0]['name'];
        }
        else{

        	$user_info[0]['name'] = "unknown";
        }

	   	$data['pageTitle']  = 'Products of '.$user_info[0]['name'];
	   	$data['page_title'] = 'Products of '.$user_info[0]['name'];
	   	
       //$this->db->where('tbl_subcategory_master.is_delete <>','1');
	   	$this->db->where('tbl_seller_upload_product.seller_id' , $seller_id);
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_seller_upload_product.subcategory_id');
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
        $data['getProducts']  = $this->master_model->getRecords('tbl_seller_upload_product');
    
		$data['middle_content'] ='sellers/manage-products';
		$this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}
	public function product_detail($product_id=FALSE)
	{

        $user_info  = $this->master_model->getRecords('tbl_seller_upload_product',array('id'=>$product_id));

        if(isset($user_info[0]['title']) && !empty($user_info[0]['title'])) {

        	$user_info[0]['title'] =$user_info[0]['title'];
        }
        else{

        	$user_info[0]['title'] = "unknown";
        }

	   	$data['pageTitle']  = 'Details of '.$user_info[0]['title'];
	   	$data['page_title'] = 'Details of '.$user_info[0]['title'];
	   	
        $this->db->where('tbl_seller_upload_product.id' , $product_id);
	   	$this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_seller_upload_product.subcategory_id');
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
        $data['getProductsDetail']  = $this->master_model->getRecords('tbl_seller_upload_product');
		$data['middle_content'] ='sellers/view-product';
		$this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}
	

} // end class