<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Buyers extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->session_validator->IsLogged();
	}

	/* subcategory listing and added the links for  add/update/delete/block/unbloack */
    public function manage()
	{
	   	$data['pageTitle']  = 'End Users';
	   	$data['page_title'] = 'End Users';
	   	if(isset($_REQUEST['checkbox_del']) && $_REQUEST['checkbox_del']!="")
	   	{
			$chkbox_count=count($_REQUEST['checkbox_del']);
			$action=$_REQUEST['act_status'];

			#----- delete ----#
			if($action=='delete')
			{
			   	for($i=0;$i<$chkbox_count;$i++)
			   	{
			   		$this->session->set_flashdata('success','Record(s) deleted successfully.');
					$this->master_model->updateRecord('tbl_user_master',array('status'=>'Delete'),array('id'=>$_REQUEST['checkbox_del'][$i]));
				}
			   	redirect(base_url().ADMIN_PANEL_NAME.'buyers/manage/');
			}

			#-----block -----#
			if($action=='block')
			{
			   	for($i=0;$i<$chkbox_count;$i++)
			   	{
				   $this->master_model->updateRecord('tbl_user_master',array('status'=>'Block'),array('id'=>$_REQUEST['checkbox_del'][$i]));
				}
				$this->session->set_flashdata('success','Record(s) status updated successfully.');
			   	redirect(base_url().ADMIN_PANEL_NAME.'buyers/manage/');
			}

			#-----unblock -----#
			if($action=='active')
			{
			   	for($i=0;$i<$chkbox_count;$i++)
			   	{
				   $this->master_model->updateRecord('tbl_user_master',array('status'=>'Unblock'),array('id'=>$_REQUEST['checkbox_del'][$i]));
			   	}
			   	$this->session->set_flashdata('success','Record(s) status updated successfully.');
			   	redirect(base_url().ADMIN_PANEL_NAME.'buyers/manage/');
			}
	   	}
	   	$this->db->where('status !=','Delete');
	   	$this->db->where('user_type','Buyer');
	   	$this->db->where('verification_status','Verified');
		$data['fetchbuyers']   = $this->master_model->getRecords('tbl_user_master',FALSE,FALSE,array('id','DESC'));
    
		$data['middle_content'] ='buyers/manage-buyers';
		$this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}

	/*Subcategory Status Function Start Here*/
	public function status($sts='',$id='')
	{
		$data['success'] = $data['error']='';
		$input_array     = array('status'=>$sts);

		if($this->master_model->updateRecord('tbl_user_master',$input_array,array('id'=>$id)))
		{
			$this->session->set_flashdata('success','Record status updated successfully.');
			redirect(base_url().ADMIN_PANEL_NAME.'buyers/manage');
		}
		else
		{
			$this->session->set_flashdata('error','Error while updating status.');
			redirect(base_url().ADMIN_PANEL_NAME.'buyers/manage');
		}
	}

	/*Subcategory Delete Function Start Here*/
	public function delete($id2='')
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->updateRecord('tbl_user_master',array('status'=>'Delete'),array('id'=>$id2)))
	  	{
	  		$this->session->set_flashdata('success','Buyers deleted successfully.');
	  		redirect(base_url().ADMIN_PANEL_NAME.'buyers/manage');
	  	}
	  	else
	  	{
	  		$this->session->set_flashdata('error','Error while deleting buyers.');
	  		redirect(base_url().ADMIN_PANEL_NAME.'buyers/manage');
	  	}
	}

    public function view($user_id='')
	{
		/* VIew User Info */
		$data['pageTitle']  = 'Buyer Details';
	   	$data['page_title'] = 'Buyer Details';
		$user_id            = $user_id;
		$data['user_info']  = $this->master_model->getRecords('tbl_user_master',array('id'=>$user_id));

		$data['middle_content']='buyers/view-user';
        $this->load->view(ADMIN_PANEL_NAME.'template',$data);	
    }
    public function requirments($buyer_id=FALSE)
	{

        $user_info  = $this->master_model->getRecords('tbl_user_master',array('id'=>$buyer_id));

        if(isset($user_info[0]['name']) && !empty($user_info[0]['name'])) {

        	$user_info[0]['name'] =$user_info[0]['name'];
        }
        else{

        	$user_info[0]['name'] = "unknown";
        }

	   	$data['pageTitle']  = 'Requirments of '.$user_info[0]['name'];
	   	$data['page_title'] = 'Requirments of '.$user_info[0]['name'];
	   	
        $this->db->where('tbl_buyer_post_requirement.buyer_id' , $buyer_id);
	   	$this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
        $this->db->where('tbl_buyer_post_requirement.status <>' , 'Delete');
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_buyer_post_requirement.subcategory_id');
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
        $data['getrequirments']  = $this->master_model->getRecords('tbl_buyer_post_requirement');
    
		$data['middle_content'] ='buyers/manage-requirment';
		$this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}
	public function requirment_detail($requirment_id=FALSE)
	{

        $user_info  = $this->master_model->getRecords('tbl_buyer_post_requirement',array('id'=>$requirment_id));

        if(isset($user_info[0]['title']) && !empty($user_info[0]['title'])) {

        	$user_info[0]['title'] =$user_info[0]['title'];
        }
        else{

        	$user_info[0]['title'] = "unknown";
        }

	   	$data['pageTitle']  = 'Details of '.$user_info[0]['title'];
	   	$data['page_title'] = 'Details of '.$user_info[0]['title'];
	   	
        $this->db->where('tbl_buyer_post_requirement.id' , $requirment_id);
	   	$this->db->where('tbl_buyer_post_requirement.status' , 'Unblock');
        $this->db->where('tbl_buyer_post_requirement.status <>' , 'Delete');
        $this->db->join('tbl_subcategory_master' , 'tbl_subcategory_master.subcategory_id = tbl_buyer_post_requirement.subcategory_id');
        $this->db->join('tbl_category_master' , 'tbl_category_master.category_id = tbl_subcategory_master.category_id');
        $data['getRequirmentDetail']  = $this->master_model->getRecords('tbl_buyer_post_requirement');
		$data['middle_content'] ='buyers/view-requirment';
		$this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}
} // end Class 