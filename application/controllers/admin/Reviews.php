<?php if(!defined('BASEPATH')) exit('No direct script access allow');
class Reviews extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->session_validator->IsLogged();

	}

	public function manage()
	{
		$data['pageTitle']        = 'Reviews - '.PROJECT_NAME;
   	    $data['page_title']       = 'Reviews - - '.PROJECT_NAME;
   	    $data['middle_content']   = 'sellers/review-ratings';

   	    if(isset($_REQUEST['checkbox_del']) && $_REQUEST['checkbox_del']!="")
	   	{
			$chkbox_count=count($_REQUEST['checkbox_del']);
			$action=$_REQUEST['act_status'];

			#----- delete ----#
			if($action=='delete')
			{
			   	for($i=0;$i<$chkbox_count;$i++)
			   	{
			   		$this->session->set_flashdata('success','Record(s) deleted successfully.');
					$this->master_model->updateRecord('tbl_seller_rating',array('status'=>'Delete'),array('review_id'=>$_REQUEST['checkbox_del'][$i]));
				}
			   	redirect(base_url().ADMIN_PANEL_NAME.'reviews/manage/');
			}

			#-----block -----#
			if($action=='block')
			{
			   	for($i=0;$i<$chkbox_count;$i++)
			   	{
				   $this->master_model->updateRecord('tbl_seller_rating',array('status'=>'Block'),array('review_id'=>$_REQUEST['checkbox_del'][$i]));
				}
				$this->session->set_flashdata('success','Record(s) status updated successfully.');
			   	redirect(base_url().ADMIN_PANEL_NAME.'reviews/manage/');
			}

			#-----unblock -----#
			if($action=='active')
			{
			   	for($i=0;$i<$chkbox_count;$i++)
			   	{
				   $this->master_model->updateRecord('tbl_seller_rating',array('status'=>'Unblock'),array('review_id'=>$_REQUEST['checkbox_del'][$i]));
			   	}
			   	$this->session->set_flashdata('success','Record(s) status updated successfully.');
			   	redirect(base_url().ADMIN_PANEL_NAME.'reviews/manage/');
			}
	   	}

        $this->db->where('tbl_seller_rating.status <>' , 'Delete');
        $this->db->order_by('tbl_seller_rating.review_id' , 'desc');
        $data['getreview']  = $this->master_model->getRecords('tbl_seller_rating');
        $this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}
	public function status($sts='',$id='')
	{
		$data['success'] = $data['error']='';
		$input_array     = array('status'=>$sts);

		if($this->master_model->updateRecord('tbl_seller_rating',$input_array,array('review_id'=>$id)))
		{
			$this->session->set_flashdata('success','Record status updated successfully.');
			redirect(base_url().ADMIN_PANEL_NAME.'reviews/manage');
		}
		else
		{
			$this->session->set_flashdata('error','Error while updating status.');
			redirect(base_url().ADMIN_PANEL_NAME.'reviews/manage');
		}
	}
	public function delete($review_id)
	{
		if($review_id!='')
		{
			$del=$this->master_model->updateRecord('tbl_seller_rating',array('status'=>'Delete'),array('review_id'=>$review_id));
			if($del)
			{
				$this->session->set_flashdata('success','Review deleted successfully.');
				redirect(base_url().ADMIN_PANEL_NAME.'reviews/manage');
			}
			else
			{
				$this->session->set_flashdata('error','Error While Deleting review.');
				redirect(base_url().ADMIN_PANEL_NAME.'reviews/manage');
			}
		}
	}
}
?>