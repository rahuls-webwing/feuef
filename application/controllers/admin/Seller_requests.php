<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Seller_requests extends CI_Controller
{
	/*Call construct for load auto model and all things */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('email_sending');
	}

    public function for_products()
	{
	   	$data['pageTitle']     ='Upload product requests';
	   	$data['page_title']    ='Upload product requests';

		$this->db->where('status','open');
		$this->db->where('is_accepet','no');
		$this->db->order_by('id','desc');
		$data['requestdata']   = $this->master_model->getRecords('tbl_sellers_upload_product_request');
		$data['middle_content']= 'sellers_requests/product_upload_requests/manage_requests';
		$this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}

	public function details($req_id='')
	{
	   $req_id=$req_id;
	   $data['page_title'] ='Product Request Details';
	   $data['pageTitle']  ='Product Request Details';
	   if($req_id=='')
	   {
	   		redirect(base_url().ADMIN_PANEL_NAME."seller_requests/for_products");
	   }
	   $data['fetchdata']=$this->master_model->getRecords('tbl_sellers_upload_product_request',array('id'=>$req_id));
	   $data['middle_content']='sellers_requests/product_upload_requests/request_details';
	   $this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}
    
	
	public function toggle_visibility($req_id)
	{
		
		$this->master_model->updateRecord('tbl_sellers_upload_product_request',array('is_accepet' => 'yes'),array('id'=>"'".$req_id."'"));
		if($this->db->affected_rows()>0)
		{
			$this->session->set_flashdata('success','You accept this request successfully.');
			redirect(base_url().ADMIN_PANEL_NAME."seller_requests/for_products");
		}
		else
		{
			$this->session->set_flashdata('error','Problem Occured , Please try again');
	        redirect(base_url().ADMIN_PANEL_NAME."seller_requests/for_products");
		}
	}


    public function for_offers()
	{
	   	$data['pageTitle']     ='Make offer requests (from live market)';
	   	$data['page_title']    ='Make offer requests (from live market)';

		$this->db->where('status','open');
		$this->db->where('is_accepet','no');
		$this->db->order_by('id','desc');
		$data['requestdata']   = $this->master_model->getRecords('tbl_sellers_make_offer_purchase_request');
		$data['middle_content']= 'sellers_requests/make_offer_for_requirement_requests/manage_requests';
		$this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}
	public function request_details($req_id='')
	{
	   $req_id=$req_id;
	   $data['page_title'] ='Make offer request details';
	   $data['pageTitle']  ='Make offer request details';
	   if($req_id=='')
	   {
	   		redirect(base_url().ADMIN_PANEL_NAME."seller_requests/for_offers");
	   }
	   $data['fetchdata']=$this->master_model->getRecords('tbl_sellers_make_offer_purchase_request',array('id'=>$req_id));
	   $data['middle_content']='sellers_requests/make_offer_for_requirement_requests/request_details';
	   $this->load->view(ADMIN_PANEL_NAME.'template',$data);
	}
	public function offers_toggle_visibility($req_id)
	{
		
		$this->master_model->updateRecord('tbl_sellers_make_offer_purchase_request',array('is_accepet' => 'yes'),array('id'=>"'".$req_id."'"));
		if($this->db->affected_rows()>0)
		{
			$this->session->set_flashdata('success','You accept this request successfully.');
			redirect(base_url().ADMIN_PANEL_NAME."seller_requests/for_offers");
		}
		else
		{
			$this->session->set_flashdata('error','Problem Occured , Please try again');
	        redirect(base_url().ADMIN_PANEL_NAME."seller_requests/for_offers");
		}
	}
   
} // end Class