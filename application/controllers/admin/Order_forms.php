<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Order_forms extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->session_validator->IsLogged();
	}

	/* subcategory listing and added the links for  add/update/delete/block/unbloack */
	public function all()
	{

    	$data['pageTitle']       = 'Order forms';
   	    $data['page_title']      = 'Order forms';
   	    $data['middle_content']  = 'Orderforms/all-orderforms';

        
        $this->db->order_by('id' , 'Desc');
        $data['getOffers']  = $this->master_model->getRecords('tbl_order_forms');
        $Count = count($data['getOffers']);



        /* create pagination */
	    $this->load->library('pagination');
	    $config1['total_rows']          = $Count;
	    $config1['base_url']            = base_url().'admin/order_forms/all/';
	    $config1['per_page']            = 15;
	    $config1['uri_segment']         = '4';
	    $config1['full_tag_open']        = '<div class="pagination pull-right"><ul class="pagination pagination-blog">';
	    $config1['full_tag_close']       = '</ul></div>';

	    $config1['first_link']           = '<i class="fa fa-angle-double-left" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['first_tag_open']       = '<li class="prev page">';
	    $config1['first_tag_close']      = '</li>';

	    $config1['last_link']            = '<i class="fa fa-angle-double-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['last_tag_open']        = '<li class="next page">';
	    $config1['last_tag_close']       = '</li>';

	    $config1['next_link']            = '<i class="fa fa-angle-right" style="font-size: 1.4em;" aria-hidden="true"></i>';
	    $config1['next_tag_open']        = '<li class="next page">';
	    $config1['next_tag_close']       = '</li>';

	    $config1['prev_link']            = '<i class="fa fa-angle-left" style="font-size: 1.4em;"></i>';
	    $config1['prev_tag_open']        = '<li class="prev page">';
	    $config1['prev_tag_close']       = '</li>';

	    $config1['cur_tag_open']         = '<li ><a href="" class="act" style="color: rgb(242, 246, 249);background-color: #034A7B;" >';
	    $config1['cur_tag_close']        = '</a></li>';

	    $config1['num_tag_open']         = '<li class="page">';
	    $config1['num_tag_close']        = '</li>'; 
	    

	    $this->pagination->initialize($config1);
	    $page        = ($this->uri->segment(4));
	    /* end create pagination */

        $this->db->order_by('id' , 'Desc');
        $data['getOffers'] = $this->master_model->getRecords('tbl_order_forms',FALSE,FALSE,FALSE,$page,$config1["per_page"]);

       


	    $this->load->view('admin/template',$data);
	}


} // end Class 